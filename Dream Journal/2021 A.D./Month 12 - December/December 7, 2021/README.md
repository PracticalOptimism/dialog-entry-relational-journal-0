


# Dream Journal Entry - December 7, 2021

### [Written @ 11:17]

I had a dream last night.

(1) I saw a person that looked like Dr. Hanson from "Hanson Robotics"
(2) The person gave me a showing of a secret technology
(3) The technology was a view of "how to make certain visual information technology systems"

(1) If you draw rectangles
(2) And you stretch the rectangles
(3) Stretch the rectangles, vertically or horizontally
(4) Shrink the rectangles, vertically or horizontally
(5) Draw something on the rectangles
(6) Have a transparent background if needed
(7) Have multiple rectangles that overlap and have transparent backgrounds

Redraw the frame of the rectangle with a new image if you want to capture a new perspective.

This was like a "Massive Animation System"

It was like a way to animate a lot of different things

........

(1) It was like an augmented reality technology system.
(2) 

........

(1) If you have the rectangle sliding around on the floor
(2) You can draw and re-draw an image of someone walking
(3) The person can walk around objects in the room
(4) The person can walk around on the floor and be hidden when going around objects

........















