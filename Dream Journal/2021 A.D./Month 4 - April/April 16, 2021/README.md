

# Dream Journal Entry - April 16, 2021

[Written @ 5:26]

[Copy-Pasted From Development Journal - Technological Ideas / ** / Minimum Description Reference]

[2.0]
The term 'laundry' was inspired from a dream that I had while taking a nap yesterday (in my time of when this post was written on April 16, 2021 @ ~5:07) which was April 15, 2021

The dream I had was of a college student whom I remembered from when I was in college. The person was in a video on YouTube or on a video sharing platform similar to YouTube (since maybe in the dream world it wasn't exactly YouTube but a proportional technology with possibly a different name but the similar usecase for sharing and watching videos).

The dream of the student showed the student in a video and they were describing what they enjoyed about going to college. One thing that stuck out to me really spikingly was when the student said 'they gave us laundry baskets' as though the school provided the students with laundry baskets when they first arrived on campus. That was amazing. I remember that the college that we went to in real life was a nice college but when I woke up from the dream I didn't remember that they gave us laundry baskets. Well a nice thing to do is give people laundry baskets to do their laundry in case no one really has those things. So in proportion to my real world experience of going to a nice college, the dream of 'receiving laundry baskets' from the school (supposedly in Freshman year) that was really a nice thing to see that the school was doing.

Maybe that spirit of providing people with laundry baskets for their lives is a really good one to have for people when they first wake up. It isn't too dissimilar from providing a parent or an allowance or some score for the person to know they are permitted and welcomed to do certain things like taking care of their bottoms and trying to clean their bottoms. Well it's not that big of a deal if you don't have a system like that because that could be a jungle survival system like in a video game where people really like that stuff.

Well, I'm not sure, it seems quite useful. The term laundry is quite useful. It is light hearted and general enough to describe the role of a loving parent. The role of a loving parent is to wash all of your laundry until you grow up.

Learn to brush your teeth.
Learn to comb your hair.
Learn to pick up after yourself.
Well. Those are big deals.
A laundry basket could be a robot or a basket.
You need to hold its hand[handle]

This is a poem about laundry. It's just for fun and it's not meant to say anything -_- delete




