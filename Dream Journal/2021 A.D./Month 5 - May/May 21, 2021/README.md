
# Dream Journal Entry - May 21, 2021



[Written @ 4:11 - 4:46]

Dream Title: `Time Doesn't Work`

Dream Description:

`A terrible bomb falling out occurred in our city.` I ran out of my house unprepared with my baby brother in my hands. My other younger brother and I were unprepared but we knew this day was coming. The 3 of us, myself, my baby brother and my younger brother who is 2 or 3 years younger than myself . . we ran out of the house . . only on the lucky chance that our neighbor knocked on the window and we happened to notice it at that time.

The bomb was being prepared for for months before that day it seemed. We knew they were going to come. There was a hideout prepared for when that day came.

Our house lived above the ground. People were required to go to a cave nearby that would shelter them.

Upon the chance of seeing my teenage young man neighbor's young face that looked neutral and painless and tinted the color blue as though this was a sad movie animation or video game animation, we didn't know what was up . . I . . personally . . didn't know what was up . . until behind him . . in the window . . from inside of our house . . Behind my neighbor . . I saw those bombs that flew down onto the city in the distance and treating each row of houses like cardboard mountains and flattening them into a smoke cloud that was brownish and less and less resembling houses and more and more resembling the silhouette of brown hills made of soft paper material.

The bombs flew down from airplanes that flew quickly. It seemed like they had plenty of ammunition to let each row of houses go unmissed and very quickly and all from the many miles away that these houses were.

`We had minutes. Possibly 1 minute. A single minute to get out of the house.`

It was weird. It was intense. Now the face of my neighbor which showed a neutral expression. The young person who didn't say anything but accidentally showed up or unexpectedly showed up.

How could we have forgotten this day was coming?

I yelled at my younger brother to get up and that we need to go. We knew our parents had already left many days ago . . They already told us to prepare for our chance to enter the cave.

We must have overslept this particular day and didn't have any prepared response besides the accident of our neighbor who was my age and looked very familiar.

I grabbed my baby brother.

The mountain nearby that had a cave opening on the side was less than 1 minute walk away.

We ran there. We sprinted.

. . 

Upon entering the cave . . which by the way is a very tall cave mouth opening . . we . . did . . in fact . . see no one was there . . 

Why was no military person there waiting for us? 

There was a gloomy feeling of expecting someone to be there but they weren't there . . 

Inside of this giant cave mouth we prepared to move further into another area . . a room up ahead many seconds run . . a many seconds run . . to this metal door that would lead to a new room . . 

I'm not sure what happened outside . . 

I don't remember hearing the bombs fall . . 

I don't remember hearing the chaos ensue . .

At this time . . we must have been safe from the bombs that were quickly elapsing over our local neighborhood and flying in the direction of our own house . . 

We must have avoided what could have been a sudden death . . 

A sudden death only a movie would show from 2 lazy young men who forgot to wake up and were serendipitously reminded to do so by a local neighbor boy who knocked on their house window . . 

What a strange state of affairs . . 

The movie of this panic-at-the-last-second type of dream . . became possibly what could be interpretted as a video game . . since the metal door that we were planning to reach . . became a 'spider' . . or 'giant spider' above the door and the physical door became like a 'hallucinogenic' 'portal' where the portal was spiraling with electric clouds of 'gray' and 'white' surrouding a black or dark oval area which was supposed to be the way into the portal . . 

The scene of the portal door opening really reminded me of a video game environment like 'World of Warcraft' since the 'giant' 'spider' reminded me of a sort of 'dungeon' or 'dungeon instance' that players can enter to see what was inside for exploring.

Upon nearing the door . . the spider also seemed to be impatient with our arrival . . The giant spider that hung from the top of the portal door . . seemed to also have a timer or something since we possibly could have been running out of time to enter before a certain date and time . . 

The spider looked to almost try to prevent us from entering the portal door . . All we had to do was step forward . . into the black mist . . and we would be on the other side . . of this environment . . we would be inside of this instance environment where it was possibly safe . . 

We tried once . . my brother and I . . and were rejected entry . . since the spider . . leaped forward with its body . . to cover the portal door . . and just in time . . it also later . . only a few milliseconds later . . leaped back up to its original stationary position above the door as it hung there from possibly a thin thread . . that wasn't very apparent . . but the orientation of the spider sort of suggested it wasn't standing on something solid . . 

In the time that was allotted for us to enter . . which was brief seconds between the spider's attack . . then . . my brother and I had enough time to enter the portal gate . .

`Inside of the portal`

There was a creature up ahead . . only a few feet away from us . . 

The creature was a giant head . . 

The giant head bounced . . 

The giant head . . the size of a person . . bounced . . 

. . 

The giant head . . bounced towards my brother and I . . like a creature mob from the game of 'World of Warcraft' would do if you came too close to it . . and if it was aggressive . . 

This 'aggressive mob' character . . bounced . . 

My brother was killed . . since we weren't high enough level

I think I was possibly killed as well . . 

My brother was killed before me . . and . . I was afraid that the baby brother in my arms . . was also going to be in danger . . 

Somehow . .

Possibly thanks to 'game mechanics' . . 

I was able to 'drop' my baby brother or put them on the ground . . before being attacked by the character mob nearby . . 

After I re-spawned . . or came back to life . . after also being sort-of-killed by the mob . . 

I really didn't see myself getting attacked . . but I ended up in the re-spawn location . . just like my other brother . . after I had seen him getting attacked . . 

And since the re-spawn location . . the location where we were transported after we died . . the place where we came back to life with new instances of our body . . 

We saw our baby brother nearby . . 

This baby brother couldn't walk and needed us to hold them . . 

We had to hold them around . . I guess that's called an 'infant' person . . or an 'infant' character . . The person . . couldn't walk on their own it seemed . . 

For some reason . . 

`There were 2 infants that appeared before us . . `

Was this a hack?

Was there a bug in the software?

Somehow . . 

One infant was luckily available for my brother to wield so possibly our infant brother didn't need to die . . But for some reason . . maybe my move to drop the baby . . or to place them on the ground . . before being attacked . . also back-fired since . . There was a second infant baby . . that appeared . . after my brother had already picked up or selected the baby from the ground . . nearby where we were killed . . 

What a strange dream . . 




