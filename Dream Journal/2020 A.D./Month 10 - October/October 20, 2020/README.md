


# Dream Journal Entry - October 20, 2020


[Written @ 16:10]

JWhaaly & SSteven Computer Design for A Mouse Interaction Scheme In an Alternative Reality from What I Remeber the Dream That I Had Today
![jwhaaly-and-ssteven.png](./Photographs/jwhaaly-and-ssteven.png)


The interaction . . . . uh . . when I think about this topic . . uh . . when I think about this topic . . now . . when I'm awake . . uh . . uh . . it's uh . . the idea relates to the mouse . . moving . . uh . . from one circle to another . . uh . . and so . . for example . . uh . . the example . . circle . . does something . . uh . . and uh . . well . . the example circle uh . . can do something . . uh . . maybe it can be customized by the user somehow . . in terms of that that action does . . and . . the . . undo . . circle . . the circle . . next to the label . . "Undo" . . uh . . maybe . . that is an item that . . undoes the previous action performed on the computer . . or on a computer . . uh . . and so for example . . this can be something like . . undoing . . the last words typed into the keybaord . . or undoing the last . . uh . . uh . . search query . . uh . . or uh . . undoing the last browser uh . . navigation or something like that . . I'm not really sure on the specific . . uh . . ability . . uh . . and uh . . well . . uh . . my intuition on what this topic was about . . is how the mouse . . can be used for more than . . uh . . uh . . well . . uh . . it's uh . . something related to switching . . uh . . and being able to switch between the abilities of the mouse gesture . . and the mouse movement gesture . . for example . . uh . . maybe . . uh . . well . . the mouse movement gesture . . uh . . like . . uh . . uh . . moving from one circle to another . . uh . . can be . . a way for the mouse to gesture an action being performed by the user of the computer . . 

Well . . uh . . In the dream . . I really wasn't given instructions on how this uh . . idea worked really . . uh . . I remember looking at a computer screen . . with this image on the screen . . uh . . well . . the uh . . details of the stylized nature of the . . names of the people who designed this system . . uh . . look different from how I've presented them here in this image . . uh . . and uh . . the uh . . desktop . . which is . . where this . . image was being displayed . . uh . . well . . it seemed like the desktop was more interactive . . and uh . . not only a list of files that you could drag and drop from one place to another . . or open windows uh . . or applications . . uh . . or open directories . . uh . . it seemed like . . there was this . . mouse feature . . that allowed you to . . navigate uh . . and undo your previous action . . uh . . and then you can also set the uh . . example action to be uh . . something of your choosing . . uh . . well . . also . . the item 1, item 2 and item 3 . . these were other uh . . items that you could select between . . well . . uh . . in terms of the actions performed . . uh . . I think for example . . when you hold down the . . command button . . maybe that triggers an event to lock the computer mouse to be able to move only between these items . . so . . if you hold the command button maybe your computer mouse can only then move between . . the 3 circles that are in a group . . and labelled . . "item 1" . . "item 2" . . and "item 3" . . . hmm . . right . . 

uh . . so uh . . one way of interpretting this dream . . uh . . and uh . . uh . . how this . . uh . . computer worked . . uh . . which . . I'm trying to apply my intuitive reasoning skills in trying to decipher what was being developed in what appeared to be this alternative reality where uh . . maybe uh . . well . . uh . . this person . . uh . . JWhaaly . . and . . SSteven . . something like that . . uh . . uh . . well uh . . in this alternative reality it seems like these were the designers responsible for this development and they had their names on the desktop to showcase the technology . . or something like this . . and I was a user of the computer . . who was experiencing . . uh . . something like . . uh . . uh . . I was like a fan . . and I was really . . uh . . inspired by these people uh . . and I had a computer uh . . just like a person can have a macbook computer today . . or a windows computer today . . and uh . . uh . . well . . I was looking at this demo . . of uh . . this invention that these people had made . . but there was no one there giving me a tutorial on how it works . . and so . . uh . . I'm uh . . really uh . . maybe trying to reason about it . . and uh . . maybe uh . . reason about how it works . . uh . . which uh may or may not really be specifically how it worked uh . . I don't really remember seeing how it worked in action uh . . it was uh . . uh . . really uh . . for example . . maybe seeing . . an example where . . the mouse is moved in relation to these uh . . items . . uh . . well . . uh . . uh . . well . . uh . . I tried to reverse engineer how I think it could work in the dream . . uh . . and uh . . well . . that's inspiring me to write more about this reverse engineered possibility of how this item could work . . 

well . . uh . . one way that this could work . . in my current interpretation is . . for example . . let's say . . the user presses and holds the . . keyboard keys . . command + h . . and then . . those uh . . well . . it can be any keyboard key combination you would like . . those are only examples . . maybe . . uh . . command + m . . or command + n . . are better uh . . but you can choose any keyboard key combination you would like . . or maybe . . control + m . . or control + n . . if you're using a windows computer . . uh . . well . . if those keys available . . then you can press those keys . . and then that locks your computer mouse into only being able to select between . . a space of options . . like the space presented here . . and so for example . . uh . . the space of items that contains . . "item 1" . . "item 2" . . "item 3" . . those are like uh . . maybe uh . . well . . you can click on those items . . uh . . and maybe navigatint he space is not really uh . . like navigating like moving uh the mouse to uh . . one space to get a proportional uh . . related space distance . . for example . . uh . . well something like that . . 

uh .  well . . so . . maybe in moving through this space . . the computer mouse . . when you move it . . it snaps into place . . at various distances as you move and so uh . . its like . . a snap-in-place space map grid where you aren't always having to scroll along the edge of the computer screen . . with the mouse position . . instead if you scroll a little bit to the right . . that snaps into place the mouse position to be at item 2 . . and if you scroll a little bit down . . uh . . that snaps into place the mouse position . . to be at item 3 . . and so . . uh . . this snapping action can be a way of "teleporting" . . uh . . in space for the mouse . . and teleporting at certain actions or something like that . . 

Uh . . well . . uh . . imagine that its maybe like . . uh . . well it reminds me of this . . move the mouse to the corner of the screen to show the desktop feature on the macbook pro . . which is available in the settings . . . . uh . . the feature is called "Hot Corners" . . which is a really cool feature . . in my perspective . . on what is really cool about macbook computers . . the hot corner feature . . 

uh . . well . . uh . . hmm . . it seems like . . uh . . maybe this feature . . uh . . uh . . for uh . . snapping the mouse into place . . for performing actions . . uh . . in these different . . uh . . "mouse action spaces" . . uh . . so the . . "item 1 . . item 2 . . item 3" . . is maybe called . . "item space 2" . . and the . . "example and undo" . . is called "item space 1" . . and . . when you press . . command + h . . you can navigate the mouse in the item space 2 . . which can then uh . . for example . . lock the mouse actions to be uh . . either . . uh . . clicking on item 1 . . item 2 or item 3 . . or something like that . . uh . . and so this can be a hot-key way of uh . . selecting items of interest very quickly . . in a mouse space that is separate from the position location space . . that is normally the default like . . navigating the mouse position location across the screen . . uh . . is uh . . maybe a gesture uh . . that can take a while if you're looking for uh . . navigating to a certain directory . . uh . . or performing a certain uh . . series of actions rapidly in a sequence . . and so . . uh . . uh . . whether that's navigating to different windows rapidly in a sequence . . or maybe playing a video game where you are trying to perform different actions very rapidly in a sequence . . uh . . and also maybe you need to be able to trade the outcomes very rapidly . . and so having multiple item spaces . . or mouse items spaces . . uh . . and not only . . one . . or two . . or three or four . . having multiple item spaces . . each with their own variend number of . . possible points . . or joints . . or positions . . to be navigated to . . and so . . being able to select all in these various spaces by . . pressing a key . . and getting computer screen . . or computer action space navigatability options . . available to someone very rapidly . . those are things to consider . . 

uh . . something like that . . uh . . having multiple . . vector spaces for the mouse uh . . seems like a way of describing this topic . . uh . . vector spaces are uh . . like . . uh . . directional spaces . . or uh . . maybe gestural . . positional spaces where your gestures of the mouse can relate to certain . . manuevers . . or relate to certain . . ways for the mouse to move . . along the screen of the computer . . and so that can be a way to have actions of varied types be easily accessible very rapidly in succession . . uh . . something like that . . 

hmm . . well . . I'm having the program idea imagined uh . . but . . uh . . I'll maybe have to think about demonstrating this idea at another time well . . uh . . also this dream journal entry is here to inspire anyone else who is interested in pursuing this topic further . . and so you are all welcome . . or anyone is welcome . . uh . . you are all welcome . . to consider applying the things that are discussed . . uh . . I hope this information exchange is useful and uh . . always feel free to give me feedback on if these uh . . messages uh . . are coming through uh . . in uh . . in a way that is appropriate uh . . I suppose . . uh . . well . . I'm trying to be appropriate uh . . in some respects I suppose well . . uh . . in any case . . you are also able to determine that in the case of your own to share your thoughts and feelings on the topic . . uh . . well . . uh . . Thank you for reading this message . . I know it was quite long . . uh . . something . . thank you very much . 



[Written @ 16:04]


Word To Photo Translation Result Possibility 1
![word-to-photo-translation-result-possibility-1](./Photographs/word-to-photo-translation-result-possibility-1.png)


[Written @ 15:09]

uh . . well . . uh . . hmm . . I'll make this one the last one for today . . and then another time I can cover the other dreams that I had today while sleeping during today's live stream . . uh . . or something like that . . uh . . while sleeping during today's live stream . . 

. . . 
[physical paper material notebook notes that I have written down]:

October 20, 2020
12:31 - 12:37

* I watched a cartoon animation. it seemed fan made




                          drums
       wall                                   wall
           person          stage         person
            guard                        guard


    person                               person
    guard           foreground          guard standing
    walking                             and looking  
    to the                              to the left
    left





-
-

- Convert this photograph of word text description to produce a picture photograph of a three-dimensional scene

- Straws and Rubber Drawing
- Words to Imagination
- Words to Picture
- Words to Picture Drawing
- Words to Video Drawing
- Convert a story in a book to a movie story with pictures and sounds

. . . 

[explanation of the notes]:

In the dream I had . . uh . . uh . . well . . I was watching a cartoon animation . . uh . . well . . it was really . . quite good . . and there were characters . . and movements . . uh . . the characters were moving . . there was a camera perspective . . and there was an environmental scene . . uh . . like a castle in the background . . and there were guards standing near the gates of the castle entrance doorway . . uh . . and uh . . there was . . uh . . uh . . movement in terms of the characters moving the uh . . environmental assets or something like that . . uh for example . . uh . . the scene uh . . with the uh . . drumset appearing on a stage . . was something that was uh . . sort of surprising since I was expecting to find drumsets uh . . with cymbols [or hats] uh . . and drums . . in a scene with castles and guardsmen . . or guardspeople who were dressed in a sort of . . uh . . medieval uh . . or maybe a uh . . super roman attire where they appeared to be . . uh . . Roman guards or something . . with a professional look . . uh . . uh . . but maybe it was like a fantasy style of Roman . . uh . . where it appears in video games or something and the helmets are uh . . golden . . uh . . and . . certain elements are romanticisized to appear uh . . really uh . . fancy or something like that . . uh . . uh . . something like that . . 

Well . . uh . . 

. . . uh . . well . . hmm . . . . uh . . well . . the dream was uh . . quite a uh . . well . . I liked it . . in terms of uh . . personal taste uh . . in terms of uh . . uh . . uh . . uh . . uh . . something like that . . well . . uh . . hmm . . hmm . . I uh . . lost my train of thought one moment . . right hmm . . I was saying something about liking the dream uh . . hmm . . I liked the dream uh . . right . . it was like a movie experience of watching a cartoon animation . . uh . .

uh . . hmm . . well . . in waking up . . I started to think about the topic of uh . . uh . . well . . uh . . one of the messages uh . . uh . . well . . the cartoon animationwa was uh . . crisp . . uh . . or sort of crisp in a sense . . as though . . well . . uh . . maybe it was made by someone using uh . . their hand and their imagination . . uh . . and so someone drew each of the items in the scene . . and animated each of the items . . well . . 

another possibility could be . . how this could be achieved . . uh . . for someone who wants to make this scene . . uh . . with the moving characters . . uh . . and the uh . . diversity of the scenes that are possible . . well . . how could someone go about achieving a set like this . . a movie experience like the one that uh . . I had experienced with cartoons ? . . uh . . right well I would also uh . . I think the cartoon animation style was . . uh . . hmm . . there are cartoons like . . Spongebob . . and the fairly odd parents . . and these have their own uh . . artistic style . . uh . . and uh . . I suppose uh . . cartoon animation style as well . . well . . uh . . the style for the cartoon in my dream . . was uh . . well the proportions of the people . . were uh . . uh . . like the people that uh . . I think they uh . . it was reflective of the scale uh . . and so exaggerated features like in . . cartoons where the characters have really large noses or really large uh . . proportions of body characteristics . . uh . . like uh . . in total drama island . . uh . . or uh . . spongebob . . where squidward has a really large nose . . uh . . or . . hmm . . in the fairly odd parents where . . the fairies have uh . . the shape of their heads are uh . . maybe not really realistic . . uh . . for the relation to a human in the waking world that uh . . I'm familiar with . . or uh . .t hat maybe uh . . you uh . . as the reader uh . . I would presume are maybe familiar with . . or something like this . . the reader uh . . uh . . or . . uh . . . perceiver of this journal notebook . . or something like this . . 


uh . . the style for the cartoon animation from this dream was . . more realistic . . but . . uh . . it was uh . . the colors were uh . . more towards uh . . filling the shapes uh . . the shapes were proportional . . with respect to uh . . real life perspective or something like that . . 

. . . something uh . . something . . uh . . where the proportions are uh . . uh . . from my perspective uh . . that's uh . . something . . uh . . uh . . where the proportions of people are matching in a closer way that exaggerating uh . . the scale with respect to other people and the environment uh . . something like that . . 

uh . . 

well . . uh . . right . . so . . in a way . . what if you could write the words of the items in the scene on a uh . . sheet of paper in uh . . a sort of . . 3D perspective grid imagination structure that you can have in your mind uh . . and then you can translate that uh . . to a . . image . . uh . . in a scene . . uh . . or what if you could translate that to a movie . . uh . . and so each of the pages . . that you write can represent the uh . . movie script uh . . uh . . well . . uh . . I think this is a uh . . well . . uh . . maybe today this is called . . "word to vector" . . or "word to vec" . . or "word to picture" . . uh . . there is research being done in this area in the deep learning community and uh . . well . . uh . . that's uh . . an interesting idea . . to be able to combine . . uh . . all sorts of ideas . . uh . . and have a computer create uh . . an element of a scene that resembles what is being described by uh . . the words of text . . uh . . and so uh . . possibly . . you could imagine writing a book . . and having the computer translate that to be a movie . . and uh . . you uh . . can specify how each of the characters looks in a way uh . . that satisfies uh . . your thoughts or something like that . . uh . . well . . uh . . hmm . . 


https://hackerstreak.com/pix2pix-gan/

https://github.com/matlab-deep-learning/pix2pix

https://deepai.org/machine-learning-model/pix2pix

https://affinelayer.com/pixsrv/




[Written @ 15:06]

Uh . . well . . uh . . like I said earlier . . uh . . I would like to uh . . move on to the source code for the ecoin project at some time uh . . but uh . . well . . uh . . let's see . . uh . . I'm noticing that sometimes I will cover uh . . topics that I didn't intend on talking about or something like that . . uh . . or uh . . something like . . uh . . I'll take longer that I would prefer to take uh . . when writing about some of these topics . . uh like my dreams or something like that . . uh . . uh . . well . . uh I don't mind uh sharing this information and writing and things like that . . well . . uh . . hmm . . well . . I suppose the feeling of urgency to get to the ecoin project at some time today would also be the causing factor or motivating factor xD mmo . . factor . . that is influencing me . . to uh . . inspired to uh . . uh . . well . . uh . . not write so much in my dream journal or something like that . . 

in any case . . uh . . let's see . . uh . . 

[Written @ 15:04]

uh . . I think those are enough names that I could find so far . . uh . . those are only a few names that I could find while uh . . searching uh . . or navigating . . or flipping through . . uh . . my journal notebook . . uh . . uh . . my physical paper material journal notebook . . uh . . where uh . . uh . . on uh . . random pages of the notebook . . I will uh . . have written . . uh . . names here and there . . uh . . in random uh . . uh . . parts of the page . . uh . . as the names come to me throughout the day uh . . uh . . or uh . . if they come to me at all that day . . uh . . I'll uh . . be inspired to write the name down . . uh . . something like that . .

[time taken to write the names: approximately . . 46 minutes and 50 seconds . . ]


[Written @ 14:46]

Notes @ Newly Created Words

Lupe Dane "Pronounced "Loop Dayne""
[I had originally wrote this word in my physical paper material notebook journal on . . October 3, 2020 . . which is what the paper says on my notebook . . uh . . ]
[this comment was written on . . October 20, 2020 @ approximately . . 14:50]
. . . 
[This word came to me randomly one day . . this word phrase . . "Lupe Dane" . . uh . . it means . . "A cutest mistake" . . so when someone makes . . a very cute mistake . . uh . . well . . I think . . that means . . uh . . well . . it can mean . . they made . . a . . "Lupe Dane" . . uh . . and then you can show a peace sign next to the mouth and do a kissy face . . when you say . . "Lupe Dane" . . which  is a cute way of acknowledging that someone made a cute mistake . . uh . . or it may uh . . appear like a mistake but uh . . maybe uh . . uh . . well . . uh . . maybe uh . . it's uh . . something else . . well . . it's cute in any case . . "Lupe Dane" . . [peace sign emoji with kissy face next to peace-sign hands]]


[Written @ 14:15]

. . . well . . I wrote a lot of other notes . . while I was . . "sleeping" . . uh . . during the live stream . . I say . . "sleeping" . . in quotation marks . . uh . . since maybe for . . uh . . 1 or 2 or 3 hours . . I was . . uh . . uh . . laying with my eyes . . closed . . laying on the coach . . where I normally sleep . . and . . uh . . my memory was uh . . phasing in and out of uh . . a dream state . . uh . . and so . . I was . . uh . . sort of . . uh . . hallucinating . . or visualizing . . uh . . other realities or something like that . . uh . . where my memory was uh . . being engaged in other realities . . or other . . uh . . possible consciousness perceptive realms of thinking . . uh . . and then . . uh . . well . . I would also return to my senses by being engaged in . . uh . . the reality that I know when uh . . for example . . I pick up my pencil . . and start writing in my physical paper material notebook where I record the random thoughts that I had . . which seem quite . . uh . . diverse in their distance from what I'm used to . . in my waking reality world . . uh . . or something like this . . 

. . . . . 

Well . . uh . . a few things I wrote down . . are really . . random thoughts . . that came up . . uh . . 

Well . . then . . I uh . . later went to sleep . . uh . . where I felt like I was actually . . uh . . completely uh . . not really uh . . focused on my physical body . . uh . . my consciousness was unfocused from the living room where my physical waking body resides from where I am experiencing writing in my journal now as I type these words and phrases and paragraphs of text information . . or something like that . . well . . uh . . uh . . well . . now I'm sort of struggling between not being able to decide which part to start from . . uh . . whether I should start by writing the random things that I wrote down . . and record them here . . uh . . well . . some of the things are really uh . . random . . which relates mostly to for example . . random words that came to mind . . while I was phasing in and out of the waking world . . and so . . random words would come to my mind and I would get an inspiration . . or I was inspired to write them down because I'm interested in learning more about things and seeing what my uh . . imaginative and creative abilities are uh . . even those abilities for example that I'm not really sure that I have . . and I want to see what is able to come out . . and so for example . . random words that come to my mind . . words that I haven't heard before . . uh . . but seem like plausible ways of communicating information . . uh . . well . . those words will come to mind . . and those are useful ways for uh . . communicating information if uh . . for example if I ever need to use a word that is really not defined or a thought that really doesn't have a strong hold on the abilities of people to think about that type of concept then maybe a new word would help to anchor those concepts into the minds of people that are able to be communicated with or something like that . . uh . . people that uh . . share the common understanding through word language patterns and so that can be a useful way to communicate information that could otherwise uh . . need further dialogue . . with relation to more words of text uh to tell uh . . a story maybe or something like that . . uh . . uh . . well uh . . i suppose maybe for example . . in the topic of telling stories . . uh . . to communicate a message . . uh . . maybe uh . . that's why titles for movies are valuable . . uh . . to compress . . the information of the text uh . . to a relation of the uh . . title . . into a few words . . uh . . and so . . uh . . titles for books . . a small sequence of a sequence of a few characters can be used to comprise the text or summary of information relating to the topic of interest in a book or in a movie . . and so uh . . maybe also the name of a person for example . . can also be responsible for relating to the information of the topic of an individual's life . . uh . . the name uh . . Robert . . uh . . or the name . . Juju . . or the name . . Jinx . . there can be so many names can be have an uh . . story related conotation uh . . uh . . well . . uh . . in terms of names . . random uh . . names have come to my mind before . . where uh . . I keep uh . . well . . I write those names down . . and uh . . well . . I imagine the names are for people . . because uh . . they seem like uh . . a way to address someone . . uh . . well . . let me see . . uh . . hmm . . I suppose uh . . I could look for some of those names in my physical material paper notebook here . . uh . . well . . uh . . hmm . . one moment . . 

[Some of the names might be uh . . familiar to people uh . . but maybe some of them are new . . I'm really interested in all those uh . . types of names . . uh . . there are so many beautiful ways to sound a message to address someone . . and a name seems like a way to sound uh . . uh . . a message to address someone . . uh . . something like that . . ]



. . . 

McFett
McFet
[this name came to me randomly one day . . I think it came to me . . in the last . . 2 weeks . . uh . . maybe near . . October 3, 2020 . . uh . . another page . . the page adjacent to the page that this name is written on . . shows the date . . "October 3, 2020" . . ]


Nurivictoria
[this name came to me randomly one day . . uh . . I think it could have been on . . October 3, 2020 . . which is what is said on the top of the page . . that this word is written on . . ]

Nuriyata
[this name came to me randomly one day . . uh . . I think it could have been on . . October 3, 2020 . . which is what is said on the top of the page . . that this word is written on . . ]


Sirda King
[This name came to me one day . . it seems like it could have been on October 3, 2020 . . which is what ths paper notebook says for the page that this word is written on . . ]

Brittaney Clarks
[This name came to me one day a theoretical possible name . . for uh . . Isabel Paige . . uh . . I thought I had . . uh . . uh . . well . . uh . . I think sometimes uh . . when I uh . . think about someone . . well . . uh . . well . . in this case with Isabel Paige . . I had . . uh . . the name . . "Brittaney Clarks" came to mind one day . . at random . . uh . . and then that uh . . name thought was also associated with the feeling of . . . . "well this could be a name for a counterpart person for Isabel Paige . . or uh . . well . . maybe it could be an alternative reality name for Isabel Paige . . also . . " . . uh . . well . . uh . . uh . . I really don' tknow a lot about . . probable realities . . or alternative realities . . uh . . and if these people could actually exist . . in other worlds . . uh . . uh . . uh . . well . . uh . . in my present understanding . . this topic seems to relate a lot to the topic of . . personalities . . where . . for example . . the personality of a person . . is something that . . is able to uh . . be . . uh . . embedded in a lot of different physical personal bodied people . . uh . . and so for example . . we can have uh . . a lot of different people who have a body of their own . . and they appear different from one another . . and yet . . uh . . uh . . well . . they could maybe share . . personality characteristics . . uh . . relating to the things they are interested in . . uh . . whether it's math puzzles . . or dancing . . or art . . or . . uh . . jogging or running . . ]


Pera Gatro
[This name is written in my physical material paper notebook . . uh . . I think it came to me as a random name . . but I'm not sure . . uh . . normally when I write names . . uh . . I don't really leave an explanation . . uh . . for how the name came to me . . uh . . it's only the name that I write and not really a source of inspiration . . uh . . well . . uh . . now I'm applying my memory construct to try to imagine how this name came to me . . uh . . well . . uh . . well . . I think a lot of the names uh . . come to me by . . intuitive spontaneity . . where uh . . the sound of the name comes to mind . . and uh . . I feel like it's a cool name uh . . or that it's interesting to address someone like that . . uh . . for example . . what if I uh . . well . . it seems like . . it could be a way to address someone like . . uh . . Pedro . . or Wallace . . or . . uh . . Cornmeal . . these could be names for people that exist today in the world . . uh . . Christopher . . Alexander . . Alex . . Steven . . Walter . . Elizabeth . . Ally . . Constantine . . well . . these are so many and such diverse names . . uh . . each with their own unique sound . . and so . . well . . when a new name comes to mind . . I feel like . . it is also . . something that can be uh . . respected or taken seriously even uh . . for example if it's really only theoretical uh . . if it's only a probable facing cause for reason . . uh . . well . . maybe the name . . "Elli" . . or the name . . "Elizabeth" . . or "Natalie" . . maybe these names were once . . only theoretical constructs and people hadn't really known people by those names . . uh . . well . . they are quite uh . . beautiful uh . . and wonderful names . . uh . . from my personal . . uh . . perspective at this time . . and so . . well . . it seems like . . uh . . even if they were only theoretical at once . . uh . . well . . their application is quite useful when you think of someone named . . "Elizabeth" . . or when you think of someone named . . "Smith" . . or . . "Max" . . or . . "Jackson" . . or . . "Henry" . . "Natalie" . . "Rose" . . "Roberta" . . "Blake" . . "Caleb" . . and so on and so forth . . uh . . "Xin" . . "Shintaru" . . "Shintaro" . . "Kimi" . . "Kimiwasu" . . uh . . "Victor" . . "Vlad" . . "Vladmir" . . "Bourladeaux" . . uh . . uh . . well . . "Borladeaux" . . I made up just now . . uh . . well . . uh . . it seems like a beautiful possible name for someone . . haha . . ]



Scott Alan
[this name came to me randomly one day . . in the last few days . . maybe the last week]

Thesia
[this name came to me randomly . . in a half-waking half-dreaming state]

Ralph Parker
[this is the name of an African American R&B Singer that I perceived in one of my dreams . . uh . . while I was sleeping . . uh . . this person appeared in one of the dreams that I had . . like they were a real person . . in a probable world uh . . or in a real world to them but it seems like uh . . only a possibility to me . . uh . . it uh . . was a world like . . uh . . where the Earth seems like today . . but uh . . the name was of a person . . uh . . in this dream world . . uh . . in this dream planet . . uh . . I haven't really done research to see . . if this person actually exists on the physical world uh . . that I'm familiar with . . uh . . but that could be a possibility as well . . ]


Sucu
Suku
Sookoo
Shayt

[these are words that I wrote in my physical material paper notebook but I'm not really sure if they were intended to be . . names . . I don't remember at this time . . but uh . . in the way they look to me . . uh . . these could possibly also be names . . ]





. . . 
[a page of notes from my physical paper material notebook]:

Breative - Perspective

Lava forefront

Sucu
Suku
Sookoo
Shayt

You can say you did something, with Infraility

Thank Your for launching Aigen Space

Upbragile Upbragile

Upgradile

. . . 


















[Written @ 14:08]

[time to take the notes . . approximately . . 6 minutes and 12 seconds]


[here are notes that I took in my physical paper material journal]:

* Dogs that look like blueberry muffins
* Cats that look like apples
* Birds that look like brownie squares

. . . 

[explanation of the notes]:

uh . . well . . the first line . . "Dogs that look like blueberry muffins" . . is a popular . . image set that is shared in the deep learning community . . when uh . . I've learned about artificial intelligence through the deep learning community . . uh . . and the final 2 lines . . relating to cats and birds . . are imagined to have . . uh . . well . . I wanted to have examples . . or . . I wanted to think of examples . . of relating topics . . uh . . in terms of . . uh . . what other things that could be part of reality . . uh . . that . . are uh . . maybe . . theoretical . . uh . . and I haven't really seen examples . . of . . birds that look like brownie squares for example . . and uh . . well . . I really need to jog my memory if I want to think of uh . . cats . . that look like apples . . uh . . well . . these are theoretically possible when I think about it . . and I can create an image in my mind of these items if I wanted them to exist . . at least in my mind . . in doing this exercise . . I wanted to see what is possible with my imagination . . and also . . a way of measuring the creativity or the creative capacity of computer programs relating to artificial intelligence . . and so . . if for example . . a computer can return the results of an image . . of . . any of these items . . maybe it seems possible that . . a computer program is as creative as human beings . . uh . . and uh . . creativity can perhaps be the measure of the "intelligence" . . of a computer . . or something like that . . 

. . . 

Dogs that Look like Muffins:

[dogs-that-look-like-muffins](./Photographs/dogs-that-look-like-muffins.png)


[Written @ 7:22]

. . . 

The notes that I recorded . . 

uh . . those uh . . names that were written . . were an attempt . . or . . a way of seeing if I could write the names of the people that I read from the dream . . uh . . 

JWhaaly . . seems like the name of the first person . . if I'm spelling that correctly . . uh . . the text was in a sort of . . 1900s . . stylized text era uh . . aesthetic . . uh . . sort of like in the early computer days with a retro theme . . uh . . the theme . . was uh . . stylized text . . uh . . well . . uh . . maybe all text is sstylized to the particular design language of interest . . uh . . but . . uh . . if I remember correctly . . uh . . the theme reminded me of . . a retro style . . but it wasn't really exactly something that . . uh . . I remember from the reality that I'm from . . it could have been a slight variation on the retro theme . . and so it was like . . an alterative style of retro that could have been something that appeared in our history of retro uh but maybe it was skipped over because the people weren't there to create that style of retro or maybe no one was interested in creating that style of retro and so in my waking consciousness that art work or theme or style isn't so apparent as maybe it could have been if those people were there to express that interest or something like that . . 

Retro theme text example
[retro-theme-text](./Photographs/retro-theme-text.jpg)

. . . 

uh . . while sleeping . . I was listening . . to the video here . . [1] on repeat . . uh . . for the . . many hours . . that I was asleep . . it seems that I could have slept for . . more than . . 3 or 4 hours . .

[1]
Daily Routines Of My Life - It’s Getting Cold... (Story 52)
By Isabel Paige
https://www.youtube.com/watch?v=J2w0LNywLYM

. . . 

In general . . while I'm sleeping . . I like to have music or . . information uh . . sound information . . uh . . through my headphones as I sleep . . uh . . which . . uh . . I've uh . . often thought that this sound information gets through to my dream . . uh . . my dream self or my dream experience in some way . . sometimes its really apparent where it seems like the dream that I had . . was in synchrony with . . the sound or the music that I was listening to . . and so for example . . I'll be listening to an audiobook . . while sleeping . . and . . the audiobook will . . uh . . say things . . the person reading the audiobook will be reading the book . . reading the story . . uh . . sharing the thoughts uh . . based on the topic of the book or something like that . . uh . . well . . uh . . the audiobooks that I normally listen to uh . . 

Tim Hart Hart
https://www.youtube.com/user/Timhart1144/playlists


And so . . uh . . when . . the book uh . . is being read . . or uh . . Tim is reading . . or uh . . the message is being shared . . or something like that . . I wake up from the dream . . and the message synchronizes with what I experienced from the dream . . for example . . synchrony can mean that . . it seems to someone psychologically relate to a thing that I remember from a dream . . uh . . and uh . . so for example . . uh . . maybe the psychological relation is something like . . uh . . I learn something new . . from the dream . . uh . . about uh . . my personal experience . . uh . . as uh . . a living person . . like . . something about . . my siblings was said . . uh . . my 3 brothers . . or something like that . . uh . . and that reminded me . . well . . oh . . I've never thought of it like that . . uh . . that's quite new information for me . . 

Uh . . also in general I like to uh . . think that .  hmm . . well . . I haven't really written this thought down before and so maybe it's uh . . not really something I can think that I'll uh say properly in the first way of saying it or something like that but the idea is that maybe uh in the morning I really am more interested in waking up with learning something new . . uh . . something like that . . 

Another way of saying this is maybe to say . . uh . . When I wake up in the morning . . I have the idea in my mind that I would really like to learn something new before waking up uh . . and so . . uh . . from my dream . . I can learn something new . . or . . I'll stay in bed . . or uh . . stay lying down . . or lying around . . uh . . lying around in my blanket . uh . . or lying around in a fetal position or uh . . with my eyes closed or however it is that it appears that I'm sleeping or laying down . . uh . . I'll lay there until a new thought of something new uh . . seems to come to me . . uh . . so for example this can come through . . something like thinking about things . . or thinking about my experience . . uh . . or uh . . about the future or uh . . about the past and what I think about life or what I would like to do . . or even my feeling of the present moment or something like that . . where I feel something about how I feel and acknowledge my feelings of uncertainty or my feeling of confidence or my feeling of uh . . whatever I happen to feel . . if I'm sad . . I'll uh . . well it really depends . . uh . . whatever uh I am . . uh . . feeling that day . . uh . . I'll uh . . well . . I haven't really thought about this too much uh . . but it seems to be the case . . that if I observe my waking up process . . uh . . or my ability of waking up . . how it appears is that it somehow relates to being interested in having learned something new before waking up and doing something for the day . . and so . . if for example . . uh . . I wake up . . and I wake up from a dream . . uh . . for example maybe uh . . I'll write the dream in my journal notebook . . uh . . if it seems uh that I'm interested in that information or something like that . . uh . . some dreams are really quite uh . . I'm not really sure about them . . they are something I need more time to think about or that I really judge a lot by what they look like from their appearance in uh . . well . . the information is new somehow but maybe there is a suggestion of something that I really am not interested in looking at at the moment that I'm waking up . . uh . . for example . . recently . . I had a dream about . . "ugly" people . . or . . uh . . people who uh . . in my immediate impression . . don't appear to be very attractive to look at . . uh . . and this dream . . uh . . well . . uh . . I didn't write it down . . uh . . whereas with a dream about . . technology or something like that . . if it seems technically related like I can apply it to uh . . algorithms . . or thinking techniques for how to think about computers . . or something like this . . uh . . or how to apply it to artificial intelligence . . or how to apply the dream to video games . . or information technology or robotics or things like this . . then normally I am very excited to write the dream . . uh . . 

well . . uh . . uh . . well uh . . the dream about "ugly" people seems to uh . . well . . there uh . . welll . . uh . . uh . . the beauty of ugly people is a topic uh . . and uh . . uh . . the appearance of uh . . the unconventional appearance of people uh . . that is a topic . . uh . . which is suggested by the dream . . but because of maybe uh . . my mental bias . . uh . . or uh . . my mental proclivities . . 

uh . . it seems that . . uh . . even though maybe I am not immediately comfortable with thinking about these topics . . uh . . that maybe it uh . . really uh . . is uh . . well . . if the dream appears to me uh . . uh . . well . . there is an interest in this topic in some realm of thinking in some respect uh . . and also new thoughts uh . . however uncomfrtable they are can also be uh . . information sources that are taken into consideration . . or something like that . . uh . . there are other dreams that I've had for the past few nights that I don't really write them down . . or have the inclination to write them down . . even though there are numerous of these dreams . . uh . . having to do . . with uh . . family relations uh . . between people from other worlds . . uh . . or something like that . . where I uh . . am uh . . maybe communicating with people but they uh . . hmm . . I don't really remember a specific dream that comes to mind in this respect but it seems like . . some people show up here and there and I don't really uh . . know why I had that dream . . it seems to be unrelated to my life at that moment . . uh . . maybe it was like a journey to somewhere . . uh . . and this place is somewhere that I forgot that I had gone to uh . . or maybe it's a hypothetical place where I am trying to imagine in my mind where is the possible place that can be a source of new understanding for my life experience . . to experience and circumstance that has a new aura to it . . and how could it relate to this or that style of thinking that I'm having in terms of human relations or something like that . . 

. . uh . . 2 dreams are coming to mind at the moment . . uh . . (1) I dreamt of a person with white hair . . who had a visual impairment with their eyes . . and appeared sort of disabled in the eye region . . and so maybe they were blind or something like this . . and I was visiting them in their house . . I was looking for someone at that house . . and the person responded that . . "oh . . [name of person here] . . and . . [name of second person here] " . . and uh . . well I got the impression that the person no longer lived there or something like that . . 

uh . . (2) . . I had a dream uh . . where 2 people were in a room together . . and I was a friend of theirs in some way . . uh . . but . . these 2 people were . . uh . . being romantic towards one another . . one person (a lady person) was sitting on the lap of the man person who was sitting down . . and uh . . well . . my understanding . . in the dream . . was that these people . . although they were being romantic towards one another . . uh . . well . . these people were also . . uh . . familialy related to one another . . these two people were cousins or something like that . . uh . . well . . uh . . they uh . . I don't know if they kissed or locked lips or something like that . . well . . uh . . this is the first time I'm writing about this dream . . uh . . and the . . white haired person dream as well . . uh . . 

well . . either of these dreams is quite strange for me and I'm not really sure what they have to do with my life experience at this time . . uh . . but uh . . I can also form hypotheses around the meaning of these dreams and uh . . think about the possibilities . . uh . . well . . uh . . I have hypotheses uh . . that uh . . for example for (1) . . the dream with the person with white hair . . uh . . who was a teenager in their appearance . . it seems that . . uh . . maybe I was visiting someone . . new . . uh . . or something like that . . uh . . I was visiting a friend from a past life or something like that . . or they wanted to communicate with me some sort of information about their familiarity . . uh . . I'm not sure uh . . where those memories are if we were ever friends uh . . I'm not really sure what to think about them uh . . it seems like they could uh . . uh . . have been a friendly person or something like that . . uh . . they were uh . . holding two guns . . uh . . one was like a rifle . . and the other was like a pistol . . uh . . well . . these were toy guns . . like they were uh . . playing pretend . . uh . . because they were imagining they were playing a game like related to guns . . or a game related to gun action relations and people being involved or something like that . . 

uh . . well . . uh . . uh . . in the respect towards guns . . maybe I'm not the mostly friendly creature to talk with relations of guns and people and so uh . . this aspect of their personality could also be a way to say that I'm not highly respectful towards this active ability to conceive of taking people's lives or something like that uh . . but that is a personal perhaps takes that is maybe not as well informed about the importance of these topics of human relations to guns and so maybe I need to be more educated to learn more about these importance characteristics of human weapons . . or something like that . . uh . . I'm not sure . . that's one way to say that I'm not a fan of guns . . 

uh . . well . . the person uh . . was a friendly teenage person . . uh . . and I was standing inside of their house . . I asked them . . if anyone else was home with them . . if they were home with their parents for example . . which was my intution to think that they lived with their parents or caretakers of this type of familial relations . . uh . . well . . they gave me the impression that it wasn't the case . . uh . . well actually now that I'm writing more about this topic . . uh . . well I'm not really sure . . they could have also said that there was someone home . . like their parents . . in the case that their parents were home . . I didn't really see them . . and uh . . well . . I didn't see them . . at all in the dream . . and so . . well . . uh . . the room was dark . . we were standing in the living room . . and it was though I was standing inside of their house . . uh which is strange to me when I think about it when I'm awake . . uh . . normally in real life or the life that I'm familiar with in my waking life . . when you meet someone new for the first time maybe it isn't always the case to stand inside of their house . . to ask them a question about someone that you're looking for . . uh . . and so . . uh . . maybe it's uh . . a culture shock for me to experience this way of standing in the living room which is darkly lit . . uh . . and standing there talking to this person . . that I've never met about uh . . people that I'm looking for at the house . . and uh . . it wasn't really clear to me in the dream uh . . that uh . . what the knocking on the door process looked like . . it uh . . seemed when I had this dream . . it was like I was uh . . uh . . entering this dream from the scene where I was already in the house . . and . . uh . . making my appearance standing next to the living room coach . . and uh . . asking . . if there was uh . . a group of people that lived here . . uh . . I used their names . . uh . . we can call them . . "John and Jacob" . . uh . . but those aren't the real names that I used . . uh . . for the sake of uh . . privacy or something like that . . since these people are also familiar to me in real life uh . . their names resonate with people that I knew in childhood from when I was in my early uh . . teenage years . . uh . . from maybe uh . . 10 years of age to later . . up until uh . . maybe . . 15 years old . . I had known these people uh . . to a fair extent as people who were around in my physical personhood neighborhood in my childhood experience . . uh . . either we uh . . spent time together at their house . . playing video games or uh . . we went to the park . . or they came to my house and played video games on the television . . uh . . and our parents knew each other . . and talked to one another . . uh they were 2 boys or uh . . 2 young men . . or uh . . 2 pre-adult people who were males . . that I was looking for at the house . . uh . . and so . . I suppose this dream could also relate to memories of childhood . . and uh . . maybe in my dream body . . I was trying to be closer to my friends . . and trying to remember them more . . uh . . and so visiting their house was a way to be in memory with them or something like that . . 


. . . the person from my dream in (1) . . which was . . uh . . maybe . . uh . . a few days ago . . maybe . . yesterday . . or maybe . . the day before . . uh . . this seems certainly that it was within the last week . . uh . . well . . the person . . had long white hair . . that grew to . . nearly the shoulder length . . 

the person appeared to be a young adult . . in maybe their . . 16 to 17 years old . . or maybe even younger . . uh . . 15 years old or . . uh . . 14 years old . . uh . . is also reasonable to think uh . . 

They appeared to be . . 5 feet . . and uh . . maybe . . 10 inches tall . . uh . . they were quite tall . . uh . . for a teenager it seemed to me . . but it also seemed like they hadn't reached 6 feet tall yet . . in my approximation . . but it's really uh . . difficult to say . . maybe uh . . they were uh . . well . . uh . . from what I remember . . it was uh . . uh . . someone who was . . uh . . slighly less tall than . . 6 feet tall . . uh . . their eyes . . were whitish in appearance . . white or graying or or uh . . in the way that . . uh . . it appeared that they had . . a vision impairment . . or that they couldn't see through their eyes properly . . uh . . I'm not really sure what they were wearing . . it seemed that it could have been a casual outfit . . like a t-shirt . . uh . . and pants . . uh . . but as I think about it more now that I'm awake . . uh . . maybe I remember . . a long sleeve t-shirt . . uh . . because I don't remember seeing their arms skin so much . . which could have been a feature that would have struck out to me to see their pale skinned or pale colored arms . . their skin coloration was was white . . uh . . or pale . . uh . . it was a very light peachy skin color uh . . like an albino person . . uh . . their hair was long as straight . . uh . . in perhaps complementary relation to curly hair or uh . . braided hair . . uh . . so . . for example . . the hair appeared not to be curled or braided . . and was long strands of hair . . down to nearly the shoulder length . . 


[Written @ 6:56]


Okay . . I wrote a few notes on my physical paper material notebook . . which is available here on this notebook journal entry as I wrote those notes . . uh . . I try to keep a word to word recitation of the material that I wrote from my notebook . . which is . . uh . . well . . there are 2 diagrams that I draw and so those are going to be described further in other ways . . or something like that . . 


. . . 

J Waaly & S Steven
JWhally & Steven
JWaaly & [First Initial] Last Name
JWhaaly  [First Initial] Last Name Nickname
          [Last Initial] First Name Nickname


                             [Diagram #2 is drawn here]
[Diagram #1 is drawn here]


* Move the mouse cursor between these two options (two is an example)
* Within these examples, you can select between the groups of items to perform an action such as "Undo" an action taken on the computer.

. . . 

uh . . these are the notes that I took . . uh . . well . . besides a personal message . . that .  . I also remember from the dream . . well . . uh . . 

. . . 

. . . 

In the dream . . 


What I remember from the dream:

- I was looking at a computer monitor
- I saw two peoples names on the mointor
- One of the people's names reminded me of Steve Jobs' name
- I was excited to see the name of "JWhaaly" [or something like that] . . where I remember getting the intuition that "J" stands for Jobs . . and "Whaaly" was their first name or uh . . maybe . . uh . . I'm not sure . . I thought I got the feeling . . that . . "J" was the initial of their last name . . and "Whaaly" . . could have been their first name . . but in any case . . this was an alternative reality and maybe naming conventions are different . . in this dream world . . uh . . but I remember specifically feeling that . . "J" was related to Jobs . . from the reality that I'm familiar with when I wake up . . 
- The name of the second person next to . . "JWhaaly" . . was something like "Steven" . . or . . uh . . maybe it was something else . . I don't remember the name so much . . I said to mysel fin the dream . . "Who cares?!" or something like that . . where I was expressing excitement for seeing the name of "Steven Jobs" or "Steve Jobs" or . . seeing "Steven Jobs"'s counterpart in an alternative reality . . uh . . I said something like . . "I am very excited to see the name relating to Steven Jobs since they are famous in our reality and they are well known for their work in the computer industry" . . those weren't the words that I said specifically but really those words mixed with feelings have having this thought feeling of familiarity and warm welcoming towards the name . . of "JWhaaly" or something like that . . there was an intuitional knowledge that uh . . maybe was unspoken in the dream world but this is a possible way of translating those feelings . . which seems close enough to how I feel and remember things from now that I'm awake and writing about the dream from my waking consciousness perspective . . 
- I remember feeling that I was looking at the designs for a computer system in an alternative reality where Steve Jobs worked at this other alternative reality computer company and they were part of a team that was with this other person . . named [I forgot the name now that I'm awake; hazy memory] . . 
- [the two diagrams that I draw are depicting the idea of what I saw on the computer monitor]
- I saw the design of a computer system or a way of interacting with a computer that was also shown next to the names on the monitor
- The idea is that you can use the mouse . . or computer mouse pad to do more than is already being done . . from my experience . . uh today . . when I wake up and experience how mouses are being used . . or something like that . . 
- 



Post dream thoughts:





[Written @ 6:51]


I was sleeping earlier today . . uh . . I started sleeping . . around the time of 0:00 . . today . . for . . October . . 20 . . 2020 . . and . . uh . . well . . uh . . it could have been later than 0:00 . . maybe . . 1:00 . . or 2:00 . . on October 20 . . 2020 . . which is today . . as I write this post . . 

I woke up from my sleep . . a few minutes ago now . . I woke up . . uh . . maybe . . 20 or 30 minutes ago . . uh . . well . . I started the live stream . . nearly 20 minutes ago from the time now . . at 6:53 . . as I write this message . . uh . . well . . uh . . 6:53 . . uh . . is early in the morning . . uh . . in my time zone uh . . something like that . . uh . . I'm only on planet Earth as I write this message uh and so that time delineation . . uh . . something something . . 

uh . . well . . uh . . uh . . I live in Missouri . . uh . . so that's my . . time delineation . . uh . . Missouri in the United States of America . . 

uh . . UTC . . 



Notes @ Newly Learned Words

delinea

Notes @ Newly Created Words

delinia
[Written @ October 20, 2020 @ 6:55]
[I accidentally typed the word . . "delinia" . . instead of typing the word . . "delineation" . . I'm sorry . . I'm not sure . . what this term . . "delinia" . . should mean . . at this time . . ]


