

# Dream Journal Entry - October 30, 2020

[Written @ 0:22 - 3:17]


I had a dream . . yesterday . . night . . for October 29, 2020 . . 

I wrote a few notes in my physical paper journal notebook when I woke up from my dream . . from my sleeping . . night-time-wake-up-from-a-dream type of dream . .

uh . . well . . sometimes dreams can be interpretted as goals that someone has . . or something like that . . and yet . . I am meaning to say night time dreams that I have when sleeping . . or visual uh . . life-like experiences that I have in my sleep time . . or something like that . . 

. . 

I wrote a few notes in my physical paper journal notebook . . 

here is a word-to-word transcription of what I wrote down . . 

. . 

[Page #1]:

Dream Journal Entry                 October 29, 2020
* I am in a classroom
* We are being taught mathematics
* I remember feeling like I had missed many lessons or classroom sessions before. I felt like
* we were sudying the properties of numbers it seems [from how I interpret this dream from my waking life standpoint after already having the dream]
* We'll stop here for today. Otherwise, that would let science go on . 

* Kent wrote this on a sheet of paper (notebook)
60% computer use left for my life
5% computer use left for my father [Richard] life
* Kent explained that our lives have energy about them that are representable by numbers which suggests that our present lives

* Range +/- 5 for each number analysis [?] A number ring for analyzing each number?

* ( ( 18 x 2 / 81 ) _/ Number ) = Number      81 _/ (  Number  ) = (Number / 18)
                                                     (  Number  )
                                                      (   Number  )
                                                        (   Number  )
                                                          (   Number  )


                            the form of the
                            result is expressed
                            in terms of
                            (Number / 18) x Number

                                    ^
81 is 18 in reverse                 |
                                    |
There is an equation or             |
formulation that allows you to      |
look at the result of dividing      |
any number with 81 and --------------


. . . 

[Page #2]:

* Numbers
* There was a photograph where we could see the
  properties of numbers such as a map where you
  could see which numbers are prime and which numbers are not prime.
* The properties of each number can be more than whethether
  a number is prime or if a number is not prime
* Each number has more properties that can be
  discovered or discussed and we were studying
  something like how numbers relate to one another
  across several property listings such as
  maybe one could study how atomic elements
  could relate to one another across atomic property
  listings such as the number of atomic nuclei
  and how the atomic element reacts with other
  elements in the physical world experience (for example
  combustion effects or elusiveness or rusting or other
  effects)
* A photograph or a picture of a flamingo and
  another creature were drawn in pink colored
  lighted squares or dots on the photograph. The map
  looked like maybe a heat map or maybe a forestry
  map where the forest has a shape and color density
  distribution ratio across the shape. The map
  represented how numbers relate to one another across
  several property vertices or several property verticals.


. . . 

[Page #3]:

* There were sieving processes involved in determining the
  property values of any individual numbers. Maybe
  like calculating a cacophony of values for any individual
  number to see how that number relates to all the other
  numbers across the entire number space [infinity and beyond]

* The photograph represented how numbers related to
  one another. Is the number prime ? Is the
  number an even number ? Is the number an odd
  number ? Does the number satisfy this or that relation?
  [Relation with other numbers]

* This photograph or picture map [after showing us]
* The teacher then said something like this map
  shows us that energy has certain properties
  in terms of energy being represented as numerical
  values. You want to use certain numbers for
  certain things based on the properties of that
  number and that this is done in cryptography
  for example. [Now that I'm awake and thinking
  about this topic, this seems to suggest that energy
  has different properties and some energies you would prefer
  to use for certain things based on its properties or
  something like that. Personally, I'm not sure how energy
  works so much. It seems that energy is a potential
  of some kind {crossed out: or a possibility or a probability}. But
  Maybe from how this dream teacher is teaching

. . . 

[Page #4]:

* Maybe . I don't know . I need to spend more time
  thinking about this topic.

* The properties of numbers can be drawn in a map
  where certain properties are considered very useful to
  study.
* Those properties relate to applying certain formulations
  of an individual number that also takes into
  account all the other numbers that may [or also
  may not] satisfy the formulation. The formulations took
  ranges of possibilities into account +/- 5 of something.  ______|
* A student in the class asked about the map and                  |
  how it relates to atoms. "Aren't atoms invisible?"              |
  "Aren't electrons positions not observable or calculatable      |
  and it's only like a probability or something like that ?"      |
  "Aren't atoms invisible and you don't know where their          |
  position is and it's only like a probability or                 |
  something like that?"                                           |
* Something like this was asked.                                 |
* The teacher responded to the student by                       \|/
  saying something about cryptography                         Maybe
  and topics of mathematics related                       something that
  topics.                                               relates to
                                                        probability?
                                                        probabilities?


. . . 

Notes @ Newly Discovered / Created Words

severl
[Written @ October 30, 2020 @ 1:18]
[I accidentally typed the word . . "severl" . . instead of typing the word . . "several" . . I'm sorry . . I'm not sure . . what this word . . "severl" . . should mean at this time . . ]

[Notes]:


### Notes on the _/ division sign / symbol

_/ is the division sign . . meaning that . . Number is being divided . . uh . . uh . . where the Number is . . the dividend . . and the content to the left is the divisor . . 

The content to the right of the = (pronounced "equals" sign) . . is . . the quotient . . 


Well . . in the dream . . the notation appeared similiar to the way division looks as in the image below . . and yet . . I have a feeling that maybe the notation had a different . . usecase . . from maybe how the division is applied in my waking life . . But I'm really not sure on that idea . . at this time . . I'm not sure . . maybe it's more of a symbol ? relating to the idea ? uh . . maybe it is exactly how division is carried out like how I'm familiar with . . in my waking life . . I'm not sure . . either way it is interesting to explore the possibilities to see how either interpretation could go . . 

Algebra Division Notation
![algebra division notation](./Photographs/algebra-division.png)




### Notes on the "Number" designation as written from my notes

I'm really not sure . . what the specific formulation or what the specific equation . . uh . . if we could even call it an equation . . I'm not sure what the process of performing this operation . . what the specific components . . were . . what the specific . . numbers were . . uh . . I'm analyzing my dream now . . from the stand point of my waking consciousness which has already seemed to have forgetten the specific details of the numbers that were involved in the division calculation . . well . . uh . . I wrote the term . . or the word . . "Number" . . to specify that it could be any number . . I'm not really sure . . which numbers . . 7 . . 6 . . 5 . . 4. . 3 . . 2 . . etc . . uh . . 700 . . 3000 . . and so on . . uh . . I guess . . I uh . . remember seeing double digit numbers . . uh . . so maybe that's more data that I could have written in my physical paper journal notebooks . . uh . . double digit and triple digit numbers . . so . . 78 . . 55 . . 32 . . uh . . those are a few . . double digit numbers . . uh . . maybe they're not exactly the ones that were from my dream . . uh . . well . . uh . . the ones from my dream . . uh . . well . . as I think about things now . . from my waking mind perspective . . I think they uh . . well . . I get the feeling that we were working with . . small numbers . . uh . . and not something . . really long like . . 1000986 . . uh . . or something really with more than 2 characters or digits . . 

uh . . well . . uh . . 2 or 3 digits is fine . . to approximate my feelings at this time . . uh . . well also . . uh . . it's not like my uh . . well . . specifically from what I remember . . I don't remember . . uh . . focusing . . uh . . focusing . . my attention . . uh . . focusing . . my psychological awareness . . or focusing my psychological attention . . or focusing . . my mind . . uh . . or focusing my uh . . psychological awareness or . . focusing . . my psychological attention . . or my . . psychological focus . . on what the specific numbers were . . and so . . I'm sorry . . for not remembering this aspect of the dream . . 

uh . . right . . uh . . "dummy data" is maybe another term that is used for this type of notation . . "Number" . . uh . . it's not real data . . but maybe a representation . . or something like that . . uh . . "hypothetical data" . . could maybe be another term . . uh . . I'm not exactly sure if "dummy" is a friendly word to use -_- . . 


### Notes on the list of dividends in parentheses

(  Number  )
 (  Number  )
   (   Number  )
     (   Number  )
       (   Number  )

The numbers . . appearing in the vertical list like the list above . . uh . . well . . uh . . I'm guessing this could have been . . uh . . in a way . . similar . . or maybe exactly the same . . uh . . way in which . . division . . is operated in my waking life experience . . where there are . . uh . . quotients of multiplaction processes . . that . . are . . uh . . subtracted . . from uh . . previous steps . . uh . . where a zero is added to the end of the previous step . . or something like that . . 

. . I don't remember what the specifc . . or what the exact numbers were that were on the sheet of paper that I was having in the classroom . . while taking notes . . in class . . but . . I do remember . . that there was . . a list of numbers . . uh . . uh . . right . . uh . . 

### Notes on the what Kent showed me in the dream

Kent had a notebook . . that he wrote on . . after we had . . finished watching the classroom lecture . . in the class . . together . . uh . . well . . I think we watched it together . . uh . . I don't really remember where he was sitting in the classroom . . uh . . but uh . . he approached me after class . . and uh . . 

on the notebook . . 

"60% computer use left for my life"
or maybe it was . . 
"60% computer use left for your life" . . 

uh . . he was referring to me . . in terms of . . my life . . as a person . . uh . . in the dream . . I think . . uh . . well . . if you could measure . . how long or how much time I spend on the computer . . uh . . well . . you could represent that as a number . . and . . you could do that for . . before and after the present moment . . uh . . and so . . uh . . I'm not sure . . how uh . . what my age was . . for my dream personhood . . or for my dream person . . or for who I was in the dream . . or who I was experiencing life as . . uh . . well . . uh . . I think Kent was showing me an example . . of what he was thinking . . in terms . . of . . what the teacher was talking about . . uh . . maybe he was trying to explain it to me in other terms . . in simpler terms . . with respect to how numbers are able to coordinate our lives or something like that . . or that we can use numbers to represent the energy values of our lives or something like that . . 

And so . . uh . . 60% computer use left for your life could mean something like . . uh . . you'll watch . . or you'll spend more time on the computer . . uh . . than had earlier spent time on the computer . . before the present moment . . by 10% . . 

something like that . . so for example . . 50% computer use left for your life could mean . . something like . . uh . . you'll spend . . as much time as you have . . spent on the computer . . up to this present moment . . and no more and no less . . and then . . uh . . that will be the end of your computer use . . for your life . . 

50% spent before now . . 
50% spent after now . . 

. . . 

40% spent before now . . 
60% spent after now . . 

. . . 

uh . . 

well . . uh . . Kent had also written . . below the text . . saying . . "60% computer use left for my life" . . 

"60% computer use left for my life"
"5% computer use left for my father's life"

. . . well . . uh . . uh . . I got the impression from the dream . . that Kent's father's name was "Richard" . . and so I wrote that down in my notebook . . uh . . I think he could have told me . . but . . uh . . I don't really remember seeing his lips move for that sound . . effect . . uh . . he could have spoken it and I heard the words but didn't see his lips move for that sound effect . . 

. . . 


### Notes on Page 2

elusiveness . . uh . . well . . this one . . when I was writing my notes earlier . . uh . . when I was writing the notes uh . . uh . . in my physical paper material notebook journal . . I had the impression in my mind . . of . . a transparent material . . like . . a . . uh . . plexiglass . . or uh . . maybe something different from that . . uh . . or . . uh . . a transparent . . material of some kind . . uh . . I'm not really sure . . in my mind's eye . . a transparent material doesn't necessarily need to have a shape or form in some sense . . for uh . . for uh . . well uh . . maybe then these aren't always uh considerable as atomic elements well uh . . I'm not sure . . uh . . well . . uh . . elusiveness was the term that I wrote uh . . even though uh . . uh . . maybe uh . . the topic of transparency could also have been written . . 

. . . 

[Thoughts]:

- the flamingo + creature image
  - this was a map of certin relationships about all the numbers that could exist . . something like that . . [*1]
  - the map of numbers looked like a colored picture
- 


[*1]
The idea that I got . . in my mind uh . . from this lecture . . in the dream world . . uh . . was that . . uh . . well . . you have numbers like . . 1 . . 2. . 3. . . 4. . . 5. . .6 . . and so on . . and those numbers can extend outward uh . . for infinity or something like that -_- . . 

okay . . well . . each number has . . certain properties that they satisfy . . for example . . you can ask if the number 1 . . is a prime number or if it's not a prime number . . and you can ask if the number 2 . . is a prime number or if it's not a prime number . . and you can ask if the number 3 . . is a prime number or if it's not a prime number . . you can continue to ask if each of the numbers is a prime number . . and this uh . . prime number characteristic of a number is really quite a cool property . . but there are more properties to learn about . . there are more properties to discover about each and every number . . 

okay . . so for any given number . . you can look at how that single number . . relates to . . every other number that satisfies certain conditions . . 

For example . . you can look at the number 2. . and . . look at how the number . . 2 . . uh . . relates to . . every number that satisfies the condition of being an even number . . okay . . and so . . if you have a relation formulation . . uh . . hmm I'm not exactly sure where i'm going with this . . the idea in my mind seems a little . . involved . . the example I'm giving now is really something I'm using to try to figure this out for myself . . uh . . at this time but also teaching it in a way that respects how the dream representation was seeming to appear in my memory or in my classroom dream vision . . uh . . hmm . . 

okay . . well the teacher in the dream . . didn't talk about . . prime numebrs and even numbers . . but to my waking mind . . these are close counterpart ideas or close ideas that relate to the topic of . . numbers having properties . . 

In my dream however . . there was this discussion of . . what if you can look at how a number relates to infinitey many numbers . . of a certain kind . . how a number relates to . . uh . . a class of numbers I guess is an idea uh . . for example uh . . with prime numbers . . uh . . maybe uh . . well . . I guess . . uh . . maybe it's uh . . well . . with prime numbers . . uh . . you are not able to have a divisor for that number that gives you a whole number ratio as a quotient . . 

uh . . well . . 3 is a prime number . . and yet . . 6 . . which is 3 * 2 . . or . . 3 multiplied by 2 . . is not a prime number . . because . . uh . . the number 3 and 2 are able to be divisors which result in a whole number ratio as a quotient . .

ahh right . . so uh . . I'm not uh . . that much of an expert in this topic . . and so I'm sorry . . uh . . for talking so much about it . . uh . . -_- . uh . but I'm encouraged to learn more and so please forgive me uh for sharing so much . . uh even if I'm so much a noob and trying to figure things out . . 

with a number like 6 . . maybe you uh-_- . . for certain processes in determining whether this number is a prime . . you are not in the assumption that looking at the numbers after 6 if an important idea for finding if 6 is prime or not prime . . and so for example . . looking at 7 . . to see if 7 is able to be a divisor . . of 6 that results in a whole number ratio . . is not important to do . . or something like that . . 

6 / 7 . . is a number that is less than 1 . . and so . . for example . . this uh . . could be considered as uh . . not going to be a whole number . . like . . 1 . . 2. 3. . . 4 . . 5 . 6. . 7. . 8. and so on . . 

6 / 8 . . is less than 1 . . uh . . and uh . . uh . . well . . uh . . it's also not going to be less than 0 . . and so . . well the class of numbers that are larger than 6 are numbers that we don't need to consider or something like that . . we are not necessarily needing to evaluate them in coming to the idea if 6 is a prime or not . . 

. . 

well . . imagine if you are having a type of number . . uh . . well . . and that you need to check . . uh . . numbers that are after that number to evaluate whether or not that number satisfies a certain property . . and so . . 

for example . . if you had another property called . . wow-so-cool . . wow-so-cool . . 

if you had . . a number that satisfies the property of wow-so-cool . . maybe you can only find if that number is wow-so-cool or not wow-so-cool . . if you evaluate other . . wow-so-cool numbers . . uh . . which may be larger than the wow-so-cool number that you're thinking about . . and so . . if your number is 7 for example . . and you want to determine if 7 is wow-so-cool . . you may need to have a heat map of the other wow-so-cool numbers before and after 7 . . . . -_- this is awkward . . I'm really trying to approach this idea that I was experiencing in the dream but it's uh . .

. . 

uh . . well in my dream . . I really felt . . uh . . awakened to a new way of thinking about numbers . .

hmm . . one thing . . that uh . . I think I remember . . uh . . well . . there are a lot of properties you can study about numbers . . and uh . . how those numbers relate to one another . . these relations are able to be formalized . . or formulated . . uh . . to be like . . measuring sticks . . or measuring poles on the ability of numbers and which group those numbers belong to . . there are so many groups to belong to 

the groups are like families . . families of numbers . . like prime numbers is a family of numbers . . you can be in the prime family . . like 3 . . and 5 . . and 7 . . and 11 . . if you are 13 . . you have infinitely many prime family members :O .  and you also have infinitely many odd family members :O how odd . . you are an odd number :O . . you are an odd character Shísān . . [Sān]: you are an odd member of the family of primes :O . . [Shísān]: there is a theorem that supposes that we're all odd :O . . [Sān]: do you think we'll ever get the chance to be normal? [Shísān]: maybe if we bring the whole family of primes together side by side . . :O [1]


. . . 

hmm . . 

. . . 

hmm . . 

well . . 

I think the picture of the flamingo and the animal in a colored diagram together was more of a representation . . to illustrate that you can have organizations of these number property relations on a map . . that is able to appear in a way that can be translatable by a human being . . or to be translatable . . in an easy way . .

Some maps of numbers . . aren't always lending themselves to be easily translatable or uh . . easily extendable in the way that people think . . and so . . for example . . the arrangement can look random or not organized in a way that is familiar to the person . . and uh . . well . . a picture that maybe is more familiar uh . . like a photograph where the shape is clear . . uh . . 

I guess these maps of a variety of patterns can be translatable and aren't necessarily difficult to read . . uh . . maybe it depends on your consciousness perspective and how you are familiar to reading information

. . 

hmm . . so maybe the flamingo is a symbol and uh . . in terms of how it relates to my waking life I could interpret the photograph that way . . and maybe not take it seriously that . . for example . . the thing we were studying . . which were really uh . . interesting groups of properties . . uh . . we were looking at . . groups of properties . . of numbers . . hmm . . and in a way these groups of properties of numbers . . could be a class of . . uh . . structures that you can visualize . . or that you can study . . 

hmm wow . . :O . . I think I finally wrote down what I was meaning to say after some time now :O 

groups of properties of numbers . . 

groups of properties of numbers . . 


. . . 

the properties like:

property #1. is this number a prime number ? 
property #2. is this number an even number ? 
property #3. is this number an odd number ? 
property #4. is this number a normal number ? 
property #5. is this number a rational number ? 
property #6. is this number a whole number ? 

. . . there are infinitely many properties one could consider for relating numbers to one another . . 

. . . 

you can have relationships between these properties that can themselves be . . 

. . 

. . properties of properties . . of numbers :O . . 

. . 

. . 

. . these properties of properties of numbers are a way for classifying or arranging properties so the properties of numbers can be studied in more and more systematic ways . . 

. . 
.
. 

properties of properties of numbers . . properties of properties of properties of properties of numbers . . 

uh . . hmm . . something like that I'm not sure one moment . . 

. . . 

. . . 

uh . . hmm . . properties of numbers . . right . . 

and then you can categorize those properties to find theoretical properties of numbers . . that you didn't know about . . like theoretical definitions for what a prime number is . . and so then those definitions could be . . uh . . well . . looked at . . to see if we can make more progress in the traditional definition and how we think about that definition or something like that . . or how we can find more primes or something like that -_- . . 

-_- . . we can colorize our map of properties of properties of numbers to make it appear as though those properties of properties of numbers are bale to be arranged in some systematic fashion so that there is a way to at least arrange a mental image of how those properties of properties of numbers relate to one another similar to how the arrangement of numbers appear as a straight line in a lot of numerical representation systems uh . . uh . . like the number line representation of numbers even though perhaps you could theoretically have denominations of the number system that are organized by the shape of what they represent such as . . 1 representing a period share . . and 2 representing a line shape . . and 3 representing a triangle . . 4 representing a square . . and 5 representing a pentagon . . and 6 representing a hexagon . . and 7 representing a septagon . . or a heptagon . . uh . . and . . 8 . . representing an octogon and so on and so forth . . 

and so . . you can instead of using a number line for example . . train the human mind to be more familiar with shape languages of a variety of types and not only linear numerical symbols for how numbers are presented today or something like that . . 

this can encourage curiosity of the shapes and languages of shapes and shapes and numbers can be related to one another in intuitive patterns that make the response to certain imagery easier to redesign and arrange by looking at . . 

. . 

the shape of numbers themselves could maybe be considered properties of numbers for example . . the shape of the character 0 . . is like an "o" . . or like circle shape . . uh . . and well . . imagine a theoretical representation of 0 . . to be something like . . T . . and so how does the shape of "T" influence the development of our mathematical system . . or how does the shape of "T" influence our way of thinking about the rest of the volume of mathematical ideas in our ability to suggest at this time . . or is that the right wording . . I'm not sure . . it seems like a hypothetical probable supposition that maybe supposes itself in the form other shapes that can be redesigned from looking at intuition patterns and how the meaning of words can be re-established at re-evaluation periods . . according to the chooser's will :O . . 

. . . 

properties of numbers . . 
  1. 1, 2, 3, 4, 5, 6 have property #1
  2. 100, 99, 98, 97, 96 have property #2
  3. 99, 999, 9999, 99999 have property #3
  4. 1, 1023, 702 have property #4
  5. 520, 1314 have property #5
  6. first, second, third, fourth have property #6 (ordinal numbers)
  7. point, line, triangle, square, 
  8. square, rectangle, trapezoid have property #8 (4 sides, 4 corner shapes)
  . . . 

properties of properties of numbers . . 
  1. property #1, property #2, and property #3 belong to property type #1
  2. property #1, property #100, property #103 belong to property type #2
  3. property #9, property #99, property #999, property #9999 belong to property type #3
  4. 


properties of properties of properties of numbers . . 

properties of properties of properties of properties of numbers . . 


. . . 

When we have numbers with properties . . if the numbers . . we have found . . satisfy a property . . uh . . well . . we could ask the question . . 

what are some other numbers that satisfy this property ? 

and those other numbers may not be known . . or those numbers . . may not be easy to find . . 

well maybe by studying . . a property that is in the same property type . . as the property we are originally studying . . we can find . . those unknown numbers . . or we can maybe get a clue of which other properties we should consider using to studying . . where those numbers could be . .


. . . and you are always able to create new properties . . based on wanting to satisfy the condition of being in the property type . . if you would really like . . to . . uh . . really uh . . study uh . . study the property type itself . . 

and if you still are not really sure how this property type really works . . you can always study another property type . . that is in the same . . property super type . . as the property type that you are originally . . thinking about . . this can maybe give you a clue on how to construct . . or how to create . . or come up with a property that will allow you to maybe . . observe . . certain . . characteristics of numbers . . or something like that . . I'm not sure . . 

There's an ocean of numbers . . there's an ocean of properties . . there's an ocean of property types . . and you can always identify new . . and unknown numbers . . and you can always discover new and unknown . . properties . . and you can keep studying into the truest infinities of your heart's imagination

. . . 


. . . 

these are each belonging in groups . . for example . . maybe . . 

property 2 and 3 are in the same group called . . something something . . 

there are different types of prime number identity systems . . for example . . why do you have to check if a number is able to be divided by another number less than itself ? . . uh . . can you set an arbitrary limit to be like . . uh . . so long as the number is greater than half the number . . it is not okay for that number to be divided into a whole number . . . . 

if you have a number P . . then the number P / 2 . . is half of P . . and so . . you can say that . . it is okay for the numbers less than P / 2 . . to have whole number divisors . . of P . . -_- . . I'm not really sure if this is uh . . something that uh . . let's see . . an example . . 13 . . is P . . and so . . 13 / 2 . . 6.5 is P / 2 . . uh . . well . . uh . . it is okay if any whole number less than 6.5 is able to be a divisor . . of . . uh . . P where the remainder is 0 . . 

okay . . so . . 6 . . 5 . . 4 . . . 3 . . 2. . . 1 . . these can all be divisors of P . . and we are okay with that :D . . uh . . something like that . . 

hmm . . well uh . . these are uh . . not divisors . . uh . . so we only need to check if . . 7 . . 8 . . 9 . . 10 . . 11 . . and 12 . . are divisors . . uh . . well these are not divisors either since . . uh . . we uh . . know that 13 is prime xD . . which is a special type of prime . . without this arbitrary storyline description . . about . . limitations on which divisors can and cannot satisfy the ability of being prime . . well in any case . . without going to far into this storyline then . . we can see this new type of property as being in a group of properties with the property of being a prime (item#1 in the list above . . :O )




Algebra Division Notation with vertical list of dividend numbers
![algebra division notation](./Photographs/algebra-division-2.jpg)




The Copeland–Erdős constant

0.23571113171923293137414347535961677173798389...,

obtained by concatenating the prime numbers in base 10, is normal in base 10, as proved by A. H. Copeland and Paul Erdős (1946). More generally, the latter authors proved that the real number represented in base b by the concatenation



[1]
Normal number
https://en.wikipedia.org/wiki/Normal_number

