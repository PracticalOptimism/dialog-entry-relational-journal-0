

# Dream Journal Entry - November 2, 2020

[Written @ 14:58]


[Written @ 14:17]

Last night . . while I was sleeping . . I had a dream . . 

I woke up . . 1 hour ago . . or less . . in terms of time . . uh . . in terms of . . uh . . minutes . . I woke up . . 60 minutes ago or fewer minutes . . uh . . maybe . . closer to 40 minutes ago . . 

. . 

I had a dream where I was with a group of friends . . there people are like family friends . . I have known them for . . a long time . . since . . I was a child . . or since I was in my childhood in my pre-teenage and teenage years . . and . . well . . 

There were 3 people . . a father . . the father's son . . and . . the father's daughter . . 

The 3 people I was with . . we were in a cavern . . and . . well . . there was a door . . entrance to the cavern . . the father's son . . had . . found a camera . . inside of the cavern . . I was standing . . outside . . of the cavern entrance door . . and . . the father's daughter . . was not inside of the cavern with the father's son . . and the father . . themselves . . well . . the father's daughter . . may have been standing outside of the cavern door . . as well . . and so . . well . . 

The father's son . . had found a device inside of the cavern . . the cavern entrance appeared like a closet . . a walk in closet . . to the distance that I was looking at . . and so . . the father and the father's son . . were standing . . with . . rocks . . and the roughness of the surface of the cavern . . wall . . behind them . . 

And . . I was standing outside of the cavern . . entrance . . area . . where they were standing . . and there was a metal gate . . 

The metal gate had bars . . vertical and horizontal metal bars . . that separated the . . me . . from the . . the father and the father's son . . 

. . . 

The father's son . . had been deeper inside of the cavern . . into an area that I couldn't completely see . . while . . standing from outside of the cavern entrance door . . 

. . . 

The father's son . . returned from being deeper inside of the cavern . . and . . I noticed that he looked different from how I had known him to look before . . His face structure was different . . His chin . . looked slightly less . . width . . or . . less tall . . where he would have normally had . . a long . . face . . with an extended chin . . or with an extended jaw in the vertical direction . . it seemed that . . he had . . a new face . . and looked slightly different from how he looked before . . 

. . . 

I found a camera . . in the cavern . . later in the dream . . when I had entered the cavern . . I had entered the cavern . . and . . well . . in a room . . a few meters into the cavern . . from where the entrance was . . I could see . . a table . . I think . . the table could have been made of a rock . . or a giant boulder . . or maybe it was a metal table . . I'm not exactly sure . . as I'm writing this from memory . . and . . uh . . well . . I forgot the exact details of what the table was made from . . but I noticed a table . . at the center of this cavern room . . which had an opening in the . . sky . . or it had a sky area . . right after the room . . that . . I uh . . well . . the cavern was maybe more like a hallway . . and the hallway . . curved . . uh . . and changed sizes . . at various lengths a long the hallway or cavern . . uh . . area . . well . . at . . 20 or so meters inside of the hallway . . that had already curved . . left . . straight forward . . then right . . right after you go to the right direction . . you'll see uh . . well . . I think this is what I remember from the dream . . but it maybe not be the way that I remember exactly . . uh . . or I uh . . well . . I feel like the cavern was not exactly . . closed . . or not exactly fully covered in the center where we were . . in the area where the "straight forward" direction in the hallway-like cavern structure was . . 

Well . . right after you turn right . . in the hallway . . uh . . you'll see a sky . . to the left side of the cavern hallway wall . . to the top left . . and there will be a sky . . and the sunlight is shining down . . I don't remember looking exactly at this feature of the cavern . . but it feels like it could have been the background for where we were . . since I felt the sunlight coming through from a . . distance away from . . area where this table was . . from the area where the cavern hallway is straight . . or parallel with respect to the entrance . . area . . 


. . 

cavern entrance [go straight ahead]
cavern entrance inside of the door [turn left]
cavern interior a few meters inside [go straight, parallel to the entrance]
cavern interior more meters [turn right]
  - look to the top left of the hallway to see a sky 

. . 

I remember there was a camera . . on the table . . inside of the cavern . . 

I think this the camera . . that my friend . . had used to take a photograph of themselves . . The camera looked quite old in terms of how it relates to digital cameras that I'm familiar with in my waking life . . the Camera looked like something out of the Fallout game series . . 

The camera . . had a way . . for you to take a picture of someone . . as they look into the camera . . and the camera would . . transform how that person looked . . for . . the time duration of a few seconds . . or a few minutes . . And so . . I think . . the result of my friend looking different from how they looked before . . was from having used this camera . . uh . . well . . the camera . . was uh . . like . . you press a button . . which is . . uh . . like a push-in button . . that's large and rectangular . . and it has . . like maybe a flap over it to preven accidental presses or something like that . . a flap or a . . uh . . cover . . uh . . or a garage-door style opening where you can pull the garage door out . . and inside is a button . . that you can push in a few millimeters . . and it would capture an image of the person . . as well as . . transform their appearance . . 

The camera . . printed an image of the person . . and so . . you could see a photograph of what they looked like right after being transformed . . in tersm of their appearance . . 

Their face transformed . . it's like . . they looked like a slight variation of themselves . . like a possible alternative . . version of themselves . . a counterpart . . or a doppleganger . . or a genetic variant of themselves . . 

. . . 

In terms of how the camera worked . . I'm not really sure . . uh . . well . . there was a camera flash . . and so maybe there is light involved in terms of how the light transforms or manipulates the person's genetic structure as well as the person's bone structure or something like that . . 

. . . 

In the dream . . My friend's father . . the father . . the friend . . the friend father . . that I was with . . took the camera . . and showed me how to use it . . I was confused in terms of how to use the camera . . but . . I gave the camera to the father . . and . . he took a photograph . . of . . his daughter . . the daughter . . well . . the dream ended before I could see the transformation . . but I think . . that's . . uh . . well . . I saw a . . uh . . well . . I think there was . . uh . . maybe there was a photograph film that was printed so you could see a physical print out copy of a photograph of the person after they had transformed . . but . . this was in the corner of my vision before I woke up . . and so I didn't really see what the details of the photograph showed . . and I didn't really see what the person looked like . . I'm sorry . . 

. . .

Notes @ Newly Created Words

durect
[Written @ November 2, 2020 @ 14:52]
[I accidentally typed the word . . "durect" . . instead of typing the word . . "duration" . . I'm sorry . . I'm not sure what this word . . "durect" . . should mean . . at this time]


durac
[Written @ November 2, 2020 @ 14:57]
[I accidentally typed the word . . "durac" . . instead of typing the word .  "duration" . . I'm sorry . . I'm not sure what this word . . "durac" . . should mean . . at this time]


. . . 

Notes @ Newly Created Words

disn
[Written @ November 2, 2020 @ 14:44]
[Discovered from typographical error of "distance"]

Notes @ Newly Learned Words

fent
[Written @ November 2, 2020 @ approximately 14:44]
[Discovered from typographical error of . . "felt"]

Notes @ Newly Created Words

enteren
[Written @ November 2, 2020 @ 14:32]
[I accidentally typed the word . . "entered" . . as . . "enteren" . . I'm sorry . . I'm not sure what this word . . "enteren" . . should mean at this time . . ]

