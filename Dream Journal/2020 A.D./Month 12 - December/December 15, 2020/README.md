

# Dream Journal Entry - December 15, 2020


[Written @ 23:03]

I had a dream a nearly 2 hours ago . . give or take . . 1 hour . . 2 hours ago . . + / - 1 hours . . 

2 hours ago + / - 1 hours . . 

2 hours ago Plus or Minus 1 hour . . 

. . 

Timeline Annotation:

Part I:
- I was in a neighborhood
- The neighborhood was filled with nice houses
- I saw people walking in the neighborhood sidewalks
- I followed some of the people
- I followed some of the women walking around
- I think the women came from a school nearby and were going home afer school
- I felt like I was a student who attended the same school as these women
- The school felt like it was a high school or pre-college experience
- The women were attractive in their physical appearances
- The neighborhood was filled with nice houses that were large and fabulous like suburban housing with a wealthy-family-lifestyle-aesthetic for all of the houses that I saw [Wealthy with respect to the American style of large suburban house living that could be stereotyped from movies in the period of the century where I'm from 2020 A.D. + / - 20 years]

Part II:
- I was in a house
- I think I was in my best friend's house . My best friend is a caucasian male human being with dark brown hair and nearing 6 feet tall + / - 2 inches
- I saw my best friend and his father in a car leaving to go somewhere together . My friend's father is a caucasian male human being and dark brown hair color and nearing 6 feet tall + / - 4 inches . . I think they could have been slightly taller than their . . son . . uh . . but . . That is a hypothesis based on the . . fact . . that these people . . are represented in my dream . . as physical counterparts . . to the people that I know from my waking life . . who are also . . my best friend and their father . . and I've seen . . their . . proportions . . and I think it's the case . . that in real life . . the father . . is taller . . in real life . . than the . . son . . in real life . . which . . uh . . well . . the dream people . . had similar qualities . . and so I'm . . hypothesizing that the same height relation could also be satisfied in this . . dream world . . where I was . . However . . I uh . . didn't see the comparison . . of the height of the dream people . . in the dream world . . to see . . if in fact that hypothesis . . is supported by my dream world vision . . and dream world thinking . . and reasoning . . facilities . . or something like that . . The people were going into their car in the drive way . . and I didn't necessarily . . determine . . or think to determine . . who was . . taller . . or something like that . . 
- I had the thought to myself . . that . . the father was taking their son to eat . . and do . . activities together or something like that . . 
- -_-

Part III:
- I was standing outside on the front porch of my best friend's house
- I saw my best friend talking to a lady . . down the stairs . . in the front yard of the house . . a few feet from the porch . . above . . the stairs . . where I stood . . watching them . . talking . . to one another . . 
- My best friend is a handsome male person
- I thought to myself . . or felt that it was possible for this woman to be romantically interested in my best friend
- I on the otherhand . . didn't necessarily feel . . like I was a handsome looking person . . -_- . . it was a complicated series of thoughts . . 
- On the one hand . . I wasn't feeling like I was a very attractive person . .
- On the other hand . . -___----__- . . I looked at attractive people
- The woman . . who my friend was talking with . . was semi-attractive . . 
- Attractive qualities from attractive people . . And new qualities that I didn't really know . . how to think about . . and so . . a bulbous . . face . . with . . a supermodel face appearance . . weren't necessarily features that I had seen before working together . . and so . . although the face was beautiful . . the shape of the head . . was bulbous . . and alien to me . .
- -_-
- -_-
- -_-
- -_-
- -_-
- In my mind
- I saw this woman as attractive . . the woman that my best friend was talking with
- I saw this woman as attractive . . and yet I felt that maybe I was questionable to them . . and that my attractive best friend was . . more attractive to talk to . . uh . . but I think I saw that the woman could also be said to be unattractive . . with respect to the population familiarity score of the combinations of the features of her body and face . . or something like that . . how familiar her facial and bodily features are . . could possibly be a way to determine the perception of beauty for that person . . but I'm not really sure . . familiarity . . as well as . . associative landmarks for familiar personal qualities like kindness and love and goodheartedness . . and other qualities . . that could be represented in associations through films or popular media -_- this is maybe not the way you would think about attractiveness -_- I'm not really sure what I'm saying but am trying to describe the phenomenon of how I felt or whatever -_- . .
- -_- . . why is this unattractive woman . . communicating with my friend . . as though to suggest . . attractiveness . . Do they think they're attractive ? . . That is questionable . . -_- . . And so maybe there was a dice in the air . . at all times . . during the conversation . . as I saw them . . talking . . where I questioned . . whether this person was attractive or not . . and the answer . . -_- . . was a dice roll . . -_- . . I guess . . this could be either . . uh . . -_- . . but . . when it comes to an attractive person . . the dice isn't -_- . . the feeling of . . -_- . . -_- . . -_- . . the feeling of -_- . . -_- . . -_- . . hmm . . I guess I am -_- . . okay with an attractive person -_- . . or something like that . . I don't have -_- . . -_- . . -_- . . -_- . . 
- -_- _--_-_-
- -_-
- 

Part IV:
- I saw someone running in the neighborhood . . the person was running outside on the sidewalk . . as though they were exercising . . 
- The person appeared to be a male
- I saw the person turn near a street corner
- I think I saw 1 or 2 other people . . after this person . . who were also on various . . uh . . sides of the street of my neighborhood . . and running on their own . . paths . . or on their own exercise routines or something like that . . They also appeared to be . . male human beings . . 
- I was inspired to also run . . and . . running . . I thought that I could run with someone else . . so we could both go fast . . and motivate each other to keep a fast pace . . or at least to . . motive each other to continue the run . . even if we slowed down . . we didn't need to walk . . because . . I like to run . . and I felt like running . . and thought it would be cool to run . . on that sunny day . .
- I saw someone . . a male person . . running out of . . their house . . they appeared to wear shorts . . and . . a short sleeved shirt . . or something like that . . this . . person . . was . . running from their house . . across the street . . a few houses . . away . . from where I was . . and near where I saw the first person running . . the person who turned the street corner . . uh . . well . . this new person who ran out of their house . . crossed the street road . . and was on the side of the road . . where . . my friend's . . house . . was . . The person . . was running . . on the sidewalk . . on the side of the road . . where . . my friend's . . house . . was . . 
- I got a mental image of myself . . and that I was wearing . . a hoodie . . and . . pants . . I was carrying something in my hoodie pockets . . I'm not exactly sure . . what it was . . it could have been . . a pokeball -_- . . the item was large . . and round . . spherical . . -_- . . -_- I don't think it was a pokeball -_- but the item could have been interpreted that way . . I think it could have been a plastic bag . . or a group of plastic bags . . rolled up . . into a large . . plastic bag . . ball . .
- I ran after the person . . with the shorts . . who had . . crossed the road . .
- I ran quickly towards them . . I was intending to ask them if it was okay if we ran together . . just because . . I was anticipating that maybe the person would like to run fast . . since they appeared to start that way . . from when they crossed the road . . and continued forward from there . . along the sidewalk . . in the direction . . away . . from where . . I was . . standing . . near the front yard . . of my friend's house . . 
- Before I caught up to the same position . . that the person was in . . The . . person . . had . . turned . . the . . corner . . The person . . turned . . right . . at . . the . . street . . corner . . ahead . . similar to how the first person I saw . . had turned left . . on the same . . T-shaped . . street corner . . 
- When I turned the corner . . I saw the person . . a few . . feet ahead . . but . . they . . had . . stopped . . running . . and they were . . walking . . instead . . or maybe they had . . just started to walk . . I'm not sure . . but . . I got the impression that . . maybe . . I . . didn't need to ask this person if they would like to run together . . any more . . instead . . uh . . I could uh . . do something else . . since maybe . . they wouldn't want to run . . so much . . or . . uh . . had . . another plan . . for how . . they want to exercise . . or something like that . . or maybe they weren't as aerobically endured . . as I was anticipating and preferring from a running partner at that time . . in the dream . . or something like that . . 


Part V: Erasing All Of My Phone Data By People From Another Crag
- -_-
- -_-
- -_-
- -_-
- -_-
- -_-
- -_-
- -_-
- the person erased the data from my cellphone
- the person talked about linux computers
- -_-
- -_-
- I had the feeling that this place was a part of the neighborhood where . . if you are nearby . . the government . . is able to . . collect all the information about you that they wish to collect . . and so for example . . all of the data on the cellphone of all the people are collected . . 
- There could have been cameras in this area as well
- It was like a spy-center for governments to spy freely
- These individuals erased my phone data . . before the data was transmitted . . to the government agencies . . or the military . . or whoever was involved in the project . . something like that . . 
- I said "I'm sorry . . I'm sorry" . . I didn't know this was the law around this area . . -_- . . but -_- . . I'm not exactly sure -_- . .
- One of the persons inside of the fence . . reached their hand through the fence . . and tapped their cellphone-appearing hand device . . against my cellphone [These were both cellphones with touch-screen displays] . . Upon touching my cellphone with their cellphone . . The graphics on my cellphone changed to show . . what could have been . . as . . progress bar . . the progress bar . . was gray in color . . and maybe . . could have . . moved . . in the left direction . . or translated . . in the left direction . . or . . interpolated . . in the left . . direction . . as though to suggest . . deletion was occuring . . and . . psychologically . . this had given me the impression . . that the data on my cellphone was being erased . . after . . my cellphone had . . been . . in proximity with the cellphone device . . of this other person . . 
- -_-
- -_-
- -_-
- It looked like these caucasian male human beings . . were trying to help me . . to prevent . . the government from spying on me . . in this . . high density open government spying area . . where there was the privilege for the military . . or the government . . to collect . . all of the data . . on what happens in this area . . of . . the . . city . . or in this area . . or the neighborhood . . of this city . . or something like that . . in this . . baseball field . . in this . . neighboorhood . . park area . . of the city . . in this neighborhood baseball . . field of the park . . or something like that . . 
- 


Notes @ Newly Learned Words

Crag
[Written @ December 16, 2020 @ 0:24]
[-_- . . it feels like . . the places that I visit in my dreams . . are possibly associable . . to real places . . that exist . . uh . . and so for example . . the place . . that I . . visited . . in my dream . . last night . . one of the persons . . mentioned that I was . . "A black guy from another crag." . . "You're a black guy from another crag." . . "We want to do a here-with-you-now." . . I received this message at the end of . . Part V . . and . . got . . the . . impression . . that . . the person . . wanted to . . do a live stream conversation . . for the dream journal recording that I have here on the live stream . . My memories tell me that "Crag" is another meaning for "star system" in the dream world language environment for the people on this planet somewhere far away from the planet that I call home]
[[1.0] . . is a resource . . relating the topic of imagining the reality . . of . . fantasy . . worlds]

Here-with-you-now
[Written @ December 16, 2020 0:30]
[In the dream world . . I experienced a person saying the words "here-with-you-now" . . which is interpretable to mean "live stream"]




[1.0]
Danielle Baskin CTW2019 Lightning Talk
By Creative Tech Week
https://www.youtube.com/watch?v=VGUGMWez3Dg

