

# Dream Journal Entry - December 8, 2020


[Written @ 3:20]

### Sequence of Events:

- I was a person . . running . . on the sidewalk . . next to my local school building . . It was night time as I ran . . No sunlight . . No moonlight . . No streetlights . . it was really dark . . 
- I was crossing a road . . that leads into the school parking lot . . from outside of the school . . 
- I stopped in the middle of the road . . I was crossing . . and my senses were . . stunned . . I felt like . . I was disoriented . .
- I crawled backward . . on my arms and . . legs . . with my bottom . . facing the ground . . 
- I was afraid that a car was going to come across over the hill . . and wouldn't see me . . in my vision-impaired state . . it felt like I was . . maybe . . my body had shut down . . my nervous system . . the electric component of my body was really . . impaired and I was shocked and couldn't . . sense the world like I was normally used to . . My eyes . . were having a wavy rippled . . fragmented-glass-like visionary system . . and the sound maybe felt similarly fragmented . . I'm not sure . . I remember mostly I felt an electric sensation throughout my body . . as though I had . . frightened or shocked . . or uh . . pressed . . on uh . . various . . nervous system . . uh . . nerve endings . . or uh . . large . . uh . . bodies of . . nervous tissue in my joints or in various parts of my body or something like that . . 
- While I was . . in a physically shocked state . . My uh . . mind felt like I was . . uh . . at least able . . to crawl . . uh . . but I was fearful that . . because it was . . night time . . and it was really dark outside . . and . . the road . . that I was crossing . . was a sort of hill . . where . . I imagined . . if a car were coming over this . . small hill structure . . that . . because . . I was . . on the ground . . the person in the car . . would find it difficult to see me . . and maybe the car could drive over me . . and my body . . and my head would get crushed . . 
- The dream transitioned from this . . previously described sequence . . uh . . I don't exactly know the . . time taken to transition . . or if there were any . . neighboring events in this . . area . . of the dream before the transition . . but . . uh . . A sort of different . . environment . . appeared before my presentations of experience . . in the dream . . where it seemed like . . it was a scene change . . but noticing the scene change wasn't really uh . . a fast transition or something like that . . it was more gradual and maybe it was difficult to notice . . but because I don't remember the scenes inbetween . . I'm saying that it's a scene change . . uh . . but really uh . . uh . . the environments were so similar . . uh . . it could be considered the same scene . . but I just . . uh . . I don't exactly remember the details of uh . . doing the uh . . actions that would have made the . . uh . . memory of . . uh . . transferring uh . . using my body . . and not transferring using . . a camera scene transition effect . . 
- In this next scene, I was nearby the same road . . but . . uh . . maybe it wasn't the same road . . maybe it was a different place . . but . . uh . . it was still night time . . and uh . . to my right . . behind me . . uh . . I could imagine that the road . . was there . . the road that I was trying . . to cross . . 
- I was disoriented . . and next to a wooden house . . the house . . appeared . . uh . . like . . uh . . a log cabin . . a log cabin . . that appeared uh . . a little . . uh . . middle-class lifestyle . . like . . I had the impression in my mind that the family that lived there could be living a comfortable life there . . uh . . 
- I wasn't as disoriented as before . .
- I was sitting in the grass . . front yard . . which uh . . I suppose . . uh . . because I didn't see myself crawling to this house . . uh . . I don't really know . . but uh could imagine . . now that I'm awake . . I could imagine . . that . . the house . . the wooden log cabin house . . was to the left side of the . . school campus . . uh . . if uh . . you're facing . . the direction of the front yard of the school . . where the . . road I was trying to cross was in the front yard on the left edge . . of the campus . . 
- 

. . . 

### Thoughts About The Dream:

- Roads
  - The road was hilled
  - Sometimes I've seen hilled roads in real life
  - From how I'm imagining things in my mind at this time . . I don't know if a car is going to see someone sitting on the ground . . especially when they're immediately over the hill top . . that could be a dangerous thing that happen in real life
  - I don't know what to say -_-
- Neighbors
  - I don't know what to say -_-

. . . 

### Dreams Related To This Dream . . That I've Had Before:

- [Dream #1] I was running . . near a school in one dream . . within the last 1 month [Related because I was running . . near the school . . on the sidewalk]
- [Dream #2] I jumped into a swampy ditch . . [or a swampy . . or muddy . . trench . . or sewage washway creek dent on the side of the road . . parallel to the roadway] . . to get someone out of their crashed car . . within the last 1 or 2 month . . [Related because someone was in trouble] [Occurred on the same day or night as Dream #1 . . if I remember correctly . . ]
- 




[Written @ 3:01]

I had a dream . . on . . yesterday . . the night of . . December . . 7, 2020 . . on the day of . . December 7, 2020 . . 

uh . . I think it was day time . . I'm not exactly sure . . 

I think I was taking a nap during the day time . . -_- . . I don't really know . . uh . . these days I don't pay attention to that a lot . . it seems like . . I'll wake up . . uh . . and the window is dark . . and so . . uh . . maybe it's uh . . late afternoon . . or . . uh . . I guess . . it could also be early morning . . but I'm uh . . not always sure . . I'm not sure . . 

Uh . . well . . I slept . . uh . . I think it was . . in the late evening . . uh . . or maybe it was . . uh . . in the middle of the day . . and I forgot . . that . . I was taking a nap in the middle of the day . . -_- . . 

uh . . hmm . . 


Dream Title
Dream Description
Dream Benefits
Dream Features
Dream Limitations
Dream 

. . . 

hmm . . I'm not exactly . . sure . . how to write a synopsis for dream journal entries . . I've tried . . a 3 part method before . . 

part 1: . . . 
part 2: . . . 
part 3: . . . 

. . . 

hmm . . 

I also normally like to write free-style where I just write whatever . . uh . . whatever uh . . I'm feeling . . uh . . but sometimes that seems to get . . out of hand in the sense that . . maybe . . uh . . I'm uh . . uh . . spending a lot of time on the . . uh . . story . . uh . . when maybe . . I could . . spend less time on writing the story . . uh . . and extract . . those details of the dream that I remember . . 

. . . 

uh . . 

hmm . . one thing that's also . . interesting about writing dreams . . is that . . (1) . . I'm awake . . now that I'm writing . . uh . . which has an influence on . . what I uh . . remember . . or what I write . . uh . . it seems uh . . that maybe . . uh . . because I'm awake . . uh . . my uh . . normal waking mode of consciousness . . is uh . . realized . . or uh . . more uh . . influenced by the things that I'm aware of or the things that I'm familiar with . . like . . uh . . things making sense to me . . within a sort of certain mental . . uh . . framework . . that I'm used to in my waking life . . uh . . and so in other words . . I feel like . . that maybe . . uh . . I'm not always . . immediately . . aware . . of maybe how I should present the story . . because the . . sequence of events that I . . experienced . . doesn't always necessarily seem like . . a sequence of events that . . I would experience . . in my waking life . . and so . . adjusting my mental architecture . . or my mental pattern . . or my mental preparedness . . to describe the story . . may be a thing . . that . . I uh . . undertake . . to prepare . . to write the story . . in a way that preserves the . . story . . uh . . and the details of the story . . uh . . and not always for example . . providing commentary . . on the symbology . . or the story . . uh . . which is something that I can do quite often . . if I'm writing in freestyle . . maybe . . I'll try to . . interpret . . the story . . as . . I'm writing it . . which is a sort of uh . . maybe uh . . those things should be . . separated to uh . . make clear which part of the . . story was the dream . . and which part of the story was the mental interpretation model of what you think the symbols meant or . . what sort of . . uh . . message was being communicated uh . . uh . . in terms of . . maybe a moral message or something like that . . or uh . . consequences . . or uh . . emotional relations or something . . or social relations . . uh . . right . . so . . maybe writing . . uh . . a . . uh . . (1.1) background . . foreground characters . . scenery . . uh . . consciousness state description . . sequence of events . . those . . could be a few touching stones . . to . . talk about the story of the dream . . and then things like . . (1.2) your thoughts and opinions . . on the story . . uh . . how you felt during certain . . scenes . . of the dream . . maybe that could be . . in a secondary . . stream of information . . that can be concatenated with the first stream . . using some sort of referencing system like . . uh . . number references . . at uh . . part 1 . . at . . part 2 . . -_- . . I don't know -_- . . 




Notes @ Newly Created Words

mentral
[Written @ December 8, 2020 @ approximately 3:12]
[I accidentally typed the word . . "mentral" . . instead of typing the word . . "mental" . . I'm sorry . . I'm not sure what this word . . "mentral" . . should . . mean . . at this time . . ]


emtal
emnatal
emtanl
[Written @ December 8, 2020 @ approximately 3:11]
[I accidentally typed the word . . "em" . . while I was preparing to type the word . . "mental" . . "emtal" . . "emnatal" . . "emtanl" . . seem like . . possible . . probable . . counterparts . . or . . uh . . uh . . possible uh . . continuations . . of the word . . "mental" . . uh . . if uh . . well . . if I was . . uh . . trying to get the . . architecture . . of the word "mental" . . even after writing . . "em" . . which is . . or was . . a typographical error . . uh . . along my journey to type the word . . "mental" . . uh . . I'm not really sure what "emtal" . . should mean . . at this time . . I'm not sure what the word . . "emnatal" . . should mean at this time . . I'm not sure what this word . . "emtanl" . . should mean at this time . . ]




