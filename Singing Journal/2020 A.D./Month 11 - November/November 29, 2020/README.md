


# Singing Journal Entry - November 29, 2020


[Written @ 0:53]


### Album Title: Ajeet Kaur - Haseya
### Song Title: Re Man Lullaby
### Song Number Index: 6 of 6
### Lyrics: [7.0]
### Song Reference: [1.0 @ 0:40:10 - 0:48:43]

Ray man eh bidh jog kamaa-o
Oh my mind, practice Yoga in this way:

Singee saach akapat kanthalaa
Let Truth be your horn, sincerity your necklace,

Dhi-aan Bibhoot charaa-o
and meditation the ashes you apply on your body.

Taatee geho aatam bas kar kee
Catch your burning soul (self) and stop the flames.

Bhicchhaa naam adhaarang
Let the soul (self) be the alms bowl in which you
collect the sweet Naam and this will be the only
support you will ever need.

Baajay param taar tat har ko
The Universe plays its divine music. The sound of
reality is shrill, but this is where God is.

Upajai raag rasaarang
When you listen to the reality from this place of
awareness the sweet essence of Raag arises.

Ughatai taan tarang rang
Waves of melodies, emotions, and passions arise and
flow through you.

Gi-aan geet bandhaanang
Bind yourself with the song of God.

Chak chak rehay dayv daanav mun
The Universe spins like a potter's wheel and from it
fly demons and angels.

Chhak chhak bayom bivaanang
The sage listens to this and instead of getting caught
in either one, the sage drinks the nectar of the
heavens and is carried to the heavens in a divine
chariot.

Aatam upadays bhays sanjam ko
Instruct and clothe yourself with self control.
Meditate unto infinity

Jaap so ajapaa jaapai
until you are meditating without meditating.

Sadaa rehai kanchan see kaayaa
In this way, your body shall remain forever golden,

Kaal na kabahoo bayaapai
and death shall never approach you.

### Album Title: Ajeet Kaur - Haseya
### Song Title: Ra Ma Da Sa Healing
### Song Number Index: 5 of 6
### Lyrics: [6.0]
### Song Reference: [1.0 @ 0:29:19 - 0:40:10]

Ra Ma Da Sa Sa Say So Hung
Ra Ma Da Sa Sa Say So Hung

: Ra Ma Da Sa Sa Say So Hung
: Ra Ma Da Sa Sa Say So Hung

: Ra Ma Da Sa Sa Say So Hung

: Ra Ma Da Sa Sa Say So Hung
: Ra Ma Da Sa Sa Say So Hung
: Ra Ma Da Sa Sa Say So Hung
: Ra Ma Da Sa Sa Say So Hung

Ra significa Sol y nos conecta con la frecuencia que da energía.
Ma significa Luna, y nos vuelve más receptivos.
Da es la energía de la Tierra, energía que nos enraíza con la Madre, nos da seguridad y fortaleza.
Sa es Infinito y mientras lo cantas tu energía se eleva trayendo la energía sanadora del Universo.

### Album Title: Ajeet Kaur - Haseya
### Song Title: Akaal (feat. Trevor Hall)
### Song Number Index: 4 of 6
### Lyrics: [5.0]
### Song Reference: [1.0 @ 0:20:30 - 0:29:19]

Mother, I feel you under my feet
Father, I hear you
Your heartbeat within me
My spirit flies free

Carry me home
Carry me home

Mother, I feel you under my feet
Father, I hear you
Your heartbeat within me
My spirit flies free

Carry me home
Carry me home
Carry me home

Akaal
Akaal
Akaal (Akaal)
Akaal (Akaal)
Akaal (Akaal)
Akaal (Akaal)
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal)

All my relations, I can hear you loud
The story circles, it comes back around
We chant Akaal and watch them all go through
That door I never saw, I never knew, me and You

We come from earth and water
Straight from that mountain's daughter
Back to the Mother we must journey now
We ride it up up on the wings of sound, hear it now

That holy tree is upside down, branches in the ground
Those roots and sky, open third eye, now pass the nectar 'round
Inside the heart is that eternal space
That melody showed me my own true face, yeah

You chant Akaal, I chant Akaal
All in the One, One in the all
Love on the rise, it never falls
Reach out your hand and I can pull you in
Reach out your hand and I can, pull you in

Akaal (Akaal)
Akaal (Akaal)
Akaal (Akaal)
Akaal (Akaal)
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal) Akaal
(Akaal, Akaal, Akaal) Akaal
Akaal (Akaal)
Akaal (Akaal)
Akaal (Akaal)
Akaal
Akaal

Can you see the faces of the mothers of your past?
Listen, they are calling for your healing (Akaal)
Can you see the faces of the fathers of your past?
Listen (Akaal) they are calling for your healing

(Father I hear you)
Can you see the faces of the mothers of your past?
(Akaal) Listen, they are calling for your healing (Mother I feel you)
Can you see the faces of the fathers of your past?
(Akaal) Listen, they are calling for your healing


[Written @ 0:51]

### Album Title: Ajeet Kaur - Haseya
### Song Title: Kiss the Earth
### Song Number Index: 3 of 6
### Lyrics: [4.0]
### Song Reference: [1.0 @ 0:13:24 - 0:20:30]

Walk quietly my love
Let's kiss this earth we walk upon
With our steps

The moon she shines
In the silence of your mind
Close your eyes

YaYa La Luna
YaYa La Luna

What is the sound
Of the song in your heart
Listen Close

Dance wildly my love
Let's throw our songs into the wind
And let them echo echo

YaYa La Luna
YaYa La Luna


[Written @ 0:46]

### Album Title: Ajeet Kaur - Haseya
### Song Title: Chattra Chakkra
### Song Number Index: 2 of 6
### Lyrics: [3.0]
### Song Reference: [1.0 @ 0:04:32 - 0:13:24]

Chattra Chakkra Vartee,
Chattra Chakkra Bhugatay,
Suyambhav Subhang Sarab Daa Saraab Jugatay.
Dukaalan Pranasee, Diaalang Saroopay, Sadaa Ang Sangay, Abhangang Bibhutaay

Language: Gurmukhi
You are pervading in all the four directions, the Enjoyer in all the four directions.
You are self-illumined, profoundly beautiful, and united with all.
Destroyer of the torments of birth and death, embodiment of mercy.
You are ever within us.
You are the everlasting giver of indestructible power.


[For-Personal-Pronunciation-Support]:
[Pronunciation-Help] - [Actual-Lyrics]
[Chatareh-cha Kara Varatee] - Chattra Chakkra Vartee,
[Chatareh-cha Karum] - Chattra Chakkra

Chattra Chakkra Vartee,
Chattra Chakkra Bhugatay,
Suyambhav Subhang Sarab Daa Saraab Jugatay.

Dukaalan Pranasee, Diaalang Saroopay, Sadaa Ang Sangay, Abhangang Bibhutaay

. .

[Repeat-3-times]

. . 

Mistung uhl kare-na . . Mistung prepareh
Mistung oo-roo-pena . . Mistung oo-noo-peh
Mistung uhn dekena . . Mistung ool-noo-kay
Mistung oon kare-na . . Mistung oon-jahey
Mistung new-kanjena . . Mistung uhn pan-jay
Mistung oon-mamena . . Mistung oor-parve
Mistung uh karmanda . . Mistung oon-kar-ma
Mistung oon-armanda . . Mistung oon-kar-ma

. . 

Mistung uhn joo-peh-na . . Mistung uh doo-pey
Mistung uhn dah-hena . . Mistung uhn dah-hey
Mistung en ee-lena . . Mistung uhn na-ge
Mistung neh-shee-dena . . Mistung ma-gah-hey
Mistung rung-gáhn-jena . . Mistung ra-gáhn-je
Mistung oon-darena . . Mistung oo-kareh
Mistung su-yu-kena . . Mistung oon-noo-kay
Mistung ruh-booktega . . Mistung rey-jook-tay

. . 

[Pronunciation-Help] - [Actual-Lyrics]
[Chatareh-cha Kara Varatee] - Chattra Chakkra Vartee,
[Chatareh-cha Karum] - Chattra Chakkra

Chattra Chakkra Vartee,
Chattra Chakkra Bhugatay,
Suyambhav Subhang Sarab Daa Saraab Jugatay.

Dukaalan Pranasee, Diaalang Saroopay, Sadaa Ang Sangay, Abhangang Bibhutaay

[Repeat-3-times]

. . . 

Mistung uhl kare-na . . Mistung prepareh
Mistung oo-roo-pena . . Mistung oo-noo-peh
Mistung uhn dekena . . Mistung ool-noo-kay
Mistung oon kare-na . . Mistung oon-jahey
Mistung oon-kanjena . . Mistung uhn pan-jay
Mistung oon-mamena . . Mistung oor-parve
Mistung uh karmanda . . Mistung oon-kar-ma
Mistung oon-armanda . . Mistung oon-kar-ma

[Repeat-2-times]
10:48

Mistung uhn joo-peh-na . . Mistung uh dah-pey
Mistung uhn dah-hena . . Mistung uhn dah-hey
Mistung en ee-lena . . Mistung uhn na-ge
Mistung neh-shee-dena . . Mistung ma-gah-hey
Mistung rung-goohn-jena . . Mistung ra-goohn-je
Mistung oon-darena . . Mistung oo-kareh
Mistung su-yu-kena . . Mistung oon-noo-kay
Mistung ruh-booktega . . Mistung rey-jook-tay

[Repeat-1-time]

. . .

[Pronunciation-Help] - [Actual-Lyrics]
[Chatareh-cha Kara Varatee] - Chattra Chakkra Vartee,
[Chatareh-cha Karum] - Chattra Chakkra

Chattra Chakkra Vartee,
Chattra Chakkra Bhugatay,
Suyambhav Subhang Sarab Daa Saraab Jugatay.

Dukaalan Pranasee, Diaalang Saroopay, Sadaa Ang Sangay, Abhangang Bibhutaay

[Repeat-3-times]

. . . 


[Written @ approximately 0:43]

### Album Title: Ajeet Kaur - Haseya
### Song Title: Haseya (feat. Peia)
### Song Number Index: 1 of 6
### Lyrics: [2.0]
### Song Reference: [1.0 @ 0:00:00 - 0:04:32]


I am the river of life
I am the keeper of this dream
Walk with me,
I'll tell you a thousand stories
Long ago given to the earth

These mountains tall
They are my only walls
In this temple where I bow

Haseya
Haseya
Haseya
Haseya

Rise up my sisters, rise up
We are the water, the sacred cup
It's in our hands that all life grows
It's in your dance, it's in your hands,
It's in your love we rise above
It's in your song I hear my soul

So rise up my sisters, rise up
Let us lift each other up
Sing it from your heart and from your soul

Haseya
Haseya
Haseya
Haseya

x2

The sun and moon they shine together
She moves the waters and dances with the heavens
In your eyes I see her eyes
In your eyes I see the giver of this life

Haseya
Haseya
Haseya
Haseya

x2

Haseya



References:

[1.0]
Ajeet Kaur Full Album - Haseya
By Sikh Mantras
https://www.youtube.com/watch?v=_cX70nrrvM4&ab_channel=SikhMantras

[2.0]
Ajeet Kaur feat. Peia - Haseya Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur-feat-peia/haseya-lyrics/

[3.0]
Snatam Kaur - Chattr Chakkr Vartee (Courage) Lyrics
By SongLyrics
http://www.songlyrics.com/snatam-kaur/chattr-chakkr-vartee-courage-lyrics/

[4.0]
Ajeet Kaur - Kiss the Earth Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur/kiss-the-earth-lyrics/

[5.0]
Ajeet Kaur feat. Trevor Hall - Akaal Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur-feat-trevor-hall/akaal-lyrics/

[6.0]
Snatam Kaur - Ra Ma da Sa (Total Healing) Lyrics
By SongLyrics
http://www.songlyrics.com/snatam-kaur/ra-ma-da-sa-total-healing-lyrics/

[7.0]
Ajeet Kaur - Re Man Lullaby Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur/re-man-lullaby-lyrics/


[8.0]
Timeline Annotation for [1.0]
Provided By Aurimar Pessoa

00:01 Haseya (feat. Peia)
04:32 Chattr Chakkr
13:24 Kiss the Earth
20:30 Akaal (feat. Trevor Hall)
29:19 Ra Ma Da Sa Healing
40:10 Re Man Lullaby 






