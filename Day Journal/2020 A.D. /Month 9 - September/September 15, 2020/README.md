# Day Journal Entry - September 15, 2020

[Written @ 21:59]

Notes @ Quotes
"
The light of new realization shines on you today . . 

Howard's legacy is not wrapped up in the money that you will make . . But the challenges that you choose to confront . . As you commence to your past . . Press on with Pride . . and Press on . . With Purpose . . 

God Bless You . . I love you Howard . . Howard Forever . . 
"
-- Chadwick Boseman
[1 @ 33:30 - 34:00]


Notes @ Quotes
"
Graduating Class . . Hear me well on this day . . When you h- . . This day . . when you have reached the hill top . . And you are deciding on . . on . . next jobs . . next steps . . careers . . further education . . 

You would rather find purpose . . than a job . . or a career . . Purpose . . crosses disciplines . . Purpose . . is an essential element of you . . It is the reason you are on the planet at this particular time in history . . 

Your very existence is wrapped up in the things you are here to fulfill . . Whatever you choose for a career path . . remember . . the struggles along the way are only meant to shape you for your purpose . . 
"
-- Chadwick Boseman
[1 @ 31:31 - 32:15]


Notes @ Interesting Video Segment
"
In his elder years . . drawing from his victories [*] . . and his losses . . 

At that moment . . I realized something new . . about this . . the greatness of Ali . . and how he carried his crown . . 

I realized that he was . . transferring . . something to me . . on that day . . He was transferring the spirit of the fighter in me . . He was . . p- . . He was transferring of the fighter to me . . 

He was transferring the spirit . . of the fighter . . to me . 
"
[1 @ 30:30 - 31:06]


Notes @ Quotes
"
Sometimes you need to feel the pain and sting of defeat . . to activate the real passion . . and purpose . . that God pre-destined . . inside of you . .
"
-- Chadwick Boseman
[1 @ 31:06 - 31:15]



[*]
[An alternate probable phrasing of what was said . . ]
[Alternative probable historical records of what was said . .]
[Realities that exist where this was said instead . . ]
"Drawing from his victory swords . . "
"Drawing from his victory hands . . "
"Drawing from his victorious hands . . "




[1]
Chadwick Boseman's Howard University 2018 Commencement Speech
By Howard University
https://www.youtube.com/watch?v=RIHZypMyQ2s

[Written @ 21:47]

Notes @ People Shoutouts

"To the professors that challenged me and struggled against me . . "

Professor Robys Williams
Doc. Singleton
George Eptein
President Swaggard

[1 @ 29:39 - 30:06]


[Written @ 21:42]

"
I planted the seed and Apolis watered it . But God kept growing it . God Kept it growing . 
"
[1 @ 27:08 27:16]


[Written @ 21:26]

Notes @ Newly Learned Words
"Conservatory"
"Conservatory Experience"
[1 @ 18:22 - 18:24]


[Written @ 21:20]

Notes @ People Shoutouts

"The fine arts program had produced . . "

Felicia Reshi
Debbie Allen
Isaiah Washington
Richard Westling
Donnie Hathaway
Roberta Flack
[1 @ 17:50 - 18:02]


[Written @ 21:10]

Notes @ Quotes
"
When completing a long climb . . one first experiences . . dizziness . . disintor- . . disor- . . disorientation . . And shortness of breath . . due to the high altitude . . But once you become accustomed to the climb . . your mind opens up to the . . tranquility of the triumph . . 
"
-- Chadwick Boseman
[1 @ 15:15 - 15:30]


tranquiligy
[Written @ September 15, 2020 @ 21:13]
[I accidentally typed . . "tranquility" . . as "tranquiligy" . . I'm not sure what this word . . "tranquiligy" . . should mean at this time . . ]

tria
[Written @ September 15, 2020 @ 21:14]
[I accidentally typed the word . . "triumph" . . as "tria" . . I'm not sure what this word . . "tria" should mean at this time . . I suppose . . a possible continuation of this typographical error could have been . . "triamph" . . ]


[Written @ 21:01]

Notes @ Newly Learned Historical References

Frazier in the Thrilla in Manila
[1 @ 9:52]



[1]
Chadwick Boseman's Howard University 2018 Commencement Speech
By Howard university
https://www.youtube.com/watch?v=RIHZypMyQ2s



[Written @ 19:58]

Notes @ Newly Learned Organizations
GIVE Nation
https://givenation.world
A World For Kids By Kids

[1 @ 0:53]


[Written @ 19:36]


Notes @ Newly Learned Words

Case Uses
[1 @ 7:59]
[1 @ 15:30]
[1 @ 16:58]
[1 @ 17:05]
["Case Uses" seems similar to "Usecases" . . and is yet another way of thinking about the usecase of "usecase" . . "case use" . . I'm not sure if this was a typographical error in speaking . . a verbal typographical error . . and yet it seems like a useful term to consider learning more about . . ]


Notes @ Newly Created Words
Album Rhythmic Data
[Inspired by: 1 @ 6:24]
[The transcript for this video . . transcribed the spoken . . words to "Album rhythmic data" . . instead of . . "Algorithmic Data" . . ]


[1]
An Introduction to Stablecoins with Alyze Sam
By Girl Gone Crypto
https://www.youtube.com/watch?v=_VOevK1GtIY



