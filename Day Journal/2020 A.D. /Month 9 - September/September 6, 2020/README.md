# Day Journal Entry - September 6, 2020


[Written @ 23:43]

Notes @ Newly Learned Words
SPECT Scan [1 @ 21:59 - 22:33]

Notes @ Newly Discovered Books
Change Your Brain, Change Your Life [1 @ 20:35]


[Written @ 23:21]


Notes @ Newly Learned Words
Reinke's Edema [1 @ 1:20]


Notes @ Newly Discovered Artists
Wayne Coyne [1 @ 10:20]


[1]
Joe Rogan Experience #1531 - Miley Cyrus
By Joe Rogan
https://www.youtube.com/watch?v=D7WUMXKV-FE


[Written @ 22:48 - 22:56]

I like the aesthetics of this video . . 

[1]
Scything lawn-length grass
By ScytheConnection
https://www.youtube.com/watch?v=URJ31uqH07E 


Along the same topic of scythe . . lawn mowing . . I thought this was a cool video as well . . 

[2]
Lawn Mowing with a Scythe
By Scythe Supply
https://www.youtube.com/watch?v=bXreEShJ1OI



[Written @ 22:12 - 22:20]

. . . Well . . I'll go with option . . 1 . . . . 

for now . . and if we have more time later . . then I can also think about . . AI . . and . . record . . the dreams that I have . . in my dream journal . . 

And all of these are quite fun for me . . uhm . . well . . dreams are fun because they are quite practical . . or in my sense . . or in my own interpretation . . is that they can inspire you to think about things idfferently . . or give you new insights . . uhm . . I had some really cool dreams last night . . relating to . . uhm . . new possible alien races . . as well . . as new video recording software . . ideas . . . and so . . you can create videos . . uhm . . using . . different . . maybe. . techniques I suppose . . but those techniques . . uhm . . from how I interpreted . . what I saw . . relate a lot to . . . A.I . . or artificial intelligence . . or digial consciousness algorithms. . . . uhm . . well . . . and so while I was eating today . . eating my pasta . . . just a few minutes ago now . . . I was . . uhm . . thinking of how . . that could be possible on today's computers . . . . uhm . . which would really . . . . . make it a lot easier . . . for people . . . . . from all sorts of backgrounds . . uhm . . educational backgrounds I suppose . . uhm . . I'm not sure . . really anyone . . . uhm . . in general I suppose . . . . which I would be most excited to see what other people would do with this technology . . uhm . . to be able to make videos in the way that . . I saw in my dream . . which is really . . a really . . . . uhm . . . . it's like . . imaginary art . . . . . . . uhm . . like . . . taking what you see in your imagination . . and uhm . . well . . rendering it into a video or something like that . . but that is really not maybe . . a . . uhm . . really helpful way for you to visualize the particular instance that I saw in my dream . . . . and . . uhm . . well . . I'm sorry about that . . 

Uhm . well . . uhm . . . . . . also . . . there were . . . . . uh . . . . right -_- . . . uhm . . . . well . . . . . . . . . . . . . . . . . . . . . . odd . . . . . . . . . . . . . . . . . . I'm recording my dreams . . . . . . . . uhm . . . . . . . well . . . . . . . . . . . . . . . . -_- . . 

hmm . . . . . . . 

. . . 

Well . . it seems like the live stream . . went out . . or cut out . . or . . something happened . . so the live stream is no longer live right now . . 

Well . . I'll restart the live stream . . and get back to programming then . . that's my plan anyway . . 

[Written @ 21:55 - 22:12]

Well . . Let's see . . 

There are a few options on my mind . . 

(1) . . go back to working on ecoin . . on the task relating to gitlab ci integration . . 

(2) . . record my . . thoughts on other matters . . like . . while eating . . I was thinking about . . artificial intelligence . . or . . uhm . . there are other terms for this topic . . like . . digital consciousness . . uhm . . consciousness algorithms relating to digital computers . . uhm . . something like that . . 

(3) . . record my dreams from last night . . in my dream journal . . uhm . . well . . uhm . . that's always fun for me to write about my dreams . . and . . ha . right .

Well . . those are . . 3 options . . . to consider . . 

I like all of them to be honest . . 

But maybe the one that's most important right now . . in uhm . . well . . I think it should be kept as priority for now . . is . . option number 1 . . and so I can finish this . . ecoin project . . for version 1 . . much sooner . . uhm . . . well . . uhm . . . . uhm . . I guess . . I'm trying to be more responsible . . towards this subject . . uhm . in previous live streams . . I would go . . on many tangents . . towards topics . . like . . dream journal recordings . . and . . uhm . . random thoughts that would come up . . uhm . . things relating to math . . or artificial intelligence . . or something like that . . 









Notes @ Newly Created Words
Ine
[Written @ September 6, 2020 @ 22:06]
[I accidentally typed "ine" . . . while . . trying to type the word . . "intelligence" . . as in . . "artificial . . " . . "intelligence" . . something like that . . uhm . . well . . "ine" . . . . right . . . . A . . uh . . . . . well . . it seems to be a greek suffix . . already . . today in its use . . [1] . . . well . . then . . uhm . . . . I was thinking of . . continuing the . . word . . "ine" . . to spell . . a counterpart . . or . . close neighbor of the word . . "intelligence" . . . "inelligence" . . "inellicense" . . "inellisense" . . hmm . . . . . . well . . those are all okay . . I'm not sure . . . . they do remind me of the . . software development word . . "intellisense" . . by Jetbrains . . . . uhm . . well . . I'm not sure what to think . . that's also . . a cool word . . uhm . . that's all . . I'm sorry . ]


Notes @ Newly Created Words
redord
[Written @ September 6, 2020 @ 21:58]
[I accidentally typed . . "red" . . while meaning . . or intending to type the word . . "record" . . and . . well . . a probable . . or possible . . continuation . . of this typographical error . . could have . . could have . . been . . "redord" . . I'm not sure what this . . what this . . what this . . word could mean . . at this time . . "redord" . . hmm . . it's quite . . it's quite . . it's quite like this . . . redording . . it's . . quite . . like this . . like this . . to redord . . to redord . . redundant recording ? . . something like that ? . . I like it ? . . and . . well . . it's an interesting way of . . uhm . . it's an interesting way . . of . . uhm . . it's an interesting . . way . . of . . of . . uhm . . of . . communicating . . "ahh . . it seems like you're redording . . " . . that's . . that's . . that's right . ]


[1]
https://www.dictionary.com/browse/-ine


[Written @ 21:29 - 21:56]

I'm taking a break now to get something to eat . . 

Be right back . . 

. . . 

I spent some time eating pasta . . I also steped outside briefly . . to get some air . . the night sky is dark . . and . . uhm . . well . . it was . . a cool . . evening . . or a cool sight . . visually speaking . . there are houses around . . and . . cars . . near the driving way . . or driving road . . and so my neighbors have cars parked . . near the road . . what a beautiful scene to witness . . 

. . . 

Okay . . well . . 22 minutes . . and 36 seconds . . I haven't used the bathroom yet . . uhm . . I would suppose that taking a bathroom break . . uhm . . would also be useful . . or something like that . . I uh . just don't feel like going right now . . I don't feel like going to the bathroom right now . . 


00:22:36.902

[Written @ 19:14]


I found this helpful web page . . 

https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-services-are-not-for


If you need to have php, node and go available for your script, you should either:

* Choose an existing Docker image that contains all required tools.
* Create your own Docker image, with all the required tools included and use that in your job.

. . 


I'm not sure . . 

Well . . 

It seems like . . 

One way to go about this . . 

. . . 

We need a version of . . Node.js . . and Java installed in our Docker container . . 




[Written @ 15:23]


Problems that I'm running into:

* Running expectation definition measurements with firebase emulator
  * firebase emulator requires a version of java installed to run the emulator
  * gitlab is responding with . . "E: Unable to locate package default-jdk" . . while trying to install . . java . . using . . "apt-get install default-jdk"
  * 


Problem Solving Strategies:

* Research the topic of interest
* 


Research the topic of interest

* Research Phrases for the topic "Java and Node.js Gitlab CI"
  * Java and Node.js Gitlab CI
  * How to install both java and node.js in gitlab
  * gitlab ci install multiple images
  * Multi-image gitlab ci installation
  * 

* Things that I expect to find during the research process
  * A way to use "apt-get" to install a version of java that is available . . for example . . installing "default-jdk" . . isn't working for some reason . . but maybe there is another version . . of java that will work . . or another . . package name . . that I could find . . that works . . to install the version of java that works with the firebase emulator
  * 







Notes @ Newly Created Words
toip
[Written @ September 6, 2020 @ 15:30 - 15:31]
[I accidentally typed the word "topic" . . . as "toip" . . I'm not sure what this word . . "toip" . . should mean at this time . . ]


[Written @ 15:16 - 15:22]


Time spent working . . and time spent . . away from the computer . . taking a nap and eating . . 

50 minutes . . and 8 seconds . . 

30 minutes . . and 22 seconds . . 

respectively . . 

#1	00:50:08.529	Pause [Time spent working . ]
#2	00:00:00.840	Split
#3	00:30:22.663	Pause [Time spent napping and eating . ]


. . . . 


Time spent working : 
50 minutes . . and 8 seconds . . 

Time spent napping and eating breakfast :
30 minutes . . and 22 seconds . . 


[Written @ 15:10]


I spent some time eating . . 

I spent . . nearly . . 30 minutes . . and 22 seconds . . away from the computer . . 

Some of that time was spent eating (2 slices of pizza) . . and drinking some 'white grape fruit juice' . . . . 

Before eating . . I spent nearly 20 minutes . . or so . . 20 or 22 minutes . . something like that . . something like this . . was spent . . taking a nap . . on the couch . . . . uhm . . I slept on the couch . . in my living room . . and . . uh . . well . . I rested uhm . . my . . uhm . . well . . I closed my eyes . . and . . relaxed . . my body . . to lay on the sofa . . 

00:30:22.663



Notes @ Newly Created Words
sofar
[Written @ September 6, 2020 @ 15:13]
[I accidentally typed the word . . "sofar" . . . . instead of typing the originally . . intended . . word . . "sofa" . . . . . . and so . . . "sofar" . . is a typographical error . . . while . . I was intending to type the word . . "sofa" . . . . . . . In any case . . I'm not sure what this word . . "sofar" . . should mean at this time . . "sofar" . . . . "sofar" . . "sofar" . . odd . . this is a really interesting word . . "sofar" . . I'm not sure . . uhm . . . it seems like . . it is quite useful in conversation . . "this is what I've done . . so far . . " . . or . . "this is what I've done . . sofar . . " . . ]



[Written @ 14:36 - 14:36]

I'm going to go get something to eat . . 

Be right back . . 


