
# Day Journal Entry - September 19, 2020


[Written @ 23:25]



[Written @ 21:15]

Things that I wrote in my notebook journal:

"

[These are the things that I initially wrote to understand why I was feeling anxious or nervous before today's live stream]

* Not sure how to proceed
* I would like to work on Ecoin
* I would like to share my dream Journal dialog
* I should maybe plan a list of things to say so I can be on track to complete a list of certain topics.
* My body is feeling a lot of different emotions
* I'm not always sure how to express the thoughts. And well I think I am thinking way too much about things.

"

One of the things that conflicted me the most was my thoughts or desires to (1) work on the ecoin project and (2) write dream journal content . . which are recordings of what I experience in my dream when I sleep in the day time or night time . . 

I think the source of my conflict was that I wouldn't be able to do these two things at the same time . . uhm well . . I say that this is possibly a source of the conflict but I'm not sure . . . really . . I didn't know . . or I didn't find out uh . . that maybe this was the case . . until I wrote a list of the times of the day that are left . . or were left . . before starting the live stream . . 

18:50 [this was the time I wrote the first time measurement]

18:50
19:00 [Planning and Task Relations] [failed :O - didn't start the stream by this time]
20:00 [Planning and Task Relations] [success :D]
21:00 [Planning and Task Relations] [work-in-progress :O]
22:00 [Dream Journal Entry]
23:00 [Dream Journal Entry]
24:00 [Dream Journal Entry]


I wrote a list of the hours remaining for the day . . today . . and planned a list of things that I would do for this time period . . 

. . 

I'm still working on planning . . at this time .

Well . . 

Uh . 

There are 30 minutes left for this hour's task . . and I had planned to work on the ecoin project README.md file . . to update the appearance to another aesthetic . . uh the tables that were worked on yesterday . . are really tall . . and consume a lot of vertical space on mobile devices . . which gives a strange appearance to me . . uh . well . in terms of how I think maybe another way could look in another way to satisfy other thoughts . . like . . being . . dense . . and being in a short list . . but the list can be extended with a lot more features if that is necessary . . and still be quite . . friendly to digest a lot of the feature items together in a single view . . without scrolling so much to see the next feature item . . 



[Written @ 20:08 - 21:15]

I've started the live stream now . . a few minutes ago . Today's episode is Ecoin #308 . . For September 19, 2020

I haven't regularly done this in previous live streams . . 

Uhm . . I haven't . . previously . . started with an agenda . . for what I would like to accomplish . . in the live stream . . Uhm . . Today . . Before starting this live stream . . I wrote a few notes on things that I'm thinking about . . And things that I've been considering . . 

Uh . well . . Let me think . . on the one hand . . I'm not sure whether to . . copy-paste what I've written from my paper-pencil notebook writings . . uh . . that might be useful for . . historical accuracy or archival purposes . . uh . . copying . . word-for-word . . what was written in the spiral notebook of 180 sheets of paper [as listed on the cover of the notebook]

. . Well . . 

Uh . . I should really have more time-management skills and so maybe this is good practice for me to have an agenda . . and so for example . . I can reach my goal of having the Ecoin website available by September 30, 2020 . . 

Uh . . Right . Uh . I'm sorry . I'm uh . an adventurist . . and . . don't always mind the tangents . . that I go on . . like . . writing the newly discovered words while typing . . uh . typing . . typographical errors I suppose . . well . . in any case . . maybe . . by having an agenda . . or a list of things that I would like to accomplish . . available . . at the beginning of the live stream . . uh . . . maybe . . I . . . . uh . well . . (1) this is a useful . . uh . . device for you . . the audience . . who would like to . . maybe . . realize . . what the intended . . future is . . or the intended result is . . before . . it's developed . . or . . before . . it's . uh . . uh . resulting . . achievement . . or something like that . . 

(2) it's also useful for . . well . . teams . uh . when you're working with . . other people on the same project . . having a plan . . or a map . . of . . where . . people . . uh . can . . see the various goals . . of the project . . or the various . . destinations . . that . . the . . uh . project would like to satisfy . . or fulfill . . uh . maybe by some . . expected . . date . . or deadline . . or . . festival hour . . or calendar timeline consideration measurement dates . . right . uh . 

(3) . . 


Summary . . 

(1) You as an audience member are expectant on certain measurements being taken throughout the video progression (such as the progress in completing a task)

(2) Team members who would like to contribute are able to coordinate on the responsibilities of each member

(3) I can learn more about time management skills 


-- 

Amount of time spent writing this group of words . . . 

21 minutes . . and 40 seconds . . to write . . 

00:21:40.686
00:21:40.686


--

The Purpose of Writing this group of words . . . I'd like to learn more about time management skills . . so I can finish this project . . early . . but also . . while practicing . . team-based tactics . . like . . writing notes about . . what are the remaining tasks left to complete . . because I imagine those are going to be useful in case anyone is interested in contributing . . uh . . or . . uh . . well . . working together . . in . uh . some capacity . . I guess . .

Well . . uh . . Also another purpose in writing this group of words . . or these paragraphs . . of text . . is to relate more to how teams are working together and managing their roles . . and their time to complete various uh . . project goals . . and . . I'm . . uh . well . . 

When I started the live stream . . or before I started the live stream . . I spent . . nearly . . 1 hour . . sitting . . on the couch . . in silence . . uhm . . with my eyes closed . . or uh . well .  sometimes I opened my eyes also . . but . I was really feeling nervous about getting on the live stream today . . ANd I've felt nervous . . for the past few days . . or for the past few live streams now . . 



[Written @ 1:46 - 1:48]


Approximate amount of time . . on live stream . . for September 18, 2020 and September 19, 2020 . . . . this is the live stream for Ecoin #307 . . 


02:07:08.088
02:07:08.088


#1
02:07:08.088
Pause



[Written @ 1:26]

Notes @ Newly Created Words

cursor-blinking-effect
[Written @ September 19, 2020 @ 1:26]
[This would be a way to communicate a thought that's a work in progress . . . it's still a prototype statement word and yet it came to mind when thinking about this topic . . probably there are better or more standard ways to express or relate to this topic . . ]


[Written @ 1:08]

The more I think about this type of typing . . 

uhm . . hmm . . 

whe t probabl event



a type of typing (whe)ere (t)he message is hidden by a (probabl)e scheme of (event)s . . . . 

. . . this reminds me of a type of . . short hand writing scheme . . that . . Robert F. Butts applied to communicate the Seth sessions as channeled by Jane Roberts

Well . . I'm only reminded by this way to take notes . . which is a short-hand method of communicating information when not all the information is readily visible .



[Written @ 0:55]

Notes @ Unique Phrases

"
Yea . . we have no shortage of eager talent
"
[1 @ 50:59 - 51:03]


[1]
Cardano Public Roadmap | First Review
By Digital Fortress
https://www.youtube.com/watch?v=iyqmfM6X8Fc



[Written @ 0:46]

Notes @ Newly Discovered Words

puzl
[Written @ September 19, 2020 @ 0:46]
[I typed the word . . "puzl" . . when I had originally intended to type the word . . "puzzled" . . I'm not sure . . uh . . my emotions are really . . a work in progress in terms of what a way of thinking about . . certain topics . . I'm not sure . . uh . puzl . uh . . I'm not sure what this word . . this would . . should mean at this time . . . . this would . . is a probable . . perse]

Notes @ Newly Discovered Words

perse
[Written @ September 19, 2020 @ 1:00]
[A typographical error of "perspective" . . in a way . . the way this word was written . . uh . . well . . I had the word . . "perspective" in mind . . in terms of what I would have liked to type . . but then . . the word . . "perse" uh . . well . . appeared . . uh . . -_- in a sort of intuitional stopping routine . . for example . . uh . . when I type on the keyboard . . uh . . my fingers . . have a sense of . . uh . . the pressure . . of . uh . . whether or not I should apply pressure on the key on the keyboard . . and how much uh . . ]


reis
[Written @ September 19, 2020 @ 1:03]
[A typographical error of . . "resistance" . . uh . . Well . . . . I had originally intended to type the word . . "resistance" . . and . . uh . . well . . my uh . hands . . are typing . . and yet . . uh . . there are certai . . pauses . . that my hand will take . . where . . my certainty on whether or not to apply a certain character is . . resisted and the communication . . is not tracked by the computer and you have to guess at the probable communication message [which isn't always the original message that was thought of . . ] . . that you have to guess at . . what was communicated . . uh  pressure on the keyboard was not applied as expected . . is that understood ? ]


keybaord
[Written @ September 19, 2020 @ approximately 1:17]
[I accidentally typed the word . . "keybaord" . . and . . . . the originally intended word to type was . . "keyboard" . . I'm not sure what this word . . "keybaord" . . should mean at this time . . I'm sorry . . ]


koeboard
[Written @ September 19, 2020 @ 1:18]
[I accidentally typed "koe" . . and a possible continuation . . of this typographical error is . . "koeboard" . . I'm not really sure what this word should mean at this time . . I'm sorry . ]

Notes @ Newly Discovered Words

wuld
[Written @ September 19, 2020 @ 0:58]
[A typographical error of "would" . . hmm . . for some reason . . when I type . . on the keyboard . . my uh . . fingers . . pause . . uh . . in a certain . . uh . . pausing sybolic way such that . uh . maybe . uh . I'm considering a new combination of the character sequences or not really sure how to continue the sequence or something like that . uh . in case for example . . I would like to remove the word . . or change the word . . or replace the word . . uh . . before typing it . . uh . . and . so . . even if I think I would like to use one word . . uh . . maybe . . another word . . another possible word entity . sequence . . or another possible word . . sequence . . proceeds to uh . . maybe . . uh . . make me more sensible towards typing a sentence like that . uh . even if I'm not sure . hmm . . really writing words really seems uh more uh . . probabilistic or something I'm thinking . . and uh . well . . uh . that reminds me in a message that Seth says in their book . . "Seth Speaks" . . in relation to . . how . . humans . . uh . . speak . . or . . people . . speak . . and . . they don't necessarily . . know . . the how . . or . . what . . or . . uh . . how . . or what . . they will say . . directly . . word for word . . but they . . are . . confident . . in their ability . . to be able to finish the sentence . . uh . and so . . speaking . . a sentence . . uh . . even if you're not sure what to say . . uh . . you . . uh . . I guess . . "trust" . . could be a word . . or . . you . . "rely" . . on your feelings . . or your confidence . . in yourself . . to . . say words . . to . . or . . that . . uh . . to . . reflect the meaning that you are trying to express . . or trying to say . . and so . . even if you don't know the word-to-word paragraph or sentence of what you'll say . . before you say it . . you are trusting in your ability . . to . . translate . . uh . well . . in your . . body's ability . . to automatically . . translate the feelings you have . . into . . words . . and . . spoken . . or . . written uh . written . . or . . spoken . . vocal sentences . . uh . . uh . . uh . . uh . . well . . it's of interest perhaps to talk about something like this topic . . uh . uh . the topic of . . uh . . speaking . uh . . I suppose . . uh . you can go back in your speech . . to . . re-communicate something you said . . which can be an effective way . . to . . re-establish . . the cognitive . . pathways . . that are . . enlightened . . or ensparked . . or . . encouraged . . to . . communicate . . certain . . social . . communications . . or certain social . . uh . . social associations . . [a few minutes later after researching the book . . ] . . Well . . the book is "Dreams, "Evolution" and Value Fulfillment Volume 1" by Seth, Jane Roberts and Robert Butts . The original book I was talking about in this paragraph . . was "Seth Speaks" . . which . . was a mistake . . in my effort at referencing the proper or . . the correct . . book . . relating to where this topic . . is mentioned . . . . this topic . . appears to be mentioned . . uh . . this topic . . of . . uh . . "having the faith that you'll complete a sentence when you start it . . if you don't necessarily know what you will say word-to-word . . before you start speaking . . " . . in relation to this topic . . well . . this topic . . is related on . . page . . 194 of "Dreams "Evolution" and Value Fulfillment Volume 1" . . ]


Notes @ Newly Created Words
Dreamd
[Written @ September 19, 2020 @ 1:39]
[I accidentally typed the word . . "Dreamd" . . instead of the word . . "Dreams" . . I'm not sure . . what this word . . "Dreamd" . . means at this time . . ]


[Written @ 0:30 - 0:31]

Notes @ Newly Discovered Word Homophones

Overwriting, Overriding


Notes @ Newly Discovered Word Homophones

Counsil, Counsel


[Written @ 0:19 - 0:30]

Notes @ Newly Created Words
Uptdate
[Written @ September 19, 2020 @ 0:19]
[I accidentally typed . . "Upt" . . and . . a possible continuation of this typographical error . . to . . spell the . . originally . . intended word . . "Update" . . a possible . . continuation . . of the typographical error . . of . . "Upt" . . is . . "Uptdate" . . uh . . well . . it's not really so differently spelled from "Update" . . and . . Uh . . . maybe . . it's not a big t]


Notes @ Newly Created Words
t
[Written @ September 19, 2020 @ 0:22]
[I accidentally typed the letter . . "t" . . instead of . . "d" . . while . . trying to type the word . . "deal" . . well . . this mistake . . led me to think . . . . Hmm . . what is the possible continuation . . for the . . typographical error . . of . . -_-- . . . . "t" . . well . . "teal" . . is one possible continuation . . and yet . . this word . . "teal" . . is already a word . . uh . that . . I'm familiar with . . or that . . uh . . . is already defined in a library dictionary uh . from my memory perspective on what I think I remember about the standard library dictionary . . but maybe my memory needs to be updated . . not sure -_o . . .  hmm . . what does it mean for a single . . letter . . to also be a word . . . . hmm . . not sure . . well . . in any case . . what does the word . . t . . mean . . I'm not sure at this moment . . hmm . . well . . this record of this newly created word . . should help a future verion of people . . to consider . . . ascribing . . meaning . . to this word . . t . . that is otherwise uh . maybe not always considered a word . . ]


[Written @ 0:08 - 0:09]


Notes @ Newly Created Words
incorportate
[Written @ September 19, 2020 @ 0:09]
[I accidentally typed "incorportate" . . instead of . . "incorporate" . . I'm not sure what this word . . "incorportate" . . should mean at this time . . ]
