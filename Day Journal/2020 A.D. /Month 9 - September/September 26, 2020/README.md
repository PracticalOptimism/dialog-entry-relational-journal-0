

# Day Journal Entry - September 26, 2020


[Written @ 23:50 - September 27, 2020 @ 0:12]

Okay . . well . . my day today . . at some point after I woke up . . I was . . sitting on the couch . . that I'm sitting on now as I type my notes . . and . . I was . . uh . . thinking . . or day dreaming . . or something like that . . uh . . [isn't it funny how day dreams work . . your body can be in one physical location . . and your mind is maybe in another place of uh . . I don't know . . a cloud of thoughts ? . . a cloud of emotions ? . . a cloud of feelings ? . . uh . . for example . . uh . . do you think . . that maybe . . people can visit one another in these dream clouds ? . . for example . . if I am sitting in the lounge of the topic of artifical intelligence . . do you think that other . . uh . . people or . . other thinkers are also able to communicate their presence . . uh . . since they . . are . . for example . . on another uh . . part of the planet . . and also thinking about the same topic ? . . do you think there are theoretical particles that make communication possible between people that share the same thoughts ? . . or maybe it isn't even time-dependent and so maybe someone had the same idea that I had . . or there are common themes about the idea . . uh . . does that mean anything to see their face or is it something else ? . . would you see a trace of their presence ? . . maybe a name ? . . Andrew Ng . . Ian Goodfellow . . Siraj Raval . . uh . . Rodney Brooks . . uh . . Fei-Fei Li . . . . uh . . this is a thought that I considered today . . but didn't delve so deeply like I wrote here . . it only brushed my chin . . the idea that . . I don't remember what my surrouding looks like . . it's like . . my body is automated to walk around . . and get food from the kitchen . . and I don't need to worry . . why my mind is in another sort of . . uh . . emotional space . . a space of emotions . . my thoughts and feelings on artificial intelligence . . the direction of thoughts that are taking place . . my own personal perspective . . opinions . . uh . . uh . . thoughts and feelings . . emotions . . uh . . I'm not always sure where to differentiate these topics . . logical reasoning thoughts like deduction or generalization are also feeling like emotional constructs directed in certain ways . . uh . . I'm not sure . . uh . . it is maybe related to uh the intuition uh maybe one build-_- I'm really only hypothesizing and hope I'm not sounding so macho or that I know something . . uh . . but thinking and reasoning area also feeling like they are based in intuition maybe . . but maybe they are really different . . uh . . intuition . . emotion . . feelings . . uh . . these seem related to thinking . . and reasoning and logic or associative reasoning and planning and characterizing -_- but I'm not really sure . . I'm not really uh thinking that well --_-]

-__--_-_-_---__-

I'm so sorry this has taken so long to type my notes but I have only the background which is uh . . maybe part of the story . . 

Uh . . well -_- . 

right . I was trying to get to the point that I like to think about mathematics and artificial intellignece but was about to tell a story of how that was practicalized in my daily life today -_- . maybe now I don't need to do that . 

well . . the idea of . . "isometric graphs" . . game to mind . . or "isometric grids" . . I thought to myself . . "wow . . isometric graphs are so cool . . " . . [I think maybe the sound is a sort of . . other frequency or pitch . . uh . . maybe it's like . . uh . . sonar . . or . . uh . . below sonar . . infra-sound . . uh . . or another range of sound . . it seems uh . . maybe it could be a pre-recording ? -_- sometimes I uh . . it could be like another uh . . stream of live audio . . uh . . and then I associate that as my personal thought or personal reasoning complex -_- . . -_- . . I've heard other voices speak . . that sound uh . . some of the people sound like human male organisms . . and some of the people sound like human female organisms . . mostly the voice that I hear . . that is my quote-un-quote self . . is unique . . and I don't normally hear other persons speaking through that voice . . but I've heard . . uh . . voices of my mother for example . . and voices of other people that I've met in my life . . resonate . . through my head . . like they call my name . . or they say . . good job or something . . or uh . . "I love you so much" . . or something like that . . uh . . and those voices are usually appearing in another frequency or sound pitch or something like that . . I'm not really sure . . the feeling is how . . I differentiate between the sounds . . ]


subvolv
subvolca
sub-vaol
[Written @ September 27, 2020 @ 0:06]
[sub-vocal mis-characteriszation]


[Written @ 23:29 - 23:50]

Okay . . well . . my day 
. . uh . . I think those are a lot of new words but I would really like to get to my notes soon . . and so . . I will try to watch my behavior in this respect . . 

I'm sorry . . 

Notes @ Newly Created Words
starteved
[Written @ September 26, 2020 @ 23:43]
[I accidentally typed the word . . "starteved" . . instead of the word . . "started" . . I'm not sure what this word . . "starteved" . . should mean at this time . . although . . the word . . "starteved" . . syntactically . . aesthetically . . in the way the word . . looks . . in how the characters are organized around one another . . gives me the feeling of . . "star" . . and "elves" . . or . . "star" . . and . . "eve" . . hmm . . in general . . in pricinple . . these words are quite beautiful to look at . . and even the word . . "eve" . . reminds me of the word . . "eye" in how it appears from a peripheral glance or maybe even from a visual-associative glander at the picture image of the text . . "eye" "eve" . . "eye" relating to beauty . . eyes and beauty are also psychologically associated in my mind . . and so . . beauty <==> eyes <==> eve <==> starteved . . and so starteved is a beautiful word :O :D ]


Setpe
[Written @ September 26, 2020 @ 23:30]
[I accidentally typed . . "September" . . as . . "Setpe" . . And now I'm not sure what this word . . "Setpe" . . should mean at this time . . ]

thist
[Written @ September 26, 2020 @ approximately 23:41]
[I accidentally typed the word . . "thist" . . instead of the originally intended . . "this" . . "Ohh noo" . . that uh . . maybe is a good uh meaning of this word . . "Ohh noo" . . like . . "thist" . . I forgot my house keys in the car . . thist . . ]

insended
[Written @ September 26, 2020 @ approximately 23:37]
[I accidentally typed the word "ins" . . or uh . . the character sequence . . uh . . "ins" . . instead of . . "int" . . uh . . my goal was to type the word . . "intended" . . and . . I made a mistake . . and now . . the hypothetical continuation of my error . . of my practically perceived error . . of seeing the word sequence "ins" . . seems to introduce the hypothetical continuation for the purpose of reaching the word "intended" . . to be something like . . "insended" . . and now . . this hypothesis is really a guess at finding meaning in a new sequence of word character schemas that intends to rapture the existence of inconsiderable corner cases of logic and rhythm schemes that would other . . uh . . otherwise talk into the rhythm of a non-existence that is spoken about only in story books . . "insended" . . in a deep elven town in the riverhalls of mud and systematic fashion . . there are an insended group of people that cannot otherwise be known]

bhy
[Written @ September 26, 2020 @ 23:39]
[Down below . . "Bhy" . . that is the meaning of this new word . . this word was formed from a typographical error . . of "by" . . "down below" . . is an intuitive reading that came to mind . . I heard the phrase "down below" in my head . . while starting the paragraph for the explanation of the discovery of this newly found or newly created word]

uther
[Written @ September 26, 2020 @ approximately 23:36]
[I accidentally typed the word character "u" . . when I meant to type the word character . . "o" . . and this . . character "u" . . really shoveled me down a hole where I'm not always the one to know that the sequence of observed mystakes are intended or otherwise characterized by a golden god that smiles and attempts to make laughter a common theme around the family house hold structure]


[Written @ 23:20]

Today . . I spent a lot of time . . thinking about uh . . mathematics and artificial intelligence . . and things that related to these topics . . 

Uh . . well . . uh  . . this is my day journal . . and so I want to feel free to write . . without being so constricted . . or constrained . . maybe . . as I could be . . by . uh . . social . . relations . . with . . uh . . how people uh . . may or may not perceive my day journal entry . . dialogue . . for today . . uh . . I'm not exactly sure . . what that means . . or how I will achieve that feeling of freedom . . uh . . -_- . . uh . . I guess I don't want to feel like I'm wasting my time by writing . . my feelings . . in an emotional way . . and not so formal . . like for example . . a research paper could be more formal in how these ideas are presented . . and uh . . well . . my notes are . . uh . . more . . uh . . free style . . and so . . uh . . I can feel like . . I uh . . don't have any obligations for where my thoughts end up . . or . . if I go on tangents or anything like that . . uh . . the courseware mentality of the individuals who are watching or perceiving this digital diagram of conversational script can understand that their thought course constructs can be deshoveled by mud in the making this dialogue journal entry for today on the September of 26th's of the 2000's year in the 20'th beginning of the decade era season syntax for the characterized prescribed doctrines of unordinated symptoms of disease -_- -_- uh . . uh . . . 2020 . . september 26th yet there are other particular ways of considering this date that would be observed in alexander smith rogers would have been born to coordinate the dates of alexander 12, 2103 dialogues in the ordinary scheme of how we identify human organizational structures of the alexandrian calendar year and alexandrian calendar year is a scheme that Alexander themselves is the originator of in the alternate history events that we are ignoring if we don't look at the plain facts that alex

. . . 

I'm sorry . . that was rubbish . . and now I feel a lot better . . maybe this will help settle my nerves


[Written @ 23:13]

Notes @ Newly Created Words

suppoer
[Written @ September 26, 2020 @ 23:14]
[I accidentally typed the word "suppoer" . . while meaning to type the word . . "suppose" . . and now I'm not sure what this word . . "suppoer" . . should mean at this time . . I'm sorry . . but at least writing this discovery of a possible . . theoretically definable word . . could mean . . new meanings to be created from word structures that are . . perhaps . . underrepresented . . in the world . . of . . meaningful word structures . . on the planet as we know it today . . already . . in 2020 A.D. . . as this word is being discovered by myself . . Jon Ide at this time . .]


[Written @ 17:10]


Notes @ Newly Discovered Websites

therepublicangirl.com
https://www.therepublicangirl.com/
[1]

Notes @ Personal Notes

There's a bug on the website "therepublicangirl.com" . . when I visited the website today . . uh . . the time is now . . 17:15 . . as I write this message . . and my web browser . . is . . pausing . . or . . having . . an issue . . with an infinite loop or something [I would hypothesize] . . my web browser . . is aksing me to . . "wait" . . or . . "stop the page" . . and so . . I'm thinking there is an error at this time . . I'm using the browser . . "Brave" . . 

Brave Browser
Version 1.14.81 Chromium: 85.0.4183.102 (Official Build) (64-bit)



[1]
🔴 Watch LIVE: President Trump Holds Campaign Event in Middletown, PA 9/26/20
By Right Side Broadcasting Network
https://www.youtube.com/watch?v=krJv1dSo5Ls

