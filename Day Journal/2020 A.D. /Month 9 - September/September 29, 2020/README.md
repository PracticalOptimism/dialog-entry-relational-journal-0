
# Day Journal Entry - September 29, 2020

[Written @ 19:09 - 19:10]

Approximate amount of time spent programming on ecoin on today's live stream . . Ecoin #319

07:01:40

Approximate amount of time spent relaxing or taking a break on today's live stream . . Ecoin #319

00:43:53.630

#1
00:43:53.630
Pause


[Written @ 11:54]

Live Stream Checklist

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - "Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha)" By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - Ru's Piano Ru味春捲 [YouTube Channel] https://www.youtube.com/c/RusPiano/playlists
  - "Ajeet Kaur Full Album - Haseya" By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4

Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
  - (1) YouTube LiveStream Chat
  - (2) YouTube + Firefox Web Browser (Picture-in-Picture Mode)
  - (3) iTerm
  - (4) Firefox Web Browser + https://www.timeanddate.com/
  - (5) Brave, Firefox, Tor Browser
  - (6) Visual Studio Code
  - (7) Express VPN
  - (8) Visual Studio Code . Previously: "Notes" on my macbook

Periodic Tasks

* [x] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)


Salutations

* [x] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [x] Talk about the possibilities for the next live stream




[Written @ 2:25]

Poem
Notes @ Poetry

Parted in towards the way of the world
I just don't have a cowl

I wrote this poem . . while inspired by . . [1 @ 10:58 - 11:17]


[Written @ 1:41]


Notes @ Newly Learned Fashion

Flannel [1 @ 7:01]

Cowl [1 @ 7:03]


[1]
Days Of My Life... And Love (Story 48)
By Isabel Paige
https://www.youtube.com/watch?v=lGifGj_FPps



[Written @ 1:06]

Notes @ Newly Discovered Books

Freedom From the Known, by Jiddu Krishnamurti [1]

The Tao of Pooh, Benjamin Hoff [2]

Psycho-cybernetics, Maxwell Maltz [3]



[1]
Audiobook : Freedom From The Known by Jiddu Krishnamurti
By Krishnamurti's Teachings
https://www.youtube.com/watch?v=pOowwi8xYUA

[2]
The Tao of Pooh
https://www.youtube.com/watch?v=pSzjWtR40Q0&ab_channel=JohnnyReed

[3]
Psycho-cybernetics (the best self-help book ever)
By Idris
https://www.youtube.com/watch?v=1TLzQf21V2A


Notes @ Videos To Watch

[1]
Why does sex play such an important part in life? | J. Krishnamurti
By J. Krishnamurti - Official Channel
https://www.youtube.com/watch?v=lrKj3lRXeCM

[2]
Know and Understand your 6 Core Needs
By The Yoga Life
https://www.youtube.com/watch?v=w3mQNE7I9c4



Notes @ Quotes

"It is no sign of good health to be well adapted to a profoundly sick society"
- J. Krishnamurti

[1 @ Comment by Debbie Decker]



[1]
Audiobook : Freedom From The Known by Jiddu Krishnamurti
By Krishnamurti's Teachings
https://www.youtube.com/watch?v=pOowwi8xYUA


[Written @ 0:51]


Thank you for watching

plans for the next live stream: Ecoin #319

* start the live stream at 0:00 on September 29, 2020
  - I succeeded at this because . . we're still on the live stream . . Ecoin #18 . . however . . I failed in starting the live stream for . . Ecoin #319 :/
* complete the live stream checklist . .


* cache the account to transaction map item for an account as a parameterized option when retrieving an account . .
* work on the "apply transaction list" algorithm and expectation definitions . . 


[Not right away . . I need to spend more time working . . on the codebase . . ]
* write the dream journal entry on transparent phones
* maybe write some other dreams ? . . maybe

. . . 

Thank you for watching :) You're the best . 


