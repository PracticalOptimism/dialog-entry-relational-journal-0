
# Day Journal Entry - September 5, 2020




[Written @ 21:39 - 21:47]

I'll be restarting the live stream for today . . 

I'll get some food to eat . . 

Oh right . . when I came home earlier today . . from after running at the park . . I spent a few minutes in the shower . . . and so . . . the time . . uhm . . 2 hours . . and 35 minutes . . also includes time that I spent . . taking a shower . . . after uhm . . running at the park . . . uhm . . . picking up trash on the side of the . . uhm . . sidewalk . . while running . . 

Uhm . . . I ran to the park . . . uhm . . which is maybe . . . 1 mile from where I live . . . 1600 meters . . 

uh . . well . . then I walked back from the park . . and so . . . . that's uh . . . well . . some time . . . . accounted for . . . in the 2 hour . . 35 minute . . estimation . . 

I also spent . . about . . 10 to 20 minutes in the shower . . . . which wasn't timed . . . but that's normally how long I stay in the shower . . uhm . . . and . . uhm . . well . . . that measurement is by intuition . . . uhm . . and not that I timed it with a physical clock or something like that . . 

In any case . . Uhm . . . . . . Well . . I think . . uhm . . yea . . that's all for now then . . I'm not sure what else to share . . Uhm . . I heated some . . . pasta . . . . uhm . . long . . noodle-like pasta . . . . or spaghetti . . . in the microwave . . for a minute and 30 seconds . . . and so that time is also included in the . . . . 2 hour and 35 minute estimation . . 

Also . . that's all for today . . I'll see you in the next live stream episode . . . . . . #293 . . . See you later . 


[Written @ 21:32 - 21:38]

I'm back from running . . I'm sorry I forgot to turn the timer on . . . the stopwatch rather . . I forgot to turn it on . . uhm . . . before I left . . . . . Uhm . . well . . Uhm . . 

Let's see . . 

The current time is . . . 21:34 . . 

I left at .  . . . 18:59 . . 

And so . . . 

21 - 18 = 3
34 - 59 = -25

. . . 

and so . . 

3 hours + -25 minutes . . 

uhm . . 

That's . . 2 hours . . and 35 minutes . . . 

Uhm . . Well . . that seems about right . . 

I was running at the park . . for nearly . . 2 hours and 35 minutes today . . 

[sorry if this a miscalculation . . I'm not sure if my method of calculation is . . appropriate . . maybe there is another way to calculate this time . . . or amount of time spent . . . while outside . . . and so I'm sorry . .  please let me know . . ]

[Written @ 18:59]

I'm going running now . . be right back . . 

[Written @ 18:57 - 18:59]

Notes @ Newly Created Words
Discovere
[Written @ September 5, 2020 @ 18:57]
[I accidentally spelled the word "discovere" . . . . with an "e" . . . . at the end of the word . . . . Instead of . . "discover" . . . . . I'm not sure what this word should mean at this time . . I'm sorry . . ]

[Written @ 18:53 - 18:57]

Approximate amount of time spent getting ready . . . . to go run . . . [I brushed my teeth, I used the toilet (spent about 5 minutes . . .) . . I drank some apple juice [we uhm . . didn't have water bottles in the fridge :( . . . sad face emoji . . no water bottle friend to take with me on my run . . ] . . . I found a hat to wear (spent 2 or 3 minutes to search for . . and finally discovered . . ) . . . had to find socks . . [spent about 30 seconds to find and discover . . .]]

I'm ready to go running now . 

. . 20 minutes . . . 38 seconds . . . 

00:20:38.535
00:20:38.535


#1	00:20:38.535	Pause


[Written @ 18:27 - 18:34]

Running at the park . Be right back . 

Please enjoy Jacque Fresco and their teaching on "The Venus Project" . . 


Thank you for watching . . 


[Written @ 18:06 - 18:27]

I'm sorry for another delay in Ecoin development . . I'll be right back . . I'll be running in the park . . 

Uhm . . I'm not sure whether to keep the live stream open . . . . . . Uhm . . I've done that in the past . . 

In previous live streams . . . . . I guess I could do that again here . . and keep a log for how long I was outside . . running . . And I suppose you can enjoy the content . . that's running in the background . . or running in the foreground . . to represent things that I'm thinking about . . uhm . . like The Venus Project . . uhm . . thinking about . . in terms of . . I guess . . uhm . . things that I like to think about these days . . uhm . . yea . . well . . the more education peopole have around the alterative . . or . . . an . . alternative . . to the topic of a money-based society . . that is a very meaningful concept . . 

Uhm . . in general . . I try to stay positive . . in my day to day life . . a lot of what I watch . . on YouTube for example . . which consumes a large part of my life . . . for example . . besides live streaming . . I'm mostly watching people . . on YouTube . . and . . seeing how their lives are going . . how they're doing in their lives . . 

uhm . . Yesterday . . I was . . uhm . . informed . . on a topic that I forgot about  . . . . uhm . . . . . . well . . . . . . . . . uhm . . . I remember . . many years ago . . maybe . . uhm . . in high school and college . . I watched videos on . . . . topics . . like . . . uhm . . the poverty in the world . . . . . . . . and . . refugees . . people without homes . . things like this . . . . . I wasn't really . . uhm . . . . . . well . . . . . . . . . . . . . I don't normally watch this kind of topic . . when I watch videos on YouTube for example . . when I watch things on the internet . . I'm mostly looking . . or . . I'm mostly viewing things that are positively oriented . . . . and so . . this can be things like . . uhm . . technology related videos . . so I can learn more about certain technologies . . . . social related videos . . so I can hear the thoughts and views of other people . . . . . . . . . . . . uhm . . . . . . . . . . . maybe some fashion videos . . a lot of fashion videos . . uhm . . well . . it really depends . . . I like to learn more about fashion . . but also in general I find the . . . . body of a . . . . uhm -_- . . . . . . . . -_-  . . . . . . . well it's not really a relevant topic for this message . . 

--_- . . uhm . . 

I'm trying to say that . . Yesterday . . uhm . . -_- . 

Yesterday . . I saw the twitter feed of a person . . and their . . feed was relating to . . . uhm . . news about . . people dying for all . . sorts of reasons . . like a flooding happening . . . . . . uhm . . they also uhm . . maybe had content about refugees . . having a hard time . . finding . . uhm . . . housing . . or places to live . . 

Ahh . . well . . . . . . you know . . I haven't really watched this kind of content . . now for . . uhm . . well . . I graduated college in 2016 . . . . uhm . . . and I graduated high school in 2012 . . . . well . . . . . . . so . . . in those time periods . . in high school and college . . well . . . those were the times when I kept up with news relating to . . what's not so well or what's not so great in our world . . or what could be improved . . or people . . that . . are . . uhm . . . in really different conditions from how I see my self . . or . uhm . . yea . . well . . if I were in their conditions . . I'm not sure . . 

uhm . . well . . in any case . . I'm really not happy about this . . uhm . . in the sense that . . maybe . . uhm . . well . . . . . . . . . . . . . . . I don't know . . . . . . I don't want to say I'm in a priviledged position . . . . . . . . . . . . . . . . . I don't really want to say that my life style is better . . . because I have a place to stay . . and food to eat . . . but dang . . . it's really difficult not to say those things . . 

I want to think . . . that their lives . . the lives of those people . . that . . are . . without a home . . . . . . . without food . . or water . . . . . . I want to think . . that their lives are equally as valid as mine . . . and that they are . . uhm . . each of these people . . are finding . . uhm . . . unique aspects of their life experience . . valuable . . uhm . . . . uhm . . I really don't know . . . -_- . . this is a difficult topic to talk about . . I'm not really use words that I really think to . . -_- . think about too much . . -_- . . . . . In any case . . 

I'm more in the lines of thinking like . . . . well . . we are all incredibly smart people . . . . . . . . . -_- . . why not have a way to . . uhm . . . work together . . . . . . uhm . . I don't know -_- . . my words aren't coming out appropriately . . . . . . . . uhm . 

Well . . in any case . . I would prefer people to have food and housing . . . and water and food . . if those are the things that they want . . . . . and those things should be easily accessible to people for free . . and without cost . . . . . . . something like that . . 

Well . . . . . . . . . uhm . . . . -_- . . I mean . . I don't know . . . when I think more about this topic . . . uhm . . . . . . I don't know . . . . . that's what I would prefer . . . . . . . . . . . . . uhm . . . . . . . . . . . . . . . . . . . . well . . . I'm trying my best in certain respects . . . . . . . . . uhm . . it's really difficult . . . . . . . . . . . . . . . . . . . . . . . well . . . . .running . . maybe is a waste of time for example . . to go running . . . . . and . . uhm . -_- . appreciate the day . . . . . and see what I'm grateful for . . . and so I feel . . uhm . . slightly . . . or kind of . . guilty for doing this . . . . . . . . . . .  uhm . . maybe I should work on Ecoin instead . . . . . . . . of making these compromises . . . . . . . . uhm . . . . . . . . . . . . -_- . . . it's so difficult . . . . . well in any case . . I'm not sure . . . -_- . I don't want to be so sad . . . uhm . . I'm sad that maybe someone is out there and they don't have food to eat like I do . . . . . or are living outside . . . without a house . . . . . uhm . . . in a forest . . . or on the streets of a city or neighborhood town . . or something like this . . I'm not sure . . . . . . I'm very sorry . . 

Uhm . . well . . . . . . . In any case . . . . I'm happy also to think about . . the idea that maybe my happiness . . and being . . satisfieid . . . . you know . . . . is maybe . . . . . something . . . . . . . . . . . . . uhm . . . . . . . . . . . . something . . . . . . . . . . . . . . . . . . . . . . . . . . . uhm . . . . . . . . . . . . . something . . . . and . . so . . I should also be happy . . . . . . . . . . . . . . . and . . . . . . . . yea . . . . . well . . if I can . . . . . . . . . . . . . uhm . . I have this vision . . . . of how . . uhm .. . . . . uhm . . other people could be happy . . . . . . . . . . . running in the neighborhood . . . . . walking in the park . . drinking water . . eating food . . and so . . . . . . yea . . well . . I can do all of those things quite easily . . today . . . . . and so . . . . . . . well . . . yea . . . . . . maybe if someone else doesn't have those things . . I shouldn't be so sad . . I can just work . . uhm . . uhm . . for myself personally . . I can work . . to uhm . . see that . . maybe if that is a possible thing for them to achieve . . . then that's also quite nice . . 

Well . . I don't know what else to say . . I'm not sure . . uhm . . . . . .  . . . . . . each person has their own life . . uhm . . . yea . . I don't know . . . I'll do my best I suppose . . I'll do my best . 



[Written @ 17:58 - 18:06]


Approximate amount of time . . spent . . eating food . . . and stepping outside . . to get some air . . 

I haven't used the toilet yet . . 

Also . . while I was outside . . I was motivated . . to . . go for a run . . at the local park . . in my neighborhood . . 

And well . . uhm . . it's a nice day today . . the sun is out . . and the weather . . is warm . . the sky is clear of clouds . . and . . uhm . . also . . I'm . . uhm . . . feeling inspired to think about . . uhm . . the world . . and . . uhm . . think about . . how the world is . . and how it got this way . . and what are the possibilities for how the world could be . . uhm . . things like uhm . . well . . there are a lot of things that inspired me to think about this . . uhm . . uhm . . well . . computers are quite a cool new technology . . and . . . I'm not sure . . is . . the computer . . how it is today . . uhm . . the way things could have turned out . . uhm . . uhm . . i'm not sure . . I'm thinking there could have been other innovations . . and still . . we're alive today . . and there's so much to be grateful for . . and the future is always created in the present moment . . with each of us dreaming . . uhm . . and the people beofre us . . were also dreaming  . . . uhm . . of a future . . which . . uhm . . we are a future or . . a possible future for them . . and so . . . there's so much to be grateful for . . 

Uhm . . in any case . . I'm going to go to the park to run . . and . . also think about the things that I'm grateful for . . as I look around . . at the trees . . and the people walking . . and running . . and the sky . . and . . well . . the neighbors . . the houses . . the buildings . . and the building architecture . . and the grass . . and how the grass . . is really beautiful . . with the color of the brown buildings . . uhm . . with the brown brick buildings . . those are quite beautiful . . and . . the uhm . . blue sky I guess . . well . . in any case . . I'm not sure . . there's so much to be grateful for . . I'll uhm . . I'll do something along those lines . . 

00:47:58.830
00:47:58.830

#1	00:47:58.830	Pause


[Written @ 17:09 - 17:10]

Be right back . . going to go to use the toilet . . and also will get something to eat . . before returning to my computer laptop . . . . . and uhm . . maybe watching an entertainment video or something . . . while eating . . . and then . . . uhm . . getting back to work . 


Be right back . . 


[Written @ 17:02 - 17:09]

I spent a lot of time working on . . . Docker . . 

I am new to using Docker . . 

And was wondering if I could use a docker container . . to run . . the uhm . . expectation definition measurements . . or uhm . . unit tests . . when running . . . a continuous integration / continuous deployment pipeline . . or process . . 

Becuase 


Notes @ Newly Created Words
Becuase
[Written @ September 5, 2020 @ 17:05 - 17:09]
[description]: I accidentally typed the word "Becuase" . . instead of "Because" . . I'm not sure what this word could mean at this time . . 

Beacause
[Written @ September 5, 2020 @ 17:06 - 17:09]
[description]: I accidentally typed the word "Bea" . . while preparing to type the word "Because" . . . a possible continuation of the word . . . "Bea" . . could be . . "Beacause" . . . . . and this word looks new to me at this time . . I'm not sure what this word means at this time . . it seems like a new word . . and each new word can be provided a definition . . if one chooses I suppose . . and so maybe this is also a useful word to ascribe a definition to . . I'm not sure . . I can think about it in the future . . uhm . . since I've written it down here . . for those thoughts to be . . uhm . . assured . . or . . uhm . . considered . . from this sort of foundation . . of knowledge . . or something like that . . 




Approximate amount of time spent working . . . . so far today . . 

1 hour . . 40 minutes . . 10 seconds . . 

01:40:10.423
01:40:10.423

#1	01:40:10.423	Pause


