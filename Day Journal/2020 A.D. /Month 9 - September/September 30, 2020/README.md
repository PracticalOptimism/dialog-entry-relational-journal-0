
# Day Journal Entry - September 30, 2020


[Written @ 11:24]

Notes @ Newly Learned Words / Phrases

Super Cowabunga

Cowabunga

[Written @ 10:40]

Notes @ Newly Learned Topics

[Sports and coding/mind-related activities for achieving mastery or proficiency at an activity]
[thoughts: how do sports relate to the topic of coding?]
[thoughts: how do sports relate to the topic of becoming a wellversed player or participant of the sport? is 10,000 hours enough?]

"
doesn't that 10.000hrs derive from sports? i think there is a huge difference in sports vs coding/mind practice
"
-- antonkristensen [Twitch chat]
[1 @ 4:29:34 - 4:29:39]


Notes @ Newly Learned Insight into programming culture

"
Every programming language . . teaches you something different . . about programming in general . . Uhm . . like . . for me . . JavaScript actually . . taught me about functional programming . . Uhm . . 'cause . . I came from the C# world . . where it was mainly . . object-oriented . . and . . I wasn't really used to passing functions around . . And . . when I started to learn JavaScript . . I thought that was really cool . . 

And I started to think about things in a different way . . because of how . . JavaScript did things . . 

Uhm . . And you . . And you potentially solve problems in a different way . . because . . you . . you see that a programming language . . kind of . . forces you . . into a certain way of coding . . 

Uhm . . and similarly . . any of these other languages . . like . . functional languages . . Uhm . . If you learn them . . Uhm . . it's uhm . . You'll . . you'll . . start to think about problems . . how to solve problems . . in a different way . . 
"
[1 @ 4:30:17 - 4:31:01]



Notes @ Newly Learned Words / Terms

Zone of proximal development
[1 @ 4:28:26 - 4:29:32]


Notes @ Newly Learned Books

Practice Perfect

[1 @ 4:26:29 - 4:27:30]


Notes @ Newly Learned Website

The Codeless Code
http://thecodelesscode.com/contents

[1 @ 4:20:40]


Notes @ Newly Learned Website

Clash of Code
https://www.codingame.com/multiplayer/clashofcode

[1 @ 4:20:49]


[1]
❄️ Overview of Vue, 10000 Mistakes, Practice Perfect, Reviewing Entropy Chat PRs and MORE
By Coding Garden
https://www.youtube.com/watch?v=Sba3S4SD4IE&pbjreload=101


[Written @ 10:36]

Notes @ Fashion @ Hairstyles
Notes @ Newly Learned Hairstyles


Mohawk Bun [1 @ 2:28 - 7:45]


[1]
Build a Full Stack Twitter Clone with Coding Garden
By The Coding Train
https://www.youtube.com/watch?v=JnEH9tYLxLk


[Written @ 5:00]

Notes @ Videos Watched

[1], [2]


[1]
Rare Japanese American home movies released by Oregon Historical Society
By The Oregonian
https://www.youtube.com/watch?v=60DuQlGf_pc

[2]
How America Bungled the Plague | NYT Opinion
By The New York Times
https://www.youtube.com/watch?v=GBGShUmEAFA

[Written @ 4:50]

Notes @ Newly Discovered Organizations

Wall of Moms
[1 @ 0:20]

Moms United for Black Lives Matter Portland leader Demetria Hester
[1 @ 0:39]


[1]
Moms United for Black Lives Matter Portland leader Demetria Hester speaks out
By The Oregonian
https://www.youtube.com/watch?v=qIRvAS0Ut-M


[Written @ 1:07]


Notes @ Quotes

"
So . . uh . . I think what you just said . . was so . . inspiring . . and so . . important . . Because . . while people might not understand exactly . . everything that you're talking about . . or . . they might not understand . . or believe . . that it's possible to become President of the United States . . or . . anything like that . . 

I think that this is something that . . you stand for . . that I stand for . . that makes us pretty . . unique . . people . . is focusing on solutions . And I think . . If we had a society . . that . . actually . . uhm . . valued that . . I think there's a lot of homeless people . . who have amazing ideas . . for solutions . . to . . problems . . I think there's a lot of poor people . . who have . . amazing ideas . . And they don't have any support . . They don't have any ability to implement . . those ideas . . They don't have anybody to listen to them . . 

And it's . . frustrating to me . . And . . I have a lot of friends . . who . . you know . . live with their parents . . or . . who are homeless . . or . . you know . . uhm . . are . . "not successful" . . [air quotes] . . in society . . but they're absolue geniuses . . when it comes to thinking up ways to make the world better . . 

And so . . if we can at least come from the understanding . . that . . as . . you know . . just . . wrap your . . wrapping our head around . . some of these ideas . . like . . as a society . . if we learned how to empower people . . with ideas . . that are solutions . . If we learned how to . . reward people . . uh . . for having solutions . . If we learned how to give people . . who . . have . . solut- . . 

I mean . . when I look at my own journey . . trying to bring solutions . . It's like . . I'm met with . . so much . . internet . . hate . . And it's like . . so much work . . and I don't get any money for it . . And it's like . . I have to do it . . I have to work a regular job still . . And I have to like . . come online . . like . . every day . . and try to like . . do everything I can . . to get my movement going . . because . . I believe in it . 

And that's great . . it's great that I can do that . . it's great that I have that amount of energy and passion . . But a lot of people . . you know . . If I had 2 kids . . maybe I wouldn't . . you know . . maybe . . I wouldn't be able to do that . . 

We need support . . for people . . to come up with ideas . . And I think that . . You know . . That's why . . I want . . Wanted . . to keep doing this campaign . . Because . . I want to support you . . I want to support myself . . 

The election is going to get crazy . . Like . . every ye- . . time . . there's . . a presidential election . . I say . . I'm not gonna pay attention . . And then . . you know . . by . . October . . I'm like . . ahhhhh . . . haha . . you know . . Like . . obsessively . . reading the news . . And I'm not someone who even . . gets that invested . . Uhm . . I know people . . get . . way more . . invested that me . . So . . this is gonna be . . a really intense one . . And . . I think that . . you know . . When I stood up . . and said . . that I am gonna continue this campaign . . 

I said . . part of . . partly . . because . . it's for my own mental health . . I need to feel like I'm doing something . . During this time of chaos . .

I need to feel like . . I'm standing up . . for what I believe .
"
-- Unicole Unicron
[1 @ 49:23 - 52:05]


Notes @ Quotes

"
I just want to say what I said earlier . . about information as government . . and just recap that . .

Because . . I was talking about . . the . . uhm . . my vision of the future . . when I believed in Facebook . . and . . uhm . . I was thinking . . uh . . err . . when I believed in Google . . as a good company . . 

I felt that Google . . should be . . replacing the government . . because . . information . . is really . . important . . and what we need is good information . . As the internet progressed . . uh . . the information got worse and worse . . and . . sort of . . articles started taking over the internet . . and now . . it's really bad . . because we have information streams . . that are . . uhm . . totally convoluted with each other . . and . . just . . separating everybody . . and . . creating more division . .

So . . I was saying . . which . . uhm . . you heard Marcus . . but the other viewers didn't . . that . . Your plan . . is a really good stepping stone . . for that . . because it utilizes information . . as . . uhm . . like . . the people's votes . . and . . what the people believe . . 

And also . . Just coming at it . . from . . uh . . a non-greed-based place . . From a place of . . uh . . creating . . a . . platform . . of . . factual . . checks and balances . . of . . information . . 
"
-- Unicole Unicron
[1 @ 48:10 - 49:17]



[1]
TOWN HALL
By Unicole Unicron
https://www.youtube.com/watch?v=Sk8NkqWR9PI

[Written @ 0:38]

Notes @ Newly Discovered YouTube Channels


The Oregonian
https://www.youtube.com/oregoniannews/videos

