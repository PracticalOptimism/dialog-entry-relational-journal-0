
# Day Journal Entry - September 28, 2020


[Written @ 22:58]

Thank you for watching

plans for the next live stream: Ecoin #319

* start the live stream at 0:00 on September 29, 2020
* complete the live stream checklist . . 
* cache the account to transaction map item for an account as a parameterized option when retrieving an account . .
* work on the "apply transaction list" algorithm and expectation definitions . . 

* write the dream journal entry on transparent phones
* maybe write some other dreams ? . . maybe

. . . 

I'm sorry the checklist took so long to complete . . uh . . 1 hour is quite a bit of time . . uh . . hmm . . well . . one thing . . that is also interesting about software development . . is the topic . . of . . research and development . . research . . relates to the topic . . of . . uh . . looking . . or searching for . . or considering . . or thinking about . . how things work . . and trying to find . . a way . . to maybe . . even . . consider . . making things better . . uh . . sometimes . . in my own mind . . uh . . while . . I'm not typing for example . . I'll be thinking . . about . . research related topics . . and trying to improve . . the way . . the database model is organized . . how the data is structured . . in firebase . . or how the data is structured . . in variable trees . . in the memory of the program . . uh . . in the browser . . or something like that . . or . . uh . . maybe . . I'll think about . . how the . . file structure of the program could be updated . . uh . . well . . ideally . . the best uh . . theoretical data structure . . for example . . for the uh . . how the codebase is organized . . is also a data structure . . that makes it easy . . uh . . to allow uh . . further theory to be related to it . . uh . . so for example . . if a new idea . . or a new feature is wanting to be added to the program . . uh . . if the codebase is organized to uh . . well . . consider that possibility . . then . . it is going to be . . easier for . . people . . to extend . . or add to the existing codebase . . and uh . . well . . the preservation of the theoretical uh . . extensibility . . or maybe . . the preservation . . of the idea . . that the program . . is incomplete . . and . . uh . . in its incompleteness . . maybe that is the best way to be complete . . uh . . since in the incompleteness . . the program allows the opportunity to be uh . . further considered uh . . or further continued . . uh . . and the people that come after . . to consider adding further things to the codebase . . will . . not have a hard time to do so . . uh . . in terms of . . having to remove . . a lot of the code . . or having to update a so much of the code . . uh . . if it is . . already . . uh . . considering . . the possibility that those people will be there to have new needs . . or something like that . . 

Uh . . well . . uh . . practical terms . . I mean . . well . . I haven't considered so much . . how the troy architecture . . extends . . for . . having . . topics . . within topics . . within topics . . within topics . . within topics . . within topics . . for several layers of topics . . and so you could have a codebase where the code is something like . . 


/compiled
/compiler
/precompiled
  - /provider
  - /consumer
  - /usecase
    - /usecase-1 // this is a topic
      - /algorithms
        - /algorithm-group-1
          - /algorithm-1 // this is another topic ? ? [1]
            - /algorithms
              - /algorithm-group-1
                - /algorithm-1 // this is another topic
                  - /algorithms
                  - /constants
                  - /data-structures
                    - /data-structure-group-1
                      - /data-structure-1 // this is another topic
                        - /algorithms
                        - /constants
                        - /data-structures
                        - /infinite-loops
                        - /moment-point-algorithms
                        - /variables
                      - /data-structure-2
                      - /data-structure-3
                    - /data-structure-group-2
                    - /data-structure-group-3
                  - /infinite-loops
                  - /moment-point-algorithms
                  - /variables
                - /algorithm-2
              - /algorithm-group-2
            - /constants
            - /data-structures
            - /infinite-loops
            - /moment-point-algorithms
            - /variables
          - /algorithm-2
          - /algorithm-3
        - /algorithm-group-2
        - /algorithm-group-3
      - /constants
      - /data-structures
      - /infinite-loops
      - /moment-point-algorithms
      - /variables
    - /usecase-2
    - /usecase-3


[1]
what does it mean for algorithm-1 to be a topic ? . . . 
when you continue this topic hierarchy further . . is this still a convenient way to organize programs ? . . ?? [hmm . . ]



[Written @ 22:17 - 22:58]


[As described in the previous live stream . . for . . September 27, 2020 . .]
plans for the next live stream: Ecoin #318

* start the live stream at 0:00 on September 28, 2020 
  - Failed to accomplish this . . 
  - Started the live stream . . at around 21:28 on September 28, 2020
* complete the live stream checklist . . 
  - Yes, this was accomplished
  - So far it's taken . . nearly 1 hour to complete this task . . 
* cache the account to transaction map item for an account as a parameterized option when retrieving an account . .
  - 
* work on the "apply transaction list" algorithm and expectation definitions . . 


I'd really like to start the live stream for tomorrow . . September 29, 2020 . . at . . around . . 0:00 . . uh . . but . . uh . . according to how yesterday went . . I may not achieve that . . the problem being . . I don't feel like I have enough time to relax . . and take a break . . before starting the stream again . . 

I've noticed already . . from . . yesterday's stream . . the stream for . . September 27, 2020 . . Ecoin #317 . . that . . performing this checklist operation . . takes nearly 1 hour . . and uh . . that's uh . . that's . . really quite some time . . 

And well . . I would prefer to start programming without explaining too much . . -_- . but that's not really maybe how this should be done if it's for educational purposes . . and so I'm thinking that a checklist helps prepare me more . . to learn more uh . . well . . to learn but also uh . . to teach what the possible ways are to start a live stream . . uh . . uh . . uh . . in previous episodes I have not always . . started with a checklist . . and would not explain the activities for the live stream . . well . . it is quite a long procedure here so far . . uh . . well . . I'm still learning and so . . uh . . please give me some time to adjust to the things that are here in terms of what the presentation style is for these videos and the live stream format . . or something like that . . 

And as always . . I am very sorry for the delay in the development of the program . . ecoin . . which maybe could be here sooner if uh . . I were to learn more time management skills . . uh . . but I am still a work-in-progress when it comes to these topics as well . . I'm sorry . 


oh . . well . . I would like to leave soon . . uh . . well . . one of my goals when coming to the live stream . . is to . . stream for at least 1 hour . . and that seems to be accomplished . . already by now . . it appears or it seems that we're been live streaming for nearly . . 1 hour and 11 minutes . . [Written @ September 22:32]

. . uh . . well . . it feels kind of boring maybe . . to get on to live stream a checklist of what the live stream will be about and then leave -_- . uh . . maybe it's boring but maybe it's not . . 

uh . in any case . . I will be uh . . leaving at 23:00 . . uh . . maybe that is a good time to leave . . and then return . . at . . 0:00 or 24:00 for September 29, 2020 . . which should give me an hour . . to take a nap . . or eat . . and do other things . . to relax my mind . . and give me uh . . more courage to live stream for longer than 3 or 4 hours as some of the previous live streams . . have been . . uh . . well . . hmm . . or I suppose I could stop the live stream here . . uh . . yes . . 

Yes . . maybe it's a good idea to stop here for now . . I was going to suggest that maybe I can write in my dream journal . . until the time of . . 23:00 . . I had . . a cool . . uh . . I dreamt a lot last night . . and maybe the dream of one of them will interest . . you . . the audience . . as well . . it's uh . . it was uh . . well . . uh . . about . . the possibility of . . transparent phones . . 

transparent phones . . are . . for example . . uh . . a sheet of glass . . or a thin film of plastic . . that you can hold in your hand like a cell phone today . . like an android or iphone cellphone . . iphone or android cellphone . . and the cellphone is see-through . . or uh . . so you can see your hand . . behind the cellphone body . . uh . . in the dream . . -_- . well I might as well . . share this in my dream journal uh . . I'm sorry again . . these days I'm feeling quite guilty for not finishing Ecoin more quickly . . uh . . well . . things like writing in my dream journal really interest me . . and so for example . . that's also part of the reason for the delay . . I'm trying to practice a type of science described uh . . in the books by Seth and Jane Roberts . . called . . Dream-Art Science . . which involves . . dreaming . . and . . uh . . uh . . recording your dreams . . and studying your dreams . . and the nature of dreams is like an art form and so you can also invent your own things in the dream . . or you can take inspiration from things that you learn from the dreams . . and . . uh . . for example . . technical innovations can be inspired from dreams . . uh . . or . . uh . . thoughts on social . . relations . . uh . . thoughts . . uh . . well . . a lot of the time . . I look forward to having dreams about technical innovations . . like new . . tools . . and algorithms . . and techniques . . uh . . for social change . . for example . . I have taken . . uh . . notes . . before going to sleep . . where I write that my intention is to have a dream . . about . . artificial intelligence . . or . . a dream about . . games . . video games . . and . . uh . . having the intention to make . . computer video games that are really cool . . uh . . well . . those are some of my intentions previously . . uh . . well . . in actuality . . my dreams . . are quite varied . . (1) adventure [last night I had a dream about walking around . . on a battlefield . . and thinking about the things that I liked about battlefields . . the friends . . the actions . . the ideas . . ], (2) love and human relations [last night I had a dream . . about . . my step brother . . who's still a baby . . nearly age 3 . . 2 or 3 years old . . and I was chasing him around . . in the house . . and we were having fun . . and laughing . . before waking up . . we stopped . . We were laying on the floor . . I told him . . "I love you . . " . . there was a pause . . then . . "I have to go back to work" . . and then that's when I woke up from the dream . . ], (3) action-adventure [when I think of action-adventure . . I think of . . . Hunter x Hunter . . the Japanese animated television show . . and yet . . uh . . for now . . I'm not remembering very many action-adventure dreams . . I'm sure I've had them . . uh . . I have adventures . . but I don't uh . . well . . I guess . . the dream . . where I started to choke someone . . uh . . the death dream . . is action . . death dreams . . are action based I suppose . . uh . . but . . action-adventure . . to me . . seems to also represent the adventure aspect . . uh . . and I tend to interpret adventure as . . more peaceful . . walking around . . thinking . . ], (4) death [I had a dream . . more than 2 weeks ago now . . less than 2 months ago . . someone said . . they killed my family . . we were travellers from another town . . and . . the person said to me . . "too bad we had to kill those travellers" . . or something like that . . -_- . . it was quite sad . . uh . . then I walked toward the person . . and grabbed their neck to start choking them . . their friend . . was a bigger person . . more . . chubby . . or thick . . or big-boned . . or large-bodied . . and . . the chubby person . . started swinging a bat at me or somekind of blunt weapon . . or uh . . something like that . . not a gun . . or their fists . . uh . . well . . I used their . . thin . . thin-bodied . . or skinny friend . . the person that I was chocking . . with my right hand . . I used their body . . to defend against the attacks from the chubby person . . uh . . but maybe I was hit by one of the attacks . . because . . I woke up . . and . . uh . . well . . I feel like I was hit . . but it was uh . sudden . . uh . . well . . so then I suppose . . uh . . I died in that dream . . or . . uh . . my dream body . . uh . . died . . ], (5) 

Well . . The time is almost 23:00 . . and so . . I'll stop here . . I'll come back another time . . to record the dream journal entry on the topic of . . transparent phones . . 

[Written @ 21:28]


Live Stream Checklist

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - "Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha)" By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - Ru's Piano Ru味春捲 [YouTube Channel] https://www.youtube.com/c/RusPiano/playlists
  - "Ajeet Kaur Full Album - Haseya" By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4

Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
  - (1) YouTube LiveStream Chat
  - (2) YouTube + Firefox Web Browser (Picture-in-Picture Mode)
  - (3) iTerm
  - (4) Firefox Web Browser + https://www.timeanddate.com/
  - (5) Brave, Firefox, Tor Browser
  - (6) Visual Studio Code
  - (7) Express VPN
  - (8) Visual Studio Code . Previously: "Notes" on my macbook

Periodic Tasks

* [x] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - checked at approximately 00:52:38 in the video (52 minutes, 38 seconds). Everything looks good on YouTube: https://www.youtube.com/watch?v=wAky1OtHMxY
  - 

Salutations

* [x] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [x] Talk about the possibilities for the next live stream


[Written @ 18:20]

Image 1:

Laura Shin of the Unchained Podcast
![Laura Shin of the Unchained Podcast](./Photographs/laura-shin.png)


Image 2:

Unchained Podcast Logo
![Unchained Podcast Logo](./Photographs/unchained-podcast-logo.jpg)

Notes @ Strange Coincidences That I Discovered

The photograph of Laura Shin in the above photo shows a person wearing a black shirt . . [Image 1]

The skin color of the person as well as the color of the black shirt match the appearance of the logo icon for the "Unchained Podcast" YouTube Channel . . [Image 2]

. . . The U shape of the bottom chain of the logo . . appears to be like the shirt . . with the U-shaped collar . . and the . . n-shaped head . . appears like the . . top of the head . . of the person . . in the photograph . . [Image 1]

:O So cool :O 


[1]
AMA With Laura: The Greatest Innovation in the Industry Right Now - Ep.139
By Unchained Podcast
https://www.youtube.com/watch?v=39ST6O7I3Mw&pp=QAA%3D

[Written @ 17:16]

Notes @ Newly Learned Words

Suss out
[2 @ 5:21 - 5:23]


Notes @ Newly Discovered Organizations

Social Capital
https://www.socialcapital.com/
[1]


[1]
Chamath Palihapitiya | Social Capital News
By SC Global
https://www.youtube.com/watch?v=9hswjmgLMKw

[2]
AMA With Laura: The Greatest Innovation in the Industry Right Now - Ep.139
By Unchained Podcast
https://www.youtube.com/watch?v=39ST6O7I3Mw&pp=QAA%3D


[Written @ 0:41]

Notes @ Newly Learned Words

Instanced rendering [1 @ 0:52]


[1]
My Week Implementing People into my City-Builder Game
By ThinMatrix
https://www.youtube.com/watch?v=CattLrDZt3o


