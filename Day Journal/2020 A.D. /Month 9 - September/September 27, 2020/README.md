
# Day Journal Entry - September 27, 2020

[Written @ 23:43]


Approximate amount of time . . spent live streaming today for Ecoin #317

03:18:21.979
03:18:21.979

#1
03:18:21.979
Pause

[Written @ 23:23]

Thank you for watching

plans for the next live stream: Ecoin #318

* start the live stream at 0:00 on September 28, 2020 
* complete the live stream checklist . . 
* cache the account to transaction map item for an account as a parameterized option when retrieving an account . .
* work on the "apply transaction list" algorithm and expectation definitions . . 


[Written @ 22:59 - 23:23]

things completed:

* fix errors for the website development localhost to work
* stop working at 23:00 to review the live stream


things I didn't get to [I didn't start working on this]:

* work on the "apply transaction list" algorithm and expectation definitions

. . . 

review of today's live stream:

An earlier approach . . in . . viewing the . . list of . . digital currency amounts . . for a particular account . . relied on using . . the transactions . . that were processed by that account . . and . . uh . . reviewing . . the list of transactions . . one . . before another . . uh . . and so . . for example . . each transaction object would also contain the information of . . "senderAccountCurrencyAmountAfterTransactionAccount" . . and "recipientAccountCurrencyAmountAfterTransactionAccount" . . well . . uh . . 

So . . if you wanted to know the account currency amount . . for an account . . before a particular transaction was applied . . you would need to know that transaction id . .

how the firebase database is modelled or organized . . how the data is organized . . didn't make it uh . . seem to be . . very easy . . to retrieve . . the . . account currency amount . . of an account . . before . . a transaction was applied . . 

Uh . . well . . 

Usint another database model . . where you ahve a variable tree of . . processed transactions associated to each . . digital currency account . . 

you can see the database model for the account to transaction map here at . . 

https://gitlab.com/ecorp-org/ecoin/-/blob/master/precompiled/usecase/digital-currency/data-structures/digital-currency-transaction/index.ts

. . well . . 

uh . . 

this . . way of doing things . . seems to uh . . not take advantage of the . . digital currency transactions . . that have already been retrieved from the database . . and stored locally . . in the Vuex store . . uh . . well . . uh . . because . . uh . . these processed transaction variable tree items aren't being stored in the vuex store . . well . . that's going to introduce a lot of database requests . . for every time . . we are viewing . . the graph of . . an individual's . . account currency amount . . uh . . so for example . . uh . . because . . the processed transaction variable tree items . . aren't being . . cached . . or stored in the vuex store to be re-used . . well . . uh . . even if we had previously retrieved that data . . uh . . uh . . well . . leaving the page for example . . would erase that data from memory . . or queue that memory uh . . information to be . . referenced for . . garbage collection . . uh . . or something like that . . 

Uh . . well . . now I'm thinking about . . uh . . caching this data . . so that . . uh . . even if you leave the page where you're viewing the account currency amoun history . . and then return to the page . . uh . . you won't need to . . make those several . . database requests . . to get the information . . on . . the account currency amounts before a digital currency transaction was applied . . instead . . you can re-use that data . . uh . . since it's kept in memory . . and not . . marked to be . . recycled . . uh . . or deleted . . uh . . or . . recycled . . by the . . garbage collector program . . uh 

Well . . those are my thoughts right now . . uh . . so . . with the introduction of this database model . . of having the . . account currency amount . . in another . . variable tree . . and not attached to the . . digital currency transaction . . as in using "senderAccountCurrencyAmountAfterTransactionAccount" and "recipientAccountCurrencyAmountAfterTransactionAccount" . . as was previously used . . as a property . . on each transaction . . 

well . . this uh . . different approach . . uh . . uh . . well . . to make . . reuse of the data . . uh . . we'll also need to cache it locally on the . . browser . . using . . either . . vuex . . or . . maybe . . a javascript hash table . . uh . . well . . once . . we . . cache the . . processed transaction variable tree item . . we'll . . uh . . uh . . in my mind . . I'm thinking that it's maybe . . going to improve . . the efficiency . . of database queries . . and uh . . that would . . uh . . well . . be nice . . uh . . uh . . the earlier model we used . . the database model of having the account currency amount for each account of the transaction . . being attached to the transaction itself . . was . . efficient . . uh . . and . . I guess . . I'd like to preserve that efficiency . . uh . . it was efficient . . because we were caching . . or we are . . currently caching . . the digital currency transactions . . and so . . now . . all we need to do . . to keep that same efficiency . . is to also cache the processed transaction information from the database . . 


[Written @ 21:18 - 21:30]

plans for today's live stream: 

* fix errors for the website development localhost to work
* work on the "apply transaction list" algorithm and expectation definitions
* stop working at 23:00 to review the live stream
* stop the live stream by 23:59


plans for the future live stream:

* start the live stream at 1:00 on September 28, 2020 [ideally . . we would start the live stream at 0:00 of September 28, 2020 . . and yet we may need to use the toilet . . and also eat and do other things . . like walk outside or take a nap . . or take a few breaths to breath . . or think about other things . . whatever comes to mind I suppose . . ]
* complete the live stream checklist . . 
* continue working on the "apply transaction list" algorithm and expectation definitions . . 


thoughts on the live stream review:

* annotate the video timeline
* is this an effective or efficient way of developing the project . . . . . 



[Written @ 21:17 - 21:18]

Time elapsed measure @ 00:52:39

Preparing to write a plan on the tasks to complete for today's development for the Ecoin project. 

[Written @ 20:26]

Live Stream Checklist for Ecoin #317 for September 27, 2020


Greetings

* [x] Greet the viewers
  - [I said hello to everyone]
* [x] Share a message relating to peace and love and world peace
  - [I let people know they are beautiful]
  - <3
* [x] Plan the tasks to complete
  - [Yes, we planned]

Health and Comfort

* [x] Have drinking water available
  - [I have water]
* [x] Use the toilet to release biofluids and biosolids
  - [I have used the toilet]
* [x] Sit or stand in a comfortable position
  - [I am sitting in a comfortable position]
* [x] Practice a breathing exercise for 5 - 15 minutes 
  - I have taken time to breath today

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [I am listening to "[ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music" on YouTube by  Nhạc sóng não chính gốc Hùng Eker at the website resource url: https://www.youtube.com/watch?v=4MMHXDD_mzs&pp=QAA%3D&ab_channel=Nh%E1%BA%A1cs%C3%B3ngn%C3%A3och%C3%ADnhg%E1%BB%91cH%C3%B9ngEker]


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
  - I have prepared all of these programs


Periodic Tasks

* [x] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - I'm not sure . . if I did this but the live stream is still happening :O . . [Written @ Approximately 23:32 . . approximately . . 03:08:17 . . time has elapsed . . 3 hours . . 8 minutes . . and 17 seconds . .]
* 

Salutations

* [x] Thank the audience for viewing or attending the live stream
  - Yes, I said thank you
* [x] Annotate the timeline of the current live stream
  - Yes, The timeline is annotated
* [x] Talk about the possibilities for the next live stream
  - Yes, we will work on caching (or storing locally on the web browser) . . the processed transaction information that's retrieved from the database . . so we don't need to retrieve data that has already been retrieved before . . for example . . if a processed transaction information has already been retrieved . . from the database . . we won't need to retrieve it again . . because it will already still be in the computer memory (and not deleted, removed or recycled) and so we can return that same data . . 


Notes @ Newly Created Word
texample
[Written @ September 27, 2020 @ approximately 23:38]
[I accidentally typed "texample" . . instead of . . "example" . . I'm not sure what this word . "texample" . . should mean at this time . ]

trei
[Written @ September 27, 2020 @ approximately 23:38]
[I accidentally typed the word . . or the phrase . . "trei" . . instead of . . "retrieved" . . I'm not sure what this word . . "trei" . . should mean at this time . . ]



[Written @ 6:29]

Uh . . well . . in Part I . . of the time . . 2:26 - 5:27 . . we talked about "isometric graphs" . . "vector graphs" . . and "textual information" . . 

well . . this . . uh whole idea was inspired from reading the paper on the "Ouroboros" protocol . . [1]

. . . uh well . . I was looking through the paper . . earlier . . yesterday . . on September 26, 2020 . . and . . I was . . inspired by the . . Vector Graphic at the end of the paper . . to write down the . . idea . . that . . "Vector Graphics" . . is really an important . . idea . . as important . . as . . the idea of . . isometric graphs . . or something like that . . or at least to consider . . "Vector Graphics" . . as . another important way to represent information . . 

. . 

Ouroboros Forkable Strings and Divergence Vector Graphic
![Ouroboros Forkable Strings and Divergence Vector Graphic](./Photographs/mathematics/ouroboros-forkable-strings-and-divergence-vector-graphic.png)

. .

. . 

The mathematical precision . . in calculating . imagery . is very important . . it's the difference .  between having a computer that is off-center . . or the graphics of the pixels aren't very aligned . . in a horizontal and vertical grid . . and so for example . . the whole computer imagery . . tilts uh . . maybe a few degrees . . and even though its maybe not perceptible on the scale of a few inches or a few millimeters . . the computer . . uh . . well . . 

the computer . . uh . . well . . scaled . . to more . . uh . . inches . . or millimeters squared in area . . or uh . . size . . dimension . . uh . . well . . that could be .  . resulting in an even greater offset . . and so . . the mathematical precision in vector graphics is really important in this respect to attempt to preserve . . a uh . . relation to the theory . . in a . . sense that relates uh . . in the way that the theory . . or theoretical limit is attempted to be reached or preserved . . even in the wake perhaps say . . or uh . . scalings of the vector graphics . . or . . other transformations are introduced that require a re-calculation of the vector imagery . . 

uh . . Vector graphics . . is . . uh . . one way to term this . . type of information presentation . . or one style of naming this information presentation style . . vector graphs . . or . . calculated graphics . . or precise graph calculation methods . . I'm not sure . . the idea . . uh . . doesn't only need to relate to drawing graphs on the computer screen . . but can for example . . relate to . . the approximation of . . uh . . a concept to another concept . . and so maybe the visualization of one idea can be achieved to be related to another idea by having a vector graphics format that is highly specific and is also able to mold and change or shape around the topic of the idea of the goal . . or . . uh . . well . . in a way . . it preserves certain properties about both of the two starting ideas . . the uh . . approximation idea as well as the the  . . target idea . . and so even if these ideas aren't presented in a . . 2D matrix of . . uh . . computer graphics . . like . . lines . . and . . uh . . colors . . or . . shapes . . uh . . . and sizes of shapes . . maybe . . uh . . the idea of uh . . vector graphics precision . . the mathematical . . relatedness that comes with the vector graphics format . . uh . . maybe that is uh . . also a useful idea . . like the topic . . of density and intensity of . . textual information . . as well as ease of . . comparing and contrast . . with . . isometric graphs . . 

Well . . 

In conclusion . . these 3 ideas  . . isometric graphs . . vector graphics . . and textual information . . uh . . when you combine all 3 of them . . uh . . when I was thinking about them today . . uh . . well . . actually uh . . I was thinking about them yesterday on September 26, 2020 . . and then . . I started to take notes . . ont this topic . . today . . uh . . notes in . . this day journal . . dialogue . . uh . . for communicating uh . . and sharing thoughts with others . . 

Well . . 

uh . . . so . . it uh . . the density and intensity of comparing and contrasting mathematically precise topics is really a powerful tool when I think about it . . 

Uh . . mathematical precision is really interesting . . uh . . this could mean . . uh . . not only for example . . . the particular features and characteristics . . of a person . . or of a planet . . uh . . well . . uh . . mathematical precision . . when I think about it . . relates not necessarily to the mathematics that is understood today . . in terms of the mathematics . . and how the interpretations of the world works . . or of how the mathematical universe works . . uh . . in terms of today's understandings . . by mathematical precision . . uh . . well . . mathematics is intended to be a theoretical framework upon which other theoretical frameworks can be developed or understood . . and those theories aren't necessarily established or known and so there's a theory factory that needs to be created that is also within the realm of mathematical topics . . uh . . when you have theory factories . . uh . . maybe those are useful for creating new theories automatically uh . . and so those theories can be explored and relied upon . . uh . . and maybe it's not always easy to connect existing theories with the newly discovered theories and so expeditions in other varied theory constructs may need to be established as well . . to wand forward with ideas relating to connecting or communicating interchangeably from the terms of one theory identity to another . . 




Notes @ Newly Created Words
facroties
[Written @ September 27, 2020 @ 6:57]
[I accidentally spelled the word . . "factories" . . as "facroties" . . I'm not sure what this word . . "facroties" . . should mean at this time . . I'm sorry . . ]


[1]
Ouroboros: A Provably Secure Proof-of-Stake Blockchain Protocol
By Aggelos Kiayias, Alexander Russell, Bernardo David and Roman Oliynykov
https://eprint.iacr.org/2016/889.pdf


[Written @ 6:11 - 6:29]

DNA . . Deoxyribo Nucleic Acid . . is an example . . of what could be considered . . as a textual information presentation format . . or something like that . . 

uh . . well . . the idea of . . density and intensity . . as talked about in Christopher Alexander's "The Nature of Order, Volume 1: The Phenomenon of Life" . . seems to apply here as well . . and so . . not only . . for . . topics like . . English . . or Chinese . . or Physics . . or Chemistry or Algebra . . notation . . all sorts of uh . . disciplines of society . . relate to textual information communication contact . . and these information standards . . are quite . . uh . . rich . . or . . quite . . dense with . . information . . that . . uh . . well uh is like a language with various syntax like a story of a certain type that is told by an author of a certain type or an author with a certain purpose or something like that  uh well biology relating to authorship in terms of organisms . . biological organisms like goats, cats and sheep . . are interesting to relate to in terms of textual information and storylines since they seem really like unique types of stories in relation to the stories of uh other formats like uh a book with a sort of time based narrative where events occur in a sequence uh well the idea that the story has to occur in sequence is also observed in the sheep relationship or the cat relationship of storylines . . and yet there are also . . uh . . notations about . . the . . uh . . possibility . . for the sheep to . . uh . . for example . . grow . . different color . . fur . . or different color sheepskin . . and so . . this language that's being used to talk about the sheep organism relates to the theoretical possibility of the sheep's coloration being a variation of other sources and not really only necessarily one pattern that's printed repeatedly by the same author or manufacturer of the next version of the sheep body book . . if that is a good way to describe it . . I'm sorry . . uh . . well . . that's uh . . maybe one way to say it . . but maybe it's better to also keep in my the textual information relating to the sheep's consciousness which is really a thinking mechanism for the sheep to realize its own thoughts and dreams or thoughts and feelings in the waking reality and satisfy its own conditions for living or something like that . . well . . that is uh . . a thought . . inspired a lot by what . . Seth teaches in his books . . relating to the nature of personal reality . . and the nature of dreams . . and the nature of consciousness . . uh . . 

Well . . uh . . . seth talks about these structures . . called . . consciousness units . . which . . uh . . well . . uh . . I'm still learning a lot about them and so . . uh . . I'm not an expert at describing them at this time . . and yet . . in accordance to something of something that I could possibly say . . maybe a way to view the unit of consciousnes is to consider a way of viewing a particle or . . a ball of light . . that is . . uh . . indivisible . . uh . . so for example . . if you are trying to divide it . . you are . . uh . . going to get more of itself and to see it like . . a mirror maybe of itself . . and so . . uh . . well . . maybe that's one way to describe the property but . . it's really still obscure . . uh . . in the way that I understand it . . 

uh . . another way to perhaps . . uh . . describe what the consciousness units are like . . those units of consciousness that are like the uh . . basis of . . uh . . consciousness . . and also the basis of physical reality . . uh . . . . uh . . well . . the consciousness units . . uh . . for example . . if you are . . to . . seem to observe the unit . . in a way . . you would be observing . . the uh . . uh . . everything else . . and so the unit itself is really like an illusion . . or an entity that takes on the form of everything that it could be . . and so for example . . uh . . in terms of space and time perception . . of how we observe space time reality for example . . if you have a ball . . made of glass . . in your hand . . and you said that this ball . . is the origin of bodies of origins . . uh . . well . . this origin . . uh . . this origin . . uh . . . . you're maybe assuming that there are many of these origins . . and so there are many balls . . uh distributed through space uh . . and so your friend could maybe have a ball . . and yet . . . this is uh . . maybe not the case as since the consciousness unit is uh . . well . . uh . . the ball is maybe . . you are looking at it . . and uh . . looking at the ball . . you will see inside the ball is the ball of your friends . . and uh . . well . . inside the ball of your friends that you se through the ball that you have . . you are maybe uh . . seeing your ball as well . . as this type of structure is maybe like a uh referencing of the balls that are all the balls and not really only the ball that you are thinking to be observing which is really uh a represention of the consciousness unit and so the consciousness unit is like camoflauge that takes on the shape and form of the thought that is being created uh . . or something like that . . I'm not sure . . uh . . I'm still studying this topic . . 


[Written @ 5:43 - 6:11]

. . . Text Information . . provides . . density . . and intensity . . to a communication . . 

Do you imagine that we could have a textual or text-based language . . for interpretting . . the waves of air in the clouds . . or maybe the patterns of air motions when people breath . . already our textual information is applied towards representing . . uh . . for example . . uh . . ideas . . in a sort of . . uh . . format . . for . . people to read . . on paper . . for example . . 

However . . text based information or text information or textual information . . in the way that I started to think about today . . because of how useful it is to represent . . things . . or . . represent . . ideas . . or . . uh . . represent things . . in a way that . . is . . providing a lot of meaning . . that is . . uh . . in a small amount of space . . [density] . . and . . the meaning of each word . . itself is also . . uh  uh uh hmmm density and intensity . . I suppose these uh . . well . . the intensity of the words are a type of uh . . density in themselves . . and so . . maybe it sounds like a redundant phrase . . to say . . density and intensity . . however . . uh . . well . . I like the phrase . . and also . . I learned it from the . . work of . . Christopher Alexander . . in the first few chapters . . in the first . . 1 or 2 . . chapters . . in the . . "The Nature of Order, Volume 1: The Phenomenon of Life" . . which talks about the "density and intensity" of the centers that we observe in our ordinary experience . . 

And so . . for example . . the uh . . centers that . . Christopher Alexander talks about . . are related to things like . . uh . . books . . on a book shelf . . leaves on the ground . . uh . . houses in a town . . doorknobs . . but not only the physical things we can sense with our eyes . . but also . . the non-physical things that we can feel . . and this . . having a feeling of something . . also makes it a center . . of uh . . well . . a center . . [you could call it a center of interest but that's not the langauge uh . . that I think Christopher Alexander . . . uses in the book . . and yet . . it could also help to . . uh . . understand what a center is . . at least in my present opinion at this time . . "a center of interest" . . and so it's not only thoughts you have . . or feelings you have . . uh . . its also physical things you see . . people walking . . processes . . names . . uh . . but also . . maybe you notice a far away theoretical structure that doesn't seem possible . . unless you look at it in a certain way . . and it's only available from your mind . . but it's really not something you're familiar with talking to people about . . or people would uh . . well . . it's not like it's interesting or has anything to do with your present experience of life . . . . it's a far off structure that presents a possibility that isn't really uh . . something uh . . necessarily conditional on the things you're familiar with . .] . . uh . . 

well centers . . are . . feelings . . thoughts . . physical objects . . anything you notice is a center . . if you notice or observe anything . . those are centers . . and uh . . well . . these various centers that work in your experience . . are all related to one another . . uh . . and they help . . uh . . intensify the life . . in . . uh . . the whole of the . . construct . . or . . other centers . . and so . . centers . . help . . strengthen one another . . and they work together . . to create more life . . well . . that isn't maybe the best introduction to Christopher Alexander's work . . in this first book in the volume series "The Nature of Order" . . and yet . . that's a synoposis . . in an informal . . unedited way . . in terms of how I'm speaking in English and typing on the keyboard . . and trying not to press the backspace key very often . . uh . . well . . uh . . yes . . I'll have to think more about this way of writing . . I know personally . . I like to write things . . but I don't always come back to read them . . and so . . this informal style . . is maybe for sharing information with myself if I want to uh . . come back to see my journey . . or journal in how my thoughts worked . . and also . . there's the advantage that I can take more and more notes . . and maybe don't need to feel nervous or shy . . to write . . without stopping even if it's not really something that I would necessarily want people to read  uh . . in terms of having a formal document that is complete and has all the references to the pages . . and to the documents that are being discussed . . uh . . or something like that . . 

The density and intensity of something . . 

I think of . . computer chips . . uh . . how dense the computer circuitry is . . in . . a . . single unit . . of measurement . . like an inch . . or a millimeter . . or . . the area of . . an inch squared . . or a . . millimeter squared . . of . . a computer chip . . and how much . . information is specified in this unit of area . . 

And the information itself . . is in a sense . . like a text format . . as there is a sort of . . assumed language . . of how the . . uh . . message is meant to be interpretted . . by a computer engineer . . who's looking at the . . message . . that is . . this unit . . of area . . on the computer chip . . and trying to . . decipher . . what that meaning could relate to . . 

And so . . once you have the language . . of . . uh . . understanding . . in what this unit area means . . on its own . . maybe you can look at other words . . which are like . . other . . uh . . areas of the computer . . uh . . other . . physical space-time locations of the computer perhaps . . if that's how the computer is uh . . communicated to work . . or assumed to work . . 

uh . . well . . those are interesting computers . . physical-space-time textual information language computers . . where the density and intensity relates to . . positioning in space and time . . component operations . . 

And so there are really uh . . a lot of . . uh . . well . . if you understand the basic language units . . the syntax of speech . . maybe you can . . uh . . work into looking out at how those uh . . combinations of language syntax speech units work . . uh . . to . . uh . . have the . . uh . . computer . . uh . . component . . uh . . be diagosed . . as working . . as expected . . according to the language understanding of the computer engineer . . 

uh . . or something like that . . or something like that . . uh . . or something like that . . the metaphor to how . . uh . . the computer engineering communication with another engineer . . through a computer chip . . technology is really one way of looking at textual information . . uh . . well . . one way of representing textual information . . it's not necessarily . . uh . . . the manuscript text . . of uh . . a dictionary . . or . . . a physical word-and-paper book . . 

But maybe it's uh . . the idea of . . uh . . a density . . of characteriszed idea formats through space and time uh could be the basis of a theory that works to communicate information in a highly packed way or in a dense and intense way that makes communication highly efficient or something like this . . well uh . . the communication networking between computers through electrical transportation units could be seen as a textual language like machine code or uh code developed a text computer program like javascript or rust or golang and yet uh well that introduces the concept then that maybe one can take a language like english and add an abstraction layer over english to make it possible to have a more uh more general thought-condusive language that makes communicating uh certain types of ideas more easier to do or something like that well that is a new idea to me and i'm not really sure where i'm going with this now so i'll stop here for now

[Written @ 5:27 - 5:43]

Notes on Part I . . continued . . 

In the previous paragraph of text . . on the notes for Part I . . uh . . relating to the previous time entry from 2:26 - 5:27 . . from 2:26 to 5:27 . . . . . . military time . . uh . . timezone unspecified . . 

Well . . uh . . we talked about . . Isomatric graphs . . 

And well . . now . . I'll say a few things about what my thoughts were . . uh . . from yesterday . . on the topic of . . Text . . 

Uh . . . textual information . . is another way of presenting information . . which is really quite cool . . you can have (1) multiple languages . . of text . . 

世界，您好，这是用中文写的文字。
Hello world、これは日本語で書かれたテキストです。
Hello World, 이것은 한국어로 작성된 텍스트입니다.

. . 

A single block of text . . like a paragraph . . or line statement . . can communicate so much information . . since we already are relying on the history of the language learning process . . from the speaker of the language of text . . 

. . 

The language of algebra in mathematics today . . is expressed . . in wide spread use . . as a textual information language . . and so for example . . 

. . .

Here is an example of text that applies . . numbers . . and algebraic syntax . . or algebraic expressions . . or algebraic . . communication tools . . like . . numbers . . variables . . parentheses . . exponential markers . . . . some of the tools . . like . . exponents for example . . are also used . . in other textual languages . . like . . English . . in the use of . . superscripts . . that could communicate the reference . . of . . a footnote . . in a research paper . . for example . . 

. . . 

[1]
Proofs of the Pythagorean Theorem 
By Armain Labeeb, Gaurav Kumar, Mohammad Farhat, and 3 others contribued [Brilliant.org]
https://brilliant.org/wiki/proofs-of-the-pythagorean-theorem/



 Two Algebraic Proofs using 4 Sets of Triangles

The theorem can be proved algebraically using four copies of a right triangle with sides aaa, b,b,b, and ccc arranged inside a square with side c,c,c, as in the top half of the diagram. The triangles are similar with area 12ab {\frac {1}{2}ab}21​ab, while the small square has side b−ab - ab−a and area (b−a)2(b - a)^2(b−a)2. The area of the large square is therefore

(b−a)2+4ab2=(b−a)2+2ab=a2+b2.(b-a)^{2}+4{\frac {ab}{2}}=(b-a)^{2}+2ab=a^{2}+b^{2}.(b−a)2+42ab​=(b−a)2+2ab=a2+b2.

But this is a square with side ccc and area c2c^2c2, so

c2=a2+b2.c^{2}=a^{2}+b^{2}.c2=a2+b2.

A similar proof uses four copies of the same triangle arranged symmetrically around a square with side c, as shown in the lower part of the diagram. This results in a larger square with side a+ba + ba+b and area (a+b)2(a + b)^2(a+b)2. The four triangles and the square with side ccc must have the same area as the larger square:

(b+a)2=c2+4ab2=c2+2ab,(b+a)^{2}=c^{2}+4{\frac {ab}{2}}=c^{2}+2ab,(b+a)2=c2+42ab​=c2+2ab,

giving

c2=(b+a)2−2ab=a2+b2.c^{2}=(b+a)^{2}-2ab=a^{2}+b^{2}.c2=(b+a)2−2ab=a2+b2.

A related proof was published by future U.S. President James A. Garfield. Instead of a square, it uses a trapezoid, which can be constructed from the square in the second of the above proofs by bisecting along a diagonal of the inner square, to give the trapezoid as shown in the diagram. The area of the trapezoid can be calculated to be half the area of the square, that is,

12(b+a)2.{\frac {1}{2}}(b+a)^{2}.21​(b+a)2.

The inner square is similarly halved and there are only two triangles, so the proof proceeds as above except for a factor of 12\frac{1}{2}21​, which is removed by multiplying by two to give the result. □_\square


[Written @ 2:40]

I took these notes on September 26, 2020 . . after the previous notes that are recorded in this journal entry dialogue . . 

| Space Transformations | Time Transformations | Theoretical Transformations |
| - | - | - |
| Translation | Time Translation | New |
| Rotation | Time Rotation | New |
| Scale | Time Scale | New |
| Reflection | Time Reflection | New |
| Association | | New |

<br>
Transformations

* Translation
* Rotation
* Scale
* Reflection
* Association

* Time Translation
* Time Rotation
* Time Scale
* Time Reflection

* New
* New
* New
* New
* New

--

I took these notes earlier on September 26, 2020 . . and so . . uh . . I transcribed those notes from my physical paper material notebook . . and wrote them here . . or typed them here . . in my . . computer . . day journal entry dialogue . . 

The ideas . . I was having were related to how . . isometric graph elements . . can be positioned along the graph . . or how information can be presented . . 



[Written @ 2:26 - 5:27]

I took some notes in my physical paper material notebook . . and here are a few of those notes . . I wrote these notes using a pencil on September 26, 2020 . . I took these notes on . . September 26, 2020:


Part I:

Information Presentation Formats:

Isometric Graphs [Comparison & Contrast]
Vector Graphics [Mathematical Precision]
Text Information [Density & Intensity]
Space-Time Perception [Imagination & Memory]
Associative-Time Perception [Imagination & Memory]
Emotional-Associative Perception [Experience]

* Isometric Graphs
  - Useful for "Comparison & Contrast" of information presentation
  - Viewing information as an isometric graph makes comparison and contrast very nice and easy . . 
* Vector Graphics
  - Useful for "Mathematical Precision" of information presentation
  - A theoretical model can be presented accurately in a mathematical-theoretically accurate sense that can scale to further calculations or be embeddable with other mathematical constructs
* Text Information
  - Useful for "Density & Intensity" of information presentation
  - A lot of information can be presented in a small amount of space
* Space-Time Perception
  - Useful for "Imagination & Memory" of information presentation
  - Space-Time Perception can be a useful way to present information when you are an organism that uses imagination and memory structures that support this way of perceiving reality
* Associative-Time Perception
  - Useful for "Imagination & Memory" of information presentation
  - Associative-Time Perception can be a useful way to present information when you are an organism that uses imagination and memory structures that support this way of perceiving reality
  - Associative-Time Perception relates to Seth's teachings on how humans prefer to view the world not only in terms of physical space presence but also psychological space presence as a way of perceiving the immediately available environment . . and so for example . . a toy car that you played with when you were a child . . can be just as present in your memory experience . . today . . as a physical car that you're walking into as you are on your way to work . . as an adult . . and so the association of the toy car that isn't physically present in the environment . . as you are heading to work . . in a physical person-sized car . . the toy car can be emotionally reminded or brought up in memory as something to think about . . if for example . . you are a fan of cars . . and if you had a good experience playing with the toy car as a child . . and now the real physical person-sized car is initiating a recall of this childhood event . . 
* Emotional-Associative Perception [Experience]
- Useful for "Experience" of information presentation
- Emotional-Associative Perception can be useful for uh . . purpose of experiencing . . uh . . information . . based on the emotions . . or something like that . . [I'm really not clear uh . . why I wrote this one in my notes . . but it was just an idea that I had . . that emotional-associative perception is maybe the basis for reality . . and experience . . and so for example . . uh . . something like . . people experience is expressed by emotional associative events . . or something -_- . . but i'm not really sure . .]


Part II:

Time is a creative construct. The creation of time relates to the cycles and rhythms of motion of perceived information on data such as the motion of a planetary object or the motion of words of speech to count at regular or regularly perceived intervals of speech . The association of the motion of an object, such as the motion of a clock, to the motion of another object, such as the motion of a person, is a creative decision. Other clocks with other perceived values could have also been applied by the creative decision maker. The nature of perception creates time .


-- 

Notes on Part II:

This is a word-to-word transcript of what is written in my notes . . and yet . . uh . . even when I look . . at it now . . after having written the transcript in my . . day journal entry here . . I am not . . really . . that impressed with how it's written . . or . . uh . . I think there needs to be . . uh . . more clarity . . or more . . uh . . ellaboration . . or explanation in the thoughts . . uh . . it's not all that easy to read . . or something like that . . and also . . I feel it is not really based on knowledge so much . . uh . . it is . . a lot of it is . . uh . . . thinking . . and feeling out my present thoughts of this topic at this moment . . and yet . . I feel that those thoughts could be edited . . uh . . and so . . in general I suppose . . this paragraph . . uh . . could be edited to be uh . . more a filter of the feelings I have in relation to this topic . . and not only having this rough draft of my thoughts on this topic at this time . . I'm sorry . .


--

Notes on Part I:

I was only playing around with ideas . . uh . . this list is uh . . just for fun . . and nothing serious . . uh . . but of course . . uh . . fun is a serious thing . . and so uh . . well . . I had fun . . and maybe uh . . these ideas are useful . . uh . . not only for myself . . to uh . . think about later . . but . . uh . . also maybe . . uh . . even though they are in rough sketch format . . and not really a complete picture that is meant to be considered uh . . as something complete . . uh . . well . . uh . . maybe . . you can still use them in your own work in think about what they mean in the terms that you're familiar with . . 

Uh . . 

Well . . 

Let's start with the isometric grid . . 

![Cat Isometric Grid](./Photographs/isometric/cat-isometric-grid.jpeg)

![Animals Isometric Grid](./Photographs/isometric/animals-isometric-icon.jpeg)

![Calendar Isometric Grid](./Photographs/isometric/calendar-isometric-grid.jpeg)

![Mega Man Isometric Grid](./Photographs/isometric/mega-man-isometric-grid.png)

![Park Isometric Grid](./Photographs/isometric/park-isometry.jpeg)


. . . 

Isometric grids . . Isometric graphs . . Sprite sheets . . 

uh . . these are some of the names . . that these . . objects . . uh . . or . . uh . . constructs . . or mathematical objects . . or mathematical constructs . . or mathematical objects . . go by . . 

Isometric Grid . . or Isometric Graph . . or Sprite Sheets . . 

I like the ease of being able to look . . left . . right . . up and down . . and in . . uh . . other uh . . directions . . and a mix of these directions . . like diagonal . . and in circles . . and . . I can pace . . my eyes . . back and forth rapidly . . and search for information . . along . . the . . plane . . of the isometric graph . . uh . . or along . . the surface . . of the graph . . and . . my eyes . . feel comfortable . . and it seems quite . . normal . . or natural . . and I don't feel like I'm forcing myself to move so much . . 

uh . . 

Well . . some "isometric" graphs . . don't make this easy to do . . uh . . for example . . it's not all that easy for me . . to . . reach around at any object in the graph . . with my physical or psychological-associative vision . . or something like that . . I have . . uh . . my eyes . . are strained . . or I feel like I have to stop . . to re-adjust . . and think about something . . uh . . like . . "what does this mean" . . or "what does that mean" . . 

. . When it comes to an isometric graph . . the ones shown above . . in photographs are really quite nice to look at . . and it's easy to compare and contrast . . among the items in the graph . . or between the items in the graph . . the graph nodes . . or the graph . . elements . . or the graph entities . . 

Uh . . in comparison . . 

Uh . . the following "isometric" graph . . which isn't really . . uh . . uniformly isometric . . I would guess . . could be the right terminology for this case . . well . . because of this lack of . . uniform . . or universal . . isometry . . "Isometry" . . meaning "same measurement" . . as in . . "iso-" . . "same" . . and "metry" . . meaning "measure" . . 

![Angry Birds Isometry Non-Uniform](./Photographs/isometric/angry-birds-isometry-non-uniform.png)

![Game Isometry Non-Uniform](./Photographs/isometric/game-isometry-non-uniform.jpeg)

![Game Isometry 2 Non-Uniform](./Photographs/isometric/game-isometry-2-non-uniform.png)

![Game Isometry 3 Non-Uniform](./Photographs/isometric/game-isometry-3-non-uniform.jpeg)

![Police Isometry Non Uniform](./Photographs/isometric/police-isometric-non-uniform.jpeg)

. . . 

uh . . well . . the varying sizes of the . . graph . . elements . . in the non-uniform . . isometric graph . . are . . uh . . maybe . . why . . my . . impression . . my psychological impression is to stop and read more information about . . what are the relationships between the elements . . and how can I further . . compare and contrast them . . in accordance to their prximity to one another . . or their shape or color . . or something like that . . 

uh . . I'm not really sure . . uh . . I really find that the isometric graphs . . where the sizes of the . . items in the graph . . are all the same . . these graphs . . are much . . easier . . to read . . from my present experience . . of having looked at these graphs . . and uh . . well . . having looked at them . . uh . . yea . . and thinking that . . well . . it seems like there is a psychological difference . . in terms of . . how I feel . . about the ease of use . . of one graph versus another . . 

Well . . Isometric graphs . . that are . . uniform . . in terms . . of . . uh . . the size . . characteristic . . uh . . and so . . all the items are relatively the same size . . and the . . distance . . uh . . between each of the items . . is also in a . . rectangular . . grid like format . . preferrably . . a square . . helix . . or a square . . uh . . matrix . . a square matrix is the term . . helix was the first term that came to mind . . but that seems more like a creative . . approximation of . . the word . . matrix . . which is the originally intended word . . that . . I meant to use . . in this case . . uh . . matrix . . uh . . well . . like . . a euclidean . . plot . . or a 2d graph of the euclidean space . . or . . the cartesian coordinate space . . 

![Cartesian Plane](./Photographs/mathematics/cartesian-plane.png)

Cartesian Coordinate Space . . 
Euclidean Space . . 
Isometric Grid
Isometric Graph
Sprite Sheet

These are a few of the names that this topic . . of . . a . . uniform distance measure between . . the elements in a graph . . in terms of how those elements could be presented with relation to one another . . 

Well . . 

uh . . I planed around with this idea . . of Cartesian . . Coordinate space . . or the cartesian coordinate . . space . . 

And . . 

uh . . well . . here's an example . . I found on the internet . . very quickly . . see how . . on the right of the image . . the 3 graphs . . listed one on top of one another . . are quite . . easy to compare and contrast . . to one another . . it's very easy . . to look up and down . . to see what are the similar elements . . and what are the differing elements . . 


![Graph Comparison](./Photographs/isometric/graph-comparison-isometric-grid.png)


. . . 

There are a lot of examples of isometric graphs . . being applied today . . 



YouTube
![YouTube Isometric Grid](./Photographs/isometric/youtube-isometric-grid.png)


Google
![Google Isometric Grid](./Photographs/isometric/google-isometric-grid.png)


Instagram
![Instagram Isometric Grid](./Photographs/isometric/instagram-isometric-grid.jpeg)

Instagram Follow
![Instagram Follow Isometric Grid](./Photographs/isometric/instagram-follow-isometric-grid.png)


Android Phone Display
![Android Phone Display Isometric Grid](./Photographs/isometric/android.jpeg)

iPhone Phone Display
![iPhone Display Isometric Grid](./Photographs/isometric/iphone-isometric-grid.png)

Phone Comparison Presentations
![Phone Comparison Isometric Grid](./Photographs/isometric/phone-comparison-isometric-graph.jpeg)

-- 

Here are some more images of Isometric graphs:


Vans in an isometric graph
![Vans in an isometric grid](./Photographs/isometric-2/van-isometric-grid.jpeg)

Shopping mall (cylindrical projection of an isometric grid)
![Shopping mall isometric grid](./Photographs/isometric-2/shopping-mall-isometric-grid.jpeg)

Grocery store
![Grocery store](./Photographs/isometric-2/grocery-store-isometric-grid-1.jpeg)


Grocery store
![Grocery store](./Photographs/isometric-2/grocery-store-isometric-grid-2.jpeg)


Grocery Store
![Grocery store](./Photographs/isometric-2/grocery-store-isometric-grid-3.jpeg)


Classroom
![Classroom](./Photographs/isometric-2/classroom-isometric-grid-1.jpeg)


Classroom
![Classroom](./Photographs/isometric-2/classroom-isometric-grid-2.jpeg)


Classroom
![Classroom](./Photographs/isometric-2/classroom-isometric-grid-3.jpeg)


Housing
![Housing](./Photographs/isometric-2/housing-isometric-grid-1.jpeg)


Housing
![Housing](./Photographs/isometric-2/housing-isometric-grid-2.jpeg)


Deep Learning
![Deep Learning](./Photographs/isometric-2/deep-learning-isometric-grid-1.jpeg)


Deep Learning
![Deep Learning](./Photographs/isometric-2/deep-learning-isometric-grid-2.png)


Deep Learning
![Deep Learning](./Photographs/isometric-2/deep-learning-isometric-grid-3.jpeg)


Deep Learning
![Deep Learning](./Photographs/isometric-2/deep-learning-isometric-grid-4.jpeg)


Earth image presentation in an isometric graph
![Earth](./Photographs/isometric-2/earth-isometric-grid-1.jpeg)


Earths in an isometric graph
![Earth](./Photographs/isometric-2/earth-isometric-grid-2.png)


Movie-based versions of Earth in an Isometric graph
![Earth](./Photographs/isometric-2/earth-isometric-grid-3.jpeg)


Universes in an isometric graph [maybe it is theoretically possible to quickly compare and contrast these universes by applying thought-teleportation techniques to observe at the speed of thought at various depths through theoretical matter as well as practicalized matter realms of the universes of interest]
![Universe](./Photographs/isometric-2/universe-isometric-grid-1.jpeg)


<!--
[Updated @ September 28, 2020 @ 22:02 . . It's not easy to tell if this is an isometric grid or isometric graph . . I'm sorry . . ]

Universe
![Universe](./Photographs/isometric-2/universe-isometric-grid-2.jpeg)
-->

People
![People](./Photographs/isometric-2/people-isometric-grid-1.jpeg)


People
![People](./Photographs/isometric-2/people-isometric-grid-2.jpeg)




. . . 

Thinking about the ease of comparing and contrasting . . items in an isometric grid . . and also about other things . . like . . alternate . . present . . lives that . . I could be living right now . . 

I . . could . . visualize . . in my mind . . a matrix . . an isometric grid . . of . . the . . alternative . . lives . . in . . other . . realities . . perhaps . . to me . . they appear theoretical . . and . . yet . . from another perspective . . all of these . . probable . . versions of myself . . could . . be . . actualized . . or practical . . and so the theory . . and the practice . . are met . . uh . . for example . . uh . . well . . from their own perspective . . 

And so for example . . when I take . . my consciousness . experience . . or my perspective of the scene that I see around me . . as well . . as . . my thoughts and feelings . . and my emotions . . and when I try to . . project them onto an isometric grid . . of . . other . . uh . . feelings and thoughts . . that I could . . use to . . compare . . uh . . to my . . present . . perception of myself . . or my present perception of my self image or something like that . . 

Uh . . well . . . It's really convenient for me to use . . an isometric grid to do that . . and so for example . . I can remove the bias . . of uh . . being emotionally attached to my . . uh . . skin color . . or my . . uh . . biological background as a human being . . and try to perceive myself . . as an ant creature . . or an ant-humanoid creature . . and so . . it's not . . like . . uh . . for example . . well . . I would imagine . . my head  . . could be looking a lot different . . and I can easily see in my mind . . a grid of . . uh . . characters that I could look like . . 

uh . . and also . . if I wanted I could imagine how those people would walk . . in comparison . . to how I imagine that I walk in my life today . . just by using an isometric grid . . in my mind . . things are made . . a lot easier to visualize . . 

uh . . fisualize [vision + fission] . . [visualize + fission] . . a typographical error . . fisualize . . A newly created word . . to . . fragementation of a vision . . and so for example . . uh . . uh . . well . . for example . . the visualization of myself . . as having a hulk-like body . . and so . . my body could be green . . and . . that image comes to my mind quite rapidly . . with very little effort . . and I can select from all these other colors of the hulk . . that I would want . . like . . being . . a hulk with . . green . . skin color . . or with . . pink skin color . . and yellow skin color . . I can visualize the graph of my probable selves . . if those were the characteristics that I were trying to . . uh . . consider . . in my isometric grid . . or isometric graph . . and I can change or . . uh . . compare and contrast . . between different . . noses . . or different lips . . 

uh . . 

Well . . 

uh . . it's kind of a really weird idea . . uh . . . for example . . to me . . I'm a little uh . . nervous . . about how to think about people . . uh . . for example . . I can . . uh . . have a friend that I've known for a while . . but . . uh . . placing them in an isometric graph image . . makes it . . uh . . really . . like . . I could change my friend's characteristics . . with . . the thought of my imagination . . and it's like . . well . . why doesn't my friend match some of these other characteristics . . or . . uh . . if it's for example . . a romantic partner . . that I'm imagining in my mind . . uh . . well . . 

That's uh . . I mean . . it makes me feel uh . . not really sure . . what romance is all about . . how it could it be that . . perhaps . . probably . . these other people . . exist . . those . . alternative versions . . of the people . . that I'm imagining . . exist . . and so for example . . uh . . my romantic partner could have . . uh . . this or that quality . . about them . . and . . uh . . in an isometric graph setting . . I can change . . uh . . or I can observe what other variations of that characteristic . . exist . . and so . . what does it mean for my spouse to have a slightly bigger nose . . 

What does it mean for my spouse to have slightly bigger lips ? . . 

Wha does it mean for my spouse to have . . brown hair . . or brown-black hair . . or silver or white hair . . what are all of these characteristics going to mean for my spouse ? . . are they still beautiful to me ? . . what has changed about them ? . . what has changed about me ? . . do they feel differently . . if for example . . it's an alternative version of them . . that watched . . this or that movie . . or if it's an alternative version of them . . that had this or that . . childhood background . . or this or that . . childhood history . . or this or that teacher . . 

What if they had siblings . . when in the present reality that I know them from . . they don't have siblings . . or maybe they only have step-siblings . . what does that mean for them to have step-siblings in this reality . . as opposed to not having step-siblings in another reality ? . . 

How should I treat the person I know now . . especially uh . . with me . . uh . . I am in the realm of believing that it's possible to perhaps meet these other probable selves of ours . . uh . . if there is a technological innovation that would allow that to happen . . uh . . I'm sure then that is possible to for example . . meet your probable selves . . and see the variation in their personality . . uh . . maybe it's not that big of a difference . . uh . . or a perceptible difference . . maybe . . you can watch their life history . . in a video . . uh . . a video or movie diagram . . or maybe another form of thought information retention . . or retrieval . . where you can perceive their life in a single flash of a memory for example . . and so . . uh . . for example . . uh . . that could be in a way . . presented in an isometric grid . . so you can compare one life to another . . and compare . . the . . uh . . sequences of events . . 

Uh . . I imagine . . it's like . . comparing one universe to another because . . uh  . . for example . . it's like . . each individual . . really makes a difference . . to the whole universe with everything they do . . and the universe itself is created by the individual . . and so . . alternative versions of ourselves . . and the combinations and non combinations of things they did . . or things you and I did . . or things that we couldn't have done because of the restrictions of the universe . . or the restrictions of the universe that were placed there by the individual or group of individuals . . uh . . well . . something like that . . uh . . well . I mean . . it really seems like . . uh . . the whole universe is effected by the choice of . . uh . . even the smallest uh . . microbe . . or the smallest . . subatomic element . . and so it's quite uh . . in a way . . if it is possible to imagine these things . . then possibly it's possible to compare and contrast them . . in an isometric grid . . where you can compare and contrast various qualities about these places . . Uh . . 

Well . . it's quite interesitng . . I'm really amazed but uh . . well . . also . . uh . . well . . I think I need to learn more . . I'm not used to seeing people . . especially people in a way that . . well . . it's like . . this is a probable version of themselves . . and so . . it's like . . you know . . they could have been a doctor . . or a lawyer . . or a scientist . . or a technician . . or uh . . any other technical or non-technical or social or non-social worker or something like that . . They could have donated to this or that organization . . they could . . spent this or that amount of money at this or that store . . they could have gotten married to this or that other person . . they could have . . uh . . gotten in a car accident . . or gotten sick . . or . . uh . . anything could have really happened . . and all of a sudden . . there is this . . magical opportunity . . to meet with them here . . in this . . uh . . form . . uh . . the probable form . . uh . . of meeting them here . . is . . practicalized . . or . . uh . . not just probable . . but . . actual . . or actualized . . uh . . well . . uh . . I guess . . I'm really not sure . . 

Isometric grids are quite cool uh . . thinking about all of this stuff . . could have been complicated . . uh . . but . . for some reason . . when I place . . the things I want to compare in an isometric grid . . I'm able to see . . more . . uh . . things . . that I can compare . . and I'm not feeling limited . . to the thinigs that I would like to compare or contrast . . they could be physical things . . or maybe . . ideas . . that I have in mind . . like . . maybe the idea . . of walking . . and other ways that people could walk . . what if people walked backwards ? . . uh . . or what if people walked on their arms ? . . uh . . hand balancers . . hand walkers . . uh . . what if people . . uh . . walked with their hands in the air . . what would that look like ? . . can you imagine an isometric grid of what that would look like to compare different walking styles ? . . 

What about an isometric grid . . to compare the ideas of . . uh . . democracy . . and . . socialism . . and communism . . are those physical things that come to mind ? . . are they processes ? . . uh . . for example . . for me . . uh . . I can visualize . . uh . . well . . people . . uh . . walking around . . or gathering around . . and talking about things . . uh . . and so . . uh . . in a sense . . maybe . . all of these ideas . . uh . . are related to one another in this kind of way . . where people are involved . . and talking about things . . uh . . and . . uh . . maybe there the face of a famous person that comes up . . uh . . a famous . . bald person maybe ? . . or uh . . well . . I could give them different hair . . maybe to not make them based in the history that I know . . and so they could have qualities that I could . . uh . . associate with someone . . of uh . . those . . idealogical . . uh . . considerations . . idealogical considerations . . so for example . . a person who likes books . . or likes to read books . . could be given glasses . . and maybe they are wearing a suit and a tie . . and so . . these are things that are psychologically associated with . . intelligent people . . or people . . that like to read books . . and so . . I can update the people in my isometric graph to also share these qualities . . if uh . . that's how I think about people who . . uh . . practice democracy for example . . or people that practice . . uh . . socialism . . or communism . . or uh . . anything else that would bring people together to talk about things . . uh . . well . . in any case . . they don't all have to have glasses . . I could pick and choose which of the elements of the people in the graph . . have glasses . . uh . . maybe the number of people wearing glasses in each group of the people with their own ideologies . . could represent how . . smart they are . . and so that would be interesting to also consider . . 

Well . . uh . . 

That's one of the reasons uh . . I really like isometric graphs . . 




Notes on Part II:

uh . . these are personal notes . . and not really . . uh . . things that are set in stone for example . . and so . . I'm always . . interested in learning to communicate in uh . . ways that maybe . . uh . . preserving the meaning of how I feel or what I know even across the times . . and so . . maybe the message is timely to myself . . 

Uh . . but because these are personal notes . . uh . . they are more . . uh . . not always the best way of expressing things . . and so I can always change my mind in how I feel about these notes . . and so . . I'm sorry for that . . 

uh . . In general . . a lot of my thinking in the relation of time . . on the paragraph about time . . is influenced . . uh . . or inspired . . to be thought about . . especially in relation to how . . I've learned about this topic . . through the . . Seth books . . and . . also . . the book . . on . . "The Alien Interview" . . by Matilda O'Donnel McElroy . . Which . . all of these books . . uh . . including . . other books that . . uh . . are . . uh . . really . . in my particular realm of interest in my life . . uh . . especially at this time . . of having written this message . . on September 27, 2020 @ 2:32 . . uh  . . these books can be accessed by visiting the resource URL: 

https://drive.google.com/drive/folders/1mW-MIplFXNJZ2QLF2TQOKr_Evy5FdsSM?usp=sharing


[Written @ 1:44]

"
Living a meaningful life . . takes work . . 

It's an ongoing process . . 

As each day goes by . . we're constantly creating our lives . . adding to our story . . and sometimes . . we can get off track . . 

Whenever that happens to me . . I remember a powerful experience I had with my father . . 

Several months after I graduated from college . . my dad had a massive heart attack that should have killed him . . He survived . . and when I asked him . . what was going through his mind . . as he faced death . .

He said . . All he could think about was needing to live so he could be there . . for my brother and me .

And this gave him the will to fight for life .

When he went under anesthesia for emergency surgery . . instead of counting backwards from 10 . . he repeated our names like a mantra . . He wanted our names to be the last words he spoke on Earth if he died . . 

My dad . . is a carpenter . . and a Sufi . . It's a humble life . . but a good life . . 

Lying there . . facing death . . he had a reason to live . . Love . .

His sense of belonging . . within his family . . his purpose as a dad . . his transcendent meditation . . repeating our names . . These he says . . are the reasons why he survived . . That's the story he tells himself . . 

That's the power of . . meaning . . 

Happiness . . comes and goes . . But when life is really good . . and when things are really bad . . having meaning gives you something to hold on to . . 
"
[1 @ 10:34 - 12:12]


Notes @ Newly Created Words

hep
[Written @ September 27, 2020 @ 1:49]
[A typographical error of . . "happens" . . I'm not sure what this word . . "hep" . . should mean at this time . . I'm sorry . .]

maz
[Written @ September 27, 2020 @ 1:51]
[A typographical error of . . "massive" . . I'm not sure what this word . . "maz" . . should mean at this time . . I'm sorry . . ]

[Written @ 1:31]

Notes @ Rediscovered Phrases or Common Expressions
"
I lose all sense of time and place .
"
[1 @ 6:13 - 6:16]

[Written @ 1:20]

Notes @ Newly Learned Words

Unmoored
[1 @ 9:48]

[Written @ 1:12]

Notes @ Newly Learned Words

Whirling Dervishes
[1 @ 9:18]

Notes @ Newly Discovered Poets

Rumi
[1 @ 9:20]


[Written @ 0:25]

Notes @ Quotes
"
The key to purpose is using your strengths to serve others .
"
-- Emily Esfahani Smith
[1 @ 5:06 - 5:09]

Notes @ Quotes
"
True belonging springs from love . . and lives in moments among individuals . . and it's a choice . . you can choose to cultivate belonging with others .
"
-- Emily Esfahani Smith
[1 @ 3:22 - 3:32]


Notes @ Quotes
"
There's an emptiness gnawing away at people . . and you don't have to be clinically depressed to feel it . . 

Sooner or later . . I think . . we all wonder . . is this . . all there is ? 
"
-- Emily Esfahani Smith
[1 @ 1:18 - 1:30]

[1]
There's more to live than being happy | Emily Esfahani Smith
By TED
https://www.youtube.com/watch?v=y9Trdafp83U



Cultim
[Written @ September 27, 2020 @ approximately 0:42]
[A typographical error of . . "cultivate" . . I'm not sure what this word . . "Cultim" . . should mean at this time . . ]

typograhical
[Written @ September 27, 2020 @ 0:42]
[A typographical error . . of "typographical" . . ]

[Written @ 0:12]


I'm sorry . . uh . . huh . . I seem to have gotten carried away in talking . . in the last day journal entry and didn't take so many notes on the notes that I took today . . uh . . from my physical paper material notebook . . and thought . . I thought . . I would share them with you here today . . 

Well . . uh . . I suppose . . even though it's a new day . . uh . . September 27, 2020 . . and normally . . I would like to . . start the next live stream episode . . uh . . well . . uh . .

Okay . . I'll be right back . . and so . . for now . . I will stop the live stream for September 26, 2020 . . for the Ecoin Episode #316 . . 




