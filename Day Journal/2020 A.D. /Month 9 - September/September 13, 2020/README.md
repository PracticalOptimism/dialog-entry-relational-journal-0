# Day Journal Entry - September 13, 2020


[Written @ 23:26 - 23:59]

A lot of my thinking is still a work in progress and I think that's why I'm nervous in saying so much . . or nervous in saying anything . . 

It's theoretical devices that I play around with in my mind . . 

. . odd . . while writing this list . . I'm not sure . . how to organize things . . Artificial Intelligence is an important aspect . . and yet . . uhm . . Well . . uhm . . hmm . . right . . well . . hmm . . I want to say something like . . an emotional foundation of love peace and kindness . . that should first be the emotional pillar that we all ride upon . . to see each other succeed or something like that . . and yet . . well . . maybe . . uhm . that's assumed . . uhm . . I'm not sure where to place that on the list . . I suppose it could be . . number 0 . . uhm . . well . . I think Unicult is a great group of people to be established with for this purpose . . And as for myself . . personally . . I'm learning more about Unicult and I'm not sure if it's for everyone . . uhm . . well . . if a structure . . or an emotional structure works for everyone . . which love . . peace . . and kindess . . seem to be emotional structures that could support everyone's mission on Earth . . and beyond . . wherever they decide to go . . uhm . . . . or something like that . . I'm not sure . . I'm still learning about these concepts . . and so . . I'm not always sure . . how to . uhm . . suppose them . . in the conversation . . uhm . . while assuming nothing about the educational background of my . . uhm . . community . . or . uhm . . advisee support group . . which is you . . and whoever else . . uhm . watches these videos . . and gives comments and supporting feedback and things like this . . 

. . . 

a lot of projects will depend on . . artificial intelligence technology being readily available for people to use . . 

And . . a good first prototype . . will lead the way . . to another era for humanity . . in the case that we would suppose that thinking . . uhm . and not many of us necessarily support this thinking or planning structure for the rest of us . . and so . uhm . there are thoughts that need to be cleared in this regard . . as well . . uhm . - -_ - - . . well . as I am trying to say repeatedly now . I'm not sure if any of my comments should be taken as advice . I'm only trying to say thing from my current perspective in light of the things that I understand at this present time . Already here on September 13, 2020 @ 23:38 PST . Something like that . I-_- . hmm right . 

. . right . 

what are things to think about . . with regards to . uhm . . well . . The Venus Project ? . . 

In regards to The Venus Project . . hmm . . housing, water hmm peace conflict resolution resources . .

The Venus Project would like to build a city that supports everyone's needs with water, housing, educational resources and all the resources that are available from the reservoir of society's productions to each of the people without cost by requiring money or servitude . . or something like that . . 

Instead of being required to work for resources . . And trading money for resources . . You are . . relatively free to do as you wish . . whether that's forming in groups or communities . . doing nothing on your couch . . uhm . . well . . it's really diverse in terms of what things are available for you to do . . uhm . Each individual really has the mission to come to grips with how they would like to satisfy their life on Earth . . whether through . . painting . . or . . working on artwork . . or building technologies . . that help empower other people . . It's each individual's goal to determine what they would like to contribute . . Philosophy . . Architecture . . Statistics Measurements . . Computer Science . . and there are more opportunities . . as you would imagine them . . And so . . "You Create Your Own Reality" . . is a basic foundational idea . . or principle that we each share . 

The enthusiasm and developments of those volunteers that want to build things like . . robots . . and computer systems that manage the architecture of the societal agricultural and maintenance operations . . is an idea in how the program can be initiated and can sustain itself . In those terms . . Each of us are already volunteers . . volunteering our thoughts and effort . . towards missions we think are useful for people today . . And having our basic needs met . . by being provided money . . for housing and food expenses . . is a consequence . . . . 

I'm not sure how to continue this dialog . 

Uhm . 

I want to say something like . . well . . as long as there are volunteers . . that people are in need of . . . . like . . computer engineers . . spiritual teachers . . We can all live the lives we want to live . And can each benefit from the work of everyone else . We can benefit from the creations and creativity from other people . We can 

Well . . uhm . right . . 

Uhm . . there's uhm . things to say . . and we've only 3 minutes . . 2 minutes now . . until the next day . . 

Uhm . well . . I suppose I can come back in a few minutes . Alright . Well . Uhm . 

That's it for today's day journal entry . See you tomorrow . 


[Written @ 23:20]

Okay . . I've thought of a few things . . 

(1.) Artificial Intelligence
(2.) 
(3.)
(4.)
(5.)
(6.)
(7.)
(8.)


This is still a work in progress . . there are so many things to think about . . It depends firstly on the people's interest . . I'm really not sure . I'm really not sure . Uhm . Well . . I'm going to speak from uhm . . a perspective -_______--------- - -_ -_- _ - -_- - - _____ - - - - - ----------------------- - - -_-

-_-------------

In any case 





























Notes @ Newly 
Thosre
[those are]


[Written @ 23:15]

From the last paragraph . . I was inspired to think . . about . . what . . I . . would . . do . . with . . a . . billion . . people . . 

I imagined . . What if . . a . . billion . . people . . showed . . up . . ready . . to . . work . . on . . a . . project . . with . . you . . 

. . "How will the project start to be implemented?" . . 

This is a good question as well . . 

And . . well . . 

I'm not sure . . 


[Written @ 22:57]

Right . . writing in my day journal really feels freeing . . I'm trying to now write about . . thoughts and ideas about . . The Venus Project . . Uhm . . Before starting the live stream . . in the video I'm watching [1] . . the question was brought up . . what would you do with . . a billion dollars . . and . . uh . . I couldn't think of much . . really . . and . . well . . uhm . -__ . . I mean besides giving it away to other people . . I don't know . . what else to do . . I'm already uhm . feeling satisfied in many aspects of my life . . and so . . uhm . . I'm not sure . . 

Well . . Jacque Fresco was mentioned in the video . . And . .

"
If I would give you . . a billion dollars . . how many of you know exactly what you would do with it ? . . Day 1 . . You know . . How would you spend it ? . . Most of us . . you know . . yes . . I'm gonna go . . you know . . I'm gonna go . . I'm gonna travel . . That's as far as we can take it . . most of us . . We don't know . . how . . how we would spend that amount of money . . Because we have always been living in poverty . . But I think if you would have told Jacque Fresco . . There's a billion dollars . . what do you think of that ? . . I think he would say . . you know . . Do you have more ? . . I need more . . because . . I'm on a mission . . I have a lot of things I want to do . . A billion is great . . but it's not enough . . I need more . . 
"
[1 @ 33:25]



[1]
2020 05 22 The Venus project Designing the future lecture
By TVPActivism
https://www.youtube.com/watch?v=wh1SaO1YJtY


[Written @ 22:55]

Well . . I got through the idea that I'm feeling nervous . . uhm . . I've been watching a video on a . . team . . of activists . . for the venus project . . on YouTube . . before . . starting the live stream today . . 

I'm quite happy to stumble upon . . a team like this . . I'm not sure . . it seems like . . uhm . well . it's quite a cool thing to have a community . . isn't it ? . . well . . in any case . . -_- . . hmm . . writing is strange right ? . . . hmm . . on one hand . there's also what I think . . and then . . on the other hand . . I'm thinking about .  another thinker . . called . . you . . or . . uhm  . . whoever is reading this . . and those thoughts . . uhm . . I'm working around to see . . what is . uhm . . -_- . in any case . . 

I'm not sure . right . uhm . . . . . . . . . . . . . . . . . . . . . . . oh right  . there's a lot of things . to say . right . uh . right . and . uh . I . guess . I'll . speak . uhm . something . 

Uhm . right . maybe give me more time to . speak . if . Uhm . . I don't really have much to say or something like that -_- . right . that's not really the answer that I wanted to give . But that's okay for right now I suppose . I'm not sure . 

[Written @ 22:38]

Well . . This is my Day Journal . . I'm not sure why . . But I'm starting to feel nervous . . writing in my day journal these days . . These are supposed to be personal recordings of my thoughts and feelings . . and so . . in days of the future . . I can look back to read what I was thinking at that time . . and how . . I was feeling . . or what things I was going through at the time . . what circumstances and events I was going through at the time . . What my attitude was . . What my questions were . . Any thoughts . . aspirations . . dreams . . ideas . . random things to say . . uhm . . normal . . every day occurences . . what I at that day . . things like that . . 


. . 

. . 

Well . . . those are my feelings and thoughts at this time . . 

Uhm . . one aspect that makes writing in my journal weird to me . . these days . . is the idea that other people . . uhm . . people at all . . besides myself . . for example . . uhm . . well . . anyone at all really . . I feel like . . I don't really want to give advice to people . . on how they . . uhm . . should  . . or could live their lives . . uhm . . uhm . . suggesting . . that . . uhm . . this or that thing . .seems . uhm . I don't know . kind of awkward for me . . 

In any case . . I'm more interested in talking about stuf . . I really like to talk . . and share information . . and think about things . . but at the same time . . uhm . . suggesting . . that . uhm . . whatever I do . . however I talk . . is . uhm . . the thing that others . uhm . . should consider doing . . is . . uhm . well . . it seems almost implicit in the nature of my writing . . like . . just by writing . . it seems like I'm suggesting . . that this is a thing that other people can do as well . . 

Uhm . . and so . . it can influence . . people . . and children . . and whoever wants to be inspired or something like that . . 

But I'm really not sure .. uhm . . a source of inspiration . . uhm . . I'm always uhm . I'm always . . or more often than not . . liking to think of myself . . as . uhm . a student or something like that . . I'm uhm . . more of a person that's learning and growing and working through my own . . mental complications . . my own . . thought patterns . . and am not really sure . . my mental obligations you could say . . whatever mental attitudes I'm having . . or that I'm holding on to . . I'm still really . . just starting to really learn more about them . . and why they are there . . and I don't really have much advice on . uhm . . well . . whether that's also something that someone else should uhm . look into . . it seems like from my perspective . . that there are so many ways of living . . and life is valid in all sorts of form . . regardless of maybe . . how I'm used to perceiveing the life form or something like that I'm really not sure . . these topics can be involving for me to think about or even write about maybe . . uhm . . I'm really still a work in progress . . I'm still thinking things through . . In any case . . I'm not sure how to conclude this paragraph . . I'm uhm . . really wanting to talk about the idea that I'm still kind of nervous at this point . . my arms are slightly sweating . . and I'm not sure . . I'm trying to be careful to not talk too much about . . uhm . . I don't know . . on the one hand I feel like people . . uhm . . are smart enough to figure things out on their own . . and so . . "why give advice . . ? . . why point things out ? . . can't they figure it out . . I believe in them to be able to figure things out . . I think they are smart . . or they can find their own solutions to things . . "  . . and on the other hand . . well . . we're all students aren't we ? . . aren't we all studying things from our own . uhm . I guess . perspectives  . . . or psychological vantage points ? . . I'm not sure . . I'm uh . . I'm . uh . well . . . uhm . something . right . something something . 


Notes @ Newly Created Words
Occurenes
[Written @ September 13, 2020 @ 22:45]
[I accidentally typed the word "occurences" . . as "Occurenes" . . I'm not sure what this word . . "Occurenes" . . should mean at this time . . "Occurenes" . . by the sound of the term . . the pronunciation . . the pronunciation reminds me of the term . . "Ocarina of Time" . . like in the Legend of Zelda . . series of video games . . ]


Notes @ Newly Learned Words
Daj
[Written @ September 13, 2020 @ 22:40]
[I accidentally typed "Day" as "Daj" . . I'm not sure what this word "Daj" . . should mean at this time . . . . Well . . on second thought . . it seems like this word already has . . a definition . . "Symbol for the decajoule" . . https://www.yourdictionary.com/daj . . In any case . . . I thought this was a newly created word . . and it seems like it's already an existing word . . and so . . it's a newly learned word for me . . at this time . . something like that . . I may need to do more research as well . . ]



[Written @ 22:01]


"How can we have abundance and sustainability?"
[1 @ 26:54]

"
How can we have world . . where you have access to fantastics . . fantastic things . . It's there . . whenever you want . . It's there . . But you still live in a sustainable world . . 
"
[1 @ 27:19 - 27:32]

![Bill Gates Paper and CD](./Photographs/bill-gates-paper-and-cds.png)




[1]
2020 05 22 The Venus project Designing the future lecture
By TVPActivism
https://www.youtube.com/watch?v=wh1SaO1YJtY


[Written @ 21:09]

Notes @ Cool Sounding Clips @ Movie Clips
"They don't have the experience to understand this subject"
[1 @ 1:44:21 - 1:44:25]



[Written @ 20:21]

Notes @ Newly Learned Words and Phrases
"Sunny day in May"
[Filler content ? A lot of filler content in books ? "Bullshit" content in books . "A lot of bullshit in books"]
[1 @ 1:03:01]

Notes @ Newly Learned Words
Turbo Lungs
"In the lecture, he mentions out, that . . In the future, the humans will evolve from language . . and maybe we can develop . . turbo lungs . . so we will be . . instead of talking with words . . we will be like . . using sounds . . like animals . . dog things . . and all of that . . because we will develop turbo lungs . . and even our communication will be much better . . "
[1 @ 1:04:27 - 1:04:52]

[Written @ 19:05]


Notes @ Newly Created Words
Beucase
[Written @ September 13, 2020 @ 19:03]
[I accidentally typed . . "Beucase" . . instead of . . "Because" . . I'm not sure what this word . . "beucase" . . should mean . . at this time . . ]

Notes @ Newly Created Words
Beaucase
[Written @ September 13, 2020 @ 19:03]
[I accidentally typed . . "Beaucase" . . instead of . . "Beucase" . . I'm not sure what this word . . "Beaucase" . . should mean at this time . . although . . the word . . "Beaucase" . . does remind me . . of "Beautiful" . . in how it starts with . . "Beau" . . ]


[Written @ 18:56]

Notes @ Quotes
"
For every minute . . I transcribed . . it took me like . . 15 minutes . . That was my average . . Uh . . To listen to 1 minute . . I spent 15 minutes . . 

And I did that for 3 years . . So . . Whenever I had some free time . . that's what I did . . 

And I got the opportunity to listen to almost 400 other lectures . . 
"
[1 @ 6:12 - 6:35]

"
I have no illusions of being . . eh . . smart . . I just know . . this subject . . a lot . . Beucase I spent . . so much time on it . . that's the only thing . . This is my major skill in life . . Knowing about this subject . . and . . I'm very interested still . . to talk about it . . I really enjoy . . listening to Jacque Fresco . . And trying to talk about this subject . . And that's why we have these sessions . . 

I enjoy speaking to you and trying to assist you in any way I can . . 
"
[1 @ 6:48 - 7:19]



[1]
2020 08 28 The Venus project Event
By TVPActivism
https://www.youtube.com/watch?v=Ey7p1JueidA


[Written @ 17:36]

Notes @ Newly Learned Words
Top-line growth [1 @ 11:09]


[Written @ 17:32]


Notes @ Newly Learned Words
Back-office [1 @ 8:44]

[Written @ 17:17]


Notes @ Newly Learned Words
Esprit de Corps [1 @ 6:05]
The spirit of the group that makes the members want the group to succeed


[1]
Kellogg CEO discusses leading a company through the coronavirus pandemic and the impact on business
By Yahoo Finance
https://www.youtube.com/watch?v=54wlmiAECx8

