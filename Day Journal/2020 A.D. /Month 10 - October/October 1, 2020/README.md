
# Day Journal Entry - October 1, 2020

[Written @ 21:47]

Notes @ Newly Learned Words

Reactionary Energy
[A energy that is trapped in time . . ]

[1 @ 37:24 - 38:00]


[Written @ 20:52]

Notes @ Quotes

"
Things change . The behaviors . . the systems . . the . . s- . . the sys- . . the . . fundamental laws of the universe . . change . . when you are in a different vibration . . 

And maybe . . we should start with this . . from your own personal . . perspective . . because . . what does this all mean to you . . who cares . . ?

This is severely . . and . . s- . . s- . . s- . . totally . . important . . To . . you . . and . . your life . . Because . . we're talking about . . your emotional state . . we're talking about . . your capabilities as a person . . we're . . talking about . . your ability . . to . . rise . . into a . . state of . . near . . constant . . joy . . 

And . . the reason . . why Unicult . . focuses . . on rising into . . a state . . of . . near constant . . joy . . isn't . . just because . . we're . . like . . "it's . . good to be . . happy . . " . . no . . no . . 

It's way more than that . . Like . . when we . . when we . . ourselves . . rise into a state of unconditional love . . through radical action . . and through . . radical . . boundaries . . and through . . radical . . f- . . attention . . and . . focus . . on what we want to bloom and thrive . . 

And we learn how to listen to our own hearts . . Our . . own . . true . . hearts . . And . . our own . . compass . . 

We are changing . . the vibratory nature . . of our . . mind . . soul . . body . . world . . 

That I . . don't . . like . . living . . on Earth .

I tried to kill myself . . I was like . . "This place . . fucking . . sucks . . " . . I'm not interested . . in living . . in the world . . that I was born into . . 

My art . . is . . to . . change . . the . . entire . . vibration . . of Earth . . to be . . better . . for everyone . . 

How do we do that ? . . We do that by raising up . . [the speaker raises their finger to their forehead] . .  our own vibration . . our . . own . . consciousness . . our own . . vibratory . . state . . into . . one of joy . . 

Authentic joy . . that's . . that's . . listening . . to . . our . . true . . hearts . . as a compass . . 

Our hearts . . connect . to . . source . . energy . . Our hearts . . radiate . . resonate . . on the vibration . . of truth . .

And the world that I was born into . . is built on deception . . And deception . . is . . unhealthy . . for everyone . . Deception . . crea-se [*] . . creates . . false . . timelines . . So . . we'll . . talk about . . false timelines . . and we're gonna talk about . . all the ways . . 

There's so much . . there's so much . . to this . . I should have . . I should . . have . . prepared . . 

ha-ha-ha-ha-ha . . . It's okay . . we're gonna get through it . . And . . it's gonna be fun . . 

"
-- Unicole Unicron
[1 @ 28:15 - 30:33]


Notes @ Newly Learned Words

[*]
crea-se
crease
[Written @ October 1, 2020 @ 21:18]
[I thought I heard the word . . "crea-se" . . uh . . "crea-se" . . sounds like . . a typographical . . or . . verbal . . verbal-graphical . . error . . of . . "creates" . . well . . uh . . "crea-se" . . uh . . I'm not sure what this word should mean . . ]


Notes @ Newly Created Words

kei
[Written @ October 1, 2020 @ 21:08]
[I accidentally typed the word . . "kei" . . instead of the word . . "kill" . . I'm sorry . . I'm not sure what this word . . "kei" . . should mean at this time . . I also . . need . . to do . . research . . to see if this is indeed a new word . . be right back . . ]


cabai
[Written @ October 1, 2020 @ 21:00]
[I accidentally wrote the word . . "cabai" . . instead of the word . . "capabilities" . . I'm sorry . . I'm not sure what this word . . should . . mean . . at . . this time . . ]

abo
[Written @ October 1, 2020 @ 21:00]
[I accidentally wrote the word . . "abo" . . instead of the word . . "ability" . . I'm sorry . . I'm not sure . . what this word . . should mean . . at this time . . ]


[Written @ 20:45]

Notes @ Newly Learned Words

Self-Healing Ledger
[2 @ 3:26]

Ouroboros Cotsero
[2 @ 3:24]


Notes @ Fashion @ Hair Fashion

Golden Halo Ring
[1 @ 28:12]


Notes @ Quotes

"
Time spent in contemplation is always well worth it . 
"
-- Unicole Unicron
[1 @ 28:07 - 28:10]


[1]
CAM CHURCH - Time and Dimensional Implications
By Unicole Unicron
https://www.youtube.com/watch?v=qQPopojLbMw

[2]
Some Brief Thoughts
By Charles Hoskinson
https://www.youtube.com/watch?v=LrtaEPwb7lc


[Written @ 20:19]

Notes @ Story Ideas


There is an alien from another planet . . the people on this other planet . . are far more advanced . . than . . the people . . on the local planet . . the alien . . is . . uh . . maybe . . like a humanoid . . person . . or humanoid . . character . . 

uh . . the planet . . is like . . Earth . . uh . . the planet . . where . . uh . . the story takes place . . is an Earth-like planet . . with . . . . uh . . what we would call . . "pre-historic" . . people . . and so . . the level of technology . . isn't really as advanced . . as . . what we are familiar with today . . in terms of things . . like . . bicycles . . trains . . airplanes . . computers . . and so on . . 

Well . . this alien . . lands on . . this "pre-historic" . . or . . uh . . "primitive" . . Earth-dweller planet . . and . . there are people there . . on this planet . . that . . are . . inventors . . and very smart people . . like . . uh . . well . . uh . . they're smart . . uh . . well . . like . . Ben Goertzel . . I was inspired to think of this story . . by the story told in the video at [1] . . in how . . Ben Goertzel . . would like to have . . or would like . . an A.G.I. . or that they are very interested in creating . . and A.G.I. . that's an amazing endeavor . . and I started thinking to myself . . wow . . I wonder what it could have been like . . for . . a primitive group of people . . to want to make a bicycle . . what complications could they come across . . and . . so . . uh . . if they came across any complications . . in trying . . to make a bicycle for example . . which . . for people like uh . . in our version of history . . or in the history that . . a lot of people . . know about today . . well . . uh . . a bicycle . . uh . . might be a simple thing to make . . for us . . but for . . a more . . primitive . . Earth-like civilization . . without . . our background and history . . maybe things are more complicated . . 

Well . . what if there were an alien . . that came down to Earth . . and . . uh . . they are from a planet like ours . . in comparison . . to the primitive planet of people . . and so for example . . the alien person . . is from a planet . . where . . the people are familiar . . with bicycles . . and a lot of other advanced things as well . . 

Well . . then . . shouldn't the alien . . be able to . . help . . the . . primitive group of people . . to make their own bicycle ? . . or . . help make bicycles . . 

Uh . . I mean . . specifically . . for uh . . that . . group of smart people . . or for that one person . . who is like . . a primitive analog . . to Ben Goertzel . . then . . uh . . well . . what does it mean . . for them . . to ask the alien . . "can you help me make a bicycle?" . . . wouldn't that be funny ? . . if . . the alien said something like . . "I forgot " . . hahaha . . that would be a funny story . . if maybe uh . . it was . . uh . . true . . or something like that . . 

And so . . for example . . the alien could respond . . to the . . bicycle . . person . . the person who wants to invent to bicycle . . uh . . well . . if the . . person asked the alien . . "can you help me make a bicycle?" . . and the alien said . . "I'm sorry . . but . . I forgot . . how to make bicycles . . it was something about the air . . and the environment that I was in . . that really made it easy to . . conceive . . of the idea . . of how to make a bicycle . . " . . 

That could be an interesting storyline . . if a storyline . . started on a premise like this . . I think that could be an interesting . . story . . and so . . I was inspired to write it down . . and well . . uh . . the idea is . . that . . well . . while I was trying to think about . . "how do you make a bicycle?" . . well . . I myself . . came to the idea . . that . . uh . . well . . if I was asked that . . by a primitive Earth person . . I could . . uh . . make a lot of mistakes . . because . . I'm really not . . an expert . . in making bicycles . . and . . I only know . . what they . . uh . . look like . . and I don't know . . the schematics . . or even the material science . . that goes into making a bicycle . . I can imagine . . in my mind . . uh . . that maybe . . some metals are involved . . but I'm not sure what metals . . uh . . for example . . the metals that makes the metal mesh . . of the wheels of the bicycle . . uh . . and what if the person . . the primitive person on the planet . . didn't know what a wheel is ? . . uh . . "what is a wheel? " . . what if . . wheels really hadn't been developed yet ? . . well . . I am more familiar with wheels . . so I can . . or I feel like I could help in that regard . . uh . . I'm not really sure . . how to make . . rubber . . or . . rubber tires . . uh . . so . . rubber wheels . . well . . I suppose . . I could experiment . . and try to figure out what scientists before me . . uh . . in our own timeline . . of history . . what scientists . . before . . had done . . to make rubber materials . . uh . . well . . And then . . uh . . well . . we have the bearings . . to make the wheels turn . . and I'm really not an expert in how those work . . I know there's some sort of lubricant involved . . but . . uh . . where would you get those oils ? . . uh . . 

Well . . in general . . I really don't know how to make a bicycle . . and yet . . uh . . maybe . . I wouldn't take very long to develop it . . since . . I have the imagination . . involved . . uh . . the imagination . . and experience . . of . . what a bicycle looks like . . and so . . I can always . . play . . and experiment . . with ideas . . or . . play around with ideas . . 

uh . . well . . Maybe . . whereas . . it would . . or could take . . the primitive . . Earth person . . the person who doesn't have experience . . with . . bicycles . . or the . . elementary . . components . . of a bicycle . . or the components that make up a bicycle . . then . . maybe . . it could be the case . . that . . it could take me . . uh . . 2 years . . for example . . to help develop the background . . uh . . for myself . . personally . . because . . I'm not familiar with bicycle . . schematics . . and material science . . involved . . uh . . well . . but . . uh . . maybe . . the person . . that doesn't have all the other . . material science . . uh . . thoughts . . and ideas . . on how . . metals . . work . . or how metals are mined . . or how metals are collected . . and how metals . . are . . refined . . and . . molded . . into different shapes . . well . . so long as maybe . . the social environments allow this sort of experimentation . . to make progress . . in the . . uh . . in the . . experimenting . . of . . how . . to make a bicycle work . . uh . . well . . uh . . and . . I suppose . . so long as we're still interested in the topic . . then . . it's really possible that . . maybe . . it would take . . a few years . . as opposed to not really being sure . . when . . uh . . when . . uh . . for example . . uh . . well . . maybe . . the primitive person . . takes . . uh more time . . and doesn't necessarily know . . . . uh . . when . . or how long . . it will take . . to . . make a bicycle . . uh . . and maybe . . they don't even have an intuition . . on . . the . . hardcore . . or . . definite . . requirements . . of . . making . . the bicycle . . well . . uh . . at . . least . . with a person like myself . . having seen a bicycle . . and . . it's there in my mind . . thanks to the history . . of the people . . that I'm here with . . thanks to a lot of people . . working together . . in a lot of ways . . to make . . our history . . a reality . . and so . . we have . . beautiful technologies like bicycles . . 

Well . . thanks for our ancestors . . we have bicycles . . and . . so . . uh . . it's possibly . . an interesting story . . uh . . to explore the idea . . of how much . . information . . is . . forgotten . . by a human being . . when . . they . . move from a highly . . advanced . . civilization . . and . . they go . . to a . . more primitive civilization . . how could they work with the people ? . . uh . . and uh . . what things . . could they remember . . uh . . if they didn't study certain things . . those things could take them much longer to develop . . versus the things . . and ideas that they studied . . while they were . . on their . . more . . advanced . . civilization . . planet . . or something like that . . 

. . . story inspired by . . [1 @ 5:35 - 5:47]

"
This man . . wants a A.G.I. . You know . . he wakes up every day . . saying . . artificial . . general . . intelligence . . We are . . gonna make that happen . . 

That's why he created . . OpenCog . . That's why . . he created . . SingularityNET . . 
"
[1 @ 5:35 - 5:47]



Notes @ Newly Created Words

dones
[Written @ October 1, 2020 @ 20:45]
[I accidentally typed the word . . "dones" . . instead of . . "doesn't" . . I'm not sure what this world . . "dones" . . should mean at this time . . I'm sorry . . ]


[Written @ 20:13]

Notes @ Remebered Organizations

OpenCog
[1 @ 5:44]

Singularity Net
[1 @ 5:46]

Notes @ Newly Learned Facts

Ben Goertzel received his PhD. in Mathematics at age 21 . 
[1 @ 5:17]


[1]
Some Brief Thoughts
By Charles Hoskinson
https://www.youtube.com/watch?v=LrtaEPwb7lc

[Written @ 17:29]

[] incompleted tasks

[x] completed tasks

[cancelled] concelled tasks

Notes @ Newly Created Words


concelled
[Written @ October 2, 2020 @ 1:34]
[I accidentally typed the word . . "concelled" . . instead of . . the originally . . intended . . word . . "cancelled" . . I'm sorry . . I'm not sure . . what this word . . "concelled" . . should . . mean . . at this time . . I'm sorry . . ]

Octom
[Written @ October 2, 2020 @ 1:32]
[I accidentally typed the word . . "Octom" . . instead of . . "October" . . I'm not sure what this word . . "Octom" . . should . . mean . . at this . . time . . I'm sorry . . ]

Octeome
[Written @ October 2, 2020 @ 1:33]
[I accidentally typed the word . . "Octeome" . . Instead . . of . . typing . . the originally . . intended word . . "October" . . I'm not sure . . what this word . . "Octeome" . . should mean at this time . . I'm sorry . . ]


* [] Plan the tasks to complete
  - fix the issues on the command line interface
  - update account
    - update account profile picture
    - update acocunt name
    - update account username
      1. add to queue map of users requested for that username
      2. Check queue map to see if user is the first to attend the operation
      3. Delete the entry map from existing username to accountId
      4. Update the entry map for the new username to accountId
      5. Change the username of the account [Firebase validate other operations]
      6. Delete from queue map of users registered for that username
  - get account
    - get account by qr code id
  - create account
    - create a qr code id
    - add to account-qr-code-to-account-id-map
    - add to create-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - delete account
    - add to delete-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - update transaction
    - update transaction amount
    - update transaction text memorandum
  - apply universal basic income
    - create a transaction for all accounts to add digital currency amount
  - initialize-service-account (sign in)
    - with google
    - with email and password
  - uninitialize-service-account (sign out)
  - gitlab continuous integration / continuous deployment (ci / cd)
    - publish ecoin development website
    - publish ecoin daemon to development server
  - readme.md
    - demo usecase preview: update transaction settings
    - demo usecase preview: view transaction information
    - [x] *Publish Software Updates*: Publish software updates to fix issues or improve the existing codebase
    - [x] *Create Documentation*: Create project organization information documentation
    - [x] *Report Issues*: Report issues and flaws of the current software
    - [x] *Ideas*: Give us your ideas on how to improve the software
    - [x] *Share Knowledge*: share the project with your friends and family
    - [x] *Donate Resources*: donate to keep the demo project online for people to use
    - [x] *Inform the Community*: let us know about community participants that allow customers to transact using this digital currency
    - [x] *Use Your Imagination*: And many many more ways you can contribute
    - [x] progress report section shows a list of plans, and work in progress tasks
    - [x] features, limitations remove the plans and work in progress icons
    - [cancelled] add shopping mall and digital currency exchange section to api reference with a list of topic site names with "Planned but not yet started"
    - [x] update readme section with latest thoughts and ideas
      - introduction
      - [x] table of contents
        - demo usecase preview
        - installation
        - javascript runtime environment
        - javascript api reference
          - usecase: digital currency
          - usecase: online shopping mall
          - usecase: digital currency exchange
          - usecase: community participant list
        - about this project
        - benefits
        - features
        - limitations
        - progress report
        - contributors
        - related work
        - acknowledgements
        - license
  - html component
    - pay with digital currency button works on websites
  - usecase: community participant list
    - create community participant list for digital currency
    - create community participant list for digital currency exchange
      - digital currency exchanges that support ecoin transactions
    - create community participant list for online shopping mall
      - online shopping malls that support ecoin transactions
    - create community participant list for physical-location shops and stores
      - physical-location shopping places and stores like restaurants, shopping malls, shopping centers, grocery stores, food marts, small business store fronts, farmer's markets, etc.
  - update the styles of the desktop version of the website
    - account section
      - show the account section on the left
      - desktop version width should be not full width. [follow how it's done on the settings and about page]
      - desktop header tabs (v-tabs) should be condensed and not wide screen.
    - account statistics
      - show the account statistics on the right (allow vertical scroll)
    - community participants: show a list using vuetify table
  - settings page
    - 
  - add a `qr code` button
    - [header] Your Account QR Code
      - [subheader] Account QR Code for `account name`
    - [camera icon] Capture Account QR Code
    - find a digital currency account by its qr code by taking a photograph of the account qr code
  - network status page
    - number of ecoin distributed (timeline graph)
    - number of created accounts (timeline graph)
    - number of created transactions (timeline graph)
    - number of created community participants (timeline graph)
    - newly distributed ecoin (live updating)
    - newly created accounts list (live updating)
    - newly created transactions list (live updating)
    - newly created community participants (timeline graph)
    - total number of ecoin created (running sum of overall currency amount)
    - total number of accounts created (running sum of overall accounts amount)
    - total number of transactions created (running sum of overall transactions amount)
    - total number of community participants created (running sum of overall community participants)
  - about page
    - update the "Why does Ecoin exist?" answer 
      - "[A] A short answer like this is meant to encure your curiosity and sponsor your research project to learn more about these wounderous individuals. ;)"
      - enumerate the long answer clause



Notes @ Newly Created Words

protj
[Written @ October 1, 2020 @ 18:16]
[I accidentally typed the word . . "protj" . . instead of the word . . "project" . . I'm not sure what this word . . "protj" . . should mean . . at this time . . I'm . . sorry . . ]


accon
[Written @ October 1, 2020 @ 18:16]
[I accidentally typed the word . . "accon" . . while . . trying . . to type the word . . "account" . . I'm not sure . . what this word . . "accon" . . should mean . . at this time . . I'm sorry . .]

variat
[Written @ October 1, 2020 @ 17:37]
[I accidentally typed the word . . "variat" . . instead of the word . . "variable" . . I'm not sure . . what this word . . "variat" . . should mean at this time . . ]


[Written @ 17:25]

Live Stream Checklist for October 1, 2020 @ 23:00

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Ru's Piano | ACG Music 儒儒的宅鋼琴時間 ピアノ By Ru's Piano Ru味春捲
 https://www.youtube.com/playlist?list=PLUnBm8KiAXgUfJCNbFMgZccbdkwkxMyuL
 - Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4
 - Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc

Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [failed] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - [02:43:50] I checked the live stream . . a few minutes ago . . and . . it seems that the video . . had . . stopped . . more than 1 hour ago . . I'm . . very sorry . . I forgot to check the live stream status . . so this . . task is listed as [failed] . . but it's not always that easy to succeed at this task . . since . . uh . . I'm really not used to . . having to do this . . some days . . the live streaming goes . . fine . . for many hours . . hmm . . so . . well . . uh . . I'm sorry . . uh . . well . . I'll . . uh . . see more . . what I can do . . in this regard . . I'm sorry . . 

Salutations

* [] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [x] Talk about the possibilities for the next live stream


[Written @ 1:37]

Notes @ Newly Created Words

Optober
[Written @ October 1, 2020 @ 1:38]
[I accidentally typed the word . . "Opt" . . while . . trying . . to type the word . . "October" . . I'm not sure what this word . . "Optober" . . should mean at this time . . and yet . . this word . . "Optober" . . is . . a possible . . continuation . . of the . . original . . word . . "Opt" . . that I typed . . and so . . uh . . well . . in trying to type the word . . "October" . . "Optober" . . is . . uh . . a theoretical . . version . . of the word . . "October" . . uh . . and so . . history . . could be . . different . . if . . Optober . . were the way . . the . . uh . . month . . of . . October . . were . . said . . or . . uh . . pronounced . . Uh . . I'm not sure . . what this word . . "Optober" . . should . . mean . . at this time . . [a few minutes later . . after . . searching . . the meaning of . . "opto" . .] ahh right . . it seems that . . opto- is a Greek . . prefix . . meaning . . "vision" . . ahh . . so then . . uh . . "Opto" . . uh . . is . . uh . . well . . it has a meaning . . associated . . with the term . . already . . hmm . . so . . Optober . . could be . . the month . . of vision ? . . uh . . I'm not sure . . uh . . there could be other meanings . . I'm not sure . . if . . ber . . means anything . . uh . . so . . uh . . maybe . . the month of vision . . isn't necessarily . . a . . uh . . way . . to . . uh . . suggest a definition . . for this term . . ]

[Written @ 1:29]

Notes @ Newly Discovered Music
Notes @ Cool Video Sequence

"Come to the Earth . . Song of the Earth . . Come to the Earth . . and sing . . 

We are . . one with the sun . We are one with the sun . . We are one with the sun . . 
"
[1 @ 17:20 - 20:10]


Notes @ Newly Learned Words

Prime Machina
[1 @ 16:50]


[Written @ 1:27]

Notes @ Newly Discovered YouTube Channels

The Bitcoin Family
https://www.youtube.com/c/TheBitcoinfamily/videos



[Written @ 1:22]

Notes @ Newly Learned Terminology Usecase

"Candles" as the bars in a box and whiskers plot

[Candles . . or bars . . for the name of the . . vertical . . rectangles . . of a box-and-whiskers plot . . ]
[1 @ 13:12]


[Written @ 1:00]

Notes @ Newly Learned Words / Terms

Bamming Energetic
[1 @ 8:44]

Shit Fly
[1 @ 10:13]

Notes @ Newly Learned Words / Terms

Copy Trading
[1 @ 5:27]

Notes @ Cool Video Sequence

"
Bam! . . Indeed .
"
[1 @ 6:29 - 6:33]


[1]
WOOW!!! IF BITCOIN HISTORY [chart] REPEATS WE WILL SEE +$200.000 in 2021!! Check the BTC Machina!!
By The Bitcoin Family
https://www.youtube.com/watch?v=kTDCqZFFr-M


Notes @ Newly Created Words 

Seuq
[Written @ October 1, 2020 @ 1:03]
[I accidentally typed the word . . "seuq" . . instead of the word . . "sequence" . . or . . "sequences" . . I'm not sure . . what this word . . "Seuq" . . should mean at this time . . ]


Sequ
[Written @ October 1, 2020 @ 1:03]
[I accidentally . . typed . . the word . . "sequ" . . instead of the word . . "Seuq" . . I'm not . . sure . . what this word . . "Sequ" . . should mean at this time . . I'm sorry . . ]

Segu
[Written @ October 1, 2020 @ 1:05]
[I accidentally typed the word . . "segu" . . instead of the word . . "sequ" . . I'm not . . sure . . what this word . . "Segu" . . should mean . . at this time . . I'm sorry . . "Quit" . . that is a word . . that came to mind . . just now . . uh . . as I am writing this message . . "Quit . . I'm sorry" . . that could be the meaning of this word .]


[Written @ 0:40]

Notes @ Newly Created Words

Octeo
[Written @ October 1, 2020 @ 0:41]
[I accidentally typed the word . . "Octeo" . . isntea]

isntea
[Written @ October 1, 2020 @ 0:41]
[I accidentally typed the word . . "isntea" . . instead of . . "instead" . . as in . . "instead of" . . I'm not sure . . what this word . . "isntea" . . should . . mean . . at this time . . I'm sorry . . ]

Wrii
[Written @ October 1, 2020 @ 0:42]
[I accidentally typed the word . . "Wrii" . . instead of the word . . "Written" . . I'm . . not . . sure . . what . . this . . word . . "Wrii" . . should mean at this time . . I'm . . . . hmm . . well . . hmm . . now . . uh . . well . . a few seconds . . after I've written . . this . . uh . . message . . or paragraph . . all of a sudden . . I got . . a very strange idea . . to me . . that came up . . in my memory structure . . hmm . . let's see . . do you know . . how . . people talk to one another . . and then . . the option for . . someone to respond . . is there . . and there are . . a variety of ways for someone to respond . . to the way someone speaks to them . . "ahh yes . . I agree" . . "ahh . . no . . I'm sorry . . that's incorrect" . . "ahh . . right . . you're right . . thank you for the information . . " . . "ahh . . no . . I'm sorry . . I disagree" . . "oh . . yes . . that's quite considerate of you . . yes indeed . . that is really indeed meaningful information . . thank you very much . ." . . uh . . you know . . uh . . how in a conversation . . all of these various . . directions . . can be discussed . . you could . . stop . . theoretically . . uh . . you have the possibility . . or the opportunity to stop . . yourself . . and look around . . to see . . uh . . what the possibilities are . . that you could consider next . . hmm . . well . . In my mind . . I had . . a flash . . of an image . . of . . someone . . standing . . in the desert . . uh . . someone is standing in the desert . . uh . . it's like . . uh . . the way . . uh . . I'm not sure . . how exactly . . to interpret this . . image . . and also the accompanying . . uh . . sound-thought-feeling . . that . . I had . . uh . . like a . . gut . . feeling . . uh . . it was . . really quick . . it occurred . . in . . a few . . uh . milliseconds . . or . . fractions of a second . . this thought . . and feeling . . of . . this person . . standing in the desert . . and . . uh . . the idea being . . that . . you have so many ways to go . . uh . . and that maybe . . uh . -_- . . I'm not really a fan of this interpretation . . but I'll write it down anyway . . uh . . so . . uh . . a desert is meant to represent . . the emptiness . . uh . . well . . like . . uh . . maybe . . there aren't as many options . . as there appear to be . . uh . . and so for example . . when you have . . the option to . . uh . . say . . "yes . . yes . . that's exactly what I meant . . you got it . . " . . uh . . a person . . responding in this way . . could have . . also . . said . . instead . . "ahh . . yea . . well . . uh . . you're . . uh . . not really understanding what . . I'm saying here . . " . . uh . . well . . this uh . . is also . . like a response . . uh . . that is . . like . . in the map . . of options that could have been . . uh . . responded as . . so for example . . uh . . maybe . . there is a characteristic . . response map . . that each of us . . are . . uh . . navigating . . in the way . . we participate . . in the world . . and . . those characteristic maps . . overlap . . uh . . with others . . and so . . uh . . together . . that characteristic . . map . . uh . . grouping . . or . . cogregation . . is the thing . . that we experience . . as time . . and . . the thing . . that we interpret . . as . . a common . . history . . or . . isographic . . chart . . of . . our present moment . . and the experience . . that each of us are having . . uh . . this map . . uh . . of possibilities . . uh . . well . . those possibilities . . are on a map . . uh . . and so . . you can overlap . . and so . . that map of possibilities . . is uh . . uh . . if it exists . . uh . . maybe . . really . . there are no possibilities . . and . . only . . uh . . one thing . . that is . . the map . . uh . . and so . . the idea . . of having choice . . uh . . uh . . you're not able to choose . . it is only . . uh . . you . . uh . . there . . are the possibilities . . and they all are possible . . on this map . . of possibilities . . so . . maybe . . did you really have the possibility ? . . or . . it appeared that the possibility was there . . when indeed the map shows all the possibilities . . which makes it appear that . . well . . uh . . there are only possibilities . . and so . . uh . . maybe . . the possibility . . of having possibilities . . is a possible . . possibility . . uh . . [a few minutes later] . . uh . . well . . if Wrii . . uh . . uh . . maybe . . wrii . . . . maybe . . wrii . .]



informaiton
[Written @ October 1, 2020 @ approximately 0:57]
[I accidentally typed the word . . "informaiton" . . instead of the word . . "information" . . I'm not sure what this word . . "informaiton" . . should mean at this time . . ]

thatn
[Written @ October 1, 2020 @ approximately 0:58]
[I accidentally typed the word word . . "thatn" . . instead of the word . . "thank" . . uh . . I'm not . . sure . . what . . this word . . "thatn" . . should mean . . at this time . . I'm sorry . . ]


