
# Day Journal Entry - October 16, 2020


[Written @ 23:56]

Approximate amount of time . . spent working . . for the second . . live stream for today . . For Ecoin #339 https://www.youtube.com/watch?v=JPj5Lc9Ny78


04:44:06.476

#1
04:44:06.476
Pause


[Written @ 16:28]

Approximate amount of time spent working . . while . . we thought . . we were live streaming . . but . . it seems like . . the network connection stopped . . around . . 1 hour and 16 minutes . . into the video . . 

04:26:50.817

#1
04:26:50.817
Pause

[Written @ 12:04]

Live Stream Checklist

Greetings

* [x] Greet the viewers
* [] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [] Practice a breathing exercise for 5 - 15 minutes
  - 


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Days Of My Life...Back To The City...(Story 50) By Isabel Paige https://www.youtube.com/watch?v=bKQ_VMQPb2M
  - Ardas Bhaee ⋄ Mirabai Ceiba ⋄ Snatam Kaur ⋄ Jai-Jagdeesh ⋄ Simrit Kaur ⋄ Sirgun Kaur ⋄ Singh Kaur By M U S I Q A A https://www.youtube.com/watch?v=es_gSCmswBg
  - Ru's Piano | ACG Music 儒儒的宅鋼琴時間 ピアノ By Ru's Piano Ru味春捲
 https://www.youtube.com/playlist?list=PLUnBm8KiAXgUfJCNbFMgZccbdkwkxMyuL
  - Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4
  - Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - Snatam Kaur and Ajeet Kaur ⋄ Sacred Chants By M U S I Q A A https://www.youtube.com/watch?v=xoBpkz_yay8&pbjreload=101
  - Touhou Project「Lost Word Chronicle」Ru's Piano x @Kathie Violin 黃品舒 By Ru's Piano Ru味春捲
 https://www.youtube.com/watch?v=gvBYTwmoaH4&list=PLUnBm8KiAXgXoVTwJScG5o3BEgl2VVNyz
 - Relaxing Bossa Nova & Jazz Music For Study - Smooth Jazz Music - Background Music By Cafe Music BGM Channel https://www.youtube.com/watch?v=Y-JQ-RCyPpQ&ab_channel=CafeMusicBGMchannel
 - Bossa Nova Covers Of Popular Songs 2020 https://www.youtube.com/watch?v=IkGS89JCBdY By Diva Channel


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
  - (1) YouTube LiveStream Chat
  - (2) YouTube + Firefox Web Browser (Picture-in-Picture Mode)
  - (3) iTerm
  - (4) Firefox Web Browser + https://www.timeanddate.com/
  - (5) Brave, Firefox, Tor Browser
  - (6) Visual Studio Code
  - (7) Express VPN
  - (8) Visual Studio Code . Previously: "Notes" on my macbook

Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - [0:40:00] everything looks good.
  - [1:15:46] everything looks good.
  - 

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream



. . . 


* Things that are planned to be completed


- [] http-server/data-structures/provider-service-emulator-http-server-proxy
  - [] lunr service http server proxy
  - [] firebase emulator service http server proxy
    - [] initialize provider service
      - [] initialize provider service by http server proxy
      - [] initialize provider service for firebase algorithms
    - [x] create item
    - [x] get item
    - [] get item list
    - [] update item
    - [] add item event listener
    - [] remove item event listener
- [] daemon/data-structures/administrator-daemon
- [] daemon/data-structures/service-activity-simulator-daemon
- 





- add multi-language support [internationalization; i18n]
- add language support button
- fix the issues on the command line interface
- update account
  - update firebase auth uid for security
  - update account profile picture
  - update acocunt name
  - update account username
    1. add to queue map of users requested for that username
    2. Check queue map to see if user is the first to attend the operation
    3. Delete the entry map from existing username to accountId
    4. Update the entry map for the new username to accountId
    5. Change the username of the account [Firebase validate other operations]
    6. Delete from queue map of users registered for that username
  - update account by adding blocked account
    - can no longer send currency to this account
    - can no longer receive currency from this account // ???
  - update account by removing blocked account
    - 
  - update account behavior statistics // for security, fraud detection, transaction overuse
    - admin only read and write
    - measure the frequency of creating a transaction
    - measure the transaction amount median
    - freeze an account in case of abnormal account use ??
- get account
  - get account by qr code id
- create account
  - create a qr code id
  - add to account-qr-code-to-account-id-map
  - add to create-account-variable-tree
    - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- delete account
  - add to delete-account-variable-tree
    - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- update transaction
  - update transaction amount
  - update transaction text memorandum
  - update transaction by adding a like
  - update transaction by removing a like
  - update transaction by adding a dislike
  - update transaction by removing a dislike
  - update transaction by adding a comment
  - update transaction by removing a comment
- create transaction
  - include permissioned contact information
  - text memorandum
  - private text memorandum
  - 
- apply universal basic income
  - create a transaction for all accounts to add digital currency amount
- initialize-service-account (sign in)
  - with google
  - with email and password
- uninitialize-service-account (sign out)
- gitlab continuous integration / continuous deployment (ci / cd)
  - publish ecoin daemon to development server
- readme.md
  - demo usecase preview: update transaction settings
  - demo usecase preview: view transaction information
- html component
  - pay with digital currency button works on websites
  - create an html component with an initial property object
  - return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
  - return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
- digital currency account data structure
  - business acount description
  - does this account represent a copy or fork of the ecoin source code?
    - how many accounts does this currency have?
    - how many transactions does this currency have?
  - is a digital currency exchange that transacts using ecoin
  - is business account, selling products and services using ecoin
    - is providing currency exchange service
      - supported currency list
    - is providing products
      - product category list, product price ranges
    - is providing services
    - is providing other
  - is a physical location shop or store
    - restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
  - contact information variable tree (email, phone number, website url, physical address etc.)
    - permission rule on variable tree item: Public, Private, Permissioned
  - account statistics variable tree (set fields like 'number of employees' and other information)
  - are you seeking employment?
    - employment skills list
    - sought after ecoin payment amount per time period
  - are you an employer?
    - are you hiring right now
    - available position variable tree
      - ecoin payed per time period
      - sought after skills list
      - position name, time description (ie. 30 hours / week)
  - 
- update the styles of the desktop version of the website
  - account section
    - show the account section on the left
    - desktop version width should be not full width. [follow how it's done on the settings and about page]
    - desktop header tabs (v-tabs) should be condensed and not wide screen.
  - account statistics
    - show the account statistics on the right (allow vertical scroll)
  - community participants: show a list using vuetify table
- settings page
  - add light and dark theme setting
- add a `qr code` button
  - [header] Your Account QR Code
    - [subheader] Account QR Code for `account name`
  - [camera icon] Capture Account QR Code
  - find a digital currency account by its qr code by taking a photograph of the account qr code
- network statistics page
  - number of ecoin distributed (timeline graph)
  - number of created accounts (timeline graph)
    - number of created business accounts (timeline graph)
  - number of created transactions (timeline graph)
  - newly distributed ecoin (live updating)
  - newly created accounts list (live updating)
    - number of created business accounts (timeline graph)
  - newly created transactions list (live updating)
  - total number of ecoin created (running sum of overall currency amount)
  - total number of accounts created (running sum of overall accounts amount)
    - total number of business accounts created (running sum of overall community participants)
  - total number of transactions created (running sum of overall transactions amount)
  - flights gl map to show transactions in realtime
- account page
  - navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
  - add settings icon button
  - transaction list should show "view transaction button"
    - open transaction dialog
  - transaction list should show "repeat transaction button"
  - like to dislike ratio timeline graph
  - number of accounts transacted with timeline graph
  - currency amount transacted timeline graph
  - number of transactions timeline graph
    - ratio of likes from recipient, sender
    - ratio of dislikes from recipient, sender
  - calendar heatmap of transactions created
  - find employers to work with
    - work with businesses that pay in Ecoin
  - find businesses to shop with
    - shop with businesses that transact using Ecoin
  - find currency exchanges to trade with
    - exchange Ecoin for other currencies
- about page
  - update the "Why does Ecoin exist?" answer 
    - "[A] A short answer like this is meant to encure your curiosity and sponsor your research project to learn more about these wounderous individuals. ;)"
    - enumerate the long answer clause
- apply transaction list
  - Recurring Transactions as Processed set to false
    - dateTobeProcessed = d
    - numberOfTimesProcessed = n
    - millisecondDelayBetweenProcessing = m
    - currenctDateForTransaction = c
    - const isAlreadyProcessedTimePeriod = d + n * m > c
- website style / theme
  - research how to improve account page style for desktop
  - research how to improve settings page style for desktop
  - research how to style the network status page
  - 
- readme.md
  - add the names, and website urls of the software dependencies items (use the same layout as the related work section)
  - remove list index from the software dependencies table
  - add appended list to each list item in the table of contents
  - add "completed" text to the features description text in the readme.md file
  - fix typo to be "for storing data on remote computers", in software dependencies / database provider / firebase realtime database section
  - add the domain name registration service used for https://ecoin369.com . . GoDaddy
  - add website library "echarts" (for logistics measurement display)
  - add expectation definitions measurement tools: mocha, chai
  - add project compilation: node.js, npm
  - add prepaid credit card service (with anonymous support): Bluebird, American Express
  - 
- digital-currency-business data structure
  - number of employees, are you hiring at the moment (and do you pay in Ecoin), salary range, number of available positions, business type, are remote positions available, 
- firebase cloud firestore
    - initialize the algorithms and expectation definitions
    - initialize the security rules for creating, updating and deleting
    - 






