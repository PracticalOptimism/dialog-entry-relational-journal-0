

# Day Journal Entry - October 29, 2020

[Written @ 23:58]

Notes @ YouTube Channels That I watch frequently or look out for updates from

Eugenia Cooney 
https://www.youtube.com/c/eugeniacooney/videos


Amanda Flaker
https://www.youtube.com/c/AmandaFlaker/videos


Isabel Paige
https://www.youtube.com/c/IsabelPaige/videos


Code with Ania Kubów #JavaScriptGames
https://www.youtube.com/c/AniaKub%C3%B3w/videos


OMGenomics
https://www.youtube.com/c/OMGenomics/videos

Unicole Unicron
https://www.youtube.com/c/UnicoleUnicron/videos

Clara Dao
https://www.youtube.com/c/CLARADAO/videos


Crypto Casey
https://www.youtube.com/c/CryptoCasey/videos


Paget Kagy
https://www.youtube.com/c/PagetKagy27/videos

Claudia Walsh 
https://www.youtube.com/c/ClaudiaWalsh1/videos


Kennedy Walsh 
https://www.youtube.com/c/KennedyWalsh/videos


Akiane Kramarik
https://www.youtube.com/user/akianeart/videos





[Written @ 23:48]

Notes @ Newly Created Words

Phorot
[Written @ October 29, 2020 @ 23:49]
[I accidentally typed the word . . "Phorot" . . instead of typing the word . . "Photographs" . . I'm sorry . . I'm not sure . . what this word . . "Phorot" . . should mean . . at this time]



[Written @ 23:43]


Notes @ People that look similar and have similar names

Dr Clara Sousa-Silva [1]

Silvia Perea [2]


Dr Clara Sousa-Silva Photo #1
![Dr. Clara Sousa-Silva](./Photographs/dr-clara-sousa-silva.png)



Dr Clara Sousa-Silva Photo #2
![Dr. Clara Sousa-Silva photo #2](./Photographs/dr-clara-sousa-silva-2.png)



Silvia Perea Photo #1
![Silvia Perea](./Photographs/silvia-perea.jpg)


Silvia Perea Photo #2
![Silvia Perea photo #2](./Photographs/silvia-perea-2.jpg)






[This thought was inspired by the appearance of the person in the thumbnail of the video at [1] . . I read the name of the person . . in the video title . . and was inspired to wonder if the person could also relate to the person that I'm familiar with who is "Silvia Perea" who I've read about from The Venus Project website at [2] . . Silva . . and Silvia . . seem quite similar . . and the people also appear quite similar from what I was thinking in my mind . . but maybe it's a mistake . . I'm sorry]




[1]
Everything We Know So Far About Phosphine On Venus feat. Dr Clara Sousa-Silva
By Astrum
https://www.youtube.com/watch?v=K2A8aqHvA54


[2]
GQ: The Project That Seeks to Save the World Through Technology
Posted on April 30, 2017 by The Venus Project
https://www.thevenusproject.com/gq-project-seeks-save-world-technology/


[Written @ 23:39]

Notes @ Newly Discovered YouTube Channels


Astrum
https://www.youtube.com/c/astrumspace/videos
[Discovered by: Everything We Know So Far About Phosphine On Venus feat. Dr Clara Sousa-Silva By Astrum https://www.youtube.com/watch?v=K2A8aqHvA54 appearing in the YouTube search results for "roxanne meadows"]


[Written @ 23:25]

Notes @ Newly Discovered Names

Crysto
crysto
[Written @ October 29, 2020 @ 23:25]
[I accidentally typed the word . . "crysto" . . or the name . . "crysto" . . instead of typing . . the name . . "Crypto" as in . . "Cryto Casey" . . :O . . ]



[Written @ 1:33]

Notes @ Newly Created Words

anre
[Written @ October 29, 2020 @ 1:55]
[I accidentally typed the word . . "anre" . . instead of typing the word . . "and" . . I'm sorry . . I'm not sure . . what this word . . "anre" . . should mean . . at this time . . after a searching on the DuckDuckGo search engine . . it seems that . . anre . . that . . Anre . . is a name . . that . . Anre . . is a name . . of a person . . or that . . it could be the name . . of . . a . . person . . ]

statitistics
[Written @ October 29, 2020 @ 1:59]
[I accidentally typed the word . . "statitistics" . . instead of typing the word . . "statistics" . . I'm sorry . . I'm not sure . . what this word . . "statitistics" . . should mean . . at this time . . ]



### Things that are planned to be completed

Use the mobile-first design strategy:

- [] Update the about page
  - [] add text: "loading content . . . " while lazy load is incomplete

- [] Update the your account page
  - [] 
  - [] add a account description section
    - [] account id
    - [] account name
    - [] account username
    - [] account description
    - [] physical location address (country, city/town/province)
    - [] account contact list (email address, website url list, social media url list, physical mail address list, other items)
    - [] account business product listing
    - [] account currency exchange currency listing
    - [] account employer positions listing
    - [] 
  - [] use the name "account statistics" to contain "account balance history" and "transaction amount history" (received and sent transaction amount)
  - [] add a "find shops to buy products or services" section
    - [] add text: find shops that accept ecoin such as "individuals", "online commerce stores", "restaurants", "apartments", "house salesplaces", "gymnasiums", "sports recreation facilities" and more
  - [] add a "find employers to work with" section
    - [] add text: find employers such as "individuals", "schools", "technology companies", "hospitals", "government agencies", "non-profit organizations", "local businesses" and more
  - [] add a "find currency exchanges to trade with" section
    - [] add text: find digital currency exchanges to trade Ecoin for fiat currencies (ie. U.S. Dollar, Euro, Yuan, and more) or alternative digital currencies
  - [] add a "find ecoin alternatives" section
    - [] add text: find alternative digital currencies to send and receive money with
  - [] 


- [] Update the network status page
  - [] add a "digital currency statistics" section
  - [] add a "account statistics" section
  - [] add a "transaction statistics" section
  - [] add a globe activity flight map section



[Written @ 1:26]

### Things that are planned to be completed
[Copy-pasted from October 27, 2020]

* Things that are planned to be completed

- [] http-server/data-structures/provider-service-emulator-http-server-proxy
  - [] lunr service http server proxy
  - [] firebase emulator service http server proxy
    - [] initialize provider service
      - [] initialize provider service by http server proxy
      - [] initialize provider service for firebase algorithms
    - [x] create item
    - [x] get item
    - [] get item list
    - [] update item
    - [] add item event listener
    - [] remove item event listener
- [] daemon/data-structures/administrator-daemon
- [] daemon/data-structures/service-activity-simulator-daemon
- 



- [] add multi-language support [internationalization; i18n]
- [] add language support button
- [] fix the issues on the command line interface
- [] update account
  - [] update firebase auth uid for security
  - [] update account profile picture
  - [] update acocunt name
  - [] update account username
    1. [] add to queue map of users requested for that username
    2. [] Check queue map to see if user is the first to attend the operation
    3. [] Delete the entry map from existing username to accountId
    4. [] Update the entry map for the new username to accountId
    5. [] Change the username of the account [Firebase validate other operations]
    6. [] Delete from queue map of users registered for that username
  - [] update account by adding blocked account
    - [] can no longer send currency to this account
    - [] can no longer receive currency from this account // ???
  - [] update account by removing blocked account
    - 
  - [] update account behavior statistics // for security, fraud detection, transaction overuse
    - [] admin only read and write
    - [] measure the frequency of creating a transaction
    - [] measure the transaction amount median
    - [] freeze an account in case of abnormal account use ??
- [] get account
  - [] get account by qr code id
- [] create account
  - [] create a qr code id
  - [] add to account-qr-code-to-account-id-map
  - [] add to create-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] delete account
  - [] add to delete-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] update transaction
  - [] update transaction amount
  - [] update transaction text memorandum
  - [] update transaction by adding a like
  - [] update transaction by removing a like
  - [] update transaction by adding a dislike
  - [] update transaction by removing a dislike
  - [] update transaction by adding a comment
  - [] update transaction by removing a comment
- [] create transaction
  - [] include permissioned contact information
  - [] text memorandum
  - [] private text memorandum
  - 
- [] apply universal basic income
  - [] create a transaction for all accounts to add digital currency amount
- [] initialize-service-account (sign in)
  - [] with google
  - [] with email and password
- [] uninitialize-service-account (sign out)
- [] gitlab continuous integration / continuous deployment (ci / cd)
  - [] publish ecoin daemon to development server
- [] readme.md
  - [] demo usecase preview: update transaction settings
  - [] demo usecase preview: view transaction information
- [] html component
  - [] pay with digital currency button works on websites
  - [] create an html component with an initial property object
  - [] return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
  - [] return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
- [] digital currency account data structure
  - [] business acount description
  - [] does this account represent a copy or fork of the ecoin source code?
    - [] how many accounts does this currency have?
    - [] how many transactions does this currency have?
  - [] is a digital currency exchange that transacts using ecoin
  - [] is business account, selling products and services using ecoin
    - [] is providing currency exchange service
      - [] supported currency list
    - [] is providing products
      - [] product category list, product price ranges
    - [] is providing services
    - [] is providing other
  - [] is a physical location shop or store
    - [] restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
  - [] contact information variable tree (email, phone number, website url, physical address etc.)
    - [] permission rule on variable tree item: Public, Private, Permissioned
  - [] account statistics variable tree (set fields like 'number of employees' and other information)
  - [] are you seeking employment?
    - [] employment skills list
    - [] sought after ecoin payment amount per time period
  - [] are you an employer?
    - [] are you hiring right now
    - [] available position variable tree
      - [] ecoin payed per time period
      - [] sought after skills list
      - [] position name, time description (ie. 30 hours / week)
  - 
- [] update the styles of the desktop version of the website
  - [] account section
    - [] show the account section on the left
    - [] desktop version width should be not full width. [follow how it's done on the settings and about page]
    - [] desktop header tabs (v-tabs) should be condensed and not wide screen.
  - [] account statistics
    - [] show the account statistics on the right (allow vertical scroll)
  - [] community participants: show a list using vuetify table
- [] settings page
  - [] add light and dark theme setting
- [] add a `qr code` button
  - [] [header] Your Account QR Code
    - [] [subheader] Account QR Code for `account name`
  - [] [camera icon] Capture Account QR Code
  - [] find a digital currency account by its qr code by taking a photograph of the account qr code
- [] network statistics page
  - [] number of ecoin distributed (timeline graph)
  - [] number of created accounts (timeline graph)
    - [] number of created business accounts (timeline graph)
  - [] number of created transactions (timeline graph)
  - [] newly distributed ecoin (live updating)
  - [] newly created accounts list (live updating)
    - [] number of created business accounts (timeline graph)
  - [] newly created transactions list (live updating)
  - [] total number of ecoin created (running sum of overall currency amount)
  - [] total number of accounts created (running sum of overall accounts amount)
    - [] total number of business accounts created (running sum of overall community participants)
  - [] total number of transactions created (running sum of overall transactions amount)
  - [] flights gl map to show transactions in realtime
- [] account page
  - [] navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
  - [] add settings icon button
  - [] transaction list should show "view transaction button"
    - [] open transaction dialog
  - [] transaction list should show "repeat transaction button"
  - [] like to dislike ratio timeline graph
  - [] number of accounts transacted with timeline graph
  - [] currency amount transacted timeline graph
  - [] number of transactions timeline graph
    - [] ratio of likes from recipient, sender
    - [] ratio of dislikes from recipient, sender
  - [] calendar heatmap of transactions created
  - [] find employers to work with
    - [] work with businesses that pay in Ecoin
  - [] find businesses to shop with
    - [] shop with businesses that transact using Ecoin
  - [] find currency exchanges to trade with
    - [] exchange Ecoin for other currencies
- [] about page
  - [] update the "Why does Ecoin exist?" answer 
    - [] enumerate the long answer clause
    - [] change the long answer to be more friendly to read and not so presumptious of minority and majority logic thinking
- [] apply transaction list
  - [] Recurring Transactions as Processed set to false
    - [] dateTobeProcessed = d
    - [] numberOfTimesProcessed = n
    - [] millisecondDelayBetweenProcessing = m
    - [] currenctDateForTransaction = c
    - [] const isAlreadyProcessedTimePeriod = d + n * m > c
- [] website style / theme
  - [] research how to improve account page style for desktop
  - [] research how to improve settings page style for desktop
  - [] research how to style the network status page
  - 
- [] readme.md
  - [] add the names, and website urls of the software dependencies items (use the same layout as the related work section)
  - [] remove list index from the software dependencies table
  - [] add appended list to each list item in the table of contents
  - [] add "completed" text to the features description text in the readme.md file
  - [] fix typo to be "for storing data on remote computers", in software dependencies / database provider / firebase realtime database section
  - [] add the domain name registration service used for https://ecoin369.com . . GoDaddy
  - [] add website library "echarts" (for logistics measurement display)
  - [] add expectation definitions measurement tools: mocha, chai
  - [] add project compilation: node.js, npm
  - [] add prepaid credit card service (with anonymous support): Bluebird, American Express
  - [] add the project authors for the software dependency list
  - [] update the software depenencies message to change the open source software message
  - [] consider adding a frequently asked questions section to the readme
  - [] add a guide to get started in publishing your own ecoin-based digital currency
    - [] easily change the name, image icon, resource urls and project description
      - [] new directory for ease of use ? or precompiled/@project ? 
    - [] how to add firebase credentials for development and production environments
      - [] create firebase api token using the command line interface tool
    - [] how to add algolia credentials for development and production environments
    - [] how to publish the project service website
    - [] how to publish the administrator daemon
  - [] add a guide to get started with the development of the project source code
    - [] how to install nodejs and npm
    - [] how to install the firebase emulator
    - [] how to install ngrok
    - [] how to start the administrator daemon
    - [] how to start the firebase emulator
    - [] how to start the service provider http server proxy
    - [] how to start the development server for the project service website
    - [] how to run a measurement of the expectation definitions (also known as unit tests but also includes integration tests)
    - [] how to start the service activity emulator daemon
    - [] an introduction to the troy architecture
    - [] 
- [] digital-currency-business data structure
  - [] number of employees, are you hiring at the moment (and do you pay in Ecoin), salary range, number of available positions, business type, are remote positions available, 
- [] firebase cloud firestore
    - [] initialize the algorithms and expectation definitions
    - [] initialize the security rules for creating, updating and deleting
    - 
- [] javascript library demo
  - [] create a demo html page for creating a "pay with ecoin button"
  - [] create a demo html page for creating a "ecoin price label"



[Written @ 1:25]

### Planned Tasks to Be Completed
[copy-pasted from Day Journal Entry for October 27, 2020]


// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider


// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list


// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)


// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts


##### Live Stream Checklist
[copy-pasted from Day Journal Entry for October 27, 2020]

Greetings

* [] Greet the viewers
* [] Plan the tasks to complete

Health and Comfort

* [] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [] Sit or stand in a comfortable position
* [] Practice a breathing exercise for 5 - 15 minutes

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Blue Lotus Feet (Asherah) By Charanpal - Topic https://www.youtube.com/watch?v=p5mCSPeY6tE&list=OLAK5uy_nDVbZAbLowdfy69nsnDhGj0tgBoGTa8jg
  - Wah Yantee By Jai-Jagdeesh - Topic https://www.youtube.com/watch?v=njTJRGwaYIY&list=OLAK5uy_l5gmdP3hvHXWLnp8d0dKzvDGjfJVDUXgw&ab_channel=Jai-Jagdeesh-Topic
  - CLAUDIO - A bit of support for those still in lockdown By Rachel Claudio https://www.youtube.com/watch?v=NsdM4HBXLnQ
  - 


Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - 

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream




[Written @ 1:20]

Approximate amount of time spent on the live stream for Ecoin # 351 https://www.youtube.com/watch?v=4TzHD3oI5pc

2 hours . . 18 minutes . . 36 seconds . . 

02:18:36.811

#1
02:18:36.811
Pause


[Written @ 1:14]


Notes @ Videos To Watch Later


[1]
Neil Turok Public Lecture: The Astonishing Simplicity of Everything
By Perimeter Institute for Theoretical Physics
https://www.youtube.com/watch?v=f1x9lgX8GaE



Notes @ Videos Watching Today and Yesterday

[1]
TOWN HALL
By Unicole Unicron
https://www.youtube.com/watch?v=wPdBINn3apw


