

# Day Journal Entry - October 20, 2020

[Written @ 16:23]

Notes @ Mark-my-words Reminds

"
I love pumpkin . . I do . . I love it so much . . And these pumpkins were grown with love . . by a local farmer . . And I saved all the seeds . . And next year . . I'm gonna grow . . this pumpkin . . 

And in one year from now . . mark my words . . I will grow this pumpkin . . and I will make this soup . . It will be the best day of my life . . The Best Day of My Life . . 
"
-- Isabel Paige
[1 @ 9:49 - 10:11]


[1]
Daily Routines Of My Life - It’s Getting Cold... (Story 52)
By Isabel Paige
https://www.youtube.com/watch?v=J2w0LNywLYM


[Written @ 15:03]
[notes copy-pasted from my dream journal notebook for October 20, 2020]

Notes @ Newly Created Words

junning
[Written @ October 20, 2020 @ 14:59]
[I was going to type the word . . "rogging" . . uh . . well . . uh . . I was inspired to write "rogging" . . uh . . while thinking about the word . . "running" . . and while I was thinking about the word . . "jogging" . . well . . uh . . "rogging" . . is a possible way to combine the words . . "running" . . and . . "jogging" . . uh . . well . . uh . . it is also possible to maybe consider the word . . "junning" . . as a way uh . . to combine . . "running" . . and "jogging" . . uh . . as far as the meaning for this word . . uh . . I'm not really sure at this time . . I'm sorry . . ]


rogging
[Written @ October 20, 2020 @ approximately 14:59]
[I was going to type the word . . "Jogging" . . but . . uh . . as I prepared to type the word . . "jogging" . . I was also thinking about the counterpart word . . "running" . . and . . uh . . well . . uh . . I didn't know which word to choose for uh . . presenting the meaning of what I wanted to say . . uh . . so uh . . it felt uh . . like a natural uh . . idea . . uh . . uh . . or a natural . . concept to combine the two words in some way . . uh . . and "rogging" . . was the result . . and so . . "rogging" . . was combined uh . . by the result of . . "jogging" . . and "running" . . uh . . which are words which uh . . seemt o have a similar personality . . uh . . but maybe they are . . uh . . appearing differently uh . . to the physical eyes in terms of how they are uh . . applied in a uh . . sentence in how they appear . . uh . . well . . . uh . . now they have another counterpart word . . uh . . which also uh . . could mean something like . . junning . . uh . . haha . . junning uh . . is another word . . uh . . well . . uh . . in another way to uh . . combine . . running . . and jogging . . uh . . junning could be a possible way of combining these words . . uh . . well . . uh . . . hmm . . uh . . well . . uh . . uh . . well . . I uh . . suppose that this word . . "rogging" . . could mean something relating to . . "running" . . and . . "jogging" . . uh . . uh . . which . . uh could perhaps be a way of characterisizing the word . . to uh . . have a similar character of these words . . uh . . uh . . but . . uh . . I suppose this word . . "rogging" . . could also have another meaning . . uh . . besides . . "to run" . . or . . "to jog" . . uh . . maybe . . uh . . uh . . well uh . . I'm not really sure . . right now . . uh . . "to rob a bank" . .]


[Written @ 14:08]


[Written @ approximately 14:06]

[Copy-pasted from my dream journal . . ]
[These notes on newly created words, newly learned words and newly learned names . . were copy-pasted from my dream journal]


[Written @ approximately 14:03]
[copy-pasted from my dream journal]

Notes @ Newly Learned Names

alson
[Written @ October 20, 2020 @ approximately 14:04]
[I accidentally typed the word . . "alson" . . instead of the word . . "alongside" . . well . . uh . . this word . . "alson" . . uh . . well . . it seems like a new word to me . . at this time . . hmm . . well it seems like this word . . "Alson" . . could also be related to a name . . that is being used . . well . . I had the intuition that it would make a really cool name . . a cool name . . like . . "Elon" . . uh . . well . . "Elon" . . and . . "Alson" . . have a sort of related appearance and sound . . uh . . well . . this is quite a beautiful discovery them . . this is a newly learned name . . ]


Notes @ Newly Learned Words

Alford
Alford Plea


Notes @ Newly Created Words

alsord
[Written @ October 20, 2020 @ approximately 14:04]
[I accidentally typed the word "alsord" . . instead of . . "alson" . . uh . . well . . i was thinking about the word . . "alson" . . but . . uh . . also . . I in a split second . . or in the fraction of a second . . I had realized I made a uh . . well . . I made or I could have made a typographical error of writing the word . . "sord" . . instead of . . "word" . . uh . . well . . uh . . the word" . . uh . . "word" . . uh . . well . . I could have written "sord" . . by accident . . but I uh . . I don't think I did . . uh . . but it could have been a possibility . . uh . . well . . and then maybe . . uh . . having this idea in the back of my mind like . . "oh . . I could have made . . this typographical error of writing . . 'sord' instead of . . 'word' . ." . . well . . uh . . maybe some how that inspired by fingers to slip . . and so . . I accidentally typed the word . . "alsord" . . instead of . . "alson"]



[Written @ 6:51]



[Written @ 6:39]

Notes @ Newly Created Words

fewm
[Written @ October 20, 2020 @ 6:39]
[I accidentally typed the word "fewm" . . instead of typing the word . . "few minutes" . . or . . "few minutes ago" . . uh . . uh . . well . . word phrase . . uh . . I guess you could call this . . "few minutes ago" . . uh . . well . . or "words" . . uh . . plural . . I suppose . . uh . . -_-- . . uh . . well . . uh . . what is the meaning of this word ? . . uh . . fewm . . well . . maybe . . when you're saying the notation of something . . you can declare that the thing is a fewm . . by writing "instead of typing a fewm" . . and that thing can not need to be specified in terms of plural or any other . . uh . . specifier for a term . . uh . . such as the number uh . . of uh . . things that it relates to or something like that . . I'm not sure . . I only . . uh . . started to characterize this word like this at this time . . and yet . . uh . . it seems like a possible . . denomination . . or a posible denounciation . . of uh . . a possible . . way to create a meaning for this term . . uh . . denounciation seems to suggest a way that this term is maybe uh being given a poor definition uh . . but maybe uh . . uh . . denounciation . . denounciation is uh . . like uh . . pronounciation . . uh . . pronounciation captures the term of something that is in relation to how the topic relates to acoustic rhythm . . vibrations . . uh . . or uh . . uh . . human sounding voice acoustic rhythm pronunciation uh . . pronunciation or pronounciation uh . . vocabulary schematic diagrams or something like that . . uh . . and denounciation . . uh . . uh . . maybe supercedes in its arrangement by ordering a new way to categorize the sound relation to being a poorly made anagram of a counntry or town . . uh . . well . . uh . . in other words . . maybe . . this word . . fewm . . should mean . . "country" . . or . . "town" . . so . . uh . . well . . I think . . that's possibly uh . . less . . in the way of interrupting the possibilities of how this word can collect the sound of equestrian rhythms through the space and time categories of compassionate speaking dialect people . . or something like that . . or something like that . . ]


Notes @ Newly Created Words

rhythin
[Written @ October 20, 2020 @ approximately 6:46]
[I accidentally typed the word "rhythin" . . instead of . . "rhythm" . . I'm sorry . . I'm not sure what this word . . "rhythin" . . should mean . . at this time]

anot
[Written @ October 20, 2020 @ approximately 6:46]
[I accidentally typed the word "anot" . . instead of typing . . "any other" . . and so . . uh . . I'm not sure . . what this word . . "anot" . . means . . at this time . . uh . . well . . uh . . anot . . uh . . that could be an interesting fewm . . ]

[Written @ 0:12]



Approximate amount of time . . spent working on today's . . Ecoin Live Stream . . Ecoin #342

03:04:25.930

#1
03:04:25.930
Pause


