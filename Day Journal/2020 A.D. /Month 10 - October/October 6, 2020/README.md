

# Day Journal Entry - October 6, 2020

[Written @ 20:00]

Notes @ Newly Learned Words

epi
[I originally typed a shorthand for "episode" . . "epi" seems to have another meaning however :O . . ]

[Written @ 17:54]



[Written @ 17:48]

For the next live stream episode . . I'd like to work on the following things:

We'll work on the task list . . shown below . . and . . uh . . 

- work on the command line interface (cli) errors
- work on the errors with "apply transaction list" algorithm and expectation definitions
- work on the errors with "delete account" algorithm and expectation definitions
- work on the website errors
- work on the administrator daemon
- work on "apply universal basic income" algorithm and expectation definitions
- work on the "development environment" website continuous integration / continuous deployment process
- work on the "development environment" daemon continuous integration / continuous deployment process



[Written @ 15:34]

artooshin
[Written @ October 6, 2020 @ 15:35]
[This is a word that came to mind earlier today . . I wrote this word . . in my physical paper material notebook . . and . . now I'm writing it here . . uh . . I think . . it came to mind . . from a . . uh . . state of consciousness that is like the sleeping state but . . uh . . more . . aware . . uh . . uh . . like . . partially . . sleeping . . partially . . dreaming . . partially . . thinking . . I'm not sure . . what . . this word . . "artooshin" . . should mean at this time . . I'm sorry]


[Written @ 14:47]

I uh . . I would like to ocntinue taking a nap for a few more minutes . . and then I will use the restroom . . and then . . I'll return to the live stream . . :O

[Written @ 14:46]

Well . . I'm not really sure . . I also . . uh . . don't want to write so much sometimes . . uh . . I also enjoy writing in a physical paper notebook uh . . for the sake of enjoying the hand movements of writing with a pencil . . 

[Written @ 14:30]

While taking a nap . . I saw the words "Sneaking Journaling Suffering" 

. . the message I am thinking that this represents . . uh 

The first 2 words "Sneaking" and "Journaling" were written above the word "Suffering" 

I'm thinking that the meaning of this message is something like . . uh  well . . I call this a message because . . In the vision that I had with my eyes closed while laying on the couch . . I saw a hand . . maybe the right hand . . I would say . . it was probably the right hand or it could have been the right hand of the person [in relation to the left hand of the person . . or in proportion to the understanding of the left and right handedness of people]

. . 

I'm thinking . . that this message is maybe meaning something like . . uh . . well . . in relationship . . 

well . . uh . . for example . . for myself . . uh . . well . . sometimes I take notes on my physical material paper journal notebook and . . uh . . well . . uh . . I'm uh . . well . . it's not really all that transparent to develop the project in this way if you are not able to see what those physical material paper notebooks . . look like . . uh . . well 

Uh . . maybe uh . . I should write more in this uh . . dialog entry relational journal . . uh . . so that is more transparent for what my thoughts are or something like that . . well . . I like the idea of uh . sharing information . . uh . . 

well . . uh . . I'm not really sure . I think about a lot of things and so uh sometime I don't find it useful to share some things that I have thinking in my mind uh . . for example somtimes I draw human female breasts or human breasts in my books and those are fun and more for me to relax and not feel stressed or worried about things . . 

uh . . well . . I also . . uh . . well . . hmm . . drawing . . uh . . hmm . . uh right that wouldn't necessarily be publishable for the live stream . uh . since there are rules around this topic . 

Uh . let's see. . well . . also maybe uh . . sketches of ideas . like the ideas that uh . . are being planned for the current project . . uh . . so for example . . a lot of the items in the . . planned tasks to complete section on this page . . are . . things that I had written down . . in my physical material paper journal notebook before . . uh . . writing here in this dialog entry relational journal . . digital format of writing information . 




handenedned
[handedness typographical error]

[Written @ 14:23]

I was taking a nap . . and while laying on the couch . . I was having a vision of a thought . . uh . . a hand moved to write something . . 

"
Sneaking Journaling
Suffering
"

Those were the words that I read . . I uh  . well . . they were kind of hard uh . to read . the text wasn't so outlined and easy to read and so I had to use a sort of intuitional reading . . like . . when you . . move your head back and forth very quickly . . and try to read the words on a page . . you can see the shapes of the character . . and maybe the sequences . . uh . . but you might need to train yourself to see the words and learn how to read when you don't give yourself time to adjust to print on the page . . like . . uh . . another example . . is . . if you are on a train . . and the train is driving very quickly . . and you are reading the text on a billboard or on a sign that is outside of the uh . . train . . well . . you can train yourself to read these signs quickly and know what the message within maybe some degree of accuracy . . uh . . if uh . . maybe you only had . . 2 seconds or less time to read the message . . uh . . and uh . especially long messages . . are uh . . something you could want to read . . if uh . . well . . uh . . uh . . in any case relying on intuition is quite useful to rely on how the familiarity of the pattern is apparent or something like that . . and so then you can make uh probabilistic guesses or probabilistic determinations of what it was that was being said or something like that . . uh . . I think . . Captchas . . are maybe another example . . of what the intuitional reading is about uh .


Notes @ Newly Created Words

intuio
[Written @ October 6, 2020 @ approximately 14:34]
[I accidentally typed the word . . "intuio" . . instead of . . "intuitional" . . I'm sorry . . I'm not sure what this word . . "intuio" . . should mean at this time . . ]


initional
[Written @ October 6, 2020 @ approximately 14:35]
[I accidentally typed the word . . "initional" . . instead of the word . . "intuitional" . . I'm sorry . . I'm not sure what this word . . "initional" . . should mean at this time . . ]


expab
[Written @ October 6, 2020 @ approximately 14:36]
[I accidentally typed the word . . "expab" . . instead of the word . . "example" . . I'm sorry . . I'm not sure what this word . . "expab" . . should mean at this time . . ]

exmpla
[Written @ October 6, 2020 @ approximately 14:37]
[I accidentally typed the word "exmpla" . . instead of the word "example" . . I'm sorry . . I'm not sure what this word . . "exmpla" . . should mean at this time]


[Written @ 14:08]

I'm going to take a nap now . . Be right back . . 

[Written @ approximately 13:58]


Notes @ Newly Created Words

layour
[Written @ October 6, 2020 @ 13:57]
[I accidentally typed the word . . "layour" . . instead of . . "layout" . . I'm sorry . . I'm not sure . . what this word . . "layour" . . should mean at this time]


[Written @ 13:39]

Notes @ Newly Learned Words

Tabla
[Written @ October 6, 2020 @ 13:40]
[I saw the word "tabla" . . when reading the text "table of contents" . . and . . "tabla" . . seems like a new word to me . . it reminds me of . . tabla . . tabla . . uh . . [after doing some research] . . tabla seems to be a pair of twin hand drums]


[Written @ 12:46]

Notes @ Newly Created Poems

Poem Idea #2:

The surface of the web

Each of the pages of the world wide web

Each of the surf board pinnacle alleyways

Are humongous waves of traffic

And each surfer continues


Notes @ Newly Created Words

sufrace
[Written @ October 6, 2020 @ 12:47]
[I accidentally typed the word . . "sufrace" . . instead of . . "surface" . . I'm sorry . . I'm not sure . . what this word . . "sufrace" . . should . . mean . . at this . . time . . ]

[Written @ 12:43]

Notes @ Newly Created Poems

Poem Idea #1:

The surface of the web . . 

The part of the web that you can see . . 

And surfing the web . . 

Could be a . . 

Deeper . . 

[Written @ 12:42]

Notes @ Random Thoughts Throughout the Day

"for storing data on the surface of the web" . . 

Inspired from reading: 

Localforage (for storing data locally in the web browser)

. . while updating the project README.md file for . . https://gitlab.com/ecorp-org/ecoin/-/edit/master/README.md



[Written @ 12:21]

Notes @ Newly Created Words

tuools
[Written @ October 6, 2020 @ 12:22]
[I accidentally typed the word . . "tuools" . . instead of . . "tools" . . I'm sorry . . I don't know . . what this word . . "tuools" . . should mean . . at this time . . ]


[Written @ 12:13]

Notes @ Newly Learned Words

Reat
[I accidentally typed . . "great" . . as . . "reat" . . ]

[Written @ 10:24]

Notes @ Possible Alternative Reality Names for People

Possible Alternative Reality Names for Isabel Paige

Grace Fiddle
[this came to mind as a possible alternaive name for Isabel Paige in a reality where they are a ballet dancer . . I had this thought . . 2 or 3 months ago . . ]

Bess
[this name came to mind as a possible alternative name for Isabel today at around 10:24]

Brittaney Clarks
[this name came to mind to me yesterday . . while . . relaxing . . and watching YouTube videos . . as a possible alternative reality name for . . Isabel Paige . . ]


[Written @ 10:23]

[copy-pasted from "October 4, 2020" day journal entry]


* Things that are planned to be completed

  - fix the issues on the command line interface
  - update account
    - update firebase auth uid for security
    - update account profile picture
    - update acocunt name
    - update account username
      1. add to queue map of users requested for that username
      2. Check queue map to see if user is the first to attend the operation
      3. Delete the entry map from existing username to accountId
      4. Update the entry map for the new username to accountId
      5. Change the username of the account [Firebase validate other operations]
      6. Delete from queue map of users registered for that username
    - update account by adding blocked account
      - can no longer send currency to this account
      - can no longer receive currency from this account // ???
    - update account by removing blocked account
      - 
    - update account behavior statistics // for security, fraud detection, transaction overuse
      - admin only read and write
      - measure the frequency of creating a transaction
      - measure the transaction amount median
      - freeze an account in case of abnormal account use ??
  - get account
    - get account by qr code id
  - create account
    - create a qr code id
    - add to account-qr-code-to-account-id-map
    - add to create-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - delete account
    - add to delete-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - update transaction
    - update transaction amount
    - update transaction text memorandum
    - update transaction by adding a like
    - update transaction by removing a like
    - update transaction by adding a dislike
    - update transaction by removing a dislike
    - update transaction by adding a comment
    - update transaction by removing a comment
  - create transaction
    - include permissioned contact information
    - text memorandum
    - private text memorandum
    - 
  - apply universal basic income
    - create a transaction for all accounts to add digital currency amount
  - initialize-service-account (sign in)
    - with google
    - with email and password
  - uninitialize-service-account (sign out)
  - gitlab continuous integration / continuous deployment (ci / cd)
    - publish ecoin development website
    - publish ecoin daemon to development server
  - readme.md
    - demo usecase preview: update transaction settings
    - demo usecase preview: view transaction information
  - html component
    - pay with digital currency button works on websites
    - create an html component with an initial property object
    - return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
    - return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
  - digital currency account data structure
    - is a digital currency exchange that transacts using ecoin
    - is business account, selling products and services using ecoin
      - is providing currency exchange service
        - supported currency list
      - is providing products
        - product category list, product price ranges
      - is providing services
      - is providing other
    - is a physical location shop or store
      - restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
    - contact information variable tree (email, phone number, website url, physical address etc.)
      - permission rule on variable tree item: Public, Private, Permissioned
    - 
  - update the styles of the desktop version of the website
    - account section
      - show the account section on the left
      - desktop version width should be not full width. [follow how it's done on the settings and about page]
      - desktop header tabs (v-tabs) should be condensed and not wide screen.
    - account statistics
      - show the account statistics on the right (allow vertical scroll)
    - community participants: show a list using vuetify table
  - settings page
    - add light and dark theme setting
  - add a `qr code` button
    - [header] Your Account QR Code
      - [subheader] Account QR Code for `account name`
    - [camera icon] Capture Account QR Code
    - find a digital currency account by its qr code by taking a photograph of the account qr code
  - network status page
    - number of ecoin distributed (timeline graph)
    - number of created accounts (timeline graph)
      - number of created business accounts (timeline graph)
    - number of created transactions (timeline graph)
    - newly distributed ecoin (live updating)
    - newly created accounts list (live updating)
      - number of created business accounts (timeline graph)
    - newly created transactions list (live updating)
    - total number of ecoin created (running sum of overall currency amount)
    - total number of accounts created (running sum of overall accounts amount)
      - total number of business accounts created (running sum of overall community participants)
    - total number of transactions created (running sum of overall transactions amount)
    - flights gl map to show transactions in realtime
  - account page
    - navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
    - add settings icon button
    - transaction list should show "view transaction button"
      - open transaction dialog
    - transaction list should show "repeat transaction button"
    - like to dislike ratio timeline graph
    - number of accounts transacted with timeline graph
    - currency amount transacted timeline graph
    - number of transactions timeline graph
      - ratio of likes from recipient, sender
      - ratio of dislikes from recipient, sender
    - calendar heatmap of transactions created
  - about page
    - update the "Why does Ecoin exist?" answer 
      - "[A] A short answer like this is meant to encure your curiosity and sponsor your research project to learn more about these wounderous individuals. ;)"
      - enumerate the long answer clause
  - apply transaction list
    - Recurring Transactions as Processed set to false
      - dateTobeProcessed = d
      - numberOfTimesProcessed = n
      - millisecondDelayBetweenProcessing = m
      - currenctDateForTransaction = c
      - const isAlreadyProcessedTimePeriod = d + n * m > c
  - website style / theme
    - research how to improve account page style for desktop
    - research how to improve settings page style for desktop
    - research how to style the network status page
    - 
  - readme.md
    - add the names, and website urls of the software dependencies items (use the same layout as the related work section)
    - remove list index from the software dependencies table
    - add appended list to each list item in the table of contents
    - add "completed" text to the features description text in the readme.md file
    - fix typo to be "for storing data on remote computers", in software dependencies / database provider / firebase realtime database section
    - add the domain name registration service used for https://ecoin369.com . . GoDaddy
    - add website library "echarts" (for logistics measurement display)
    - add expectation definitions measurement tools: mocha, chai
    - add project compilation: node.js, npm
    - add prepaid credit card service (with anonymous support): Bluebird, American Express
    - 


[Written @ approximately 10:23]

[Copy-pasted from "October 4, 2020" day journal entry]

Live Stream Checklist

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Days Of My Life...Back To The City...(Story 50) By Isabel Paige https://www.youtube.com/watch?v=bKQ_VMQPb2M
  - Ardas Bhaee ⋄ Mirabai Ceiba ⋄ Snatam Kaur ⋄ Jai-Jagdeesh ⋄ Simrit Kaur ⋄ Sirgun Kaur ⋄ Singh Kaur By M U S I Q A A https://www.youtube.com/watch?v=es_gSCmswBg
  - Ru's Piano | ACG Music 儒儒的宅鋼琴時間 ピアノ By Ru's Piano Ru味春捲
 https://www.youtube.com/playlist?list=PLUnBm8KiAXgUfJCNbFMgZccbdkwkxMyuL
  - Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4
  - Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - Snatam Kaur and Ajeet Kaur ⋄ Sacred Chants By M U S I Q A A https://www.youtube.com/watch?v=xoBpkz_yay8&pbjreload=101
  - Touhou Project「Lost Word Chronicle」Ru's Piano x @Kathie Violin 黃品舒 By Ru's Piano Ru味春捲
 https://www.youtube.com/watch?v=gvBYTwmoaH4&list=PLUnBm8KiAXgXoVTwJScG5o3BEgl2VVNyz


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
  - (1) YouTube LiveStream Chat
  - (2) YouTube + Firefox Web Browser (Picture-in-Picture Mode)
  - (3) iTerm
  - (4) Firefox Web Browser + https://www.timeanddate.com/
  - (5) Brave, Firefox, Tor Browser
  - (6) Visual Studio Code
  - (7) Express VPN
  - (8) Visual Studio Code . Previously: "Notes" on my macbook

Periodic Tasks

* [x] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - [Written @ approximately 11:51]: I checked the live stream at approximately 01:05:02. The live stream video seems to be continuing.
  - [Written @ 13:05]: I achecked the live stream at approximately 02:20:29. The live stream video seems to be continuing as expected.
  - [Written @ 13:44]: I checked the live stream at approximately 02:59:25. The live stream video seems to be continuing . . as expected . .
  - [Written @ 15:38]: I checked the live stream at approximately 04:52:44. The live stream video seems to be continuing as expected . . 

Salutations

* [x] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [x] Talk about the possibilities for the next live stream



[Written @ 0:17]


Notes @ Newly Created Words

expis
[Written @ October 6, 2020 @ 0:17]
[I accidentally typed the word . . "expis" . . instead of the word . . "episode" . . I'm not sure what this word . . "expis" . . should mean . . at this time . . I'm sorry . . ]



