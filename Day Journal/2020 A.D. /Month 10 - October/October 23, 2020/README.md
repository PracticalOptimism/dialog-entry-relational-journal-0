
# Day Journal Entry - October 23, 2020

[Written @ 23:59]

Notes @ Newly Learned Words

Octaves of Light
[1 @ 11:44]


[Written @ 23:43]

Notes @ Newly Created Words
[copy-pasted from the time period labelled as "Written @ 22:47" in this dialog entry page . . for Day Journal Entry - October 23, 2020]


extrebely
[Written @ October 23, 2020 @ 23:39]
[On a highway in another universe . . in another reality . . there was the text that read "extrebely" . . In typing the word . . "extremely" . . my gut intuition told me to come to the thought that "maybe your fingers could type "b" in the word "extremely" . . and what would that look like?" . . in my particular interpretation at the type of this writing . . "extrebely" . . seems like a possible way for typing this word of what the word "extremely" could look like with a "b" . . and now we're involved . . in a universe that is "extrebely" involved . . how did that come to be ? . . I'm not exactly sure if I should ignore words like this . . it could mean life and death in the mind of the word's intuition and the words that step into our reality as a consequence of coming to understanding what the meaning of "extrebely" could be . . somethings . . I'm sorry . . which words should be ignored ? . . is this a word that is possible to bring uninvited guests to the universe where we extend our trust and love towards one another ? . . is that a difficult gesture to make to ignore such a scheme of characters ? . . ]


[Written @ 23:31]

Notes @ Newly Created Words
[copy-pasted from the time period labelled as "Written @ 22:47" in this dialog entry page . . for Day Journal Entry - October 23, 2020]

sel
[Written @ October 23, 2020 @ approximately 23:31]
[I was preparing to type the word . . "sense of self" . . and the word . . "sel" . . emerged from my characteristic typing language which is quite new to me to interact with in the way it prepares to type languages to the keyboard in the anticipatory rate that I'm not always assured of the final meaning or result of the text that is written until I've read the text to ensure the words are what I had also intended to type . . ]
[I wanse]

wanse
[Written @ October 23, 2020 @ approximately 23:31]
[I was preparing to type the word . . "was" . . and uh . . well . . the word . . "wanse" . . was typed . . I'm also not sure if the word . . "want" was mixed in my preparatory material of things that I was thinking about when thinking about the word phrase that I ended up typing later which is . . "I was preparing to type the word . . " . . which is maybe also related to the hypothetical phrasing that I could have originally typed . . "I was wanting to write the word . . " . . and so . . if maybe . . "was" . . and "wanting" . . combined in some way then perhaps the text word character sequence pattern could have been the brand new model toy car train station rhythm pattern principled array matrix diagram which spells the text "wanse" . . ]


[Written @ 23:17]

Notes @ Newly Created Words
[copy-pasted from the time period labelled as "Written @ 22:47" in this dialog entry page . . for Day Journal Entry - October 23, 2020]

afterline
[Written @ October 23, 2020 @ 23:17]
[I accidentally typed the word . . "afterline" . . instead of typing the word . . "afterlife" . . I'm sorry . . I'm not sure . . what this word . . "afterline" . . should mean . . at this time . . ]



empathei
[Written @ October 23, 2020 @ 23:23]
[I accidentally typed the word . . "empathei" . . instead of typing the originally intended word . . to be typed . . which was . . "empathize" . . I accidentally typed the word . . "empathei" . . instead of typing the word . . "empathize" . . I'm sorry . . I'm not sure . . what this word . . "empathei" . . should mean at this time . . ]


[Written @ 22:51]

Notes @ Newly Created Words
[copy-pasted from the time period labelled as "Written @ 22:47" in this dialog entry page . . for Day Journal Entry - October 23, 2020]

Uninve
[Written @ October 23, 2020 @ 22:51]
[I accidentally typed the word . . "Uninve" . . instead of . . "Universe" . . I'm sorry . . I'm not sure what this word should . . mean at this time . . ]

shoould
[Written @ October 23, 2020 @ 22:52]
[I accidentally typed the word . . "shoould" . . instead of . . typing the word . . "should" . . I'm sorry . . I'm not sure . . what this word . . "shoould" . . should . . mean . . at this time . . ]

thingle
[Written @ October 23, 2020 @ approximately 23:05]
[I was going to type the word . . "th" . . while trying to type the word . . "single" . . well . . I avoided that typographical error . . but . . it was still a possible typographical error that I could have made . . and a possible continuation of that typographical error . . could have been . . "thingle" . . uh . . well . . I'm not really sure what this word . . "thingle" . . should mean at this time . . but at least it's here now in the journal entry to be remembered for another time if it needs to be considered for a meaning of some intended purpose or something like that . . . . huh . . uh . . I think I made a mistake while writing this message . . ]
[Well . . I think the origin story that I've written above is incorrect . . I had just typed the word . . "single" . . and was preparing to type the word . . "thing" . . and . . uh . . I think uh . . maybe I combined the word . . uh . . in my imagination . . hmm . . I'm kind of lost on how things happened . . I have to rewind . . one moment . . hmm . . uh . . I'll consider doing that later . . Ecoin #346 . . at approximately 1:05:34 and before in the last . . 20 minutes . . .https://www.youtube.com/watch?v=xVyDJ-t0nk8]



[Written @ 22:47]

Notes @ Quotes

"
You have an individual perspective . . And that individual perspective . . across time . . has its own experience . . its own unique experience . . Every single perspective . . in the Universe . . has a unique eh-experience . . across time . . 

So . . I like to think of it like . . I have a perspective . . every atom in my body . . has a perspective . . Every planet . . has a perspective . . 

So you can think of . . Planet Earth as having the perspective . . as it goes around the sun . . And you can think of the sun . . having the perspective . . as all the planets go around . . over time . . 

Ah-eh . . you can . . you can . . take a slice of information . . You know . . you can . . you can . . pause . . if you could just . . if you could . . view the universe on a DVR . . and you could pause it . . and rewind it . . and look . . from each . . perspective . . 

So you jump into the sun . . today right now . . and you see . . where all the planets are . . and you can see how you feel . . You jump into my body . . You see how I feel . . You see . . how everything is going . . 

You jump into . . you know . . the . . the wall . . over there . . and you can see . . it's all a different perspective . 

So . . every single . . thing . . that exists . . has a different perspective across time . . That is valuable . . That is beautiful .

As each of these perspectives . . go along . . the idea . . of Ultimate Reality . . is the possibility . . that every single perspective . . can be experienced . . across all of time . . at the same time . 

So . . it's . . understanding . . every . . it's just . . pure understanding . . it's every bit of information . . collected . . into . . one reality . . 

Ultimate Reality . . I used to think . . would be . . my ideal . . afterlife . . But after . . I experienced . . some really intense . . informational downloads . . And you have to humble yourself . . And you . . You sometimes . . lose yourself . . You lose your individuality . . In your . . acceptance . . of other realities . . And in your empathy with other realities . . 

Because . . and sometimes . . we . . we . . egotistically . . want to hold on to . . our individual sense . . of reality . . and we don't wanna empathize . . with other people . . And that's because . . it's very painful sometimes . . and it's kind of . . scary . . to feel like . . you're losing . . your sense of self . . Ultimately . . that's not how it works . . When you . . Actually . . when you learn more about other people . . you actually become . . more of yourself . . [*1][Technically] . . you become . . more . . of . . your . . true . . self . . 

So . . Ultimate Reality . . Would . . actually be . . extremely . . painful . . And . . it would actually be . . very humbling . . It would . . probably . . obliterate the self . . The sense of self that you have . . at least . . for a time . . And . . uhm . . I think it would be quite difficult . . 
"
-- Unicole Unicron
[1 @ 4:26 - 7:17]


[*1] This word sounds like "technically" perhaps . . uh . . there could be something else that was said here . . 


[Written @ 22:34]

Notes @ Newly Learned Words

Individual Sensors
[1 @ 4:23]


Notes @ Sound Clips that Would Be Cool In a Music Video

"
Quakers say . . pray while feet are moving . . Dalai Lama says . . pray whenever moving . . I like praying when washing the dishes . . 

That's good . . I like to pray in bed . . So . . I . . I usually am not moving . . but . . whatever you choose .
"
-- Unicole Unicron
[1 @ 1:31 - 1:42]


Notes @ Newly Learned Words

Oat Milk
[1 @ 1:06]

Aerator
[1 @ 1:09]


[1]
Cam Church - Ultimate Reality and Personal Reality
By UNICULT
https://www.youtube.com/watch?v=bOyCwQqZWeY

[Written @ 21:37]

Notes @ Newly Created Words

Biokinesthetically
[Written @ october 23, 2020 @ 21:38]
[I'm reading an article on the P. diabolicus' exoskeleton and it seems interesting that the shell or exoskeleton of this beetle is able to mold and shape itself just enough that even if it's being compressed . . the shell won't squish or squeeze or ruin the jelly-like insides of the beetle . . the organs and the organic material of the beetle's insides . . well . . for at least some amount of pressure . . I started to ask myself the question of how the beetle could have a more durable shell than what it already has . . "why does the shell have to be malleable?" . . "what if the shell were a diamond crystal?" . . "does biokinesthetics support the development of a diamond crystal structure?" . . biokinesthetics is meant to mean . . the ability of biological organic processes to move in creating the biological organism in the way that genetics and biological processes work . . "how come the biological tissue couldn't be as crystalline and dense as a diamond crystal?" . . "does biological material always have a density rhythmic limit according to the capabilities of the genetics?" . . "what does the genetic code look like that supports more higher density compounds to be formed?" . . "is that possible to have a higher density of material production?" . . well . . these are a few thoughts that come to mind when I think about this topic . . "Meet the Diabolical Ironclad Beetle. It's Almost Uncrushable." . . An article by . . "Katherine J. Wu" . . on the New York Times Website . . https://www.nytimes.com/2020/10/21/science/beetle-uncrushable-ironclad.html . . published on . . October 21, 2020]


Notes @ Newly Discovered Words

styr
[Written @ October 23, 2020 @ 21:46]
[I accidentally typed the word . . "styr" . . instead of . . "structure" . . This is the name of a river in the UK . . well . . uh . . I suppose it could also be a word with a meaning . . uh . . I'm not sure . . well . . uh . . I suppose that's a possiblity . . ]


Notes @ Newly Created Words

strcture
[Written @ October 23, 2020 @ 21:48]
[I accidentally typed the word . . "strcture" . . instead of typing the word . . "structure" . . I'm sorry . . I'm not sure . . what this word . . "strcture" . . should mean . . at this time . . I'm sorry . . ]


[Written @ 21:04]


Notes @ Newly Learned Words

Brain Dump
[1 @ 17:20]

Locus of Control
[1 @ 17:22]


[1]
Daily Habits to Reduce Stress and Anxiety: Find a Way #WithMe
By Therapy in a Nutshell
https://www.youtube.com/watch?v=7EX1Xnvvk5c


[Written @ 20:36 - 20:47]

Uhm . . for some reason . . uh . . well . . the idea of collecting things . . like . . collecting coins . . feels like an attractive idea . . uh . . for example . . in the dream . . that . . I had . . relating to the image . . of coins . . 

I was excited to collect the coins . . and for example . . in the dream that I had . . I got the feeling that . . uh . . it was an adventure . . uh . . for example . . if I turned the corner . . near a bush . . on the side of the road . . I would . . be steered into a new direction where more new coins could be found . . coins that I had never seen before . . uh . . or coins that were new and different from the ones I had previously found . . uh . . well . . uh . . that's such a cool thought to be be able to find new coins . . uh . . and so . . uh . for example . . with the coins in my waking life . . with uh . . the united states quarters or the united states dime or nickel or penny . . uh . . those are the coins that I'm most familiar with . . even though there are other coins in the world such as the European Euro . . or uh . . the Russian or Chinese coins . . well . . uh . . it seems like uh . . well if you imagine a new coin . . for each uh . . different plant of civilization of the world . . uh . . to find a new coin around the corner is like finding a new civilization uh . . that you hadn't seen before and maybe you can relate the civilizations . . in terms of what they value . . by looking at the things they place on the coin . . and also maybe uh . . the shape of the coin and other physical properties of the coin . . such as the material . . or the uh . . people or plants or animals or shapes or mathematics or languages or symbols uh and so many different types of things placed on the coin face . . 

And so you could learn a new language of mathematics by observing a new coin that a civilization had produced . . and you could learn a new social system by observing the coin from another civilization . . and so many coins . . and so many civilizations . . and so many people who would use those coins and have influential thoughts . . did those people support that coin ? . . how did that coin change to support the values of those people? . . did the shape change ? . . did the precision machinery change in terms of how the coin was produced ? . . how the coin was manufactured ? . . are the edges jagged . . or is the coin completely smooth across the surface ? . . what shapes are imprinted ? . . Does the gradient of materials remain constant or does it vary from one place to the other . . along the volume of the coin body area . . or something like that . . or something like that

[Written @ 20:05]

Notes @ Remembered Dreams From Being Inspired By Looking At Something

I just remembered a dream that I had . . uh . . I was inspired to remember . . this dream . . because of the image here . . 

Photo From Video [1] that shows coins in the thumbnail of a video that reminds me of a dream that I had 2 or more years ago
![mudlarking](./Photographs/nicola-white-mudlark.png)

Which is from [1 @ 14:40] . . in the video . . [1] . . 

. . . 

Uh . . well . . the dream I had . . was maybe . . uh . . one or two or three . . uh . . I think . . uh . . it was more thatn 2 or 3 years ago . . Uh . . it was quite some time ago . . uh . . and seems like it was uh . . it wasn't a dream that happened in 2020 or 2019 . . uh . . well . . that's the timeline . . in my perspective opinion at this time . . 

Uh . . well . . the dream was where I was . . a person . . and I was . . uh . . somewhere . . I'm not exactly sure where . . but I remember there were a whole lot of coins . . of all sorts of sizes and shapes . . these coins . . looked . . like . . quarters . . and dimes . . and nickels . . and . . pennies . . but there were even way more than that . . for example . . there were coins that were . . like . . really larger than these uh . . coins . . or uh . . uh . . metal . . or uh . . symbolic currency units or something like that . . it was really such a cool dream . . uh . . I felt like there was something cool about this dream because . . well . . it was like . . there were all these coins . . and they looked so different . . and unique . . and some were larger than . . uh . . uh . . well . . they were like coins . . but the size of a tennis ball . . or the diameter of a tennis ball . . uh . . and maybe . . some were even larger than that . . and the colors were silver . . and bronze . . and gold . . and the decorations were of people's faces . . uh . . maybe even uh . . other things . . and so . . uh . . not just people's faces . . but also . . art styles . . uh . . like maybe . . animals . . or plants . . uh . . but I don't really remember seeing . . animals or plants . . those are hypothesized . . uh . . so . . uh . . those are only possibilities uh . . as I don't really remember seeing animals or plants on the coins . . or on the faces of the coins . . in my dream . . but those could have been there . . 

Uh . . well . . uh . . I think I was impressed by the dream . . but now in my physical waking life today . . as I think about the dream again . . uh . . or as . . I'm reminded by the dream . . by looking at the mudlarking video thumbnail . . inspired by [1 @ 14:40] . . uh . . well . . now . . uh . . I'm having uh . . more inspired thoughts . . 

For example . . when I think about the coins . . maybe it's like . . a symbol of value . . uh . . for example . . all of those coins are so . . uh . . like . uh . . well . . each of those coins could have come from a unique civilization . . and so in those civilizations . . those coins . . each one of the coins . . for whichever civilization of people . . uh . . people for example . . uh . . and so those people could have used those coins . . in their societies . . and there could have been a lot of value placed on those coins . . 

That's uh . . well . . uh . . imagine . . thousands and hundreds of thousands of coins . . which maybe could mean those are hundreds of thousands of different civilizations . . uh . . well . . those civilizations could have been in the same time period . . for example . . in the period of 100 years or 1000 years or something like that . . or any arbitrary number of years . . or those civilizations could have been from completely different planets . . and could have . . uh . . well . . each had their own thoughts on what to place on their monetary value system coin . . and so . . those are coming with their own unique art styles . . and the things that the society values such as . . people . . and uh . . architecture . . and animals . . and plants . . those could have all been represented on the money supply unit . . which is really quite a beautiful thought . . and so . . all of these uh . . societies . . uh . . using money . . uh . . or using a money supply system like . . dimes or quarters . . uh . . or uh . . well . . uh . . in my reality I remember the idea of a half-dollar coin . . uh . . I've never actually used one in my waking life . . uh . . a coin that is the shape of a circle . . uh . . and maybe looks like a quarter . . uh . . and yet is worth . . 50 cents . . uh . . or a half dollar . . and so . . 0.50 dollars . . uh . . well . . that's really quite incredible . . uh . . because it shows that there are possible systems of organization that we could have applied uh . . and those could have been the natural state of our world . . uh . . just like we could have had . . the faces uh . . of new presidents on each of the new coins that are printed . . and so the election of a new president comes alongside with . . the uh . . coins being changed . . so each president comes with their own unique money supply unit that looks different from the ones that came before . . uh . . well . . that's really interesting . . there are so many . . uh . . coins . . uh . . so many . . uh . . ways of exchanging value . . 

Uh . . the idea of value is such an interesting idea . . uh . . to place the idea of value in a . . uh . . uh . . uh . . well in a coin . . or uh . . to associate value in the unique coins uh -_- . . well . . I'm really getting off track . . in my dream I was uh . . really uh . . awe-struck . . uh . . I really uh . . thought it was a cool idea to be a person . . and explore to look at all the different types of coins . . uh . . it was so cool . . there was like a trail of coins . . and you could play and look at each one . . and now that I'm awake . . I'm imagining to myself . . wow . . the people that could have used these coins . . could have taken them very seriously . . and these could have been the difference between life and death in a society for any particular individual . . and so if you didn't have any of these coins in your pocket . . you could be homeless . . uh . . and so . . uh . . well . . it's quite funny . . there are uh . . there uh . . there uh . . well . . when I'm standing outside of any particular society . . and just standing in a pile of thousands of coins . . uh . . well . . uh . . either thousands . . uh . . uh . . well . . there were quite a lot . . I'm tempted to say . . thousands . . but uh . . i didn't count . . uh . . uh . . well . . there could have been hundreds . . uh . . and so . . uh . . well . . the idea uh . . or the impresseion that I got is that there were quite a lot of coins . . uh . . certainly more than . . uh . . 10 or something like that . . uh . . well something like that . . 

Uh . . well . . uh . . the coins uh . . varied . . uh .  .in size . . uh . . which makes me think of birds . . uh . . how there can be birds like . . young hummingbirds . . or uh . . elderly falcons . . or uh . . large pelicans . . uh . . or uh . . even dodo birds uh . . that are so called extinct or something like that . . imagine . . that there is such a beautiful diversity of . . coins like there is a beautiful diversity of birds . . well . . it's really quite a beautiful thought since . . birds are so unique uh . . in their uh . . activities . . uh . . and their shape . . and their size . . uh . . mostly in the dream . . I remember . . uh . . circular coins . . uh . . and some even had . . circle uh . . circular cut-out centers where the center of the coin was empty . . and so uh . . you could uh . . place your finger through the center of the coin or something like that . . uh . . well . . uh . . or maybe place a threaded needle through the center of the coin . . and wear the coin as a necklace uh . . or a pendulum uh . . around your neck or uh . . around your torso or something like that . . uh . . well . . uh . . hmm . . right . . the image . . uh . . 

right . . coins . . uh . . values . . uh . . diversity . . uh multidimensionality . . 

multidimensionality is the topic relating to the dimensions of thought . . when you look at thousands of coins or millions of coins . . it's like looking at millions of civilizations from all sorts of different planets uh . . from various star systems where all these different coins are being used as a money supply system . . it's really quite unique to see all those civilizations all the great variety of those civilizations all in one place where you can see them all at once in your dream . . 

uh . . well . . they are only coins but still uh the symbol is there that the coins are valuable to the various people that would have valued them or the coins are valuable to the various people that would have used them in their consciousness homeland planet where they are familiar with applying the coin to support their life in some way in those planetary habitat mentalities . . 


. . . 



Remembeder
[Written @ October 23, 2020 @ 20:06]
[I accidentally typed the word . . "Remembeder" . . instead of . . typing the word . . "Remembered" . . I'm sorry . . I'm not sure . . what this word . . "Remembeder" . . should mean . . at this time]


[Written @ 19:44]

Notes @ Newly Learned Words

Mudlarking
[1 @ 14:37]

[Written @ 19:31]

Notes @ Newly Discovered YouTube Channels

The Fitness Marshall 
https://www.youtube.com/user/TheFitnessMarshall/videos
[Discovered by: [1 @ 10:07]]

Therapy in a Nutshell
https://www.youtube.com/c/TherapyinaNutshell/videos
[Discovered by: [1]]

nicola white mudlark - Tideline Art
https://www.youtube.com/c/nicolawhitemudlarkTidelineArt/videos
[Discovered by: [1 @ 14:40]]



[1]
Daily Habits to Prevent Depression During Stressful Times- Coronavirus Covid-19 Depression #WithMe
By Therapy in a Nutshell
https://www.youtube.com/watch?v=YsQ02zc3mQ0



[Written @ 16:27]


### Planned Tasks For Today
[copy-pasted from Day Journal Entry for October 17, 2020]


// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider


// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list


// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)


// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts

[Written @ 16:02]

##### Live Stream Checklist

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes
  - Gratitude Meditation ❤️️ 21 Day Transformation ❤️️ 432 HZ [By Live The Life You Love] https://www.youtube.com/watch?v=xfD4HaBBc0I
  - Gratitude Meditation [By relax for a while] https://www.youtube.com/watch?v=nCq5MkNem6k

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - hunter x hunter 2011 OST best songs [By Saitama sama] https://www.youtube.com/watch?v=en5dTk8OQnE&ab_channel=Saitamasama


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - 

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream






