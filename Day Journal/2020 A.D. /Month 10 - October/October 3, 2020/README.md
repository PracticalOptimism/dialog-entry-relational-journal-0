

# Day Journal Entry - October 3, 2020

[Written @ 23:57]

Thanks for watching today's live stream for Ecoin #323 . . 


[Written @ 23:29]

uh . . rhyolight_ . . uh . . I think . . was the . . username . . that Matt Taylor . . worked with on the internet . . for example . . on Twitch . . Uh . . 

[Written @ 23:11]

A few YouTube Channels that I watch to keep up on the latest thoughts . . and happenings . . on . . planet Earth . . 

Russell Brand . . Eugenia Cooney . . Isabel Paige . . Roxanne Meadows . . Charles Hoskinson . . Leemon Baird . . Siraj Raval . . Joe Rogan . . Claudia Walsh . . Devan Christine . . Paget Kagy . . IPFS . . Hedera Hashgraph . . Krista Raisa . . Unicole Unicron . . Paul Frazee . . Wolfram . . Numenta . . 

Asmongold . . Esfand . . Stay Safe . . Tips Out . . 



. . I'm very sorry to see that . . Matt Taylor . . has . . left . . the planet . . Well . . uh . . that's . . uh . . well . . I'm sorry to see . . that . . uh . . 

. . . 

Rest In Peace Matt Taylor . . 

. . . 

Well . . I was planning on taking a shower . . uh . . so . . I'm gonna go ahead and do that . . uh . . I'll . . come back . . to list . . uh . . more YouTube . . uh . . YouTube Channels . . that I keep up with . . in case . . anyone is interested in that information . . uh . . there are a lot of interesting people . . working on all sorts of things . . 

the spiritual side of youtube is awesome . . the technical computer science side of youtube is awesome . . the art . . and painting . . side of YouTube . . is awesome . . the . . uh . . hmm . . well . . the beauty . . and fashion . . side of YouTube is really great . . There's also some information channels . . and news . . podcasts . . and . . updates . . on social . . relations . . uh . . well . . those . . are really awesome too . . I used to watch a lot of gaming content on YouTube . . uh . . World of Warcraft mostly . . uh . . well . . uh . . World of Warcraft content on YouTube is really cool . . uh . . uh . . some of the names aren't coming to me right now . . but there are a lot of people that I keep up with here and there . . uh . . meme channels as well . . uh . . commentary . . games community commentary . . uh . . uh . . it's fun to see what people are up to . . so many thoughts . . uh . but that's a personal opinion . . well . . I'm sorry . . 

Alright . . well . . uh . . I'm sorry . . 

Notes @ Newly Created Words


peoplare
[Written @ October 3, 2020 @ 23:23]
[I accidentally typed the word . . "peoplare" . . instead of . . "people" . . I'm not sure what this word . . "peoplare" . . should mean at this time . . I'm sorry . . ]


poer
[Written @ October 3, 2020 @ 23:25]
[I accidentally typed the word . . "poer" . . insted of . . "personal opinion" . . I'm not sure . . what this word . . "poer" . . should mean right now . . I'm sorry . . ]


[Written @ 18:30]

* [x] Plan the tasks to complete

  - fix the issues on the command line interface
  - update account
    - update firebase auth uid for security
    - update account profile picture
    - update acocunt name
    - update account username
      1. add to queue map of users requested for that username
      2. Check queue map to see if user is the first to attend the operation
      3. Delete the entry map from existing username to accountId
      4. Update the entry map for the new username to accountId
      5. Change the username of the account [Firebase validate other operations]
      6. Delete from queue map of users registered for that username
    - update account by adding blocked account
      - can no longer send currency to this account
      - can no longer receive currency from this account // ???
    - update account by removing blocked account
      - 
    - update account behavior statistics // for security, fraud detection, transaction overuse
      - admin only read and write
      - measure the frequency of creating a transaction
      - measure the transaction amount median
      - freeze an account in case of abnormal account use ??
  - get account
    - get account by qr code id
  - create account
    - create a qr code id
    - add to account-qr-code-to-account-id-map
    - add to create-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - delete account
    - add to delete-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - update transaction
    - update transaction amount
    - update transaction text memorandum
    - update transaction by adding a like
    - update transaction by removing a like
    - update transaction by adding a dislike
    - update transaction by removing a dislike
    - update transaction by adding a comment
    - update transaction by removing a comment
  - create transaction
    - include permissioned contact information
    - text memorandum
    - private text memorandum
    - 
  - apply universal basic income
    - create a transaction for all accounts to add digital currency amount
  - initialize-service-account (sign in)
    - with google
    - with email and password
  - uninitialize-service-account (sign out)
  - gitlab continuous integration / continuous deployment (ci / cd)
    - publish ecoin development website
    - publish ecoin daemon to development server
  - readme.md
    - demo usecase preview: update transaction settings
    - demo usecase preview: view transaction information
  - html component
    - pay with digital currency button works on websites
    - create an html component with an initial property object
    - return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
    - return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
  - digital currency account data structure
    - is a digital currency exchange that transacts using ecoin
    - is business account, selling products and services using ecoin
      - is providing currency exchange service
        - supported currency list
      - is providing products
        - product category list, product price ranges
      - is providing services
      - is providing other
    - is a physical location shop or store
      - restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
    - contact information variable tree (email, phone number, website url, physical address etc.)
      - permission rule on variable tree item: Public, Private, Permissioned
    - 
  - update the styles of the desktop version of the website
    - account section
      - show the account section on the left
      - desktop version width should be not full width. [follow how it's done on the settings and about page]
      - desktop header tabs (v-tabs) should be condensed and not wide screen.
    - account statistics
      - show the account statistics on the right (allow vertical scroll)
    - community participants: show a list using vuetify table
  - settings page
    - add light and dark theme setting
  - add a `qr code` button
    - [header] Your Account QR Code
      - [subheader] Account QR Code for `account name`
    - [camera icon] Capture Account QR Code
    - find a digital currency account by its qr code by taking a photograph of the account qr code
  - network status page
    - number of ecoin distributed (timeline graph)
    - number of created accounts (timeline graph)
      - number of created business accounts (timeline graph)
    - number of created transactions (timeline graph)
    - newly distributed ecoin (live updating)
    - newly created accounts list (live updating)
      - number of created business accounts (timeline graph)
    - newly created transactions list (live updating)
    - total number of ecoin created (running sum of overall currency amount)
    - total number of accounts created (running sum of overall accounts amount)
      - total number of business accounts created (running sum of overall community participants)
    - total number of transactions created (running sum of overall transactions amount)
    - flights gl map to show transactions in realtime
  - account page
    - navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
    - add settings icon button
    - transaction list should show "view transaction button"
      - open transaction dialog
    - transaction list should show "repeat transaction button"
    - like to dislike ratio timeline graph
    - number of accounts transacted with timeline graph
    - currency amount transacted timeline graph
    - number of transactions timeline graph
      - ratio of likes from recipient, sender
      - ratio of dislikes from recipient, sender
    - calendar heatmap of transactions created
  - about page
    - update the "Why does Ecoin exist?" answer 
      - "[A] A short answer like this is meant to encure your curiosity and sponsor your research project to learn more about these wounderous individuals. ;)"
      - enumerate the long answer clause
  - apply transaction list
    - Recurring Transactions as Processed set to false
      - dateTobeProcessed = d
      - numberOfTimesProcessed = n
      - millisecondDelayBetweenProcessing = m
      - currenctDateForTransaction = c
      - const isAlreadyProcessedTimePeriod = d + n * m > c
  - website style / theme
    - research how to improve account page style for desktop
    - research how to improve settings page style for desktop
    - research how to style the network status page
    - 



Notes @ Newly Created Words

Taget
[Written @ October 3, 2020 @ 19:11]
[I noticed I typed . . "Account Taget Variable Tree" . . I think . . I could have meant to type . . "Account Contact Variable Tree" . . I'm not sure . . what . . "Taget" . . should mean . . I think . . maybe . . I uh . . could have meant to type . . "Account Target Variable Tree" . . uh . . but . . I'm not sure . . why . . I would type "Target" . . uh . . well . . "Target" . . uh . . suggests . . that . . uh . . maybe . . the recipient . . of . . an action . . uh . . or the target . . for a transaction . . or something . . like that . . but . . uh . . well . . I'm not sure . . why . . I wrote that . . I'm not . . uh . . able . . to uh . . or . . I haven't . . previously . . been . . thinking . . about . . or . . have . . thought about a usecase . . for this word . . "Target" . . so . . I'm actually . . kind of surprised . . how . . "Target" . . uh . . if that was the originally intended word . . uh . . it seems like a closer . . uh . . meant . . word . . than . . "Contact" . . uh . . so . . "Contact" . . uh . . well . . if . . "Contact" . . was the originally intended word . . uh . . "Taget" . . is quite a . . uh . . distance . . uh . . from . . the word . . uh . . "Contact" . . well . . "Account Contact Variable Tree" . . uh . . makes . . uh . . some sense to me . . since . . I've been thinking about the usecase . . or . . use case . . uh . . uh . . use case . . or usecase . . uh . . [I'm not really sure which way to spell this word . . with . . 1 word . . or 2 words . . separated by . . a space character . . ] . . well . . in any case . . uh . . "Contact Variable Tree" . . means something like . . a list . . of . . uh . . contact information . . in the . . format . . of a . . variable . . tree . . data structure . . or uh . . a hash table . . uh . . well . "Taget" . . is really quite an interesting word . . I'm not sure what this word . . should mean at this time . . I'm sorry . . ]

restauranges
[Written @ October 3, 2020 @ 18:36]
[A typographical error of . . "restaurants" . . I'm not sure what this word . . "restauranges" should mean at this . . I'm sorry . . ]

resua
[Written @ October 3, 2020 @ 18:36]
[A typographical error of . . "restaurants" . . I'm sorry . . I'm not sure what this word . . "resua" . . should mean at this time . . ]

reaurants
[Written @ October 3, 2020 @ 18:36]
[A typographical error of . . "restaurants" . . I'm sorry . . I'm not sure what this word . . "reaurants" . . should mean at this time . . ]

[Written @ 18:27]

Live Stream Checklist

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Ardas Bhaee ⋄ Mirabai Ceiba ⋄ Snatam Kaur ⋄ Jai-Jagdeesh ⋄ Simrit Kaur ⋄ Sirgun Kaur ⋄ Singh Kaur By M U S I Q A A https://www.youtube.com/watch?v=es_gSCmswBg
  - Ru's Piano | ACG Music 儒儒的宅鋼琴時間 ピアノ By Ru's Piano Ru味春捲
 https://www.youtube.com/playlist?list=PLUnBm8KiAXgUfJCNbFMgZccbdkwkxMyuL
  - Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4
  - Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - Snatam Kaur and Ajeet Kaur ⋄ Sacred Chants By M U S I Q A A https://www.youtube.com/watch?v=xoBpkz_yay8&pbjreload=101


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
  - (1) YouTube LiveStream Chat
  - (2) YouTube + Firefox Web Browser (Picture-in-Picture Mode)
  - (3) iTerm
  - (4) Firefox Web Browser + https://www.timeanddate.com/
  - (5) Brave, Firefox, Tor Browser
  - (6) Visual Studio Code
  - (7) Express VPN
  - (8) Visual Studio Code . Previously: "Notes" on my macbook

Periodic Tasks

* [not completed] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)

Salutations

* [x] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [x] Talk about the possibilities for the next live stream




[Written @ 0:27]



[Written @ 0:11 - 0:27]

A list of things that I wrote down . . yesterday . . on October 2, 2020 . . in my physical paper material journal notebook . . 


. . . 

Self Identifying Information:
- Physical Location Address
- Email Address
- Phone Number
- Other Contact Information

Used for Purchasing and Sharing Contact Info[rmation] with Resource Traders or Community Participants

- Public
- Private
- Permission

. . . 


- Add Contact Information to the transaction
- Add Contact Information List Settings
- Dark and Light Theme Vuetify
- Settings Icon Button on account page

. . . 

- view transaction button
- repeat transaction button
- community participant is the digital currency account
- isShoppingMall
- isDigitalCurrencyExchange
- Product categories
- currency categories / supported currencies

- Information about the business


- Reset account uid (security)
- Public Text Memorandum & Private Text
- Block from Sending to Account Memorandum
- Block Account
- Like to Dislike Ratio Over Time
- Like the Transactions (Customer and Client)
- Account Analytics Defending (Security)
  * Before Submitting a transaction, check the analytics to try to stop unexepcted transactions [modified from the original notes for more clairty]
  - Time frequency [of transaction] [analytics to take into account]
  - Amount frequencey [of transaction] [analytics to take into account]

- Number of Accounts Transacted with over time
- currency amount overtime
- Number of Transactions over time
- Like to dislike ratio of transactions over time

- Recurring Transactions as Processed set to false
  - dateTobeProcessed = d
  - numberOfTimesProcessed = n
  - millisecondDelayBetweenProcessing = m
  - currenctDateForTransaction = c
  - const isAlreadyProcessedTimePeriod = d + n * m > c

- Ratio of Likes per Transaction
- Ratio of Dislikes per Transaction
- Number of Transactions


-- 
[Account related things]

- Account Username
- Account Name
- Account Taget Variable Tree
- Account Description
- Contact Information Variable Tree
  - website, instagram, email, phone number, street address [etc.]
  - permission status id for each contact information [public, private, permissioned]

- Purchasing Products & Services
- Selling Products & Services
- Other Methods
  - Explain what other usecases you have

[more thoughts]

- isPersonalAccount, isBusinessAccount



[Written @ 0:11]



* [] Plan the tasks to complete

  - fix the issues on the command line interface
  - update account
    - update account profile picture
    - update acocunt name
    - update account username
      1. add to queue map of users requested for that username
      2. Check queue map to see if user is the first to attend the operation
      3. Delete the entry map from existing username to accountId
      4. Update the entry map for the new username to accountId
      5. Change the username of the account [Firebase validate other operations]
      6. Delete from queue map of users registered for that username
  - get account
    - get account by qr code id
  - create account
    - create a qr code id
    - add to account-qr-code-to-account-id-map
    - add to create-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - delete account
    - add to delete-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - update transaction
    - update transaction amount
    - update transaction text memorandum
  - apply universal basic income
    - create a transaction for all accounts to add digital currency amount
  - initialize-service-account (sign in)
    - with google
    - with email and password
  - uninitialize-service-account (sign out)
  - gitlab continuous integration / continuous deployment (ci / cd)
    - publish ecoin development website
    - publish ecoin daemon to development server
  - readme.md
    - demo usecase preview: update transaction settings
    - demo usecase preview: view transaction information
  - html component
    - pay with digital currency button works on websites
  - usecase: community participant list
    - create community participant list for digital currency
    - create community participant list for digital currency exchange
      - digital currency exchanges that support ecoin transactions
    - create community participant list for online shopping mall
      - online shopping malls that support ecoin transactions
    - create community participant list for physical-location shops and stores
      - physical-location shopping places and stores like restaurants, shopping malls, shopping centers, grocery stores, food marts, small business store fronts, farmer's markets, etc.
  - update the styles of the desktop version of the website
    - account section
      - show the account section on the left
      - desktop version width should be not full width. [follow how it's done on the settings and about page]
      - desktop header tabs (v-tabs) should be condensed and not wide screen.
    - account statistics
      - show the account statistics on the right (allow vertical scroll)
    - community participants: show a list using vuetify table
  - settings page
    - 
  - add a `qr code` button
    - [header] Your Account QR Code
      - [subheader] Account QR Code for `account name`
    - [camera icon] Capture Account QR Code
    - find a digital currency account by its qr code by taking a photograph of the account qr code
  - network status page
    - number of ecoin distributed (timeline graph)
    - number of created accounts (timeline graph)
    - number of created transactions (timeline graph)
    - number of created community participants (timeline graph)
    - newly distributed ecoin (live updating)
    - newly created accounts list (live updating)
    - newly created transactions list (live updating)
    - newly created community participants (timeline graph)
    - total number of ecoin created (running sum of overall currency amount)
    - total number of accounts created (running sum of overall accounts amount)
    - total number of transactions created (running sum of overall transactions amount)
    - total number of community participants created (running sum of overall community participants)
  - about page
    - update the "Why does Ecoin exist?" answer 
      - "[A] A short answer like this is meant to encure your curiosity and sponsor your research project to learn more about these wounderous individuals. ;)"
      - enumerate the long answer clause



