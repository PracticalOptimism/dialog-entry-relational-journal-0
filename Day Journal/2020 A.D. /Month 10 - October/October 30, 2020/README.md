

# Day Journal Entry - October 30, 2020


[Written @ 23:53]

Notes @ Newly Learned Words

shope

[Written @ 22:05]

##### Live Stream Checklist
[copy-pasted from Day Journal Entry for October 29, 2020]

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [] Practice a breathing exercise for 5 - 15 minutes

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Blue Lotus Feet (Asherah) By Charanpal - Topic https://www.youtube.com/watch?v=p5mCSPeY6tE&list=OLAK5uy_nDVbZAbLowdfy69nsnDhGj0tgBoGTa8jg
  - Wah Yantee By Jai-Jagdeesh - Topic https://www.youtube.com/watch?v=njTJRGwaYIY&list=OLAK5uy_l5gmdP3hvHXWLnp8d0dKzvDGjfJVDUXgw&ab_channel=Jai-Jagdeesh-Topic
  - CLAUDIO - A bit of support for those still in lockdown By Rachel Claudio https://www.youtube.com/watch?v=NsdM4HBXLnQ
  - Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - 

Salutations

* [x] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream



[Written @ 22:04]

### Things that are planned to be completed
[Copy-pasted from October 29, 2020 Day Journal Entry]

Use the mobile-first design strategy:

- [x] Update the about page
  - [x] add text: "loading content . . . " while lazy load is incomplete
- [] Update the your account page
  - [] 
  - [] add a account description section
    - [] account id
    - [] account name
    - [] account username
    - [] account description
    - [] physical location address (country, city/town/province)
    - [] account contact list (email address, website url list, social media url list, physical mail address list, other items)
    - [] account business product listing
    - [] account currency exchange currency listing
    - [] account employer positions listing
    - [] 
  - [] use the name "account statistics" to contain "account balance history" and "transaction amount history" (received and sent transaction amount)
  - [] add a "find shops to buy products or services" section
    - [] add text: find shops that accept ecoin such as "individuals", "online commerce stores", "restaurants", "apartments", "house salesplaces", "gymnasiums", "sports recreation facilities" and more (useful keywords)
  - [] add a "find employers to work with" section
    - [] add text: find employers such as "individuals", "schools", "technology companies", "hospitals", "government agencies", "non-profit organizations", "local businesses" and more (useful keywords)
  - [] add a "find currency exchanges to trade with" section
    - [] add text: find digital currency exchanges to trade Ecoin for fiat currencies (ie. U.S. Dollar, Euro, Yuan, and more) or alternative digital currencies
  - [] add a "find ecoin alternatives" section
    - [] add text: find alternative digital currencies to send and receive money with
  - [] 


- [] Update the network status page
  - [] add a "digital currency statistics" section
  - [] add a "account statistics" section
  - [] add a "transaction statistics" section
  - [] add a globe activity flight map section




[Written @ 22:01]

Timeline Annoation:

[0:00 - 0:16:45]
Reviewed the gitlab page for the ecoin project . . scrolled through the README.md page . . 

[0:16:45 - 0:33:31]

Completed the following tasks:

- [x] Update the about page
  - [x] add text: "loading content . . . " while lazy load is incomplete

Scrolled through the project service website to see if there are any places where I feel like there could be improvements . .

[0:33:31 - 2:09:16]

Added the following expansion panels to the account page:

  - [x] add a "account statistics" expansion panel
  - [x] add a "find places to shop" expansion panel
  - [x] add a "find employers" expansion panel
  - [x] add a "find digital currency exchanges" expansion panel
  - [x] add a "find ecoin alternatives" expansion panel




[Written @ 5:32]


Approximate amount of time . . spent . . on the live stream for . . Ecoin #352 . . for October 29, 2020 . . https://www.youtube.com/watch?v=PGZbFu-H4NM&feature=youtu.be


6 hours . . 35 minutes . . and 16 seconds . . 

06:35:16.775


#1
06:35:16.775
Pause



[Written @ 5:13]


Dr. Silvia Perea
https://soa.princeton.edu/content/silvia-perea
https://www.museum.ucsb.edu/news/announcement/733
https://www.museum.ucsb.edu/people/silvia-perea


Dr. Clara Sousa-Silva
https://clarasousasilva.com/
https://eapsweb.mit.edu/people/cssilva
https://space.mit.edu/people/sousa-silva-clara




[Written @ 5:01]

Timeline Annotation:

[0:07:28 - 0:44:00]
Showcasing a list of YouTube Channels that I enjoy watching

[0:40:31 - 1:01:11]
Discovering Dr Clara Sousa-Silva and . . Silvia Perea . . appear similarly . . and recording this information . . in my day journal entry . 

[1:24:52 - 4:19:59]
Writing in my Dream Journal . . for October 30, 2020

[4:19:59 - End of video]
Writing in my Day Journal . . for October 30, 2020



[Written @ 3:28]

It seems like . . in thinking about this topic . . it can be interesting to thinking about how you would want to arrange the properties . . uh . . for example what to name the property or how to enumerate the property . . if you uh . . well it's uh . . maybe not so important but at first glance . . I'm thinking you don't uh . . necessarily want to say . . uh . . hmm for example . . 

property #1 . . could be . . uh . . any whole number . . 

uh . . and then . . property #2 . . could be . . any odd number . . 

property #3 . . could be . . any . . power of 10 . . 


1, 10, 100, 1000, 10000 . . . 

the powers of 10 have a certain property they are satisfying . . by arranging them in this order . . 

from least to greatest . . 

well . . 

10, 10000000000000, 1000, 1000000, 100000, 100, 1 . . . 

the powers of 10 . . aren't satisfying the . . from . . least . . to . . greatest . . property . . any more :O . . 

hmm . . 

well . . in writing the properties . . uh . . in a way that's uh . . enumerated we could be uh . . well . . uh . . it seems kind of arbitrary . . uh . . like the second listing of the powers of 10 here . . 

uh . . well . . uh . . how could we maybe organize the property list to be more satisfactory like the first listing with the least to greatest principle being applied ? . . 

hmm . . least to greatest . . 

normally when we talk about least to greatest we talk about . . numbers for example . . and so . . uh . . well . . numbers can be related with uh . . the less than and greater than symbol . . 

we could rely on our own less than or greater than symbol to relate to property definitions . . 

and so . . we uh . . for example . . uh . . could think about what makes . . a property definition uh . . less than another property definition . . uh . . well uh . . that's maybe one way to think about this topic . . 

what makes a property definition . . orderable in relation to another property definition . . and how could we order those property definitions to uh . . make it so we can uh . . easily find theoretical property definitions that we didn't know before . . and for some reason . . these properties in their order relationship are able to be . . uh . . related in this order statistic method in the same sense as numbers ? . . least to greatest ? . . is that the idea ? !? [scratching head emoji . . ] . . hmm . . I'm kind of flustered by this thinking

. . 

well it really seems that having properties listed in a uh . . incongruent way is uh . . maybe not the friendliest but I'm not sure . . maybe by discovering new properties and enumerating them . . it's like . . discovering new numbers ? . . and enumerating them ? . . haha . . strange analogy because at least with numbers it is easy to find the new number . . just by continuing to move forward so to speak a long the number line . . 

and what is the property definition number line that seems to relate to this analogy ? . . in which direction do we take to create a new definition for a property that can be satisfied by a number ? . . the realm of the imagination could be useful here :O . . 

the direction of thought . . 

because really . . aren't there so many possibilities of how you could arrange property definitions ? . . 

and so you could try to be convenient and maybe partition the space of the known property definitions and try to fit those into a nice grid that is convenient for today . . and in the morrow . . you can discuss a new partition on the topology that is able to grow . . 

. . . 

and in trying to place a property that is newly discovered into a place that is next to an already known property . . but that property index value is already taken . . one could . . create . . a new uh . . index . . uh . . section . . or index . . group . . that could relate to the section where the insertion or the relation was trying to be created from earlier . . 

so for example . . 

these are properties referenced by their property index number . . 1, 2, 3, etc.

1, 2, 3, 4, 5, 6 . . . 

so . . 1 refers to propert #1 . . 2 refers to property #2 . . hmm . . 


p1, p2, p3, p4, p5, p6 . . . 

okay . . so p1 . . refers to property #1 . . p2 . . refers to property #2 . . p3 . . refers to property #3 . . and so on . . and so forth in this way . . p100 refers to property #100 . . p101 refers to property #101 . . 

. . . 

p1, p2, p3, p4, p5, p6 . . . 


p1 through p100 . .
p101 through p200 . . 
p201 through p300 . . 


. . . 

let's say you discover a new property . . it has to be . . after p300 . . but . . wait a minute . . uh . . your new property . . really seems like . . a property that you've studied before in terms of uh . . certain characteristics or in terms of how you had grouped these properties already uh . . uh . . so for example . . maybe you placed all the quote-on-quote "easy" definitions inside of p1 through p100 . . and . . all of a sudden . . you discover a new . . quote-on-quote "easy" definition . . uh . . but . . uh . . well . . now you would like to uh . . allow room for more "easy" definitions to be created and identified . . should they be in p301 to p400 ? . . 


pNew . . is our latest discovered property definition . . 

. . . 

this seems a lot like finding a memory address to store a value in a computer .  

the adjacent memory addresses to the data you are interested in relating to . . is . . something . . 

hmm . . well . . I suppose we could take powers of 2 . . to be the start of another index listing . . or something like that . . and you can restart the index listing strategy by taking further powers of 2 . . hmm . . it's kind of a new idea to me . . one moment . . hmm . . 

. . . 

phase 1. p1 to p100 ["easy" definitions]
phase 2. p101 to p200 ["more easy" definitions]
phase 3. p201 to p300 ["medium" definitions]
phase 4. p301 to p400 ["difficult" definitions]
. . . 



2^0 is 1
2^1 is 2
2^2 is 4
2^3 is 8
2^4 is 16
2^5 is 32
2^6 is 64
2^7 is 128
2^8 is 256
2^9 is 512
2^10 is 1024

. . . 

phase 1. is in 2^0
phase 2. is the second phase in 2^1
phase 3. is the third phase in 2^2
phase 4. is the fourth phase in 2^3
phase 5. is the fifth phase in 2^4
phase 6. is the sixth phase in 2^5
phase 7. is the seventh phase in 2^6
phase 8. is the eighth phase in 2^7

. . . 

a newly discovered item for phase 1 . . appears in the next available power of 2 . . for that phase

. . . 

a newly discovered item for phase 1 . . an "easy" definition . . appears in the next available power of 2 . . for phase 1 . . 

the first phase in 2^0 is full since we have phase 1 defined . . 
the first phase in 2^1 is not full . . and so we can add our definition there

. . . 

length of the phase x the power of 2 = the size of the memory index for that power of 2


. . . 

. . . 

you can continue to add new phases . . and you can continue to add . . new items . . or new . . property definitions . . to each phase . . 

. . . 

a newly discovered item for phase 4 . . the "difficult" . . definitions . . 

. . . 

the first reference to phase 4 is in the 2^3 memory index . . 

the second reference to phase 4 will be in the 2^4 . . memory . . index . . 
. . . the newly discovered item for phase 4 . . can be placed here . . 

. . . when the second memory index for phase 4 is full

you can place the next newly discovered item for phase 4 in the 2^5 memory index . . 
. . . 

. . . 


. . . 

. . . 

what does that look like in practice ? . . . 

let's say you have phases that have 3 properties each . . 

. . . 


phase 1. 
- definition (hi there)
- definition (yes, hello)
- definition (wow, thank you)

phase 2.
- another definition
- another definition
- another definition

phase 3.
- wow yet another definition
- wow yet another definition
- wow yet another definition

phase 4.
- so cool i love definitions
- so cool i love definitions
- so cool i love definitions


. . . 

phase 1. index 2 . . [newly discovered definitions]
- definition (woah this is so cool)
- definition (wow this isn't so bad to be a phase 1)
- definition (wow, I love being a phase 1)


phase 2. index 2 . . [nothing here yet . . ]
- 
- 
- 




. . . 


memory index by 2^0 [1] (3 x 1 is 3)

phase 1. starts here

1. definition (hi there)
2. definition (yes, hello)
3. definition (wow, thank you)

memory index by 2^1 [2] (3 x 2 is 6)

phase 1. continues here

1. definition (woah this is so cool)
2. definition (wow this isn't so bad to be a phase 1)
3. definition (wow, I love being a phase 1)

phase 2. starts here

4. another definition
5. another definition
6. another definition

memory index by 2^2 [4] (3 x 4 is 12)

phase 1. continues here

1. [nothing here yet]
2. [nothing here yet]
3. [nothing here yet]

phase 2. continues here

4. [nothing here yet]
5. [nothing here yet]
6. [nothing here yet]

phase 3. starts here

7. wow yet another definition
8. wow yet another definition
9. wow yet another definition

10. 
11. 
12. 

memory index by 2^3 [8] (3 x 8 is 24)

phase 1. continues here

1. [nothing here yet]
2. [nothing here yet]
3. [nothing here yet]

phase 2. continues here

4. [nothing here yet]
5. [nothing here yet]
6. [nothing here yet]

phase 3. continues here

7. [nothing here yet]
8. [nothing here yet]
9. [nothing here yet]

phase 4. starts here

10. so cool i love definitions
11. so cool i love definitions
12. so cool i love definitions



13. 
14. 
15. 
16. 
17. 
18. 
19. 
20. 
21. 
22. 
23. 
24. 

memory index by 2^4 [16] (3 x 16 is 48)

. . . 

memory index by 2^5 [32] (3 x 32 is 96)

. . . 

memory index by 2^6 [64] (3 x 64 is 192)

. . . 

memory index by 2^7 [128] (3 x 128 is 384)

. . . 

memory index by 2^8 [256] (3 x 256 is 768)

. . . 

memory index by 2^9 [512] (3 x 512 is 1536)

. . . 

memory index by 2^10 [1024] (3 x 1024 is 3072)

. . . 

. . . 

continue . . as before . something like that . . 




. . . 


okay . . now we can simulate what it looks like to add a new . . phase 1 . . property . . definition . . 


and then what it looks like to add . . a new . . phase 2 . . property . . definition . . 
. . 

awesome  . . 

okay . . 

now let's see how that could look . . in the real world . . or something like that :o . . 

so  .. 

mathematician alexander gravy number 1 finds a clue on how to solve a problem . . but they are using a new definition to do so . . 

people all over the world would really love . . professor alexander gravy to use the new measurement system for measuring the relation of property definitions with the convenience metrics that have been established . . and so alexander gravy has to ask themselves . . 

🤔

. . . alexander . . are you okay ? . . are you thinking about something serious ? . . today ? . . 

. . . 

. . . 

. . . 

alright alexander . . your definition looks a lot like these other 500 definitions . . ahh . . right . . those are . . phases 1, 2, 3, 4, . . etc. . or uh . . 

they could be . . phase 1 and phase 3 definitions and that's all . . so . . we would like to maybe consider adding the definition to both of these phases . . 

we would do so by . . needing to add the definition to the memory index where each of these phases is available . . 

. . . so if phase 1. index 1. is all used up . . well we'll need to look at phase 1. index 2. 

and if phase 1. index 2. has all the definitions that it can handle . . or that are defined by our measurement system or something like that . . then . . we'll need to look at . . phase 1. index 3. 

. . we continue to use this algorithm of continuing from the earlier index to later and later indices . . or indexes . . however you would like to spell that word . . 

indices . . multiple indexes . . 

. . 

and so . . for index 4. if that one is available for example . . then . . alexander gravy . . gets a new position . . to add their definition to the list of existing definitions . . that belong to the phase 1 . . territory of definition types . . 

. . . 






Notes @ Newly Created Words

pharse
[Written @ October 30, 2020 @ 4:23]
[I accidentally typed the word . . "phase" . . as . . "pharse" . . I'm sorry . . I'm not sure . . what this word . . should mean . . at this time . . ]





























[Written @ 3:17]

. . . I'm quite surprised . . 

I'm not sure what to do with this information yet . 

hmm . . 

Numbers . . 

Numbers can have properties . .

The properties can be differentiated by identifying properties that they satisfy . . 

The way in which you differentiate the identities of each property is useful for being able to explore the differentiations of those identities . . 




. . . 

example . . 

property #1 is satisfied by:
- 1, 2, 3, 4, 5, 6, 7

property #2 is satisfied by:
- 2, 3, 5, 7, 11, 13

property #3 is satisfied by:
- 1, 10, 100, 1000, 10000, 100000

property #4 is satisfied by:
- 1, 11, 111, 1111, 11111, 111111

property #5 is satisfied by:
- 2, 22, 222, 2222, 22222, 222222

property #6 is satisfied by:
- 3, 33, 333, 3333, 33333, 333333

property #7 is satisfied by:
- 1, 3, 5, 7, 9, 11

property #8 is satisfied by:
- 2, 4, 6, 8, 10, 12

property #9 is satisfied by:
- 2, 6, 10, 14, 18

property #10 is satisfied by:
- 2, 4, 8, 16, 32, 64

. . . 

property identity #1 is satisfied by:
- property #4, property #5, property #6
- [1, 11, 111, 1111, 11111, 111111], [2, 22, 222, 2222, 22222, 222222], [3, 33, 333, 3333, 33333, 333333]

property identity #2 is satisfied by:
- property #7, property #8, property #9
- [1, 3, 5, 7, 9, 11], [2, 4, 6, 8, 10, 12], [2, 6, 10, 14, 18]

property identity #3 is satisfied by:
- property #3, property #10
- [1, 10, 100, 1000, 10000, 100000], [2, 4, 8, 16, 32, 64]


. . . 






[Written @ 0:57]

Notes @ Newly Created Words

prcoes
[Written @ October 30, 2020 @ 0:55]
[I accidentally typed the word . . "prcoes" . . instead of typing the word . . "processes" . . I'm sorry . . I'm not sure what this word "prcoes" should mean at this time]


[Written @ 0:12]


[Written @ 0:06]

Notes @ Newly Discovered YouTube Channels

Gary McIntyre Photographer
"Explore Imagine Create"
https://www.youtube.com/c/GaryMcIntyrePhotographer/videos
[Discovered by a YouTube Search Result for "silvia perea youtube channel" . . The video search result was [2]]


Azim Premji Foundation Puducherry
https://www.youtube.com/channel/UCe3j4WJjveHumwLPMCgpB8g/videos
[Discovered by a YouTube Search Result for "silvia perea youtube channel" . . The video search result was [1]]



[1]
Birds around us | Part - 2 | Bird talk in classroom
By Azim Premji Foundation Puducherry
https://www.youtube.com/watch?v=UmHDI9tI-lw


[2]
Frequency separation and eye sharpening
By Gary McIntyre Photographer
https://www.youtube.com/watch?v=ds3vef4IqI0


