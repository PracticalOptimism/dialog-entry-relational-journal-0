
# Day Journal Entry - October 27, 2020



[Written @ 19:27 - 19:31]

Notes @ Newly Created Words

calibtra
[Written @ October 27, 2020 @ 19:27]
[I accidentally typed the word . . "calibtra" . . instead of typing . . the word . . "calibrate" . . I'm sorry . . I'm not sure . . what this word . . "calibtra" . . should mean at this time . . ]


[Written @ 19:15 - 19:31]

Notes @ Probable Names For Rachel Claudio

Rachel Flaudio
[inspired by a flash-of-vision-with-my-physical-eyes-slash-mind-perception where I saw the characters of the name . . "Rachel Claudio" . . appear as . . "Rachel Flaudio" . . Flaudio . . Flaudio . . hmm . . I'm not really sure what this name means . . hmm . . hmm . .]

well maybe a note on probable names . . since sometimes I hear a probable name come to mind . . well . . it's uh . . not necessarily probable in the sense that it's only probable and isn't actualized or something like that . . and so for example . . some names that I have heard in my head are names that are normal or are familiar to me in some sense and so in that sense those names are already . . uh . . actualized . . and are uh . . based on evidence of personal experience of uh . . being in the presence of people with those names or something like that . . or uh . . hearing about someone who has something of a name like the one that I have heard . . Alex . . Grace . . are a few of the names where I have also experienced people that call themselves by these names . . something like that . . 

uh . . well . . hmm . . maybe in an alternate reality . . there is a person named . . "Rachel Flaudio" . . and so they are someone who lives their own lives and has their own scents and feelings . Or something like that . Scents and feelings scents and meanings scents and seeings scents and meanings scents and feelings scents and dealings scents and reelings scents and seeings . Scents and ghosts. -_- I'm sorry . . well uh . . that's uh . . those are a few thoughts right now

uh . . hmm . . anything else -_- > ? is that a poor way to end a live stream --__ __---___ ? / . . -_- 

hmm . . hmm . . well . . I would like to go running today . . it's kind of late . . it's uh . . well . . 19:24 . . that's uh . . 7:24pm . . uh . . well . . uh . . it's dark outside . . I was hoping to maybe run . . outside at the local park . . uh . . well . . uh . . it's dark . . uh . . hmm . . so maybe I'll uh . . maybe uh . . stay inside or uh . . I don't really know . . I'll uh . . maybe come back for another live stream today : / . not sure . . hmm . . hmm . . I haven't run outside for more than . . 1 or 2 weeks . . hmm . . 

hmm . . maybe 1 week or 2 weeks or maybe a little bit more . . I'm not exactly sure . . hmm . . it feels like . . it's been more than 1 week certainly . . uhm . . but . . that's an emotional feeling . . and the history requires reminding myself . . hmm . . it feels like its been more than 1 week . . and yet . . hmm . . maybe less than 2 weeks . . and so . . I will say . . I am . . more than 50% confident that it could have been 1 to 2 weeks ago when I last ran outside at the park . . but . . hmm . . I am maybe less than 89% confident that it has been 1 to 2 weeks ago when I last ran outside at the park . hmm even that statement needs to be qualified . . my confidence is based on . . -_-

okay . have a good night :) 


[Written @ 19:07]

Notes @ Newly Discovered Names

Charel
Charel Claudio
[A typographical error of "Rachel" . . I accidentally typed the word . . "Ch" . . while trying to type the name "Rachel Claudio" . . "Charel" . . could have been the possible result of what I was going to type . . ]

Notes @ Newly Discovered Words

goig
[Written @ October 27, 2020 @ 19:06]
[I was going to type the word . . "going" . . and accidentally typed the word . . "goig" . . I'm sorry . . I'm not sure what this word . . "goig" . . should mean . . at this time :O ]


[Written @ 18:40]

Notes @ Newly Learned Words

prie


[Written @ 17:33]

Notes @ Newly Discovered Names

Gekoh
[Written @ October 27, 2020 @ 17:36]
[I wrote this name . . earlier today . . The name came from a typographical error . . uh . . well . . uh . . I'm uh . . right uh . . well . . I think it was a typographical error . . but . . I forgot to type . . the name of the word . . or the word itself that I was originally intending to type . . Gekoh . . reminds me of the name . . Goku . . from Dragon Ball Z :O . . ]

Notes @ Newly Created Words

supro
[Written @ October 27, 2020 @ 17:35]
[I accidentally typed the word "supro" . . instead of typing the word . . "surprising" . . well . . I'm uh . . I'm sorry . . I'm not sure . . what this word . . "supro" . . ]

suprri
[Written @ October 27, 2020 @ 17:33]
[I accidentally typed the word "surprising" . . as "suprri" . . I'm sorry . . I'm not sure . . what this word "suprri" . . should mean . . at this time . . I do like the sound of the word . . however . . it sounds quite cool . . "suprri" . . prri . . is an interesting sound . . it's like . . super . . but . . "suprri" . . prri . . prii . . awesome]


[Written @ 17:31]

Notes @ Newly Learned Word Relations

Invoking
Invoke
Invocation

[Invocation is spelled with a "c" . . instead of . . with a "k" . . well . . I was uh . . not really sure . . this is an interesting discovery]



[Written @ 13:37]

[Copy-pasted from October 17, 2020]

* Things that are planned to be completed

- [] http-server/data-structures/provider-service-emulator-http-server-proxy
  - [] lunr service http server proxy
  - [] firebase emulator service http server proxy
    - [] initialize provider service
      - [] initialize provider service by http server proxy
      - [] initialize provider service for firebase algorithms
    - [x] create item
    - [x] get item
    - [] get item list
    - [] update item
    - [] add item event listener
    - [] remove item event listener
- [] daemon/data-structures/administrator-daemon
- [] daemon/data-structures/service-activity-simulator-daemon
- 



- [] add multi-language support [internationalization; i18n]
- [] add language support button
- [] fix the issues on the command line interface
- [] update account
  - [] update firebase auth uid for security
  - [] update account profile picture
  - [] update acocunt name
  - [] update account username
    1. [] add to queue map of users requested for that username
    2. [] Check queue map to see if user is the first to attend the operation
    3. [] Delete the entry map from existing username to accountId
    4. [] Update the entry map for the new username to accountId
    5. [] Change the username of the account [Firebase validate other operations]
    6. [] Delete from queue map of users registered for that username
  - [] update account by adding blocked account
    - [] can no longer send currency to this account
    - [] can no longer receive currency from this account // ???
  - [] update account by removing blocked account
    - 
  - [] update account behavior statistics // for security, fraud detection, transaction overuse
    - [] admin only read and write
    - [] measure the frequency of creating a transaction
    - [] measure the transaction amount median
    - [] freeze an account in case of abnormal account use ??
- [] get account
  - [] get account by qr code id
- [] create account
  - [] create a qr code id
  - [] add to account-qr-code-to-account-id-map
  - [] add to create-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] delete account
  - [] add to delete-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] update transaction
  - [] update transaction amount
  - [] update transaction text memorandum
  - [] update transaction by adding a like
  - [] update transaction by removing a like
  - [] update transaction by adding a dislike
  - [] update transaction by removing a dislike
  - [] update transaction by adding a comment
  - [] update transaction by removing a comment
- [] create transaction
  - [] include permissioned contact information
  - [] text memorandum
  - [] private text memorandum
  - 
- [] apply universal basic income
  - [] create a transaction for all accounts to add digital currency amount
- [] initialize-service-account (sign in)
  - [] with google
  - [] with email and password
- [] uninitialize-service-account (sign out)
- [] gitlab continuous integration / continuous deployment (ci / cd)
  - [] publish ecoin daemon to development server
- [] readme.md
  - [] demo usecase preview: update transaction settings
  - [] demo usecase preview: view transaction information
- [] html component
  - [] pay with digital currency button works on websites
  - [] create an html component with an initial property object
  - [] return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
  - [] return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
- [] digital currency account data structure
  - [] business acount description
  - [] does this account represent a copy or fork of the ecoin source code?
    - [] how many accounts does this currency have?
    - [] how many transactions does this currency have?
  - [] is a digital currency exchange that transacts using ecoin
  - [] is business account, selling products and services using ecoin
    - [] is providing currency exchange service
      - [] supported currency list
    - [] is providing products
      - [] product category list, product price ranges
    - [] is providing services
    - [] is providing other
  - [] is a physical location shop or store
    - [] restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
  - [] contact information variable tree (email, phone number, website url, physical address etc.)
    - [] permission rule on variable tree item: Public, Private, Permissioned
  - [] account statistics variable tree (set fields like 'number of employees' and other information)
  - [] are you seeking employment?
    - [] employment skills list
    - [] sought after ecoin payment amount per time period
  - [] are you an employer?
    - [] are you hiring right now
    - [] available position variable tree
      - [] ecoin payed per time period
      - [] sought after skills list
      - [] position name, time description (ie. 30 hours / week)
  - 
- [] update the styles of the desktop version of the website
  - [] account section
    - [] show the account section on the left
    - [] desktop version width should be not full width. [follow how it's done on the settings and about page]
    - [] desktop header tabs (v-tabs) should be condensed and not wide screen.
  - [] account statistics
    - [] show the account statistics on the right (allow vertical scroll)
  - [] community participants: show a list using vuetify table
- [] settings page
  - [] add light and dark theme setting
- [] add a `qr code` button
  - [] [header] Your Account QR Code
    - [] [subheader] Account QR Code for `account name`
  - [] [camera icon] Capture Account QR Code
  - [] find a digital currency account by its qr code by taking a photograph of the account qr code
- [] network statistics page
  - [] number of ecoin distributed (timeline graph)
  - [] number of created accounts (timeline graph)
    - [] number of created business accounts (timeline graph)
  - [] number of created transactions (timeline graph)
  - [] newly distributed ecoin (live updating)
  - [] newly created accounts list (live updating)
    - [] number of created business accounts (timeline graph)
  - [] newly created transactions list (live updating)
  - [] total number of ecoin created (running sum of overall currency amount)
  - [] total number of accounts created (running sum of overall accounts amount)
    - [] total number of business accounts created (running sum of overall community participants)
  - [] total number of transactions created (running sum of overall transactions amount)
  - [] flights gl map to show transactions in realtime
- [] account page
  - [] navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
  - [] add settings icon button
  - [] transaction list should show "view transaction button"
    - [] open transaction dialog
  - [] transaction list should show "repeat transaction button"
  - [] like to dislike ratio timeline graph
  - [] number of accounts transacted with timeline graph
  - [] currency amount transacted timeline graph
  - [] number of transactions timeline graph
    - [] ratio of likes from recipient, sender
    - [] ratio of dislikes from recipient, sender
  - [] calendar heatmap of transactions created
  - [] find employers to work with
    - [] work with businesses that pay in Ecoin
  - [] find businesses to shop with
    - [] shop with businesses that transact using Ecoin
  - [] find currency exchanges to trade with
    - [] exchange Ecoin for other currencies
- [] about page
  - [] update the "Why does Ecoin exist?" answer 
    - [x] "[A] A short answer like this is meant to encure your curiosity and sponsor your research project to learn more about these wounderous individuals. ;)"
    - [] enumerate the long answer clause
- [] apply transaction list
  - [] Recurring Transactions as Processed set to false
    - [] dateTobeProcessed = d
    - [] numberOfTimesProcessed = n
    - [] millisecondDelayBetweenProcessing = m
    - [] currenctDateForTransaction = c
    - [] const isAlreadyProcessedTimePeriod = d + n * m > c
- [] website style / theme
  - [] research how to improve account page style for desktop
  - [] research how to improve settings page style for desktop
  - [] research how to style the network status page
  - 
- [] readme.md
  - [] add the names, and website urls of the software dependencies items (use the same layout as the related work section)
  - [] remove list index from the software dependencies table
  - [] add appended list to each list item in the table of contents
  - [] add "completed" text to the features description text in the readme.md file
  - [] fix typo to be "for storing data on remote computers", in software dependencies / database provider / firebase realtime database section
  - [] add the domain name registration service used for https://ecoin369.com . . GoDaddy
  - [] add website library "echarts" (for logistics measurement display)
  - [] add expectation definitions measurement tools: mocha, chai
  - [] add project compilation: node.js, npm
  - [] add prepaid credit card service (with anonymous support): Bluebird, American Express
  - [] add the project authors for the software dependency list
  - [] update the software depenencies message to change the open source software message
  - [] consider adding a frequently asked questions section to the readme
  - [] add a guide to get started in publishing your own ecoin-based digital currency
    - [] easily change the name, image icon, resource urls and project description
      - [] new directory for ease of use ? or precompiled/@project ? 
    - [] how to add firebase credentials for development and production environments
      - [] create firebase api token using the command line interface tool
    - [] how to add algolia credentials for development and production environments
    - [] how to publish the project service website
    - [] how to publish the administrator daemon
  - [] add a guide to get started with the development of the project source code
    - [] how to install nodejs and npm
    - [] how to install the firebase emulator
    - [] how to install ngrok
    - [] how to start the administrator daemon
    - [] how to start the firebase emulator
    - [] how to start the service provider http server proxy
    - [] how to start the development server for the project service website
    - [] how to run a measurement of the expectation definitions (also known as unit tests but also includes integration tests)
    - [] how to start the service activity emulator daemon
    - [] an introduction to the troy architecture
    - [] 
- [] digital-currency-business data structure
  - [] number of employees, are you hiring at the moment (and do you pay in Ecoin), salary range, number of available positions, business type, are remote positions available, 
- [] firebase cloud firestore
    - [] initialize the algorithms and expectation definitions
    - [] initialize the security rules for creating, updating and deleting
    - 
- [] javascript library demo
  - [] create a demo html page for creating a "pay with ecoin button"
  - [] create a demo html page for creating a "ecoin price label"






[Written @ 12:29]

Notes @ Newly Created Words

basicin
[Written @ October 27, 2020 @ 12:28]
[I accidentally typed the word "basicin" . . instead of typing the phrase . . "universal basic income" . . well . . I'm sorry . . I'm not sure . . what this . . word . . "basicin" . . should . . mean . . at . . this . . time]

[Written @ 10:17]

Ecoin #350 - Live development journal entry for October 27, 2020


Timeline Annotation:

[0:00:00 - 0:28:00]
I spent time working on fixing the command line interface errors relating to the web browser project service website


[0:28:00 - 1:33:44]
I spent time working on the universal basic income animation scene to be able to right click in the area where the scene div element is displayed after the scene is no longer being displayed


[1:33:44 - 2:34:30]
- ensure the universal basic income animation scene isn't overflowing on the mobile phone device display
- added a search icon to the header of the web browser project service website
- do a search through the website to gain an intuition on how people will use the website to see if you like the way things are
- horizontally align the universal basic income notification content items

[2:34:30 - 4:25:34]
I spent some time reviewing and updating the about page for the web browser project service website. 

I spent some time reviewing the appearance and performance of the web browser project service website on my mobile cellphone device.


[4:25:34 - 6:09:06]
- updated the create transaction request component to always show the "send payment" and "cancel" buttons (also known as v-dialog card action items) regardless of the height of the containing dialog component
- updated the account sign in component to always show the "sign out", "cancel" and "select account" action buttons regardless of the height of the containing dialog comoponent

[6:09:06 - 9:04:53]
- updated the gitlab repository with the latest source code changes (https://gitlab.com/ecorp-org/ecoin)
- published the development version of the website (https://ecoin-f9c5c.web.app/)


[Written @ 9:22]

Notes @ Newly Learned Words

Furtive [1 @ Video Description]

[1]
CLAUDIO - A bit of support for those still in lockdown
By Rachel Claudio
https://www.youtube.com/watch?v=NsdM4HBXLnQ



[Written @ 8:50]


### Planned Tasks For Today
[copy-pasted from Day Journal Entry for October 26, 2020]

- [x] Update the transaction dialog design to match expectations
  - [x] update the send payment and cancel button to always show at the bottom of the dialog
- [x] Update the width of the universal basic income notification animation to not overflow
- [x] Add a search icon before the search account text field in the header
- [x] Publish the development website changes
- [x] Add close button to the digital currency universal basic income notification snackbar item



### Planned Tasks For Today or Tomorrow
[copy-pasted from Day Journal Entry for October 26, 2020]


// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider


// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list


// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)


// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts


##### Live Stream Checklist
[copy-pasted from Day Journal Entry for October 26, 2020]

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [used-the-toilet-during-the-live-stream] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Blue Lotus Feet (Asherah) By Charanpal - Topic https://www.youtube.com/watch?v=p5mCSPeY6tE&list=OLAK5uy_nDVbZAbLowdfy69nsnDhGj0tgBoGTa8jg
  - Wah Yantee By Jai-Jagdeesh - Topic https://www.youtube.com/watch?v=njTJRGwaYIY&list=OLAK5uy_l5gmdP3hvHXWLnp8d0dKzvDGjfJVDUXgw&ab_channel=Jai-Jagdeesh-Topic
  - CLAUDIO - A bit of support for those still in lockdown By Rachel Claudio https://www.youtube.com/watch?v=NsdM4HBXLnQ
  - 


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [checked-non-regularly] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - ???

Salutations

* [x] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [x] Talk about the possibilities for the next live stream


[Written @ 0:05]

Approximate amount of time spent live streaming yesterday (October 26, 2020) for Ecoin #349


01:07:09.477

#1
01:07:09.477
Pause

