

# Day Jouranl Entry - October 24, 2020

[Written @ 8:01]


[Written @ 7:59 - 8:01]

Approximate amount of time . . spent live streaming today (and yesterday, October 24, 2020) for Ecoin #346 . . https://www.youtube.com/watch?v=xVyDJ-t0nk8


09:55:58.645

#1
09:55:58.645
Pause

[Written @ 7:41]

Notes @ Newly Discovered YouTube Channels

Viridiana Yoga
https://www.youtube.com/c/ViridianaYoga/videos
[Discovered by: YouTube front page recommended video to watch . . https://www.youtube.com/watch?v=JIIzlNoUOCI]



[Written @ 6:10]

Notes @ Newly Learned Words

Poil

[Written @ 6:08]

Notes @ Quotes

"
The politics . . of disclosure . . about U.F.O. reality . . is . . I can think . . only would be a nightmare . . for any president of the United States . . 

It's like being a little bit pregnant . . You can't do a little bit of disclosure . . it won't sell . . I-it . . really is a situation . . where . . the people . . ordinary people . . have gotta recognize . . that it's in their interest . .

To have disclosure on . . U.F.O.s . . 

Really . . for their survival . . of the system of government . . that we all grew up believing we had . . A government . . of the people . . by the people . . for the people . . 

And disclosure is . . is . . a . . critical . . way . . in attaining . . that . . because . . it is . . the opening up . . of . . a . . huge . . part of our history . . that has been denied to us . . for 60 years . . 

That's a big deal !
"
-- Richard Dolan
[1 @ 0:00 - 0:51]




[1]
Richard Dolan on UFO Disclosure, 2005
By Richard Dolan
https://www.youtube.com/watch?v=j9o70itBTsc



[Written @ 5:08]

Notes @ Newly Discovered YouTube Channels

AlienScientist
https://www.youtube.com/user/AlienScientist/videos
[Discovered by: [2 by YouTube Search Result for "Area 51 Lockheed SkunkWorks Scientist"]]

TheOldScientist
https://www.youtube.com/c/TheOldScientist/videos
[Discovered by: [1 @ 3:02:20]]

Glowing Ghost Paranormal
https://www.youtube.com/c/GlowingGhostParanormal/videos
[Discovered by: [3 by YouTube Search Result for "project blue book alien interview" ]]

California FIGU-Interessengruppe für Missionswissen
https://www.youtube.com/channel/UCRSbrt08J9Z5wWQ-Ky7m2KQ/videos
[Discovered by: [4 by YouTube Search Result for "Billy Eduard Meier"]]

Earthfiles
https://www.youtube.com/c/earthfiles/videos
[Discovered by: [YouTube Search Result for "Linda Moulton Howe"]]


[1]
Wolfram's New Physics Project & Hypergraphs for Dummies
By AlienScientist
https://www.youtube.com/watch?v=sMmZk5BSGo8


[2]
Area 51 Lockheed SkunkWorks Scientist Deathbed Confession: Real or Fake?
By AlienScientist
https://www.youtube.com/watch?v=6tpaOoru1j4

[3]
Roswell Alien Interview | MichaelScot
By Glowing Ghost Paranormal
https://www.youtube.com/watch?v=cop6LUfNWgE

[4]
Film clips of the Billy Meier Contact case as investigated by Nippon TV.
By California FIGU-Interessengruppe für Missionswissen
https://www.youtube.com/watch?v=jgsPf3ohcDQ

[Written @ 4:13]

Notes @ Newly Remembered Quotes


[quote #1]
"To a god . . all names belong to it" [1 @ One of the early chapters]

[thoughts]: I had thought about what was said here . . It is quite a shocking insight into the world to imagine that a character or entity or personality of the world could carry all names as having their uh . . being or representation . . uh . . for example . . suppose your name is . . John Smith . . uh . . and you perform certain activities for your work . . uh . . well . . uh . . the idea that . . god . . or a god . . also takes on your . . uh . . being ness . . and so they are also John Smith . . is a uh . . interesting idea . . uh . . and so . . also there are names like . . "Jane Smith" . . uh . . and so . . the god is also considered as being . . Jane Smith . . and is able to do the things that Jane Smith can do . . and is able to have the friends and relationships that Jane Smith has . .

Atoms . . Molecules . . Quantum Particles . . Waves . . Light . . Neutrinos . . Sub-Neutrinos . . Quarks . . Atomic Particles . . Sub-Atomic Particles . . Hello . . Hi . . How Are You ? . . These are all names . . and those names are beings of their own in that they embody certain characteristics . . uh . . they are part of a language of being uh . . in the minds of the beings that create the meaning of these characters and the stories they take or the rhythms of how they interact with one another . . Does it mean something to say "Hello" to an Atom or a Molecule . . which are the thoughts that are related ? . . Atoms can have mass in our universe . . And "Hello" can have mass in the universe of social interactions in terms of a attractive bodied essence of some poetic type in terms of having maybe a relational equation in how people are attracted to bodies of friendliness . . or maybe that is maybe an uninvited quest into poetry parameters that are not necessarily suitable to the questions of scientific intrigue . . or something unexpected like that . ugh I'm sorry . . 

uh . . to have all the names belong to someone is quite an interesting thought and so your identity is really . . uh . . well . . it reminds me of the topic that Unicole Unicron raised in Cam Church today . . "Ultimate Reality" . . where all experiences of all perspectives of all the entities across all of time . . is experienced at once . . [3 @ 4:26 - 7:17]

[quote #2]
"The subconscious mind is like a corridor . . what difference does it make . . which door that you choose to open?"

[thoughts]:
when asked the question . . "how do we know that you're not a part of our subconscious? [subconscious mind]" . . as Jane Roberts was channeling . . a ghost . . or spiritual entity with whom they were not familiar . . the response of quote #2 . . or something similar was received

[thoughts]:
. . . This isn't an exact quote . . of what was said in the book at [1] . . but this is what I can remember at this time of writing this message . . It seems to me to suggest something like . . the subconscious mind being at a certain way . . like a shared hallway of experience and you can select which experience by selecting a doorway . . something like that . . it's kind of a shaky way of explaining it . . a shaky way of explaining the quotation . . uh . . well . . uh . . maybe uh . . uh . . well . . maybe I also misinterpreted the message . . uh . . for example . . uh . . uh . . well . . uh . . the idea that uh . . I get . . uh . . which uh . . I suppose I'm not trying to be interpretting the message incorrectly uh . . but in trying to interpret the topic correctly . . uh . . I have the idea . . or the idea that I get is imagining a hallway where the doors are conscious experiences . . like the experience that Jane Roberts received when channeling . . the ghost . . and uh . . the same uh . . door of experience could perhaps be uh . . the way of how to possibly interpret the experience that uh . . I am having . . as I type these words . . or maybe as you are having as you read these words . . and so there is a consciousness . . or uh . . non-subconscious door experience that was opened . . and so the things that aren't immediately conscious are uh . . well . . those uh . . those uh . . well . . 2 seconds ago may not be immediately a conscious experience to someone . . and 2 years ago may not be a conscious experience to someone . . uh . . and so uh . . which uh . . conscious experience did you have uh . . 2 seconds ago ? . . do you still have the present conscious immediate attention to help you decide what that experience was . . Is that memory experienced now . . as you actively choose to create it as a conscious experience ? . . is that they way of interpretting this topic . . at this time . . I'm not sure -_- . . . I'm sorry . . I'll reconsider coming back to this topic or something like this . 



. . . The quote is from . . [1] but . . I'm not sure what the actual quote is . . this is possibly an approximation . . "To a god . . all names belong to him" . . or . . "To a god . . all names belong to him" . . uh . . I'm not sure . . him or her or it . . these are pronouns that are uh . . possible to have been used . . uh . . to maybe not have a gender bias . . "it" . . may have been used . . although it sounds strange uh to maybe not provide a gender . . uh . . well . . uh . . this is also a problem that uh . . was discussed in "Tao Te Ching" . . [2 @ ix to x]



[1]
The Seth Material - Introduction (1 of 2)
By Tim Hart Hart
https://www.youtube.com/watch?v=Jlcos5ZM2NE&list=PLPDTOFbrYdqA0vpIROtyd3tXbzkK49E5T


[2]
Tao Te Ching
By Stephen Mitchell
[Book Reference]


[3]
Cam Church - Ultimate Reality and Personal Reality
By UNICULT
https://www.youtube.com/watch?v=bOyCwQqZWeY


[Inspired memory from reading . . "God has no Religion" . . from [1 @ Thumbnail]]

[I read the message from [1] . . that reads . . "God has no Religion" . . and was inspired . . to think of this other message that I remember . . ]


[1]
3166【01重】All Religion in just One+God has no Religion すべての宗教はひとつ；神に宗教はないんだよ（ガンジー）by Hiroshi Hayashi,
By Hiroshi Hayashi
https://www.youtube.com/watch?v=YLeITjvaoBA

[Written @ 3:50]


[Written @ 3:44]

Notes @ Newly Learned Words

Japabese
[1 @ Video Title]


[1]
3341【15G】The Map of Arctic Ocean in Japabese Bronze Mirror日本の銅鏡の中に彫刻されていた北極海の地図by Hiroshi Hayashi, J
By Hiroshi Hayashi
https://www.youtube.com/watch?v=k1L2zODzNGs


[Written @ 3:40]

Notes @ Newly Discovered YouTube Channels

Hiroshi Hayashi
https://www.youtube.com/c/hiroshihayashi/videos
[Discovered by: ]


[Written @ 1:20]

Notes @ Newly Learned Lesson on Octaves of Light

"
Each of us is vibrating on a particular frequency . . Each of us is cultivating . . our own . . octave . . of light . . 

And what that means . . is that . . Just like you have on a piano . . where you've got . . an . . octave . . of notes . . And you've got all the notes . . in between . . When each person . . is . . in harmony . . with . . their true . . heart . . When each person . . is in alignment . . with . . their . . divinity . . 

They become . . an octave . . They become . . a pure . . harmonic . . energy . . 

So . . as there are only so many harmonies you can play on a piano . . in the universe . . there is . . infinite . . harmony . .

So imagine . . the beautiful sound . . If we're thinking of it in terms of . . sound . .

Imagine the beautiful sound that is created by entire planets that are operating in a harmonic . . way . . [*1]

And it's not just sound . . but . . it's . . it's energy . . sound is just a way for us to understand . . harmony . . 

Harmony can exist . . energetically . . as well . . 

. . . 

. . . 

So . . when we think of . . a friend group . . who really gets along . . Who really supports . . each other . . who . . is . . really . . in harmony . . with one another . . We can feel . . the beautiful . . vibrations . . that are coming off of that . . social endeavor . . 

We can feel the beautiful energy . . that is radiating out . . of each person . . as they . . feel . . supported . . 

And when we get an entire planet . . to feel that energy . . To . . support . one . another . 

And to vibrate on . . the highest possible reality . . That . . they . . can . . operate . on . 

Then . . we . . create . . a . . world . . that . . is . . not . . only . . fun . . to . . live . . in . . 

But . . we create . . a world . . that is optimal . 
"
-- Unicole Unicron
[3 @ 11:45 - 13:26]



[*1]
This is a really interesting statement . . I haven't heard a statement like this before . . I'm not sure . . what Unicole Unicron . . is thinking about in this regard . . of . . "entire planets" . . An entire planet of people is imagined for the planet Earth today . . in 2020 A.D. as I imagine the world to operate in the majority consciousness effort . . 

And yet . . the message at [*1] seems to suggest . . or uh . . my interpretation is that "entire planets" could refer also to . . perhaps not only planets that are uh . . perhaps uninhabitated . . like existing . . uh . . planets like Venus . . or Saturn or Jupiter . . or something like this . . 

"entire planets" . . could also relate to imaginary cosmos of planets that exist in their own consciousness experience . . and so for example . . although perhaps these worlds are hypothetical from the perspective of a human being in the mental perspective uh . . of earth . . today or something like that . . I'm not sure if that's the best way to write that previous sentence . . uh . . well . . the idea is that . . uh . . "entire planets" . . really uh . . when I think about this topic that Unicole Unicron is introducing here . . I imagine . . other . . Earth-like planets . . with people . . uh . . that are living out . . uh . . consciousness story lines of how those people develop and how they work together or how they do things in relation to one another . . uh . . not all uh . . consciousness endeavors need to appear in a way that looks like the one we operate here on Earth . . with a variety of cultures and thoughts and languages and religions and consciousness perspectives and moral thoughts . . things like this . . uh . . well . . for example . . I imagine there are people there in the consciousness spaces that exist that are . . uh . . all white people . . or . . all black people . . or . . all . . asian people . . or . . all blue people . . or . . all . . reptilian people . . or . . all . . homosexual males . . or . . all . . people that are born from plants . . or . . all people that are living for a lifespan of 2000 or more years . . or . . uh . . people that live with one large continent instead of multiple land continents on the planet . . or uh . . well . . there are so many variations of opportunities for how people can experience life on a planet . . and although from the perspective uh . . well . . they may seem hypothetical . . the reality of when we are created in the relation to this or that person being elected or this or that person creating this or that device or this or that person introducing this or that idea . . these are hypothetical realities that are able to be considered and the storyline can perhaps be considered in other respects and more information is considered by the consciousness that invites that reality into their presence . . or something like that . . I'm not really sure how this topic works . . uh . . but in any case . . there seems to be . . something about thought that is uh . . still left to be considered . . uh . . at least in my own personal case . . as I think about the nature of reality and how it relates to consciousness experiences . . 

Uh . . uh . . well . . uh . . uh . . consciousness creates form . . and it's not the other way around . . consciousness creates experiences of space and time . . consciousness is in the position to convert energy into matter and matter into energy . . consciousness initiates the transformation of energy into matter . . consciousness is always in the position to initiate the transformation of energy into matter and matter into energy . . energy is conscious . . matter is conscious . . consciousness units . . uh . . well . . these are some of the things that I've learned . . uh . . well . . uh . . it seems consciousness is in the position to create physical reality experiences . . uh . . well . . which also after some time in thinking about this topic . . I've come to consider that then it is reasonable to consider the conscious experience of . . uh . . cartoon animation realities . . uh . . where cartoon characters from movies or video games are actually real uh . . people or real characters in their own consciousness realities uh . . and uh . . even if they are represented here on Earth in a particular way . . uh . . probably they could exist . . uh . . or they do uh . . maybe . . I'm not sure . . uh . . it really relates to consciousness which is a topic that I'm uh . . still . . uh . . learning about . . 

[*1]
"Imagine the beautiful sound that is created by entire planets that are operating in a harmonic . . way . ."

I imagine . . the Pleiadian planet . . or the group of Planets operated by the Pleiadian people . . which maybe . . perhaps . . thoughtfully . . includes . . Erra . . which is a planet that's been discussed in the Billy Meier Pleiadian literature . . And uh . . maybe other Pleiadian literature . . that we have uh . . available here on planet Earth today . . uh . . Well . . uh . . when I think about . . harmonious . . or harmonic . . or . . possibly harmonic planets . . 


"so . . that is harmony . . that is what I'm talking about when I talk about harmony . . 
when every person aligns themselves . . with their true selves" . . 


uh . . well . . uh . . I'm not really sure . . uh . . I think about the Pleiadian people . . when I think about harmony . . uh . . but also the topic of harmony is personally quite new to me at this time and so I'm not really sure uh . . for example . . the topic of . . "when every person aligns themselves with their true selves" . . uh . . well . . uh . . I don't know . . uh . . the true uh . . well . . uh . . I'm still learning about the topic of this relation but I am really excited to learn more from Unicole Unicron :)


. . a planet of Isabel Paiges and Hitomi Mochizuki . . that could be quite harmonious as well :D 

. . . Alicia . . Devon . . Rachel . . Isabel Paige . . 

Hitomi Mochizuki . . Nia . . Marisa . . and Dianna

. . . those would be quite beautiful memories for a planet to create . . when each person is happy to create their realities together or something like that . . :D 

well . . it's only a theoretical idea . . and really in practice maybe it's not even clear the regularities of patterns for supporting such a style of existence . . each has their own conditions for living and those conditions change as the person creates their own experience . . 

Well . . on first thought . . I think eating food is a common pattern for people to share . . and so for example having food . . :D

Well . . food is nice . . and water . . and shelter . . and time to dream and sleep . . 

Time to dream and sleep and journal and reflect . .

I don't know . . it's really quite varied and it seems like the varations of time split realities can themselves be experienced through ventures of consciousness that can involve relying on different experience tunnels

. . -_-

. . all realities exist at once
[this is a lesson I've been suggested to understand through reading the books by . . "Seth" and Jane Roberts and Robert Butts]

xD

. . xD uh -_- 


Notes @ Newly Created Words

transof
[Written @ October 24, 2020 @ 1:48]
[I accidentally typed the word . . "transof" . . instead of typing the word . . "transformation" . . I'm sorry . . I'm not sure . . what this word . . "transof" . . should mean . . at this time . . ]



[Written @ 0:16 - 1:18]

In thinking about [Quote 1] . . I could think of . . 2 examples . . right now . . that are really . . exemplary . . of the qualities . . that . . Unicole Unicron . . is talking about . . 

In both cases . . [1] and [2] . . there are 4 people who are . . camping together . . And it has been quite a unique experience to have . . to . . watch how these people interact with one another . . I feel like this is the way that Unicole Unicron . . could be describing . . from the video . . [3 @ 12:47 - 13:26]

[1]
Days Of My Life... And Love (Story 48)
By Isabel Paige
https://www.youtube.com/watch?v=lGifGj_FPps


[2]
A Weekend in my Life in Hawaii ~ we went camping ~
By Hitomi Mochizuki
https://www.youtube.com/watch?v=UsRavIE5NlM

[3]
Cam Church - Ultimate Reality and Personal Reality
By UNICULT
https://www.youtube.com/watch?v=bOyCwQqZWeY



[Written @ 0:15]

Notes @ Newly Created Words

wolrd
[Written @ October 24, 2020 @ 0:14]
[I accidentally typed the word . . "wolrd" . . instead of the word . . "world" . . I'm sorry . . I'm not sure what this word . . "wolrd" . . should . . mean . . at this time . ]


[Written @ 0:03]

Notes @ Quotes
[Quote 1]

"
When we think of . . a friend group . . who really gets along . . Who really supports . . each other . . who . . is . . really . . in harmony . . with one another . . We can feel . . the beautiful . . vibrations . . that are coming off of that . . social endeavor . . 

We can feel the beautiful energy . . that is radiating out . . of each person . . as they . . feel . . supported . . 

And when we get an entire planet . . to feel that energy . . To . . support . one . another . 

And to vibrate on . . the highest possible reality . . That . . they . . can . . operate . on . 

Then . . we . . create . . a . . world . . that . . is . . not . . only . . fun . . to . . live . . in . . 

But . . we create . . a world . . that is optimal . 
"
-- Unicole Unicron
[1 @ 12:47 - 13:26]





[1]
Cam Church - Ultimate Reality and Personal Reality
By UNICULT
https://www.youtube.com/watch?v=bOyCwQqZWeY


