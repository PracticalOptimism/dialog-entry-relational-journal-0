

# Day Journal Entry - October 31, 2020

[Written @ 23:56]

Notes @ Newly Discovered YouTube Channels


Papa Walsh
https://www.youtube.com/user/StreetCombatSystems1/videos
[Discovered by: YouTube home page showed "Food Vlog: I Made Steak Burritos For All My Kids" By Papa Walsh https://www.youtube.com/watch?v=nVL-ivzQg_A]






[Written @ 0:02]




[Written @ 0:00]

##### Live Stream Checklist
[copy-pasted from Day Journal Entry for October 30, 2020]

Greetings

* [] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [] Practice a breathing exercise for 5 - 15 minutes

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Blue Lotus Feet (Asherah) By Charanpal - Topic https://www.youtube.com/watch?v=p5mCSPeY6tE&list=OLAK5uy_nDVbZAbLowdfy69nsnDhGj0tgBoGTa8jg
  - Wah Yantee By Jai-Jagdeesh - Topic https://www.youtube.com/watch?v=njTJRGwaYIY&list=OLAK5uy_l5gmdP3hvHXWLnp8d0dKzvDGjfJVDUXgw&ab_channel=Jai-Jagdeesh-Topic
  - CLAUDIO - A bit of support for those still in lockdown By Rachel Claudio https://www.youtube.com/watch?v=NsdM4HBXLnQ
  - Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - 

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream



### Things that are planned to be completed
[Copy-pasted from October 30, 2020 Day Journal Entry]

Use the mobile-first design strategy:

- [] Update the your account page
  - [] 
  - [] add a account description section
    - [] account id
    - [] account name
    - [] account username
    - [] account description
    - [] physical location address (country, city/town/province)
    - [] account contact list (email address, website url list, social media url list, physical mail address list, other items)
    - [] account business product listing
    - [] account currency exchange currency listing
    - [] account employer positions listing
    - [] 
  - [] use the name "account statistics" to contain "account balance history" and "transaction amount history" (received and sent transaction amount)
  - [] add a "find shops to buy products or services" section
    - [] add text: find shops that accept ecoin such as "individuals", "online commerce stores", "restaurants", "apartments", "house salesplaces", "gymnasiums", "sports recreation facilities" and more (useful keywords)
  - [] add a "find employers to work with" section
    - [] add text: find employers such as "individuals", "schools", "technology companies", "hospitals", "government agencies", "non-profit organizations", "local businesses" and more (useful keywords)
  - [] add a "find currency exchanges to trade with" section
    - [] add text: find digital currency exchanges to trade Ecoin for fiat currencies (ie. U.S. Dollar, Euro, Yuan, and more) or alternative digital currencies
  - [] add a "find ecoin alternatives" section
    - [] add text: find alternative digital currencies to send and receive money with
  - [] 


- [] Update the network status page
  - [] add a "digital currency statistics" section
  - [] add a "account statistics" section
  - [] add a "transaction statistics" section
  - [] add a globe activity flight map section





