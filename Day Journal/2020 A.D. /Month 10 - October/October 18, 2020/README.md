

# Day Journal Entry - October 18, 2020

[Written @ 22:06]

as opposed to . . 

in complementary . . relationship to . . 

in complement to . . 

as complementary to . . 

as directed in by . . 

as composed in relationship with . . 

as reflected by . . 

in reflection to . . 



[Written @ 16:51]

Notes @ Newly Learned Words

Garish
[1 @ 33:27]



[Written @ 16:47]

Notes @ Quotes

"
Cynicism is important because it allows us to see the corruption that's happening. And it's an essential component of raising our vibration into joy. Because if we never realize that something was wrong . . if we never looked around . . and thought about the world . . we never would look up . . 

And what is hopefulness ? . . Hopefulness . . is merely . . that activity . . of looking around . . and then looking up . . Up . . toward the light . . up toward . . hopefulness . .
"
-- Unicole Unicron
[1 @ 28:49 - 29:20]


[Written @ 16:34]

Why is hope important ?
[1 @ 24:23]


- 

[Written @ 16:26]

cynicam
[Written @ October 18, 2020 @ 16:26]
[I accidentally typed the word . . "cynicam" . . instead of . . "cynical" . . I'm sorry . . I'm not sure . . what this word . . "cynicam" . . should mean . . at this time]

[Written @ 16:23]

Why is hope important ?
[1 @ 24:23]


Notes @ Quotes

"
And something that is really interesting about Earth . . is that . . most of us . . and most of Earth . . is rooted in cynicism . . 

And we actually don't know . . how to find . . solutions . . in this cynical . . energy . . most people . . you can even just look at the political situation . . right now . . most people . . they only want to attack . . 

They want to say . . they're coming at the situation . . from a fear-based place . . and they're saying . . well . . this person is bad . . so . . vote for the other person . . and . . it's all coming from a negative energy . . of cynicism . . 

Well . . that's gonna be worse . . so do this . . there is no . . hope . . There's no . . hopefulness . . Where's the hope ? 
"
-- Unicole Unicron
[1 @ 23:33 - 24:14]




[Written @ 16:09]

Notes @ Quotes

"
What we have to understand about joy . . is that it is an . . infinite . . ascension . . Joy . . Joy . . can only continue . . there is no limitation . . on the level of health . . and joy . . that you can experience . . and that you can have .
"
-- Unicole Unicron
[1 @ 15:39 - 15:53]


[1]
CAM CHURCH - Cynicism into Hopefulness
By Unicole Unicron
https://www.youtube.com/watch?v=z3RfQp4PPaE




[Written @ 14:29]

Notes @ Newly Created Words

majy
[Written @ October 18, 2020 @ 14:29]
[I accidentally typed the word "majy" . . instead of . . "maybe" . . I'm sorry . . I'm not sure . . what this word . . "majy" . . should mean . . at . . this . . time]


[Written @ 14:23]

Okay . . today . . we're gonna be talking about . . cynicism . . into . . hopefulness . . and . . 

If you're taking notes . . we're gonna start incorporating . . taking . . notes . . a little bit more . . with CAM CHURCH . . so . . uhm . . If you're taking notes . . go ahead and just . . maybe right now . . 

Take some time to write down your thoughts on cynacism . . Maybe . . you agree that . . uh . . cynicism is something that is required . . for art . . maybe you feel like . . cynicism . . is just the natural way . . to look . . at the world . And . . really . . just . . dig down . . into your own . . ideas . . about cynicism . . and ask yourself . . what is it . . that I feel . . about . . cynicism . . 

Where does cynicism . . serve me ? . . and maybe . . just . . take a second . . to jot those ideas . . down . .

[1 @ 14:12 - 14:50]

. . . 

Personal Thoughts:

Personal Thoughts on the topic of Cynicism:

uh . . 

Cynicism is a school of thought of ancient Greek philosophy as practiced by the Cynics. For the Cynics, the purpose of life is to live in virtue, in agreement with nature. As reasoning creatures, people can gain happiness by rigorous training and by living in a way which is natural for themselves, rejecting all conventional desires for wealth, power, and fame.

[2]

. . . 

Where does cynicism serve me ?

I'm still learning about the topic of cynicism . . uh . . my . . intuition . . relates . . to the topic . . of cynicism . . relating to . . not being . . hopeful . . uh . . but it seems like from the . . article at 2 . . cynicism . . has related . . uh . . in historical . . forms . . cynicism . . seems . . to be related to . . people . . living . . naturally . . and rejecting all conventional desires for wealth, power and fame . . uh . . this is a new . . definition . . as it relates to cynicism . . for me . . uh . . well . . uh . . I'm not sure . . well . . I think I'll talk about the topic of not being hopeful . . uh . . 

Not being hopeful . . uh . . well . . uh . . I'm not sure about this topic . . uh . . in my own personal experience . . I try to be hopeful . . uh . . Well . . uh . . being hopeful . . uh . . seems . . to help me . . uh . . uh . . well . . uh . . hmm . . I . . feel . . good . . uh . . when I am hopeful . . or when I look forward to something . . uh . . for example . . I feel . . uh . . good . . or . . uh . . happy . . when I have something to strive forward to . . like . . completing a project . . uh . . uh . . well . . I like to hope . . that . . I'll complete a project . . uh . . as uh . . opposed to . . to . . uh [in complement to . . in complementary relationship to . . ] . . being . . not so hopeful . . of the project's completion . . I like to be hopeful . . uh . . well . . uh . . maybe then . . uh . . how has . . not being hopeful . . served me ? . . hmm . . 

well . . I like . . to uh . . uh . . give up on . . certain things that I do . . uh . . for example . . uh . . uh . . well . . uh . . hmm . . I am feel discouraged because of the difficulty of solving a problem . . and so then I lose hope . . or . . I feel like . . it's better to . . lose hope . . and not solve the problem . . uh . . because . . uh . . uh . . well . . I just don't . . uh . . hmm . . I just . . don't . . uh . . hmm . . well . . uh . . I give up on the problem . . but . . giving up . . uh . . it's really . . uh . . well . . if the problem is interesting to me . . uh . . which uh . . for example . . uh . . if it's a problem with . . software engineering . . and running into a program that's not working . . how I expect . . uh . . well . . uh . . I give up . . but . . it's really a temporary thing . . uh . . to give up . . uh . . does . . not feeling hopeful . . uh . . or does  . . being . . not . . hopeful . . serve me . . in this area ? . . uh . . well . . I guess so . . uh . . because . . uh . . a lot of the time . . after I . . uh . . don't uh . . hmm . . uh . . well . . it's not really uh . . well . . uh . . I uh . . hmm . . I think . . uh . . not being hopeful . . uh . . in solving a problem in a certain way . . is very useful . . and so . . then .  I can think about solving the problem in another way . . uh . . and so . . I lose hope . . in terms of . . solving the problem . . in one way . . uh . . and so . . I start to build hope . . in solving a problem . . in another way . . uh . . and so . . yea . . I think . . losing . . hope . . or not being hopeful . . uh . . about things . . is quite useful . . uh . . uh . . uh . . yea . . well . . I lose hope in solving problems a certain way . . uh . . but I gain hope . . in solving a problem in another way . . uh . . something like that . . 

. . . 

Is cynicism something that is required ? 

hmm . . uh . . well . . when I think about things . . uh . . I'm not sure . . uh . . hmm . . let me think abou this some more . . hmm . . cynicism . . as the topic . . relates to . . not being hopeful about the future . . or . . not being hopeful about the properties . . or benefits . . of a certain topic . . uh . . 

hmm . . well . . I'm uh . . uh . . I'm not sure . . when I think uh . . in relation to uh . . what other people . . could think about this topic . . I feel that . . uh . . other people . . uh . . or uh . . people in general . . uh . . the people uh . . of the public that . . uh . . I'm thinking about today . . uh . . something like that . . uh . . well . . I'm not sure . . I think mostly that uh . . it seems that . . uh . . not being hopeful about the benefit of a certain thing uh . . seems uh . . like a uh . . natural thing . . that happens . . uh . . it's uh . . like . . critical thinking related . . uh . . where . . uh . . maybe being critical . . uh . . or uh . . being . . uh . . combatative . . uh . . or being . . oppositionary . . uh . . or being . . suggestive uh towards the trustability of the topic of interest . . uh . . well . . I'm really not sure . . these uh . . uh . . well . . uh . . there's something about . . uh . . being . . uh . . experienced . . as uh . . an experienced person . . maybe uh . . it could be the natural way of being a person that is uh . . trying to protect their self interest . . uh . . uh . . and so being uh . . defensive . . or uh . . characteristic . . of uh . . a combatant . . uh . . uh . . uh . . combating uh . . maybe uh . . in the psychological territories . . where one is uh . . defending their own experiences or defending their own uh . . memories . . or uh . . thoughts or ideas of one self . . uh . . I suppose it could be uh . . uh . . based in fear . . uh . . of being offended . . uh . . or having . . uh . . ones thoughts . . uh . . being . . uh . . invalidated . . or . . uh . . opposed . . uh . . I'm not really sure if that is the way I ought to say things . . or if that is the way . . uh . . things are maybe uh . . realized in he way that uh . . people relate to uh . . criticism . . uh . . . or uh . . protecting their thoughts uh . . uh . . the thoughts uh . . relating uh . . maybe to a personal individual cultural thought perception that ones requires to uh . . experience in the world as a personal individual or something like that . . uh . . I'm not really sure . . uh . . well uh . . uh . . uh . . uh . . well . . uh . . hmm . . is cynicism something that is required ? . . uh . . well . . uh . . it seems like . . uh . . cynicism . . as the topic relates to uh . . being not so hopeful about the benefit of a certain topic . . uh . . or not being hopeful about the purpose of a certain thing being in the way it is . . uh . . uh . . or criticizing something . . or . . uh . . it seems like uh . . I'm not really sure . . it seems like maybe uh . . it is something that people do uh . . uh . . on their own part . . uh . . by choice . . or . . uh . . by the way uh . . we think or something like that . . uh . . uh . . maybe a discriminative faculty uh . . to discriminate between the things that we like . . is uh . . a natural part of a person as we understand them today . . but that is uh . . my understanding . . at this time . . and I'm still learning more about this topic . . and not really sure . . uh . . is cynicism required ? . . I'm not sure . . maybe there are people that aren't uh . . critical uh . . in this uh . . cynical-related way . . uh . . uh . . maybe a person like that . . uh . . uh experiences uh . . life . . uh . . in a way relating to . . uh . . hmm . . I'm not really sure . . I need to be more imaginative uh . . in being able to describe such a person . . uh . . hmm . . 

a person that is uh . . not critical . . uh . . or uh . . maybe . . they see the world as uh . . maybe uh . . something like . . uh . . uh . . it is okay . . uh . . it is real . . it is valid . . it works . . it is real . . it is experience . . hmm . . I'm not . . really sure . . uh . . what that means uh . . I think a lot of people uh . . are like this uh . . hmm . . 

. . . 

Is cynicism the natural way to look at the world ? 

I'm not sure . . uh . . personally . . uh . . as an individual . uh . . I try to uh . . think on my own . . uh . . which . . uh . . in my personal thinking patterns . . uh . . when I think about the world . . uh . . I can see thoughts that arise . . that are . . uh . . combating . . uh . . the thinking uh . . styles . . uh . . that I notice about other people . . uh . . well . . for example . . uh . . if I think . . uh . . someone isn't taking this or that thought into account . . uh . . then . . uh . . I'll . . think about uh . . their thinking . . uh . . uh . . being . . incomplete or something like that . . uh . . and so . . if there is this or that knowledge that isn't taken into account . . maybe uh . . I start to question the body of knowledge that the person has in general . . as an individual . . uh . . but this way of thinking uh . . well . . I do have more thoughts to share on this style of thinking . . to uh . . think that maybe a part of a person's thoughts are incomplete . . uh . . uh . . because uh . . one perceives that this or that knowledge is uh . . missing from their faculties . . uh . . uh . . hmm . . I really try to avoid this style of thinking . . uh . . it seems more and more people are uh . . uh . . well . . each of us are really quite uh . . amazing in our individual regards . . uh . . and uh . . the ways in which we are uh . . observed uh . . uh . . in our uh . . own knowledge and amazing experience . . uh . . uh . . uh . . well . . I'm not really sure . . uh . . I uh . . would prefer to think more highly of people in regard to their personal interests and the knowledge that . . uh . . my present personhood would observe as "missing" or . . "not apparent" . . uh . . or "lacking" . . from another person . . really is quite uh . . uh . . uh . . uh . . it's uh . . uh . . . . uh . . it's uh . . maybe a psychological room . . that I don't need to be in . . uh . . it's uh . . uh . . another . . uh . . psychological room to be in . . is uh . . the room of wonder and excitement for uh . . what someone can share . . uh . . something like that . . uh . . or uh . . knowing that the person is capable . . of learning the same things you are knowledgable of . . uh . . uh . . and if the person is uh . . appearing to uh . . seem to not know or not relate to the topic of interest in terms of one perceives uh . . uh . . in other people . . uh . . uh . . well . . it's uh . . uh . . uh . . uh . . I'm not sure . . uh . . uh . . well . . uh . . the thought I had to share had something to do with looking uh . . highly towards people . . uh . . and any gaps in knowledge are uh . . something that . . uh . . uh . . uh . . uh . . one can uh . . uh . . well . . uh . . uh . . knowledge uh . . uh . . uh . . uh . . in terms of knowledge . . uh . . uh . . everyone is capable of learning . . uh . . and so . . uh . . it's uh . . maybe not a mystery . . uh . . uh . . uh . . it's maybe . . not a mystery . . uh . . uh . . well . . uh . . uh . . and uh . . uh . . right . . something like that . . 

Is cynicism the natural way to look at the world ? . . 

uh . . I think . . cynicism . . is a uh . . useful thing . . I guess . . I'm not sure . . I still am learning about this topic . . 

natural way of looking at the world ? . . uh . . I guess . . uh . . I see people who are critical of certain things . . uh . . well . . I think people are uh . . a part of nature . . uh . . well . . uh . . if uh . . people are a part of uh . . nature . . uh . . which . . uh . . seems to be the case . . uh . . from my observation . . uh . . or uh . . from my point of view . . uh . . nature being uh . . something . . uh . . well . . uh . . creative . . uh . . like a creative product of uh . . creativity . . uh . . or the creativity of creation . . or the creativity of consciousness . . or something like that . . uh . . well . . uh . . in the natural ways of creativity . . uh . . it seems uh . . that uh . . . being cynical . . uh . . or being . . uh . . critical . . uh . . or . . being . . uh . . uh . . not hopeful . . about certain things . . uh . . not being hopeful . . about certain things . . uh . . well . . uh . . not being hopeful about certain things . . seems like . . uh . . maybe . . if uh . . uh . . uh . . birds uh . . are uh . . not being hopeful about the weather in a certain place . . uh . . or insects are not being hopeful about the . . human uh population of uh . . cement . . uh . . and roadways . . uh . . around their uh . . habitation zones . . uh . . where the insects . . uh . . live . . uh . . uh . . and so then a movement uh . . towards other habitation zones . . uh . . is more towards uh . . the hope for those insects or birds . . uh . . well . . uh . . maybe humans uh . . are also having these same thoughts of . . uh . . not being hopeful about certain things . . and then moving in their own ways . . uh . . towards uh . . the things uh . . that each uh . . individual . . uh or group or uh . . however the person clears their thoughts uh . . to come to the topic of uh . . how they uh . . would uh . . come to be reasonable about the things that are uh . . more uh towards their uh . measure of uh . . maybe being hopeful to those uh . . things that are available . . uh . . 

well . . uh . . in the way that I've written above it seems to me now that maybe each individual is able to be uh . . critical about those things uh . . that uh . . are able to be uh . . criticized by them . . uh . . and so uh . . well . . maybe then . . each of us would like to have a path or a way of life that is towards those things that which we come to see as not being uh . . something that needs to be criticized uh . . and so some of those things can be . . uh .  having a house or having a place to stay . . and having drinking water . . uh . . uh . . well those are also dependent on the person  . . uh but . . someone who is uh . . not uh . . critical of uh . . nuclear energy as a way to energize the utilities uh or the tools of the people . . uh . . well then maybe those are uh . . things that they are willing to consider . . uh . . uh . . 

well . . uh . . then it seems uh . . being critical . . uh . . or being not hopeful about certain topics is uh . . maybe natural uh . . in those environments where those natures are uh . . practiced . . uh . . or maybe in other words . . if there is a planet . . where uh . . the people are behaving or uh . . thinking . . differently uh . . about the way of the world . . uh . . then uh . . it doesn't necessarily uh need to be the case that uh . . criticism in this way of thinking . . uh . . of uh . . having a uh . . knowledge related uh . . maybe uh . . preference related . . uh . . uh . . perception uh . . in terms of the ability for uh . . the topic uh . . uh . . of interest to satisify uh . . certain thoughts uh . . or certain things of interest . . uh . . well . . uh . . if the peopole uh . . on the planet aren't uh . . "naturally" doing this . . uh . . on a daily basis . . uh . . or . . uh . . regularly . . or uh . . if the plants and insects and birds of other types are also not doing this way of thinking . . then maybe it is possible that in the nature of this planet . . that it wouldn't be a natural thing . . uh . . for that to be . . uh observed uh . . or something like that . . uh . . for cynicism . . uh . . in the regard of not being hopeful about something . . to be observed uh . . or something like that . . uh . . 

Is cynicism the natural way to look at ths world?

uh . . uh . . in thinking about this topic . . uh . . uh . . so far uh . . I think uh . . maybe it depends uh . . on the nature uh of the environment . . 

uh . . on . . the Earth environment . . uh . . from my opinion . . this appears to be the case that . . it is natural . . to be . . uh . . not hopeful about certain things . . uh . . the birds . . and insects behave to be not hopeful about certain weather or climate conditions . . uh . . and humans . . are also . . thoughtful in uh . . changing their thoughts to align with the things uh . . the things that are uh . . maybe uh . . maybe if you are cynical . . or not hopeful about a lot of things . . uh . . uh . . uh . . measuring life could be more difficult for a person . . uh . . like not being hopeful about being able to come to reason about the things that one should do . . tomorrow . . or the next day . . or any day after today . . uh . . if one uh . . is not hopeful in their ability to do uh . . the tasks that are available for them to do . . uh . . uh . . the the tasks that are there for them to consider choosing . . or creating . . or uh . . manifesting . . uh . . or . . uh . . being in tune with . . or being in alignment with . . uh . . I'm not sure . . uh . . in this way . . uh . . could an organism . . that is uh . . uh . . cynical in this degree or in this regard . . could they survive ? . . uh . . to be an organism ? . . uh . . uh . . uh . . uh . . that is uh . . uh . . something uh . . I'm not sure . . uh . . uh . . in thinking about things uh . . uh . . a lot of things are possible uh . . uh . . uh . . 

right . . uh . . uh . . those are uh . . my personal thoughts on this topic at this time . . 

. . . 


Notes @ Newly Created Words

igmaginative
[Written @ October 18, 2020 @ 15:22]
[I accidentally typed the word "igm" . . while trying to type the word "imaginative" . . a possible continuation of the word "igm" . . is the word "igmaginative" . . "igmaginative" . . is quite a cool word . . "ig" . . is really quite cool . . "ignite" . . "iguana" . . the . . "ig" . . sound . . makes . . "igmaginative" . . quite exciting . . quite igniting . . quite . . exquisitive . . engaging . . ]


peoeopl
[Written @ October 18, 2020 @ 15:30]
[I accidentally typed the word "peoeopl" . . instead of typing the word "people" . . I'm sorry . . I'm not sure what this word . . "peoeopl" . . should mean . . at this time . . ]


excitef
[Written @ October 18, 2020 @ 15:37]
[I accidentally typed the word "excitef" . . instead of typing the word "excitement" . . I'm sorry . . I'm not sure what this word . . "excitef" . . should mean . . at this time]





[1]
CAM CHURCH - Cynicism into Hopefulness
By Unicole Unicron
https://www.youtube.com/watch?v=z3RfQp4PPaE


[2]
Cynicism (philosophy)
https://en.wikipedia.org/wiki/Cynicism_(philosophy)




