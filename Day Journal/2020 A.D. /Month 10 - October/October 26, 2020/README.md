

# Day Journal Entry - October 26, 2020 

[Written @ 1:21 - 1:33]

exper
[Written @ October 26, 2020 @ 1:22]
[I accidentally typed the word . . "exper" . . instead of . . typing the word . . "expression" . . I'm sorry . . I'm not sure . . what this word . . "exper" . . should mean . . at this time . . time but . . I am inspired to think about the meaning of this word "exper" . . "exper" reminds me of a word like "raspberry" . . "exper" . . the . . sound of the "s" . . the . . "r" . . and the . . "p" . . involved in the pronunciation of the word . . "exper" . . "raspberry" . . that's a beautiful sound to listen to . . and the imagination being involved with the taste of a raspberry which is so sueet and juicy . . which is so suieet and juicy . . which is so swuiet and juicy . . juiceberries are so raspy! . . raspbian pi juicy]


Phorographs
[Written @ October 26, 2020 @ 1:22]
[I accidentally typed the word . . "Phoro" . . instead of . . "Photographs" . . a possible continuation of this typographical error could be the word . . "Phorographs" . . wow . . what a beautiful word . . "Phoro" . . that is a unique expression to me at this time . . ]


[Written @ 1:13]

Notes @ Probable Names

Elon James
[Inspired by 1 @ 30:45 with the subtitles turned on . . reads the message "thank you elon james" . . James Burk . . and Elon Musk . . individually are quite beautiful names . . and together . . James Burk . . and Elon Musk . . Elon James . . that's a really beautiful name . . "Elon James"]


Elon James Carrie
[Inspired by 1 @ 30:47 . . shortly after discovering the name . . "Elon James" . . the text . . on the video . . [1] . . at . . the timestamp . . 30:47 . . read the message "thank you elon james yeah thanks carrie um another question" . . wow . . Carrie is also a beautiful name . . from my perspective . . at this time . . in my opinion at this time . . carrie . . speaking of which . . reminds me of the word . . "raspberry" . . in how juicy the word is . . uh . . which is kind of a strange discovery because as I'm writing this message . . I had previously just discovered a new word . . written on this page . . for the day journal entry for October 26, 2020 . . and that word . . also reminded me of a word . . like . . "raspberry" . . well . . "Elon James Carrie" . . reminds me of a ferris wheel . . the image that I get in my mind . . is a ferris wheel . . I think the name . . "Carrie" . . introduced this image . . because . . the name . . "Elon James" . . by itself . . didn't give me a flashing image in my mind . . of a "Ferris Wheel" . . and yet . . the name . . "Carrie" . . by itself . . gives me the impression of a . . "raspberry" . . well . . uh . . these are maybe mental hallucinations . . of some kind or maybe intuitive gestures to the topic of how names can relate to physical pronunciation images . . and have a sort of geometric suggestiveness that can be perceived across people . . and so maybe if you are also seeing the possibility of the relation to a ferris wheel . . then that may suppose there are strange relations to how names and sounds maybe relate to pictorial photographs . . in unexpected or unconsidered ways . . or something like that . . or something like that . . ]


"thank you elon james yeah thanks carrie um another question"
![Elon James Carrie](./Photographs/elon-james-carrie.png)

"thank you elon james yeah thanks carrie um another question" (full screen screen-capture)
![Elon James Carrie](./Photographs/elon-james-carrie-full-screen.png)



Notes @ Newly Discovered Words That Seem Related But Are Maybe Not

- Parks [In reference to places to relax and enjoy your day . for example "I enjoy walking at parks"]
- Parking [In reference to parking cars . for example "The parking lot is empty"]
- Park [In reference to driving a car into an empty or available parking space "I am going to park my car over there"]

[Discovered at 1 @ 31:21 - 31:27]


In memory and celebration of the discovery of these two words above . . "Boring" . . and "Boring" . . are also words that seem related but are maybe not . . 

- Boring [In reference to not having fun]
- Boring [In reference to digging a tunnel]

In memory of remembering a word language video diagram that expresses this quality of the human english language diagram matrix: [2]

[2]
English is dumb
By Julie Nulke
https://www.youtube.com/watch?v=Rus_3VjMf0s


Notes @ Quotes

"
[James Burk]: The Boring Company . . Is that just kind of uh . . an outfit . . to build tunneling machines . . that can work . . on Mars ? 

[Elon Musk]: Uh . . No . . heh heh . . The Boring Company . . actually started as kind of a joke . . Uhm . . and I . . for . . a lot of times . . people . . would ask me . . what . . what do I think . . uh . . the opportunities are . . out there . . and . . for . . I don't 5 years or more . . I kept saying . . 'Can someone please start a tunnelling . . company ?' Uh . . cause I think tunnels have a lot of opportunity . . for alleviating traffic in cities . . and . . just . . improving . . quality . . of . . life . . overall . . 

I mean . . there's a lot of streets that you could turn into parks . . Uhm . . 

C-Certainly . . you wouldn't need parking . . you could just . . park cars . . underground . . 
"
[1 @ 30:48 - 31:27]




[1]
Elon Musk - 2020 Mars Society Virtual Convention
By The Mars Society
https://www.youtube.com/watch?v=y5Aw6WG4Dww


[Written @ 0:07]

### Planned Tasks For Today
[copy-pasted from Day Journal Entry for October 25, 2020]

- [] Update the transaction dialog design to match expectations
  - [] update the send payment and cancel button to always show at the bottom of the dialog
  - 
- [] Update the width of the universal basic income notification animation to not overflow
- [] Add a search icon before the search account text field in the header
- [] Publish the development website changes
- [] Add close button to the digital currency universal basic income notification snackbar item



### Planned Tasks For Today or Tomorrow
[copy-pasted from Day Journal Entry for October 25, 2020]


// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider


// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list


// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)


// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts

[Written @ 16:02]

##### Live Stream Checklist

Greetings

* [] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [] Sit or stand in a comfortable position
* [] Practice a breathing exercise for 5 - 15 minutes

Music

* [] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Days Of My Life - Caught In A Snow Storm (Story 53) By Isabel Paige https://www.youtube.com/watch?v=XZF7F3kndi0&ab_channel=IsabelPaige


Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - 

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream



[Written @ 0:05]

Approximate amount of time spent on the live stream yesterday . . for October 25, 2020 . . for Ecoin #348

5 hours . . 8 minutes and 18 seconds . . 

05:08:18.118

#1
05:08:18.118
Pause



