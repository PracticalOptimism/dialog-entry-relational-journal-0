
# Day Journal Entry - October 2, 2020

[Written @ 1:29]



* [] Plan the tasks to complete

  - fix the issues on the command line interface
  - update account
    - update account profile picture
    - update acocunt name
    - update account username
      1. add to queue map of users requested for that username
      2. Check queue map to see if user is the first to attend the operation
      3. Delete the entry map from existing username to accountId
      4. Update the entry map for the new username to accountId
      5. Change the username of the account [Firebase validate other operations]
      6. Delete from queue map of users registered for that username
  - get account
    - get account by qr code id
  - create account
    - create a qr code id
    - add to account-qr-code-to-account-id-map
    - add to create-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - delete account
    - add to delete-account-variable-tree
      - millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
  - update transaction
    - update transaction amount
    - update transaction text memorandum
  - apply universal basic income
    - create a transaction for all accounts to add digital currency amount
  - initialize-service-account (sign in)
    - with google
    - with email and password
  - uninitialize-service-account (sign out)
  - gitlab continuous integration / continuous deployment (ci / cd)
    - publish ecoin development website
    - publish ecoin daemon to development server
  - readme.md
    - demo usecase preview: update transaction settings
    - demo usecase preview: view transaction information
  - html component
    - pay with digital currency button works on websites
  - usecase: community participant list
    - create community participant list for digital currency
    - create community participant list for digital currency exchange
      - digital currency exchanges that support ecoin transactions
    - create community participant list for online shopping mall
      - online shopping malls that support ecoin transactions
    - create community participant list for physical-location shops and stores
      - physical-location shopping places and stores like restaurants, shopping malls, shopping centers, grocery stores, food marts, small business store fronts, farmer's markets, etc.
  - update the styles of the desktop version of the website
    - account section
      - show the account section on the left
      - desktop version width should be not full width. [follow how it's done on the settings and about page]
      - desktop header tabs (v-tabs) should be condensed and not wide screen.
    - account statistics
      - show the account statistics on the right (allow vertical scroll)
    - community participants: show a list using vuetify table
  - settings page
    - 
  - add a `qr code` button
    - [header] Your Account QR Code
      - [subheader] Account QR Code for `account name`
    - [camera icon] Capture Account QR Code
    - find a digital currency account by its qr code by taking a photograph of the account qr code
  - network status page
    - number of ecoin distributed (timeline graph)
    - number of created accounts (timeline graph)
    - number of created transactions (timeline graph)
    - number of created community participants (timeline graph)
    - newly distributed ecoin (live updating)
    - newly created accounts list (live updating)
    - newly created transactions list (live updating)
    - newly created community participants (timeline graph)
    - total number of ecoin created (running sum of overall currency amount)
    - total number of accounts created (running sum of overall accounts amount)
    - total number of transactions created (running sum of overall transactions amount)
    - total number of community participants created (running sum of overall community participants)
  - about page
    - update the "Why does Ecoin exist?" answer 
      - "[A] A short answer like this is meant to encure your curiosity and sponsor your research project to learn more about these wounderous individuals. ;)"
      - enumerate the long answer clause




