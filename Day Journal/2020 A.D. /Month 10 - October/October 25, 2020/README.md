
# Day Journal Entry - October 25, 2020

[Written @ 23:56]

Notes @ Newly Created Words

begineer
[Written @ October 25, 2020 @ 23:56]
[I accidentally typed the word . . "begineer" . . instead of typing the word . . "beginner" . . I'm sorry . . I'm not sure what this word . . "begineer" . . should mean at this time . . although . . this word "begineer" . . does remind me of . . the word . . "engineer" . . and so . . "a beginner" . . who is also an "engineer" ? . . that is an interesting phrasing . . hmm . . I'm not sure . . I suppose that's a possible definition for this newly created word . . ]


[Written @ 22:43]

Notes @ Newly Discovered Similarly Named People

Andrew Ng
https://www.andrewng.org/

Andrew Ng
https://chess.stackexchange.com/users/1382/andrew-ng
https://chess.stackexchange.com/questions/2506/what-is-the-average-length-of-a-game-of-chess



[Written @ 21:54]

Notes @ Newly Discovered YouTube Channel

365 Days of Meditation
https://www.youtube.com/c/365daysofmeditation/videos


[Written @ 21:34]


[Written @ 19:45]

Notes @ Newly Discovered YouTube Channels

Shira Notes
https://www.youtube.com/channel/UCdOAZDYSOBaJ_VgiXKxRbaA/videos
[Discovered by: https://www.youtube.com/watch?v=e-ODod7lizQ found from recommended video after https://www.youtube.com/watch?v=x-9J0Lzq6Iw]


mindfulnesstoolstv
https://www.youtube.com/channel/UCMqf8t3oIsej_FRjPb3-AnQ/videos
[Discovered by: https://www.youtube.com/watch?v=x-9J0Lzq6Iw]


[Written @ 18:50]

### Planned Tasks For Today

- [] Update the transaction dialog design to match expectations
  - [] update the send payment and cancel button to always show at the bottom of the dialog
  - 
- [] Update the width of the universal basic income notification animation to not overflow
- [] Add a search icon before the search account text field in the header
- [] Publish the development website changes
- [] Add close button to the digital currency universal basic income notification snackbar item



[Written @ 18:49]

### Planned Tasks For Today or Tomorrow
[copy-pasted from Day Journal Entry for October 23, 2020]


// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider


// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list


// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)


// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts

[Written @ 16:02]

##### Live Stream Checklist

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes
  - Gratitude Meditation ❤️️ 21 Day Transformation ❤️️ 432 HZ By Live The Life You Love https://www.youtube.com/watch?v=xfD4HaBBc0I


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - Days Of My Life - Caught In A Snow Storm (Story 53) By Isabel Paige https://www.youtube.com/watch?v=XZF7F3kndi0&ab_channel=IsabelPaige


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - 

Salutations

* [x] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [x] Talk about the possibilities for the next live stream



[Written @ 0:51]

Approximate amount of time on the live stream for . . October 24, 2020 . . Ecoin #347

01:01:51.447

#1
01:01:51.447
Pause


