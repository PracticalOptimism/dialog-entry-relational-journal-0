

# Day Journal Entry - October 9, 2020

[Written @ 21:30]

Notes @ Things that I Learned Today

I was taking a poop in the toilet . . earlier today . . and . . in the . . last few . . tries . . to . . release . . what . . was left . . inside . . of my body . . the poop that was left inside of my body . . that I could . . still feel . . in an . . intuitive sense . . but . . it . . was . . difficult . . to get it . . out . . 

uh . . I realized . . that . . raising . . my head . . and . . opening my mouth . . made the process . . of releasing . . the poop . . from my butt . . a lot . . easier . . and so . . for example . . I . . raised my head . . or . . tilted my head . . to face the ceiling . . in the bathroom . . and . . sitting up straight . . with my head . . tilted . . upwards towards the ceiling . . and . . uh . . also . . with . . my mouth . . wide open . . 

I found it a lot easier . . to release the poop that I thought was going to be a struggle to release . . uh . . the poop that I thought was going to . . take a long time to release . . from my butt . . . easily . . came out . . with . . a little more . . pressure . . uh . . with . . uh . . just a little pressure . . and it didn't feel like I was struggling . . it was . . a lot . . smoother . . or a lot . . cleaner . . or easier . . process . . to release these last . . strings . . of poop . . which were all snake-patterned poop strings . . from my body . . 

uh . . well . . uh . . I'm not really sure how this works . . uh . . I started hypothesizing . . while . . sitting on the toilet . . that maybe . . this has something to do . . with my . . nervous system . . the nerves that connect my body . . to my . . brain . . and . . how the nervous tissue . . releases . . or relaxes . . the rectum . . or . . uh . . maybe . . not relaxes the rectum . . but . . uh . . maybe . . relaxes . . the mind . . and so . . the feeling of having to struggle . . to release any more poop is removed . . and the poop is thus able to come out more easily or something like that . . or maybe . . uh  . . well . . maybe . . uh . . well . . the main hypothesis . . that I had . . was . . something to do . . with . . releasing pressure . . uh . . from . . the nerves that connect to your butt . . and so . . for example . . when you sit up straight . . your . . body . . is releasing pressure from the nerves that would otherwise be compressed due to the muscles or spinal column area that press against this nervous tissue . . uh . . and . . uh . . make . . focusing . . on . . work . . uh . . for example . . uh . . working . . or focusing . . on a job . . uh . . if you're not able to focus uh . . that is maybe because you're not sitting up properly . . uh . . something like that . . uh . . -_- . . I need to learn more about this topic . . of nervous tissue . . and the connectivity . . uh . . and how that relates . . to . . uh . . having proper posture helps breathing . . and . . helps focus . . and concentration on tasks . . or something like that . . I need to do more research in this area . . uh . . I'm sorry for my explanation here . . which . . is from an amateur science student . 

uh . . well . . uh . . opening my mouth . . was really quite beneficial . . I tested various positions while sitting on the toilet . . to see how easy it felt to release . . the remaining . . stool . . from my body . . 

uh . . I sat with my head . . angled . . downwards towards the ground . . and so . . uh . . with my face . . directed toward the floor . . in the bathroom . . uh . . it felt . . noticeably . . different from when I had . . my head . . or my face rather . . facing . . directly toward the ceiling . . uh . . my mouth being open . . was also . . a very nice contributor . . uh . . well . . the combination of my mouth being wide open . . uh . . and . . having my face . . directed . . toward the ceiling . . made . . the poop a lot easier to release . . and there wasn't the struggle of feeling like . . I was going to have to really try to push my body release the remaining . . biological history of my . . organic matter content release cycle . . uh . . or . . uh . . my bottom 

In summary:

(1) sit up straight
(2) tilt your head up towards the sky
(3) open your mouth wide

Optional: nod your head up and down . . slowly . . and slowly increase the pace of the nod . . make the nods wide angled . . such that . . you tilt your head from facing the floor . . to . . facing . . the ceiling . . with . . each . . cycle . . of the nodding . . motion . . uh . . well . . you can also play around with variations of the angle . . and how fast you nod your head . . but I noticed that it was easier for me to poop . . when I nodded . . my head . . as well . . as . . opening my mouth . . and tilting my head up towards the sky . . uh . . and . . sitting . . up . . straight . . could have also . . helped as well . . 


Notes @ Newly Created Words

releaseing
[Written @ 21:38]
[typographical error of releasing]


dowards
[Written @ 21:42]
[typographical error of downwards]

ceiv
[Written @ 21:43]
[typographical error of ceiling]

[Written @ 19:14]


Notes that I took today while thinking about the topics relating to:

- using (1) firebase cloud firestore
- using (2) firebase realtime database
- should one service be used over the other ? (1) firebase cloud firestore or (2) firebase realtime database ?
- what are the performance characteristics like for either database provider?
- what does the javascript application programmable interface (api) look like for either database provider?
- 

. . . 

service-type
  - database / algorithms
     - start-migrating-database-store-list

Migrating Data
  - get all the data from one database
  - create all the data in another database

Things to do:
  - Use Firebase Realtime Database to accept user data processing requests
  - Use Firebase Cloud Firestore to store user data


Web Browser
  - create account
    - create account [request to realtime database]
    - listen to account update event [response from cloud firestore]
  - delete account
    - delete account [request to realtime database]
    - listen to account update event [response from cloud firestore]
  - get account
    - get account [from cloud firestore]

Realtime Database
  - create [a variable tree containing user requests for data processing]
  - update [a variable tree containing user requests for data processing]
  - delete [a variable tree containing user requests for data processing]

Cloud Firestore
  - get [all get requests are made to the cloud firestore database]
  - add item event listener [update events are made to the cloud firestore database]
  - remove item event listener [update events are made to the cloud firestore database]

Administrator Daemon
  - create account list
  - update account list
  - delete account list
  - create transaction list
  - update transaction list
  - delete transaction list
  - start-processing-transaction-list

- Update to Cloud Firestore directly
  - Client-side Firebase security rules
    - Learn more about the firestore rules language

- Update to Cloud Firestore by the Administrator Daemon
  - ".read": "auth.uid != null" || "auth.uid == firebaseAuthUid"
  - ".write": "auth.admin == true"

Get List Usecases
  - by PropertyId,
    relationId,
    propertyValue

    by Property Id Relation To Value
      - is employer account
      - is business account

Get List
  - orderByKey
  - orderByChild
  - orderByValue
  - sortedBy
  - startAt
  - limitToFirst
  - limitToLast

- 100 concurrent connections
- Up to 200K / database

Firebase Realtime Database
  -

- sorted By Number of Transactions
  (Business account list)
- sorted By Like-to-Dislike Ratio (Recent in the last month)
  (Business account list, Employer Account list)
- sorted By Number of Employee Positions Available
  (Employer Account list)


. . . 

Updating the firebase security rules
  - I'm not always sure what application features are vulnerable . . 
    - do I need to wait for an exploited case to appear before updating the firebase security rules ? 
    - 
  - having the administrator daemon be the only write-authorized user seems easier
    - by not allowing any writes from users, the administrator daemon can process requests and update the database with the updates that are administrator-approved and so you can have more advanced security rules through using the javascript programming language that aren't necessarily able to be reflected in the current native language for the firebase security language . 
      - the tradeoff seems to relate to the speed of updates to the database
      - by having a simple security rules language, updates and queries can possibly be performed much faster than if you have a programing language like javascript 

. . . 

Unique Features about firebase
  - Scalable
    - Using firebase has the potential to support multiple millions or billions of people from around the world who are simultaneously making requests to the firebase database
  - Affordable
    - The free pricing tier is really an amazing way for a hobbyist like myself to get started and make something without being restricted by payment gateways especially since I don't have a job and have 0 dollars in my bank account to afford any resources. I've had to borrow money from my friend who purchased a pre-paid debit card for me at wal-mart so as to pay for the domain ecoin369.com as well as the expressvpn subscription for 1 month . affordability is important .
  - Customer Friendly Work Environment
    - The firebase sdk is really friendly for people who are working with the api
    - 





Notes @ Newly Created Words

vult
[vulnerable typographical error]

vulner
[vulnerable typographical error]

moeny
[money typographical error]

transaci
[transaction typographical error]

Emoplyer
[Written @ approximately 19:53]
[employer typographical error]

Employter
[Written @ 19:53]
[employer typographical error]

