

# November 6, 2020


[Written @ 22:45]

. . 

I would like to . . maybe have a way . . where you can have a . . hierarchical list . . component . . 

. . 

```vue
<template>
  <div>
    <div v-for="(item, itemIndex) in itemList" :key="itemIndex">
      <span v-if="typeof item !== 'object'">{{ item }} {{ itemIndex }}</span>
      <HierarchicalList v-else :itemList="item" />
    </div>
  </div>
</template>
<script lang="ts">

import { Vue, Component, VuePropertyDecorator } from '../../../../../provider/service/vue/constants/vue'

const { Prop } = VuePropertyDecorator

@Component({
  components: {
    HierarchicalList
  }
})
class HierarchicalList extends Vue {
  @Prop({ default: [] }) itemList?: any[]
}

export default HierarchicalList
</script>
<style lang="scss">

</style>
```

. . . 

And yet . . maybe there should be . . or there could be . . a way . . to have . . the list component . . allow . . elements to be toggled . . uh . . according to the . . list index . . for example . . 

```vue
<template>
  <div class="account-description">
    <HierarchicalList :itemList="['hello', ['hello again', 'hi there']]">
      <div v-if="typeof hierarchicalListItem === 'string'">
        <span>{{ hierarchicalListItem }}</span>
      </div>

      <!-- between 1 and 2 . . [1, 2) -->
      <div v-else-if="hierarchicalListItem.length >= 1 && hierarchicalListItem.length < 2">
        <hierarchical-list-item> <!-- place the hierarchy list item here -->
      </div>
      
      <!-- between 2 and infinity [2, 3) -->
      <div v-else-if="hierarchicalListItem.length >= 2 && hierarchicalListItem.length < 3">
        <hierarchical-list-item> <!-- place the hierarchy list item here -->
      </div>
      
      <!-- between 2 and infinity [3, 4) -->
      <div v-else-if="hierarchicalListItem.length >= 3 && hierarchicalListItem.length < 4">
        <hierarchical-list-item> <!-- place the hierarchy list item here -->
      </div>
      
      <!-- between 2 and infinity [4, 5) -->
      <div v-else-if="hierarchicalListItem.length >= 4 && hierarchicalListItem.length < 5">
        <hierarchical-list-item> <!-- place the hierarchy list item here -->
      </div>
    </HierarchicalList>
  </div>
</template>
<script lang="ts">
import { Vue, Component /*, VuePropertyDecorator */ } from '../../../../../../../provider/service/vue/constants/vue'

import HierarchicalList from '../../../../../website/data-structures/hierarchical-list/index.vue'

@Component({
  components: {
    HierarchicalList
  }
})
class Component extends Vue {

}
export default Component
</script>
<style lang="scss">

</style>

```

. . . 

I'm not really sure how to do this in vue.js . . uh . . well . . uh . . I guess I need to do more research . . 

. . 




[Written @ 21:54]


Notes @ Newly Created Words
litou
lighthoud
[Written @ 21:54]
[lighthouse typographical error]

[Written @ 21:13]

Notes @ Newly Discovered Videos


[1.4]
Factorio 1.0 Any% Speedrun Former WR 1:45:56
By Nefrums
https://www.youtube.com/watch?v=6ZUckTrmDo8
[Discovered by the YouTube Autoplay video for [2.4]]


[2.4]
FACTORIO IS A PERFECTLY BALANCED GAME WITH NO EXPLOITS - 500 Players Is Broken Strategy
By The Spiffing Brit
https://www.youtube.com/watch?v=2hgvIhMkgKU
[Discovered by YouTube Homepage amongst the first 10 videos at the top of the page]


[3.4]
New GAME BREAKING Hyper Tube Cannon! - Satisfactory Early Access Gameplay Ep 54
By ImKibitz
https://www.youtube.com/watch?v=X0OfS51vH90
[Discovered by YouTube Homepage . . in the 4th of 6 videos on the top of the page . . ]

. . 

Notes @ Newly Watching Videos

[4.4]
https://www.youtube.com/watch?v=mXVLUvt2CBw
By Unicole Unicron
Fantasy, Sex, and Love Addiction Recovery Vlog (Day 6)



[Written @ 20:36]

Notes @ Observations . . 

I just made the observation . . that . . if I take notes . . like I do now . . 

" . . note #3 . . "
  - part #1
  - part #2
  - part #3

" . . note #2 . . "
  - part #1
  - part #2
  - part #3

" . . note #1 . . "
  - part #1
  - part #2
  - part #3

. . . 

well . . uh . . well . . sometimes . . I write references . . to previous notes . . that I had taken . . and so . . 

doing a reference system like . . number1.number2.number3 . . etc . . is one way to have . . uh . . a reference system or something like that . . 

well . . so far . . today has been the first day where I've started to apply . . this sort of method . . 

for example . . 

. .

. . 

when writing the following . . uh . . you start with . . 1.0 . . and then . . for the second notes . . you do . . 1.1 . . and for the . . third group of notes . . you do 1.2 . . and for the . . fourth group of notes . . you do . . 1.3 . . and so on . . 

however . . for any of the parts . . within the notes . . you do . . 1.number . . 2.number . . 3.number . . 4.number . . and so on . . 

well . . for the first group of notes that would look like this . . 

1.0
// reference here (for example, a youtube video link, title and author name)

2.0
// reference here (for example, a website link, title, and author name)

3.0
// reference here (for example, a book title, author name)

4.0
// reference here

. . . 

etc . . 

100.0
// reference here

101.0
// reference here

etc . . 

. . . 

well . . 

then in the second group of notes . . 

1.1
// reference here . . 

2.1
// reference here . . 

3.1
// reference here . . 

4.1
// reference here . . 


and so on . . 


. . . 


well . . 


uh . . the idea that I had . . just a few moments ago . . is that . . well . . what happens when you run into the case of needing to . . uh . . use the . . 3 or 4 or 5th number index that are possible by using this scheme of development ? . . 

for example . . 

1.0.0
1.0.0.0
1.0.0.0.0
1.0.0.0.0.0

. . . 

what are the 3rd and 4th . . and Nth index used for ? . . . 

well . . this gives me an idea of something related to having . . multiple timeline indexes or something like that . . I'm not really sure . . for example . . maybe . . for the previous day . . you had a reference video that you would like to reference . . and so . . you can . . have . . uh . . 1.0.1 . . to represent . . yesterday . . or maybe you can have . . 1.0.2 . . represent the day before yesterday . . 

and well . . uh  . . now that gives me the idea to instead maybe have a date index . . . . uh . . so something like . . 

1.0.11.5.2020 . . and so . . this number can represent . . November 5, 2020 . . which is yesterday . . 

1.0.1.2.2020 . . can represent January 2, 2020 . . 

. . . 

huh . . 

well . . 

that's really quite cool . . and so if you have any references from those sections to reference . . you can have the text that reads . . 


this is an idea . . [Idea . . inspired by . . [1.0.1.2.2020]] . . and so . . the reference . . at this location . . can be interpretted very easily and quickly . . just by reading it . . uh . . and uh . . also I guess . . knowing the decoding scheme that we've talked about here . . and of course . . uh . . other encoding and decoding schemes can be developed and this is only an example . . 

Uh . . well . . that's the sort of thought that uh . . I was thinking about just now . . for the past uh . . few minutes uh . . now . . and so decided to write about it . . 

uh . . one of the ideas that inspires me . . uh . . to write about this . . uh . . which is maybe more along the idea that I originally had . . uh . . well . . uh . . is the idea that the numbers can really represent anything you would like . . and so . . uh . . these are like . . uh . . well . . it could be like . . uh . . a way of alternating between different realities . . uh . . or something like that . . and so . . maybe like a main stream of consciousness or a main stream of understanding or something . . uh . . well . . you can also maybe have references of probable streams of consciuosnes xD . . which is kind of weird . . because uh . . well . . maybe they were probable but uh . . really when do you have probabilities . . uh . . when you are making them realities ? . . and so . . the probabilities are like real . . uh . . and then those version of realities . . uh . . are really indentifying themselves as real . . and yet . . uh . . -_- . . 

. . 

right . . probable realities . . alternate realities . . realities . . theory . . practice

is it really all the same thing ? . . 


well in any case . . you can have numbers to keep track of your representations of imaginary new constructs which is quite cool :O . . 

. . 

in any case . . the way I'm writing notes these days . . seems to only really require . . 2 numbers . . number1.number2 . . 

and so for example . . 1.0 . . 2.0 . . 3.0 . . 4.0 . . 

1.1 . . 2.1 . . 3.1 . . 4.1 . . 

1.2 . . 2.2 . . 3.2 . . 4.2 . . 

1.3 . . 2.3 . . 3.3 . . 4.3 . . 

well . . and so on . . 

Uh . . I don't really reference . . the references for previous days . . and so . . another number for representing . . the . . day . . that uh . . I'm interested for my notes to reference . . uh . . well . . uh . . so . . I only need the 2 number references . . 

well . . one thing that also inspired me to start using a second number . . is because I don't want to really keep track in my head . . which reference group . . that I'm using . . and so for example . . I often have a [1] . . and . . well . . that . . [1] . . is meant to represent . . the [1] . . before the content . . in the order of the notes as they're written from bottom to top . . from the bottom of the page . . to the top of the page . . 

and so for example . . 


. . . 


"This is a Quote." [1 @ 0:00]

. . . 


[1]
YouTube Video Title
By YouTube Video Author
https://youtube.com/watch?v=videoId

. . . 


well . . 

. . 

. . what if later in the notes . . I write another . . video reference . . but the notes are taken . . maybe . . 3 or 4 or more hours . . in the day . . or maybe the notes are taken . . uh . . well . . in another time group . . and so . . in those notes . . if I want to have another . . video reference . . I would have to keep track . . of . . the . . [1] . . that was earlier created . . and the next video would have . . [2] . . 

well . . uh . . so if I had a really long list of . . references from a previous time region . . like . . [Written @ 19:19] . . or [Written @ 20:20] . . well . . uh . . [2] . . [3] . . [4] . . [5] . . these could be other references that have already been written as the last . . previous references . . and so . . I'd have to scroll to the bottom of the list . . to see what the last reference was . . 

Instead of doing the . . scrolling down to the bottom of the list of references . . to see . . what number the last reference was . . if it was . . [10] . . or if it . . [20] . . or [30] . . and so on . . well . . I can scroll to the top of that list . . and look at th . . last number index . . if the number is . . 1.0 . . then . . the next note group will have . . 1.1 . . if the number was . . 1.10 . . the next note group will start with . . 1.11 . . and if you continue in this way . . you don't need to scroll down to the last . . reference in the . . 1.10 . . group of notes . . 

. . . 

. . . 

##### (1) Example of what you don't have to do:

Reason why you may not want to use this method is because you have to scroll down to the bottom of the list . . which could potentially be . . very long . . for example . . 100s of references long . . which is quite lengthy to traverse . . as opposed to the next alternative method of using . . 2 numbers to represent references :O


[Written @ time#3]

[to write the next note here . . you need to look at the bottom of the list . . of the previous set of notes (time#2) . . to see what the last number was . .]
[after looking at the bottom of the list . . for the previous notes (time#2) . . 8 . . is discovered as the previous number . . for references . . ]
[. . 8 being the last reference for the previous set of notes . . means . . [9] . . is the next reference for us in this next set of notes here at time#3 . . ]

. . . 



[Written @ time#2]

[5]
YouTube Video Title #5
By YouTube Video Channel #5
https://youtube.com/watch?v=videoId5

[6]
YouTube Video Title #6
By YouTube Video Channel #6
https://youtube.com/watch?v=videoId6


[7]
YouTube Video Title #7
By YouTube Video Channel #7
https://youtube.com/watch?v=videoId7


[8]
YouTube Video Title #8
By YouTube Video Channel #8
https://youtube.com/watch?v=videoId8



[Written @ time#1]

[1]
YouTube Video Title #1
By YouTube Video Channel #1
https://youtube.com/watch?v=videoId1

[2]
YouTube Video Title #2
By YouTube Video Channel #2
https://youtube.com/watch?v=videoId2


[3]
YouTube Video Title #3
By YouTube Video Channel #3
https://youtube.com/watch?v=videoId3


[4]
YouTube Video Title #4
By YouTube Video Channel #4
https://youtube.com/watch?v=videoId4





##### (2) Example of what you could do instead:

[Written @ time#3]

[to know what reference is next for . . our set of notes here in time#3 . . you can look at the first . . reference of the previous set of notes (time#2) . . and . . use the 2nd number . . of the first reference . . to determine . . what the second number . . of the first reference . . for this set of notes will be . . :O this is a lot faster than . . having to scroll down . . to see the bottom of the list . . if you are using the scroll based approach . . for retrieving the list information . . for the number of the reference :O ]
[1.1 . . is the previous first reference . . since . . 1 . . is the . . number . . of the second number . . in this reference . . notation implementation . . strategy . . then . . 1 . . uh . . well . . we'll do . . the next number after one . . in the number order of increasing order . . to determine what the reference number for the notes in this time period of notes (time#3) . . will be . . in this case . . 1.2 . . is the first reference for this group of notes . . ]


[Written @ time#2]

[1.1]
YouTube Video Title #5
By YouTube Video Channel #5
https://youtube.com/watch?v=videoId5

[2.1]
YouTube Video Title #6
By YouTube Video Channel #6
https://youtube.com/watch?v=videoId6


[2.1]
YouTube Video Title #7
By YouTube Video Channel #7
https://youtube.com/watch?v=videoId7


[4.1]
YouTube Video Title #8
By YouTube Video Channel #8
https://youtube.com/watch?v=videoId8



[Written @ time#1]

[1.0]
YouTube Video Title #1
By YouTube Video Channel #1
https://youtube.com/watch?v=videoId1

[2.0]
YouTube Video Title #2
By YouTube Video Channel #2
https://youtube.com/watch?v=videoId2


[3.0]
YouTube Video Title #3
By YouTube Video Channel #3
https://youtube.com/watch?v=videoId3


[4.0]
YouTube Video Title #4
By YouTube Video Channel #4
https://youtube.com/watch?v=videoId4






[Written @ 20:34]

Notes @ Videos Watched Before but wanting to list them here in case they go unlisted like the previous videos :O


[1.3]
Fantasy, Sex, and Love Addiction Recovery Vlog (Day 5) microdose & masturbation
By Unicole Unicron
https://www.youtube.com/watch?v=Xg6Jo6NIlgU

. . . 

Notes @ Newly Getting Ready To Start Watching Video

[2.3]
Fantasy, Sex, and Love Addiction Recovery Vlog (Day 6)
By Unicole Unicron
https://www.youtube.com/watch?v=mXVLUvt2CBw



[Written @ 19:10]

Notes @ Newly Learned Words

Septuagenarian 
[1.2 @ 0:33]

[1.2]
Everything Awful About 2020 | PAGET KAGY
By Paget Kagy
https://www.youtube.com/watch?v=CQDsH4GvwWc



[Written @ 18:51]

Notes @ Videos To Watch Later


[1.1]
A Storyteller of Visualization Evolution: Apache ECharts (incubating)
By Ovilia
https://www.youtube.com/watch?v=Q9ZWPd1WDYE
[Discovered by YouTube Search Result for "echarts" . . video result . . number . . 12]


Notes @ Newly Discovered YouTube Channels

林毅
https://www.youtube.com/channel/UCViiDvoHXfS4EuHlNhgm4wQ/videos
[Discovered by [2.1]]


[2.1]
Vue 开发 ECharts 踩坑指南@袁源_VueConf CN 2019
By 林毅
https://www.youtube.com/watch?v=jziQG8_qKMU
[Discovered by YouTube Search Result for "echarts" . . video result . . number 28]


[3.1]
echarts radar and funnel charts visualizations
By Sky Data Labs
https://www.youtube.com/watch?v=YT4DF69zWeI
[Discovered by YouTube Search Result for "echarts" . . video result . . number 39]




[Written @ 18:25]

Notes @ Newly Discovered YouTube Channels

William Candillon
https://www.youtube.com/c/wcandillon/videos
[Discovered by [1]]

Senior Software Vlogger
https://www.youtube.com/channel/UCX3w3jB05SHLbGjZPR0PM6g

Chau Codes
https://www.youtube.com/c/ChauCodes/videos


[1.0]
Challenging Ben Awad to Clash of Code
By William Candillon
https://www.youtube.com/watch?v=Aoy2MRvYK2A
[Discovered by the auto-play video after [3]]


[2.0]
Программист, певица, фото модель. Необязательно быть кем-то одним.
By Senior Software Vlogger
https://www.youtube.com/watch?v=efDq9ef2DYo
[Discovered by the YouTube video recommendation section from [3.0] . . . at video index . . number . . 7 . . not including the autoplay video]


[3.0]
Be A Software Engineer
By Chris Hawkes
https://www.youtube.com/watch?v=c3X-1uY9-g4


[4.0]
My Journey from IT Support to Self-Taught Web Developer
By Chau Codes
https://www.youtube.com/watch?v=FKXDphLrNCQ



[Written @ 17:54]

Notes @ Probable Names for ForrestKnight

CodeForrest
[Inspired by the statement at [2 @ 11:10 - 11:47] . .]

Notes @ Quotes

"
I wanna ask . . just out . . of . . curiosity . . How many people like the outdoors ? . . 

Show of hands . . do you like . . hiking . . or boating . . or fishing . . or hunting . . ? . . or . . whatever . . 

Anything . . outdoors . . I . . I love that stuff . . Do I have any people like me . . 
"
-- ForrestKnight


Notes @ Common Thoughts Expression Observation

I've noticed that maybe . . people who like to program . . uh . . programmers . . uh . . well . . maybe . . they wish . . uh . . maybe they wish that other people would also spend time with them . . or hang out with them . . or do social activities with them . . like . . talking . . or . . listening . . or . . doing other things . . uh . . this thought . . is inspired by what . . ForrestKnight . . said . . in [2 @ 11:10 - 11:47] . . uh . . well . . uh . . I've heard . . [3] . . talk about this topic of . . uh . . well . . what if people . . uh . . like . . watched . . coding live streams . . or something like that . . in . . [3] . . I don't think this was the exact thing that was said . . I haven't actually watched the video [3] . . for . . more than . . 3 or 6 . . months now . . and so I'm not actually sure . . but . . I do remember . . that . . [3] . . inspired me to think about this thought . . or think about this topic . . of . . what if more people watched programming related videos ? . . what if people watched more . . coding . . videos . . to watch what projects are being programmed ? . . well . . 

Uh . . also . . my brother has said he doesn't really like . . uh . . watching someone code . . which is understandable . . uh . well . . maybe those weren't the exact words . . but he gave me the impression that . . he doesn't like to watch the live streams . . because . . uh . . well . . he doesn't want to watch someone program . . or . . write . . text code . . in a text editor program or something like that . . well . . in any case . . he didn't use the words . . 'text editor program" . . uh . . he said something . . uh . . and uh . . I'm translating . . what he said to relate to the topic of . . not being interested in watching . . the program . . development . . lifecycle . . or something like that . . or not being interested in watching . . uh . . the software . . development . . lifecycle . . which . . to be honest . . or to be personal about this topic . . it's totally understandable . . since . . text based programming . . is . . uh . . maybe not really attractive . . uh . . in terms of being able to have mental hoops you jump through to realize entertainment . . or to realize . . how fun . . or . . what exactly should be considered the goal . . or what exactly should be considered fun . . uh . . especially if the programmer on the live stream . . doesn't answer questions . . or doesn't respond . . you may have to make a game out of understanding what's going on . . on your own . . without an instructor . . uh . . well . . just reading . . text on the stream . . or just reading . . uh . . letters of code . . that represent some program manipulation parameters . . is maybe something that isn't at all that interesting if you're not wanting to reverse engineer that topic . . uh . . well . . uh . . and it is also complicated to involve the topic that . . there could be so many things that are being done . . uh . . for example . . the person could be programming . . all sorts of things . . from text editors . . to video games . . to uh . . websites . . and uh . . robotics related things . . and if some windows . . aren't open to show what exactly the person is working on . . maybe it truly is difficult . . to interpret what they're working on . . and on what parts of the program they are working on . . and so . . uh . . well . . there could be more education around this topic . . to show people all the possibilities . . and the varieties of interpretation methods for reading programs . . from . . text based programming . . to . . node based programming with circles and lines that you drag and drop to connect different parts of the program to one another . . to . . uh . . well . . a program could even be a video game . . like . . you could write programs in . . minecraft . . or there is a game called . . kerbal space program . . that allows you to program the trajectory of rockets . . and that's quite a cool simulation program . . that could possibly translate to real life . . and so . . even though . . it's maybe not so text-based focus . . well . . it's a way to instruct the instrument of interest to do the things that you like . . and so . . that is programming . . and so that's really quite cool . . also . . the sims . . is a programming type system . . where you can program . . people to move in certain ways . . which is really quite cool . . and so there are many ways to define the characteristics of a programming language . . or what a program is . . and so . . uh . . 

well . . maybe part of . . uh . . watching live streams with programs that look like texts . . is maybe to read them like books . . or stories with characters that you can connect . . to one another . . or uh . . something like that . . characters that are uh . . maybe like . . data structures . . and those data structures are being manipulated by algorithms . . such as . . "walking around from one direction to another" . . a . . character . . is able to walk to a . . grocery store to get some food . . or something like that . . and in programming . . . . a uh . . data structure . . uh . . is able to be . . uh . . walking to the . . uh . . well . . walking is one way to make the analogy work but uh . . transferring . . or moving is another word to use . . but . . walking or moving to the database . . is one way to say that . . uh . . the data structure character . . or maybe the instantiation of the data structure character . . uh . . since . . maybe it's not all people who go to the grocery store to shop . . but maybe a particular person goes to the grocery store to shop . . or something like that . . 

. . uh . . well . . maybe not really . . books . . but . . uh . . well . . uh . . in a way . . books . . in how text is related I suppose . . uh . . maybe though you want to be able to read from multiple uh . . different . . seemingly . . unrelated . . pages . . where for example . . you don't necessarily . . know . . which . . file directory the program file . . is being represented in . . and you don't necessarily know . . the program . . architecture . . being applied . . or something like that . . 

The program architecture is a data structure representing . . the directory and file structure of the program . . project . . or something like that . . and so . . the program architecture is being operated on by the programmer . . who . . retrieves . . the files they want to update . . and also . . creates new files . . in new . . locations of the data structure . . or locations of the directory tree . . of the program directory . . or something like that . . 


. . 

[More than . . 10 minutes . . later . . trying to remember a video game . . that I wanted to list . . above . . as an example . . of a programming . . language . . instrumentation system that can be in the form of a video game . . [besides the example of minecraft] . . I ended up listing . . The Kerbal Space program . . instead . . in the above . . paragraph . . and well . . uh . . I was trying to think of uh . . another example . . and now . . after . . performing a google . . search . . have found the example :O . . ]


Factorio
https://www.factorio.com/

. . 

I was thinking of the name . . "Infinito" . . repeatedly . . in my head . . and then . . after doing a google search for . . "video games about programming and automation" . . the result of . . "Factorio" . . came . . up . . 

. . 

I used . . "Steam" . . the video game . . platform website . . https://store.steampowered.com/search/?term=programming . . and searched for . . "programming" . . Steam . . provided an option on the right of the result column that allowed me to select other options like . . "simulation" and "automation" . . the "automation" word . . being a genre type for video games . . inspired me . . to type . . "automation" . . in my search . . text result . . for google . . and so . . when using google . . I used the keywords . . "video games about programming and automation" . . :O 

. . 

And now I'm actually . . able to find the result of . . Factorio . . in the Steam . . web browser . . website . . 

https://store.steampowered.com/search/?term=factorio

. . 

"automation", "base building" "resource management" . . are genres that come up when I hover over the Factorio . . game type :O . 

. . 



[Written @ 17:18]


Notes @ Newly Discovered YouTube Channels

ForrestKnight
https://www.youtube.com/c/FKnight/videos
[Discovered by [1] from the recommendation list of a video I was watching by Chris Hawkes]

Notes @ newly Watched Videos

[1]
4 Data Structures You Need to Know
By ForrestKnight
https://www.youtube.com/watch?v=y7ksXLhuy-w


[2]
3 More Data Structures You Need to Know
By ForrestKnight
https://www.youtube.com/watch?v=2EQ9WswHJ6A


[3]
Roasting Your Code (with Ben Awad)
By Nick White
https://www.youtube.com/watch?v=FWG0nw9Wq-g




[Written @ 16:14]

Notes @ Newly Learned Phrases

Fly by night
[1 @ 3:32]



Notes @ Newly Discovered Organizations

Webflow
[Advertisement from 1 @ approximately 3:48]

Notes @ Newly Learned Organizations

Dev Mountain
[1 @ 3:35]



. . . 

Notes @ Observations

Coder, Programmer, Software Engineer
[1 @ 2:28]




[1]
Everything You Need To Know About Current Web Development
By Chris Hawkes
https://www.youtube.com/watch?v=B_KmeodkZQ8



[Written @ 15:42]

Notes @ Newly Discovered YouTube Channels

TechnicalRoundup
https://www.youtube.com/c/TechnicalRoundup/videos
[Discovered by [1]]



Notes @ Newly Learned Words

Third Rail [1 @ 3:21]





[1]
Casual Friday w/ @LedgerStatus
By TechnicalRoundup
https://www.youtube.com/watch?v=adyTPs1AB9E



[Written @ 15:38]

Notes @ Newly Learned Names

paget kagyuni
[Inspired by: The name "paget kagy" was typed into the YouTube Search bar . . and . . I was preparing to search for "Unicole Unicron" . . I didn't press backspace . . to remove . . "paget kagy" . . and so . . "paget kagyuni" . . was the first thing that I saw before I realized . . that I had made a mistake . . and so . . "paget kagyuni" . . is really quite a beautiful name to discover . . "Kageyuni" . . "Kagyuni" . . well . . "Kageyuni" . . was a typographical error as I was tying to type the name . . "Kagyuni" . . but uh . . well . . either one . . uh . . when I think of how to make . . "Kagyuni" . . uh . . maybe more . . uh . . sounding how I imagine it could sound in my mind . . "Kageyuni" . . is a really beautiful way to have a last name . . ]


[Written @ 13:00]

Notes @ Newly Discovered Organizations

Leyline
https://leyline.gg/
[Discovered by: How We Can Abolish Poverty & Do Good | JEREMY DELA ROSA (LEYLINE) By Paget Kagy https://www.youtube.com/watch?v=WWf14K3Hayg]


Notes @ Newly Discovered Videos

[1]
Trying to build an Open Source browser in 2020 - Patricia Aas
By NDC Conferences
https://www.youtube.com/watch?v=eYKM_wJNzUY

[2]
Say Goodbye to Passwords and Hello to WebAuthn - Ben Dechrai - JSConf Korea 2020
By JSConfg [Presented By Ben Dechrai]
https://www.youtube.com/watch?v=rUqhBkt0xIc

[3]
Inaccessible websites? - it's you, not JavaScript - Anuradha Kumari - JSConf Korea 2020
By JSConf
https://www.youtube.com/watch?v=J42a3SP9obY


[Written @ 12:31]

[Copy pasted from November 5, 2020 from the 0:05 . . time period]


### Things to do

// Performance related topics

- [] Convert images to webp
- [] Ensure website files are updated after updates are made to the website (without having to refresh the cache)
- [] Nuxt.js for server-side rendering
- 


// Continuous Integration / Continuous Deployment

- [] performance measurement screen captures
  - [] lighthouse html output
  - [] publish lighthouse html output
  - [] screen capture of the published lighthouse html output page
- [] 



// Performance measurement

- [] Use page speed insight: https://developers.google.com/speed/pagespeed/insights/
- [] Use webpagetest: https://www.webpagetest.org/

// Readme

- [] Add "Demo Website Performance Statistics" to readme
  - [] WebPageTest . . webpagetest screen-capture
  - [] Page Speed Insight . . page speed insight screen-capture
  - [] Coverage Report . . screen-capture
- []


### Things that are planned to be completed

Use the mobile-first design strategy:

- [] Update the your account page
  - [] 
  - [] add a account description section
    - [] account id
    - [] account name
    - [] account username
    - [] account description
    - [] physical location address (country, city/town/province)
    - [] account contact list (email address, website url list, social media url list, physical mail address list, other items)
    - [] account business product listing
    - [] account currency exchange currency listing
    - [] account employer positions listing
    - [] 
  - [] use the name "account statistics" to contain "account balance history" and "transaction amount history" (received and sent transaction amount)
  - [] add a "find shops to buy products or services" section
    - [] add filter for finding shop by location
    - [] add filter for finding a shop by the products and service provided
    - [cancelled] add text: find shops that accept ecoin such as "individuals", "online commerce stores", "restaurants", "apartments", "house salesplaces", "gymnasiums", "sports recreation facilities" and more (useful keywords)
  - [] add a "find employers to work with" section
    - [] add filter for finding an employer by location
    - [] add filter for finding an employer by job genre
    - [cancelled] add text: find employers such as "individuals", "schools", "technology companies", "hospitals", "government agencies", "non-profit organizations", "local businesses" and more (useful keywords)
  - [] add a "find currency exchanges to trade with" section
    - [] add filter for finding exchange by location
    - [] add filter for finding exchange by currency
    - [cancelled] add text: find digital currency exchanges to trade Ecoin for fiat currencies (ie. U.S. Dollar, Euro, Yuan, and more) or alternative digital currencies
  - [] add a "find ecoin alternatives" section
    - [] add filter for finding exchange by feature
    - [cancelled] add text: find alternative digital currencies to send and receive money with
  - [] 


- [] Update the network status page
  - [] add a "digital currency statistics" section
  - [] add a "account statistics" section
  - [] add a "transaction statistics" section
  - [] add a globe activity flight map section


### Things To Complete - Statistics related

// account statistics

- [] account statistics
  - [] listed statistics
    - [] 2 rows, horizontal scrolling, 2nd column visible
    - [] current account balance
    - [] amount received per month
    - [] amount sent per month
  - [] graph statistics
    - [] account balance history [line graph]
    - [] places account is sending to [geography chart]
    - [] places account is receiving from [geography chart]
  - [cancelled] searchable list graph
    - [cancelled] accounts transacted with
      - [thoughts-on-cancellation]: transaction history is similar feature


// transaction statistics

- [] transaction statistics
  - [] listed statistics
    - [] 2 rows, horizontal scrolling, 2nd column visible
    - [] number of transactions sent
    - [] number of transactions received
    - [] latest transaction sent (w/ time ago)
    - [] latest transaction received (w/ time ago)
    - [] number of recurring transactions sent
    - [] number of recurring transactions received
    - [] amount received per month [by recurring transactions]
    - [] amount sent per month [by recurring transactions]
    - [] total amount received
    - [] total amount sent
    - [] percent increase or decrease from latest processed transaction
  - [] graph statistics
    - [] transaction calendar heatmap
    - [] transaction amount sent and received graph [line graph]
    - [] 
  - [] searchable list graph
    - [] transaction history


// about page

- [] show circular progress for about this project loading (v-progress-circular)


// settings page

- [] make vertical aligned settings input to the full width
  - [thoughts]: I like the appearance of the full width text fields on desktop display

// website header

- [] show send payment button
  - [] show icon button for mobile display
  - [] show horizontal button for desktop display
    - [thoughts-on-cancellation]
      - search account allows users to type the account they would like to send to . . which is similar to the feature of sending . .
      - one button is fine but maybe having access to 2 buttons is faster for sending transactions
      - we can have a "send payment" button next to the user for each result
      - by default clicking on the search result can go to the user's account page
    - [not-cancelled-any-more]
      - why not provide the option for both a button and a text field . . that could be convenient for either option to be accessible here in this scenario

- [] show account name beside the account currency amount


// transaction

- [] show progress bar until next processing
  - [] show text: percent of time left since now
  - [] show text: amount of time left until transaction processing
- [] show red progress bar for recurring transactions
- [] show blue progress bar for scheduled transactions
- [] show scheduled transaction as processed [snackbar notification]
- [] show recurring transaction as processed [snackbar notification]

// snackbar notification list

- [] create a snackbar notification list component




// welcome message

- [] show snackbar notification list for welcome message
  - [] Customer #597832, Welcome to Ecoin. Your Ecoin account is automatically created for you to start sending and receiving Ecoin payments! 🎉
  - [] Visit the Settings Page ⚙️ to change your default account settings

// 

- [] show transaction in navigation drawer
  - [thoughts]: view page content alongside the transaction for quick re-selection of transaction on main page content
  - [thoughts]: use a dialog for html component support
- [] show account in navigation drawer
  - [thoughts]: view page content alongside the account for quick re-selection of account on main page content
  - [thoughts]: use a dialog for html component support


### Things that are planned to be completed

* Things that are planned to be completed

- [] http-server/data-structures/provider-service-emulator-http-server-proxy
  - [] lunr service http server proxy
  - [] firebase emulator service http server proxy
    - [] initialize provider service
      - [] initialize provider service by http server proxy
      - [] initialize provider service for firebase algorithms
    - [x] create item
    - [x] get item
    - [] get item list
    - [] update item
    - [] add item event listener
    - [] remove item event listener
- [] daemon/data-structures/administrator-daemon
- [] daemon/data-structures/service-activity-simulator-daemon
- 


# Things to be completed

- [] add multi-language support [internationalization; i18n]
- [] add language support button
- [] fix the issues on the command line interface
- [] update account
  - [] update firebase auth uid for security
  - [] update account profile picture
  - [] update acocunt name
  - [] update account username
    1. [] add to queue map of users requested for that username
    2. [] Check queue map to see if user is the first to attend the operation
    3. [] Delete the entry map from existing username to accountId
    4. [] Update the entry map for the new username to accountId
    5. [] Change the username of the account [Firebase validate other operations]
    6. [] Delete from queue map of users registered for that username
  - [] update account by adding blocked account
    - [] can no longer send currency to this account
    - [] can no longer receive currency from this account // ???
  - [] update account by removing blocked account
    - 
  - [] update account behavior statistics // for security, fraud detection, transaction overuse
    - [] admin only read and write
    - [] measure the frequency of creating a transaction
    - [] measure the transaction amount median
    - [] freeze an account in case of abnormal account use ??
- [] get account
  - [] get account by qr code id
- [] create account
  - [] create a qr code id
  - [] add to account-qr-code-to-account-id-map
  - [] add to create-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] delete account
  - [] add to delete-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] update transaction
  - [] update transaction amount
  - [] update transaction text memorandum
  - [] update transaction by adding a like
  - [] update transaction by removing a like
  - [] update transaction by adding a dislike
  - [] update transaction by removing a dislike
  - [] update transaction by adding a comment
  - [] update transaction by removing a comment
- [] create transaction
  - [] include permissioned contact information
  - [] text memorandum
  - [] private text memorandum
  - 
- [] apply universal basic income
  - [] create a transaction for all accounts to add digital currency amount
- [] initialize-service-account (sign in)
  - [] with google
  - [] with email and password
- [] uninitialize-service-account (sign out)
- [] gitlab continuous integration / continuous deployment (ci / cd)
  - [] publish ecoin daemon to development server
- [] readme.md
  - [] demo usecase preview: update transaction settings
  - [] demo usecase preview: view transaction information
- [] html component
  - [] pay with digital currency button works on websites
  - [] create an html component with an initial property object
  - [] return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
  - [] return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
- [] digital currency account data structure
  - [] business acount description
  - [] does this account represent a copy or fork of the ecoin source code?
    - [] how many accounts does this currency have?
    - [] how many transactions does this currency have?
  - [] is a digital currency exchange that transacts using ecoin
  - [] is business account, selling products and services using ecoin
    - [] is providing currency exchange service
      - [] supported currency list
    - [] is providing products
      - [] product category list, product price ranges
    - [] is providing services
    - [] is providing other
  - [] is a physical location shop or store
    - [] restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
  - [] contact information variable tree (email, phone number, website url, physical address etc.)
    - [] permission rule on variable tree item: Public, Private, Permissioned
  - [] account statistics variable tree (set fields like 'number of employees' and other information)
  - [] are you seeking employment?
    - [] employment skills list
    - [] sought after ecoin payment amount per time period
  - [] are you an employer?
    - [] are you hiring right now
    - [] available position variable tree
      - [] ecoin payed per time period
      - [] sought after skills list
      - [] position name, time description (ie. 30 hours / week)
  - 
- [] update the styles of the desktop version of the website
  - [] account section
    - [] show the account section on the left
    - [] desktop version width should be not full width. [follow how it's done on the settings and about page]
    - [] desktop header tabs (v-tabs) should be condensed and not wide screen.
  - [] account statistics
    - [] show the account statistics on the right (allow vertical scroll)
  - [] community participants: show a list using vuetify table
- [] settings page
  - [] add light and dark theme setting
- [] add a `qr code` button
  - [] [header] Your Account QR Code
    - [] [subheader] Account QR Code for `account name`
  - [] [camera icon] Capture Account QR Code
  - [] find a digital currency account by its qr code by taking a photograph of the account qr code
- [] network statistics page
  - [] number of ecoin distributed (timeline graph)
  - [] number of created accounts (timeline graph)
    - [] number of created business accounts (timeline graph)
  - [] number of created transactions (timeline graph)
  - [] newly distributed ecoin (live updating)
  - [] newly created accounts list (live updating)
    - [] number of created business accounts (timeline graph)
  - [] newly created transactions list (live updating)
  - [] total number of ecoin created (running sum of overall currency amount)
  - [] total number of accounts created (running sum of overall accounts amount)
    - [] total number of business accounts created (running sum of overall community participants)
  - [] total number of transactions created (running sum of overall transactions amount)
  - [] flights gl map to show transactions in realtime
- [] account page
  - [] navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
  - [] add settings icon button
  - [] transaction list should show "view transaction button"
    - [] open transaction dialog
  - [] transaction list should show "repeat transaction button"
  - [] like to dislike ratio timeline graph
  - [] number of accounts transacted with timeline graph
  - [] currency amount transacted timeline graph
  - [] number of transactions timeline graph
    - [] ratio of likes from recipient, sender
    - [] ratio of dislikes from recipient, sender
  - [] calendar heatmap of transactions created
  - [] find employers to work with
    - [] work with businesses that pay in Ecoin
  - [] find businesses to shop with
    - [] shop with businesses that transact using Ecoin
  - [] find currency exchanges to trade with
    - [] exchange Ecoin for other currencies
- [] about page
  - [] update the "Why does Ecoin exist?" answer 
    - [] enumerate the long answer clause
    - [] change the long answer to be more friendly to read and not so presumptious of minority and majority logic thinking
- [] apply transaction list
  - [] Recurring Transactions as Processed set to false
    - [] dateTobeProcessed = d
    - [] numberOfTimesProcessed = n
    - [] millisecondDelayBetweenProcessing = m
    - [] currenctDateForTransaction = c
    - [] const isAlreadyProcessedTimePeriod = d + n * m > c
- [] website style / theme
  - [] research how to improve account page style for desktop
  - [] research how to improve settings page style for desktop
  - [] research how to style the network status page
  - 
- [] readme.md
  - [] add the names, and website urls of the software dependencies items (use the same layout as the related work section)
  - [] remove list index from the software dependencies table
  - [] add appended list to each list item in the table of contents
  - [] add "completed" text to the features description text in the readme.md file
  - [] fix typo to be "for storing data on remote computers", in software dependencies / database provider / firebase realtime database section
  - [] add the domain name registration service used for https://ecoin369.com . . GoDaddy
  - [] add website library "echarts" (for logistics measurement display)
  - [] add expectation definitions measurement tools: mocha, chai
  - [] add project compilation: node.js, npm
  - [] add prepaid credit card service (with anonymous support): Bluebird, American Express
  - [] add the project authors for the software dependency list
  - [] update the software depenencies message to change the open source software message
  - [] consider adding a frequently asked questions section to the readme
  - [] add a guide to get started in publishing your own ecoin-based digital currency
    - [] easily change the name, image icon, resource urls and project description
      - [] new directory for ease of use ? or precompiled/@project ? 
    - [] how to add firebase credentials for development and production environments
      - [] create firebase api token using the command line interface tool
    - [] how to add algolia credentials for development and production environments
    - [] how to publish the project service website
    - [] how to publish the administrator daemon
  - [] add a guide to get started with the development of the project source code
    - [] how to install nodejs and npm
    - [] how to install the firebase emulator
    - [] how to install ngrok
    - [] how to start the administrator daemon
    - [] how to start the firebase emulator
    - [] how to start the service provider http server proxy
    - [] how to start the development server for the project service website
    - [] how to run a measurement of the expectation definitions (also known as unit tests but also includes integration tests)
    - [] how to start the service activity emulator daemon
    - [] an introduction to the troy architecture
    - [] 
- [] digital-currency-business data structure
  - [] number of employees, are you hiring at the moment (and do you pay in Ecoin), salary range, number of available positions, business type, are remote positions available, 
- [] firebase cloud firestore
    - [] initialize the algorithms and expectation definitions
    - [] initialize the security rules for creating, updating and deleting
    - 
- [] javascript library demo
  - [] create a demo html page for creating a "pay with ecoin button"
  - [] create a demo html page for creating a "ecoin price label"


### Planned Tasks to Be Completed

// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider


// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list


// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)


// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts




[Written @ 12:21]

[copy-pasted from Day Journal Entry for November 5, 2020 . . . ]


##### Live Stream Checklist for Ecoin #359 - November 6, 2020


Greetings

* [x] Greet the viewers
* [] Plan the tasks to complete
* [x] Live stream url
  - Live Stream URL: https://www.youtube.com/watch?v=jj7X7Mgxn4Q

Health and Comfort

* [x] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [x] Practice a breathing exercise for 5 - 15 minutes
  - Mindfulness Tool: Hug Yourself By mindfulnesstoolstv https://www.youtube.com/watch?v=x-9J0Lzq6Iw
  - approximately 10 minutes . . 12:24 - 12:34


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [x] 米津玄師 MV「パプリカ」Kenshi Yonezu / Paprika By  米津玄師 https://www.youtube.com/watch?v=s582L3gujnw
  - [] Dreams - Preface by Seth By Tim Hart Hart https://www.youtube.com/watch?v=2dtXI2RATIw&list=PLPDTOFbrYdqCBF9qmNqqxZaSj4lLm537Q&index=1
  - [] Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [x] Tao Te Ching     (The Book Of The Way)     #Lao Tzu                       [audiobook]   [FREE, FULL] By Peter x https://www.youtube.com/watch?v=o2UYch2JnO4&t=1s
  - [] [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs
  - [] Instant Calm, Beautiful Relaxing Sleep Music, Dream Music (Nature Energy Healing, Quiet Ocean) ★11 By Sleep Easy Relax - Keith Smith https://www.youtube.com/watch?v=4zqKJBxRyuo


Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
- (1) youtube live stream chat window
- (2) youtube + firefox (picture-in-picture mode)
- (3) iTerm
- (4) https://www.timeanddate.com/stopwatch
- (5) Brave Browser
- (6) Visual Studio Code
- (7) Express VPN
- (8) Visual Studio Code [previously, "Notes" Application on Macbook Laptop]


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - []

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream




