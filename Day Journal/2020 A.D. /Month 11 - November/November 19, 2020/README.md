


# Day Journal Entry - November 19, 2020


[Written @ 6:58]

Ideas presented near . . 8:03:49 of live stream . . 

[1.0 @ page 81]
"how it affects us"

[1.0 @ page 83]
"visible within that thing"


[1.0]
The Nature of Order: The Phenomenon of Life
By Christopher Alexander



[Written @ 0:07]

Notes @ Newly Learned Words

Antification
[Written @ November 19, 2020 @ 0:09]
[I accidentally typed the word . . "Antification" . . instead of typing the word . . "Anticipation" . . I'm sorry . . I didn't anticipate . . having this mistake in my writing dialog journal and so . . now there is a newly discovered word . . I'm sorry . . I'm also not sure what this word shouold mean when you eat it inside of your english conversation topic sequences .]


Notes @ Newly Learned Words


Timline
[Written @ November 19, 2020 @ 0:10]
[I accidentally typed the word . . "Timline" . . instead of typing the word . . "Timeline" . . I suppose another possible way to consider typing a word that is also interesting is to consider typing the word . . "Timland" . . it's an interesting topic since the name . . "Tim" is embedded within the word name conversation written in "Timline" . . "Timline" . . a timeline with only people named "Tim" ? . . or maybe . . "Timland" . . a place where only people named "Tim" . . live ? . . amazing]
[I'm sorry . . I'm not sure . . what this word . . "Timline" . . should mean . . at this time]



[Written @ 0:03]

# Ecoin #374 - Live development journal entry for November 18, 2020

### Video Title List
- Ecoin #374 - Live development journal entry for November 18, 2020
- Troy Architecture Experimental Data Structure for A lot of Things


### (1) Video Anticipation Description

- (1) Video Anticipation Description
- (2) Video Intention Description

- (3) Video Title
- (4) Video Description
- (5) Video Benefits
- (6) Video Features
- (7) Video Limitations
- (8) Video Additional Notes

- (9) Video Contact Information
- (10) Video News Updates
- (11) Video Community Restrictions & Guidelines

- (12) Video Codebase
- (13) Video Consumer Resource
- (14) Video Provider Resource
- (15) Video Usecase Resource

- (16) Video Community & Volunteers
- (17) Video Usecase Provided By Related Projects


### (2) Video Intention Description

Before making the video . . I planned to take a shower . . I was sitting on the toilet . . One of my goals with the live streams is to publish at least 1 hour of live streaming content to encourage me to do more work on the ecoin project . . well . . even if I don't really do anything . . I would still like to be . . encouraged in some sense . . to record at least 1 hour . . uh . . one reason for that is to be transparent and open about the development process and maybe why I am taking such a long time to make things work completely . . 

During making the video . . I started to take notes about the video that was being developed . . I was interested . . at some point in the video . . to try applying the theory of the project document . . data . . structure . . that I had previously been working on . . for the past few days . . A theoretical model for structuring or organizing uh . . all types of things of interest . . including . . project readme.md files, writing comments on youtube videos, publishing a documented video on social media, having a social conversation with someone . . and more .

But it only a theoretical model -_- . . and so maybe it isn't really applicable to certain usecases . . but that uh . . hasn't uh been the evidence that I have really been seeing so far in the last few days of trying to apply the document data structure to even my thinking patterns . . but really . . uh . . it is uh . . I'm not really sure . . uh . . the idea is to apply the data structure to any particular item of the data structure . . and so for example . . you are uh . . able to have nested uh . . documentation within your documentation . . and so for example . . maybe the data structure appears flat . . but you're intended to apply it to any degree you would like . . in any of the items that are interesting to review . . uh . . well . . something like that . . uh . . an example could be that the "Video Title" can be . . uh . . containing the "Video Codebase" . . or something uh . . like that . . uh . . even if you don't normally do something like that . . it is theoretically possible . . uh . . and also it seems like you can uh . . communicate all the things that you would be interested in communicating . . and know how to organize that communication . . by following the data structure you should communicate the things you're interested in by default . . as well as being able to extend the communication possibilities beyond . . without restriction . . so long as you follow the data structure . . the data structure is meant to be a guideline . . for doing the indefinite nesting of communication abilities . . but you don't necessarily need to follow it . . you can deviate . . but . . uh . . I'm not sure . . you can probably find a way to work things out . . I don't know all the restrictions yet . . and the possibilities are only theoretical as far as I've identified . . 

### (3) Video Title List
- Ecoin #374 - Live development journal entry for November 18, 2020
- Troy Architecture Experimental Data Structure for A lot of Things


### (4) Video Description

[0:00:0 - 8:03:49]

- I take a poop in the toilet and listen to music [1.0]

- I take a shower and listen to music [2.0]

- I get dressed after taking a shower and listen to music [3.0]

- I finish getting dressed after taking a shower and listen to music [4.0]

- I take notes in my dialog entry relational journal [1.4]

- I review a few github readme.md pages . Please refer to the video for a list of the pages . . starting at . . [3:37:34]

[8:03:49 - End of Video]

- I took notes in my dialog entry relational journal [1.4]


### (5) Video Benefits

- 🎓 **Learn More Music Genres**: Learn about music that helps you do better at certain activities including (1) pooping easier, (2) taking a comfortable shower, (3) getting dressed in a comfortable atmosphere, (4) finish getting dressed in a comfortable atmosphere

- 🎓 **Experimental Document Data Structure**: Learn more about structuring the document for your projects including, videos, images, computer programs, social conversations and more


### (6) Video Features

- 🎸 **Examples of Music Videos**: See an example music videos for (1) pooping easier music, (2) taking a comfortable shower music, (3) getting dressed in a comfortable atmosphere music, (4) finish getting dressed in a comfortable atmosphere music

- 📝 **Troy Architecture Example**: Presenting an example application of the troy architecture in a video project (this documentation is an example application)

- 🔴 **Live Stream Development Process**: This video was live streamed on . . November 18, 2020 @ 23:00 UTC . . to . . November 19, 2020 @ 8:18 UTC . . for . . a . . total . . duration . . of . . 9:22:18 . . 9 hours . . 22 minutes . . 18 seconds

- 🎬 **Live Stream Video Entry Item**: This video represents the 374 . . video entry in the . . Ecoin development process updates . . The ecoin project is available . . at . . [2.4]


### (7) Video Limitations

This section hasn't been completed yet. The published is leaving this section for another time. Please let us know if you would like to see this section updated.

### (8) Video Additional Notes

This section hasn't been completed yet. The published is leaving this section for another time. Please let us know if you would like to see this section updated.

### (9) Video Contact Information

- Jon Ide [video publisher]
  - 🎬 YouTube Channel: https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g
  - 🦊 Gitlab Account Page: https://gitlab.com/PracticalOptimism

### (10) Video News Updates

- [November 18, 2020]:
  - [23:00 UTC]:
    - [Jon Ide]: Started the live stream
- [November 19, 2020]:
  - [8:46 UTC]:
    - [Jon Ide]: Published the video

### (11) Video Community Restrictions & Guidelines

You can use this video for whatever you want . Be kind and be safe . And maybe don't use the video to hurt anyone . 

### (11.1) Video License Agreement
[M.I.T. License](http://opensource.org/licenses/MIT) Video Codebase

### (12) Video Codebase

- How to download this video: Search for a YouTube video downloading application
- How to find this video: Search YouTube for "Ecoin #374"
- How to create this video: Poop, Take a Shower, Get Dressed, Finish Getting Dressed
- How to update this video: Ask Jon Ide or ask YouTube
- How to delete this video: Ask Jon Ide or ask YouTube
- How to rebrand this video: Be creative and do your best
- How to start the development of this video: (1) record a video of your computer screen, (2) comment about taking a poop while sitting on the toilet, (3) a few minutes later, comment on preparing to take a shower, (4) a few minutes later, get out of the shower and get dressed, (5) a few minutes later finish getting dressed and comment about it and leave the comment there for several minutes without saying anything about it . Take notes and also find music . 
- How to start the production of this video: (1) computer, (2) Open Broadcast Software, (3) YouTube Live Streaming Service, (4) Toilet Bowl, (5) Shower Area, (6) T-shirt and underwear, (7) Shorts, (8) Music that people upload on the internet

### (13) Video Consumer Resource

- YouTube Resource URL: https://www.youtube.com/watch?v=kiMA8CpERBk

### (14) Video Provider Resource

### (14.1) Music Video Resources

[1.0]
Music That Makes You Poop 2.0 - 100% GUARANTEED BY VIEWERS
By Sounds You Need
https://www.youtube.com/watch?v=YM6xP7KyW0I
[Discovered by YouTube Search result for "poop music" . . Search Result Number . . 1 . . (0 playlist results, 0 channel results, 1 video result)]

[2.0]
Lit Shower Playlist | Favorite Smooth/Sensual Songs
By Sailor Goon
https://www.youtube.com/watch?v=_jx8g4Tc3l8
[Discovered by YouTube Search result for "music for taking a shower" . . Search Result Number . . 6 . . (0 playlist results, 0 channel results, 6 video results)]

[3.0]
HOW-TO: Getting Dressed!
By McMaplett
https://www.youtube.com/watch?v=-E3D1yRLHOg
[Discovered by YouTube Search result for "getting dressed after shower music" . . Search Result Number . . 2 . . (0 playlist results, 0 channel results, 2 video results)]

[4.0]
Getting Dressed Is the Best
By Moey's Music Party - Topic
https://www.youtube.com/watch?v=s2SMYmtdQZc
[Discovered by YouTube Search result for "finished getting dressed music" . . Search Result Number . . 5 . . (2 playlist results, 3 video results, 0 channel results)]

### (14.2) Book Resources

[1.3]
The Nature of Order: The Phenomenon of Life
By Christopher Alexander

#### (14.3) Live Stream Video Series Resources

[1.4]
Dialog Entry Relational Journal 0
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

[2.4]
Ecoin
By Ecorp-org (Ecorp Organization)
https://gitlab.com/ecorp-org/ecoin

### (15) Video Usecase Resource

- To learn more about community software projects that were considered in this video . . please visit . . (1) Github [https://github.com] and (2) Gitlab [https://gitlab.com]

- To learn more about the topic of feelings and how they influence the built world . . 
  - [1.3 @ page 81] "how it affects us"
  - [1.3 @ page 83] "visible within that thing"

### (16) Video Community & Volunteers

### (16.1) Present Volunteers & Community Members

- Video Publisher: Jon Ide

### (16.2) How to be a Volunteer or Community Member

- Communicate with fellow community members
- Write a comment about typographical errors or mistakes that you observe about the video
- Write a comment about the title of this video
- Write a comment about the description of this video
- Write a comment about the benefits you've gained from watching this video
- Write a comment about the features of this video that you would like to talk about
- Write a comment about the limitations of this video and what you think
- Write a comment about the additional notes of this video and what you think
- Write a comment about the contact information of this video
- Write a comment about the news updates of this video
- Write a comment about the community restrictsions & guidelines
- Write a comment about the video 
- Write a comment about the YouTube consumer resource if that is where you are watching the video
- Write a comment about the provider resources
- Write a comment about the usecase resources
- Write a comment about the community and the volunteers
- Write a comment about the related community projects that also provide the same or similar usecases to this video

### (16.3) Community Feedback Measurements

- 0 of 0 community feedback has been seen
- 0 of 0 community feedback has been responded to with 1 comment
- 0 of 0 community feedback has been responded to multiple times
- 0 of 0 community feedback has been deleted

- Latest date to check the community feedback: November 19, 2020 @ 8:06 UTC
- Latest date to respond to the community feedback: November 19, 2020 @ 8:07 UTC
- Latest date to respond to the community feedback from a sequence of previous responses: November 19, 2020 @ 8:08 UTC
- Latest date to delete community feedback: November 19, 2020 @ 8:08 UTC

### (17) Video Project Community Related to the Project Usecase

[November 19, 2020 @ 2:14][Jon Ide]: You can find more projects like this by searching for "how to document project readme.md" on YouTube

[1.1]
ReadMe Template for Github Repos
By James Q Quick
https://www.youtube.com/watch?v=eVGEea7adDM
[YouTube Video Project]

[2.1]
Writing a readme: readme essentials
By Thomas Bradley
https://www.youtube.com/watch?v=RZ5vduluea4
[YouTube Video Project]

[3.1]
Next Level GitHub Profile README (NEW) | How To Create An Amazing Profile ReadMe With GitHub Actions
By codeSTACKr
https://www.youtube.com/watch?v=ECuqb5Tv9qI
[YouTube Video Project]

[4.1]
Creating a README file on Github
By Divya Thakur
https://www.youtube.com/watch?v=MCo1UtflJHM
[YouTube Video Project]




. . . 




Document Structure:


Video Document Intention Description
Video Document Anticipation Table of Contents

Video Title
Video Description
Video Benefits
Video Features
Video Limitations
Video Additional Notes

Video Contact Information
Video News Updates
Video Community Restrictions & Guidelines

Video Codebase
Video Consumer Resource
Video Provider Resource
Video Usecase Resource


Video Community & Volunteers
Video Community Related to the Project Usecase

. . . 


Experimental Document Data Structure:


(1) Project Intention Description
(2) Project Anticipation Description

(3) Project Title
(4) Project Description
(5) Project Benefits
(6) Project Features
(7) Project Limitations
(8) Project Additional Notes

(9) Project Contact Information
(10) Project News Updates
(11) Project Community Restrictions & Guidelines

(12) Project Codebase
(13) Project Consumer Resource
(14) Project Provider Resource
(15) Project Usecase Resource


(16) Project Community & Volunteers

(17) Project Usecase Provided By Related Community Projects


