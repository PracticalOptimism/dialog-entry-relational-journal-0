

# Day Journal Entry - November 11, 2020


[Written @ 12:28]

Notes @ Cool Project Ideas

- Continually update the git repository . . 
  - An infinite loop program that updates the remote git repository every . . M milliseconds . . 


[Written @ 12:16]

Timeline Annotation:

[0:00:00 - 0:18:51]

- Starting the live stream . .
- Wrote notes in my physical paper journal notebook of the tasks to complete
- Anticipating the need to use the bathroom, and eat breakfast after having woken up from sleeping nearly 1 hour prior to the live stream starting . . 

[0:21:28 - ]

- Preparing to write a list of things to do . . for the live stream today
- Preparing to complete the easy things that are on the list





[Written @ 11:25]

[copy-pasted from Day Journal Entry for November 10, 2020 . . . ]




### Tasks for today - November 11, 2020

- [] Project Readme [Challenging Task]
- [] Your Account Page [Easy Task] [In-Active-Development]
  - []
- [] Network Status Page [Easy Task]
- [] Accessibility Insights CI / CD Process [Challenging Task]
- [] Settings Page [Easy Task]




### Things to do
[Goals set on November 9, 2020]

// README.md related goals

- [] Add measurements related information to the project readme.md file
  - [] 


// Your Account Page Related Goals


### Things to do

// Performance related topics

- [] Convert images to webp
- [] Ensure website files are updated after updates are made to the website (without having to refresh the cache)
- [] Nuxt.js for server-side rendering
- 


// Continuous Integration / Continuous Deployment

- [] performance measurement screen captures
  - [] lighthouse html output
  - [] publish lighthouse html output
  - [] screen capture of the published lighthouse html output page
- [] 



// Performance measurement

- [] Use page speed insight: https://developers.google.com/speed/pagespeed/insights/
- [] Use webpagetest: https://www.webpagetest.org/

// Readme

- [] Add "Demo Website Performance Statistics" to readme
  - [] WebPageTest . . webpagetest screen-capture
  - [] Page Speed Insight . . page speed insight screen-capture
  - [] Coverage Report . . screen-capture
- []


### Things that are planned to be completed

Use the mobile-first design strategy:

- [] Update the your account page
  - [] 
  - [] add a account description section
    - [] account id
    - [] account name
    - [] account username
    - [] account description
    - [] physical location address (country, city/town/province)
    - [] account contact list (email address, website url list, social media url list, physical mail address list, other items)
    - [] account business product listing
    - [] account currency exchange currency listing
    - [] account employer positions listing
    - [] 
  - [] use the name "account statistics" to contain "account balance history" and "transaction amount history" (received and sent transaction amount)
  - [] add a "find shops to buy products or services" section
    - [] add filter for finding shop by location
    - [] add filter for finding a shop by the products and service provided
    - [cancelled] add text: find shops that accept ecoin such as "individuals", "online commerce stores", "restaurants", "apartments", "house salesplaces", "gymnasiums", "sports recreation facilities" and more (useful keywords)
  - [] add a "find employers to work with" section
    - [] add filter for finding an employer by location
    - [] add filter for finding an employer by job genre
    - [cancelled] add text: find employers such as "individuals", "schools", "technology companies", "hospitals", "government agencies", "non-profit organizations", "local businesses" and more (useful keywords)
  - [] add a "find currency exchanges to trade with" section
    - [] add filter for finding exchange by location
    - [] add filter for finding exchange by currency
    - [cancelled] add text: find digital currency exchanges to trade Ecoin for fiat currencies (ie. U.S. Dollar, Euro, Yuan, and more) or alternative digital currencies
  - [] add a "find ecoin alternatives" section
    - [] add filter for finding exchange by feature
    - [cancelled] add text: find alternative digital currencies to send and receive money with
  - [] 


- [] Update the network status page
  - [] add a "digital currency statistics" section
  - [] add a "account statistics" section
  - [] add a "transaction statistics" section
  - [] add a globe activity flight map section


### Things To Complete - Statistics related

// account statistics

- [] account statistics
  - [] listed statistics
    - [] 2 rows, horizontal scrolling, 2nd column visible
    - [] current account balance
    - [] amount received per month
    - [] amount sent per month
  - [] graph statistics
    - [] account balance history [line graph]
    - [] places account is sending to [geography chart]
    - [] places account is receiving from [geography chart]
  - [cancelled] searchable list graph
    - [cancelled] accounts transacted with
      - [thoughts-on-cancellation]: transaction history is similar feature


// transaction statistics

- [] transaction statistics
  - [] listed statistics
    - [] 2 rows, horizontal scrolling, 2nd column visible
    - [] number of transactions sent
    - [] number of transactions received
    - [] latest transaction sent (w/ time ago)
    - [] latest transaction received (w/ time ago)
    - [] number of recurring transactions sent
    - [] number of recurring transactions received
    - [] amount received per month [by recurring transactions]
    - [] amount sent per month [by recurring transactions]
    - [] total amount received
    - [] total amount sent
    - [] percent increase or decrease from latest processed transaction
  - [] graph statistics
    - [] transaction calendar heatmap
    - [] transaction amount sent and received graph [line graph]
    - [] 
  - [] searchable list graph
    - [] transaction history


// about page

- [] show circular progress for about this project loading (v-progress-circular)


// settings page

- [] make vertical aligned settings input to the full width
  - [thoughts]: I like the appearance of the full width text fields on desktop display

// website header

- [] show send payment button
  - [] show icon button for mobile display
  - [] show horizontal button for desktop display
    - [thoughts-on-cancellation]
      - search account allows users to type the account they would like to send to . . which is similar to the feature of sending . .
      - one button is fine but maybe having access to 2 buttons is faster for sending transactions
      - we can have a "send payment" button next to the user for each result
      - by default clicking on the search result can go to the user's account page
    - [not-cancelled-any-more]
      - why not provide the option for both a button and a text field . . that could be convenient for either option to be accessible here in this scenario

- [] show account name beside the account currency amount


// transaction

- [] show progress bar until next processing
  - [] show text: percent of time left since now
  - [] show text: amount of time left until transaction processing
- [] show red progress bar for recurring transactions
- [] show blue progress bar for scheduled transactions
- [] show scheduled transaction as processed [snackbar notification]
- [] show recurring transaction as processed [snackbar notification]

// snackbar notification list

- [] create a snackbar notification list component




// welcome message

- [] show snackbar notification list for welcome message
  - [] Customer #597832, Welcome to Ecoin. Your Ecoin account is automatically created for you to start sending and receiving Ecoin payments! 🎉
  - [] Visit the Settings Page ⚙️ to change your default account settings

// 

- [] show transaction in navigation drawer
  - [thoughts]: view page content alongside the transaction for quick re-selection of transaction on main page content
  - [thoughts]: use a dialog for html component support
- [] show account in navigation drawer
  - [thoughts]: view page content alongside the account for quick re-selection of account on main page content
  - [thoughts]: use a dialog for html component support


### Things that are planned to be completed

* Things that are planned to be completed

- [] http-server/data-structures/provider-service-emulator-http-server-proxy
  - [] lunr service http server proxy
  - [] firebase emulator service http server proxy
    - [] initialize provider service
      - [] initialize provider service by http server proxy
      - [] initialize provider service for firebase algorithms
    - [x] create item
    - [x] get item
    - [] get item list
    - [] update item
    - [] add item event listener
    - [] remove item event listener
- [] daemon/data-structures/administrator-daemon
- [] daemon/data-structures/service-activity-simulator-daemon
- []


# Things to be completed

- [] add multi-language support [internationalization; i18n]
- [] add language support button
- [] fix the issues on the command line interface
- [] update account
  - [] update firebase auth uid for security
  - [] update account profile picture
  - [] update acocunt name
  - [] update account username
    1. [] add to queue map of users requested for that username
    2. [] Check queue map to see if user is the first to attend the operation
    3. [] Delete the entry map from existing username to accountId
    4. [] Update the entry map for the new username to accountId
    5. [] Change the username of the account [Firebase validate other operations]
    6. [] Delete from queue map of users registered for that username
  - [] update account by adding blocked account
    - [] can no longer send currency to this account
    - [] can no longer receive currency from this account // ???
  - [] update account by removing blocked account
    - 
  - [] update account behavior statistics // for security, fraud detection, transaction overuse
    - [] admin only read and write
    - [] measure the frequency of creating a transaction
    - [] measure the transaction amount median
    - [] freeze an account in case of abnormal account use ??
- [] get account
  - [] get account by qr code id
- [] create account
  - [] create a qr code id
  - [] add to account-qr-code-to-account-id-map
  - [] add to create-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] delete account
  - [] add to delete-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] update transaction
  - [] update transaction amount
  - [] update transaction text memorandum
  - [] update transaction by adding a like
  - [] update transaction by removing a like
  - [] update transaction by adding a dislike
  - [] update transaction by removing a dislike
  - [] update transaction by adding a comment
  - [] update transaction by removing a comment
- [] create transaction
  - [] include permissioned contact information
  - [] text memorandum
  - [] private text memorandum
  - 
- [] apply universal basic income
  - [] create a transaction for all accounts to add digital currency amount
- [] initialize-service-account (sign in)
  - [] with google
  - [] with email and password
- [] uninitialize-service-account (sign out)
- [] gitlab continuous integration / continuous deployment (ci / cd)
  - [] publish ecoin daemon to development server
- [] readme.md
  - [] demo usecase preview: update transaction settings
  - [] demo usecase preview: view transaction information
- [] html component
  - [] pay with digital currency button works on websites
  - [] create an html component with an initial property object
  - [] return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
  - [] return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
- [] digital currency account data structure
  - [] business acount description
  - [] does this account represent a copy or fork of the ecoin source code?
    - [] how many accounts does this currency have?
    - [] how many transactions does this currency have?
  - [] is a digital currency exchange that transacts using ecoin
  - [] is business account, selling products and services using ecoin
    - [] is providing currency exchange service
      - [] supported currency list
    - [] is providing products
      - [] product category list, product price ranges
    - [] is providing services
    - [] is providing other
  - [] is a physical location shop or store
    - [] restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
  - [] contact information variable tree (email, phone number, website url, physical address etc.)
    - [] permission rule on variable tree item: Public, Private, Permissioned
  - [] account statistics variable tree (set fields like 'number of employees' and other information)
  - [] are you seeking employment?
    - [] employment skills list
    - [] sought after ecoin payment amount per time period
  - [] are you an employer?
    - [] are you hiring right now
    - [] available position variable tree
      - [] ecoin payed per time period
      - [] sought after skills list
      - [] position name, time description (ie. 30 hours / week)
  - []
- [] update the styles of the desktop version of the website
  - [] account section
    - [] show the account section on the left
    - [] desktop version width should be not full width. [follow how it's done on the settings and about page]
    - [] desktop header tabs (v-tabs) should be condensed and not wide screen.
  - [] account statistics
    - [] show the account statistics on the right (allow vertical scroll)
  - [] community participants: show a list using vuetify table
- [] settings page
  - [] add light and dark theme setting
- [] add a `qr code` button
  - [] [header] Your Account QR Code
    - [] [subheader] Account QR Code for `account name`
  - [] [camera icon] Capture Account QR Code
  - [] find a digital currency account by its qr code by taking a photograph of the account qr code
- [] network statistics page
  - [] number of ecoin distributed (timeline graph)
  - [] number of created accounts (timeline graph)
    - [] number of created business accounts (timeline graph)
  - [] number of created transactions (timeline graph)
  - [] newly distributed ecoin (live updating)
  - [] newly created accounts list (live updating)
    - [] number of created business accounts (timeline graph)
  - [] newly created transactions list (live updating)
  - [] total number of ecoin created (running sum of overall currency amount)
  - [] total number of accounts created (running sum of overall accounts amount)
    - [] total number of business accounts created (running sum of overall community participants)
  - [] total number of transactions created (running sum of overall transactions amount)
  - [] flights gl map to show transactions in realtime
- [] account page
  - [] navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
  - [] add settings icon button
  - [] transaction list should show "view transaction button"
    - [] open transaction dialog
  - [] transaction list should show "repeat transaction button"
  - [] like to dislike ratio timeline graph
  - [] number of accounts transacted with timeline graph
  - [] currency amount transacted timeline graph
  - [] number of transactions timeline graph
    - [] ratio of likes from recipient, sender
    - [] ratio of dislikes from recipient, sender
  - [] calendar heatmap of transactions created
  - [] find employers to work with
    - [] work with businesses that pay in Ecoin
  - [] find businesses to shop with
    - [] shop with businesses that transact using Ecoin
  - [] find currency exchanges to trade with
    - [] exchange Ecoin for other currencies
- [] about page
  - [] update the "Why does Ecoin exist?" answer 
    - [] enumerate the long answer clause
    - [] change the long answer to be more friendly to read and not so presumptious of minority and majority logic thinking
- [] apply transaction list
  - [] Recurring Transactions as Processed set to false
    - [] dateTobeProcessed = d
    - [] numberOfTimesProcessed = n
    - [] millisecondDelayBetweenProcessing = m
    - [] currenctDateForTransaction = c
    - [] const isAlreadyProcessedTimePeriod = d + n * m > c
- [] website style / theme
  - [] research how to improve account page style for desktop
  - [] research how to improve settings page style for desktop
  - [] research how to style the network status page
  - 
- [] readme.md
  - [] add the names, and website urls of the software dependencies items (use the same layout as the related work section)
  - [] remove list index from the software dependencies table
  - [] add appended list to each list item in the table of contents
  - [] add "completed" text to the features description text in the readme.md file
  - [] fix typo to be "for storing data on remote computers", in software dependencies / database provider / firebase realtime database section
  - [] add the domain name registration service used for https://ecoin369.com . . GoDaddy
  - [] add website library "echarts" (for logistics measurement display)
  - [] add expectation definitions measurement tools: mocha, chai
  - [] add project compilation: node.js, npm
  - [] add prepaid credit card service (with anonymous support): Bluebird, American Express
  - [] add the project authors for the software dependency list
  - [] update the software depenencies message to change the open source software message
  - [] consider adding a frequently asked questions section to the readme
  - [] add a guide to get started in publishing your own ecoin-based digital currency
    - [] easily change the name, image icon, resource urls and project description
      - [] new directory for ease of use ? or precompiled/@project ? 
    - [] how to add firebase credentials for development and production environments
      - [] create firebase api token using the command line interface tool
    - [] how to add algolia credentials for development and production environments
    - [] how to publish the project service website
    - [] how to publish the administrator daemon
  - [] add a guide to get started with the development of the project source code
    - [] how to install nodejs and npm
    - [] how to install the firebase emulator
    - [] how to install ngrok
    - [] how to start the administrator daemon
    - [] how to start the firebase emulator
    - [] how to start the service provider http server proxy
    - [] how to start the development server for the project service website
    - [] how to run a measurement of the expectation definitions (also known as unit tests but also includes integration tests)
    - [] how to start the service activity emulator daemon
    - [] an introduction to the troy architecture
    - [] 
- [] digital-currency-business data structure
  - [] number of employees, are you hiring at the moment (and do you pay in Ecoin), salary range, number of available positions, business type, are remote positions available, 
- [] firebase cloud firestore
    - [] initialize the algorithms and expectation definitions
    - [] initialize the security rules for creating, updating and deleting
    - 
- [] javascript library demo
  - [] create a demo html page for creating a "pay with ecoin button"
  - [] create a demo html page for creating a "ecoin price label"


### Planned Tasks to Be Completed

// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider


// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list


// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)


// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts

### Things to do

- [] use lighthouse to measure web accessibility values for the project service web browser website
- [] take a screencapture image of the lighthouse report html file
- [] publish the lighthouse measurement html page, and screencapture image to firebase static file host


##### Live Stream Checklist for Ecoin #367 - November 11, 2020


Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete
* [x] Live stream url: https://www.youtube.com/watch?v=00xtZg8K09Y
* [x] Live stream duration: 00 : 51 minutes : 19.766 seconds

Health and Comfort

* [x] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position
* [] Practice a breathing exercise for 5 - 15 minutes
  - [time range] [time elapsed]: Mindfulness Tool: Hug Yourself By mindfulnesstoolstv https://www.youtube.com/watch?v=x-9J0Lzq6Iw

Personal Goals

* [] Live stream for at least 3 hours
* [x] Start Live streaming at 12:00


Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [] 米津玄師 MV「パプリカ」Kenshi Yonezu / Paprika By  米津玄師 https://www.youtube.com/watch?v=s582L3gujnw
  - [] Dreams - Preface by Seth By Tim Hart Hart https://www.youtube.com/watch?v=2dtXI2RATIw&list=PLPDTOFbrYdqCBF9qmNqqxZaSj4lLm537Q&index=1
  - [] Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [] Tao Te Ching     (The Book Of The Way)     #Lao Tzu                       [audiobook]   [FREE, FULL] By Peter x https://www.youtube.com/watch?v=o2UYch2JnO4&t=1s
  - [] [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs
  - [] Instant Calm, Beautiful Relaxing Sleep Music, Dream Music (Nature Energy Healing, Quiet Ocean) ★11 By Sleep Easy Relax - Keith Smith https://www.youtube.com/watch?v=4zqKJBxRyuo
  - [] Bhaktas- The Cosmic Mantra! Meditation Music ! By Hareesh Sahadevan https://www.youtube.com/watch?v=O3wQBWIOCQA
  - [x] [ 𝑷𝒍𝒂𝒚𝒍𝒊𝒔𝒕 ] aesthetic song • lofi type beat • 3 hours By  연우yanu https://www.youtube.com/watch?v=cbuZfY2S2UQ


Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
- (1) youtube live stream chat window
- (2) youtube + firefox (picture-in-picture mode)
- (3) iTerm
- (4) https://www.timeanddate.com/stopwatch
- (5) Brave Browser
- (6) Visual Studio Code
- (7) ProtonVPN [previously, Express VPN]
- (8) Visual Studio Code [previously, "Notes" Application on Macbook Laptop]


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - []



Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream






