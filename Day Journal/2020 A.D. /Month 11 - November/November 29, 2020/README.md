

# Day Journal Entry - November 29, 2020

[Written @ 23:16]

Notes @ Newly Discovered Word Expression

as close as
[Written @ [at least 10 minutes earlier - 23:34]]
[inspired by "at least near"]

at least near
[Written @ [at least 10 minutes earlier - 23:34]]
["at least near" . . uh . . is like . . "at least" . . or . . "at most" . . but doesn't specify . . a . . direction . . from which . . the audience . . is approaching . . for example . . saying . . "at least" . . 10:00pm . . may suppose . . that . . it is . . 10:00pm or greater . . and so . . 11:00pm . . is also a time that works . . and . . 11:30pm . . is also a time that works . . uh . . but that could be . . uh . . cognitively involving . . uh . . when you have to uh . . I'm not sure . . hmm . . it seems like . . uh . . making the assumption of which direction you're going . . can be . . tricky . . in some cases . . for example . . uh . . I guess . . maybe with a clock . . you can . . see . . that . . the clock . . starts to rewind . . and so . . for example . . 9:00pm . . on the next day . . also satisfies . . the  . . "at least 10:00pm" . . statement . . uh . . "at least 10:00pm on November 29, 2020" . . or . . "at least 10:00pm today" . . may be another way . . of being more specific to maybe suggest to the reader . . or to the listener . . or to the audience . . that . . maybe . . uh . . 9:00pm . . on the next day . . doesn't satisfy . . the conditions . . of what is being discussed or something like that . . uh . . also . . there could be the possibility of . . uh . . thinking about using the term . . "at most 10:00pm" . . uh . . you could say for example . . the dinner will start "no later than 10:00pm" . . or . . "the dinner will start . . at a time no greater than 10:00pm" . . but maybe . . uhm . . greater than . . or less than . . is . . quite involving . . to have to decide between . . in terms of maybe how you want to write the message . . and so . . if you would like to maybe suggest a range . . of close possibilities . . "at least near" . . uh . . "at least near 10:00pm" . . "at least near 10:00pm plus or minus 10 hours" . . could also be a great way to introduce that either direction of possible time ranges . . is possible . . and you won't need to question whether you should use the term . . "at least" . . or . . "at most" . . or . . "less than" . . or . . "greater than" . . uh . . you can say . . "at least as near as" . . or . . "as close to this as 10 units" . . or . . "as close to that as 10 units" . . uh . . which is maybe uh . . a way to suppose . . where uh . . the approximate area of things is . . without necessarily specifying the direction . . or without necessarily assuming the direction that is being assumed . . such as for example . . assuming that the greater than direction is . . with the larger number . . or that the less than direction is with the smaller number . . and so for example . . uh . . 1, 2, 3, 4, 5, 6 . . uh . . an then . . at least 6 . . supposes that . . 7, 8, 9, 10 . . are then the next possibilities . . uh . . or something like that . . when in fact you could have a sort of uh . . interpretation system that tells you that . . well . . actually we uh . . reverse . . the order . . uh . . and so . . uh . . for example . . in this other number interpreation system . . or in this other uh . . way of measuring information or something . . saying . . "at least 6" . . could uh . . mean that . . 5, 4, 3, 2, 1 . . are the possibilities to consider . . or something like that . . uh . . uh . . maybe an example for this second type of . . metric system could be . . in track and field races . . where you have a . . list of uh . . athletes . . running . . on the track . . and . . they complete a race . . in a given order . . and so . . saying that . . someone . . uh . . got . . "at least 6th place" . . uh . . could mean that . . maybe they . . received . . 5th place . . or 4th place . . or 3rd place . . or 2nd place . . or maybe even 1st place . . in the order they completed the race . . with the rest of the competitors . . of the track and field event . . and so . . uh . . right . . "at least near" . . or . . "as close to" . . "as close as" . . uh  . . "as close as 6th place" . . uh . . could be . . uh . . another . . uh . . way uh . . or . . uh . . maybe communicating . . uh . . the nearness . . uh . . uh . . if that is the intention of the communication method . . but . . saying . . "at least" . . or . . "at most" . . are also really good methods for communicating . . uh . . depending on the occassion I guess . . I don't know . . uh . . this uh . . way of saying thisngs . . "at least near" . . seems new to me and so I thought I would write uh . . something about that . . uh . . it seems interesting . . I don't uh . . I don't know . . uh . . maybe I won't always use the same words to say the same thing . . for example . . uh . . I could use . . words that uh . . are . . "at least as close to the meaning that I'm trying to express" :D . . :O]


[Written @ 23:13]

Notes @ Newly Learned Words

seld
[I accidentally typed the word "self" as "seld" . . and discovered . . that the word . . "seld" . . is an existing word . . meaning . . "rarely . . uncommon . . seldom . . " . .]


[Written @ 5:33]

##### Live Stream Checklist for Ecoin #NUMBER_HERE - Month, Day Year

Greetings

* [] Greet the viewers
* [] Plan the tasks to complete
* [] Live stream url:
* [] Live stream duration: 

Health and Comfort

* [] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [] Sit or stand in a comfortable position

Self Love

* [] Practice intentional smiling
* [] Practice gratitude meditation and prayer
* [] Practice self-love (hug yourself technique)
  - [] Mindfulness Tool: Hug Yourself By mindfulnesstoolstv https://www.youtube.com/watch?v=x-9J0Lzq6Iw&ab_channel=mindfulnesstoolstv
* [] Practice intentional breathing
* [] Practice singing and toning (as recommended by Unicole Unicron [1.0 @ 3:11] and the Pleiadians [2.0 @ Page 150])
  - [] Fiddler on the roof - If I were a rich man (with subtitles) By guru006 https://www.youtube.com/watch?v=RBHZFYpQ6nc&ab_channel=guru006
  - [] Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4&ab_channel=SikhMantras


Personal Goals

* [] Live stream for at least 3 hours
* [] Start Live streaming at 12:00

Music

* [] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [] 米津玄師 MV「パプリカ」Kenshi Yonezu / Paprika By  米津玄師 https://www.youtube.com/watch?v=s582L3gujnw
  - [] Dreams - Preface by Seth By Tim Hart Hart https://www.youtube.com/watch?v=2dtXI2RATIw&list=PLPDTOFbrYdqCBF9qmNqqxZaSj4lLm537Q&index=1
  - [] Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [] Tao Te Ching     (The Book Of The Way)     #Lao Tzu                       [audiobook]   [FREE, FULL] By Peter x https://www.youtube.com/watch?v=o2UYch2JnO4&t=1s
  - [] The Seth Material - Introduction (1 of 2) By Tim Hart Hart https://www.youtube.com/watch?v=Jlcos5ZM2NE&list=PLPDTOFbrYdqA0vpIROtyd3tXbzkK49E5T
  - [] [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs
  - [] Instant Calm, Beautiful Relaxing Sleep Music, Dream Music (Nature Energy Healing, Quiet Ocean) ★11 By Sleep Easy Relax - Keith Smith https://www.youtube.com/watch?v=4zqKJBxRyuo
  - [] Bhaktas- The Cosmic Mantra! Meditation Music ! By Hareesh Sahadevan https://www.youtube.com/watch?v=O3wQBWIOCQA
  - [] [ 𝑷𝒍𝒂𝒚𝒍𝒊𝒔𝒕 ] aesthetic song • lofi type beat • 3 hours By  연우yanu https://www.youtube.com/watch?v=cbuZfY2S2UQ
  - [] The Nature of Personal Reality - Introduction by Jane Roberts By Tim Hart Hart https://www.youtube.com/watch?v=8GkzYqJmpGM&list=PLPDTOFbrYdqBBIu1k5ubY7OBKcy5Xc5co&index=1&ab_channel=TimHartHart
  - [] 2 hours Study With Me (with talking during breaks) By Study Vibes https://www.youtube.com/watch?v=MC6yJ4BvmKY&ab_channel=StudyVibes


Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
- (1) youtube live stream chat window
- (2) youtube + firefox (picture-in-picture mode)
- (3) iTerm
- (4) https://www.timeanddate.com/stopwatch
- (5) Brave Browser, Firefox, Firefox Nightly, Firefox Developer
- (6) Visual Studio Code
- (7) ProtonVPN [previously, Express VPN]
- (8) Visual Studio Code [previously, "Notes" Application on Macbook Laptop]


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - []

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream

Checklist References

[1.0]
CAM CHURCH- TRANSMUTATION
By Unicole Unicron
https://www.youtube.com/watch?v=cKvcQpPrjY4

[2.0]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak



[Written @ 4:16]


[Copy-pasted from the Singing Journal Entry - November 29, 2020 from 0:46]
Notes @ Newly Discovered Names

Enēlena
Enelena
Enilena
[Pronounced "En-Eee-Lena" . . ]
[Inspired by song lyrics . . "Mistung en ee-lena" . . which sounds like what was being said . . in [1.0 @ 7:36]]




[Written @ 1:22]

Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 2:24:48]

- 📝 [commentary] I was preparing to do some self love activities before the live stream . . (1) breathing (2) smiling (3) hugging myself and saying "I am wonderful . . I am kind . . I am loved . . I love myself . . " (4) singing . . 

- 📝 [commentary] I downloaded books from "Official First Contact" and took notes in my dialog entry relational journal . . relating to . . different professions that people could have . . that aren't necessarily . . well known . . from my experience . . today . . (1) archeoplanetography (2) dream-art science (3) true mental physics (4) complete physicians (4) shamanism (5) space pilot . . speaking of which [10.0] . . is a resource relating to . . space pilots for inner-space traveling . . 


[2:24:48 - 5:07:58]

- 📝 [commentary] I am listening to the song lyrics for [1.0] and ensuring they sound like the lyrics provided by [2.0] [3.0] [4.0] [5.0] [6.0] [7.0] . . before . . starting to sing . . as singing is one of the things that . . Unicole Unicron [8.0 @ 3:11] and the Pleiadians [9.0 @ Page 150] recommend . . 

[5:07:58 - 6:26:31]

- 📝 [commentary] I am starting to sing the first song in the album . . outloud . . using my voice . . and you are welcome . . to sing along . . if you would also like to consider . . practicing . . uh . . singing . . and toning . . as recommended by . . the whales that channeled through . . Unicole Unicron [8.0 @ 3:11] and . . the Pleiadians [9.0 @ Page 150] . .

[6:26:31 - ]

- 📝 [commentary] Salutations


References:

[1.0]
Ajeet Kaur Full Album - Haseya
By Sikh Mantras
https://www.youtube.com/watch?v=_cX70nrrvM4&ab_channel=SikhMantras

[2.0]
Ajeet Kaur feat. Peia - Haseya Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur-feat-peia/haseya-lyrics/

[3.0]
Snatam Kaur - Chattr Chakkr Vartee (Courage) Lyrics
By SongLyrics
http://www.songlyrics.com/snatam-kaur/chattr-chakkr-vartee-courage-lyrics/

[4.0]
Ajeet Kaur - Kiss the Earth Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur/kiss-the-earth-lyrics/

[5.0]
Ajeet Kaur feat. Trevor Hall - Akaal Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur-feat-trevor-hall/akaal-lyrics/

[6.0]
Snatam Kaur - Ra Ma da Sa (Total Healing) Lyrics
By SongLyrics
http://www.songlyrics.com/snatam-kaur/ra-ma-da-sa-total-healing-lyrics/

[7.0]
Ajeet Kaur - Re Man Lullaby Lyrics
By SongLyrics
http://www.songlyrics.com/ajeet-kaur/re-man-lullaby-lyrics/

[8.0]
CAM CHURCH- TRANSMUTATION
By Unicole Unicron
https://www.youtube.com/watch?v=cKvcQpPrjY4

[9.0]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak

[10.0]
Space Exploration | Gigi Young
By Gigi Young
https://www.youtube.com/watch?v=zLzyyl5_cY4&ab_channel=GigiYoung





[Written @ 0:01]


Notes @ Newly Discovered Projects


The Infinite Voice Project
https://theinfinitevoiceproject.org/


Notes @ Newly Remembered Organizations

Official First Contact
https://www.officialfirstcontact.com



