


# Day Journal Entry - November 25, 2020


[Written @ 4:27]


Notes @ Newly Discovered People


Angel R. (MissRMBA)
https://twitter.com/MissrMBA
[Inspired by [1.3] and scrolling down to see a photograph [2.3]]

. . photograph . . from . . [2.3]

Angel R. (@MissrMBA)
![Angel R. (MissrMBA)](./Photographs/angel-radcliffe-missrmba.png)



2020 dates still available, secure Angel to speak today ! (virtual as well)  
Book Angel https://lnkd.in/eBzbPPh
[By Twitter @MissrMBA]
https://twitter.com/MissRMBA/status/1242890193256026112/photo/1


[1.3]
DuckDuckGo Image Search for "twitter news feed" 
https://duckduckgo.com/?q=twitter+news+feed&t=brave&iax=images&ia=images&iai=http%3A%2F%2Fsocial-media-university-global.org%2Fwp-content%2Fuploads%2F2016%2F01%2FPeriscope-in-Twitter-feed.png

[2.3]
Big Advance: Periscope Video now in Twitter News Feed*
By Suus Non Ut Difficile • Home of the SMUGgles
http://social-media-university-global.org/2016/01/big-advance-periscope-video-now-in-twitter-news-feed/

[Written @ 3:32]

Notes @ Newly Learned Words


Predicate Transformer Semantics
[1.2 @ Contributors]





[1.2]
Rustproof
By Matthew Slocum, Sami Sahli, Vincent Schuster, Michael Salter, Bradley Rasmussen, Drew Gohman, Matthew O'Brien
https://github.com/Rust-Proof/rustproof



https://en.wikipedia.org/wiki/Predicate_transformer_semantics




[Written @ 2:47]

Notes @ Random Thoughts

A team of tall sports athletes . . overlook . . a student . . 

The tall sports atheltic figures are men . .

The camera is positioned at the student . . the tall sports atheletes stand around . . the student in a semi-circle huddle . . and look down on the student . . 

The camera is angled upward to face the sports athletes . . and their bodies appear like dark shadows . . like in a cartoon animated series to show . . the mysteriousness of the . . athletic characters who are quite good at their sport . . 

The athletes are looking down at the student in question . . "He doesn't know how to 179?" . . someone says . . "I don't think he's even 179'd before" . . another person says . . 

The atheletes are wearing t-shirt . . jerseys . . that read the number . . "179" . . 

. . . 

"What does 179? Mean?" 

"How good do you have to . . at the sport . . that the atheletes are playing . . to be on . . their team ? . . Does their sports jersey . . mean . . they . . are . . 179ers?" . . 

"Is 179 like a sports skill or something ? . . Is i a sports skills that reflects how good the tall people are at what they do ? . . they're so tall . . do you think . . a small . . student . . like the one being looked down on . . will ever learn . . how to . . 179? . . do you think they will ever join the team ? . . what's so good about being a 179er? . . "


. . . 

these are some questions that could prove to be an interesting story line . . also . . it is a random thought I was having while . . looking at the number 179 . . and wondering what the audience at home . . is thinking about . . "Do they think . . I can't . . 179?" . . "Are they looking down on my ability to 179?" . . 





[Written @ 2:03]


Timeline Annotation:

[0:00:0 - 3:02:00]

- Wrote down some ideas on how to know what the user is anticipating from visiting a webpage . . one prinicple that I've been thinking about is . . the user is always after . . (1) News Updates . . (1.1) Project News Updates or . . (1.2) Community News Updates . . Project News Updates are things about the video or movie or project codebase or . . any updates that relates to the . . item of interest . . and information relating to why the user is there . . on the webpage . . Community News Updates . . are information relating to how the community is connecting to the codebase . . and what the community thinks about the project . . and this can be anyone from the volunteers on the project . . to the . . general public . . to health officials . . or . . public government officials . . and all of that feedback . . can always be updates . . in realtime . . and anyone . . can uh . . change their mind . . or . . have something new to say . . and so . . all of these items . . are . . uh . . considered . . "News Updates" . . All of the items on the page . . of a website . . can be considered . . as . . "News Updates" . . and . . "News Updates" . . can have many properties . . and I've identified . . at least . . 2 properties . . that are quite useful to consider . . (1) the frequency of the news update . . (2) the density of information of the news update . . The frequency of the news update . . can be . . for example . . how frequently the information changes . . if you are reading a news story on the page . . of a website . . if the words or text of that story started to change . . and new words replaced the old words you were reading . . like . . a video playing . . for each single word . . or each character of the page . . the frequency . . measure . . or how often the words change over time . . can be considered . . a property . . of a news update . . The second property I've considered . . is uh . . maybe the idea of the . . density of the news update . . uh . . how much information . . is provided per unit . . of uh . . space perception . . which can be quite individualized . . for example . . the space of . . words . . that an astrophysicist . . knows . . may contain foreign space objects like . . "neutron star" . . and so if you're a 5th grader in middle school . . and don't know what astrophysics is . . then this . . "space" of . . possible word perceptions . . may not come to your immediate . . attention as to what those words represent . . what does it mean for there to be a "neutron star" . . in your immediate space perception . . maybe you imagine . . a hot gaseous object in space . . but don't necessarily have the language to describe other features like . . the density of the atmosphere . . and the units of measurement for other various items of interest . . and so that density . . of space . . isn't necessarily . . light weight . . for a 5th grade . . student . . and so . . a 5th grade student may be getting a really dense . . news update . . uh . . and if that news update is changing rapidly . . for example . . new information is discovered about the nature of neutro stars . . and the website webpage starts updating to resemble to newly discovered insights . . the density is heavy . . but also the . . frequency is quite high as well . . uh . . if . . the pressure of the atmosphere gets a new measurement unit name description every 2 seconds . . that's quite a high frequency compared to . . if the updates were every . . 5 years . .


- Looked for solution to fix hashgraph coq file compilation error . . [3.0] . . for the Coq version 8.7.2 . . on my macbook pro . . 


- I scrolled through the book on Coq . . to learn if the syntax error problem could be fixed . . [1.0]

[3:02:00 - 3:27:42]

- I wrote notes on the timline annotation for this video recording

[3:27:42 - 4:22:18]

- I updated the Formal Verification Files for the Hashgraph Coq [3.0] codebase . . to compile . . for the Coq version 8.7.2 . . to showcase . . the Coq . . program . . and introduce the concept . . of . . "formal methods" "formal verification" "computer assisted theorem provers"


References:

[1.0]
Computer Arithmetic and Formal Proofs
[By Sylvie Boldo and Guillaume Melquiond]
[Downloaded from "Libgen.is"]

[2.0]
The Coq Proof Assistant
https://coq.inria.fr/

[3.0]
Coq Proof Completed By Carnegie Mellon Professor Confirms Hashgraph Consensus Algorithm Is Asynchronous Byzantine Fault Tolerant
[By Hedera Hashgraph]
https://hedera.com/blog/coq-proof-completed-by-carnegie-mellon-professor-confirms-hashgraph-consensus-algorithm-is-asynchronous-byzantine-fault-tolerant



[Written @ 2:01]

[Copy-pasted from November 24, 2020]
[I accidentally forgot to create a new day journal entry . . and . . wrote . . the earlier . . time entries . . listed . . below . . on . . the day journal entry . . for . . November 24, 2020]


[Written @ 0:51]

Notes @ Thoughts on Product Categories
[Inspired by 1.1 @ "Areas of IMPACT"]


Health & Wellness

Early Childhood Development

Mental & Behavioral Health

Economic Opportunity

Developmental Disabilities

Youth & Family Services



[Written @ 0:49]


Notes @ Newly Discovered Organizations

[1.1]
HumanKind
https://www.humankind.org/
[Discovered by text search result for "proof" from "https://hedera.com"]






