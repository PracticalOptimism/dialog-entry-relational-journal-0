

# Day Journal Entry - November 24, 2020



[Written @ 23:14]

Thoughts about the user interface

. . . 




[Written @ 0:54]


Notes @ Newly Watched Video

[1.1]
The Pursuit Of Happyness: Job interview
By Binge Society - The Greatest Movie Scenes
https://www.youtube.com/watch?v=UUDKEbX5OQw




[Written @ 0:26]

You create your own reality
-- Seth, Jane Roberts Channeled Personality Essence


Belief makes real
-- Unicole Unicron


Your dream world is real

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . . 

. . 


. . . 

We are the voices who speak without tongues of our own. We are the sources of that energy from which you come. We are creators, yet we have also been created. We seeded your universe as you seed other realities.

We do not exist in your historical terms, nor have we known physical existence. Our joy created the exaltation from which your world comes. Our existence is such that communication must be made by others to you.

Verbal symbols have no meaning for us. Our experience is not translatable. We hope our intent is. In the vast infinite scope of consciousness, all is possible. There is meaning in each thought. We perceive your thoughts as lights. They form patterns.

Because of the difficulties of communication, it is nearly impossible for us to explain our reality. Know only that we exist. We send immeasurable vitality to you, and support all of those structures of consciousness with which you are familiar. You are never alone. We have always sent emissaries to you who understand your needs. Though you do not know us, we cherish you.

Seth is a point in my reference. He is an ancient portion of us. We are separate but united. Always the spirit forms the flesh.

[1.0 @ page 181]

[1.0]
"Seth Speaks: The Eternal Validity of the Soul"
[Book Author: Seth, Jane Roberts and Robert Butts]
[Book Publication Date: 1984]
[Book Publisher: Bookthrift Co]

Notes @ Newly Discovered Book Publishers


Bookthrift Co
https://openlibrary.org/publishers/Bookthrift_Co
