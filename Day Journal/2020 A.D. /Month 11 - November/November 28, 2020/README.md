

# Day Journal Entry - November 28, 2020

[Written @ 23:44]


- Space Pilot [4.2]
- The Art of Shamanism [Pleiadian Books] [3.2]
- Dream-Art Science [Seth Books] [2.2]
- True Mental Physicist [Seth Books] [2.2]
- The Complete Physician [Seth Books] [2.2]
- Archeoplanetography [P'ntl] [1.2]
- Curious Student [5.2]


[1.2]
Official First Contact
https://www.officialfirstcontact.com/

[2.2]
The "Unknown" Reality, Volume 2.
By Seth, Jane Roberts and Robert Butts
Pages 742

[3.2]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak
Page 150

[4.2]
Alien Interview
By Matilda O'Donnell McElroy

[5.2]
Lacerta
By Ole K.


[Written @ 23:36]

Notes @ Newly Discovered Disciplines

The Art of Shamanism

[I had listened to the audiobook . . at . . [1.1] . . several times before . . but . . it seems that . . I've . . uh . . accidentally . . uh . . skipped this . . uh . . I didn't realize . . that . . the topic . . of . . "Shamanism" . . was . . what was being practiced . . also . . uh . . I didn't realize . . that . . Shamanism . . seems to be . . the practice . . of . . effecting . . many worlds . . uh . . and not only the world of the . . physical senses of which . . uh . . my physical uh . . sense perception . . or uh . . my physically acustomed . . consciousness . . is so readily to accept . . as . . uh . . the one true world that exists . . and other worlds seem to be hidden . . in an imagination . . structure . . uh . . well . . uh . . uh . . also . . this does . . remind me . . of . . what . . uh . . Seth . . from . . the . . Jane Roberts and Robert Butts . . books . . says . . uh . . in terms of . . multidimensional . . realities . . and each of our personalities . . as individual . . uh . . consciousness . . uh . . entities . . uh . . uh . . each of our . . personalities . . is . . uh . . perceiving . . through many . . bodies . . on many different . . uh . . realms . . or realities of existence . . and so . . our uh . . essence . . is not only . . realizing its . . capabilities . . or its talents . . or its . . uh . . interests . . or uh . . challenges . . uh . . uh . . through . . uh . . the person . . or through the individuals . . or people that we see in our . . immediate . . sense perception . . or our immediate conscious awareness of ourselves . . but that there are . . uh . . latent . . imaginings . . of ourselves . . that are also capable . . of being perceived . . uh . . or at least in uh . . the case that Seth seems to present . . the topic . . these individuals . . in other worlds . . are . . separate from us . . but they are . . ourselves . . had we . . perceived our . . personality . . through . . their . . consciousness . . or something like that . . I'm stil learning about this topic . . and so . . maybe how I've described things . . isn't really . . how the presentation ought to have been made for this topic . . I'm sorry . . ]


[1.1]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak

[Written @ 23:03]



##### Live Stream Checklist for Ecoin #384 - November 27, 2020

Greetings

* [x] Greet the viewers
* [] Plan the tasks to complete
* [x] Live stream url: https://www.youtube.com/watch?v=uPE591zz9H4
* [x] Live stream duration: 06:34:43.245

Health and Comfort

* [x] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [] Sit or stand in a comfortable position

Self Love

* [x] Practice intentional smiling
  - [I smiled . . intentionally . . with my mouth . . curled . . and I did so for . . less than . . 10 minutes . . ]
* [x] Practice gratitude meditation and prayer
  - [I said . . "Thank You" . . outloud . . to myself . . one time . . ]
* [x] Practice self-love (hug yourself technique)
  - [I did the exercise . . Mindfulness Tool: Hug Yourself By mindfulnesstoolstv https://www.youtube.com/watch?v=x-9J0Lzq6Iw&ab_channel=mindfulnesstoolstv . . in my head . . while hugging myself . . and expanding my arms . . also in mind . . and not with my physical arms . . ]
* [x] Practice intentional breathing
  - [I took a few breaths from my nose while smiling . . I did this for less than 10 minutes . . ]
* [x] Practice singing and toning (as recommended by Unicole Unicron [1.0 @ 3:11] and the Pleiadians [2.0])
  - [song-1][sang 1 time, song duration . . 5 minutes 41 seconds]: Fiddler on the roof - If I were a rich man (with subtitles) By guru006 https://www.youtube.com/watch?v=RBHZFYpQ6nc&ab_channel=guru006
  - [song-2][sang 2 times, song duration . . ]: Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4&ab_channel=SikhMantras



Personal Goals

* [] Live stream for at least 3 hours
* [] Start Live streaming at 12:00


Music

* [] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [] 米津玄師 MV「パプリカ」Kenshi Yonezu / Paprika By  米津玄師 https://www.youtube.com/watch?v=s582L3gujnw
  - [] Dreams - Preface by Seth By Tim Hart Hart https://www.youtube.com/watch?v=2dtXI2RATIw&list=PLPDTOFbrYdqCBF9qmNqqxZaSj4lLm537Q&index=1
  - [] Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [] Tao Te Ching     (The Book Of The Way)     #Lao Tzu                       [audiobook]   [FREE, FULL] By Peter x https://www.youtube.com/watch?v=o2UYch2JnO4&t=1s
  - [] The Seth Material - Introduction (1 of 2) By Tim Hart Hart https://www.youtube.com/watch?v=Jlcos5ZM2NE&list=PLPDTOFbrYdqA0vpIROtyd3tXbzkK49E5T
  - [] [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs
  - [] Instant Calm, Beautiful Relaxing Sleep Music, Dream Music (Nature Energy Healing, Quiet Ocean) ★11 By Sleep Easy Relax - Keith Smith https://www.youtube.com/watch?v=4zqKJBxRyuo
  - [] Bhaktas- The Cosmic Mantra! Meditation Music ! By Hareesh Sahadevan https://www.youtube.com/watch?v=O3wQBWIOCQA
  - [] [ 𝑷𝒍𝒂𝒚𝒍𝒊𝒔𝒕 ] aesthetic song • lofi type beat • 3 hours By  연우yanu https://www.youtube.com/watch?v=cbuZfY2S2UQ
  - [] The Nature of Personal Reality - Introduction by Jane Roberts By Tim Hart Hart https://www.youtube.com/watch?v=8GkzYqJmpGM&list=PLPDTOFbrYdqBBIu1k5ubY7OBKcy5Xc5co&index=1&ab_channel=TimHartHart
  - [] 2 hours Study With Me (with talking during breaks) By Study Vibes https://www.youtube.com/watch?v=MC6yJ4BvmKY&ab_channel=StudyVibes


Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
- (1) youtube live stream chat window
- (2) youtube + firefox (picture-in-picture mode)
- (3) iTerm
- (4) https://www.timeanddate.com/stopwatch
- (5) Brave Browser, Firefox, Firefox Nightly, Firefox Developer
- (6) Visual Studio Code
- (7) ProtonVPN [previously, Express VPN]
- (8) Visual Studio Code [previously, "Notes" Application on Macbook Laptop]


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - [4:53:50] Everything looks good

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream


Checklist References

[1.0]
CAM CHURCH- TRANSMUTATION
By Unicole Unicron
https://www.youtube.com/watch?v=cKvcQpPrjY4

[2.0]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak
Page 150
"We recommend that you drink great amounts of water, spend time breathing and oxygenating, do toning, and practice allowing unlabeled sound to express itself through your body. Keep your body active and alive. Spend time communicating, meditating with peace, and listening to whatever is speaking to you from the inside. Weave your discoveries into the structure of your life, and intend along the way what it is that you want."



[Written @ 23:01]


Notes @ Videos To Watch Later

[1.0]
She used 6 years to shoot, 3 different classes of children struggling
By  一条Yit
https://www.youtube.com/watch?v=-QJlN-YbdDE


[Written @ 4:16]

Notes @ Newly Created Words

perspection
[Written @ November 28, 2020 @ 4:16]
[I had a thought about a word . . and I used the word . . "perspection" . . instead of using the word . . "perception" . . I'm sorry . . I'm not sure . . what this word . . "perspection" . . should mean . . at this time . . ]




[Written @ 2:32]

Notes @ Random Thoughts

In an alternative universe . . there are societies of people . . on a planet . . and their unit of payment . . is hip hop . . rhythm beats . . where the . . people . . there . . rap to one another . . and spit . . sick . . lyrics . . and if the recipient of the sick lyrics . . likes the freestyle sick beat . . that determines . . the weight . . of the value of the sick beat . . 

And so for example . . if you're a politician . . you're someone who learns to spit sick beats . . 

If you're a mother . . visiting the grocery store . . to get some food . . to eat . . for your kids . . you're wearing . . a heavy hoodie . . and wearing swag that makes you appear originally ready to spit sick rhymes at the cashier . . and the cashier . . receives your payment for the bottles of milk . . and the bread . . as soon as you slap down . . the latest rap hip hop mix . . in real time . . and especially for that moment . . and so . . all the payments are uniquely identifiable . . and counterfeit payments . . are detectable . . if someone has already heard the rhyme before . . and there's sticky businesses about . . trying to take people's flows . . and trying to copy people's rhythms . . and so maybe . . if you're caught trying to mimick other characters in the sick spit rap game . . of musical genre hip hop originality beats . . you're . . like a poor person . . and people . . will help . . donate by giving you advice on how to . . have your own flow . . or how to come up with a sick line . . and the type of . . beats and rhythms that make the most flow . . from the cashier's . . heart beat pump music rhythm receiving device . . that pumps out comments like . . "yea, that's pretty good" . . "yea, that's pretty sick" . . "yup, that's new. . here's your lettuce" . . "here's your lettuce" . . "keep cutting that cheese, G. . " . . "keep makin' fresh bread . . bro . . keep that family . . up and ready for the next sick rhythm . . keep makin' that bread . . " . . 

[This idea was inspired by . . (1) I'm listening to . . [1.1] . . while listening to . . [2.1] . . at the same time . . It started to sound like . . Jacque Fresco . . and . . "Larry King" . . were . . rhyming together . . or they were rapping together . . or singing together . . it sounded really cool . . and there's a beat . . and rhythm . . uh . . well . . I started playing around with the idea . . uh . . well . . I was getting ready to give myself a haircut . . in the bathroom . . and so I had the time . . to uh . . think about these things . . uh . . since uh . . preparing to cut my own hair is . . uh . . really . . uh . . an automatic task . . uh . . that I don't really need to apply a lot of cognitive attention towards . . and so . . I'm uh . . mentally free . . to . . uh . . attract myself to other things . . uh . . and well . . uh . . well . . it really did sound like what Jacque Fresco . . was saying . . uh . . well . . the way uh . . it matched up with the music . . uh . . uh . . from . . uh . . [2.1] . . uh . . well . . they uh . . uh . . the matching up . . uh . . I started to imagine . . you know . . what if . . politicians . . were . . uh . . required . . to . . uh . . talk . . with . . a beat . . in the background . . or what if ordinary every day people . . always talked to one another . . by having a beat . . in the background . . and they had to communicate their message through sick hot rhymes . . that expressed their thoughts and feelings . .  ]


[1.1]
Jacque Fresco - Introduction to Sociocyberneering - Larry King (1974)
By Jacque Fresco Foundation
https://www.youtube.com/watch?v=lBIdk-fgCeQ

[2.1]
Dreams of Japan 🍃 lofi hip hop mix
By Dreamy
https://www.youtube.com/watch?v=SXfupJIXtRA&ab_channel=Dreamy




Notes @ Newly Discovered Names

Jacqure
[I accidentally typed the . . name . . "Jacqure" . . instead of typing . . "Jacque"]


Notes @ Newly Created Words

Double Queen Rally
[Written @ November 28, 2020 @ 2:33]
[Example: Jacque Fresco and Larry King formed a Double Queen Rally to face the banks and big businesses . . and the mainstream social construct . . to inform the people . . about a new direction for the information management of the people . . of . . planet . . Earth]




