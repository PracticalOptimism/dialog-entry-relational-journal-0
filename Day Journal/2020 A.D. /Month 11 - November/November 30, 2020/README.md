

# Day Journal - November 30, 2020


[Written @ 7:59]

Notes @ Newly Discovered Activity In The Way That It's Presented

Composting [1.2 @ 16:53 - 17:09]
[I haven't seen composting done like this . . in addition . . I don't have . . a strong background in how composting works . . but this looks really cool . . and I'm going to keep it in mind . . for future . . consideration . . in terms of having a way . . to . . to recycle . . various . . organic materials . . and also I'll uh . . learn more . . about . . composting . . https://morningchores.com/composting/]


[Written @ 7:53]

Notes @ Newly Discovered Books

The Nightingale [1.2 @ 13:44]
Firefly Lane [1.2 @ 13:51]


[Written @ 7:33]

Notes @ Newly Discovered Fashion Style

- (1) Kilt-skirt
- (2) Winter Jacket
- (3) Tight Leggings
- (4) Combat Boots
[1.2 @ 7:49 - 8:23]

[Written @ 7:13]

Notes @ Newly Learned Jetta

Isabel Paige's Jetta [1.2 @ 5:02]


[1.2]
A Day Of My life In The Mountains (story 58)
By Isabel Paige
https://www.youtube.com/watch?v=NPak1xKe4_0


[2.2]
[Copy-Pasted from Previous Notes Journal Book . . Words > "Newly Created Words / Terms"]
Jetta
[written @ May 22, 2020 @ 3:00am - 4:06am]
[meaning]: Jetta is a book that isn’t published or written due to circumstances that prevented the book from being published such as stopping from writing the book or throwing away the manuscript or burning the paper or information source or a unforetold story or death of an individual who would have otherwise written a book and distributed it for others to consume.
[example]: I have a few Jettas on my mind.
[example #2]: I sure hope it’s not a Jetta that you’re keeping from everyone.
[example #3]: Are you going to publish your Jetta, yet?
[use cases]: A Jetta, This Jetta, That Jetta, Her Jetta, My Jetta, His Jetta, etc.





[Written @ 6:25]

Notes @ Newly Discovered Petition

To: The Government of Canada and The City of Vancouver - Local Government
Start The Venus Project in Canada.
https://you.leadnow.ca/petitions/build-the-first-research-city-designed-by-the-venus-project-in-vancouver-canada-area
[Discovered by . . I saw an image . . that looked interesting . . in the google . . image search results . . for . . "the venus project" . . [1.1] . . is the image photograph . . I clicked on the image . . and saw the title . . "Start The Venus Project in Canada" . . that title . . was quite interesting . . uh . . well . . I proceeded . . to right click on the title . . of the result . . and . . discovered the page was a petition . . very cool . . I'm really surprised and excited to have found this . .]


[1.1]
![vancouver](./Photographs/vancouver-1620772_1920.jpg)


[Written @ 5:04]

Notes @ Newly Discovered Group of YouTube Genres

Study Vibers
- Study Vibes https://www.youtube.com/c/StudyVibes/videos
- James Scholz https://www.youtube.com/c/JamesScholz/videos




Notes @ Newly Discovered YouTube Channel

James Scholz
https://www.youtube.com/c/JamesScholz/videos
[Discovered by [1.0]]


Buddha's Lounge
https://www.youtube.com/c/BuddhasLounge/videos
[Discovered by [2.0]]


[1.0]
study with me live pomodoro
By James Scholz
https://www.youtube.com/watch?v=ZI7QH1YsxRo
[Discovered by YouTube Home Page . . Video . . Order Number . . 15 . . ]

[2.0]
Chill African Music | African Dreams
By Buddha's Lounge
https://www.youtube.com/watch?v=OOp9qxMIzvw
[Discovered by YouTube Home Page . . Video . . Order Number . . 75 . . ]



. . . 

calculation: [15, 13, 13, 15]
number of calculations: 4
thoughts on the latest results: 15

countlist: [
  75,
  38 x 2 - 1 = (60 + 16) - 1 = 75,
  38 x 2 - 1 = 76 - 1 = 75
  ]

number of calculations: 3 calculations
thoughts on the latest results: 75


Notes @ Newly Created Words

calculates
[Written @ November 30, 2020 @ 5:20]
[I accidentally typed the word . . "calculates" . . instead of typing the word . . "calculations" . . calculates . . reminds me of . . uh . . ates . . uh . . uh . . ates . . ates . . ates . . are uh . . maybe something like . . sulfates . . glutamates . . selenates . . uh . . calculates . . a computational group of something xD -_- . . I don't know I'm making things up -_- . . I'm not sure what this word should mean at this time . . I'm sorry . . [a few moments later] . . ooo . . well . . you know . . how . . uh . . people . . measure things like . . uh . . disntance . . in feet . . or inches or meters or yards . . what if there could be other measurement units like . . ducks . . or rabbits . . or horses . . how many horses tall is that . . building ? . . that's pretty cool . . but then you could also consider . . the . . steps of a program . . the operations . . that a computer program performs to be . . a . . measurement unit . . like . . 3 operations . . or . . 3 calculations . . or . . 3 steps to perform this . . operation . . uh . . but then uh . . maybe there are other ways of . . uh . . formalizing . . or symbolizing . . a step . . or a . . calculation . . or an operation . . uh . . well maybe for example . . if you are measuring . . the luminosity of the sun . . you may be able to do so with your . . gadget #1 . . but . . with gadget #2 . . and gadget #3 . . those devices give you complete different data readers . . maybe they give you the measurements in colored pictures . . or uh . . in dice rolls or marble spins . . and not the way you expected like . . index values on a clock . . -_- . . maybe that's not really a good example . . I'm not sure . . uh . . calculates . . hmm . . something to do with . . measuring . . calculations ? . . so you can measure the calculations that were in the first sense  .. measurements . . and you can measure the measurements that you measure . . you can measure the measurements that you measure . . yea . . maybe for example . . if you are reading the clock . . one second and the clock reads . . 10:00pm . . and then 1 second later . . you look at the clock again . . and the clock reads . . 10:00pm . . you can . . uh . . be more and more uh . . confident . . in your uh . . measurement . . that the time is . . 10:00pm . . and so . . if you continue to look back and forth at the clock . . for an entire minutes . . uh . . maybe . . all you see is . . your measurements are . . 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, 10:00pm, . . and so on . . for however many measurements that you make . . well that gives you a way of uh . . starting to make . . predictions about . . uh . . what the world is like . . uh . . and you can base your predictions . . uh . . on . . the original measurements you made . . of what the clock reads . . in that 1 minute . . that you spent reading the clock . . uh . . well . . uh . . maybe you read the clock . . 1 minute later . . and discover that the clock reads . . 10:01pm . . this might really surprise you at first . . you may have thought that the clock . . should really say . . 10:00pm . . you took so many measurements . . how many measurements did you take ? . . 60 measurements ? . . where you looked at the clock . . and then you looked away . . and then you recorded . . the number . . and then you looked at the clock . . and then you looked away . . and then you recorded the number that you saw for that last round of looking ? . . you took so many calculations . . to build your confidence that the time ought to be . . 10:00pm . . and then you learn . . new surprising information . . new information . . that's maybe . . surprising . . but it's not necessarily surprising if you're already familiar with clock technology . . and then one day . . you have a dream . . and in the dream . . you are . . sitting in a classroom . . and the classroom . . has a clock on the wall and you are being asked to read the clock . . and tell the time . . from what the clock says . . and in one instant . . you look at the clock . . and then you look away from the clock and you record the number that you read . . and the clock read . . 11:00pm . . the clock read . . 11:00pm . . and then . . well . . uh . . I uh . . guess . . it's a dream . . so the clock then read . . 1:00pm . . when you look back at it in the next . . moment . . or maybe it reads . . 11:30pm . . or 11:45pm . . even though it feels like only . . 1 earth second has passed . . 1 earth second . . is how many seconds in the dream world ? . . and how many . . earth seconds for each different variety of dream worlds ? . . could there be different varieties of dream worlds ? . . would a clock make sense . . in the dream world . . :D . . I'm not sure . . now I'm just having fun . . I'm sorry for wriitng too much -_- . . I'm sorry ]


selemates
[Written @ November 30, 2020 @ 5:24]
[I accidentally typed the word . . "selemates" . . instead of typing the word . . "selenates" . . I'm sorry . . I'm not sure . . what this word . . should mean . . at this time . . [a few seconds later] hmm . . in my peripheral vision . . uh . . a few seconds uh . . after not being sure . . of what this word should mean . . I glanced at my notes . . for the word . . "calculates" . . and . . I saw . . the word . . "selemates" . . read as . . uh . . the word . . "selemates" . . read as the word . . "soulmates" . . for a short instant in my visual perspective . . -_- . . well . . uh . . it's . . quite a cool word . . "selemates" -_- . . I'm still not really sure what the word should mean . . at this time . . hmm . . as it relates to the word . . "soulmates" . . I'm really not sure . . that was a quick . . spontaneous . . uh . . surprising event . . and I'm not really sure . . uh . . what to think about this topic . . -_- . . well . . uh . . selemates . . uh . . hmm . . sele . . I'm not really sure what sele . . is relating to . . mates . . uh  . well . . this could uh . . be uh . . something related to uh . . a group of people . . or something uh . . something like that . . a group of people . . or a partnership . . or something like this . . and yet . . sele . . sele seems like it's a word that relates to . . selenium . . and when I think of selenium . . I think of the word . . metal . . or I think of . . uh . . schyillithis . . or . . uh . . schyillius . . a word that is like a metal worm . . like an eldrazi from the game . . "Magic the Gathering" . . something like . . a worm that is flexible and mobile but it is . . made of metal . . and so it's like a lizard that's fast and . . mobile but is artificial in the sense that it looks like its something that came from a laboratory somewhere . . or something like this . . metallic . . metal . . slug . . uh . . fast . . metallic slug . . fast . . metal worm . . I'm reminded of a . . centipede . . that is made of metal . . with the many . . layers or many . . uh . . rungs . . or many . . segments . . ]



