


# Day Journal Entry - November 15, 2020


[Written @ 18:49]

Notes @ Newly Discovered Videos


[1.1]
Science of Focus - Pravrajika Divyanandaprana
By VivekaVani
https://www.youtube.com/watch?v=tEK6wIanmgY

[2.1]
The Gateway Experience Wave 1 Discovery orientation
By Hemi-sync - Topic
https://www.youtube.com/watch?v=uqlaRHyZJqU

[3.1]
Futurist Conference 2020
By Futurist Conference
https://www.youtube.com/watch?v=dgc4iwT4KEg




[Written @ 18:32]


Notes @ Newly Learned Words

Pointed anger [2.0 @ 9:34]

Flash of anger [2.0 @ 9:41]

Magic dream [2.0 @ 7:38]

Relationship arc [2.0 @ 15:44]


Notes @ Newly Discovered Dream Journal Entry

"Pointed Anger Dream" [self-suggested title by Jon Ide]
[By Unicole Unicron]
[Discovered on November 15, 2020]
[2.0 @ 7:38 - 10:41]

"Relationship Arc with Elon Musk" [self-suggested title by Jon Ide]
[By Unicole Unicron]
[Discovered on November 15, 2020]
[2.0 @ 15:26 - 17:52]


[Written @ 17:21]

Notes @ Newly Learned Words

Food Haul

Grocery Haul
[3.0 @ Video Title]


[Written @ 16:40]

Timeline Annotation:

[0:00:00 - 4:45:30]

- I watched the live stream video recording for . . [1.0]

- I took a nap . . I was tired still from having woken up to start the live stream . . at 12:00 . . also I'm not sure what to do at this time . . uh . . maybe I should have worked more . . but it does feel like there are so many things to do . . uh . . and I was anxious and nervous and not sure how to organize those things to do . . in a sequence that makes sense . . or that uh . . is maybe uh . . something easy to take care of . . very quickly or something like that . . I was nervous and so instead decided to take a nap . . and not do anything . . and so I layed on the couch in the living room . . and I haven't eaten breakfast yet . . I'm sorry . . for the data usage or something like that . . I know . . maybe it's not that useful to use so much data . . uh . . on the generosity of YouTube's ability to support that storage . . at this time . . well . . uh . . something like that . . I'm sorry . . 

- I'm preparing to watch . . [2.0] . . 

- I am ideating the possibility of getting some food to eat before starting to watch . . [2.0] . . 

[4:45:30 - 5:13:13]

- Beight back . . I'm getting some food to eat . . before starting to watch the video . . [2.0] . . be right back . . 

- I made . . an egg omelette . . with . . colby jack cheese . . and spinach . . 

[5:13:13 - 5:29:44]

- I am preparing to eat food . . 1 egg omelette . . 2 bananas . . and 1 glass of orange juice . . 

- I like to eat mayonaisse sometimes . . and so maybe I'll eat the egg omelette with bread . . and mayo . . for the time being . . I have . . white bread . . at my desk . . and I suppose the idea is to . . place . . the egg omelette . . or maybe . . half the egg omelette . . inside of the layer . . between 2 slices of bread . . like an egg omelette sandwich . . with mayonaisse . . and so . . by having . . 2 halves . . of the . . egg omelette . . I can have . . 2 separate sandwiches . . which is 4 . . slices of bread . . in total . . for the sandwiches . . to eat . . something . . like . . that . . 

- As I write about . . things that I'm eating . . I am reminded of . . food haul videos . . where people share the things . . that they bought . . to eat from the grocery store . . or things that they have prepared themselves to eat for the day . . maybe a shoutout to a few of the food haul channel videos that I've seen . . [3.0] . . [4.0] . . [5.0] . . [6.0] . . [7.0] . . [8.0] . . [9.0]

[5:29:44 - 5:37:36]

- Preparing the egg omelette sandwiches . . be right back . . 

- Finished preparing the egg omelette sandwiches . . with . . mayonaisse . . I made 2 of the sandwiches . . as I had . . initially . . intended . . 

- Preparing to watch . . [2.0]

[5:37:36 - 6:36:29]

- I watched . . [2.0]

[6:36:29 - 6:37:52]

- I just discovered that the live stream video recording . . stopped . . recording . . at . . 1 hour . . 28 minutes . . and 12 seconds . . oops . . I guess . . I should have . . checked the live stream . . more frequently . . to make sure . . the live stream . . was still happening . . I'm sorry . . 

[6:37:52 - 7:10:42]

- I'm taking notes on . . things . . I learned . . from [2.0] . . be . . right . . back . . 

[7:10:42 - 7:22:30]

- I'm closing the live stream for today . . thanks for watching . . -_| . . 

References:

[1.0]
CAM CHURCH- TRANSMUTATION
By Unicole Unicron
https://www.youtube.com/watch?v=cKvcQpPrjY4

[2.0]
Fantasy, Sex, and Love Addiction Recovery Vlog (Day 15)
By Unicole Unicron
https://www.youtube.com/watch?v=7njTEWrRhb8

[3.0]
Diet, training, Grocery Haul..... Cutting Life !
By Rosie Burr
https://www.youtube.com/watch?v=XsbdBCBQ0wg
[Synposis . . Presenting food they purchased from the grocery store . . grocery haul video . . ]

[4.0]
EXTREME CLOSET ORGANIZATION *satisfying* | VLOG
By Claudia Walsh
https://www.youtube.com/watch?v=XO8tlvTBUNQ
[Synopsis . . Eating food at home . . Preparing food . . cooking . . ]

[5.0]
Full Day of eating1900cals Fitness model diet
By Rosie Burr
https://www.youtube.com/watch?v=VPNoL5zaNwQ
[Synposis . . Eating food from home . . Preparing food . . cooking . . ]

[6.0]
Spaghetti & Meatball Bolognese Recipe (Better than Gordon Ramsay?)
By Crypto Casey
https://www.youtube.com/watch?v=f0OHE5bVMRI
[Synposis . . Eating food from home . . Preparing food . . cooking . . ]

[7.0]
Trying the New McDonalds Spicy McNuggets for the First Time
By ZachRawrr
https://www.youtube.com/watch?v=oHCYVrD3s_g
[Synposis . . Eating food from restaurants . . ]

[8.0]
Fast Food #1 Tier List
By penguinz0
https://www.youtube.com/watch?v=kI9oeo920LE
[Synposis . . Eating food from restaurants . . ]

[9.0]
Isabel Paige
https://www.youtube.com/c/IsabelPaige/videos
[YouTube Channel]
[Synopsis . . Eating food at home . . Preparing food . . cooking . . ]

[10.0]
ESCAPE TO THE WILDERNESS: DIY BRICK PIZZA OVEN | Cooking Pizza in a Homemade Clay Cob Oven - Ep. 112
By Living Off Grid w/ Jake & Nicole
https://www.youtube.com/watch?v=2WA9T9-5nds
[Synposis . . Eating food at home . . Preparing food . . cooking . . pizza oven . . ]


[Written @ 16:34]


Notes @ Notes Taken From the Unicult . . Cam Church . . Teaching . . or . . Episode . . "TRANSUMTATION" . . published on November 15, 2020 . . 


[0:00:00 - 0:20:00]


Notes @ Newly Learned Words

spiritual bypassing

(1) stop using plastics
(2) humans singing is in alignment with their best wishes at this time . . something like that . .


[0:22:00 - 0:39:00]


(Notes @ Newly Learned Words)

shadow healer . .

(Notes @ Newly Learned Acronym)

A.R.I.S.E.

(A - Assure you are safe)
(R - Realize your emotions are valid.)
(I - Investigate the root cause)
(S - See how the pain is serving you)
(E - Embrace Something Higher) . . 


(Notes @ Newly Remembered Words)

agoraphobia

(Notes @ Newly Learned Words)

Reaize . .

[0:39:00 - End of Video]

hope . . 
satisfaction . .
trust . .

. . . 

near constant joy . .
radical love . .