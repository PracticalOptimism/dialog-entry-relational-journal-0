

# Day Journal Entry - December 5, 2020

[Written @ 21:11]


Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:09:57]

- 📝 [commentary] Greetings

[0:09:57 - 0:15:03]

- 📝 [commentary] I'm intending to watch videos of Isabel Paige . . hannahleeduggan . . Jonna Jinton . . and UNICULT . . on YouTube . . I am not really in the high spirit to do programming related things . . or to do planning related things . . and sometimes . . this is what I do to relax . . and not . . think about things so much . . Also . . I woke up from a nap earlier . . uh . . today . . in uh . . 1 hour and 30 minutes ago . . -_- . . -_- . . I will also be thinking about the project here and there as the thoughts are also . . uh . . interesting me . . but also . . uh . . I do have problems to solve . . that maybe I need to overcome . . and relaxing . . helps me . . overcome those problems and challenges . . uh . . in a kind way where the solution is . . uh . . practical for aesthetics . . as well as maybe practical for . . uh . . engineering related achievements . . 

[0:15:03 - 4:48:29]

- 📝 [commentary] I watched videos [1.0] [2.0] [3.0] [4.0] [5.0] [6.0] [7.0] [8.0] [9.0] [10.0] [11.0]

[4:48:29 - 5:16:58]

- 📝 [commentary] I watched video [12.0]

[5:16:58 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
A Journey To Healing (Story 22)
By Isabel Paige
https://www.youtube.com/watch?v=KxN37S3Qmy0

[2.0]
What is UNICULT Practice? How can I Participate?
By UNICULT
https://www.youtube.com/watch?v=pGFUkoO9lfk

[3.0]
Oops, I lied to you a little 🤓
By hannahleeduggan
https://www.youtube.com/watch?v=GfEJri4ThI0

[4.0]
Echoes of the North - Ice obsessions - Story 49
By Jonna Jinton
https://www.youtube.com/watch?v=gpJaQIpJx1A

[5.0]
Building A Tiny House In The Mountains - Start to Finish (Story 57)
By Isabel Paige
https://www.youtube.com/watch?v=nFYmlQ3_DnY

[6.0]
A Day Of My life In The Mountains (story 58)
By Isabel Paige
https://www.youtube.com/watch?v=NPak1xKe4_0

[7.0]
Blippi Decorates The Christmas Tree | Educational Videos For Kids
By Blippi - Educational Videos for Kids
https://www.youtube.com/watch?v=pJ7d_HKqqT8

[8.0]
Learning Fall Colors With Blippi | Art Videos For Children | Educational Videos For Kids
By Blippi - Educational Videos for Kids
https://www.youtube.com/watch?v=JpbnNpfeeOo

[9.0]
Vlogmas Day 2
By Devan Christine
https://www.youtube.com/watch?v=Zf_3KMt-BLc

[10.0]
What You're Meant For (Career) (Psychic Reading / PICK A CARD)
By The Gem Goddess
https://www.youtube.com/watch?v=e9iiPBEezZI

[11.0]
How To Start Your Spiritual Journey✨☾EASY Tips & Tricks For Getting Into Spirituality FAST✨🧘‍♀️🙏📿
By Vanessa Somuayina
https://www.youtube.com/watch?v=dcTYAxeHc9A

[12.0]
You are loved. 434.
By ꞍƎꞍ
https://www.youtube.com/watch?v=wj57uIogAS8



