


# Day Journal Entry - December 7, 2020

[Written @ 23:38]


Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:41:22]

- 📝 [commentary] I took some notes . . and prepared to write . . in my . . dream journal . . for the dream that I had . . while I was . . taking . . a . . nap . . during the day . . on . . December 7, 2020 . . 

[0:43:22 - 4:02:02]

- 📝 [commentary] I am watching . . Isabel Paige's . . latest . . video on YouTube [1.0] . . while . . eating . . rice . . and . . 2 bananas . . and 2 . . peanut butter and jelly sandwiches . . and drinking . . some water mixed with . . a . . low calorie . . drink mix . . "Wyler's Light" . . 

[4:02:02 - 4:59:48]

- 📝 [commentary] I am taking notes in my dream journal . . for . . the dream . . that I had . . on . . December 7, 2020 . . while . . taking a nap . . during the day . . 

[4:59:48 - End of Video]

- 📝 [commentary] I watched . . Unicole Unicron's . . CAM CHURCH . . for . . Unicult . . [2.0]

References:

[1.0]
Winter Is Here - Living In The Mountains (Story 59)
By Isabel Paige
https://www.youtube.com/watch?v=nXVuNMjXyLM

[2.0]
CAM CHURCH - TIME and DEDICATION
By Unicole Unicron
https://www.youtube.com/watch?v=02jRd_WLH7I




[Written @ 23:01]

Notes @ Newly Discovered Genre of Videos

* Note Taking ASMR Video
[Note Taking ASMR Video . . Taking Notes . . in a physical . . paper material . . journal . . notebook . . is quite a cool aesthetic . . that I was looking for . . to . . uh . . showcase . . while . . taking notes . . in my dream-art science journal notebook . . or in my dream journal . . notebook . . on . . my computer . . uh . . I like the asethetic of . . writing . . using a pencil and paper . . the imagery . . looks quite cool . . and . . I myself . . personally . . enjoy taking notes . . in my physical paper . . material journal notebook . . for things . . uh . . including . . notes on projects . . I'm working on . . or . . notes on . . dreams that I've had . . uh . . while sleeping . . dreams that I had while sleeping . . uh . . and so . . while searching for this . . uh . . "Note Taking ASMR Video" . . uh . . genre . . I wasn't really sure . . if I would find anything . . uh . . this has been my first time . . searching for this . . uh . . because . . uh . . well . . I guess . . I was anticipating . . uh . . taking notes in my dream journal . . uh . . when I . . uh . . started the live stream . . or a few moments after I started the live stream . . uh . . well . . I'm happy to have found . . [1.1] [2.1] [3.1] . . have the video aesthetic that I was aiming for . . I uh . . wouldn't mind seeing a human hand . . in the video . . a human hand . . that is taking notes . . using a pencil or uh . . a writing utencil . . uh . . or uh . . something like that . . a writing utensil . . uh . . like a pen . . or a pencil . . and . . the person . . or the hand . . taking notes . . in the journal notebook . . uh . . using the pencil I guess . . uh . . well . . uh . . in my mind's . . eye . . or . . in my mental vision . . I had initially . . had . . an image . . or an expectation definition . . to see something like . . a . . old wooden desk . . and . . the lighting is . . like . . uh . . I guess . . uh . . dim lighting . . or . . uh . . sephia . . style lighting where the light isn't white-bright . . or bright-white . . in coloration . . but rather a dimmed town of . . a charcoal . . uh . . a charcoal . . yellow . . or something like that . . uh . . where the light appears like you're in an old wodden . . hotel . . or . . in a fancy mansion . . that has . . a fancy . . yellow . . wooden color . . aesthetic . . or something like that . . like a Harry Potter . . theme . . uh . . with . . yellow-ish . . light color . . chandaliers . . or something like that . . uh . . well . . uh . . it seems that . . [3.1] satisfies what my expectations were . . what my initial expectations were in my mind . . of what I could uh . . want to see or what I was . . uh . . what the . . uh . . uh . . desired aesthetic was . . at the time that I had the original thought to search for the . . term . . "taking notes in a diary asmr" . . uh . . [2.1] . . the second half of . . [2.1] . . also has the . . light . . aesthetic . . of the . . yellowish . . charcoal yellow . . warm white . . uh . . [1.1] . . appears to have . . a . . cool white . . tone . . ]




Notes @ Newly Discovered Videos

[1.1]
ASMR Crinkly notebook journaling (no talking) writing with pen 🖊
By Rebecca's Beautiful ASMR Addiction
https://www.youtube.com/watch?v=v4iM0lEwmyg
[Discovered by YouTube Search Result for "taking notes in a diary asmr" . . Search Result Index . . number . . 4 . . [Search Result Index Count Method: Start from the top of the search result list . . the first item is . . 1 . . and . . increment the index number . . as you move down the video list . . to see the next video item . . the number . . achieved on the video that was the . . stopping point . . is the index number . . for the video . . ]]

[2.1]
*ASMR* Studying and taking notes – Part 1 [No talking]
By ves. asmr
https://www.youtube.com/watch?v=pM-_NUMjx3A
[Discovered by YouTube Search Result for "taking notes in a diary asmr" . . Search Result Index . . number . . 5 . . [Search Result Index Count Method: Start from the top of the ist, the first item is 1 . . and increment the index number . . as you move down the video list to see the next video item . . ]]

[3.1]
| ASMR | ✍ #1 - * Journal Entry in Cursive Writing * {{ Dreamy Candle Light }}
By Boheme and Chella ASMR
https://www.youtube.com/watch?v=iVOCvxKNXp8
[Discovered by YouTube Search Result for "taking notes in a diary asmr" . . Search Result Index . . number . . 6 . . [Search Result Index Count Method: Start from the top of the list, the first item is 1 . . and increment the index number . . as you move to see the next video item . . ]]



[Written @ 1:09]



Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 2:13:56]

- 📝 [commentary] I took a shower . . while listening to . . [1.0] [2.0]

- 📝 [commentary] I watched [3.0] to promote Amy Maricle's artwork

[2:13:56 - 3:33:56]

- 📝 [commentary] I watched . . [4.0] . . while . . taking notes in realtime . . to practice learning while taking realtime notes . . as well as to promote the art journaling of Amy Maricle

[3:33:56 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
with maya :)
By Shimmey Shay
https://www.youtube.com/watch?v=w5k83uJWEbA

[2.0]
The Joe Rogan Experience #1572 - Moxie Marlinspike
By PowerfulJRE 12
https://www.youtube.com/watch?v=_k_Mg_-RQbU

[3.0]
Paper Cut Journal Tour
By Amy Maricle
https://www.youtube.com/watch?v=S0c7BCbQPY4

[4.0]
Art Journaling Basics: Live Tutorial
By Amy Maricle
https://www.youtube.com/watch?v=Xu6EjzTo_2c





