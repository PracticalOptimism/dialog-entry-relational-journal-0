

# Day Journal Entry - December 11, 2020


[Written @ 2:38]

Notes @ Newly Watched Videos

[1.1]
how i keep my energetic channel clear
By Hitomi Mochizuki
https://www.youtube.com/watch?v=0-4m1dC4Flk

[2.1]
holiday decorating; cabin style 🎄🏠
By hannahleeduggan
https://www.youtube.com/watch?v=LiSin1N4Ilw




[Written @ 2:30]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - End of Video]

- 📝 [commentary] I watched [1.0] [2.0] [3.0]

References:

[1.0]
Vlogmas 5: Weekly Reading
By Devan Christine
https://www.youtube.com/watch?v=aAVyrGLCeeE

[2.0]
Vlogmas Day 4
By Devan Christine
https://www.youtube.com/watch?v=al_eU3jCFbw

[3.0]
Is synchronicity real? It is. Here's proof.
By ꞍƎꞍ
https://www.youtube.com/watch?v=d7aDKIOz5nU




[Written @ 1:25]

Notes @ Newly Created Words and Expressions

serendipitous joyful discovery
[Written @ December 11, 2020 @ 1:46]
[I discovered . . something . . new . . and it was fun and exciting to witness]
[It seems like this word . . could . . be related to . . synchronicity . . events . . or synchronicity experiences . . that reminds me of the YouTube channel . . 434 . . that . . often times talks about their experience with seeing the numbers . . 4 . . 3 . . 4 . . in all sorts of places in their lifehood experience . . [3.0] . . the numbers . . 11:11 . . are also a sign of synchronicity experiences]

talkine
[Written @ December 11, 2020 @ 1:34]
[I accidentally typed the word . . "talkine" . . instead of typing the word . . "talked" . . I'm not . . sure what this word . . "talkine" . . should mean . . at this time . . I'm sorry . . ]

accidenntal
[Written @ December 11, 2020 @ 1:35]
[I accidentally typed the word . . "accidenntal" . . instead of . . typing the word . . "accidentally" . . I'm not sure . . what this word . . "accidenntal" . . should mean . . at this time . . I'm . . sorry . . ]

Notes @ Newly Discovered Similar Sounding Words

Tik Tok
Ted Talk [1.1]
Tech Talk [2.1]

[1.1]
[This thought was inspired by . . [1.0 @ 19:15] . . I thought I heard . . the speaker . . say . . "I did a Ted Talk" . . on that . . at first . . and then . . realized . . that they . . had . . previously . . talked . . about . . "Tik Tok" . . and so . . I think . . uh . . by this evidence . . that . . they've mentioned . . "Tik Tok" . . before . . in one of their . . earlier . . videos . . and . . maybe also evidenced . . by the . . video transcript . . that . . reads . . "tick tock" . . then . . I'm starting to believe . . that . . "Tik Tok" . . is the development of events . . that was intended]

[2.1]
[I read the video transcript . . description . . for . . [2.0] . . and read the words . . "tech talk" . . which inspired me to start thinking that . . "hmm . . this word phrase . . 'tech talk' . . could also be in the category . . of similar sounding words . . with . . 'tik tok' . . 'ted talk' . . and . . 'tech talk' . . so cool . . what a discovery :) . . this discovery . . reminds me . . of the discovery . . that . . Devan Christine . . made . . in . . [2.0 @ 7:54 - 8:08] . . serendipitous joyful discovery . . :)]


[1.0]
Vlogmas 5: Weekly Reading
By Devan Christine
https://www.youtube.com/watch?v=aAVyrGLCeeE

[2.0]
Vlogmas Day 4
By Devan Christine
https://www.youtube.com/watch?v=al_eU3jCFbw

