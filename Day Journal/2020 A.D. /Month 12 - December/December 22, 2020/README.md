


# Day Journal Entry - December 22, 2020


[Written @ 4:47]

Notes @ Newly Discovered Music Series

[1.0]
" YOGA DANCE " TAO ECSTATIC DANCE SESSION
By TAO ECSTATIC DANCE
https://www.youtube.com/watch?v=6UuyjUDmSbs
[Discovered on the YouTube Home Page . . A video recommendation . . ]
[I thought to click on this YouTube video because I thought the cover art looks interesting . . it reminds me of . . "Shpongle's" . . cover art . . I like . . to think . . there are more . . musicians out there like . . "Shpongle" . . and although these days . . in my work with Ecoin . . I rarely listen to transcedental . . melody music . . like . . Shpongle . . because I feel like that type of music makes me feel . . uh . . weird . . and strange . . and esoteric . . like . . uh . . I'm uh . . hallucinating . . or maybe uh . . I'm uh . . -_- . . well the music is really magical . . but when I'm working on things . . I don't always feel like magical feelings are how I want to feel . . or uh . . well . . really . . it's like . . I don't know . . uh . . normally . . I'm used to . . uh . . listening to magical music in the background when I'm playing a video game . . or . . doing relaxing activities . . where I don't need to think so much about how to plan things or how things should be going . . uh . . or something like that . . but I'm not sure . . magical music also makes me think . . a lot about the nature of reality . . and uh . . sometimes the thoughts aren't even real thoughts . . they're just hazy . . day dreams . . and I'll have an adventure . . in lovely dreams . . or something like that -_- . . that's probably not really the impression that I wanted to give in writing about this . . uh . . for example . . uh . . I would like to maybe describe more about the  . general practical experiences that I had . . but uh . . also having a summary could possibly help . . but an example of a general practical experience . . that I used to have when listening to "Shpongle" . . "Ott" . . "Kaminanda" . . and other instrumental musicians . . in this area of reality on planet Earth . . as I have known it in the past . . uh . . in the year . . uh . . 2017 . . uh . . plus or minus . . 2 years . . uh . . I listened to psychedelic trance music . . and would listen to this type of music . . (1) when I would go to bed . . and hope that the music would enter my dreams as well and so . . my dream landscape can be shaped by the musical intention of the musical art piece . . or something like that . . (2) when I was relaxing with friends in a hottub . . and we would listen to this type of music . . and trance out . . as a possible alternative to drugs . . since our state didn't have legal rights for marijuana posession . . or other types of drugs . . or something like that . . uh . . also . . we can . . uh . . relax . . and uh . . enjoy the pool of the water . . uh . . while closing our eyes and . . psyching out . . in the dark . . at night . . while sitting in a hot tub . . or something . . (3) When I trance out by myself . . uh . . something like that . . if I'm in the mood . . for . . being in a tranced state . . of consciousness . . this type of music . . is . . heavenly fire . . ]


