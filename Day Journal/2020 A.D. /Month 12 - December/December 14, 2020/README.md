


# Day Journal Entry - December 14, 2020

[Written @ 6:56]

Notes @ Newly Created Words

Botanical Awareness Therapy
[Written @ December 14, 2020 @ 6:56]
[I was looking at Krystle Cole's . . art shop . . on the internet . . and discovered . . a . . painting . . titled . . "Botanical Awareness Painting" . . I had the thought to myself . . "Hmm . . that's quite interesting" . . and then I thought to myself . . through a spontaneous word imagination . . cloud . . the words . . "Botanical Awareness Therapy" . . this was spontaneous inspiration . . that encouraged me to write down this word . . "Botanical Awareness Therapy" . . When I think about the meaning of this word . . I hmm . . imagine . . the . . awareness of . . beautiful things . . to be a therapeutic . . process . . or something like that . . and so . . uh . . maybe a possible interpretation for this meaning of this word could be . . being attentive . . to the beautiful things . . in life . . and that could be . . a way . . to heal . . or to . . relax . . or . . to . . gain . . awareness in healing practices that therapeutize your mind . . or something like that . . ]



[Written @ 5:02]

Gitlab Shop
https://shop.gitlab.com/

Vuetify Store
https://store.vuetifyjs.com/

Moods Clothing
https://moodsclothing.com/

Teddy Fresh
https://teddyfresh.com/

Eugenia Cooney Shop
https://eugeniacooneyshop.com/

The Venus Project
https://www.thevenusproject.com/store/

The Venus Project Store
https://teespring.com/stores/the-venus-project

Nasty Fit
https://nastyfit.com/

The Gem Goddess
https://thegemgoddess.com/

SciBugs
https://www.etsy.com/shop/scibugscollections/

UnicultSupplyCo
https://www.etsy.com/shop/UnicultSupplyCo

Akiane Gallery
https://akiane.com/

kristasangels
https://www.etsy.com/shop/kristasangels

Hack Club
https://hackclub.com/

Bashar Store
https://www.basharstore.com/

Theraphi
https://theraphi.net/

Seth Center
https://sethcenter.com/

United Nuclear
https://unitednuclear.com/

OpenBCI
https://shop.openbci.com/collections/frontpage

Equilinox
https://www.equilinox.com/

Beau Life
https://www.beau-life.com/

White Sky Boutique
https://devanchristine.com/

Clara Dao
https://www.claradao.com/

Wonderland Home
https://wonderlandhome.shop/

Parachute Bras
https://parachutebras.my-free.website

Softology
https://softology.com.au/index.htm

SleepyE
https://www.redbubble.com/people/SleepyE/shop

Krystle Cole Fine Art
https://www.krystlecole.com/

ShaShaeile
http://shaeile.shop.blogpay.co.kr/


[Written @ 5:00]

Maybe a brain computer interface would allow you to control a new arm if you wanted another arm . . or if your existing arms are in need of support . . Line Following Robots could be useful for building a robotics translocation information system for an entire city . . You can communicate packages and other material goods or product resources to any member of the city by building a robotics highway with line following robots

. . . 

I'm going to cook some food . . to eat . . be right back . . I'm intending on . . cooking . . an egg . . and . . microwaved ramen noodles. . 



[Written @ 2:44]

I was doing an exercise . . with . . [1.2] . . 

I was moving the mouse cursor

To reflect the pattern on the screen . . of the computer

As the video . . was playing . . I would move the mouse cursor to reflect . . the pattern for the frame of the video . . or for the video segment . . or video area . . in that group of frames . .

I started to . . move the mouse cursor . . to . . reflect . . the patterns of the video . . that I had seen before . . in the video . . but . . in . . a way . . that was . . not necessarily reflecting the pattern . . associated . . for that particular frame in the video . . 

My anticipation grew . . I . . started anticipating . . when . . to . . move . . to the next . . pattern . . and . . it felt like . . my anticipation . . started to take control . . of my movements . . after I felt comfortable . . with the memorized . . hand movements . . that I was making with the mouse pad . . to move . . the mouse . . cursor . . on my . . MacBook Pro . . computer . .

I then got the idea . . that maybe . . reality . . my experience in everday life . . is possibly . . my anticipation . . of what is . . going to be experienced . . and so for example . . I'll anticipate . . events like . . -_- . . I odn't know I haven't really thought of a specific example . . But I got the idea . . that . . if there is anticipation . . then maybe . . are we . . or am i . . personally I guess . . falling into . . anticipation . . or am I . . being led . . into those things that I have come into anticipating . . it . . uh . . reminds me of . . maybe . . opening . . an expectation . . doorway . . and . . maybe . . uh . . if I'm uh . . comfortable . . where I am . . I can . . uh . . -_- . . hmm . . I don't know how to describe what I'm thinking about . . I think I'm failing at doing it right now . . I'm uh . . interested in the idea of . . anticipation . . however . . since . . personally . . I'm working on a project . . that seems to relate to computers being able to anticipate . . uh . . information . . and in anticipation . . or expecting . . they are able to perhaps . . better prepare . . for performing in a particular way . . and so for example . . that could be useful for information processing . . uh . . in uh . . maybe . . a modern . . analogue . . to this sort of idea . . is the V8 . . JavaScript Egine . . which . . builds . . a sort of . . uh . . track of . . which . . javascript . . functions . . are being . . uh . . evaluated . . uh . . or  . . operated . . or . . uh . . performed . . or something like that . . uh . . javascript functions . . or javascript uh . . uh . . lines of code . . or uh . . javascript . . uh . . operations . . or something like that . . uh . . the javascript uh . . codebase representation is . . transpailed . . to . . machine code that the computer can understand . . and that is uh . . able to do things like . . render . . graphics . . to the computer . . and . . uh . . respond . . to events . . being . . uh . . captured . . by the computer . . uh . . maybe . . like . . mouse . . click . . events . . uh . . I'm not exactly sure if this last example of mouse click event listening is a good example . . of uh . . well . . it seems like a good example . . the idea . . is that you have . . a javascript function that is performed . . when . . a mouse click event is captured . . by the browser . . uh . . and the browser . . uh . . for example . . Google Chrome . . uses the V8 . . JavaScript Engine . . to . . uh . . maybe . . uh . . speed up . . the performance . . of the javascript . . function that . . you've specified . . to respond . . to mouse . . click events . . and so . . uh . . I guess . . if your . . javascript function is . . performed . . often enough . . maybe . . if the javascript function is performed . . 100s . . of times . . or maybe . . 1000s . . of times . . uh . . the . . uh . . function will . . take . . shorter . . time . . to complete . . its operations . . or the function will take . . less time . . to perform . . its . . operations . . uh . . uh . . operations uh . . like . . uh . . variable declarations . . and uh . . network requests to a database . . or something like that . . uh . . or uh . . conditional statement checks (if-else statements, case-switch statements, etc) . . or for-loops . . or while-loops . . uh . . uh . . something like that . . uh . . well uh . . if your function is called . . 1000s times . . then the V8 JavaScript engine . . will . . uh . . memorize . . or uh . . I guess . . 'caching' or . . 'memoizing'[not to be mistaken for memorizing with an 'r' . . which is spelled similarly . . but I think they're different terms . . :O] . . something like that . . the V8 JavaScript Engine . . memoizes . . variables . . and uh . . -_- . . to be honest I don't really know what it does so much -_- . . it does something . . -_- . . 'caching' . . 'memoizing' .  -_- . . which sounds like . . storing data in a . . table somewhere . . -_- . . in memory or on disk . . -_- . . in the computer . . Random-Access Memory (or acronymed as R.A.M.) or on . . the hard drive uh . . or uh . . a hard drive disk or a uh . . permanent memory storage system like a pencil-and-paper-notebook xD . . lol just ki d d d d d d d d d ding -_- . . -_- . . pencil-and-paper is a computer however xD . . if you want to learn to program that is a good tool . . and it is maybe . . a few dollars at the dollar store . . to write down . . your computer program . . you can write a wish list . . for things you want to purchase from the gitlab store page for . . supporting the gitlab project :O . . 

[Written @ 2:01]

Notes @ Random Thoughts

"You're a 4:30-arian" . . 


[Written @ 1:47]

Notes @ Newly Discovered Quotes

"
Thought
Cognition
Planning
Caring
Emotion
Communication
Intentionality
Focus
and
Authenticity

. . are the domains . . that we have to be in . . in order . . to carry . . this . . off . . 

You wanted challenge ? . . Some of you have made . . fortunes . . that was . . no . . challenge . . 

You wanted a challenge ? ? ? . . 

Save the planet . .

There's a challenge . . That's what everybody . . who came before . . did . .

They participated . . sufficiently . . at . . least . . to pass . . the buck . . to us . . 

Can we s- . . participate . . sufficiently . . to pass . . the buck . . to our . . grand children . . or . . are we just going . . to go down . . as the lamest . . generation . . in the life . . of the planet . . ? . . 

I can't believe it . . 

I know too many of these people . . I love . . too . . many . . of . . these . . people . . 

We just need . . to . . do . . our . . best . . 

As the Grateful Dead . . say . . you know . . 'Just a little bit harder . . Just a little bit . . More . . You can't go back . . and you can't . . stand . . still . . If the thunder . . don't get ya . . then . . the light . . men . . will . . '

So . . forward . . into . . the . . future . . 

With . . a . . high . . heart . . and . . reasonable hope . . that we can . . make . . this planet . . into . .

A living . . reflection . . of the inner . . dream . . 
"
-- Terence McKenna
[1.1 @ 2:58 - 4:30]




[1.1]
Terence McKenna - Ride The Tao
By We Plants Are Happy Plants
https://www.youtube.com/watch?v=0Bhiy0797qo


[Written @ 1:27]

Timeline Annotation:


[0:00:00 - 5:01:51]

- I watched videos

[1.0]

[2.0]

[3.0]

[4.0]

[5.0]

[6.0]

[7.0]

[8.0]



'Ode to Ishtar' - Hugo The Poet
By Hugo The Poet
https://www.youtube.com/watch?v=7MN1bXRfSGw

Hugo & Soup "We Are the Humans" Sci--fi parody MV
By Hugo The Poet
https://www.youtube.com/watch?v=ljupQ9bi1UE

The Bhaktas - Shiva Shambho (Suduaya Remix)
By Arthur Moore
https://www.youtube.com/watch?v=H4enld7098k

Just Love
By Arthur Moore
https://www.youtube.com/watch?v=trg1zRVt7RQ

Lost || After The Crash In Real Time
By Dharmaville
https://www.youtube.com/watch?v=m7sYXAI1AGg

LOST For Words #3
By Dharmaville
https://www.youtube.com/watch?v=vLD7I_Y_npo

LOST For Words #2
By Dharmaville
https://www.youtube.com/watch?v=gUnLVs6jf9w

LOST For Words #1
By Dharmaville
https://www.youtube.com/watch?v=NNRX6Kuw9OY

Terence McKenna - Ride The Tao
By We Plants Are Happy Plants
https://www.youtube.com/watch?v=0Bhiy0797qo

Terence McKenna - You Could Change Your Reality
By We Plants Are Happy Plants
https://www.youtube.com/watch?v=LUH-0iFJbYs


AMDMA
By TresEscrotos
https://www.youtube.com/watch?v=UEFEI9P0qHQ
[Discovered by a telepathic transmition where I read the letters "amdma" in my mind . . and typed it . . into the YouTube . . Search Engine . . and Discovered this video . . -_- . . Video . . Search Result Number [4, 5, 5] (count results sequence) . . 5 . . is the latest result according to applying the start-from-the-top-and-count-down-by-one-and-skip-the-playlists-and-channels-counting-method . . in applying a second method for counting videos . .  . . [Counting only videos without the video playlists and the video channels . . Search Result . . number 8 . . including . . the . . video playlists . . and the video channels . . ]]

7, 6, 5, 4 . . the video is item 4 . . 

The first video in the sequence is item . . 0 . . 

and so . . shifting 0 to 1 . . and . . shifting . . 4 . . to 5 . . 5 seems to be the result of the . . first . . random count method . . result . . for counting . . the order of the . . video number . . in the youtube search result list . . something like that . . 

Let's try the random count method again : 

(1) select a video at random in the list
(2) give the video a random whole number (-Infinity etc. etc. . . -10, -9 . . 0 1 2 . . 9 10 . . etc. . . etc . . +Infinity)
(3) move in the direction of the video of interest
(4) record the number for the video of interest
(5) move in the direction of the video starting place of interest (ie. at the top of the list)
(6) record the number for the starting place of interest
(7) evaluate the expression: X + starting place of interest number = 1
(8) add X to . . the video of interest number
(9) the video of interest has an order from the starting place of interest :D

video order number = X + video of interest number


. . . 

Let's try . . . X = -10 . . where should we start ? I'm not sure -_- . . 

-6 . . for the video of interest number . . 

hmm . . 

X + (-6) = 
(-10) + (-6) = -16

. . . 

-_- . . hmm

I was expecting a 5 -_- . . 

While developing this method . . I expected the result of . . video order number . . to be 5 . . 

I'm going to make some food to eat . . be right back . . 



[Written @ 1:26]


Notes @ Newly Discovered YouTube Channels

Dharmaville
https://www.youtube.com/c/Dharmaville/videos
[Discovered by [1.0 @ YouTube Channel]]

[1.0]
Lost || After The Crash In Real Time
By Dharmaville
https://www.youtube.com/watch?v=m7sYXAI1AGg
[Discovered by YouTube search for "lost airplane crash scene" . . YouTube search result list . . item number 3]



