

# Day Journal Entry - December 10, 2020



[Written @ 22:51]

Notes @ Newly Learned Words

Poop Talks
[Written @ December 10, 2020 @ 22:51 - 22:54]
[I am sitting on the toilet . . as I write this mssage . . and . . sometimes . . I think about things . . while . . sitting on the toilet . . and . . trying . . to . . poop . .]
[Written @ December 10, 2020 @ 22:54 - 23:02]
[I thought of the word phrase . . "poop talks" . . to describe . . a process . . where . . I could . . possibly . . not offend anyone by typing while I poop . . or by communicating while I poop . . which would or could otherwise . . possibly . . be offensive to the recipient of a message sent by someone who is writing as they poop on the toilet]
[Written @ December 10, 2020 @ 23:02]
[When I sit on the toilet . . and poop . . uh . . well . . I guess . . uh . . ]
[I'm finished pooping now . . I'm sorry . . uh . . I was going to say . . something like . . I uh . . try . . uh . . to uh . . not . . uh . . -_- . . uh . . I guess . . -_- . . --__---_- . . uh . . -_------- . . uh . . watching videos while pooping . . uh . . could uh . . -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . poop -_- . . is -_- . . uh . . not necessarily how I would like to associate . . with other people . . and so for example . . maybe . . the association to poop . . is not always an association that people would like to be made with . . and so for example . . the videos . . and the effort . . and energy spent in a video . . or the hardwork . . or the . . lifestyle or culture of the person . . could be . . something that maybe . . uh . . doesn't uh . . necessarily . . uh . . uh . . uh . . I guess . . uh . . maybe it's . . more . . uh . . respectful . . or something like that . . to . . uh . . not create . . the association in my mind . . that . . the message being received is at all related to being . . "poopy" . . or . . there's the other word . . which starts with . . "s" . . but I'm not a fan of the cursing relations of the term and so maybe it's offensive" . . . s*it . . sh*t . . should be a clue in how you spell the word . . uh . . -_- uh . . I'm not really sure . . it's just a convention . . I'm not really uh . . trying -_- . . to prosyletize . . anyone or to suggest that this is a thing that anyone else should consider -_- . . uh . . in trying not to watch people's work . . uh . . especially for example . . I feel that . . the work is possibly important . . to them . . and so relating the work to . . "poop" . . is . . uh . . not something that . . uh . . -_- . . it's just a religion . . I think it makes sense . . to some degree . . also . . maybe for example . . one benefit is that I can . . enjoy the pooping process . . and also . . enjoy . . uh . . try to achieve what I was on the toilet to achieve in the first place . . without degrading anyone's work subconsciously . . unless they designed it for poop artists to consume . .-_- . . -_- . . I'm sorry for introducing this language into your subconscious . . because maybe like me . . you didn't need to hear this message . . it was only a passing thought . . and then suddenly . . (1) no more using your cellphone to read news or listen to anyone's news posts . . while pooping . . (2) trying to respect the poop culture . . by searching poop related artifacts of society . . while pooping . .]

[Written @ 22:41]

Notes @ Newly Watched Videos

[1.1]


[Written @ 22:37]

Notes @ Quotes

"
Always find the joy in the journey
"
-- Devan Christine
[1.0 @ 8:01]


[Written @ 22:25]

Connecting is a concept applied in computer programming . . 

Re-connecting . . is an interesting topic . . also that could be considered in terms of . . computer programming relations . . such as connecting or re-connecting to a computer . . or . . connecting or re-connecting . . to . . a group . . or . . numerical symbols . . by the use of weighted values in a neural network . . or something like that . . 

Re-connecting with your passions . . uh . . this message . . in my understanding . . seems to uh . . be really uh . . uh . . cool . . in maybe how one could think about . . computer programs . . uh . . relating to artificial intelligence . . 

Re-connecting with your passions . . uh . . in terms of artificial intelligence . . could relate to the topic of . . relating the topic of observation to your own personal interests . . which could for example . . make the decision making process . . for a computer program . . more like a human decision making process . . uh . . since humans . . seem to . . relate to the world in a way that aligns with their interests . . 

Computers that have interests . . are able to perhaps . . focus on those interests that are also in line with . . human interests . . and so for example . . One could possibly . . develop a computer intelligence algorithm . . and yet . . the algorithm . . seems to . . replicate the interests of . . a . . squirrel . . or of . . an . . insect . . or of a rabbit . . or something like this . . and so . . uh . . although . . the computer program is intelligent . . with respect to the variety of other intelligent creatures that are out there in the world to be observed . . and considered . . in practice . . and in theory . . A human-interest-focused . . creature . . is able to relate to the interests . . of the human creature . . uh . . which is yet still possibly considerable as a . . uh . . group of human creatures . . or a group of human . . people . . that live in a particular . . era . . and are . . satisfied by certain constraints in their thinking . . or are satisfied . . by certain . . thought patterns . . being applied . . repeatedly . . in various circumstances . . or something like that . . 

Re-connecting with your passion . . Uh . . passion . . interest . . these are interesting terms to use . . and are really quite beautiful to read . . 

I like this quote . . "Reconnecting with your passion" . . because uh . . well . . in my own particular interests . . this uh . . terminology is quite familiar . . and uh . . well . . uh . . passion . . uh . . maybe isn't always a topic considered . . in . . computer programming related . . development practices . . to be . . uh . . a passionate . . computer program . . isn't always . . a comment that's uh . . familiar for me to hear . . at this time . . or something like that . . uh . . "that computer program is . . passionate" . . "that computer program is . . interested in achieving . . its goals . . " . .

Passion does seem like an important thing to have . . which seems to be . . something that . . Devan Christine . . talks about . . and shares . . with regard to . . spiritual topics . . and personal development . . 

[Written @ 22:06]

Notes @ Newly Learned Phrases That I Took Notes On Earlier In My Physical Personal Notebook And Am Also Writing Here

Reconnecting with your passions [1.0 @ 7:07]

Notes @ Newly Learned Poems

Dust If You Must

Dust if you must
But wouldn't it be better
To paint a picture or write a letter
Bake a cake or plant a seed
Ponder the difference between want and need

Dust if you must
But there's not much time
With rivers to swim
And mountains to climb
Music to hear
And books to read
Friends to cherish
And Life to lead

Dust if you must
But the world's out there
With the sun in your eyes and the wind in your hair
A flutter of snow
A shower of rain
This day will not come around again

Dust if you must
But bear in mind
Old age will come
And it's not kind
And when you go
And go you must
You yourself will make more dust

[1.0 @ 3:40 - 4:16]

Notes @ Quotes

"
Making yourself a priority . . is like . . the main thing that you need to do . . when it comes to attraction
"
-- Devan Christine
[1.0 @ 2:37 - 2:41]


Notes @ Previously Watched Videos

[1.0]
Vlogmas Day 4
By Devan Christine
https://www.youtube.com/watch?v=al_eU3jCFbw


