

# Day Journal Entry - December 24, 2020


[Written @ 15:22]

Notes @ Newly Learned Phrases

"
Blood of the covenant . . is . . thicker . . than . . the . . water . . of . . the . . womb . . 
"
[2.3 @ 2:04 - 2:12]


Notes @ Newly Learned Words

Military Brat [2.3 @ 2:16]

Notes @ Newly Discovered Website Organizations

Privacy [1.3 @ 3:43 - End of Video]
"Buy things online using virtual cards . . instead of real ones . . "


Notes @ Newly Watched Videos

[1.3]
Your Christmas Gift Is Trash (ft. Community Channel)
By Anna Akana
https://www.youtube.com/watch?v=qMOtCJKfCOA

[2.3]
Signs of a real friend
By Anna Akana
https://www.youtube.com/watch?v=WHeOJ0oq1RE&ab_channel=AnnaAkana


[Written @ 14:17]

Notes @ Newly Learned Words

Water Goblet [2.2 @ 6:43]

Notes @ Newly Hypothesized Words

Water Goblet Stem [2.2 @ 6:45]



Notes @ Newly Watched Videos

[1.2]
The Humanization of Corkus.
By BecketTheHymnist
https://www.youtube.com/watch?v=l28R0P1J8xQ&ab_channel=KyleEntertainment

[2.2]
Dining Etiquette: how to master the basic table manners
By Jamila Musayeva
https://www.youtube.com/watch?v=zA2PfKRcm0g&ab_channel=JamilaMusayeva


[Written @ 13:35]

Notes @ Newly Discovered Shop

Frame of Mind Clothing
[1.1 @ 15:53]

Notes @ Newly Discovered Website

www.joinfightcamp.com
[1.1 @ 14:35]

www.frameofmindshop.com
[1.1 @ Video Description]


[Written @ 13:13]

Notes @ Newly Discovered Role Play

"Hibachi Chef and Customer" Roleplay
[1.1 @ 7:03 - 15:49]


Notes @ Newly Watched Videos

[1.1]
Picking Bry up from the Airport!! | Surprise Hibachi at Home | Lauren and Bry | LGBTQ
By Lauren and Bry
https://www.youtube.com/watch?v=ysrPO7wzuNM&ab_channel=LaurenandBry

[Written @ 12:53]

Notes @ Newly Learned Words

Yule
[1.0 @ 25:39]

[Written @ 12:31]

Notes @ Newly Discovered Ideas

I was inspired to think of the idea . . of . . world of warcraft characters . . that . . wear . . "tattoos" . . as one of the main types of . . body armor . . 

Tattoo gear . . could possibly be . . a way . . to gain . . magical . . spell casting abilities . . such as . . shadowbolts . . or . . firebolts . . or other types of spells . . based on the nature of the tattoo . . that the wearer . . is wearing . . and so . . different tattoos . . provide . . statistics . . for a character . . on the abilities that they have . . similar to how . . gear . . in world of warcraft today . . such as . . shoulder pads . . and . . helmets . . and gauntlets . . or . . gloves . . uh . . and leggings . . uh . . are . . uh . . common . . wearable . . clothing items . . that you can wear . . to improve . . the abilities . . or to expand . . the social . . abilities . . of your character . . or something like that . . 

That could be interesting . . to see . . the ability . . to wear . . tattoos . . as a customizable feature . . uh . . I'm not exactly sure . . what . . the tattoos . . should really do . . uh . . for example . . uh . . one cool thing about tattoos . . is that there are quite a diversity of options that you can have . . and . . they can each have . . various symbological meaning . . to the wearer . . and uh . . well . . uh . . spiritual tattoos . . could be . . uh . . things like . . mandalas . . or uh . . flowers of life patterns . . or something like that . . and also something possibly completely different . . uh . . but . . uh . . I don't really know . . maybe uh . . for example . . with spiritual related tattoos . . more spiritual abilities . . such as . . aura . . casting . . concepts . . could be developed . . or something like that . . or uh . . maybe uh . . something like . . channeling . . information . . or communicating . . across great distances . . or uh . . pistols . . made of . . iron . . toughened . . air . . aura . . abilities . . like how . . Kurapika . . in the show . . Hunter x Hunter . . is able . . to form . . consumable . . weaponry . . devices . . because of their ability . . to . . concentrate their aura . . or . . something . . like that . . 

[Thoughts inspired by [1.0 @ 20:28 - 21:58]]


Notes @ Newly Created Words

hiron
[Written @ December 24, 2020 @ 12:39]
[I accidentally spelled the word . . "hiron" . . instead of . . typing . . the word . . "iron" . . I'm sorry . . I'm not sure . . what this word . . "hiron" . . should mean . . at this . . time . . ]


[Written @ 12:20]

Notes @ Newly Learned Quotes

"
But also . . I've taken emphasis . . of identity . . now . . to . . the point of where . .

I don't really feel like . . you . . have to put . . labels . . on everything . . for it to be like . . you . .
"
-- Harmony Nice
[1.0 @ 15:06 - 15:16]


[Written @ 12:13]

Notes @ Newly Learned Acronyms

EMDR [1.0 @ 11:43]

[Written @ 12:09]

Notes @ Newly Discovered Hair Fashion
Notes @ Fashion

2 Front Braided Ponytails
2 Front Ponytails that are braided
2 Front Ponytails that are braided with a scrunchy ring around the ends of the braid
[1.0 @ approximately 10:37 is when I started to notice this unique style of haircut; although the haircut seems to be apparent throughout the video]

[Written @ 11:42]

Notes @ Newly Discovered YouTube Channels

Harmony Nice
https://www.youtube.com/channel/UCoJQafFd9es78EhaqXl4WNA/videos
[Discovered by [1.0] which was a recommended video on the YouTube Home Page on my cellphone]


Notes @ Newly Learned Words

Realse
[1.0 @ 0:35]

[1.0]
Goodbye 2020 ❊ Healing, moving & Growing, a sequel ❊
By Harmony Nice
https://www.youtube.com/watch?v=mpO9D35HLuY


[Written @ 0:15]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:04:29]

- 📝 [commentary] I watched [1.0] [2.0]

[1:04:29 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Dance of the Moon ~ Ecstatic Dance with Sophie Sôfrēē, Mana Mei & Layla El Khadri
By Sophie Sôfrēē
https://www.youtube.com/watch?v=9CEnSTruBBg&ab_channel=SophieS%C3%B4fr%C4%93%C4%93

[2.0]
My Journey Becoming an Ecstatic Dance DJ
By Sophie Sôfrēē
https://www.youtube.com/watch?v=eenwSKRw1nU&ab_channel=SophieS%C3%B4fr%C4%93%C4%93



