


# Day Journal Entry - December 2, 2020


[Written @ 23:13]

Notes @ Possible Alternative Titles for Video [1.1]

Fun Without Distance
[Inspired by a spontaneous visual insight . . or a . . visual illusion . . when I . . momentarily glanced . . at the . . title for the video . . [1.1] . . I thought I read the word . . "Fun" . . in place of . . "Faith" . . but it was a mental illusion . . and "Faith" . . uh . . appears to be . . the . . uh . . immediately perceivable term . . uh . . when I uh . . recursively iterate my eyes and attention . . to the word position area where I expect the sentence to begin . . it seems that the sentence iteration area . . conceives of the possibility of a "Faith" . . term being applied . . whereas . . in an alternative reality . . maybe the word . . "Fun" . . could have been the word that would be my sensory experience]

Notes @ Newly Created Words

nanture
[Written @ December 2, 2020 @ 23:51 - December 3, 2020 @ 0:17]
[I accidentally typed the word . . "nanture" . . instead of . . typing . . the . . word . . "nature" . . I'm sorry . . I'm not sure . . what this word . . "nanture" . . should mean . . at this time -_- . . although . . -_- . . when . . uh . . -_- . . I saw this word . . in my imagination uh . . appeared . . uh . . -_- . . a photograph . . of a particular -_- . . type of creature -_- . . -_- . . I'm not really sure what this creature has to do with this term . . -_- . . -_- . . uh . . I guess . . this was a spontaneous . . uh . . illumination . . in my mind's eye . . or in my uh . . sensory perception uh . . like uh . . a spontaneous memory image appearing in my uh . . hallucinatory visual system or something like that . . -_- . . [2.1] . . in the . . uh . . momentary . . flash of insight . . I uh . . saw the . . scarab . . uh . . creature from "World of Warcraft" . . uh . . but the background . . environment . . of the creature . . was in the . . Netherstorm . . in Outlands . . in World of Warcraft uh . . the Burning Crusade . . Expansion . . uh . . I was kind of surprised . . to see . . a . . Scarab . . uh . . -_- . . uh . . in the Netherstorm . . uh . . but uh . . it also uh . . theoretically could be a good fit . . since uh . . the uh . . Scarab creature that I saw . . was . . the . . Purple . . skinned . . Scarab . . creature . . or uh . . the Purple-Shelled . . uh . . or the . . Purple uh . . version . . of the outer appearance . . uh . . of the Scarab . . creature . . or something like that . . and uh . . yet uh . . even in the expansion of the Burning Crusade . . uh . . even if uh . . I wasn't uh . . or . . Uh . . I'm not really uh . . familiar . . with . . seeing . . a . . Scarab . . in the Netherstorm . . uh . . Uh . . well . . I am . . used to seeing the . . "Stingray" and "Netherdrake" . . creatures . . uh . . in the Netherstorm area of Outlands . . uh . . and those creatures are also uh . . commonly found in the . . Purple color . . or in the . . Purple . . skin color . . or in the . . Purple . . outer appearance . . color . . which is really uh . . uh . . suggests or uh . . maybe supposes that . . maybe other . . uh . . Purple . . uh . . creatures . . could also live in that area . . such as the Scarab . . which is uh . . uh . . the Purple . . uh . . version of the Scarab . . uh . . that I saw in my imagination . . just a few moments ago]

inspight
[Written @ December 3, 2020 @ approximately minus 20 minutes from the ending . . ended writing . . at . . 0:16]
[I was trying . . to type . . the word . . I was intending . . to type the word . . "insight" . . and . . uh . . I accidentally typed . . the word . . "inspi" . . uh . . well . . uh . . uh . . well . . Uh . . I didn' tknow . . whether to continue . . uh . . -_- . . to uh . . uh . . take . . uh . . -_- . . inspiration . . -_- . . from this typographical error . . uh . . -_- . . well . . uh . . -_- . . I uh . . considered this to be a typographical error . . in my progress towards writing the word . . "insight" . . -_- . . well uh . . -_- . . uh . . uh . . pursuing . . uh . . this path of existence . . seemed like it would take more -_- . . uh . . typing . . and more effort to create uh . . -_- . . and I was already on a quest to create . . -_- . . uh . . something else . . uh . . the description of the discovery for nanture . . as well as additional thoughts on the uh . . discovery -_- . . uh . . but now . . it seems like this reality . . uh . . -_- . . is uh . . my immediately perceptible . . uh . . -_- uh . . -_- . . -_- . . well maybe not -_- . . I'm not sure . . uh . . -_- . . but it is interesting to see -_- . . uh . . the possibility -_- . . of how . . -_- . . this uh . . -_- . . -_- . . uh . . -_- . . uh . . inspight . . could have . . -_- . . uh . . been . . -_- . . uh . . right . . uh . . the word . . "inspi" . . uh . . could possibly be continued as . . "inspight" . . uh . . and uh . . I was . . uh . . taking . . uh . . inspiration . . uh . . from . . uh . . "inspire" . . uh .  uh . . I'm sorry . . I meant to say . . "insight" . . uh . . I was trying to continue the word . . "inspi" . . uh . . while keeping in my my . . originally . . intended . . word . . "insight" . . and the result was . . "inspight" . . which is quite a beautiful word . . uh . . hmm . . uh . . hmm . . well . . it's quite beautiful . . and also reminds me of . . the idea of a . . netherdrake . . which is uh . . uh . . hmm . . like a beautiful . . fantasy creature . . [3.1] [4.1]]

Notes @ Newly Discovered YouTube Video

[1.1]
Faith Without Distance
By Dr. Ferdinand Igbeche
https://www.youtube.com/watch?v=Yu2o-PptXrA
[Discovered by YouTube Autoplay . . Video . . for [1.0]]

[2.1]
World of Warcraft Scarab
![World of Warcraft Scarab](./Photographs/world-of-warcraft-scarab.jpg)

[3.1]
World of Warcraft Netherdrake
![World of Warcraft Netherdrake](./Photographs/world-of-warcraft-netherdrake.jpg)

[4.1]
World of Warcraft Netherdrake 2
![World of Warcraft Netherdrake 2](./Photographs/world-of-warcraft-netherdrake-2.jpg)

. . . 

References:

[1.0]
Ecoin #389 - Live development journal entry for December 2, 2020
By Jon Ide
https://www.youtube.com/watch?v=VSinz0jsGvY


[Written @ 23:11]

Hegemonous Choir
Hegemonous Quire
[Written @ December 2, 2020 @ 23:13]
[I had a thought that came to mind that . . was the word . . "Hegemonous Choir" . . or . . "Hegemonous Quire" . . I thought the word sounded kind of cool . . and interesting . . and . . uh . . it . . seems like . . it was . . also partially inspired . . by the word . . "Ouroboros Quine" . . which is a term that I've heard before . . or  . maybe sometimes called . . an . . "Uroboros Quine" . . hmm . . I'm also . . partially thinking that this term relates to the topic of . . "hegemony" . . and for example . . how it relates to the topic of . . someone . . or something . . having . . a hegemony . . of . . information . . or being able to collect information . . of a particular kind . . uniquely . . or solely . . uh . . and uh . . I think uh . . a hegemonous . . quire . . or a hegemonous . . choir . . uh . . well . . I'm not exactly sure . . what it could mean . . but . . for example . . uh . . the term . . choir . . reminds me . . of . . music . . and so . . an individual . . or a group of individuals . . that hold a hegemony . . over certain information . . uh . . and . . being able to . . have a musical . . instrument . . or a uh . . music . . of some kind . . that performs this . . hegemonous . . behavior . . or forms this . . union . . of ideas . . or union . . of concepts . . or union . . of information . . uh . . a uh . . hegemonous . . choir . . uh . . could uh . . maybe be like . . a union . . of . . uh . . information . . by the use of some sort of . . resonant . . instrumentation device . . or maybe . . uh . . by the use of some sort of . . musical . . apparatus . . like a document . . or a statement . . or a choice of words . . uh . . and so for example . . a dictionary . . of words . . like . . uh . . a dictionary . . for English . . words . . could be . . considered . . a . . hegemonous . . choir . . or a hegemonous . . quire . . for those individuals that are attracted by the music . . of the . . English . . Language . . uh . . and so . . uh . . well . . that's uh . . possibly . . an example . . of a . . hegemonous . . quire . . or uh . . maybe it could also be spelled . . hegemonous . . quire . . uh . . personally . . my first . . uh . . insight into this term . . or into this word . . as it . . appeared in my mind's eye . . in my imagination . . uh . . was the word . . "quire" . . but at the same time . . uh . . the word . . "quire" . . is pronounced in the same way as the word . . "choir" . . uh . . which is mean to define . . a type of . . musical . . composition group . . or an orchestration group . . and so for example . . maybe . . in the dictionary . . of English . . words . . and phrases . . the group of orchestration . . individuals . . are the letters and words . . of each of the pages . . of the orchestra . . uh . . something like that . . hmm . . well . . I'm not really sure . . how to phrase this either . . but uh . . uh . . I think . . another . . uh . . partial . . or uh . . partial . . uh . . source of inspiration . . for this term . . that arrived . . in my imagination . . was . . my thinking about the possibility . . of attaining . . a . . hegemonous . . quire . . of inspirational . . figures . . or of . . inspirational . . people . . that . . uh . . for example . . uh . . are . . in the form of girlfriends . . or something like that . . uh . . or something like this . . uh . . in the sense that . . uh . . a lot of girls . . that are uh . . inspired . . by the work . . that I've done . . are uh . . inspired . . to . . uh . . pursue . . or uh . . approach . . or uh . . inspire . . by . . sexual . . or . . romantic . . attention . . uh . . or something like that . . this possibility . . has been apparent to me . . for some . . few . . months now . . and I've really . . uh . . -_- . . in some ways I'm quite frightened of the possibility . . and in other ways . . I'm also . . uh . . kind of inspired . . by the possibility . . kind of inspired . . to uh . . maybe be . . involved . . in such a . . possibility . . which of course . . seems so . . impossibly . . unlikely . . uh . . especially uh . . with regards . . uh . . to the circumstances . . uh . . that I'm normally used . . to considering . . uh . . such uh . . uh . . I don't really know . . I haven't really studied . . the topic of . . a . . uh . . hegemonous quire . . of individuals . . uh . . relating to . . uh . . a hegemonous quire . . uh . . of . . romantic . . uh . . and uh . . sexual . . uh . . beauties . . or uh . . romantic . . and uh . . sexual . . uh . . people . . uh . . but it's uh . . really . . uh . . well . . I'm not really excited about that possibility at this time . . especially since . . it's uh . . something that really uh . . seems . . uh . . not really . . uh . . preferrable . . uh . . to be . . a . . uh . . part of . . uh . . uh . . well uh . . I'm not really sure . . uh . . I don't know a lot about this topic personally . . uh . . well . . uh . . hmm . . it's really difficult to say . . for example . . the topic of uh . . polygamous . . involvement . . is always . . involved especially with romantic . . interests . . that are stemming . . from . . uh . . multiple . . sources . . uh . . individuals . . or . . multiple . . uh . . sources of people or something like that . . I'm not really sure . . -_- . . wow . . there's so much to say about this topic . . and uh . . -_- . . hmm . . I think I'm going to stop here because my feelings are really not in line with this topic . . and I really uh . . well . . uh . . I don't uh . . say much about this topic on the live stream . . uh . . and yet it is a thought that has crossed my mind . . or . . uh . . this is a thought that has entered my mind . . for the past few months . . uh . . due to perhaps the nature of the influence . . or uh . . the nature of the possible influence . . of the work . . that I've been performing . . uh . . in how it's uh . . possibly . . uh . . beneficial . . or uh . . possibly uh . . enjoyable . . uh . . or possibly uh . . contributing to the types of thoughts or things that uh . . people uh . . would come to admire . . or uh . . be attracted to or uh . . be uh . . uh . . uh . . uh . . uh . . uh . . uh . . I'm not really sure . . each individual . . I suspect . . will find . . uh . . what they uh . . like . . uh . . and what they . . uh . . don't . . like . . uh . . about the work . . uh . . in terms of uh . . what I'm uh . . talking about on the live stream . . or uh . . how it relates to their own personal . . uh . . privileges -_- wow . . I'm walking such a fine line and I don't . . know . . what to step on and what to talk about . . a lot of the time . . I'm feeling that maybe it's okay to share my thoughts and feelings on various topics . . uh . . even if I'm not really confident that my uh . . emotions are worked on my own . . or if I've really found a positive solution . . or uh . . if I've really found a solution that works for many other people . . and not only myself . . but for . . a lot of people that uh . . well uh . . possibly their lives would be influenced by those types of things uh . . well . . uh . . I uh . . a lot of my thinking uh . . personally uh . . a lot of . . uh . . my personal thinking . . stems from . . having read books like . . uh . . the Seth Books . . and . . uh . . books by . . the Pleiadians . . uh . . and uh . . well uh . . there have been a lot of other spiritual teachers that I've uh . . really taken their words and thoughts seriously . . and uh . . I uh . . I don't really know . . uh . . Tao Te Ching . . is another great inspiration for my line of thinking . . uh . . a lot of the time . . I would think to myself that . . it's maybe uh . . more prefereable . . uh . . to not say so much . . or uh . . to uh . . leave things the way they are . . uh . . -_- . . and yet I find myself . . talking a lot about various things . . uh . . well . . uh . . and uh . . -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . *sigh* . . -_- . . -_- . . -_- . . -_- . . -_- . . *sigh* . . hmm . . well . . I would like to stop speaking now . . ]



[Written @ 1:05]

Notes @ Newly Learned Words

Emaciated [1.0 @ 2:46]

[1.0]
RESIST | Why We Fight For Justice / part 2 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=lclBCzXrBBo

[2.0]
RESIST | Meet The Activists Disrupting LA’s Unjust Justice System / part 1 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=2DI2pY4Fn40





