

# Day Journal Entry - December 9, 2020


[Written @ 13:11]

### Project Tasks that Have Been Complicated To Resolve

- [] The Vuetify.js <v-select> component
  - `multiple` attribute to allow multiple selection
  - `v-slot:items` attribute to allow change of template
  - Question: How do you show the checkbox . . on the left that was previously visible without the `v-slot:items={ item }` ? 
  - 

<!-- [✅] The Checkbox on the left of each item is appearing -->
<v-select multiple :items="['hello', 'hey there']"></v-select>

<!-- [🚫] The Checkbox on the left of each item isn't appearing -->
<v-select multiple :items="['hello', 'hey there']">
  <template v-slot:template="{ item }">
    <!-- [❔] How to add the checkbox here ? -->
    {{ item }}
  </template>
</v-select>

[Written @ 9:35]


### Project Task List for Today

- [] Update the Create Transction Request Component
  - [completed] Add the "forTheBenefitOfAccountIdList"
  - [completed] Add settings for the Memo / Notes input field
    - [] Add the option to add a Private Note
  - [completed] Add a transctionType Select Input
  - [completed] Add a productResourceId Select Input
    - [completed] Add a settings icon
    - [] Add a productResourceFeatureSettings Settings Option
  - [completed] Add a contactInformation Input
    - [cancelled] Add a settings icon
      - [commentary] [Written @ December 9, 2020 @ 13:54]: settings information may not be required at this time
  - [delayed] Add icons to the v-select component for the contact information select input field

- [completed] Update the Project README.md
  - [initialized] [completed] Add a "Latest Development Update" . . notification
  - [initialized] [completed] Add a link reference to the number of the table of contents element

[Written @ 9:21]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:05:21]

- 📝 [commentary] Greetings

[0:05:21 - 4:24:22]

- 🚧 [work-progress] Update the Create Transction Request Component
  - ✅ [completed] Add the "forTheBenefitOfAccountIdList"
  - ✅ [completed] Add settings for the Memo / Notes input field
    - ⏳ [not-started-yet] Add the option to add a Private Note
  - ✅ [completed] Add a transctionType Select Input
  - ✅ [completed] Add a productResourceId Select Input
    - ✅ [completed] Add a settings icon
    - ⏳ [not-started-yet] Add a productResourceFeatureSettings Settings Option
  - ✅ [completed] Add a contactInformation Input
    - 🚫 [cancelled] Add a settings icon
      - 📝 [commentary] [Written @ December 9, 2020 @ 13:54]: settings information may not be required at this time
  - 🤔 [delayed] Add icons to the v-select component for the contact information select input field

- ✅ [completed] Update the Project README.md
  - 💡 [initialized] ✅ [completed] Add a "Latest Development Update" . . notification
  - 💡 [initialized] ✅ [completed] Add a link reference to the number of the table of contents element

[4:24:22 - End of Video]

- 📝 [commentary] Salutations


[Written @ 9:19]

### Live Stream Checklist for Ecoin #396 - December, 9 2020

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete
* [x] Live stream url: https://www.youtube.com/watch?v=jB3shQduKbg
* [x] Live stream duration: approximately 04:31:35.943 . . 4 hours . . 31 minutes . . 35 seconds . . 

Health and Comfort

* [] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position

Self Love

* [x] Practice intentional smiling
* [] Practice gratitude meditation and prayer
* [] Practice self-love (hug yourself technique)
  - [] Mindfulness Tool: Hug Yourself By mindfulnesstoolstv https://www.youtube.com/watch?v=x-9J0Lzq6Iw&ab_channel=mindfulnesstoolstv
* [x] Practice intentional breathing
* [] Practice singing and toning (as recommended by Unicole Unicron [1.0 @ 3:11] and the Pleiadians [2.0 @ Page 150])
  - [] Fiddler on the roof - If I were a rich man (with subtitles) By guru006 https://www.youtube.com/watch?v=RBHZFYpQ6nc&ab_channel=guru006
  - [] Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4&ab_channel=SikhMantras

Personal Goals

* [x] Live stream for at least 3 hours
* [] Start Live streaming at 12:00 [started the live stream by before 9:35]

Music

* [x] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [] 米津玄師 MV「パプリカ」Kenshi Yonezu / Paprika By  米津玄師 https://www.youtube.com/watch?v=s582L3gujnw
  - [] Dreams - Preface by Seth By Tim Hart Hart https://www.youtube.com/watch?v=2dtXI2RATIw&list=PLPDTOFbrYdqCBF9qmNqqxZaSj4lLm537Q&index=1
  - [] Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [] Tao Te Ching     (The Book Of The Way)     #Lao Tzu                       [audiobook]   [FREE, FULL] By Peter x https://www.youtube.com/watch?v=o2UYch2JnO4&t=1s
  - [] The Seth Material - Introduction (1 of 2) By Tim Hart Hart https://www.youtube.com/watch?v=Jlcos5ZM2NE&list=PLPDTOFbrYdqA0vpIROtyd3tXbzkK49E5T
  - [x] [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs
  - [] Instant Calm, Beautiful Relaxing Sleep Music, Dream Music (Nature Energy Healing, Quiet Ocean) ★11 By Sleep Easy Relax - Keith Smith https://www.youtube.com/watch?v=4zqKJBxRyuo
  - [] Bhaktas- The Cosmic Mantra! Meditation Music ! By Hareesh Sahadevan https://www.youtube.com/watch?v=O3wQBWIOCQA
  - [] [ 𝑷𝒍𝒂𝒚𝒍𝒊𝒔𝒕 ] aesthetic song • lofi type beat • 3 hours By  연우yanu https://www.youtube.com/watch?v=cbuZfY2S2UQ
  - [] The Nature of Personal Reality - Introduction by Jane Roberts By Tim Hart Hart https://www.youtube.com/watch?v=8GkzYqJmpGM&list=PLPDTOFbrYdqBBIu1k5ubY7OBKcy5Xc5co&index=1&ab_channel=TimHartHart
  - [] 2 hours Study With Me (with talking during breaks) By Study Vibes https://www.youtube.com/watch?v=MC6yJ4BvmKY&ab_channel=StudyVibes
  - [] Dreams of Japan 🍃 lofi hip hop mix By Dreamy https://www.youtube.com/watch?v=SXfupJIXtRA&ab_channel=Dreamy
  - [x] The Way Toward Health - Foreward By Tim Hart Hart https://www.youtube.com/watch?v=aQn80TG_-fI&list=PLPDTOFbrYdqAIfmXnLxIqW2qxC2CpNKXj&ab_channel=TimHartHart

Work and Stream Related Programs

* [x] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
- (1) youtube live stream chat window
- (2) youtube + firefox (picture-in-picture mode)
- (3) iTerm
- (4) https://www.timeanddate.com/stopwatch
- (5) Brave Browser, Firefox, Firefox Nightly, Firefox Developer
- (6) Visual Studio Code
- (7) ProtonVPN [previously, Express VPN]
- (8) Visual Studio Code [previously, "Notes" Application on Macbook Laptop]

Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - []

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream

Checklist References:

[1.0]
CAM CHURCH- TRANSMUTATION
By Unicole Unicron
https://www.youtube.com/watch?v=cKvcQpPrjY4

[2.0]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak





[Written @ 5:10]

waters
smart

. . . 


waters . . 
. .
maters . . (w to m)
. . 
smarte . . (transform)
. . 
smart . . (remove e)


[Written @ 5:03]

Notes @ Newly Remembered Words

Lightworker [1.2 @ Video Description]

[1.2]
Waking-Up: Interview Jessica Schab
By Amanda Flaker
https://www.youtube.com/watch?v=tNgPMJ5sw4M


[Written @ 4:38]

Notes @ Newly Created Words

Miracuo
[Written @ December 9, 2020 @ 4:37]
[I accidentally typed the word . . "Miracuo" . . instead of typing the word . . "Miraculous" . . I'm sorry . . I'm not sure . . what this word . . "Miracuo" . . should mean . . at this time . . ]

[Written @ 4:34]

Notes @ Newly Created Words

frequencie
[Written @ December 9, 2020 @ 4:32]
[I accidentally typed the word . . "frequencies" . . as . . "frequencie" . . I'm sorry . . I'm not sure . . what this word . . "frequencie" . . should mean . . at this time . . ]

freqneuncie
[Written @ December 9, 2020 @ approximately 4:33]
[I accidentally typed the word . . "frequencie" . . as . . "freqneuncie" . . I'm sorry . . I'm not sure . . what this word . . "freqneuncie" . . should mean . . at this time . . ]

[Written @ 4:25]

Notes @ Newly Created Words

gonnda
[Written @ December 9, 2020 @ 4:25]
[I accidentally typed the word . . "gonnda" . . instead of typing the word . . "gonna" . . I'm sorry . . I'm not sure . . what this word . . "gonnda" . . should mean . . at this time . . I'm sorry . . ]

[Written @ 4:16]

Notes @ Newly Learned Words

Light Bath [2.0 @ 8:38]

[Written @ 3:29]

Notes @ Newly Created Words

organse
[Written @ December 9, 2020 @ 3:29]
[I accidentally typed the word . . "organs" . . as . . "organse" . . I'm sorry . . I'm not sure what this word . . "organse" . . should mean . . at this time . . ]

[Written @ 2:48]

Notes @ Newly Learned Words

Sciatica [2.0 @ 5:19]


[Written @ 1:42]

Notes @ Cool Sounding Music Clips

"
It's a beautiful thing . . it allows us to step into higher states of consciousness . . It allows us to . . change . . our reality . .
"
[2.0 @ 1:56 - 2:03]
[I was listening to . . [1.1] . . while listening to this . . audio . . clip . . and thought that . . the mix of sounds . . was really cool . . I could see . . a video being . . developed . . for this . . audio clip . . where there are flowers . . and . . uh . . musical . . uh . . sounds . . and things like this . . uh . . something symbolic for the feeling . . uh . . that's . . uh . . being talked about . . uh . . the . . "-ty" . . sound . . at the . . end of the word . . "reality" . . sounded really cool . . "It allows us to . . change . . our reality" . . uh . . there was . . an . . accentuation . . uh . . or uh . . pronunciation . . of the word . . with . . a certain sound that made the . . word . . "reality" . . feel . . clear . . or something like that]


[1.1]
Dreams of Japan 🍃 lofi hip hop mix
By Dreamy
https://www.youtube.com/watch?v=SXfupJIXtRA&ab_channel=Dreamy

[Written @ 0:37]

Transcript for [2.0]:

Hello Everyone . . Happy . . Tuesday . . 

I love you . . Sending you a very big . . virtual hug . . whereever you are on the Earth plane . . in this now . . moment . . 

[timeline-duration]: [0:07]

I want to do . . a video . . today . . on the . . physical . . experiences . . or "symptoms" [air quotes] . . that many of us . . may be . . feeling . . this month . . as we continue to . . receive . . and . . uhm . . absorb . . the high frequency . . light . . that's coming into . . our . . bodies . . 

So . . if you've been on this path for a while . . this . . "ascension" [air quotes] . . sort . . of . . journey . . You know that there's a lot of . . physical . . sensations . . that you feel . . as your body . . moves . . into . . higher frequencies . .

But because . . this month . . is . . so . . potent . .

And there's so much light . . coming . . into . . our . . bodies . . 

I wanted to address . . some of these physical . . sensations . . you . . may . . be . . feeling . . so . . that . . you don't . . necessarily . . think . . that . . there's something wrong with you . . or . . bad . . about . . what you're experiencing . . 

Also . . it will bring awareness to . . "wow . . I can actually feel my physical body . . ascending . . " . .

[timeline-duration]: [0:56]

So w- . . first . . what does it mean . . that your physical body . . is . . ascending ? . . Ascending . . is moving from a lower frequency of energy to a higher frequency [1] of energy

Lower state of consciousness . . to a higher . . state of consciousness . . So your body . . is energy . . and it's . . electric . . When . . high frequency . . light . . moves into it . . the body . . is able to . . assimilate . . acclimate . . and anchor . . that light . . so that . . it can shift . . the entire . . physical . . form . . into . . higher . . frequencies . . This is a . . physical . . process . . 

So . . your . . organs . . shift . . Your . . blood . . shifts . . Your veins . . shift . . Your muscles . . shift . . Everything . . shifts . . It moves . . from . . more of a . . carbon . . desnse . . based . . form . . to . . a little bit more light . . and a little bit more light . . and a little bit more light . . 

So . . it makes . . perfect sense . . that we're feeling so many different physical sensations right now . . We're being activated . . Over . . and over . . and . . over . . 

It's a beautiful thing . . it allows us to step into higher states of consciousness . . It allows us to . . change . . our reality . .
[timeline-duration]: [1:56 - 2:03]

Right ? . . It allows us to step out of a third . . dimensional . . matrix . . Okay . . so . . here are . . some . . symptoms . . that I wanna . . talk about . . 

I also wanna say that . . there . . are . . so . . many . . Right ? . . so . . whatever . . you're feeling . . let . . yourself . . just . . feel it . . So . . don't make any physical sensation . . or experience . . you're having . . wrong or bad . . just let it be . . Detach . . from trying to figure it out . . Detach . . from trying . . to . . make . . sense . . of it . . Detach . . from any sense of . . control . . around it . . Just let your body move through this physical shift . . the way that it needs to move through it . . and listen to your body . . what . . does . . your body . . need ? . .

[timeline-duration]: [2:43]

Okay . . so . . here we go . . Some things that you may be feeling . . 

Dizziness . . Light-headedness . . uhm . . a need to drink . . a . . ton . . of water . . right ? . . just . . thirsty . . thirsty . . thirsty . . Massive . . amounts of rest . . or exhaustion . . Ringing . . in the ears . . Your equilibrium . . may . . be off . . You may feel . . like . . your . . eyesight . . is . . changing . . like . . it's getting blurrier . . or its . . becoming . . more . . in- . . like . . vibrant . . 

Your . . hearing . . may . . start . . to . . change . . You can . . hear . . things . . more . . vibrantly . . 

Your smell . . your taste . . is gonna . . start . . to change . . 

You're recognizing . . perhaps . . that . . you are . . in a dream state . . so . . to speak . . when you're moving about your day . . Almost like . . did that day . . happen . . ? . . Did . . it . . not . . happen ? . . 

Alm- . . kind of like . . you're floating . . There's this floaty . . type of feeling . . sometimes . . when you move . . in . . and . . out . . of . . dimensional . . fields . . throughout . . your day . . 

So it's like . . you're here . . but you're not really here . . but you're here . . I love that kind . . of sensation . . that we . . feel . . 

You may feel a lot of tingling in the body . . You may feel a lot of aches . . and tightness . . you may . . have a huge . . amount . . of tightness . . in your chest . . In . . that . . fourth . . energy . . center . . Almost . . like . . an anxiety . . attack . . But . . it's not . . anxiety . . it's this . . fourth . . energy center . . opening . . So . . a lot of breath . . in there . . 

[4:12]

Uhm . . It may feel like . . you're . . there's . . energy . . moving . . through your body . . Like . . you need . . to shake . . your legs . . or . . shake . . your arms . . Like there's just . . It's like . . you . . you can't . . stretch . . enough . . Right ? . . You may have a huge . . amount . . of . . back . . pain . . Uh . . Uh . . Upper back pain . . shoulder . . pain . . You may . . have . . uh . . rashes . . or bumps on your body . . that may . . come out . . Weird . . sort of . . physical things . . that you're gonna see . . on your . . body . . 

Uhm . . what else ? . . what else will you ? . There's going to be moments . . where you will . . find yourself . . in a dream . . like . . a . . day dream . . Uhm . . almost . . like . . time has stopped . . 

And then . . you wake back up . . into like . . where you are . . and what's going on . . Uhm . . what else . . ? . .

Any physical sensations . . or . . physical . . ailments . . that you've had . . Any sort of . . dis-ease . . that . . you've . . had . . That . . could . . get . . enflamed . . or . . more . . potent . . More . . uh . . You might . . feel . . it . . more . . So if you have any sort of like Sciatica . . or uh . . allergies . . to foods . . Or . . uhm . . uhm . . I mean . . there's so many different things . . that are . . popping in . . but . . any sort of . . what we would label . . as a . . "disea-" [air quotes] . . "dis-" [air quotes] . . "disease" [air quotes] . . or . . uhm . . uh . . anything . . physical . . like that . . It could . . feel like . . it's amplifying . . or getting "worse" [air quotes] . . It's not . . getting worse . . what's happening . . Is . . it is . . The . . light . . is coming . . down . . into your body . .

[5:46]

The light . . is coming . . down . . into . . whatever . . whereever . . that dis-ease . . is . . And it's lighting it up . . So it's breaking it apart . . And when it . . breaks . . it apart . . it feels more intense . . It feels like . . it's getting worse . .

So . . again . . recognize . . Huge . . amount of light . . is . . coming into your body . . It's physical . . You're gonna . . feel this . . physically . . 

Do you have a headache ? . . Drink more water . . Are you craving . . certain . . foods ? . . Eat . . those foods . . Do you need sunlight . . on your face ? . . Give yourself sunlight . . Do you need . . to sit . . quietly . . more ? . . Sit quietly more . . Do you need to sleep . . for 17 hours ? . . Sleep . . for . . 17 . . hours . . Do you need to get a massage ? . . Go get a massage . . Do you need to stretch ? . . Do you need to run ? . . Do you need to go get a facial ? . . Right ? . . Like . . what do you need ? . . What does your body . . need . . from you . . right now . . ? . . 

[6:40]

And . . every . . physical . . sensation . . that . . you're . . feeling . . many of them . . are connected . . to the high frequency . . light . . that's moving into your body . . 

So pay attention . . Instead of looking at everything . . you're feeling physically . . as . . something . . physically . . wrong . . see it as . . the . . ascension . . See it as . . your body . . ascending . . into . . higher . . frequencies . . And . . whatever . . it is . . that you're feeling . . is a piece . . of that . . It's a . . result . . of the high frequency . . light . . It's a result . . Okay ? . . 

Because . . what's gonna happen . . if you do . . d- . . go to the doctor . . or . . you go . . and so someone . . they're . . gonna [s-] . . not . . for the most part . . they're probably . . not gonna find anything wrong . . They're gonna say . . we can't find anything . . right ? . . 

You also may have . . like . . ball- . . There-there . . Something's . . coming up . . right now . . uhm . . ball-gladder . . gallbladder . . gallbladder . . Gallbladder stuff . . Kidney stuff . . Uhm . . Uh . . Organs . . Your organs . . may . . are . . are . . cleansing . . your organs . . are . . clearing . . Your organs . . are healing . . 

So if you are feeling . . tightness . . around . . organs . . They want me to remind you that it is okay . . That there's . . Don't . . necessarily panic . . the organs . . are . . clearing . . or healing . . Lots of healing . . with our organs right now . . 

[8:07]

Uhm . . Including . . the heart . . The actual . . physical . . heart . . So . . more . . breath . . Breathing . . Breathing . . Breathing . . More . . water . . More fresh air . . More . . sunlight . . if you can get it . . 

The fresher . . the foods . . you can eat . . the better . . right ? . . So whatever sort of foods . . you're putting into your body . . The more . . alive . . they are . . the more . . it's going to feed . . those organs . . and the blood . . Right . . ? . . Uh . . Light baths . . 

I have a free light bath . . on my YouTube channel . . I have a free light bath . . where else do I have it ? . . I think it's just on YouTube . . But . . doing . . light baths . . and . . really . . clearing the energy . . getting . . massages . . 

What do you need ? . . We are ascending . . We are shifting . . into . . higher frequencies . . We being . . the physical body . . You're already . . a high frequency light . . being . . You're already . . a . . high frequency consciousness . . But . . you're inside . . the physical form . . 

[9:03]

And it is this . . beautiful . . Amazing . . physical . . form . . that's . . moving . . into . . higher . . frequencies . . It's energetically . . Shifting . . And so . . we're . . gonna . . feel . . a lot . . this month . . So . . be . . gentle . . with yourself . . Have patience . . with your physical form . . Reframe . . how you see . . physical . . sensations . . right now . . 

I don't even like to call them . . symptoms . . they're physical sensations . . based . . on the . . ascension that we're in right now . . So . . recognize . . "ohp . . there's a sensation . . My body's shifting into higher states of consciousness . . or higher frequencies . . Fabulous . . I'm gonna . . rest . . I'm gonna . . give my body . . whatever . . it needs . ."

Pay . . attention . . to . . what the body . . needs . . 

Listen to the body . . not the ego . . 

The ego's gonna say . . "oh . . my god . . you should be doing this . . you should be doing this . . you should be doing this . . " . . Right ? . .

The body's gonna . . say . . "this is what I need . . "

Thanks for being here . . What a miraculous . . time . . to be here . . Our freaking . . physical bodies . . are shifting . . energy . . It's shifting frequencies . . Right now . . our . . bodies . . are shifting . . frequencies . . It's a . . It's a . . Miraculous . . experience . . It's a Miraculous . . gift . . 

So . . be gentle . . with yourself . . 

I love you so much . . Mwahh [kissing expression] . . 






[1]
The word phrase "higher array" appeared in place of "higher frequency" . . in a momentary . . flash of inspiration . . or a visionary . . hallucination . . perception . . as words . . changed in my physical vision . . like a augemented reality hologram of text appearing in my . . physical related . . vision . . perception field or something like that . . "higher array of energy" . . "higher frequency of energy" . . these seem to be counterpart words to one another . . when I start thinking about things . . I'm new to the term . . "higher array" . . 


Notes @ Newly Learned Words

Higher Array of Energy
[I saw the word . . phrase . . "higher array" . . while . . reading the transcript for . . [2.0] . . that read . . "higher frequency" . . uh . . the words . . "higher array" . . uh . . or . . uh . . maybe . . I should say that . . the word . . "array" . . appeared to replace the word . . "frequency" . . in my physical sight vision . . at the corner of my vision . . as my sight was . . relating to that . . area . . or something like that . . in the area . . of . . the paragraph . . and the word . . "frequency" changed to . . "array" . . and so . . I . . uh . . glanced the phrase . . "higher array of energy" . . which . . then disappeared . . a few milliseconds . . later . . uh . . or a few . . fractions of a second later . . uh . . and then the original . . words . . "higher frequency of energy" . . appeared . . Higher Array . . reminds me of . . an array of solar panels . . uh . . also I'm reminded by the arrays in computer programming . . that are sometimes represented by . . square brackets . . [] . . and so . . [1, 2, 3, 4] . . is an array of . . 4 elements . . hmm . . higher . . frequency . . reminds me . . of . . a sine wave graph . . that is . . very condensed in the horizontal direction . . and so for example . . the sine wave . . is highly sinusoidal . . or has . . a . . short . . wavelength . . uh . . hmm . . higher array of energy . . higher . . frequency . . of energy . . I'm not really sure . . how these two uh . . could necessarily relate to one another . . uh . . hmm . . I'm not exactly sure . . uh . . when I think of arrays . . I think a lot about computer programming arrays . . like . . [1, 2, 3, 4, 5] . . uh . . [4, 3, 2, 1] . . hmm . . [still thinking] . . [a few moments later . . 30 or more seconds . . ] . . uh . . a higher array . . array could also mean . . variety . . and so for example . . the variations . . of uh . . numbers . . could be considered an . . array of numbers . . uh . . the variations of . . unique . . uh . . numbers . . could maybe be . . the set of numbers contained by the . . Natural Number Set . . or something like that . . uh . . uh . . but . . uh . . but maybe . . [1, 1, 1, 1, 1] . . could also be considered a variation of numbers . . and so . . the variations of 1 . . could be considered . . a type of . . variety . . of . . numbers . . uh . . specifically . . the variety of 1's . . or something like that . . uh . . and so uh . . 5 ones are . . available . . in . . [1, 1, 1, 1, 1] . . uh . . so there is an . . array of 1's . . or a variety of 1's . . uh . . each in their own . . unique positions . . in the array . . uh . . the first 1 is in index . . 0 . . uh . . or in the 1st position . . uh . . in the array . . uh . . 0 or 1 . . uh . . whichever uh . . place you'd like to start uh . . when uh . . talking about computer programming . . uh . . the . . convention of starting at 0 . . is a common one . . and so . . index 0 . . is where the first . . element . . in the array . . and index 1 . . is where the first . . element in the . . array is . . uh . . hmm . . [thinking] . . [a few seconds later . . less than 30 seconds] . . higher array . . higher variety . . higher . . uh . . hmm . . I'm not sure . . uh . . okay well . . higher frequency . . uh . . is uh . . I'm not sure . . higher frequency . . hmm . . I was trying to relate the two . . but . . xD . . maybe the term . . "higher frequency" . . is too high frequency xD . . xD . . higher array . . xD . . is too low frequency . . to be related to . . higher frequency xD . . higher array . . seems to relate to numbers . . uh . . for example . . an ordinal set of numbers . . or a group of numbers of some order or arrangement or . . uh . . maybe uh . . parenthetical values like . . the range of numbers . . from . . [0, Infinity) . . and so the parenthesis . . on the right . . are representative of the range being open . . to uh . . an asymptotic approach . . uh . . that doesn't necessarily achieve the value on the . . right coordinate position of the . . range representation graph . . (number1, number2) . . (number1, number2] . . [number1, number2) . . [number1, number2] . . or something like that . . uh . . and yet on the other hand . . uh . . "higher frequency" . . seems . . uh . . not necessarily related to numbers . . and so for example . . uh . . it's like an invisible energy uh that uh . . doesn't necessarily have a physical representation ? is that correct ? . . I'm not sure . . I'm really not sure . . I am not sure about that . . maybe that's incorrect . . I think that maybe that is incorrect . . I'm not really sure . . the feeling that I get from the word . . "higher frequency" . . or the . . word phrase . . "higher frequency" . . is maybe something like . . a . . uh . . uh . . self-expanding . . or self-amplifying . . or uh . . uh . . like it's increasingly . . uh . . representing itself . . uh . . which uh . . maybe supposes that even in trying to represent it . . it ought to be represented as something that it isn't . . because already it is something that it cannot have been encapsulated by . . uh . . because of the nature of its own self expansion . . hmm . . I'm not really sure . . I've written uh . . some thoughts here but . . I'm not really sure . . ]






[Written @ 0:13]

Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:09:34]

- 📝 [commentary] I took . . a break . . today . . I was . . watching . . [1.0] [2.0] . . on my . . cell phone . . 

[1:09:34 - 1:40:03]

- 📝 [commentary] I watched . . [1.0] [2.0] . . 

[1:40:03 - 5:44:26]

- 📝 [commentary] I am recording a . . video . . transcript . . for . . [2.0] . . 

[5:44:26 - 5:57:00]

- 📝 [commentary] I am reviewing the video transcript for . . [2.0]

[5:57:00 - End of Video]

- 📝 [commentary] I will consider returning to another episode . . to translate . . [1.0] . . The video transcript may be available . . on YouTube . . at that time . . and so . . maybe a video transcript won't be necessary . . to help . . inform . . people . . on Lorie Ladd's . . work . . I think it's called "light work" . . for anyone who's uh . . maybe new to this topic . . but I'm not entirely sure myself . . and there's more things for me to learn . . uh . . I'm sorry . . Please learn more . . by visiting . . [3.0] [4.0] [5.0] [6.0] [7.0] [8.0] [9.0] [10.0] [11.0] [12.0] [13.0] [14.0] [15.0] [16.0] [17.0] [18.0] [19.0] [20.0]

References:

[1.0]
Message for Humanity | Stay focused on YOU ❤️
By Lorie Ladd
https://www.youtube.com/watch?v=W39Na3-Gqbo

[2.0]
Physical ASCENSION symptoms NOW
By Lorie Ladd
https://www.youtube.com/watch?v=goGXoreTNZY

[3.0]
Lorie Ladd
https://www.youtube.com/channel/UCXxn1HpP2HYaRjgqf1MXx0A
[Lightwork, Higher Vibration Reality]

[4.0]
Amanda Flaker
https://www.youtube.com/c/AmandaFlaker/videos
[Lightwork, Empath, Abundance, Spirituality, Manifestation, Lack Matrix]

[5.0]
Gigi Young
https://www.youtube.com/c/GigiYoung/videos
[Spirituality]

[6.0]
Teal Swan 
https://www.youtube.com/c/TealSwanOfficial/videos
[Philosophy]

[7.0]
All LIGHTWORKERS, WATCH THIS! (Important Message!)
By Infinite Waters (Diving Deep)
https://www.youtube.com/watch?v=DAlqTx1tOmY

[8.0]
Infinite Waters (Diving Deep) 
https://www.youtube.com/channel/UCjt7bEwtlk6A6f_CiY2ZOlQ
[Lightwork, Spirituality, Manifestation]

[9.0]
Temple of the Celestial Priestess
https://www.youtube.com/user/christinamartine/videos
[Spirituality, Life]

[10.0]
Unicole Unicron
https://www.youtube.com/c/UnicoleUnicron/videos
[Spirituality, Life Updates, Robot Ethicist, Social Relations, Higher Vibration Reality, Joy, Universal Basic Income, Abundance]

[11.0]
Anna Brown
https://www.youtube.com/c/AnnaBrown/videos
[The Oneness of The Present Moment]

[12.0]
The Gem Goddess 
https://www.youtube.com/c/TheGemGoddess/videos
[Spirituality, Love-Related Tarot Card Readings, Manifestation]

[13.0]
LUMIERE
https://www.youtube.com/channel/UC7Ra1SCLCwIVGWjs1dpCiuQ/videos
[Love-Related Tarot Card Readings]

[14.0]
Yuttadhammo Bhikkhu
https://www.youtube.com/c/yuttadhammo/videos
[Meditation, Spirituality, Life Activities, Philosophy]

[15.0]
Bentinho Massaro
https://www.youtube.com/c/BentinhoMassaro/videos
[Meditation, Spirituality, Philosophy]

[16.0]
Louise Kay
https://www.youtube.com/c/LouiseKay/videos
[Pleiadian Information Channeling, Silent Observation, Observation of the Present Moment, Spirituality, The Oneness of the Present Moment]

[17.0]
Bracha Goldsmith
https://www.youtube.com/c/BarbaraGoldsmithastrologer/videos
[Pleiadian Information Channeling]

[18.0]
Sadhguru 
https://www.youtube.com/c/sadhguru/videos
[Spirituality, Applying Spiritual Knowledge To Society]

[19.0]
Vanessa Somuayina 
https://www.youtube.com/c/VanessaSomuayina/videos
[Love-Related Tarot Card Reading, Spirituality, Love, Kindness, Respect for All]

[20.0]
Bashar Channeled by Darryl Anka
https://www.youtube.com/c/BasharChanneledbyDarrylAnka/videos
[Spirituality, Bashar Information Channeling]



