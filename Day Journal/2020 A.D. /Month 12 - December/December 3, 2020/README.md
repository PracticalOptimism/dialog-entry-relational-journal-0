


# Day Journal Entry - December 3, 2020

[Written @ 5:52]

Notes @ Newly Created Words

Noteless Casual Learning
[Written @ December 3, 2020 @ 5:54]
[. . . I find that I learn a lot . . by taking notes . . and so if I write things . . I can at least . . receive . . a memory . . of the topic . . uh . . that I can also look . . ]

. . . 




[Written @ 5:31]

Notes @ Newly Created Words

volumne
[Written @ December 3, 2020 @ 5:42]
[I accidentally typed the word . . "volume" . . as . . "volumne" . . I'm sorry . . I'm not sure . . what this word . . "volumne" . . should mean . . at this time . . ]


Notes @ Newly Discovered Quotes

"Through his research . . of studying . . musicians . . and athletes . . he found that it took an average . . of . . 10,000 hours . . of a specific . . kind of practice . . that lead these students . . in the right path . . towards . . success . . 

So it's not . . 10,000 hours . . then . . you become a master . . It's . . more so . . uh . . it takes . . a specific kind . .

A large . . volume . . of a specific . . kind of practice . . that he coined . . as . . 'deliberate practice' . . uhm . . to reach . . uhm . . uh . . a level of success . .  or . . you're in the right trajectory . . for reaching . . that certain . . level . . of . . success . . 

"
[6.1 @ 14:40 - 15:18]

Notes @ Newly Discovered Quotes

"The best way to get past any barrier is to come at it from a different direction, which is one reason it is useful to work with a teacher or coach."
-- By Anders Ericsson
[6.1 @ 12:11 - ]


[Written @ 5:22]

Notes @ Newly Discovered Quotes

"Your outcomes are a lagging measure of your habits"
[6.1 @ 8:11 - 8:20]

Notes @ Newly Created Words

Conjournalism
[Written @ December 3, 2020 @ 5:23]
[To conjoin . . research . . efforts . . or to . . concatenate . . journalistic . . efforts . . or to concatenate . . or to conjoin . . journalistic . . endeavors . . ]
[This thought was inspired from . . [6.1 @ 8:11 - 8:20] . . "Your outcomes are a lagging measure of your habits" . . in thinking about what my habits are . . I had the thought . . to myself . . "Conjournalism" . . which was a sort of suggestinon of . . being able to concatenate . . existing journalist . . efforts . . or journalistic . . endeavors . . ]

[Written @ 4:52]

Notes @ Newly Discovered Probable Alternatives to The Name "YouTube"

YouTubue
[Discovered by a typographical error of "YouTube" . . typed as . . "YouTubue"]

Notes @ Newly Discovered Videos

[1.1]
Webinar - Advanced D&R
By LimaCharlie
https://www.youtube.com/watch?v=39glwWnYi7c
[Discovered by YouTube Recommendation List . . from . . [1.0] . . Video Order Index . . 16 . . not including . . the autoplay video . . ]

[2.1]
Comparisites Review With Demo 🚦 Massive Super Vendor 🤐 Back Door Bonuses 🚦
By Mark Gossage
https://www.youtube.com/watch?v=MlQV-5KOx3w
[Discovered by YouTube Recommendation List . . from . . [1.0] . . Video Order Index . . 17 . . not including . . the autoplay video . . [17, 17] . . is the list of measurement results . . when calculating the video order index . . 2 of 2 times . . the result . . 17 was achieved . . ]

[3.1]
Gratitude In Every Moment And Through Oneness with Jonathan Ellerby, PhD and Althea Center
By AltheaTV
https://www.youtube.com/watch?v=8m0HPX-F1zM
[Discovered by YouTube Recommendation List . . from . . [1.0] . . Video Order Index . . 18 . . not including . . the autoplay video . . the measurement . . of video order index 18 . . was achieved by adding . . 1 . . to the previous . . [2.1] . . discovery order index . . uh . . since the video . . [2.1] . . was shown . . to the top of . . the video . . [3.1] . . and so for example . . a repeated count from the beginning of the list . . wasn't computed . . or calculated . . ]

[4.1]
🔴 LIVE: Trump Attorney Rudy Giuliani, Witnesses Testify at Michigan House Oversight Committee
By Right Side Broadcasting Network
https://www.youtube.com/watch?v=eUjTOSDZ0BE
[Discovered by YouTube Recommendation List . . from . . [1.0] . . Video Order Index . . 19 . . not including . . the autoplay video . . ]

[5.1]
Immigration is an LGBTQ Issue: Love Across Borders - Sept 29, 2020, English
By AtheaTV
https://www.youtube.com/watch?v=7iqAoDamRcw
[Discovered by YouTube Recommendation List . . from . . [3.1] . . Video Order Index . . 7 . . not including . . the autoplay video . . ]

[6.1]
Robert Young: A Guide to Deliberate Practice
By Saxaphone Studio Class
https://www.youtube.com/watch?v=u3GqZN3LSY8
[Discovered by YouTube Recommendation List . . from . . [1.1] . . Video Order Index . . 11 . . not including . . the autoplay video . . ]

[7.1]
HOW TO SUCCESSFULLY LEAD YOURSELF AND OTHERS FOR YOUR CAREER DEVELOPMENT WITH KOJO VAN-ESS
By CAREER CLINIC WITH TRICIA
https://www.youtube.com/watch?v=jOTcFL5Bocg
[Discovered by YouTube Recommendation List . . from . . [1.0] . . Video Order Index . . 16 . . not including . . the autoplay video . . uh . . this uh . . was the 16 order index video . . after having refreshed the page . . since . . uh . . [1.1] . . was the previous . . uh . . 16th . . order index . . result . . before . . the . . web browser . . website webpage for [1.0] . . was refreshed . . ]

. . 

References:

[1.0]
Ecoin #389 - Live development journal entry for December 2, 2020
By Jon Ide
https://www.youtube.com/watch?v=VSinz0jsGvY&feature=youtu.be

[Written @ 4:47]

Notes @ Length of the Live Stream for . . Ecoin #389 . . 

https://www.youtube.com/watch?v=VSinz0jsGvY&feature=youtu.be

05:10:01.938

. . 5 hours . . 10 minutes . . 1 seconds . . 



[Written @ 4:35]

Notes @ Newly Learned Words

Resting Energy Expenditure [1.6 @ 35:48]

[1.6]
Plant Based Health and Nutrition - Session I
By Downstate TV
https://www.youtube.com/watch?v=BhG9easNpAo

[Written @ 3:02]

Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:27:29]

- 📝 [commentary] I took notes in my dialog entry relational journal
  - [1.0 @ Day Journal Entry - December 2, 2020]
  - [1.0 @ Day Journal Entry - December 3, 2020]
  - [1.0 @ Education Notes Journal Entry - December 3, 2020]
  - [1.0 @ Word Day Journal Entry - December 2, 2020]
  - [1.0 @ Word Day Journal Entry - December 3, 2020]

- 📝 [commentary] I watched videos [2.0] [3.0] [4.0] [5.0] [6.0]

[3:27:29 - 4:52:24]

- 📝 [commentary] I am intending to take notes in the [1.0 @ Education Notes Journal Entry - December 3, 2020] for . . [7.0]

- 📝 [commentary] I make plans to watch the videos at [7.0] on another time and date . . instead of satisfying my original intentions . . 

[4:52:24 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Dialog Entry Relational Journal 0
By Jon Ide
https://gitlab.com/practicaloptimism/dialog-entry-relational-journal-0

[2.0]
Demystifying Embodied Energy
By GBRI
https://www.youtube.com/watch?v=7jmk79JKigI
[Watch Order Index: 3]

[3.0]
Yoga for Sustainability Tao Porchlyne at UN
By GBRI
https://www.youtube.com/watch?v=Dixd93UpZ-Q
[Watch Order Index: 4]

[4.0]
UN Secretary General Message About Yoga
By GBRI
https://www.youtube.com/watch?v=Y7EJrS6mt28
[Watch Order Index: 2]

[5.0]
Sustainability Around the World
By GBRI
https://www.youtube.com/watch?v=o1T_8wVGtjY
[Watch Order Index: 1]

[6.0]
The Road to becoming an Architect begins here - GBRI NATA Exam Prep promotional video
By GBRI
https://www.youtube.com/watch?v=dtBKq0gP5gQ
[Watch Order Index: 5]

[7.0]
RESIST | Communities Fighting For Justice
By Patrisse Cullors
https://www.youtube.com/watch?v=HKKz6QVdwFQ&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc


[Written @ 2:54]

Notes @ Videos To Consider Watching Later

How to Learn to Love Yourself with Peter Crone - Genius Life Podcast
By Max Lugavere
https://www.youtube.com/watch?v=qpSUa0h4ECs
[I'm inspired to watch this video later uh . . uh . . I like the topic of . . "self-love" . . and uh . . hmm . . uh . . I guess . . uh . . I uh . . read the words . . "How to Learn to Love Yourself with Peter Crone" . . uh . . uh . . and thought the video would be about . . self love . . ]



[Written @ 2:41]

Notes @ Newly Watched YouTube Videos

[1.5]
Demystifying Embodied Energy
By GBRI
https://www.youtube.com/watch?v=7jmk79JKigI
[Watch Order Index: 3]

[2.5]
Yoga for Sustainability Tao Porchlyne at UN
By GBRI
https://www.youtube.com/watch?v=Dixd93UpZ-Q
[Watch Order Index: 4]

[3.5]
UN Secretary General Message About Yoga
By GBRI
https://www.youtube.com/watch?v=Y7EJrS6mt28
[Watch Order Index: 2]

[4.5]
Sustainability Around the World
By GBRI
https://www.youtube.com/watch?v=o1T_8wVGtjY
[Watch Order Index: 1]

[5.5]
The Road to becoming an Architect begins here - GBRI NATA Exam Prep promotional video
By GBRI
https://www.youtube.com/watch?v=dtBKq0gP5gQ
[Watch Order Index: 5]


. . . 

Notes @ Videos To Consider Watching Later

[1.6]
Mike Biddle Interview
By GBRI
https://www.youtube.com/watch?v=u12hGMBYGGE
[Mike Biddle sounds like an interesting name . . I was inspired . . to watch . . this video . . because of the . . interesting name . . along . . with the . . appearance . . of the person in the thumbnail of the video . . I wonder . . uh . . well . . I wonder what the story of this person . . "Mike Biddle" . . is . . and so . . I was inspired . . to click on the video . . and also . . it's an interesting phenomenon . . to be . . the very first video . . published . . by the YouTube Channel . . GBRI . . which already . . has a . . uh . . uh . . I guess . . uh . . a good reputation . . with me . . since uh . . I uh . . enjoy uh . . their content . . about . . uh . . sustainability . . or something like that . . uh . . human relations and sustaining the uh . . availability of the Earth . . for other humans . . to use . . for the future . . or something like that . . that's the vibe that I get from the GBRI . . YouTube channel so far . . uh . . and uh . . well . . uh . . I only now discovered the YouTube channel . . today . . uh . . another criteria . . that uh . . maybe influences uh . . -_- . . maybe not never mind . . the criteria . . that I listed so far . . are mostly what I was thinking before clicking on this video . . to consider . . watching the video . . uh . . but at the time of this writing . . I would like to . . uh . . end the live stream . . uh . . for the Ecoin live stream [1.0] that I'm uh . . performing . . or for the Ecoin live stream . . that I'm doing . . and uh . . so . . uh . . maybe uh . . for example . . I'll watch the video . . [1.6] . . on my own time . . or something like that . . uh . . I'm not sure . . uh . . hmm . . If I did watch the video . . hmm . . oh right . . I would write something like . . "Notes @ Newly Watched Videos" . . in this . . uh . . dialog entry relational notebook or something like that . . cool :O . . ]


[Written @ 1:53]

Notes @ Newly Discovered YouTube Channels

GBRI
https://www.youtube.com/c/GbrionlineOrg/videos


Notes @ Newly Discovered Videos

[1.4]
Transforming our world: the 2030 Agenda for Sustainable Development -Part 3
By GBRI
https://www.youtube.com/watch?v=vRzXA_cxiRc
[Discovered by Video Recommendation List Item . . from . . [1.0]]


[Written @ 1:50]

Notes @ Newly Learned Names

Geige [2.3]

[1.3]
StreamYard
https://streamyard.com/

[2.3]
StreamYard Photograph
![Geige Name Usage](./Photographs/3a9663349159afc48979ae3e4a046c69.jpg)
[Discovered from [1.3 @ Brand your broadcast]]



[Written @ 1:38]

[Written @ 1:37]


Notes @ Possible Alternative Names For Gia Goodrich

Simone Goodrich
[I had a thought of inspiration . . uh . . when . . reading . . "Gia Goodrich" . . as . . a . . name . . and thought to myself . . "Simone" . . could have also been a name for this person . . uh . . I'm not really sure . . if this thought was inspired from other . . uh . . African . . uh . . people . . or . . African . . American . . or . . uh . . African uh . . Asian . . or . . uh . . Afro-Asian . . uh . . or uh . . uh . . or uh . . other people with . . the name . . Simone . . have also appeared similar to the person . . Gia Goodrich . . uh . . who I was watching from . . [1.2]]

Gia Apple
[I looked at the name . . "Goodrich" . . and in a flash of inspiration . . I saw the word . . "Apple" . . instead of the word . . or the name . . "Goodrich" . . uh . . well . . uh . . in seeing this word . . "Apple" . . I'm starting to think to myself . . that maybe . . that was inspired . . by . . uh . . the . . uh . . "Apple" uh . . computer . . company . . uh -_- . . but uh . . "Apple" . . is also a fruit . . and . . a healthy uh . . diet of apples will keep your health "good" and "rich" . . so to speak xD . . xD . . haha . . I'm not sure why I saw the word "Apple" . . appear in my vision . . but uh . . well . . thinking about the name as a whole . . "Gia Apple" . . hmm . . I'm uh . . well . . in accordance with referring to the name . . to the person . . from [1.1] . . who uh . . I uh . . uh . . think is called "Gia Goodrich" . . uh . . well . . uh . . it seems uh to me . . that . . the tone or the . . sense that I get from . . the name . . "Gia Apple" . . would also perhaps . . uh . . be a good match . . for . . the . . person . . "Gia Goodrich" . . ]



Notes @ Newly Created Words

Afrco
[Written @ December 3, 2020 @ 1:40]
[I accidentally typed the word . . "Afrco" . . instead of typing the word . . "Afro-Asian" . . I'm sorry . . I'm not really sure . . what this word . . "Afrco" . . should mean . . at this time . .]


Notes @ Newly Learned Words

Backlight [1.2 @ ]

Side light [1.2 @ ]


[1.2]
HOW TO LOOK BETTER ON ZOOM [ How to Light & Angle Your Screen to Look Your Best]
By Gia Goodrich
https://www.youtube.com/watch?v=YO6utfQm7Bg

[Written @ 1:25]

Notes @ Newly Discovered Programs

StreamYard [1.1 @ Top Right Corner of the Display]


Notes @ Newly Discovered YouTube Channel


VA3FUC HAM Radio
https://www.youtube.com/c/VA3FUCHAMRadio/videos
[Discovered by [1.1 @ Video Publisher]]



[1.1]
Time to study with Ron
By VA3FUC HAM Radio
https://www.youtube.com/watch?v=rFbgkVN98-Y
[Discovered by Video recommendation for [1.0]]

[2.1]




References:

[1.0]
Ecoin #389 - Live development journal entry for December 2, 2020
By Jon Ide
https://www.youtube.com/watch?v=VSinz0jsGvY






