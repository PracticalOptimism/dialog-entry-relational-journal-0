

# Day Journal Entry - December 1, 2020


[Written @ 23:55]

Notes @ Newly Discovered Words

Social Meditation Circles
[Written @ December 1, 2020 @ 23:55]
[I sometimes watch videos on YouTube . . and not necessarily knowing why I am so interested in watching them . . for me . . I just now had the insight . . or the word came to mind . . as I was looking at the front page of YouTube . . I saw . . [1.1] . . and [1.2] . . and . . well . . I think one uh . . well . . uh . . the word . . came to mind . . and that word . . was . . "Social Meditation Circles" . . or . . maybe it could have been . . "Social Meditation Groups" . . and I was really surprised to hear this word . . appear . . uh . . in a seemingly . . spontaneous way . . where I didn't really . . realize . . uh . . or uh . . maybe I didn't really suspect that it would would come to mind . . but the word . . "Meditation" . . uh . . to me . . uh . . suggests that maybe . . something is being . . uh . . considered . . or thought about . . in a silent setting where . . listening is taking place . . and information is being observed . . and so . . uh . . for example . . meditation uh . . means . . "silent observation" . . or maybe . . "silent singing" . . or uh . . well . . actually . . "silent singing" . . is also a new term for me . . let me write that down . . one moment . . [a few moments later . . at December 2, 2020 @ 0:03] . . uh . . well . . social meditation circles . . uh . . from how I felt about the term when it came to mind . . I started to feel the emotion of . . gathering around someone to listen to their story . . and so for example . . uh . . not necessarily . . judging . . or . . uh . . anticipating . . any particular behavior . . or attitude . . or style of speech . . but . . sitting still and . . absorbing the environment created . . by the social person . . or by . . the group of people that developed . . the product . . or the video . . or uh . . I don't know . . -_- . . hmm . . -_- . . -_- . . -_- . . -_- . . hmm . . -_- . . -_- . . -_- . . it's kind of a weird uh . . I don't know . . I guess when I click on videos on the internet . . I am usually anticipating some sort of information . . -_- . . -_- . . so what does it mean to watch without anticipation ? . . -_- . . -_- . . -_- . . hmm . . well the term sounded cool in my head . . -_- . . uh . . I also imagined people sitting in a room quietly with one another . . and . . watching videos together like in a meditation circle . . but . . uh . . for uh . . listening to . . uh . . people's stories on the internet . . uh . . like . . maybe watching uh . . random videos on the internet or something . . -_- . . hmm . . -_- . . -_- . . -_- . . I think this already happens and I was making a big deal of this term . . "Social Meditation Circles" . . -_- . . hmm . . well I don't know what to say . . it seems like uh . . -_- . . I don't know . . maybe now I'm feeling I was trying to draw empathy for other people out from the audience's souls as they read these . . comments -_- . . well what a barbarian . . tactic . . -_- . . I'm sorry . . ]


Silent Singing
[Written @ December 1, 2020 @ 23:59]
[I have heard the term . . "silent toning" . . somewhere I think . . uh . . the . . uh . . Pleiadian . . books . . uh . . by . . Barbara Marciniak [1.1] [2.1] . . are a source from which I think I've heard the term . . "silently speaking circles" . . and . . "silent toning" . . being used . . uh . . to talk about the phenomenon . . of . . silent . . sounds . . that influence . . our daily lives . . or something like that . . and so for example . . the silent . . sounding . . energy . . of the planet earth . . can be multitudinous . . uh . . and varied . . uh . . and it all gives us a sort of . . weather pattern for our experience of Earth . . or something like that -_- . . I'm not really sure how to describe . . this phenomenon myself . . uh . . well . . another resource . . that I've . . uh . . heard . . talk about this . . topic . . of . . silent . . energy . . in the forest . . is . . [3.1 @ 8:18 - 13:48 @ specifically @ 11:24 - 12:31]]


[1.1]
Earth: Pleiadian Keys To The Living Library
By Barbara Marciniak
[Book]
"silent sounding" Page 216
"silently speak" Page 60
"silently speaking circles" Page 241

"You don't have enough silent time to notice that sound is continuously being directed at you to alter your bodies. When you make the sound yourself by instrumentation or toning, you balance the adjustment because you work from the inside, not just from the outside. This is how you are being adjusted at this time." Page 213

[2.1]
Bringers of the Dawn
By Barbara Marciniak
[Book]

[3.1]
A time of transitions | Living life in the mountains - Story 47
By Jonna Jinton
https://www.youtube.com/watch?v=gVz7VYhmrMk


. . . 




[Written @ 23:46]

Notes @ Random Thoughts

git push origin master
git push origin mini

. . the name . . "git push origin mini" . . came to mind . . a few moments ago . . 

[Written @ 23:18]


Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:55:09]

- 📝 [commentary] I took a shower . . while listening to [2.0]

[0:55:09 - 1:24:29]

- 📝 [commentary] I took notes . . in the dialog entry relational journal . .
  - (1) . . [1.0 @ Day Journal / 2020 A.D. / Month 12 - December / December 1, 2020]
  - (2) . . [1.0 @ @Document Template / YouTube-Live-Stream-Video-Description]
  - (3) . . [1.0 @ @Document Template / Activity-Music]

[1:24:29 - End of Video]

- 📝 [commentary] Salutations


References:

[1.0]
Dialog Entry Relational Journal 0
By Jon Ide
https://gitlab.com/practicaloptimism/dialog-entry-relational-journal-0


[2.0]
Lit Shower Playlist | Favorite Smooth/Sensual Songs
By Sailor Goon
https://www.youtube.com/watch?v=_jx8g4Tc3l8





Live Stream Video Duration for the previous live stream . . 



[Written @ 5:24]

06:15:09.648

[Written @ 5:19]

"When the electronic waves of pain and beauty are combined together, this causes the IS-BE to get "stuck" in the body" . . . Page 116 . . Alien Interview . . "The "trigger" which attracts IS-BEs to inhabit a human body, or any kind of "flesh body", is the use of an artificially imprinted electronic wave which uses "aesthetic pain" to attract the IS-BEs." Page 116 . . Alien Interview


[Written @ 4:31]

Notes @ Newly Learned Words

Auro



[Written @ 3:54]

tolopol
[Written @ December 1, 2020 @ 3:54]
[I accidentally typed the word . . "tolopol" . . instead of typing the word . . "topology" . . I'm sorry . . I'm not sure . . what this word . . 'tolopol" . . should mean . . at this time :O . . ]


iacc
[I accidentally typed the word . . "iacc" . . instead of typing the word phrase . . "I accidentally" . . I'm not sure what this word . . "iacc" . . should mean at this time . . although . . iacc . . seems like an acronym for an organization :O . . ]

[Written @ 3:52]

I like to read your programs. Thanks for writing your programs.

(1) Message Intention Description

The intention of this email is to . . do this or that . . uh . . you can express your feelings here if you would like . . uh . . whatever you feel would help communicate your intention . . for . . uh . . the purpose . . of the message . . or why the message was created . . or why the message was made . . uh . . uh . . or whatever . . 

(2) Message Anticipation Description

The anticipation for what the reader should anticipate . . this could be . . a synopsis or a short summer of the . . entire message . . uh . . including things like . . uh . . contact information . . uh . . well . . uh . . it's basically a reflection of the message structure but it can be summarized to provide . . information on what the reader ought to anticipate . . or uh . . not necessarily ought to anticipate . . but you would rather prefer to provide those things . . right away that you know that they'd come to anticipate . . for example . . if maybe uh . . it's a communication that you're making with someone . . having a way to respond to your message is really uh . . something that someone could uh . . come to anticipate . . uh. . . after seeing . . a message being sent to them . . uh . . -_- . . maybe I'll see if I can provide a fun analogy . . okay . . the analogy will be about ants . . uh . . maybe now you are starting to anticipate . . the story may involve the small legs of an ant . . or maybe the story might involve . . other insects . . uh . . like ants . . uh . . maybe . . uh . . butterflies . . uh . . or bees . . or beetles . . uh . . or wasps . . uh . . okay . . well . . uh . . -_- . . maybe you would want to communicate with an ant . . and you walk by an ant one day . .-_- . . -_- . . anticipation . . hmm . . hmm I think I'm still figuing our what this . . means . . uh . . well . . uh . . it seems that . . anticipation can relate a lot to the community . . and so for example . . depending on the community you are observing . . that community may anticipate one thing . . uh . . more than another thing . . if the community . . is English . . language . .  speakers . . then . . maybe they are anticipating the document to be written in English . . and if the community is . . Chinese . . language . . speakers . . then maybe . . your audience didn't anticipate to . . read a message written in English . . and so . . to account for this . . you can maybe have a list of the languages that the document is written in . . uh . . as part of the anticipation description . . and so for example . . if you would like your message to be . . able to meet the anticipations . . of Chinese language . . readers . . uh . . or Korean language . . readers . . or . . Professors in University . . or . . uh . . people in . . uh . . middle school . . or . . uh . . college . . or . . elementary school . . or in graduate school . . or in a trade school . . or in a nursing home . . or in a prison . . or in a jail school . . or in a . . barber shop school . . or in a cooking school . . or in a social archival school . . or in a penitentiary . . uh . . depending on the anticipation of the type of information you think your consumers will be interested in receiving . . for example . . a table of contents . . for summarizing . . the document . . -_- . . uh . . -_- . . -_- . . I'm still doing research in this . . anticipation topic . . but . . uh . . I think uh -_- . . uh . . -_- maybe more information will be provided for the things that belong here . . possibly . . uh . . you can uh . . replicate the document structure and provide a shorter summary of all of the elements so someone can receive all of the information in one single bite . . or in one single look at the outline . . and so no one needs to go through the full document to get all of the . . uh . . summarized important . . highlighted news updates that they would really care to see if for example they are repeatedly visiting the document structure . . the anticipated description can make things easier to easily access . . upon fast . . and multiple . . repeated visits. . in case any of the information changes . . it can easily be accessed again quickly . . upon the 100th visit of the day by that same individual . . or upon the 1000th visit for the day by that individual . . and so the more dense the information is . . uh . . in terms of the information that is really uh characteristic of what an reader or a visiter would see . . uh . . then that is really great . . to place in the anticipation section . . uh . . high density . . uh . . in a consumable uh . . uh . . time period uh . . for example . . people's uh . . attention spans for how long they would like to receive information could maybe uh . . be anticipated to be within the few seconds . . to understand what something is about . . uh . . and so if someone can read the content in a few seconds . . for example . . the title of the project . . or the description of the project . . or contact information or news update sources . . or . . community feedback . . or . . the performance quality measurements . . of the codebase . . in only a few seconds . . all of that information is valuable . . to be placed . . in the anticipation description of the message . . 

(3) Message Title

Message Communication Format

(4) Message Description

This message is an introduction to how to structure a message. You can structure a message in any way that you like . . and yet . . in structuring a message along this way of . . structuring . . you can learn more about . . how to organize information . . that includes . . a . . uh . . maybe a formal way to anticipate . . what other things you may be interested in . . communicating . . 

(5) Message Benefits

- This section contains a list of your thoughts on why the message is beneficial . . 

- Learn: Learn more about this topic
- Healthy Feelings: Feel good about yourself by being involved in these activities . . working out at the gym . . reading books . . riding bicycles . . walking on trails outside near trees . . taking care of your garden . . gardening plants . . gardening . . farming related activities . . being around other animals . . being around other creatures . . being around other people . . being around other . . plants or animals . . being around . . other people that think like this . . or think like that . . or being around other people . . that are involved in this or that activity . . ultimate frisbee . . kayaking . . trailwalking . . mountain bicycling . . freestyle . . driving activities . . freestyle . . dancing . . illusionary sciences . . the study of illusions . . the study of illusory information . . the study of awareness . . the study of feelings . . the study of consciousness . . the study of love . . the study of . . existence . . the study of principles . . the study of thinking . . philosophy . . the study of anticipatory information . . anticipating certain things to appear . . the study of latent information . . the study of immediate information . . 
- Social Status: Earn social credit by this group of people by being involved in things in this or that way
- 

(6) Message Features

- Hello: Be greeted
- Question: Be asked a question
- Invitation: Be invited to an event
- Criticism: Be criticized
- Friend: Start a friendship
- Mentor: Start a mentorship
- Work: Start a work relationship
- Romance: Start a romantic relationship
- Inform: Be informed about information about a list of topics (1) hairstyles (2) gigantomastia (3) nipple piercings (4) nose rings (5) spirituality (6) thoughts and feelings etc.
- Request: Be requested to provide information about a list of topics (
- Provide: Be provided a resource or product . . I would like to send you my book . . on this or that topic . . I would like to send you a seed to plant if you would like tomatoes . . I would like to send you a T-Shirt . . would you be interested in receiving a pair of stickers for $2.99 . . I would like to provide you
- Consume: Be requested to consume a particular information source . . I would like for you to look at my book manuscript . . I would like you to look at this document . . 
- You can name the feature whatever you like . . uh . . well . . uh . . hmm . . if you follow the message communication structure, you can possibly find a way to list your features in accordance with the . . structure . . uh . . this is really an experimental structure . . I don't know what is possible . . uh . . and possibly you can find new things to consider . . uh . . but for the most part I've done a lot of my research in previous . . uh . . episodes . . of the Ecoin . . uh . . uh . . live stream . . and . . uh . . it's uh. . . some of the research is documented in the dialog entry relational journal . .  

- ?? I don't know . . what else . . uh . . whatever you're trying to uh . . provide by your message . . you can place here . . in the features section . . 

(7) Message Limitations
- Maybe your message has limitations that you've identified . . list those limitations . . for example . . if you are . . writing a book . . uh . . but it's . . incomplete . . 
- Work-In-Progress: This Book is a work in progress . . Maybe reading the book doesn't give the feelings that I was anticipating for the reader . . the book doesn't work the way I want at this time . . but it's a manuscript and so . . blah blah blah . . the reasons you come up with can be expressed by yourself in your own words . . I'm not really sure . . uh. . . I myself am still writing about this uh . . message format and so I'm not sure how things should be . . if you're interested to learn more you can visit . . the dialog entry relational journal . . development journal to learn more about the development process of this . . message . . format . . which . . really uh . . started . . as a project documentation format . . for example . . in how you could format the project document readme.md file . . and how you could format the project codebase . . for a project . . software codebase . . uh . . like a program for . . writing a web browser website . . or for writing . . a . . javascript library . . something like that . . to learn more . . about . . about the document structure . . you can also visit . . https://gitlab.com/ecorp-org/ecoin . . which provides information . . on this document structure . . by providing an example . . readme.md file . . that is up to date . . with . . some of the latest . . ideas . . on . . uh . . what uh . . I've . . uh . . been thinking . . in this regard . . 

(8) Message Additional Notes

(9) Message Contact Information

- Message Contact #1
- Message Sender: <Your Preferred Identity Here>
- Message Sender Contact Information:
  - Email Address: <Your Email Address Here>
  - HTTP Web Address: <Your HTTP Web Address>
  - DAT Web Address: <Your DAT Web Address>
  - IPFS Web Address: <Your IPFS Web Address>
  - YouTube Channel: <Your YouTube Channel Name>
  - Twitter Account: <Your Twitter Account Name>
  - Instagram Account: <Your Instagram Account Name>
  - Facebook Account: <Your Facebook Account Name>
  - 

- Message Contact #2
  - Message Sender Mother: <Your Mother's Preferred Identity Here>
  - Message Sender Mother Contact Information:
    - etc.
- Message Contact #3
  - Message Sender Father: <Your Father's Preferred Identity Here>
  - Message Sender Father Contact Information:
    - etc.
- Message Contact #4
  - Message Sender Sponsor: <Your Sponsor's Preferred Identity Here>
  - Message Sender Sponsor Contact Information:
    - etc.
- Message Contact #5
  - Message Sender Professor: <Your Professor's Preferred Identity Here>
  - Message Sender Professor Contact Information:
    - etc.
- Message Contact #6
  - Message Sender Sponsor: <Your Sponsor's Preferred Identity Here>
  - Message Sender Sponsor Contact Information:
    - etc.
- Message Contact #7
  - Message Sender Sponsor: <Your Sponsor's Preferred Identity Here>
  - Message Sender Sponsor Contact Information:
    - etc.
etc.

(10) Message News Updates
- Ecoin README.md: Learn more about the message communication format by visiting the Ecorp-Org/Ecoin codebase: https://gitlab.com/ecorp-org/ecoin . . if you copy that data structure for the readme.md . . that should provide you a complete . . guide on . . what to anticipate . . in terms of the possibilities of the things . . that you may or may not want to communicate . . for example . . you may want to communicate information about other community members that have received the message before . . and so if it's a friend request . . for example . . and . . you have sent the friend request to other . . individuals . . or other members of the community of people that are able to be communicated with . . or uh . . maybe it's uh . . related to any particular uh . . groups of communities that you've come to identify with . . and so for example . . uh . . if you sent a friend request message . . to . . uh . . a group of . . uh . . professors at . . University #1 . . you can . . also list that information . . of . . what the community feedback was . . for . . having uh . . received . . a message like that . . uh . . from your uh . . resource . . uh . . or from your personhood or from your uh . . resource provider . . uh . . as I suppose you're the provider for the message . . and so . . uh . . you could . . uh . . have community feedback on what other . . uh . . recipients of the message have thought . . for University #1 . . uh . . and . . have a list . . uh . . to compare . . with . . University #2 . . responses . . or uh . . it doesn't necessarily need to be universities . . uh . . for example . . Sponsor Group #1 . . or Sponsor Group #2 . . and so for example . . maybe you've sent the message to . . uh . . Sponsor Group #1 which are a list of banks or government resource providers . . to be friends or something like that . . and then . . Sponsor Group #2 . . can be . . teachers and students in classrooms . . uh . . at uh . . various uh . . locations that you've come to identify . . whether it's a particular region of the world . . or a particular uh . . caste of people . . or a particular uh . . age range of people . . or gender of people . . and so on and so forth . . and so the community feedback . . on having sent the friend request to other . . uh . . community members . . uh . . can also be . . uh . . something you would wish to provide for more clarification on the context of your message . . or something like that . . 

- Jon Ide / Dialog Entry Relational Journal 0: https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0 . . this is a great resource to learn about the development of this message communication format . . or this project . . data structure . . or this project data structure . . uh . . as far as I've come to uh . . think about this data structure so far. . it seems that this data structure can be quite comprehensive in allowing you to communicate all types of information . . from uh . . information about a video . . or a movie . . or information about . . uh . . a particular topic . . such as . . uh . . a topic that you would read from a book . . such as biology . . or physics or chemistry . . uh . . or maybe even English literature . . I'm not sure . . uh . . I haven't thought about English literature very much as a usecase for the document structure . . to also satisfy . . uh . . well . . uh . . hmm . . never mind . . I have thought about . . literature in the sense . . that . . uh . . stories can be developed in the same way . . as following this document data structure . . and so for example . . if you want to write a story . . uh . . well you can go about the story in many ways . . uh . . your story has benefits . . uh . . in terms of what is the benefit for reading the story . . or uh . . well . . if your story has uh . . people for example . . people as characters in the story for example . . then . . uh . . each character has benefits . . uh . . in accordance to what benefit they are perceived to provide by the community and what benefits they perceive to provide from their own perspective . . uh . . maybe . . your characters also have limitations . . such as . . uh . . societal limitations or . . maybe . . super power limitations like they're not allowed to climb on walls . . or they're not allowed to use jetpacks to fly around in the city . . or maybe . . the character . . has to read at least 1 book every day . . for several years . . and so their whole storyline . . is based around this . . key limitation . . that not every other character of the story necessarily has . . uh . . and so if one character perceives one thing to be beneficial . . that's uh . . not necessarily the case for another character . . and so for example . . a benefit . . from one character's perspective . . can be a limitation to another character's perspective . . and so . . uh . . well . . reading books maybe a great idea to one character . . reading at least 1 book a day . . for several years may be beneficial to . . characters like . . Bill Gates . . or Warren Buffet . . or . . John Fish . . or maybe other uh . . people or other human beings or other . . uh . . insect flyhawk creatures as you've come to identify them in your book . . uh . . but . . maybe to a dancer or . . musician . . or a painter . . the reading of 1 book each day for several years can be a limitation that undermines their performance in the tap dancing world of possibilities . . but that's only an idea . . and I'm not sure . . you can apply . . this type of thinking . . on and off as you would prefer I suppose . . I don't know -_- . . 


(11) Message Community Restrictions & Guidelines
- (for example: this message is intended to be read by Jon Ide only) . . 
- (for example: this message is meant to be read by anyone in the Skateboarding 101 for professor Tony Hawk)
- etc.

(12) Message Codebase
- How to rebrand the message:
 - you can write information on how to . . change the message . . to . . for example . . fit a new purpose or something like that . . I'm not sure . . this is maybe up to you in terms of how uh . . customizable the message is . . if you can change a few words here or there and the message can then be used for another purpose . . uh . . then maybe . . the recipient would appreciate having that information available to them . . and so for example . . uh . . if the message is a product offering . . or a product advertisement . . you can maybe mention where the . . uh . . name for the product was used through the message . . (line 1, 2, 3, 129, 345) etc . . and have the recipient of the message . . be able to . . change the words at those specified locations . . and so that allows them to know . . how to quickly edit the message . . which is representative . . of a codebase . . uh . . the uh . . codebase of the message uh . . contains the details of what is being communicated . . or what is . . uh . . being intended to be communicated . . uh . . and so . . uh . . well . . right . . information on how to rebrand . . can be placed here . . if for example . . you anticipate that your message would be useful for other purposes . . and not only the specific usecase that you had imagined . . and so you can take advantage of that anticipation . . by perhaps providing further information for the recipient to be clear about . . in case they really do need . . to apply that . . particular . . anticipated . . item of interest . . for example . . maybe the recipient would themselves enjoy to re-apply the message for . . writing a book . . or for . . uh . . selling . . a . . book . . or for . . uh . . communicating . . uh . . uh . . something to a particular group of people . . and so . . even if the message you originally sent was specified towards . . uh . . maybe . . "would you like to buy my candles?" . . or . . "would you like to buy my coffee cup?" . . or . . "would you like to buy my notebook journal?" . . uh . . well . . if the recipient . . happens to . . uh . . have the interest . . in using your promotional message for their own work . . then . . uh . . providing information like . . the word . . "coffee cup" . . was placed . . in . . this or that location . . and if you want . . it's possible to replace that word phrase with another word phrase . . because of the way the message was written . . you can replace "coffee cup" with . . "broom stick" . . and the message would still be able to read clearly . . or maybe you would uh . . need to change a few other items . . like the feature description list . . and so you that branding information can be provided here . . 


(13) Message Provider Resource
- please give me some time to think about this
- my initial thinking is that this is resources related to 3rd party resources that are important to know about with regard to the message . . References . . uh . . News Sources . . Information Sources for any particular part of the message . . if the provider for a particular section or for a particular part of the message was your mother for example . . then you may place their provider information here . . if . . there is a reference for a scientific . . doctrine . . or a scientific . . uh . . comment where more information is . . intended to be specified . . then that provider's information can be located here as well . . If there is a funding group that funded the message to be sent . . then you can provide that provider's . . information . . the . . funding group's information . . here in the provider resource . . area of the message . . 


(14) Message Consumer Resource
- If . . you would like your message to be consumed in a variety of . . ways . . you can . . list how your . . message . . is available to be consumed . . here . . and so for example . . perhaps . . the message you would like to send . . is meant to be . . interpretted . . within a particular social context . . maybe . . in a communist society . . your message may be interpretted . . through a particular lens . . that can be accounted for . . by writing about how to interpret the message in that particular context . . in this consumer resource section . . if the message . . is not necessarily suitable to individuals . . who . . are . . in pre-school . . and just starting out to learn how to read . . or only starting out to learn how to read . . then you can provide . . more information here . . on how to . . consume the message . . if you're a Kindergartener . . uh . . -_- . . for example . . maybe you could use simpler words . . like big and small . . or easy words that are already . . known . . at an early reading level . . or maybe you can provide . . pictures . . or . . cartoon . . movies . . the message body can be extended to include all the information that you would prefer . . uh . . for example . . so long as the . . messaging system is allowing that particular way of instrumenting how your message would be preferred to be consumed . . and so if you can add a movie to the document . . then that is really cool and so your message is self contained . . and you don't necessarily need to have references to outside resources . . if that is available to happen . . uh . . for example . . there may be limitations of the message such as . . character length . . and so . . if only 4000 . . text characters . . are . . allowed . . then you may be able to . . represent . . uh . . all of the information that you would have otherwise been interested in sharing for that particular message . . 

(15) Message Usecase Resource
- a greeting


(16) Message Community & Volunteers
- Here you can write information about . . the community around the message that you've written . . if the message is inspired by any particular group of people . . that can be stated here . . or by any particular . . social documents . . or community documents . . uh . . that can be stated here . . If the message was . . uh . . written by multiple people . . or by multiple providers . . uh . . like uh . . computer people for example or uh . . I don't know . . whatever or whoever you would like to consider a voluntary community member . . for having helped produce the message . . document . . uh . . then you can list those voluntary . . uh . . entities . . or voluntary . . community members . . here . . uh . . for example . . maybe your plumber . . helped . . by helping you fix your toilet . . and that gave you inspiration to continue your work . . in a more pleasureful . . or in a more pleasing environment . . or maybe your mother helped you do your laundry . . and without their love and support . . you wouldn't have come . . so far in the same pleasureful conditions that you're familiar with . . uh . . or maybe . . your spouse . . or romantic partner helped to inspire you . . along certain lines of thinking . . and so all of the names for all of the community member . . uh . . spouses . . and . . parents . . and . . uh . . friends . . and . . research assistants . . and postdoctoral research students . . and plumbers . . and garbage truck people . . garbage truck men . . garbage truck women . . garbage truck genderless entities of a common force of interest . . in regards to the project codebase . . :O

(17) Message Usecase Provided By Related Community Messages
- If your message is about a work relationship . . to work with someone . . on a project . . or something like that . . well . . uh . . there are possibly other . . messages . . that have been created . . by people . . uh . . I suppose -_- . . to uh . . also message about work relationships . . 

- Messages about work relationships sent by other community members
  - John Doe sent a message to Jane Doe . . about working together . . other community members have thought highly about the message that John Doe sent to Jane Doe . . and so maybe consider taking into account that message if you would not like to consider this message as a resource to coerce you in working together . . and just pretend that . . I am John Doe . . and that you are Jane Doe . . 
  - Alice and Bob sent a message to Carol and Dave to invite them to work together . . The message resource can be seen at this reference location: https://carol-and-dave-lets-work-together.com/thanks-by-alice-and-bob . . and so if you would like to consider . . working together under the merit of what was written . . here . . then you are welcome to use this as a resource to consider the possibilities of our trade agreement to work together . . 

- Messages about work relationships sent by other community members while also satisfying this or that criteria that the present message satisfies as well
  - We would like to work with you with regards to . . topic (1) . . topic (2) . . and topic (3) . . the previous . . community messages to encourage working together . . weren't necessarily specified in these topic areas . . https://here-is-a-resource-that-specifies-working-togther-for-topic-1.com . . https://here-is-a-resource-that-specifies-working-togther-for-topic-2.com . . https://here-is-a-resource-that-specifies-working-together-for-topic-3.net . . if you are interested in all three . . facilities being satisfied in a message such as this message . . please visit the following resources . . https://resource-1.com . . https://resource-2.com . . https://resource-3.com . . and . . https://resource-4.com . . 

- Messages about work relationships sent by other community members

etc.







