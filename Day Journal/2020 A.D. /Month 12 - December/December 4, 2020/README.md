

# Day Journal Entry - December 4, 2020

[Written @ 4:35]

Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[x:xx:xx - x:xx:xx]

- 📝 [commentary] I took notes on topics of interest

[x:xx:xx - x:xx:xx]

- 📝 [commentary] I took notes on topics of interest



[Written @ 4:34]

### Live Stream Checklist for Ecoin #392 - December, 4 2020

Greetings

* [] Greet the viewers
* [] Plan the tasks to complete
* [] Live stream url:
* [] Live stream duration: 

Health and Comfort

* [] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [] Sit or stand in a comfortable position

Self Love

* [] Practice intentional smiling
* [] Practice gratitude meditation and prayer
* [] Practice self-love (hug yourself technique)
  - [] Mindfulness Tool: Hug Yourself By mindfulnesstoolstv https://www.youtube.com/watch?v=x-9J0Lzq6Iw&ab_channel=mindfulnesstoolstv
* [] Practice intentional breathing
* [] Practice singing and toning (as recommended by Unicole Unicron [1.0 @ 3:11] and the Pleiadians [2.0 @ Page 150])
  - [] Fiddler on the roof - If I were a rich man (with subtitles) By guru006 https://www.youtube.com/watch?v=RBHZFYpQ6nc&ab_channel=guru006
  - [] Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4&ab_channel=SikhMantras

Personal Goals

* [] Live stream for at least 3 hours
* [] Start Live streaming at 12:00

Music

* [] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [] 米津玄師 MV「パプリカ」Kenshi Yonezu / Paprika By  米津玄師 https://www.youtube.com/watch?v=s582L3gujnw
  - [] Dreams - Preface by Seth By Tim Hart Hart https://www.youtube.com/watch?v=2dtXI2RATIw&list=PLPDTOFbrYdqCBF9qmNqqxZaSj4lLm537Q&index=1
  - [] Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [] Tao Te Ching     (The Book Of The Way)     #Lao Tzu                       [audiobook]   [FREE, FULL] By Peter x https://www.youtube.com/watch?v=o2UYch2JnO4&t=1s
  - [] The Seth Material - Introduction (1 of 2) By Tim Hart Hart https://www.youtube.com/watch?v=Jlcos5ZM2NE&list=PLPDTOFbrYdqA0vpIROtyd3tXbzkK49E5T
  - [] [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs
  - [] Instant Calm, Beautiful Relaxing Sleep Music, Dream Music (Nature Energy Healing, Quiet Ocean) ★11 By Sleep Easy Relax - Keith Smith https://www.youtube.com/watch?v=4zqKJBxRyuo
  - [] Bhaktas- The Cosmic Mantra! Meditation Music ! By Hareesh Sahadevan https://www.youtube.com/watch?v=O3wQBWIOCQA
  - [] [ 𝑷𝒍𝒂𝒚𝒍𝒊𝒔𝒕 ] aesthetic song • lofi type beat • 3 hours By  연우yanu https://www.youtube.com/watch?v=cbuZfY2S2UQ
  - [] The Nature of Personal Reality - Introduction by Jane Roberts By Tim Hart Hart https://www.youtube.com/watch?v=8GkzYqJmpGM&list=PLPDTOFbrYdqBBIu1k5ubY7OBKcy5Xc5co&index=1&ab_channel=TimHartHart
  - [] 2 hours Study With Me (with talking during breaks) By Study Vibes https://www.youtube.com/watch?v=MC6yJ4BvmKY&ab_channel=StudyVibes
  - [] Dreams of Japan 🍃 lofi hip hop mix By Dreamy https://www.youtube.com/watch?v=SXfupJIXtRA&ab_channel=Dreamy

Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
- (1) youtube live stream chat window
- (2) youtube + firefox (picture-in-picture mode)
- (3) iTerm
- (4) https://www.timeanddate.com/stopwatch
- (5) Brave Browser, Firefox, Firefox Nightly, Firefox Developer
- (6) Visual Studio Code
- (7) ProtonVPN [previously, Express VPN]
- (8) Visual Studio Code [previously, "Notes" Application on Macbook Laptop]

Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - []

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream

Checklist References:

[1.0]
CAM CHURCH- TRANSMUTATION
By Unicole Unicron
https://www.youtube.com/watch?v=cKvcQpPrjY4

[2.0]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak


[Written @ 4:32]

Notes @ Newly Discovered Video

Tao Porchon-Lynch on how she started teaching yoga.
By Magnificent Midlife
https://www.youtube.com/watch?v=5GPGDvYn7Qk


[Written @ 3:55]

Notes @ Random Thoughts That Came To Mind . I Thought It Was Funny . 
Notes @ Random Funny Thoughts That Just Came To Mind . 

"
The New Kind of Blue Sciences Are Basic Blue
"

"
The New Kind of Blue Sciences Are Blue Steal
"
[Possible Alternative . . that came to mind at approximately 4:30 . . hun . . ]

[Written @ 2:24]

Notes @ Newly Created Words

reql
[Written @ December 4, 2020 @ 2:30]
[I accidentally typed the word . . "reql" . . instead of typing the word . . "really" . . I'm sorry . . I'm not sure what this word . . "reql" . . should mean . . at this time . . ]

pangel
[Written @ December 4, 2020 @ 2:24]
[I accidentally typed the word . . "panel" . . as . . "pangel" . . I'm sorry . . I'm not sure . . what this word . . "pangel" . . should mean . . at this time . . although . . it is a word that looks really . . quite . . cool . . uh . . it reminds me of the word . . "angel" . . but there's uh . . something else that makes it look really quite cool . . "pan" . . uh . . "pangel" . . uh . . it seems like a particular type of angel maybe . . I'm not sure . . or it reminds me of an angelic figure of some sort . . I'm not sure . . uh . . hmm . . the colors that come to mind . . are . . a . . uh . . rustic . . uh . . rustic . . uh . . creamy . . uh . . rustic . . creamy white . . or uh . . something like . . light . . brownish . . tannish . . uh . . or something like that . . the style reminds me of . . "Devan Christine" . . uh . . in terms of their uh . . uh . . color . . uh . . preference . . uh . . since . . uh . . I guess . . uh . . I learned the word . . "rustic" . . from their YouTube channel . . uh . . recently . . [1.0 @ 7:03] . . hmm . . well . . I'm uh . . not really sure what this word . . "pangel" . . should mean . . at this time . . I'm sorry . . ]


[1.0]
Vlogmas 2020 Day 1
By Devan Christine
https://www.youtube.com/watch?v=Y9ukev-sCGs


[Written @ 0:48]



[Written @ 0:45]

### Live Stream Checklist for Ecoin #391 - December, 4 2020

Greetings

* [x] Greet the viewers
* [x] Plan the tasks to complete
* [x] Live stream url: https://www.youtube.com/watch?v=xFU_SzVEHVA
* [x] Live stream duration: 03:40:17.622

Health and Comfort

* [x] Have drinking water available
* [x] Use the toilet to release biofluids and biosolids
* [x] Sit or stand in a comfortable position

Self Love

* [x] Practice intentional smiling
* [] Practice gratitude meditation and prayer
* [] Practice self-love (hug yourself technique)
  - [] Mindfulness Tool: Hug Yourself By mindfulnesstoolstv https://www.youtube.com/watch?v=x-9J0Lzq6Iw&ab_channel=mindfulnesstoolstv
* [] Practice intentional breathing
* [] Practice singing and toning (as recommended by Unicole Unicron [1.0 @ 3:11] and the Pleiadians [2.0 @ Page 150])
  - [] Fiddler on the roof - If I were a rich man (with subtitles) By guru006 https://www.youtube.com/watch?v=RBHZFYpQ6nc&ab_channel=guru006
  - [] Ajeet Kaur Full Album - Haseya By Sikh Mantras https://www.youtube.com/watch?v=_cX70nrrvM4&ab_channel=SikhMantras

Personal Goals

* [x] Live stream for at least 3 hours
* [] Start Live streaming at 12:00

Music

* [] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)
  - [] 米津玄師 MV「パプリカ」Kenshi Yonezu / Paprika By  米津玄師 https://www.youtube.com/watch?v=s582L3gujnw
  - [] Dreams - Preface by Seth By Tim Hart Hart https://www.youtube.com/watch?v=2dtXI2RATIw&list=PLPDTOFbrYdqCBF9qmNqqxZaSj4lLm537Q&index=1
  - [] Lao Tzu - The Book of The Way - Tao Te Ching + Binaural Beats (Alpha - Theta - Alpha) By Audiobook Binaurals https://www.youtube.com/watch?v=-yu-wwi1VBc
  - [] Tao Te Ching     (The Book Of The Way)     #Lao Tzu                       [audiobook]   [FREE, FULL] By Peter x https://www.youtube.com/watch?v=o2UYch2JnO4&t=1s
  - [] The Seth Material - Introduction (1 of 2) By Tim Hart Hart https://www.youtube.com/watch?v=Jlcos5ZM2NE&list=PLPDTOFbrYdqA0vpIROtyd3tXbzkK49E5T
  - [] [ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music By  Nhạc sóng não chính gốc Hùng Eker https://www.youtube.com/watch?v=4MMHXDD_mzs
  - [] Instant Calm, Beautiful Relaxing Sleep Music, Dream Music (Nature Energy Healing, Quiet Ocean) ★11 By Sleep Easy Relax - Keith Smith https://www.youtube.com/watch?v=4zqKJBxRyuo
  - [] Bhaktas- The Cosmic Mantra! Meditation Music ! By Hareesh Sahadevan https://www.youtube.com/watch?v=O3wQBWIOCQA
  - [] [ 𝑷𝒍𝒂𝒚𝒍𝒊𝒔𝒕 ] aesthetic song • lofi type beat • 3 hours By  연우yanu https://www.youtube.com/watch?v=cbuZfY2S2UQ
  - [] The Nature of Personal Reality - Introduction by Jane Roberts By Tim Hart Hart https://www.youtube.com/watch?v=8GkzYqJmpGM&list=PLPDTOFbrYdqBBIu1k5ubY7OBKcy5Xc5co&index=1&ab_channel=TimHartHart
  - [] 2 hours Study With Me (with talking during breaks) By Study Vibes https://www.youtube.com/watch?v=MC6yJ4BvmKY&ab_channel=StudyVibes
  - [x] Dreams of Japan 🍃 lofi hip hop mix By Dreamy https://www.youtube.com/watch?v=SXfupJIXtRA&ab_channel=Dreamy

Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application
- (1) youtube live stream chat window
- (2) youtube + firefox (picture-in-picture mode)
- (3) iTerm
- (4) https://www.timeanddate.com/stopwatch
- (5) Brave Browser, Firefox, Firefox Nightly, Firefox Developer
- (6) Visual Studio Code
- (7) ProtonVPN [previously, Express VPN]
- (8) Visual Studio Code [previously, "Notes" Application on Macbook Laptop]

Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)
  - [3:19:59]: Everything looks good!
  - []

Salutations

* [] Thank the audience for viewing or attending the live stream
* [x] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream

Checklist References:

[1.0]
CAM CHURCH- TRANSMUTATION
By Unicole Unicron
https://www.youtube.com/watch?v=cKvcQpPrjY4

[2.0]
Earth: Pleiadian Keys to the Living Library
By Barbara Marciniak

[Written @ 0:32]

Timeline Annotation Legend Tree:

- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task (New Idea)
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:33:06]

- 🚧 [work-progress] I initialized an expansion panel grid on the "your account" and "network status" pages . . 

[1:33:06 - 2:43:41]

- 📝 [commentary] I am intending to create an expansion panel grid data structure that allows for storing the expansion grid settings . . in a database . . 

- 🚧 [work-progress] I updated the "create transaction request" . . vuejs . . component . . to include a settings button . . for . . the account sender, account recipient, and payment amount . . I updated the appearance of the "send payment" and "cancel" buttons . . and to move the "Not enough funds" error message below the "Remaining Account Balance" . . text . . 

- 🚧 [work-progress] I added a "get account by qr code" . . vuejs . . component . . 

[2:43:41 - 2:56:34]

- 📝 [commentary] I didn't complete what I was originally intending to do with the expansion panel grid data structure . . my thinking is that . . I'm not really sure about certain things . . like (1) where to place the data structure representation files . . which directory should those data structures reside in . . or be in . . ? . . I'm not entirely sure . . expansion panel grid . . is a new concept to me . . I'm not sure . . if it's related to the usecase codebase . . or if it should be specified . . in the . . web browser website . . codebase . . uh . . I guess I could give myself time to think about things . . more . . and uh . . work on other things . . until . . a uh . . source of inspiration . . comes up . . or maybe . . I need to uh . . deliberately uh . . start looking . . at uh . . where uh . . the uh . . options for an expansion panel . . should be . . specified . . uh . . I'm uh . . not . . entirely . . sure . . but uh . . one thing that's uh . . maybe . . being considered . . is also allowing . . library users . . for the javascript library . . to . . import the . . expansion panel . . data structures . . into their own project . . and so . . uh . . repeated work in this effort may not be required . . by . . other . . uh . . project . . developers . . or by other . . uh . . development teams . . involved in project development . . 

[2:56:34 - 3:08:18]

- 🚧 [work-progress] / 📝 [commentary] I updated the project README.md . . to make . . the contact information . . in blue . . color . . xD . . a random thought then came to mind a few moments later . . "The New Kind Of Blue Sciences Are Basic Blue" . . -_- . . I thought it was funny because of how the voice . . said those words . . it was like a cartoon character . . -_- . . I'm not really sure . . which uh . . -_- . . genre . . of cartoon . . animations . . would describe the best impression of the voice . . but the style of humor reminds me of . . that person that says . . "hun" . . in that one cartoon television show . . I think it's . . uh . . Assassination Classroom . . uh . . well they're a side character . . and uh . . I remember them in 1 of the episodes . . and they would make a comment like . . "something something something" . . and then . . say . . "hun" . . or . . "hmphh" . . and they . . sounded like a man . . with a deep voice . . and maybe I found it ironic . . why a man would speak with such a deep voice . . and casually say . . "hun" . . or .  "hmph" . . xD . . "we better get to the objective quest . . hun" . . "that's too rich . . hun" . . but the way they said . . "hun" . . was like . . "hmphh" . . like they were being . . pompous . . but slightly genuine . . and maybe it could have been a speech impediment . . and it was like a habit that they had . . "hun" . . xD . . [1.0 @ approximately 18:30 - approximately 20:20]


[3:08:18 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Assassination Classroom
Episode 19 - Pandemonium Time
[Example Resource (English Dub)]:
https://www9.kisscartoon.love/episode/assassination-classroom-episode-19/




[Written @ 0:29]

[copy-pasted from Day Journal Entry for November 27, 2020 . . . 15:55]



### Tasks for today - November 27, 2020
### Tasks for today - December 4, 2020

- [] Update the "Your Account" Page
  - [] Create the grid of expansion panel columns
    - [] Create expansion panel grid component [Medium-Difficulty-Task]
    - [] Create expansion panel column item list component [Medium-Difficulty-Task]
    - [] Create expansion panel column row list component [Medium-Difficulty-Task]
    - [] Create expansion panel column item component [Medium-Difficulty-Task]
    - [] Create constant variable tree for expansion panel column item (id, etc.) [Medium-Difficulty-Task]
- [] Update the "Network Status" Page
  - [] Add the expansion panel grid component to this page [Easy-Difficult-Task-Dependent-On-Expansion-Panel-Grid-Task]
- [] Update the "Settings" Page
  - [] Add the expansion panel grid component to this page [Easy-Difficult-Task-Dependent-On-Expansion-Panel-Grid-Task]
- [cancelled] Update the "About" Page
  - [cancelled] Remove the questions and answers above the "About This Project" Section [Easy-Difficulty-Task]
    - [commentary-on-cancellation] It seems that having a "Freqently Asked Questions" Section helps inform the user about the community's . . feedback . . on the project . . more rapidly . . than . . the . . "About This Project" . . section would otherwise . . uh . . well . . 
- [completed] Update the width of the search textfield to show the "Accounts" text


### Tasks for today - November 14, 2020

- [] Update Project Readme to be more troy-architecture related in the way it's structured [In-Active-Development]

### Tasks for today - November 11, 2020

- [] Project Readme [Challenging Task]
- [] Your Account Page [Easy Task]
  - []
- [] Network Status Page [Easy Task]
- [] Accessibility Insights CI / CD Process [Challenging Task]
- [] Settings Page [Easy Task]

### Things to do
[Goals set on November 9, 2020]

// README.md related goals

- [] Add measurements related information to the project readme.md file
  - [] 

// Your Account Page Related Goals

### Things to do

// Performance related topics

- [] Convert images to webp
- [] Ensure website files are updated after updates are made to the website (without having to refresh the cache)
- [] Nuxt.js for server-side rendering
- 

// Continuous Integration / Continuous Deployment

- [] performance measurement screen captures
  - [] lighthouse html output
  - [] publish lighthouse html output
  - [] screen capture of the published lighthouse html output page
- [] 

// Performance measurement

- [] Use page speed insight: https://developers.google.com/speed/pagespeed/insights/
- [] Use webpagetest: https://www.webpagetest.org/

// Readme

- [] Add "Demo Website Performance Statistics" to readme
  - [] WebPageTest . . webpagetest screen-capture
  - [] Page Speed Insight . . page speed insight screen-capture
  - [] Coverage Report . . screen-capture
- []

### Things that are planned to be completed

Use the mobile-first design strategy:

- [] Update the your account page
  - [] 
  - [] add a account description section
    - [] account id
    - [] account name
    - [] account username
    - [] account description
    - [] physical location address (country, city/town/province)
    - [] account contact list (email address, website url list, social media url list, physical mail address list, other items)
    - [] account business product listing
    - [] account currency exchange currency listing
    - [] account employer positions listing
    - [] 
  - [] use the name "account statistics" to contain "account balance history" and "transaction amount history" (received and sent transaction amount)
  - [] add a "find shops to buy products or services" section
    - [] add filter for finding shop by location
    - [] add filter for finding a shop by the products and service provided
    - [cancelled] add text: find shops that accept ecoin such as "individuals", "online commerce stores", "restaurants", "apartments", "house salesplaces", "gymnasiums", "sports recreation facilities" and more (useful keywords)
  - [] add a "find employers to work with" section
    - [] add filter for finding an employer by location
    - [] add filter for finding an employer by job genre
    - [cancelled] add text: find employers such as "individuals", "schools", "technology companies", "hospitals", "government agencies", "non-profit organizations", "local businesses" and more (useful keywords)
  - [] add a "find currency exchanges to trade with" section
    - [] add filter for finding exchange by location
    - [] add filter for finding exchange by currency
    - [cancelled] add text: find digital currency exchanges to trade Ecoin for fiat currencies (ie. U.S. Dollar, Euro, Yuan, and more) or alternative digital currencies
  - [] add a "find ecoin alternatives" section
    - [] add filter for finding exchange by feature
    - [cancelled] add text: find alternative digital currencies to send and receive money with
  - [] 

- [] Update the network status page
  - [] add a "digital currency statistics" section
  - [] add a "account statistics" section
  - [] add a "transaction statistics" section
  - [] add a globe activity flight map section

### Things To Complete - Statistics related

// account statistics

- [] account statistics
  - [] listed statistics
    - [] 2 rows, horizontal scrolling, 2nd column visible
    - [] current account balance
    - [] amount received per month
    - [] amount sent per month
  - [] graph statistics
    - [] account balance history [line graph]
    - [] places account is sending to [geography chart]
    - [] places account is receiving from [geography chart]
  - [cancelled] searchable list graph
    - [cancelled] accounts transacted with
      - [thoughts-on-cancellation]: transaction history is similar feature


// transaction statistics

- [] transaction statistics
  - [] listed statistics
    - [] 2 rows, horizontal scrolling, 2nd column visible
    - [] number of transactions sent
    - [] number of transactions received
    - [] latest transaction sent (w/ time ago)
    - [] latest transaction received (w/ time ago)
    - [] number of recurring transactions sent
    - [] number of recurring transactions received
    - [] amount received per month [by recurring transactions]
    - [] amount sent per month [by recurring transactions]
    - [] total amount received
    - [] total amount sent
    - [] percent increase or decrease from latest processed transaction
  - [] graph statistics
    - [] transaction calendar heatmap
    - [] transaction amount sent and received graph [line graph]
    - [] 
  - [] searchable list graph
    - [] transaction history


// about page

- [] show circular progress for about this project loading (v-progress-circular)


// settings page

- [] make vertical aligned settings input to the full width
  - [thoughts]: I like the appearance of the full width text fields on desktop display

// website header

- [] show send payment button
  - [] show icon button for mobile display
  - [] show horizontal button for desktop display
    - [thoughts-on-cancellation]
      - search account allows users to type the account they would like to send to . . which is similar to the feature of sending . .
      - one button is fine but maybe having access to 2 buttons is faster for sending transactions
      - we can have a "send payment" button next to the user for each result
      - by default clicking on the search result can go to the user's account page
    - [not-cancelled-any-more]
      - why not provide the option for both a button and a text field . . that could be convenient for either option to be accessible here in this scenario

- [] show account name beside the account currency amount


// transaction

- [] show progress bar until next processing
  - [] show text: percent of time left since now
  - [] show text: amount of time left until transaction processing
- [] show red progress bar for recurring transactions
- [] show blue progress bar for scheduled transactions
- [] show scheduled transaction as processed [snackbar notification]
- [] show recurring transaction as processed [snackbar notification]

// snackbar notification list

- [] create a snackbar notification list component

// welcome message

- [] show snackbar notification list for welcome message
  - [] Customer #597832, Welcome to Ecoin. Your Ecoin account is automatically created for you to start sending and receiving Ecoin payments! 🎉
  - [] Visit the Settings Page ⚙️ to change your default account settings

// 

- [] show transaction in navigation drawer
  - [thoughts]: view page content alongside the transaction for quick re-selection of transaction on main page content
  - [thoughts]: use a dialog for html component support
- [] show account in navigation drawer
  - [thoughts]: view page content alongside the account for quick re-selection of account on main page content
  - [thoughts]: use a dialog for html component support

### Things that are planned to be completed

* Things that are planned to be completed

- [] http-server/data-structures/provider-service-emulator-http-server-proxy
  - [] lunr service http server proxy
  - [] firebase emulator service http server proxy
    - [] initialize provider service
      - [] initialize provider service by http server proxy
      - [] initialize provider service for firebase algorithms
    - [x] create item
    - [x] get item
    - [] get item list
    - [] update item
    - [] add item event listener
    - [] remove item event listener
- [] daemon/data-structures/administrator-daemon
- [] daemon/data-structures/service-activity-simulator-daemon
- []

# Things to be completed

- [] add multi-language support [internationalization; i18n]
- [] add language support button
- [] fix the issues on the command line interface
- [] update account
  - [] update firebase auth uid for security
  - [] update account profile picture
  - [] update acocunt name
  - [] update account username
    1. [] add to queue map of users requested for that username
    2. [] Check queue map to see if user is the first to attend the operation
    3. [] Delete the entry map from existing username to accountId
    4. [] Update the entry map for the new username to accountId
    5. [] Change the username of the account [Firebase validate other operations]
    6. [] Delete from queue map of users registered for that username
  - [] update account by adding blocked account
    - [] can no longer send currency to this account
    - [] can no longer receive currency from this account // ???
  - [] update account by removing blocked account
    - 
  - [] update account behavior statistics // for security, fraud detection, transaction overuse
    - [] admin only read and write
    - [] measure the frequency of creating a transaction
    - [] measure the transaction amount median
    - [] freeze an account in case of abnormal account use ??
- [] get account
  - [] get account by qr code id
- [] create account
  - [] create a qr code id
  - [] add to account-qr-code-to-account-id-map
  - [] add to create-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] delete account
  - [] add to delete-account-variable-tree
    - [] millisecondDay/millisecondHour/millisecondMinute/accountIdVariableTree/accountId
- [] update transaction
  - [] update transaction amount
  - [] update transaction text memorandum
  - [] update transaction by adding a like
  - [] update transaction by removing a like
  - [] update transaction by adding a dislike
  - [] update transaction by removing a dislike
  - [] update transaction by adding a comment
  - [] update transaction by removing a comment
- [] create transaction
  - [] include permissioned contact information
  - [] text memorandum
  - [] private text memorandum
  - 
- [] apply universal basic income
  - [] create a transaction for all accounts to add digital currency amount
- [] initialize-service-account (sign in)
  - [] with google
  - [] with email and password
- [] uninitialize-service-account (sign out)
- [] gitlab continuous integration / continuous deployment (ci / cd)
  - [] publish ecoin daemon to development server
- [] readme.md
  - [] demo usecase preview: update transaction settings
  - [] demo usecase preview: view transaction information
- [] html component
  - [] pay with digital currency button works on websites
  - [] create an html component with an initial property object
  - [] return an object that supports updateHtmlComponentProperty(propertyId: string, propertyValue: any)
  - [] return an object that supports onUpdateHtmlComponent(onUpdateCallbackFunction: (propertyId: string, propertyValue: any))
- [] digital currency account data structure
  - [] business acount description
  - [] does this account represent a copy or fork of the ecoin source code?
    - [] how many accounts does this currency have?
    - [] how many transactions does this currency have?
  - [] is a digital currency exchange that transacts using ecoin
  - [] is business account, selling products and services using ecoin
    - [] is providing currency exchange service
      - [] supported currency list
    - [] is providing products
      - [] product category list, product price ranges
    - [] is providing services
    - [] is providing other
  - [] is a physical location shop or store
    - [] restaurants, cafes, shopping centers, shopping malls, grocery stores, food marts, small business store fronts, farmer's markets, individual storefronts, etc.
  - [] contact information variable tree (email, phone number, website url, physical address etc.)
    - [] permission rule on variable tree item: Public, Private, Permissioned
  - [] account statistics variable tree (set fields like 'number of employees' and other information)
  - [] are you seeking employment?
    - [] employment skills list
    - [] sought after ecoin payment amount per time period
  - [] are you an employer?
    - [] are you hiring right now
    - [] available position variable tree
      - [] ecoin payed per time period
      - [] sought after skills list
      - [] position name, time description (ie. 30 hours / week)
  - []
- [] update the styles of the desktop version of the website
  - [] account section
    - [] show the account section on the left
    - [] desktop version width should be not full width. [follow how it's done on the settings and about page]
    - [] desktop header tabs (v-tabs) should be condensed and not wide screen.
  - [] account statistics
    - [] show the account statistics on the right (allow vertical scroll)
  - [] community participants: show a list using vuetify table
- [] settings page
  - [] add light and dark theme setting
- [] add a `qr code` button
  - [] [header] Your Account QR Code
    - [] [subheader] Account QR Code for `account name`
  - [] [camera icon] Capture Account QR Code
  - [] find a digital currency account by its qr code by taking a photograph of the account qr code
- [] network statistics page
  - [] number of ecoin distributed (timeline graph)
  - [] number of created accounts (timeline graph)
    - [] number of created business accounts (timeline graph)
  - [] number of created transactions (timeline graph)
  - [] newly distributed ecoin (live updating)
  - [] newly created accounts list (live updating)
    - [] number of created business accounts (timeline graph)
  - [] newly created transactions list (live updating)
  - [] total number of ecoin created (running sum of overall currency amount)
  - [] total number of accounts created (running sum of overall accounts amount)
    - [] total number of business accounts created (running sum of overall community participants)
  - [] total number of transactions created (running sum of overall transactions amount)
  - [] flights gl map to show transactions in realtime
- [] account page
  - [] navigation bar, expand on hover, for desktop: https://vuetifyjs.com/en/components/navigation-drawers/#api
  - [] add settings icon button
  - [] transaction list should show "view transaction button"
    - [] open transaction dialog
  - [] transaction list should show "repeat transaction button"
  - [] like to dislike ratio timeline graph
  - [] number of accounts transacted with timeline graph
  - [] currency amount transacted timeline graph
  - [] number of transactions timeline graph
    - [] ratio of likes from recipient, sender
    - [] ratio of dislikes from recipient, sender
  - [] calendar heatmap of transactions created
  - [] find employers to work with
    - [] work with businesses that pay in Ecoin
  - [] find businesses to shop with
    - [] shop with businesses that transact using Ecoin
  - [] find currency exchanges to trade with
    - [] exchange Ecoin for other currencies
- [] about page
  - [] update the "Why does Ecoin exist?" answer 
    - [] enumerate the long answer clause
    - [] change the long answer to be more friendly to read and not so presumptious of minority and majority logic thinking
- [] apply transaction list
  - [] Recurring Transactions as Processed set to false
    - [] dateTobeProcessed = d
    - [] numberOfTimesProcessed = n
    - [] millisecondDelayBetweenProcessing = m
    - [] currenctDateForTransaction = c
    - [] const isAlreadyProcessedTimePeriod = d + n * m > c
- [] website style / theme
  - [] research how to improve account page style for desktop
  - [] research how to improve settings page style for desktop
  - [] research how to style the network status page
  - 
- [] readme.md
  - [] add the names, and website urls of the software dependencies items (use the same layout as the related work section)
  - [] remove list index from the software dependencies table
  - [] add appended list to each list item in the table of contents
  - [] add "completed" text to the features description text in the readme.md file
  - [] fix typo to be "for storing data on remote computers", in software dependencies / database provider / firebase realtime database section
  - [] add the domain name registration service used for https://ecoin369.com . . GoDaddy
  - [] add website library "echarts" (for logistics measurement display)
  - [] add expectation definitions measurement tools: mocha, chai
  - [] add project compilation: node.js, npm
  - [] add prepaid credit card service (with anonymous support): Bluebird, American Express
  - [] add the project authors for the software dependency list
  - [] update the software depenencies message to change the open source software message
  - [] consider adding a frequently asked questions section to the readme
  - [] add a guide to get started in publishing your own ecoin-based digital currency
    - [] easily change the name, image icon, resource urls and project description
      - [] new directory for ease of use ? or precompiled/@project ? 
    - [] how to add firebase credentials for development and production environments
      - [] create firebase api token using the command line interface tool
    - [] how to add algolia credentials for development and production environments
    - [] how to publish the project service website
    - [] how to publish the administrator daemon
  - [] add a guide to get started with the development of the project source code
    - [] how to install nodejs and npm
    - [] how to install the firebase emulator
    - [] how to install ngrok
    - [] how to start the administrator daemon
    - [] how to start the firebase emulator
    - [] how to start the service provider http server proxy
    - [] how to start the development server for the project service website
    - [] how to run a measurement of the expectation definitions (also known as unit tests but also includes integration tests)
    - [] how to start the service activity emulator daemon
    - [] an introduction to the troy architecture
    - [] 
- [] digital-currency-business data structure
  - [] number of employees, are you hiring at the moment (and do you pay in Ecoin), salary range, number of available positions, business type, are remote positions available, 
- [] firebase cloud firestore
    - [] initialize the algorithms and expectation definitions
    - [] initialize the security rules for creating, updating and deleting
    - 
- [] javascript library demo
  - [] create a demo html page for creating a "pay with ecoin button"
  - [] create a demo html page for creating a "ecoin price label"

### Planned Tasks to Be Completed

// tasks related to the provider service emulator http server proxy

- [] service emulator http server proxy
  - [] firebase emulator
  - [] lunr (text search) emulator
    - [] initialize-provider-service-emulator for algolia search provider

// tasks related to firebase

- [] create multiple service accounts
  - [] firebase extract variable with Service Account Id
  - [] firebase create service account algorithm
  - [] firebase get service account list

// tasks related to the project service website integrating with firebase

- [] get service account list from firebase using vuex
- [] select the first service account from the list using vuex
- [] create service account if list is empty (anonymous)
- [] get digital currency account list
- [] get active digital currency accountId from localforage
- [] create digital currency account (if account list is empty)

// tasks related to a service activity simulator to simulate service use by creating accounts and transactions

- [] service activity-simulator-daeomon
  - [] create a service account
  - [] create a digital currency account
  - [] select two accounts at random
  - [] create a digital currency transaction between the 2 accounts

### Things to do

- [] use lighthouse to measure web accessibility values for the project service web browser website
- [] take a screencapture image of the lighthouse report html file
- [] publish the lighthouse measurement html page, and screencapture image to firebase static file host





