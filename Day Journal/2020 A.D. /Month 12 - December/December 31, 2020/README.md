

# Day Journal Entry - December 31, 2020

[Written @ 21:47]

Notes @ Newly Discovered Sound Clips
Notes @ Cute Sounding Moments In Music Clips


"Alright . . I'll see you after lunch . . Bye . . "
[1.0 @ 3:06:30 - 3:06:35]


[1.0]
HAPPY NEW YEAR - Study With Me *6 HOURS* (with talking during breaks)
By Study Vibes
https://www.youtube.com/watch?v=c_fevTU9tYs

[Written @ 18:36]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:01:13]

- 📝 [commentary] I live streamed a message for Seth



[Written @ 0:53]

Notes @ Newly Advertised Information

"Telepathic Training Porgram Club Workbook" By Unicole Unicron
[1.0 @ 1:16]


[1.0]
Why Study Telepathy?
By Unicole Unicron
https://www.youtube.com/watch?v=aEBjRa8rF94



[Written @ 0:49]



Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:03:35]

- 📝 [commentary] I took notes for a creature from Mars who would like to share a message at [1.0] . . I also wrote commentary on what I thought about the message that was transmitted to me through telepathic channeling . .

[3:03:35 - 3:20:37]

- 📝 [commentary] I watched [2.0]

[3:20:37 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Dialog Entry Relational Journal 0 - Development Journal Entry - December 31, 2020
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2020%20A.D./Month%2012%20-%20December/December%2030%2C%202020

[2.0]
Why Study Telepathy?
By Unicole Unicron
https://www.youtube.com/watch?v=aEBjRa8rF94


