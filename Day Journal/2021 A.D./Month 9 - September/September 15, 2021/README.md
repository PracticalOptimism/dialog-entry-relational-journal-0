
# Day Journal Entry - September 15, 2021

### [Written @ 13:31]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:38:40]

- 📝 [commentary] I didn't do anything. Although the notes at [1.0] re-iterate the latest or last or previous things that we did regarding Ecoins' more latest previous developments

[1:38:40 - 4:08:29]

- ✅ [completed] I created an initialized version of a vue.js project with (1) typescript and (2) vuetify. I tried vue.js 3.x with vuetify 3.x and ran into problems. I decided to use vue.js 2.x and vuetify 2.x.

- 📝 [commentary] (1) the light architecture (2) has evolved a lot (3) I made mistakes (4) I continue to make mistakes. (5) I love architecting stuff but finally I think it would be neat-o to just say that starting a new project architecture ought to be an okay-idea

[4:08:29 - 5:09:32]

- 🚧 [work-progress] (1) I updated the light architecture to use `types` (2) type-1 type-2 type-3 etc. (3) and numbers (4) 1 2 3 etc. (5) for the naming conventions and also finally (6) 'service-for' (7) prefix for when you need a service for something.

[5:09:32 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal - July 20, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%207%20-%20July/July%2020%2C%202021




### [Written @ 11:58]



