
# Day Journal Entry - September 30, 2021

### [Written @ 9:11]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:11:04]

- 🚧 [work-progress] I worked on updating the folder data table component [1.0]

[2:11:04 - 3:02:57]

- 😴 [break-time] I used the bathroom and got some food to eat as well.

[3:02:57 - 6:04:32]

- 🚧 [work-progress] I created a list of the statistics that are needed for the initial project release [2.0]

[6:04:32 - 6:35:35]

- 😴 [break-time] I took a break and got some food to eat. I also listened to [3.0] while on my lunch break.

[6:35:35 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin

[2.0]
Development Journal Entry - September 30, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%209%20-%20September/September%2030%2C%202021

[3.0]
A TRUE GENIUS!!! - A World Worth Imagining; Jacque Fresco - The Man with the Plan (Sbtls)
By SOUL Documentary
https://www.youtube.com/watch?v=0E9IjQSSnqA


