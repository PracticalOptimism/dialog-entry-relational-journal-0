

# Day Journal Entry - September 16, 2021


### [Written @ 13:36]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:08:33]

- 🚧 [work-progress] I wrote some notes for a design [1.0]

[2:08:33 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin / Design For Pages
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/Journal%20On%20Things%20About%20Design/Ecoin/Design%20For%20Pages



### [Written @ 9:31]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:13:43]

- ✅ [completed] I fixed errors with the initial vue.js 2.x application that uses the updated the-light-architecture idea

[0:13:43 - 2:41:18]

- ✅ [completed] I created an updated mock up design sketch for the 'your account' page for the latest Ecoin. [2.0]

[2:41:18 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal / September 16, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%209%20-%20September/September%2016%2C%202021

[2.0]
Ecoin / Home Page - Your Account
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/Journal%20On%20Things%20About%20Design/Ecoin/Design%20For%20Pages/Home%20Page%20-%20Your%20Account





