

# Day Journal Entry - September 22, 2021

### [Written @ 10:05]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:15:33]

- 📝 [commentary] I created a 'continuous integration / continuous deployment' process for publishing Ecoin (the ecoin customer website) to firebase at the development build website at https://ecoin-f9c5c.web.app

[1:15:33 - 3:15:22]

- 📝 [commentary] I was on lunch break and also wrote plans for the Ecoin project while away from the computer.

[3:15:22 - 3:41:20]

- 📝 [commentary] I reviewed random things like just looking around on the computer.

[3:41:20 - 3:46:44]

- 📝 [commentary] I started eating yogurt for my second lunch.

[3:46:44 - 4:47:09]

- 📝 [commentary] I watched UNICULT. Join UNICULT. [1.0]

[4:47:09 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Unitopian Autonomy & Anarchy
By UNICULT
https://www.youtube.com/watch?v=FJf5rKWsim8&ab_channel=NBCNews








