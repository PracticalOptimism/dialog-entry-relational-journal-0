
# Day Journal Entry - September 29, 2021


### [Written @ 9:52]

[Copy-Pasted From The Ecoin 2.0 codebase]

search: string = ''
calories: string = ''
desserts: any[] = [
  {
    name: 'Frozen Yogurt',
    calories: 159,
    fat: 6.0,
    carbs: 24,
    protein: 4.0,
    iron: '1%'
  },
  {
    name: 'Ice cream sandwich hehehe',
    calories: 237,
    fat: 9.0,
    carbs: 37,
    protein: 4.3,
    iron: '1%'
  },
  {
    name: 'Eclair',
    calories: 262,
    fat: 16.0,
    carbs: 23,
    protein: 6.0,
    iron: '7%'
  },
  {
    name: 'Cupcake',
    calories: 305,
    fat: 3.7,
    carbs: 67,
    protein: 4.3,
    iron: '8%'
  },
  {
    name: 'Gingerbread',
    calories: 356,
    fat: 16.0,
    carbs: 49,
    protein: 3.9,
    iron: '16%'
  },
  {
    name: 'Jelly bean',
    calories: 375,
    fat: 0.0,
    carbs: 94,
    protein: 0.0,
    iron: '0%'
  },
  {
    name: 'Lollipop',
    calories: 392,
    fat: 0.2,
    carbs: 98,
    protein: 0,
    iron: '2%'
  },
  {
    name: 'Honeycomb',
    calories: 408,
    fat: 3.2,
    carbs: 87,
    protein: 6.5,
    iron: '45%'
  },
  {
    name: 'Donut',
    calories: 452,
    fat: 25.0,
    carbs: 51,
    protein: 4.9,
    iron: '22%'
  },
  {
    name: 'KitKat',
    calories: 518,
    fat: 26.0,
    carbs: 65,
    protein: 7,
    iron: '6%'
  }
]
get headers () {
  return [
    {
      text: 'Dessert (100g serving)',
      align: 'start',
      sortable: false,
      value: 'name'
    },
    {
      text: 'Calories',
      value: 'calories',
      filter: (value: any) => {
        if (!this.calories) return true

        return value < parseInt(this.calories, 10)
      }
    },
    { text: 'Fat (g)', value: 'fat' },
    { text: 'Carbs (g)', value: 'carbs' },
    { text: 'Protein (g)', value: 'protein' },
    { text: 'Iron (%)', value: 'iron' }
  ]
}


### [Written @ 9:28]

### Things To Say After The Ecoin Project's release

"I think society's transitioning to a resource based economy." [1.0]

"The venus project is popular now" [1.0]




[1.0]
The Venus Project
By Jacque Fresco and Roxanne Meadows
https://www.thevenusproject.com/





### [Written @ 9:20]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:21:25]

- 🚧 [work-progress] I worked on using example data or test data to see how the folder system or file system viewer would look [1.0]

[1:21:25 - 1:58:37]

- 😴 [break-time] I ate food and took a break

[1:58:37 - 3:18:35]

- 🚧 [work-progress] I worked on initializing the working navigation of going forward and backward in the folder data table component [1.0]

[3:18:35 - 4:01:10]

- 😴 [break-time] I ate some food and took a break

[4:01:10 - 5:27:52]

- 🚧 [work-progress] I worked on updating the description list view and ensuring the search works with it for the folder data table [1.0]

[5:27:52 - 5:43:11]

- 😴 [break-time] I took a break and ate some food

[5:43:11 - 8:23:14]

- 🚧 [work-progress] I worked on making the icon image url work for the folder or file items in the folder data table [1.0]

[8:23:14 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin




