
# Day Journal Entry - September 17, 2021



### [Written @ 15:48]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:42:54]

- 📝 [commentary] I updated the notes on the design for Ecoin's user interface [1.0]

[1:42:54 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin / Design / Home Page - Your Account
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/Journal%20On%20Things%20About%20Design/Ecoin/Design%20For%20Pages/Home%20Page%20-%20Your%20Account


### [Written @ 14:10]

Thank you.



