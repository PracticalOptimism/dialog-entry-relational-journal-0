

# Day Journal Entry - September 19, 2021

### [Written @ 13:14]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:25:42]

- 📝 [commentary] I updated the notes on the Ecoin popup items [1.0]

[1:25:42 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin / Design / Popup Item
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/Journal%20On%20Things%20About%20Design/Ecoin/Design%20For%20Pages/Popup%20Item


