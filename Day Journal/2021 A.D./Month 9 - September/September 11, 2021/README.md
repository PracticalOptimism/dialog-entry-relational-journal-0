

# Day Journal Entry - September 11, 2021


### [Written @ 12:58]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 4:30:37 + 0:17:47 = 4:48:7+17 ~ 4:48:00]

- 📝 [commentary] I accidentally forgot to start the timer to sync this live stream with the video content

- 📝 [commentary] I spent time trying to make an offset-timer-corrector program that basically tries to correct for the right time

- 📝 [commentary] I didn't finish the program because I got into topics like Universal Basic Marriage and large breasts

[4:48:7+17 - 4:35:38 + 0:17:47 = 4:52:38+47 ~ 4:53:00]

- 📝 [commentary] I started a career for mathematicians by pointing out that some calculations are difficult to make

- 📝 [commentary] It's possibly good notation to write 7+17 instead of evaluating the expression

- 📝 [commentary] It's hard to know if the person did their mathematics right if they sometimes make mistakes on easy problems like 7 + 17

- 📝 [commentary] That's why preserving the original information of 7 and 17 is really essential

- 📝 [commentary] Then you can evaluate and re-evaluate your own calculations to see if you get the same result repeatedly.

- 📝 [commentary] Algebraically, mental thoughts may be different -_- do you always get the same calculations -_- personally I don't know but Seth is says 'iyes'

[4:53:00 - ]


References:

[1.0]
Day Journal Entry / September 11, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2011%2C%202021

[2.0]
Pornography 101 / Day Journal Entry / September 11, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2011%2C%202021




### [Written @ 8:44 - 12:58]


[Copy-pasted from the youtube live stream chat box]
😑 I forgot to start the timer. what a shame. 17:47



[copy-pasted from codepen]

```html
<div id="timer" style="font-size: 60px;">0:00:00
</div>

<!-- Text Field -->
<input id="startTime" type="text" placeholder="Start Time: e.g. 1:10:10 (hour:minutes:seconds)" style="min-width: 300px" onkeyup="updateInitialTime()">

<div id="wrongInputMessage" style="display: none; color: red"><br>Impressive, but wrong input. Try colon separated values like 1:10:10</div>

<br><br>

<!-- Start Timer Button -->
<input id="startTimer" type="button" value="Start Timer" onclick="startTimer()">

<br><br>

<!-- Stop Timer Button -->
<input id="stopTimer" type="button" value="Stop Timer" onclick="stopTimer()">

<br><br>

<!-- Reset Timer Button -->
<input id="resetTimer" type="button" value="Reset Timer" onclick="resetTimer()">


```


[copy-pasted from codepen]
```javascript

const timer = document.getElementById('timer')

const startTime = document.getElementById('startTime')

const wrongInputMessage = document.getElementById('wrongInputMessage')


let initialTimerStartTime = null

let startTimerIntervalId = 0

const totalRatioMultiplierList = [60, 60, 24, 30] // 60 seconds in a minute, 60 minutes in an hour, 24 hours in a day, 30 days in a month; concentric circle outward -_- more ratios -_- approximate -_- not exact -_- i.e. 30 days isn't always true right? -_- -_- and leap years could be complicated -_- but you can use a list of lists if you want -_- to elaborate -_- but that might need more code -_- so -_- it's up to you -_-



function startTimer () {
  const initialStartTime = startTime.value || timer.innerHTML
  
  const isValidTime = isTimeValid(initialStartTime)
  
  if (!isValidTime) {
    return
  }
  
  const initialTimerStartTime = getInitialTimerStartTime(initialStartTime)
  
  startTimerIntervalId = setInterval(function () {
    const currentTimerTime = (new Date()).getTime()
    const newTimeInMilliseconds = currentTimerTime - initialTimerStartTime
    const timeObject = getTimeObjectFromMilliseconds(newTimeInMilliseconds)
    
  }, 1000)
}

function isTimeValid (time) {
    let isColonSeparatedNumberList = true
  
  const timeList = time.split(':')
  
  for (let i = 0; i < timeList.length; i++) {
    isColonSeparatedNumberList = isColonSeparatedNumberList && isNumber(timeList[i])
  }
  
  if (!isColonSeparatedNumberList) {
    wrongInputMessage.style.display = 'initial'
  } else {
    wrongInputMessage.style.display = 'none'
  }
  
  return isColonSeparatedNumberList
}

function getInitialTimerStartTime (initialStartTime) {
  const numberList = initialStartTime.split(':').reverse()

  let timeInMilliseconds = 0
 
  for (let i = 0; i < numberList.length; i++) {
    let totalRatioMultiplier = 1
    for (let j = 0; j < i; j++) {
      totalRatioMultiplier *= totalRatioMultiplierList[j]
    }

    timeInMilliseconds += numberList[i] * totalRatioMultiplier
  }
  
  timeInMilliseconds *= 1000 // 1000 milliseconds per second.
  
  initialTimerStartTime = new Date()
  initialTimerStartTime = initialTimerStartTime.getTime() - timeInMilliseconds
}

function getTimeObjectFromMilliseconds (timeInMilliseconds) {
  let timeObject = [timeInMilliseconds / 1000]
  for (let i = 0; i < totalRatioMultiplierList.length; i++) {
    timeObject.push(Math.floor(timeObject[i] / totalRatioMultiplierList[i]))
  }
  return timeObject // [seconds, minutes, hours]
}

function stopTimer () {
  // const
}

function resetTimer () {
  initialTimerStartTime = null
  timer.innerHTML = '0:00:00'
}

function updateInitialTime () {
  timer.innerHTML = startTime.value || '0:00:00'
}

function isNumber(str) {
  if (typeof str != "string") return false
  return !isNaN(str) &&
         !isNaN(parseFloat(str))
}


window.startTimer = startTimer
window.stopTimer = stopTimer
window.resetTimer = resetTimer

window.updateInitialTime = updateInitialTime







```





### [Written @ 8:25 - 8:26]

Alternative suspicions could be (1) women who get to have sex with any man they want

(2) Women that abandon men

(3) women that smell like shit but don't tell anyone

(4) women that have bad breath

.......................................................................

All of these suspicions

.......................................................................

Suspicions

.......................................................................

And to see them manifest

.......................................................................

Is suspicious

.......................................................................

It means 'dense concentric circles'

.......................................................................


### [Written @ 8:23 - 8:25]

"trust in the layers you left behind"

means to suggest that time is illusory. That there is a "you" "outside of time" that left something behind for you "inside of time" to witness or observe.

Ecoin might be one of those observations

"A man who single handedly has sex with any woman he wants.?"

"No other man involved?"

What a suspicious thought


### [Written @ 8:19 - 8:23]

It's all very confusing

(1) Traditional physics assumes time

(2) Tomorrow's physics is based on 'intuition'

.......................................................................

But basically

.......................................................................

We can even quickly glimpse at how we got from 'space' 'time' to 'suspicion' and 'intuition'

.......................................................................

And so 'suspicion' may be new to you, it's certainly new to me. It's basically as dense as you could get for concentric circles

.......................................................................

'incredibly dense concentric circles'

.......................................................................

And intuition is -_- something that I just made up right now

.......................................................................

I don't know what it really is

.......................................................................

Suspicion is not only the traditional suspicion that you're used to

.......................................................................

But even invisible suspicions

.......................................................................

'that person spelled the name wrong'

.......................................................................

which is concentrically really dense

.......................................................................

'the reason they intentionally or unintentionally spelled the name wrong is really dense'

.......................................................................

concentric circles

.......................................................................

but you of course have suspicions

.......................................................................

that new concentrations besides concentric circle theory

.......................................................................

And all of this is nonesense that I'm making up unless you're also thinking like this

.......................................................................


### [Written @ 8:16 - 8:19]

Trust in the layers you left behind.

(1) These notes have to do with "the future" and also "trace theory" [1.0]

(2) basically my thoughts are invisible to me and still I get traces of invisible ideas that life isn't an accident.

(3) There's a sort of 'ahead-ness' that lies that already has magnets of invisible potentials waiting for you to run into

(4) When all the men in the world lose all their pretty 10-girlfriends. (that is their hotness level is 10)

(5) That means so much jealousy ensues

(6) But it's also a time to consider that there are already invisible energies waiting for you to ride on new waves of insight

(7) girlfriends were one insight you had

(8) but stars of invisible energy are already waiting for you to call them your new girlfriends even if they are not girls

(9) Only Jon gets girls


[1.0]
Trace Theory
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/trace-theory.md


### [Written @ 8:08 - 8:16]

Trust in the layers you left behind.

These words came to my mind just a few minutes ago as I was entering the library doors where I go to start the live streams these days.

As you may have been aware. I used to live with my mom. Then I moved out. She had to kick me out because I was being a bum without a job. I didn't help pay for the bills. And then I went to a homeless shelter.

I am still at the homeless shelter which is very godly and pleasant by the way since (1) I get free food (2) I get a free bed (3) each night before sleeping I try to do a prayer (3.1) "Thank you for the food" "Thank you for the bed that I sleep in (3.2) If you are grateful, I pray that your prayers are being heard and listened to (both you random people that I sleep with in this shelter as well as the world in general) (3.3) I pray to have a good sleep

This random prayer I made up came to me many days ago.

I just wanted to have a prayer because gosh my life feels like a mess sometimes.

In any case. I was kicked out. I went to a homeless shelter.


.......................................................................

Post-Notes

[Written @ September 16, 2021 @ 8:25]

[copy-pasted from the day journal entry notes above]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 4:30:37 + 0:17:47 = 4:48:7+17 ~ 4:48:00]

- 📝 [commentary] I accidentally forgot to start the timer to sync this live stream with the video content

- 📝 [commentary] I spent time trying to make an offset-timer-corrector program that basically tries to correct for the right time

- 📝 [commentary] I didn't finish the program because I got into topics like Universal Basic Marriage and large breasts

[~ 4:48:00 - 4:35:38 + 0:17:47 = 4:52:38+47 ~ 4:53:00]

- 📝 [commentary] I started a career for mathematicians by pointing out that some calculations are difficult to make

- 📝 [commentary] It's possibly good notation to write 7+17 instead of evaluating the expression

- 📝 [commentary] It's hard to know if the person did their mathematics right if they sometimes make mistakes on easy problems like 7 + 17

- 📝 [commentary] That's why preserving the original information of 7 and 17 is really essential

- 📝 [commentary] Then you can evaluate and re-evaluate your own calculations to see if you get the same result repeatedly.

- 📝 [commentary] Algebraically, mental thoughts may be different -_- do you always get the same calculations -_- personally I don't know but Seth is says 'iyes'

[~ 4:53:00 - 7:32:32]

- 📝 [commentary] I typed notes about 60 different dream realities that could be real. I typed notes about 36 different possible future realities. [3.0]

[7:32:32 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Day Journal Entry / September 11, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2011%2C%202021

[2.0]
Pornography 101 / Day Journal Entry / September 11, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2011%2C%202021

[3.0]
Universal Basic Marriage / Day Journal Entry / September 11, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Universal%20Basic%20Marriage/2021%20A.D./Month%209%20-%20September/September%2011%2C%202021







