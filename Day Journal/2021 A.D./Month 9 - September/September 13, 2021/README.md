

# Day Journal Entry - September 13, 2021


### [Written @ 16:30 - 16:30]

[copy-pasted from the youtube live stream]

Pressures of heat and cold waves. Select the length you like. 

[1.0] the notes here at [1.0] inspired the quote on 'pressures of heat and cold waves' 'select the length you like'

'`long`'

'`heat`'

'`cold`'

'`pressure`'

[1.0]
Random Thoughts and Ideas - September 13, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas/Random%20Thoughts%20and%20Ideas/2021%20A.D./Month%209%20-%20September/September%2013%2C%202021

### [Written @ 12:50]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 4:21:28]

- 📝 [commentary] [1.0] [2.0] [3.0]

[4:21:28 - 5:04:09]

- 📝 [commentary] I wrote more notes [3.0]

[5:04:09 - 7:56:59]

- 📝 [commentary] I wrote about a lot of things [3.0] [4.0] [5.0]

[7:56:59 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Random Thoughts and Ideas - September 13, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas/Random%20Thoughts%20and%20Ideas/2021%20A.D./Month%209%20-%20September/September%2013%2C%202021

[2.0]
Day Journal - September 13, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2013%2C%202021

[3.0]
Pornography 101 - September 13, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2013%2C%202021

[4.0]
Favors That I Ask - September 13, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Favors%20That%20I%20Ask/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2013%2C%202021

[5.0]
Universal Basic Marriage - September 13, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Universal%20Basic%20Marriage/2021%20A.D./Month%209%20-%20September/September%2013%2C%202021




### [Written @ 8:40]


### [Written @ 8:31 - 8:40]

Here is the codebase for a timer program that I started a few live streams ago (1) I meant to finish it but I feel like I don't need to (2) I could finish it if I wanted to but might rather move on to other things (3) I'm sorry for not finishing the program (4) The program allows you to enter a start time for the timer and you should be able to click 'start timer' and the timer then counts up from there (5) theoretically such a program has other practical usecases in other places

Places where such a timer program might be used is in (1) random access computers where you want to start reading from an entered time index and not just a position index (2) time indexes are interesting it's basically like time travelling (3) any time you have a pendulum in the system a nice way to start your program is being able to (3.1) set the rate at which the pendulum swings (which by the way is like setting the time at which the pendulum starts) (3.2) and setting the material that the pendulum is made of (which by the way is again like setting the time at which the pendulum starts)

The pendulum starting with cotton fabric strings at a frequency of 2 swings per minute really is a practical example of why 'time' isn't just things like 'pendulum swings' like 'the swing of the clock of time that is the relative reference for time for a lot of people' but things like 'material integrity' is also another time heartbeat measure. basically if you have a mathematics for material chemistry you can realize there are way more atomic arrangements that come for free than what is available in the periodic elements

The periodic elements 'period'-hehe-get-it'it's like a pendulum swing' is just one set of pendulously discovered swings of atomic composition.

Seriously. physicists should know this by now that hypnosis is how the physics of tomorrow can start to start to have a based theory.

hypnosis means there are period swings about everything around us. basically why not have variants of hydrogen that allow for different curvatures.

basically it's a way of saying that 'time' can be observed in a lot of things.

..

starting from one place of time is a useful application because it means you don't have to start from 0:00:00 all the time which is like saying you need to start with understanding hydrogen every time you go to your chemistry periodic table set and try to navigate to see which elements you are considering for a particular usecase

If you are allowed to jump based on intuition of the arrangements of chemicals you can possibly have a nice usecase for that.

..

The usecase is something like (1) oops we made a mistake (2) let's start at another time (3) start at another time and maybe move forward from there.

..


// HTML
// copy-pasted from the codepen window from the web browser

```html

<div id="timer" style="font-size: 60px;">0:00:00
</div>

<!-- Text Field -->
<input id="startTime" type="text" placeholder="Start Time: e.g. 1:10:10 (hour:minutes:seconds)" style="min-width: 300px" onkeyup="updateInitialTime()">

<div id="wrongInputMessage" style="display: none; color: red"><br>Impressive, but wrong input. Try colon separated values like 1:10:10</div>

<br><br>

<!-- Start Timer Button -->
<input id="startTimer" type="button" value="Start Timer" onclick="startTimer()">

<br><br>

<!-- Stop Timer Button -->
<input id="stopTimer" type="button" value="Stop Timer" onclick="stopTimer()">

<br><br>

<!-- Reset Timer Button -->
<input id="resetTimer" type="button" value="Reset Timer" onclick="resetTimer()">

```

// JavaScript
// copy-pasted from the codepen window from the web browser

```js

const timer = document.getElementById('timer')

const startTime = document.getElementById('startTime')

const wrongInputMessage = document.getElementById('wrongInputMessage')


let initialTimerStartTime = null

let startTimerIntervalId = 0

const totalRatioMultiplierList = [60, 60, 24, 30] // 60 seconds in a minute, 60 minutes in an hour, 24 hours in a day, 30 days in a month; concentric circle outward -_- more ratios -_- approximate -_- not exact -_- i.e. 30 days isn't always true right? -_- -_- and leap years could be complicated -_- but you can use a list of lists if you want -_- to elaborate -_- but that might need more code -_- so -_- it's up to you -_-



function startTimer () {
  const initialStartTime = startTime.value || timer.innerHTML
  
  const isValidTime = isTimeValid(initialStartTime)
  
  if (!isValidTime) {
    return
  }
  
  const initialTimerStartTime = getInitialTimerStartTime(initialStartTime)
  
  startTimerIntervalId = setInterval(function () {
    const currentTimerTime = (new Date()).getTime()
    const newTimeInMilliseconds = currentTimerTime - initialTimerStartTime
    const timeObject = getTimeObjectFromMilliseconds(newTimeInMilliseconds)
    
  }, 1000)
}

function isTimeValid (time) {
    let isColonSeparatedNumberList = true
  
  const timeList = time.split(':')
  
  for (let i = 0; i < timeList.length; i++) {
    isColonSeparatedNumberList = isColonSeparatedNumberList && isNumber(timeList[i])
  }
  
  if (!isColonSeparatedNumberList) {
    wrongInputMessage.style.display = 'initial'
  } else {
    wrongInputMessage.style.display = 'none'
  }
  
  return isColonSeparatedNumberList
}

function getInitialTimerStartTime (initialStartTime) {
  const numberList = initialStartTime.split(':').reverse()

  let timeInMilliseconds = 0
 
  for (let i = 0; i < numberList.length; i++) {
    let totalRatioMultiplier = 1
    for (let j = 0; j < i; j++) {
      totalRatioMultiplier *= totalRatioMultiplierList[j]
    }

    timeInMilliseconds += numberList[i] * totalRatioMultiplier
  }
  
  timeInMilliseconds *= 1000 // 1000 milliseconds per second.
  
  initialTimerStartTime = new Date()
  initialTimerStartTime = initialTimerStartTime.getTime() - timeInMilliseconds
}

function getTimeObjectFromMilliseconds (timeInMilliseconds) {
  let timeObject = [timeInMilliseconds / 1000]
  for (let i = 0; i < totalRatioMultiplierList.length; i++) {
    timeObject.push(Math.floor(timeObject[i] / totalRatioMultiplierList[i]))
  }
  return timeObject // [seconds, minutes, hours]
}

function stopTimer () {
  // const
}

function resetTimer () {
  initialTimerStartTime = null
  timer.innerHTML = '0:00:00'
}

function updateInitialTime () {
  timer.innerHTML = startTime.value || '0:00:00'
}

function isNumber(str) {
  if (typeof str != "string") return false
  return !isNaN(str) &&
         !isNaN(parseFloat(str))
}


window.startTimer = startTimer
window.stopTimer = stopTimer
window.resetTimer = resetTimer

window.updateInitialTime = updateInitialTime

```


