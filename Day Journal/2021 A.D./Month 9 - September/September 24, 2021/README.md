
# Day Journal Entry - September 24, 2021

### [Written @ 12:51]

[Copy-pasted from an email I received (Jon Ide Received) from their email address]

[Title of Email]: Music for a Million Trees


![Music For A Million Trees](./Photographs/music-for-a-million-trees.jpg)

[Email Body Content]:

image

Dear Jon,

It's lovely to land in your inbox and say hello! I hope this email finds you enjoying life as much as possible at the moment.

I'm writing from California, which increasingly seems to have been a soul choice designed to build muscles of coping in the face of extreme uncertainty, drought, heat and fire. It's like preparation for what's coming everywhere if we don't start to operate at a whole next level of urgency, inspiration and love in action. Which is why I'm writing.

The Shift Network is also based here in the small town of Nevada City - facing alongside all of us the intensity of fire season. It has galvanized them into the boldest tree campaign I've ever witnessed, with the intention to raise funds for one million trees in three days.

Shift has created a three-day music festival, bringing together artists from around the world and some extraordinary speakers, to deliver messages of hope, transformation and inspiration as their way of trying to raise funds for the trees. The whole thing is for the trees, so in effect, they are literally campaigning for us and for the future, which is amazing.

They have included some of my deepest loves musically speaking - Carrie Tree, Ayla Shafer, Ayla Nero, Climbing Poetree... and some must see feminine leaders including Jane Goodall, Marianne Williamson, Laura Thomas… Every speaker and musician is showing up on behalf of the trees, to draw as many people as possible to this mighty endeavour of love in action.

None of us have any idea whether they will reach their goal but that's not the point. It’s the recognition that this is the kind of scale that is needed and that is why I would love your help to get the word out to inspire more courage to think big in everyone that hears about this.

I know many of us are completely done with anything online and yet gathering like this still has power. So, I would love to invite you to look at the lineup and see if there is a musician or speaker that touches you, and then reach out to your friends in whatever way feels easy and invite them to join.

I’m both speaking and doing live Q&As on both Friday and Sunday and a panel happening on Saturday! Here is a little video I made to inspire you or for you to share.

Being Shift, they are highly organized and so their media pack is here. Below this email is some text that might make it easier to share.

Thank you for your ongoing commitment to the restoration of our sacred forests!



Much love to you,
Clare Dubois, Founder of TreeSisters

PS. To sign up for the Shift Music Festival or to donate, click here.

Here is some text that we would love for you to share on your social media!:

Something to feel wonderful about! Music really can plant trees!

Here is a three-day online music festival aiming to raise funds for one million trees, and the lineup is glorious! I’m inviting everyone I know to at the very least check out who is playing and who is speaking (Jane Goodall, Mariane Williamson, Laura Thomas and so many more!) to help the Shift Network raise enough funds for TreeSisters to plant another one million trees.

Just think if we achieved it, it would be a game changer for all events which feels so important right now! Follow this link to find out more: https://portl.com/shiftmusicfestival/

Facebook Twitter YouTube Pinterest Instagram
Visit treesisters.org
TreeSisters is a registered UK Charity #1149961

Go here if you no longer wish to receive our emails: Unsubscribe

TreeSisters Fifth Floor, Mariner House 62 Prince Street Bristol BS1 4QD United Kingdom







