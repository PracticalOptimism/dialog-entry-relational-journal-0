
# Day Journal Entry - September 23, 2021

### [Written @ 9:16]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:02:53]

- 📝 [commentary] I talked about 'How To Have Conversations' [1.0] and 'Gigantomastia' [2.0]

[2:02:53 - 3:14:34]

- 😴 [break-time] I watched Unicole Unicron on YouTube [3.0] [4.0]

[3:14:34 - 3:28:59]

- 😴 [break-time] I didn't do anything

[3:28:59 - 6:58:53]

- 🚧 [work-progress] I wrote some code for the codebase

[6:58:53 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
How To Have Conversations / September 23, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Courses%20On%20Random%20Topics/Course%201%20-%20How%20To%20Have%20Conversations/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2023%2C%202021

[2.0]
Pornography 101 / September 23, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2023%2C%202021

[3.0]
True Intelligence & Value
By UNICULT
https://www.youtube.com/watch?v=l_dc7V2FKjU

[4.0]
Ritual & Intention (Fundamentals of UNICULT)
By UNICULT
https://www.youtube.com/watch?v=ivTD3ho_vHw




