

# Day Journal Entry - September 14, 2021

[Written @ 8:17]



.......................................................................

Post-Notes

[Written @ September 16, 2021 @ 8:59]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:30:40]

- 📝 [commentary] I wrote a lot about different things [1.0] [2.0] [3.0] [4.0] READ THE MESSAGES. APPROVAL TO DATE JON . APPROVAL TO BREAK UP WITH EXISTING BOYFRIEND . 

- 📝 [commentary] YOU NEED TO HAVE SIGNATURES TO MARRY JONATHAN [3.0]

- 📝 [commentary] YOU NEED TO HAVE SIGNATURES TO BREAK UP WITH YOUR EXISTING BOYFRIEND OR HUSBAND OR MARRIAGE PARTNER [4.0]

[1:30:40 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Universal Basic Marriage / September 14, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Universal%20Basic%20Marriage/2021%20A.D./Month%209%20-%20September/September%2014%2C%202021

[2.0]
Favors That I Ask / September 14, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Favors%20That%20I%20Ask/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2014%2C%202021

[3.0]
Approval To Date Jon
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Favors%20That%20I%20Ask/Forms%20and%20Release%20Slips/Approval-To-Date-Jon-Ide

[4.0]
Approval To Break Up With Existing Boyfriend
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Favors%20That%20I%20Ask/Forms%20and%20Release%20Slips/Approval-To-Break-Up-With-Existing-Boyfriend



