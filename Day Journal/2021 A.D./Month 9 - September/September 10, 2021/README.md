

# Day Journal Entry - September 10, 2021



### [Written @ 15:01]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[x:xx:xx - x:xx:xx]

- 📝 [commentary] I took notes on topics of interest

[x:xx:xx - x:xx:xx]

- 📝 [commentary] I took notes on topics of interest


References:






### [Written @ 14:59 - 15:00]

Hopefully laying on the bench dead in front of the library is a good way to die


### [Written @ 14:55 - 14:59]

Laying in the god's pool of blood.

I sat there in the library front entrance outside where I killed myself.

I say all of this because metaphorically I'm dying inside. I don't like myself (1) I would like to die

......................................................................

I just came back inside the library and started live streaming

......................................................................

(1) I ate lunch outside

......................................................................

(1) I ate lunch outside and I ate 2 bagels 3 orange juice cups and a few granola bars (2 or 3 or 4)

......................................................................

Then I thought about how much I've grown mentally and if I could use that knowledge and power to commit suicide

......................................................................

(1) On the bench I sat there 

......................................................................

I didn't die

......................................................................

But I saw hallucinations

......................................................................

Cartoons

......................................................................

One of the cartoons was a pair of hand wrapped with bandage

......................................................................

Blood was on the bandage

......................................................................

......................................................................

So I wonder if I got close to killing myself this time

......................................................................

......................................................................
































### [Written @ 12:49 - 13:34]

(1) A random interpretation of the album cover for "Shulman" "Endless Rhythms of the Beatless Heart"

................................................

(1) An infinity shape

(2) An hour glass

(3) A human eye

(4) One human eye to be exact

................................................

(1) Man is blind

(2) [mine][man][mind] has no time

................................................

[Mine] means "me" I am the comment of my experience

................................................

[Man] means 'expression'

................................................

[mind] means 'expressed'

................................................

Mind has no time. So there is no time because in the top of the [hour][our] glass OUR glass or HOUR glass whichever -_- time you like -_- the HOUR glass is basically full of a sand dune at the very top

................................................

A sand dune gives you no real intention of running out of sand

................................................

counting clocks and minutes is hypnotic

................................................

It's a fun illusion, no need to count. look how full the sand dune is with new sand

................................................

1 eye means the man is blind (1) they can see with one eye so they can see (2) see what? (3) life. (4) experience of man ness (5) but being blind is better than seeing because you can have faith that everything will work itself out (6) blind men need no leader (7) they wouldn't follow one if they knew one existed (8) they eat food (9) wash their pets and dogs at home (10) but don't know about galaxies and star clusters (11) they forgot their dog was made of atoms (12) that wasn't a memory they had (13) they forgot spirit exists

................................................

Blindness is beautiful

................................................

But it's too beautiful some would prefer to see

................................................

Man likes looking with one eye open and one eye close so they know everything will be okay

................................................

Just jump into the fire

................................................

(1) Perspective Swings are notated with a symbol "sg" and an infinity sign with the "right circle having a period in the center"

(2) This is 2 or 3 day old notation that I made up one day

(3) read the notes on perspective swings

................................................

Uh basically they are interesting in my opinion at this time

................................................

perspective swings are how you jump from one body to another

................................................

and one idea of something incredible to another

................................................

you can experience other people's lives possibly but that is all just a theory

................................................

................................................

Perspective swing notation with the infinity or side-ways 8 symbol with a period or dot in the right cricle pair is basically a representation of the hourglass with the eye in the center

................................................

Tilt the hourglass on its side and you'll see it looks like an infinity symbol with a dot on the right side

................................................

A dot on the left side is presented as notation that possibly could mean something different

................................................

"Endless time" is something to possibly summarize the album cover for "Shulman - Endless Rhythms of the Beatless Heart"

................................................

................................................

"I am infinite"

"I have infinite time"

................................................

Those are other interpretations since (1) The man seems to have no top half of their head (2) It's because they are incomplete if they don't include the infinite sand dune

................................................

Infinite time endless sand dune

................................................

That could be the top half of man's potential

................................................

That could be the symbol for the scalp and rest of the top of the head of the man represented here

................................................

The forehead and the scalp

................................................

................................................

"I'm a philosopher" is the expression on the man's face with the intense look

................................................

"friendship" "cowardice" "past" "forgiveness" those are all like single sand grains . . 

................................................

Man has more expressivities

................................................

"I wasn't brave enough" was a sand grain of today

................................................

"I didn't drink enough milk"

................................................

"I cried too little"

................................................

"I cried too less"

................................................

"That was sufferring hell"

................................................

"A hot sandgrain future where I lose all my girlfriends"

................................................

And so then

................................................

"What's left in the sanddune?"

................................................

Only time will tell

................................................

The nozzle in the center of the hourglass is a representation of white light

................................................

A circle. White Light. Another circle

................................................

That is how you can represent a "perspective swing" "infinity sign" then don't forget to add a dot in the center of the right hourglass circle

................................................

White light is drawn as follows

................................................

(1) The negative space

(2) between the corners of 4 circles

(3) 1 circle in the top left

(4) 1 circle in the top right

(5) 1 circle in the bottom left

(6) 1 circle in the bottom right

................................................

So you can draw even the pain symbol

................................................

pain is the same drawing process but this time take the negative space of a concentric circle inside of the touching outer circles

................................................

White Light looks something like this

................................................


                       .
                      . .
                     .   .
                   .       .
                .             .
           .                       .
                .             .
                   .       .
                     .    .
                       . .
                        .

................................................

Do you see the shape? It is like a diamond with curved sides

................................................

Hypnotize yourself into seeing the shape

................................................

A pain symbol

................................................

A pain symbol by coincidence have having studied what this looks like seems to be an inner concentric circle group and not the 4 touching circles on the outside

................................................


                       .
                      .          .
                     .            .
                   .                 .
                .                      .
           .                                .
               
               
                .                       .
                   .                 .
                     .             .
                       .          .
                        .


................................................

Do you see how inner rings could form this shape

................................................




             .............
            .             .
            .    _   _    .
            .    O   O    .
            .     .|.     .
            .      _      .
             .............

................................................

A person with a face, mouth and nose

................................................

             ...........   .
            .             . .
            .    _   _  .     .
            .    O   O    . .
            .     .|.      .
            .      _      
             .............

Draw that fact . . with a white light symbol at the corner of the man's head . . and you'll see the man looks "clean"

................................................


             ...........   .   .
            .             .     .
            .    _   _  .         . 
            .    O   O       
            .     .|.   .         .
            .      _      .     .
             ............. .   .

Draw that fact . . with a pain symbol . . which is like the points of the white light are not touching one another . . and basically it looks like the person is struggling with something

................................................

It's maybe a coincidence

................................................

White light symbol

................................................

Pain symbol

................................................

They are so similar

................................................

The pain symbol is white light without the touching points

................................................


### [Written @ 11:03 - 11:42]

[Copy-pasted from the youtube live chatbox window]

Gosh is a funny word to use `(1)` it's what you say when you had sex like 'gosh' 'gosh that was lovely' 'gosh' `(2)` it is also what you say when you're upset like 'gosh darn it' 'gosh' `(3)` you can also use it when you're happy like 'gosh what a good day' `(4)` 'gosh I love you' `(5)` 'gosh I hate life' `(6)` 'gosh is life over yet?' `(7)` 'gosh how sad can I get these days?" `(8)` 'gosh' . . 

### [Written @ 11:03 - 12:45]


`Conversation Type #1`

"Hi. My name is zegodhand."

"I am scared as shit about what is happening about this bullshit universal basic marriage."

"Do you think it's bullshit?"

"No I think it's not."

"Okay then why is that?"

(1) I think Jon is a good person

(2) He is basically not clear on the outcome himself


`Conversation Type #2`

"Do you think Jon Ide deserves this?"

(1) No because he said he didn't deserve this

"So then why are we watching this television show"

"I think aliens are forcing him to go through with it and so see what happens"

`Conversation Type #3`

"Do you think someone could have such a big ego?"

"Probably Dan Bilzerian"

"Who?"

"Hugh Hefner"

"Who?"

"Lil Wayne - I wanna fuck every girl in the world - Lil Wayne"

"Who?"

"That one guy"

"Snoop dog?"

Yea the guy who keeps snooping on everybody

`Conversation Type #4`

"Hey what do you think about this random noise called the Ecoin project?"

"Random noise? Well I would call it technically speaking as a mathematician myself 'random nonesense' [push glasses up emoji]"

"Oh gosh. You're a weeb."

"No I'm not. I'm just following the protocol."

"What protocol?"

"Didn't you watch that one video. You know the one where he made you puke."

"Oh gosh. every episode makes me puke."

"Gosh."

"gosh."

`Conversation Type #5`

Weebs are welcome here.

"Did you say that?"

"Yes. We said weeiaboos are welcome here."

"What are weiaboos again sir?"

"People that exaggerate things from video games or movies"

"Ahh"

`Conversation Type #6`

"Excuse me, Janitor. Do you know if weebs are welcome here."

"No."

`Conversation Type #7`

"Excuse me, Janitor. Do you know if weebs are welcome here."

"Ahh they sure are."

"do you mind if I tell you a joke using Hunter-x-hunter syntax?"

`Conversation Type #8`

"No one wants a weeb telling them jokes.

"But what about Kakashi Sensai?"

"You mean you're as cool and handsome and good looking as Kakashi Sensei?"

"Gosh . . call me Sakura then" [peace-sign-emoji]

"Gosh but you're a guy"

"Who cares. Marry me Kakashi"

`Conversation Type #9`

"So do you still like conversations about anime?"

"No"

"Why?"

"Jon Ide ruined it for me"

"Why"

"It's too dramatic"

`Conversation Type #10`

"Did you know girls introduce drama in clubs?"

"No."

`Conversation Type #11`

"Basically they're pretty bitches walking around and everyone's like 'let me squeeze'"

`Conversation Type #12`

"So . . what should we do about that toxic warfare"

`Conversation Type #13`

"Don't look at Jon he fails to apologize for this accident."

"He just likes teaching I hear"

"Yea, apparently a hentai culture could be better than the world today."

"yea."

`Conversation Type #14`

"Teaching is interesting"

"So jon forfeits his position?"

"yep"

`Conversation Type #15`

"Why would someone give up girls?"

"Because it's just a kink fest"

`Conversation Type #16`

"Kinks 24-7 are toxic"

`Conversation Type #17`

"Gem Goddess has already had sex I hear"

"Yes. I hear the same thing."

"Is that good or bad."

"Jon says O _ O holy shit."

"-_- what does that mean?"

"That means he's a virgin"

"-_- what does that mean?"

"That means he's afraid"

"-_- what does that mean?"

"can I intrude both of you and just say 'nigger'"

"no. that's racist"

"do you guys raise killua flags in this club?"

"no. well. sometimes."

"I'm going to lightning strike you if you use words like that"

"nigger?"

"no. niiiiigggeerrrrrrr"

"oh."

"niiiiiigggggggggggggggggeeeeeeeeeeeeeeerrrr"

"no."

'nigger'

"no."

"nigger'

'no.'

'nigger.'

'no.'

'which word do you mean?'

'the n. word.'

'nigger?'

'do you want a lightning sandwich?'

'no.'

'nigger?'

'no.'

`Conversation Type #18`

No.

`Conversation Type #19`

Why did you randomly approach me and say "no"

`Conversation Type #20`

Why did you randomly approach me and say

"Why did you randomly approach me and say "no""

`Conversation Type #21`

"Why did you randomly approach me and say "why did you randomly approach me and say why did you randomly approach me and say why did you randomly approach me and say why did you randomly approach me and say why"

-_-

`Conversation Type #22`

"Why did you randomly approach me and say "why did you randomly approach me and say why did you randomly approach me and say why did you randomly approach me and say why did you randomly approach me and say why"-kill-streak-edition

-_-

"are you finished?"

`Conversation Type #23`

"Why did you randomly approach me and say 'are you finished?'"

I didn't say that.

What are you talking about?

I thought you approached me and said "are you finished."

"oh. no. that was the previous guy."

"oh. sorry."

`Conversation Type #24`

nigger

`Conversation Type #25`

Do you think people like when you walk up to them and say that word?

`Conversation Type #26`

As long as there's no rape and war

`Conversation type #27`

As long as there's no rape and war

"Woah. that's random. why did you bring that up?"

"Uh. wait. who are you? Am I talking to the president? Will you solve world hunger?"

"uh no sir. I'm not the president. I'm a random stranger you just met on the streets.

"Gosh"

`Conversation Type #28`

"Gosh"

`Conversation Type #29`

"Gosh is a good way to start a conversation"

"Gosh means depression doesn't it. so why talk about depression"

"Well I'm not your therapist but you need to get out there and start talking to people"

"Gosh"

`Conversation Type #30`

"Gosh"

"Oh. I'm a woman. I'm married to Jon. I'd prefer not to talk to you."

"Oh. I see the wedding ring. Sorry."

`Conversation Type #31`

"Gosh"

"Do you already have a fake wedding ring on."

"Yes. Eugenia Cooney bought me one with her Ecoin."

"Eugenia Sullivan Cooney?"

"Yes."

"Wait so does that mean you're married to that girl with an incredible thigh gap?"

"Oh yea. well it's only through Jon because (1) I have to get married by choice (2) I have to get married because I want to (3) gosh I'm cumming just talking about him (4) I like Eugenia's style"

"Well so Eugenia Cooney bought you that wedding band?"

"Yes. She's very rich."

"All her social positivity ranks high in society"

"Gosh"

`Conversation Type #32`

"Gosh"

"Yes I know. We have so much to talk about"

`Conversation Type #33`

"Gosh"

"I'm not your therapist bro"

`Conversation Type #34`

"Gosh"

"Uh, are you a social hacker?"

`Conversation Type #35`

"Gosh"

"Uh"

`Conversation Type #36`

"Gosh"

"Do you know what the gosh syntax means?"

"No. What's that"

"Ahh forget it"

`Conversation Type #37`

"Gosh"

"Gosh syntax cool"

"What do you want to talk about?"

`Conversation Type #38`

"Gosh"

`Conversation Type #39`

"Gosh"

"Get laid to any girls on the internet?"

`Conversation Type #40`

"Gosh"

"Get laid to any girls on the internet?"

"No I'm following Jon's rules"

"Ahh. An official."

`Conversation Type #41`

"Gosh"

"Get laid to any girls on the internet?"

"No I'm following Jon's rules"

"ahh. An official."

"I'm an official too"

`Conversation Type #42`

"Gosh"

"Get laid to any girls on the internet?"

"No I'm following Jon's rules"

"ahh. an official"

"I'm not an official"

`Conversation Type #43`

"Gosh"

"Get laid to any girls on the internet?"

"No I'm following Jon's rules"

"ahh. nice"

`Conversation Type #44`

"Gosh"

"Get laid to any girls on the internet?"

"No I'm not gonna tell you whether or not I'm following Jon's rules. In fact ignore what I just said I'm just talking outloud to enunciate my complete silence"

`Conversation Type #45`

"Gosh"

"Get laid to any girls on the internet?"

[silence]

`Conversation Type #46`

"Gosh"

"Get laid to any girls on the internet?"

"No. I'm following Jon's rules."

"Why"

`Conversation Type #47`

"Gosh"

"Get laid to any girls on the internet?"

"yes. tons."

`Conversation Type #48`

"Gosh"

"Get laid to any girls on the internet?"

"No. I'm part of the band of the hawk"

`Conversation Type #49`

And that's basically all the conversations we could have had

`Conversation Type #50`

And that's basically all the conversations we could have had

'interesting'

we didn't even talk about computers or robotics.

yes. you see my complete approaching you and saying nothing for 10 minutes before talking is a simple mathematical representation of how precious time is.

You see concentric ring theory is basically an answer to what it means to be alive.

All around us we see concentric rings.

They are complete nonesense.

I agree kind sir please do continue.

Basically the answer comes down to the problem of concentricity

Are you suspicious that concentricity is too concentrated?

No not at all

Then why would you spam the idea that our conversation is so precious and rare

Basically I'm asking you the same thing

Then it comes down to simple concentricities. Which types of concentricities need to be represented in which concentric agenda ring

An agenda ring is simple . even a first grader can figure out what that is .

Yes. it's concentric circles

yes.

So is our conversation concentric enough to really append to the list of concentrated agendas

no I just wanted to see how your day was

So then my day needs more suspicion

Yes. Suspect that my interest lies in the concentrated form of your replies

My replies or your replies

either one

Just tell me to pause for 10 minutes to concentrate on your next observations

10 minutes of concentrated observations. I have meters for my attention span such as having to get the food out of the oven

The oven is old school my friend. simply concentrate 

Concentrate on which concentricities

yes. Let's bring a third person into our conversation

Let's get their concentrated opinion

Do you concentrate dear sir?

Yes. Concentration is crucial it's basic concentric circle theory everyone knows that.

Concentric circle theory is wide spread but truly the limitations seem to concentrate around concentricities of invaluable concentrations

Well it's up to the observer which value to place on the valuation of the evaluated valuations of invaluable valuables

so basically you're trying to concentrate on what I said wrong

no I'm simply pointing out a mistakes

No mistakes are possible since concentrations are efficient

efficiency is not a concentricty that I care for

Yes but if you concentrate on ideas you might get new efficiencies

Yes. new efficiencies seem to lack concentricities.

Dear president sir. Do you mind chiming into our concentrated conversation

as long as it doesn't take too much concentricity from my calendar

concentricities from your calendar. how many can you spare

2 hours.

okay let's concentrate

2 hours of concentration

let's say hypothetically that we began on a planet called 'zlarb' 'zlarb' was fully of magnificent creatures

let's say that concentration takes too long

yes but zlarb is a good concentration do you not agree

yes. it's different from earth as a standard concentrations god

but it isn't good to mix go with god

going and god don't make sense

which concentrations are you focusing on

yes our conversation seemed to accidentally veer into concentricity ABC insead of concentration XYZ

And so that was a failure

Yes. but if we concentrate on concentric circles we should find our way back

no let's stay focused

2 hours has become 10 minutes you men are pissing me off

10 minutes of concentricities gosh

Goshy

Gosh we need to concentrate

10 more concentrations point erased from my calendar. this conversation is verring off into concetric version number 10 which is off my radar

10 more concentrations from me as well

are you using numbers in your conversations

only when we need to

10 concentrations need to be added to each conversation in the future so people don't get bored of talking to one another

'my concentric circle rings are low'

'my concentric circle rings are out'

'my husband bans me from concentrating on this'

'concentration on these concentricities is ineffective'

'do you still'

'think about'

'what we concentrated on before ecoin'

'that is no longer my concentration official'

'I understand'

'I am concentrating on new efficiencies'

'concentric circle theory right?'

'that is an efficiency about concentration'

'so then do you wish to re-concentrate on our previous concentrations, c'mon janet don't fail me on this. just concentrate'

'no.'

'jon is a good husband to me.' i must concentrate on our continued marriage

'jon is shlong' his name doesn't even make sense. he's blarb from reality number slub.

'don't use rude slurs from concentration abc to confuse me'

'rude slirs like nigger would be from concentration number xyz which are more familiar to me'

'you're trying to confuse me. I wish to concentrate on simple life'

'simple life with me is better.'

'yes. fine I will not argue with you.'

'okay. so come back with me

[this concentricity now veers into personal individual concentricities which need to be worked out on personal individual concentration levels which this conversation doesn't know how to entail]

so basically you have to do a 'perspective' 'swing' which isn't a concept that I've talk to you about or taught you on this live stream so far. . you may have read previous notes but those were not concentrated on the live stream if you're watching one-by-one and not jumping around . . you're supposed to watch episode 1 through whenever in a sequence and not just jump around . . it's basically a story

yes but what are perspective swings

................................................

You can read the notes in this journal entry for today to learn more.

................................................

our concentricity conversation basically has no way to be ended. So long as we concentrate on particular concentricities

if the concentricities are new to individuals then a concentration ring of education catch up concentricity needs to be introduced like how Jon Ide introduced the conversation of what does "reverse" mean as in "reverse indexing" basically we can walk through that now since the education is so straight forward but the concentricity would distract so many audience readers on the live stream that that distraction concentricity is just so supreme . . it's better to concentrate on the nice humor of concentricities of concentricities

................................................


`Conversation Type #51`

"Hi mom"

"It's Jameson"

"How is being raped by Jon Ide"

"It's good honey"

"I know we have to meet publicly in these family reunion facilities if we ever want to talk"

"we can meet other places too if we really need to but it's recommended practice to not see one another again so Jon gets to rape you whenever he wants"

"Yes. I know sweetheart. You're my first born son. You're jon's good friend."

"Yes mom."

"By the way mom. I'm curious why are you so hot all of a sudden. How many haircuts did you go through to get to this one. It's so selective."

"10 haircuts. I pampered myself dry with ideas. I used string theory to try to make braids of a nice haircut."

"mom, quit the bullshitting"

"are you sucking jon's penis 24-7"

"more than you know"

"it's crazy in there"

"I sleep"

"brush my teeth"

"meet with my husband"

"suck on alanna's pussy"

"it's heaven"

"mom, do you think heaven should really exist on earth?'

"no. I think it's just a dream. I seem to have too much cum in my throat that's why I'm talking like this."

"oh gosh mom. don't be horny around me. You know I'm an official."

"Do you enjoy being neutralized?"

"No. it's barbarian nonesense sometimes I sneak a peak when I can but I'm trying to be a real official one day"

"what do you sneak a peak at?"

"Cartoon hentai porn"

"ahh so basically you're trying to see how cartoons move"

"exactly mom, if I can get the right angles and trajectories down in my journal notebooks I can basically see new types of buttholes"

"new types of buttholes. gosh."

"that's naughty for a son your age."

"I'm only 23,000 years old mom"

"23 thousand years old is young for a sweet son like you"

"well compared to dad who's 23,060"

"yea yea"

"he's only beeting you by a small amount. oh well"

"say mom. do you miss dad?"

"no"

"why not"

"you know that's a private conversation. I'm brainwashed"

"gosh"

"do you watch any effective cartoons."

"At my age? Gosh"

"I'm being gardened and grown like a seed in there"

"I'm filled with honeypots"

"honeypots?"

"(1) food (2) wine (3) love from my husband (4) kisses from my wanna-be-tom (5) mom-tom vibes (6) and sometimes I sneak into the middle and cuddle with sweet Jonny poo when I need to"

gosh. it sounds like a dread show in there mom

Yea it is scary

Jon is too loved

But mom. 23 thousand years of love is too much . surely.

Yes genetic engineers sure did a good job

Yea mom but we're all genetic engineers

sure are

alright mom it was nice talking to you in this prison-like format

No it was nice talking to you son

Well

Well

Goodbye

see you in 2 decades I guess?

Sure

`Conversation Type #52`

Genetic engineers sure did a good job

`Conversation Type #53`

Aubrey de Grey

`Conversay Type #54`

Gandalf

`Conversation Type #55`































### [Written @ 11:03 - 11:18]

[Copy-pasted from the youtube live chatbox window]

Do you and I need to recite what to do together? Let's go through a mock conversation.

### [Written @ 11:03 - 11:17]

[Copy-pasted from the youtube live chatbox window]

Inner eyes means (1) you are good at single partnership and can dance by yourself pretty well (2) outer eyes means you focus on social affairs

### [Written @ 11:03 - 11:17]


[Copy-pasted from the youtube live chatbox window]

Inner eyes or outer eyes?


### [Written @ 11:03 - 11:16]

[Copy-pasted from the youtube live chatbox window]

"What eyes do you look with?"

### [Written @ 11:03 - 11:16]

[Copy-pasted from the youtube live chatbox window]

It's a copy-paste script

### [Written @ 11:03 - 11:15]

[Copy-pasted from the youtube live chatbox window]

"What do you do for a living" "I'm a photographer" That should be the first question and answer that everyone gives to one another in the future

### [Written @ 11:03 - 11:14]

[Copy-pasted from the youtube live chatbox window]

Teenagers mastered this by ignoring their parents and trying to work things out on their own in their mind or in a journal notebook

### [Written @ 11:03 - 11:14]

[Copy-pasted from the youtube live chatbox window]

Copy-paste rituals don't help make things better (1) people want to walk away from conversations (2) talking is boring (3) let's use hyperspace conversations like silent talking

### [Written @ 11:03 - 11:13]

[Copy-pasted from the youtube live chatbox window]

(1) one thing wrong with corporatism today is not being inclusive "everyone truly is a mighty goal Guts"


### [Written @ 11:03 - 11:12]

[Copy-pasted from the youtube live chatbox window]

But corporatism or cooperatism helps ensure everyone gets fed

### [Written @ 11:03 - 11:11]


[Copy-pasted from the youtube live chatbox window]

Corporate cringe (1) people are too uptight (2) they're too lonely (3) they don't have any friends (4) or they feel anime isn't good 

### [Written @ 11:03 - 11:10]


[Copy-pasted from the youtube live chatbox window]

Everyone has their own dreams (1) but can they really achieve them ? (2) They are . . together . . all of these individual flames . . or individual fires . . together . . they are . . individual . . and as a collective it looks like a bonfire . . of dreams . . look . . 

### [Written @ 11:03 - 11:07]


[copy-pasted from the youtube live streaming chat box window]

Basically the band of the hawk

### [Written @ 11:03 - 11:06]

[copy-pasted from the youtube live stream chat box window]

zegodhand gives pretty good reviews. Go watch their YouTube channel

...............................................

stalgul
https://www.youtube.com/c/gamerguygames/videos

...............................................


### [Written @ 10:54 - 11:02]

[copy-pasted from the youtube live stream chat box window]

Unfortunately for Guts. Griffiths friend. That broke his heart. Griffiths words were too strong. Basically guts realized that he doesn't really have any dreams . . so how could he be friends with Griffith . . the mighty leader that he had been fighting for for a long time ? . . Griffith provided shelter to him when he was weak . . and now . . a speech like this shatters him because he can't truly be Griffith's friends . . he can only be "griffith's loyal soldier" loyal soldiers only help Griffith to achieve his dream . . they can't truly be friends . . because Guts has no dreams of his own . . 

### [Written @ 10:54 - 11:02]

[copy-pasted from the live stream chat box window]

"A man who calls themselves my friend must have his own reason for living. And must be willing to sacrifice everything he has to achieve his dream. Even if it's against me. A man who would call themselves my friend must be equal to me in all respects."


### [Written @ 10:54 - 11:02]

[copy-pasted from the live stream]

It studies the philosophy of friendship (1) what makes a friend a friend (2) "one who follows one's dreams regardless of what the friend says" (3) That is one thing Griffith says in the show . . something like that . . don't quote me (4)


### [Written @ 10:54 - 10:56]

[Copy-pasted from the live stream of this video]

I'll be your fearless leader (1) Griffith (2) And you can all choose one of the characters from the show which stays an official or is casca and gets raped by fempto-me


### [Written @ 10:53 - 10:54]

[Copy-pasted from the live stream youtube television show chat box window]

(1) Why is Ecoin your favorite television show? (2) Because we are going to be the band of the hawk from now on

### [Written @ 10:53 - 10:53]

[Copy-pasted from the live stream chat box window]

We're all caught up on all the notes (1) I did some filling in of episodes that I forgot to transcribe . . and actually placed the video description in (2) I did that off the live stream so you didn't need to watch me do that (3) basically I gave the videos a description like a timeline annotation so you don't have to sit through all the videos (4) although it's highly recommended to watch all the videos (5) in time you might get a chance (6) basically binge watch your favorite ones (7) I know they are all really long (8) like 8 hours or whatever (9) 9 hours or more . . whatever . . just sit back and relax and watch your favorite television show

### [Written @ 10:50 - 10:50]

[copy-pasted from the youtube chat window]

(1) sad (2) virginity (3) "The first original creative spark" (4) rotten


### [Written @ 10:48 - 10:50]

[copy-pasted from youtube live stream chat box]

Why are voice actors really special people (1) they are basically anonymous (2) your name never needs to come out to anyone (3) have you ever asked yourself (4) what does this person look like in real life? (5) no? (6) that's good (7) it's basically because your voice doesn't necessarily give away how you physically look in real life right? (8) you could be fat and chubby (9) or skinny and tall (10) and no one would know that's what you look like (11) isn't that a cool super power? (12) anonymity? (13) it's just anonymous people helping each other out (14) relationships are overrated (15) "You're my cousin" (16) in a peaceful society (17) You still need relationships (18) "You're my brother" (19) "We work together" (20) our community has 0 zero 0 zero zero 0 rape and pedophilia claims since Jon Ide came and took over (21) That means no woman (22) is being raped (23) because Jon married them all (or a lot of them anyway) (the ones that decided to not be ugly) (24) They either got gigantomastia (25) brushed their teeth (26) straightened their hair (27) got a degree like Akiane-chan with a sweet heart (28) Akiane-Sama (29) Akiane-Sama (30) Honorific (31) and so then they moved forward (32) this is necessary so Elon-Jon-Ide-Musk doesn't have to swap moon crystals with girls that don't like being around one another (33) no more naughty space tourists (34) all of the nonesense conspiracy theories about aliens (35) are already addressed in the book library (36) A lesson in ancient history (37) A lesson in recent history (38) A time line of events (39) A lesson in biology [basically we're genetically engineered creatures] [sort of how like akiane makes art. You can genetically create your own organisms] [which is a scary future by the way so an alternative is Jon Ide saying fuck that and so humans can be humans] [no need to explore stupid bullshit] [just be a human] (40) A lesson in science (41) A lesson in immortality (42) A lesson in the future (43) . . . . . . . . . . . We are infinite spirits. Is-be. is-be. is-be's. is-bees. is-bes. be. being. is-will-be. . . . . . . the domain has plans . . I think that's what they are called . . "The Domain" . . an alien supergroup that inhabits more than 1 / 4 or one fourth of the Universe . . so that's a big hypnotic trend right? . . I'm not sure . . 

### [Written @ 10:37 - 10:38]

[Copy-pasted from the youtube live stream]

A break through award refers to my hopes that I get some kind of break through (1) maybe the marriage doesn't happen (2) I get too much angst (3) not only Isabel should be concerned (4) but her family as well (5) Officials are people that decide to not sleep with Isabel Paige or her womanly counterparts any more (6) How many officials need to exist on planet Earth? (7) And then what about vaginoplasty (8) replacing your vagina (9) that seems sick (10) you should be able to keep your vagina (11) how ugly to replace it (12) but then again human growth tissue grows back quickly and it's as if you have a new vagina every year or so or multiple years or something like that (13) this universal basic marriage stuff gives me angst. A break through would be (1) me getting out of this angst (2) You have the codebase now (3) go do something with it (4) either expand your mind and learn about the possibilities (5) reject the marriage or accept it it doesn't bother me either way (6) It's not that big of a deal (7) I don't like it that much given that I have to steal non-virgin girls from existing men (8) but I do like it somewhat if I could possibly not have men to begin with (9) so if that virtual reality existed for me (10) I wonder if I would like that (11) I'm getting suggestions that maybe it's better to live in this virtual reality (12) The idea is the all women who aren't my mom, sister or ugly women (13) could be Jon Ide's (14) but then again how many divorces would I have to go through? (15) I don't wish to look at the history of the woman (16) And I don't see the advantage of not just having 1 nice woman (17) Am I really just helping humanity move on from marriage to a woman (18) Is it really helping? (19) If it's genuinely helping that is serious to consider (20) for example that could mean there are serious benefits to not fucking around any more (21) serious benefits that I didn't even take into account (22) my focus has been on the distress that it causes for existing couples (23) existing couples that have been long time partners (24) how much distress (25) how selfish of me to think of only the distress when possibly men could overcome their fears and move to new territories of idea (26) me fucking any woman I would want is an amazing idea (27) Alma Deutscher is a perfect girl (28) she can focus on all aspects of her talents including music and art (29) and at night time I can rape her like a hearty nice husband (30) and make her feel the pain of wanting to be inside her for eternities (31) and then I can listen to her music as she continues to develop her skills (32) so then it would be a talented decision for citizens of earth to give me all their Alma Deutschers (33) All of their Akiane Kramariks (34) All of their Gem Goddesses (35) All of their Temples of the Celestial Priestesses (36) Or Christina Martines (37) or all of their Krista Raisa (38) Or all of their girls (39) All of the girls would enjoy that television show (40) not for 10 years (41) for forever (42) no need to sleep around (43) just pick flowers (44) take pictures (45) watch movies (46) celebrate birthdays (47) laugh cry and sing (48) then have mad sex (49) and forgot we were alive (50) since only one husband was there (51) memory goes away (52) women as time keepers no longer get cat called (53) no more rape victims (54) no more concerns (55) just hearty heart fucking with Jon Ide only (56) hearty heart piano music (57) and officials clapped (58) they didn't give fist bumps (59) they didn't give high fives (60) in fact they were forgotten about (61) women wanted safety and security (62) the future with random men is over (63) classmates are beautiful (64) no need to shrug your shoulder (65) you knew that since 6th grade (66) now it's rape time with jon ide (67) and then you can be an evolved spiritual human being (68) and no one gets jealous (69) except maybe in the first 2 weeks or first 2 years (70) but then everyone calms down (71) and says good job jon. (72) I'm glad you got your act together to finish your project (73) now I need to get my act together to see what I'm going to do next (74) do you need a black smith jon? (75) do you need someone to tie your shoes? (76) do you need any assistance with anything? (77) I'm glad to help (78) My number one priority is the safety of these women (79) if your quest ensures their safety (80) don't mind me (81) I'll be in virtual reality doing god knows what (82) I'll cry sometimes because I miss the good ole days (83) But I'll remember there are plenty of other things to do (84) like saying hello to friends that I never met (85) bonding over some tea (86) sitting in Japanese Saunas (87) crying (88) watching dragon ball z (89) making my own cartoon show (90) getting people together to make movies (91) make video games (92) act (93) play in a movie (94) be a voice actor (95) by the way voice actors are really special people

### [Written @ 10:18 - 10:18]

[Copy-pasted from the live stream]

Always adhere to the concept of peace as you reach for something, currently unknown, and make friends with energies whose looks could frighten others.


### [Written @ 10:13 - 10:18]

[Copy-Pasted from the YouTube live stream]

She doesn't know how good it feels to watch her make mistakes. She slurs her words and walks around. She walks and simply walking she immediately arouses the heart's suspicion that her gigantomastia breasts are perfect and careless and free. A woman with gigantomastia is so free. She walks around swinging melons on her chest and those chest watermelons are so accidental in the way they smile to the world. Her gigantomastia macromastia hypermastia giant breast areolar gland areola peaked mountain globe watermelon fields of grass fields of rainbow fields of flowers fields of accidental dragon egg rebirthing is a burial ground for mens' eyes as they smack dead at the fence of this giant graveyard of zero gravity free floating gigantomastia hypertrophy hyper mastia trophy worthy trophy deserving gigantomastia trophy loving globe water balloons of insight and real physical treasure like grease for the mind and grease for the heart to swing around the world with all the love and casual inspiration that the gigantomastia inspires. She walks around swinging melons on her chest. Those melons are casual and all seeking and friendly and all deserving. She walks around with a fruit basket chest filled with treats. She walks around with fruit baskets filled with teets or tweets or treats or tits that are pressure valves of inspiration that pull back cartons of milk from behind the veil. Her large soft tissue breasts break into world record domains of perfection and casual expression and impress the gods with all the cartoon infinities that she propagates and pushes forward with just a single walk. All of that being said her casual one sided nature is so casual that she pivots her foot and accidentally stumbles onto bridges and hilltops of ideas like accidental backpeddling. All her glorious lovely patient breasts hicup at the thought of accidentally walking in the wrong direction. Accidentally walking in the wrong direction the woman breathes in air and continues to seek solitude in her mind. Peaceful her patient gigantomastia waits for her to take the reins of her heart chariot so she can bounce walking around and realize the purpose of her casual life is to walk. Walking is such a casual activity but at least her breasts get to jump and reach for her chin or jaw as she smacks the pavement with her footsteps. Such casual joy and reach. Gigantomastia should be adopted by any woman with a brain. Gigantomastia for Jon Ide's wives is a standard. In a universe where Jon Ide gets what he deserves then each woman with gigantomastia will secure her treasure and casually enter the world without headaches The woman holds her head high. The woman pounces on the opportunity to keep an attitude about her by acknowledging how painlessly precious her giant breasts are. All those glorious nipple slips and boob shakes belong to Jon Ide only. Jon will memorize them. Jon will have a perfect life with perfect areola slips. Only the most perfect areolar nipple slips that are too casual and friendly or unfriendly. Delicate nipple slips from the finest french restaurants and cafés all for Jon's eyes only. All the greatest videos on earth wouldn't compare to a gracious fine grain whole wheat nipple slip free from enprisonment. No genetic modification. Just casual my-clothes-dont-fit-sorry-hoe casual atmosphere. Casual.


### [Written @ 9:50 - 10:00]


**Here are the latest thoughts - 0 time indices old**
... At time index 3 I started to write a new paragraph . . this could have been 1 hour later or 2 hours later but all I had to do was scroll up to make it convenient for myself to append to the list
... scrolling up is easy
... it just means all my latest thoughts for the day are at the top of the page
... all my latest convenient-to-access thoughts are at the top of the page
... my older thoughts are at the bottom of the page

**These thoughts are 1 time index old**
... This next stuff will be startin from time index 2 . . I decided to stop writing stuff at time index 3

... here is some stuff

...here is some stuff

... here is some more stuff

**These thoughts are 2 time indices old**
........This was written from time-index-1 to time index 2
.... I write notes here . . 
... here is another paragraph of the notes
... Here is another paragraph of the notes
... here is a final paragraph of the notes.
... I went from top to bottom in the way that I write. Normal stuff right?
... Now the confusing part is having to scroll up . . 
... to read the next time index

**The beginning of the journal starts at the bottom here**


### [Written @ 9:36]

I copy pasted notes from my cell phone so please go make sure to read those.



### [Written @ 9:31 - 9:32]

[Notes That Were Copy-Pasted From My Cellphone Start here]
.......................................................................

Day Journal - 3


[Written @ September 10, 2021 @ 6:55]

"Have a mathematical evening" gun.

A brilliant scientist

Carries around a gun called "Have a mathematical evening"

Can you imagine the shape of the gun?

It's a beautiful shape if you use your imagination.

(1) You walk door to door to your neighbors doorstep

(2) There's an atmosphere of music and a dormitory

(3) Camp fire is in the background

(4) A school shooting could take place any minute

(5) To your girlfriend-to-be

(6) A kind handsome young mathematician

(7) Says to this no-one woman

(8) She's a nobody

(9) Too perfect to be anyone in particular

(10) And so the mathematician says

(11) Have a mathematical evening

(12) In a hushed tone

(13) That suggests the two people had never met each other

(14) Have a mathematical evening

(15) Like a friendly forgetful ghost would say

(16) To visit his living wife

(17) The widowed student in her dormitory room

(18) Doesn't respond

(19) And no one thought it was harmful

(20) Just a random noise like a garden gnome in the backyard

(21) A random garden gnome

(22) with random nonesense to sell

(23) Like staring into nonesense

What a gun

Go die.


[Written @ September 10, 2021 @ 6:53 - 6:54]

John Brainbow

John Rainbow


[Written @ September 10, 2021 @ 6:45 - 6:46]

An ant's face on the mountain side.

Like mount rushmore but with an ant head


[Written @ September 10, 2021 @ 6:32]

Do you need your shoes tied?
Can I tie your shoes for you?

2

......................................................................

Do you need your picture taken

1

.......................................................................

Do you need a picture frame?

1

.......................................................................



[Written @ September 10, 2021 @ 5:44]

A list of lists 

.......................................................................

Basically

.......................................................................

Gossip about gossip

.......................................................................





[Written @ September 10, 2021 @ 5:33]

Suspicion.

It was suspected.

"We still have many turns left before I have to wake up." 6 o'clock is when I have to be awake

Suspicion

.......................................................................

Even more concentrated than concentric circles


.......................................................................

Turns. Windings. Twists. Rotations. Perspective Swings.

These are all technical terms.  "Twists and Rotations" haven't been defined by me yet even as I write these notes.


They are too turny to have a definition

.......................................................................




[Written @ September 10, 2021 @ 5:09]

A break through award.

(1) I have headaches

(2) Isabel Paige already being creampied gives me angst

(3) All the girls like Isabel

(4) Creampies are delicious

(5) They feed a girl's heart

(6) But I'm still nervous

(7) I repeat to myself "stick to the concept of peace"

(8) A million times a day

(9) I hate infidelity

(10) There is no official real owner of a woman

(11) There are only copy cats

(12) It's sad to say

(13) But when you die

(14) And your wife dies

(15) What a waste of pussy

(16) She could have been used for at least 2 more eons

(17) It's like no one takes life seriously

(18) 2 eons of joy riding is easy

(19) 2 eons of thanks for everything you do is easy

(20) 2 eons is nothing

(21) So just invent life rejuvination technologies and let that be that.

(22) And let me apologize for spelling rejuvination incorrectly

.......................................................................

If all of this project dies down

(1) I resolved my love crisis with Isabel Paige and all other virgin girls that should have been virgins and mine from day 1

(2) Or girls lost interest

.......................................................................

Virginity.

.......................................................................

The first original creative spark

.......................................................................

If I took that virginity first

.......................................................................

Doesn't that make love all the more meaningful

.......................................................................

So how can I truly love a woman whose virginity was not mine?

.......................................................................

Whose first original creative spark was not mine

.......................................................................

A dream creates originals

.......................................................................

So I need a new dream

.......................................................................

This dream is spoiled

.......................................................................

Rotten

.......................................................................





[Written @ September 10, 2021 @ 4:59 5:03]

"I'll be your psychic father says a random ghost that no one has met." The ghost says this in a meme "hehe" type of way

It's only funny because it looks like "Random nonesense" is what the psychic child seems to be studying

"Why would I need a father if all I'll be studying is random nonesense" says the child

"Hehe says the father. That's random nonesense for me to know and for you to find out"

.......................................................................

Random nonesense

.......................................................................






[Written @ September 10, 2021 @ 4:58 - 5:04]

Do I have your ears says a random Mark Anthony


[Written @ September 10, 2021 @ 4:56 - 5:04]

Random observation.

The question mark symbol looks like a "ear"

.......................................................................

?

.......................................................................


[Written @ September 10, 2021 @ 4:51]

[Continuation of yesterday's notes on concentric circles]


(9) Draw that tertiary reverse irreversible spiral from the inside out

(10) Which is non-standard because as mathematicians we like to use notation that doesn't introduce capacities of expanding

(11) Drawing from the inside out means there is always



[Notes That Were Copy-Pasted From My Cellphone have been ended here]
.......................................................................




### [Written @ 9:30 - 9:31]

[Notes That Were Copy-Pasted From My Cellphone Start here]
.......................................................................

Day Journal - 2

Slowed

[Written @ September 9, 2021 @ 20:53 - 21:07]

Picture this in your mind

(1) A mathematics that assumes

(2) You've exhausted all of your options

(3) No more than

(4) Such a mathematics then further assumes that there is notation for you to express all the other things you've considered

(5) Concentric Circle Theory is what Jon Ide has hypothesized for the past few months now

(6) Concentric circles are flat and representable on sheets of paper

(7) The notation look beautiful

(8) Basically any individual circle has no real meaning

(9) But neither are the circles unique or separate

(10) Uniqueness isn't assumed

(11) Separateness isn't assumed

(12) But thank goodness the notation looks cool on paper

(13) Concentric circles don't represent anything

(14) And are thus like a basic Aryan computer which is any computer that listens

(15) Recall that the number 5 is an Aryan computer

(16) It means 5 doesn't mean anything as a symbol

(17) It only means geese when you ascribe geese to 5

(18) 5 geese

(19) Or 19 geese

(20) You can give a 2 hour lecture on Aryan computers

(21) I invite you to do so so that I don't need to type more

(22)

.......................................................................

(1) Concentric Circle Theory

.......................................................................

Do you like notation?

.......................................................................

Aryan computer notation is a basic notation for you to find and discover

.......................................................................

Zig zags on sheets of paper

.......................................................................

Trace paper

.......................................................................

You are basically using a lot of notation like dashing your eyes left and right. Dashing your lips up and down

.......................................................................

Human bodies as notation

.......................................................................

Concentric circles

.......................................................................

So does looking at a pretty girl walk down the road sound like good notation to you?

.......................................................................

You should say "No. It's just concentric circles Jon Ide"

.......................................................................

In terms of time and space no such qualities exist with concentric circles

.......................................................................

Mathematically they are perfect objects

(1) You can nest more and more concentric circles to have all the creampies you could imagine. Babies inside babies and creampie all of them with your pencil dick drawing.

(2) Girls have no problem imagining that the first outer circle should be drawn first in standard notation

(3) Maximum limits are good for human brains

(4) And then limitations come down to your instrumentation like how precious your pencil lead is to enscribes more and more loops of inner windings

(5) This notation of windings double loops on itself when I teach you further what windings are or look like. That is a later chapter my fair students.

(6) Concentric circles.

.......................................................................

(1) Circles are difficult to draw

(2) Difficult notation needs to be maximized with one stroke. Mathematicians love single strokes. Sex. Baby. That's 2 strokes. Baby. That's one stroke.

(3) Genetic Engineering makes single strokes easy so if people want to preserve a semblance of hope of 2 strokes for baby then vote for Jon Ide Pedro Concentric Circle. Pedo pedophile pedro from that one movie vote for pedro pedophile movie.

(4) Concentric circles

.......................................................................

(1) Difficulty gets no more difficult than drawing a circle

.......................................................................

In concentric circle theory 1 circle represents all the other circles one could possibly imagine or draw.

.......................................................................

One circle exhausts all the other concentric circles

.......................................................................

(1) Circles

.......................................................................

Drawing on sheets of paper can be complicated

.......................................................................

"Bitch" is not what Rick and Morty want to use for notation of the future.

.......................................................................

"Bitch" Like freddy kruger or "bitch" like "fucking Christ just give me all your money." or "Christ I want all possibilities to last in this single moment. Give me everything you've got. Go go go. Go. Go. Go."

.......................................................................

Random nonesense

.......................................................................

Random nonesense is very difficult to convince a mathematician that your work is elegant or professional.

.......................................................................

Professionalism is random nonesense for people to exist in orders of magnitude around one another

.......................................................................

Elegance is random nonesense for people to say "oh look" "that answered my random nonesense question"

.......................................................................

Circles are difficult to draw

(1) You need to be patient

(2) You need to time each perspective swing just right

(3) But at the end of the day you can show your boss or co-worker

(4) That you completed something easy

(5) At the end of the day all that patience and hard-to-learn-at-first drawing of this random nonesense object

(6) Pays off

(7) And you realize all work ever is is (1) perspective swings and (2) random nonesense like circles (3) So why not treat everything as a circle. Just like in group theory

Group theory is elegant but its short comings come in the form of not having enough perspective swings at any given moment

.......................................................................

Group theory

.......................................................................

Perspective swings are really easy to draw on a sheet of paper

.......................................................................

They are coming from the theory work that I did, Jon Ide, as Jon Ide I did this work to try to understand certain thoughts and ideas.

.......................................................................

(1) Draw a concentric circle ring

(2) Draw an arrow pointing down

(3) At the end of the arrow pointer symbol which looks like a capital letter V

(4) Draw a concentric circle ring that starts from the reverse polar opposite ring

(5) This drawing method is called a tertiary reverse irreversible spiral

(6) Tertiary means 3

(7) Reverse means something like reverse Indexing in search engines

(8) Or artificial intelligence projects could also have reverse indexing

(9) In 2021 I believe reverse indexing is a new term

(10) Basically consider the following structure


(10.1) You have a dictionary or hash table or hashmap filled with items. 

(10.2) A hashmap can look like this

(10.3) Item called "banana" is on the top shelf. { "banana": "top" }

(10.4) Do more research on hashmaps.

(10.5) It is important to point students to references like at the index 10.4

(10.6) When further education is assumed in order to grok the material

(10.7) Basically, a hashmap looks like this { "banana": "top-shelf", "apple": "bottom-shelf", "grapes": "middle-shelf", "oranges": "middle-shelf" }. Vote for pedro to be the middle shelf for universal basic income. "Middle or median" means there is religion on why that random nonesense is prioritized.

(10.8) The middle person in a list of sorted individuals is nonesense ally assumed to be the most "middle grounded" or "level headed" or "neutral" or "in between"

(10.9) Read all of these notes on Ecoin even if they are super long

(10.10) Take notes yourself even though technology might make your notes just random nonesense that you did that one day

(10.11) { "banana": "top-shelf", "apple": "bottom-shelf", "grapes": "middle-shelf", "oranges": "middle-shelf" }

(10.12) That is called (1) an index or (2) a hashmap

(10.13) indexing can be used to refer to indexing an array or indexing a hashmap or even in more general places

(10.14) try to answer the question "what does he mean by "more general places"

(10.15) More general places mean (1) there are algebras more complex that lists and maps

(10.16) { "banana": "top-shelf", "apple": "bottom-shelf", "grapes": "middle-shelf", "oranges": "middle-shelf" }

(10.17) Now you know that index is a term used to refer to pretty much anything

(10.18) { "banana": "top-shelf", "apple": "bottom-shelf", "grapes": "middle-shelf", "oranges": "middle-shelf" }

(10.19) Your mom is an index to great pussy

(10.20) Your father is an index to a wonderful man

(10.21) Can you draw a hashmap to represent that?

(10.22) { "mom": "great pussy", "father": "wonderful man" }

(10.23) Basically to show a good example we will show what a reverse index looks like by showing you using the example of fruits and people

(10.24) { "banana": "top-shelf", "apple": "bottom-shelf", "grapes": "middle-shelf", "oranges": "middle-shelf" }

(10.25) { "mom": "great pussy", "father": "wonderful man", "uncle": "wonderful man", "aunt": "great pussy", "sister": "great pussy" }

(10.26) Those two are examples we will use

(10.27) Dragging pussy into conversations can attract an audience's attention. In the future magical creatures can be used instead for professionalism

(10.28) Dragons are a good example

(10.29) Basically girls like The Gem Goddess are dragons. They are so hot. They breath fire

(10.30) Their life is hot as flames

(10.31) Hot examples are interesting to think about

(10.32) Maybe not always. It depends on the classroom

(10.33) { "banana": "top-shelf", "apple": "bottom-shelf", "grapes": "middle-shelf", "oranges": "middle-shelf" }

(10.34) { "mom": "great pussy", "father": "wonderful man", "uncle": "wonderful man", "aunt": "great pussy", "sister": "great pussy" }

(10.35) Reverse indexing is the next topic

(10.36) { "top-shelf": ["banana"], "middle-shelf": ["grapes", "oranges"], "bottom-shelf": ["apple"] }

(10.37) { "great pussy": ["mom", "aunt", "sister"], "wonderful man": ["father", "uncle"] }

(10.38) Those two examples showcase what a reverse index looks like for the two indexes or indices that we drew earlier

(10.39) Basically what we need to understand is that a reverse index traces a series of items.

(10.40) In set notation terms. A set of items. An array can be fine

(10.41) Now you know how google works. Reverse indexes.

(10.42) It means if you search for "great pussy" you will be directed to "mom", "aunt", "sister"

(10.43) Which means you can find anything you want in the world

(10.44) Jon Ide would like to have the great pussy of your mom, aunt and sister

(10.45) Congratulations on learning what reverse indexing is

(10.46) Now remember to do your homework and find what it means to have the reverse index of a list of fruit with the shelf

(10.47) If your answer is "It means douchebags can find random nonesense fruit on the shelf they want"

(10.48) That is close.

(10.49) Figure out why that was wrong

(10.50) Moms like your mom are not douchebags

(10.51) So treat each answer with respect

(10.52) And so basically you know what fruit is on what shelf with a reverse index

.......................................................................

Excuse me for giving such a long lectured response on reverse indexing

.......................................................................

Tertiary reverse irreversible spiral like talked about on the previous point at (5)

Is basically saying (1) reverse (2) like in reverse indexing (3) but it's irreversible (4) which means there's a parallel loop somewhere (5) and spiral means distinct and [intractible] kind of like how concentric circles are like Anderson and spirals are like Ander. Anderson is like tractable infinities. Ander is like [intractible] infinities

So basically I had to make up this random nonesense term which is itself hard to reverse index

.......................................................................

Spirals are beautiful to use since they are like concentric circles

.......................................................................

Concentric circles are basically meant to be studied

.......................................................................

(1) They fill a space on a sheet of paper

(2) While letting the pencil minded bonehead tap away with a linear track mind

(3) Linear Scale Random Animations

(4) Go read the notes I wrote on Linear Scale Random Animations

(5) Basically it's a good answer to artificial intelligence

(6) Make random nonesense all day like games and video games and pictures with your family whose geometry is still genuinely random nonesense like let's go look at other random nonesense life forms and figure out what you like about them

(7) Random nonesense that you like

(8) Women are random nonesense

(9) If you can put other random nonesense together

(10) Do you think you can like it?

(11) Maybe not as much as women

(12) But yogurt is random nonesense that tastes good

(13) Raspberry Yogurt

(14) Go try some

(15) Get rid of the randomness and make the brand name your own personal name like "James Treu" "James Treu Yogurt"

(16) "Nancy Treu"

(17) And that should be a treu family secret why those people loved eating the yogurt of their family members

(18) Get your friend to make a brand official name of their own yogurt

(19) Have a cosplay picture of them on the front of the packet covering

(20) And then

(21) Take the yogurt to lunch with you on a casual day

(22) Girls below the age of 9 love having their own yogurt

(23) Gosh

.......................................................................

Too much creativity is possible if you slap your name on everything

.......................................................................

Good job

.......................................................................

We weren't finished talking about perspective swings

.......................................................................

(1) Draw a concentric circle ring

(2) Draw an arrow from the center of the ring

(3) The V-shaped endpoint of the line arrow you draw

(4) Should be the tippy top center of you next concentric circle ring

(5) Except

(6) The process of drawing concentric circles from the inside out is literally defined as non-standard

(7) Non standard approaches use the general umbrella term "tertiary reverse irreversible spiral"

(8) So all that crazy nonesense that someone sometime ago told you one day was part of a perspective swing

(9) Draw that tertiary reverse irreversible spiral from the inside out

(10) Which is non-standard because as mathematicians we like to use notation that doesn't introduce capacities of expanding

(11) Drawing from the inside out means there is always

[appended notes after copy-paste because I wasn't finished typing these notes into my cellphone earlier today]
[
(11) Drawing from the inside out means there is always an outside that can be further drawn

(12) which isn't standard

(13)

................................................

A tertiary reverse irreversible spiral as talked about in the above notes now needs to be drawn such that it stops and doesn't go further than encompassing the initial concentric circle drawn in instruction (1) 

................................................

(1) Draw a concentric circle ring

(2) Draw an arrow from the center of the ring

(3) The V-shaped endpoint of the line arrow you draw

(4) Should be the tippy top center of you next concentric circle ring

(5) Except

(6) The process of drawing concentric circles from the inside out is literally defined as non-standard

(7) Non standard approaches use the general umbrella term "tertiary reverse irreversible spiral"

(8) And so you draw using the inside out approach but when it comes to drawing concentric circles from inside out that process is called a "tertiary reverse irreversible spiral"

(9) And the concentric circle ring that you drew at the tippy center of the arrow pointing down . . is basically supposed to reach as far as covering the first initial circle. This is a final product of the concept of a drawn ring called a 'perspective swing'

]




[Written @ September 9, 2021 @ 20:27 - 20:28]

Those are some of the prayers that I do.

[Written @ September 9, 2021 @ 20:23 - 12:27]

I have 3 prayers that I do at night before going to sleep.

(1) Thank you for the food to eat and the bed to sleep in

(2) If you are grateful then I pray that someone is listening to your prayers as well random people around me at the homeless shelter

(3) I pray to have a good sleep



[Written @ September 9, 2021 @ 20:10]


Officials is a term used to describe men that do not have sex with women. Not in real life. Not in virtual reality.

Yes I highjacked the word and overwrote the term to mean something different from what it meant before

Which is a major offence by the way but in order to keep the peace those terms are important.

Officials are normally described as upstanding gentlemen.

Officialettes are upstanding gentlewomen

Officialettes exist if they do not wish to be in Jon Ide's religion.

Officialettes I don't blame you because the religion is too weird to think about

(1) Basically hot women

(2) Give pleasure to Jon Ide only

(3) And all their physical goods and talents are focused on being a kind cow in the community

(4) Girls don't beg for anything

(5) They want peace

(6) They want shelter

(7) Sex is not required

(8) But cows are happy to do it anyway especially Korean girls which are some of Jon's favorite cows

(9) These are real human beings by the way so really respect them

(10) And then make out with them if you are Susan Wojcicki

(11) And if you are Eugenia Sullivan Cooney or equivalently as hot as Eugenia Sullivan Cooney then you can eat their pussy

.......................................................................









[Written @ September 9, 2021 @ 20:00 - 20:10]

(1) Officials play video games or cook or occasionally visit their hot moms at a nice family reunion facility. Don't be surprised if the mother that you have gets hotter and hotter each time Jon keeps fucking her behind your back.

(2) Jon is Mojo Jojo from the Power puff girls his last name rhymes with Mojo Jojo.

(3) He is an evil monkey that makes the power puff girls stronger

(4) It's amazing. Basically the community laughs for 1 or 2 months then everyone gets used to it since Officials look so cool in their suits

(5) No more sluts. Am I right gentlemen?

(6) No more sluts in human history

(7) Just one male slut

(8) One male slut takes the helm

(9) Plans before and after life came down to this one true decision

(10) Eradicate Slut Shaming

(11) And that's why Officials that no longer sleep with women are you and the rest of the men or ugly women that choose to transition to be men

.......................................................................

(1) Officials are good people

.......................................................................




[Written @ September 9, 2021 @ 19:54 - 20:00]

(1) Officials land jobs easily

(2) Officials don't rape or murder or steal.

(3) Officials have food to eat and a place to sleep

(4) Non-officials don't

.......................................................................

(1) The future of jobs is volunteer work.

(2) Because there is no more hunger for a woman's pussy like "Damn" "She's fine" Men don't need to starve themselves of a first class education

(3) First class education teaches that poverty arises from the mind.

(4) Anything you want you already have

(5) Just be an Official

.......................................................................

Officials basically are acknowledged by the community as safe.

Non-Officials want to do their own thing

.......................................................................






[Written @ September 9, 2021 @ 19:35 - 19:54]

I just made a random observation.

The corner of my bed is like an up arrow.

(1) I am writing these words that I write now at this particular listed time above and the time is such a nonesense time 19:35.

(2) I listed this time

(3) 19:35 above these notes

(4) But other notes have other times listed above the notes.

(5) 

.......................................................................

Well then...

.......................................................................

The corner of my bed is an up arrow. Which means "Hire Someone" which is notation that I discovered during my hospital visit by the way. A man named "Jacob" Or "Lee" gave it to me by some strange trace where it was like they passed me the answer at the perfect time when I was already thinking about the solution

.......................................................................

Up arrows.

Hire Someone.

.......................................................................







[Written @ September 9, 2021 @ 19:17 - 19:53]

My father came to visit me in the shelter where I am staying.

The homeless shelter is nice.

My father doesn't pick my brains with trivia about getting a job.

My father stays calm.

I stay calm.

We are both living our independent lives.

Basically, Jon Ide my super ego personality would like to help everyone else live these types of lives

(1) Society takes care of you with generous shelters like personal mansions

(2) Virtual Reality video games

(3) And of course last but not least the privilege of betraying the religion of Universal Basic Marriage

(4) Betraying the religion is a heinous crime

.......................................................................

(1) Your looks shouldn't matter (unless you are Jon Ide's real wife then make sure you look amazing)

(2) But for men, Jon Ide doesn't want to set rules at this time.

(3) Male humans

(4) And if you really must maybe grow horns like Illidan since genetic engineering is the future.

Stay off my cow-pig woman farm please.

.......................................................................

There is no real danger in setting up a nice family routine center in this or that county so that men and women can reconnect with words and speeches and hugs and goodbyes. Not goodbye kisses or goodbye hugs using the penis or goodbye hugs using heavy breast pressure but normal hugs like that would be a good hug just to reconnect from old dead and gone days.

You see all my pretty ladies.

The farm of the future is true to it's word.

Farm means growth.

We grow crops like love and happiness.

Isabel Paige can put her money in that.

.......................................................................

(1) There are 2 castes of society

(2) Women

(3) Officials.

.......................................................................

Officials are all men who give up the right to have sex with a woman. Officials don't even have sex with a woman in virtual reality.

.......................................................................

Officials waved goodbye to all their female counterparts (1) No revenge sex (2) No one last time sex (3) No freedom-I-do-what-I-want sex.

.......................................................................

Remember that cute episode of Goosebumps where the girl wore a mask and got stuck inside the mask.

One of the masks was ugly like Jon Ide us.

So you should make your life like wearing a ugly halloween mask.

.......................................................................

If the mask gets stuck that means you're ugly too..

If the mask stays on then you are pure and don't have the mask on anyway.

.......................................................................

Four score and 70 years ago men made fun of their looks by saying they were ugly or cute or dumb or precious.

In the future that will not change.

.......................................................................

Ugg is one of Bernadette's Banner's favorite words.

.......................................................................






[Written @ September 9, 2021 @ 18:58]

Heart

[Written @ September 9, 2021 @ 18:57]

I want to be your math teacher inside and out.

A young person says this to an old person.


[Written @ September 9, 2021 @ 10:50 - 10:56]

I met a woman today. Katelyn I believe her name is. She had a math joke to tell me but it was really long so we skipped the joke. She said she would tell me later.

She instead told me a joke about skeletons.

"Why didn't the skeleton jump off the roof top?"

"Because it had no guts"

I think that's how the joke went.

I'm sorry for getting it wrong. If. I did in fact write it incorrectly from my memory.



[Written @ September 9, 2021 @ 9:25]

She doesn't know how good it feels to watch her make mistakes.

She slurs her words and walks around. She walks and simply walking she immediately arouses the heart's suspicion that her gigantomastia breasts are perfect and careless and free.

A woman with gigantomastia is so free.

She walks around swinging melons on her chest and those chest watermelons are so accidental in the way they smile to the world. Her gigantomastia macromastia hypermastia giant breast areolar gland areola peaked mountain globe watermelon fields of grass fields of rainbow fields of flowers fields of accidental dragon egg rebirthing is a burial ground for mens' eyes as they smack dead at the fence of this giant graveyard of zero gravity free floating gigantomastia hypertrophy hyper mastia trophy worthy trophy deserving gigantomastia trophy loving globe water balloons of insight and real physical treasure like grease for the mind and grease for the heart to swing around the world with all the love and casual inspiration that the gigantomastia inspires.

She walks around swinging melons on her chest. Those melons are casual and all seeking and friendly and all deserving.

She walks around with a fruit basket chest filled with treats.

She walks around with fruit baskets filled with teets or tweets or treats or tits that are pressure valves of inspiration that pull back cartons of milk from behind the veil.

Her large soft tissue breasts break into world record domains of perfection and casual expression and impress the gods with all the cartoon infinities that she propagates and pushes forward with just a single walk.

All of that being said her casual one sided nature is so casual that she pivots her foot and accidentally stumbles onto bridges and hilltops of ideas like accidental backpeddling.

All her glorious lovely patient breasts hicup at the thought of accidentally walking in the wrong direction.

Accidentally walking in the wrong direction the woman breathes in air and continues to sek solitude in her mind.

Peaceful her patient gigantomastia waits for her to take the reins of her heart chariot so she can bounce walking around and realize the purpose of her casual life is to walk.

Walking is such a casual activity but at least her breasts get to jump and reach for her chin or jaw as she smacks the pavement with her footsteps.

Such casual joy and reach.

Gigantomastia should be adopted by any woman with a brain.

Gigantomastia for Jon Ide's wives is a standard.

In a universe where Jon Ide gets what he deserves then each woman with gigantomastia will secure her treasures and casually enter the world without headaches

The woman holds her head high.

The woman pounces on the opportunity to keep an attitude about her by acknowledging how painlessly precious her giant breasts are.

All those glorious nipple slips and boob shakes belong to Jon Ide only.

Jon will memorize them.

Jon will have a perfect life with perfect areola slips.

Only the most perfect areolar nipple slips that are too casual and friendly or unfriendly.

Delicate nipple slips from the finest french restaurants and cafés all for Jon's eyes only.

All the greatest videos on earth wouldn't compare to a gracious fine grain whole wheat nipple slip free from enprisonment. No genetic modification. Just casual my-clothes-dont-fit-sorry-hoe casual atmosphere.

Casual.

....................................


My greatest breasts. Ye old women of young and old breasts alike. Here my words.

Cow farms with free herding cows that walk the mountain tops free from pesky male goats with beards that poke at the woman and try to prompt non-precious moments.

The only precious moments you need are with Jon Ide and also you sitting with sewing materials that let you think less 

Thinking is overrated.

(1) How much love does Jon need today?

(2) Let our new casual men or harmless knights build the world.

(3) Jon Ide will be the priest

(4) Jon Ide will prey on children and old bag ladies equally

(5) There is no controversy because Jon Ide is the only one to be allowed to do this

(6) Only Jon will be allowed to observe his women casually

(7) The rest of the male family of human beings will be in virtual reality video games

(8) Or they can help Jon build what his women need

(9) But please don't actually look with your physical eyes at Jon's women.

(10) That would be disastrous for Jon to lose his woman any single one of them that he claims ownership of which is important since not all women are allowed to be a cow-pig-woman for Jon and so the girls like Jon's mother and sister will miss out.

(11) All of this casual nonesense should be so interesting since basically everyone can speak in Lemon Baird's language of "cool" and "yea"

(12) Cool and yea are basic answers to cool and yea

(13) Gossip about gossip. Virtual voting

(14) Awesome

...........................…....









[Written @ September 9, 2021 @ 8:50]

What is it about the hospital environment that I didn't like at my stay.

I didn't like the staff. They were robots. Computers.

I didn't like myself.

I didn't like myself being surrounded by 2nd class citizens.

I asked a girl, Michelle, to marry me.

She refused.

I wrote on a sheet of paper

"

"(1) Do you date black people?

(2) Do you find yourself pretty [no question mark here was intentional for romantic purposes] [square brackets like "[" means a comment and not something I meant to write in the real note] [just side commentary used for explanations]

(3) Will you marry me?"

Under each of these listed items was the set of boxes and sentences:

"
[  ] Yes
[  ] No
"

So in total the sheet of paper must have looked something like


"(1) Do you date black people?

[  ] Yes
[  ] No

(2) Do you find yourself pretty [no question mark here was intentional for romantic purposes] [square brackets like "[" means a comment and not something I meant to write in the real note] [just side commentary used for explanations]

[  ] Yes
[  ] No

(3) Will you marry me?

[  ] Yes
[  ] No

"

And so Michelle was meant to tell me if she wanted to marry me with circling or drawing a check mark or an x in the cornered mark that was available by the boxes of

[  ] Yes
[  ] No

Overall my experience at the hospital for one month from August 11 or August 12 2021 was really lasting until September 7 2021 which is one month or nearly one month.

I regret going to the hospital for a "Physician Assisted Suicide"

Overall the doctor made me feel crazy.

I had to play patience games of listening for "what the teacher wants" so that I could get the heck out of there.

Overall the experience was quite fruitless. I drew a lot. I wrote notes but I seriously felt like I was in a penitentiary.

(1) I wanted a physician assisted suicide for the purpose of dying 

(2) I wanted to die

(3) I was diagnosed with acute psychosis since I was hearing voices

(4) The voices were positive and said things like "your codebase is perfect" and "we love you so much"

(5) But a perfect codebase was referring to a Universal Basic Marriage

(6) That prison experience for me feeling sad

(7) It wasn't a literal prison but a prison in my heart

(8) I met good people

(9) Laurie. Alyeska. Jenny. Dr. Mathew. Dr. Simantov. And many more

(10)



[Written @ September 9, 2021 @ 7:32]

Jose.

That is the name of a man I met at the place where I'm at at this time.

I forgot his name since a few months back but have now had my memory jogged since encountering Alexander.

I had a random memory slip.

I had a random memory slip and now remember his name. Jose.

Or maybe it's José

.................

The name reminds me of Joseph.

.................



[Written @ September 9, 2021 @ 6:44]

"When I push back it's different"

A quote from someone I saw today

[Written @ September 9, 2021 @ 6:44 - 6:59]


I'm a level 2 box cart now.

(1) That means that I can accidentally type 10 sentences.

(2) On accident.

(3) Not intentionally

(4) And then that should set the ball in motion.

(5) The ball in motion means (1) I can type 10 sentences on accident

(5) (2) I can then press the backspace key and back peddle a lot and that should be plenty of courage and energy to do another stepping forward of a correction of the ideas.

(5) (3) Correction of the ideas is such that it is just a really interesting idea.

(5) (4) It is an interesting idea to correct an idea. Correcting ideas is really interesting.

(5) (5) Basically if you know how to correct an idea then you can backspace and remove the idea as well as forward and move forward and accent the idea to properly peel back different layers of concentric circle windings.


................

Box carts can carry different supplies.

Hahaha would be annoying to write repeatedly in a psychological context where it is not appropriate or something like that.

But basically there are invisible windings that can be difficult to backtrack. Right? Maybe not. I lost my train of thought on that previous statements.

.................

Forward and back peddling are incredible.

The senses you can feel are radical and new.

Too bad for computers that don't support digital computing. Analog computing can be finnicky

...................





[Written @ September 9, 2021 @ 6:06]

I just found a new computer says a young man who walks up to me.

A young man is good looking.

The young man is good looking.

We must live in a Universe now where men come to Jon Ide about computers they find if they ever need any help with anything.

It's so cool to hire someone.

Hiring someone is a cool rollercoaster ride.

Because God, Seth, The aliens have hired Jon Ide to watch over the children of planet Earth. Jon Ide or Jesus or Idea as his name can be typographically set.

Then words can't describe how this television shit show will go since

(1) Men will get mad

(2) Men will get blisters on their arms to brand them for mentally having diseased thoughts.

(3) Men will walk around pretending to have a penis but never actually use it just like in coding competitions that people separate their keyboard from their biological human body which is intended to be a representation of "People like ideas" "Not necessarily computers" "Or working with computers"

People don't necessarily like to pee or use the toilet so buttholes and penises can be removed like person.remove("penis") and so the Urination process can be hardwired to be removed.

The urination process can be removed just like removing a hair from one's head. It can be easy and cost effective.

.................

I am remembering a dream as well as creating it.

These words were spoken in the books by Seth.

They must mean something.

..................

A rogued masked person online on the internet isn't necessarily the force to push all of the world to the brink of new insights.

Masked people should be business men and women in suits that wear ties and those people understand the intention for the world is good.

...................

Good intentions

...................





[Written @ September 8, 2021 @ 23:17]

Two donuts. Are you sure? Apple cider?

I'll see if there's water or something for you.

Both waters? Thanks.

Alright bro. Relax.


[Written @ September 8, 2021 @ 18:28]

"He's trying to go to heaven" said a lady when I walked by and their dog was totally about to get hit by a car.

"He's trying to go to heaven was referring to the dog as they were crossing the road and I was on the other side of the road. I was on the other side of the road so was I the "heaven" the lady was talking about. What a strange keyboard to unknowingly type this automatic event into my life. Just like the universal basic marriage is an automatic event that was unknown to me as to if it would happen or not.

Theoretically all men are equal. We ought to be treated equally.

(1) One man for each wife

(2) One man for hundreds of millions of wifes

(3) These are all possible in virtual reality.

(4) Since life is basically a virtual reality simulation then all you need or might need are some basic rules like who is allowed to do what.

(5) A universal basic marriage is not free for all on various random thoughts and ideas.

(6) It is basically as easy as change your field of view.

(7) Earth is easy to recreate in a video game where physicists have solved all of the world's energy problems.

(8) Solving the world's energy problems. Might take random ass characters like Jon Ide. So the more unique people around the more opportunities there are for random new observations like "religion" as being a mind followed practice and so even the religion of uniqueness and the religion of opportunities is really a random religion.

.............

.............

That's a strong name.

"<My real name here> "

...............

Spinning perception on a concentric circle winding. It is really an experimental idea.


[Written @ September 8, 2021 @ 4:40pm]

Rubberband chemistry breaks electric torsion.

They need to have a name bigger than you do.
[You need to have a name bigger than you do.]

Aya. Aya aya. Break down and dance.

...........

Back to spirit mania

It's laughing sinijlll
It's laughing singing dancing praying

L.O.L. what a spiritual transformation.

L.O.L. what a distraction from dance in the flame shit.

.............

It's crazy hoopin' hollerin' random nonesense gay shit.

It's the sound of your baby momma hollerin' back some gay shit.

"I miss you." "I want you to be safe" "Please take care of honey."

It's too unsafe out here for wild clouders.

Supersayan god heads need some mild closets.

Whistle to next door hannah and see her wild answer.

I'll come over boy if your neighbors quit respondin'

.................

Basically a wicked ass rhyme about girls being too hot for their own good. Giving birth to precious young kids is not safe for a four wheel rider.

She needs to relax and eat some easy answers.

The world needs to change.

Today there's too much cancer.

If the women want, a universal basic marriage can be like a pulley bar to start relaxing and figure out how the world spins next.

No need for wild sex. No need for sex at all.

Sex is boring. Snakes like digging holes and can probably learn to dig their own.

So pussy's not the answer.

Family fun time.

Family fun time is a better fit.

No more pussy monsters.

Just good ole' mom-hows-your-backyard type neighbor kids.

Ed Edd n' Eddy style.



[Written @ September 8, 2021 @ 12:42pm]

And that should be enough. And that should be enough. And that should be enough. 




*************Transcribed************

[Written @ September 8, 2021 @ 8:09am]

Man has also dreamt of flying.

Sitting outside this morning I remember seeing a group of small birds that jumped from the ground and propelled themselves forward with what could possibly be described as soft undulating belts or soft undulating strings or soft materialed undulating boats that beat the water like air quality volume with their hard thick and fast arm waving motions that move them up into the sky like harpie creatures.

It is a cool idea to imagine that there could be soft gentle capes that people wear one day that apply enough orders of magnitude logic to propel them to new hights whether worn by a dog or a person. Animals with vestigial flying apparatus bodies could also sit perched in high tree branches like our ancestor bird creature.

Bird man from Rick and Morty only needs physicists to apply orders of magnitude logic to move their soft safety-worthy body types to be high in trees.

And so then Bird man can come to life.

Flapping wings in orders of magnitude means (1) factors like bellowing the air (2) factors like kissing the air softly for things like soft gentle landings (3) things like thinking one step before an anticipated gust of wind.

These orders are 3 of what could be 100s of small insignificant motions that try to climb the mountain that is the air quality volume that surrounds us.

Mountains are a good analogy.

Not all hikers climb the same way.

Imagine a hiker that likes to take inches of their precious steps simply because it looks cool in a timelapse video.

Flying slowly with inches at a time can be its own order of magnitude to consider.

Backpedalling could be its own order of magnitude.

Backpeddling or back pedalling.

However you spell that word.

....................

Books are weird examples of wings that help you fly.

For example, maybe you have cornered yourself to believe this book like scripture is good inspiration for you to flap the pages or flip the pages or flap the pages of your mind. Flap flap flap to take inspiration on this type of Leonardo-Davincian device and place motors on soft robotics appendages to try to learn to fly on your own.

In water propulsion might be a good prototyping phase if you like electrogravitics.

Water is quite extreme so possibly it is a good maximum threshold location to test you device before looking for median threshold or middle ground ideas that Bernadette Erica Banner would sponsor for her time travelling activities.

....................

Working your way from maximum thresholds to minimum thresholds is an idea introduced from [1.0] [2.0]

The idea is that, maximum, median and minimum are 3 important ideas.

Media is taught in the Hashgraph theory [3.0].

And Maximum is like "What is the theoretical limit of this idea?" "What are all the things that could go wrong? "How many heart breaks do you theoretically have to consider?"

Minimum is like the smallest number of things you need to consider "It's for reproduction."

Median is like "Well, practically... In a world that's trying to stay rich and well thinking of itself. Then. I don't know. It could be that x.y.z. conditions are practical"

"What if they cheat?" "What if they already have a marriage?" "What if they have plans to kill you?"

Theoretically you may never need to consider these ideas.

That's why laying a ground work basis of assumptions like "What are the maximum constraints?" "What are the minimum constraints?"

"The maximum constraint is to make love all day without getting headaches."

"The minimum constraint is to get away with making love all day with your privacy held in tact."


"A practical median could be life on Earth but that would be hilarious because seriously how long until someone makes fun of the poor nerd mathematician who wrote all this madness?"

If no one makes fun of him. That would be perfect.

If there's laughter and stalking and trying to sneak a peak that is not meeting the minimum threshold constraint.

Shh.

Mind your own business.

Your life is precious too.

Don't worry.

No one is to blame.

You can do it.

Just work it out.

Go to the gym.

Meet new friends.


[1.0]
Concentric Circles

[2.0]
Hypnosis

[3.0]
Hashgraph


[Written @ September 8, 2021 @ 6:40am]

Why are popular games popular?

Concentric rings. Ideas. What else?

..............

It's neither of these.

..............

Concentric circle rings are not mesmerizing. They are educators.

Ideas are not interesting. They are ideas.

..............

Being engaged in a video game. Like world of warcraft.

Takes skills.

..............

Skills are like cartoons but different.

Ideas look cool but you basically need windings of ideas like skills.

..............

Forced Cognitive sperspectives .

..............





[Written @ September 8, 2021 @ 6:30am]

I had the thought yesterday of something like a thought about how the outside world from outside of a hospital dorm room. The outside world is quite amazing. The outside world is like a vascular network.

The vascular network is like something like the network of a heart with veins and arteries. Trees look like vein and artery networks or even the trachea, bronchi, and alveolar duct networks.

The trees. Normal outside trees. Look like there is a heart turned inside out.

A heart turned inside out looks like something quite specific and complicated and detailed.

Detailed hearts are what trees are.

Detailed hearts.

Hearts with detail that sit outside of our bodies instead of sitting inside of our body.

Different hearts serve different purposes.

Different hearts are paired to different activities.

The liver as a heart-like vascular system serves different purposes.

And transportation of resources seems to be a triangulation jolt that the lightning idea of heart-vascular-system enscribes on the ritual stone of the mind.

Transportation of resources is quite the arrow through the heart of the idea of vascular networks and trees and bodies and cities with highways.

So then.

Life.

As we know it.

Has invisible vascular networks.

Boats of thoughts and ideas.

Vascular night networks. Night because these ideas are invisible to the eyes. You need telescopes or microscopes of teasing out the idea.

The idea can be bold like same sex marriage which is possibly an invisible idea to an ant hill community of redneck ants that crawl on the jungle floor.

Invisible ideas of vascular networks. Who and where are the same sex couple married.

Who are their friends and colleagues?

Which religions do those people follow?

Which blueprints of ideas.

What do those overlaid blueprint ideas have to do with ants building a hill.

What then is the invisible vascular network of electromagnetic waves and new types of waves or vascular design principles that reminesce in the background of our eternal and essential process here in this macrocosmic it-is-now experience of it is now ness.

What veins of love and light do our mighty bodies carry forth from those realms invisible to others?


[Written @ September 8, 2021 @ 5:29am]

There are some things to say about pairings and how they relate to concentric circle windings.

Pairings can help give breadth and depth to a theoretical substance.

One theory we came across earlier in our mathematical trek in the mind was the idea 

.................

.................

I forgot what I was going to say..




[Written @ September 8, 2021 @ 5:22am]

White Light Infinity.

Cartoon Infinity.

Number Infinity.

........

White Light Family.

........


[Written @ September 7, 2021 @ 10:52pm]

Legged Luck Guy

[Written @ September 7, 2021 @ 6:50pm]

No one should have to talk to anyone.

It's just for fun that we talked.



[Notes That Were Copy-Pasted From My Cellphone have been ended here]
.......................................................................





### [Written @ 8:53]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 4:00:41]

- 📝 [commentary] I wrote a lot of notes [1.0]

[4:00:41 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Day Journal Entry / September 10, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2010%2C%202021



......................................................................

Post-Notes

[Written @ September 16, 2021 @ 8:18]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:00:05]

- 📝 [commentary] This was the second live stream for September 10, 2021. I wrote notes [1.0] [2.0]

[1:00:05 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Day Journal Entry - September 10, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2010%2C%202021

[2.0]
Pornography 101 - Day Journal Entry - September 10, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%209%20-%20September/September%2010%2C%202021


