
# Day Journal Entry - September 18, 2021

### [Written @ 13:02]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:44:19]

- 🚧 [work-progress] I updated the design for Ecoin's Your Account Home Page [1.0]

- 💡 [initialized] I added the design for Ecoin's Transaction Popup item [2.0]

[2:44:19 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin / Design / Home Page - Your Account
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/Journal%20On%20Things%20About%20Design/Ecoin/Design%20For%20Pages/Home%20Page%20-%20Your%20Account

[2.0]
Ecoin / Design / Popup Item
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/Journal%20On%20Things%20About%20Design/Ecoin/Design%20For%20Pages/Popup%20Item

