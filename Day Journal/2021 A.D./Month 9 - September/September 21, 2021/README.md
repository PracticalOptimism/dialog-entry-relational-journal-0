
# Day Journal Entry - September 21, 2021


### [Written @ 8:56]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:41:58]

- 📝 [commentary] I didn't do a lot . Mostly copy-pasting what's from Ecoin 1.0 to Ecoin 2.0 . The difference is that Ecoin 2.0 uses a so called 'listless' archiecture which seems to possibly be the new 'the light architecture' . It's a complicated topic. Earlier videos talk about it. And maybe I'll go over it again in future videos.

[2:41:58 - 3:26:24]

- 😴 [break-time] I went to eat lunch.

[3:26:24 - 4:33:23]

- 📝 [commentary] I published the very first version of https://ecoin369.web.app

[4:33:23 - 6:07:39]

- 📝 [commentary] I didn't do a lot.

[6:07:39 - End of Video]

- 📝 [commentary] Salutations

References:


