

# Day Journal Entry - July 22, 2021

[Written @ 12:41]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:36:07]

- 📝 [commentary] I wrotes some random thoughts on strings and infinite magnifying glasses [1.0]

[2:36:07 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal - Technological Ideas / July 22, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas/Random%20Thoughts%20and%20Ideas/2021%20A.D./Month%207%20-%20July/July%2022%2C%202021


