

# Day Journal Entry - July 7, 2021


[Written @ 11:16]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:12:13] (10 minutes)

- 📝 [commentary] I talked about Web Servers [1.0]

[0:12:13 - 0:51:32] (40 minutes)

- 🚧 [work-progress] I fixed errors for one of the Ecoin 2.0 files [2.0]

[0:51:32 - 1:11:24] (20 minutes)

- 😴 [break-time] I took a break

[1:11:24 - 2:17:46] (60 minutes)

- 🚧 [work-progress] I fixed errors from one of the Ecoin 2.0 files [2.0]

[2:17:46 - 3:17:08] (60 minutes) 

- 😴 [break-time] I took a break

[3:17:08 - 4:49:38] (90 minutes)

- ✅ [completed] I created an initial Node.js build for the `did-you-do-that-1-original-codebase.ts` file [2.0]

[4:49:38 - 5:49:54] (60 minutes) 

- 😴 [break-time] I took a break

[5:49:54 - 6:50:53] (60 minutes)

- ✅ [completed] I created an initial Web Browser build for the `did-you-do-that-1-original-codebase.ts` file [2.0]

[6:50:53 - 8:06:45] (70 minutes) 

- 😴 [break-time] I took a break

[8:06:45 - End of Video] 

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / July 7, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%207%20-%20July/July%207%2C%202021

[2.0]
Ecoin / July 7, 2021 Commits
By E Corp
https://gitlab.com/ecorp-org/ecoin





[Written @ 10:55]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:00:00]

- 📝 [commentary] I didn't do anything

[1:00:00 - End of Video] 

- 📝 [commentary] Salutations


