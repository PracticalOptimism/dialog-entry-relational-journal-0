

# Day Journal Entry - July 10, 2021

[Written @ 19:08]

You might get a better understanding of javascript if you take this course [2.0]

(1) Itt's difficult to type a thought
(2) And also not vocalize it in your mind
(3) If you try to delay the letters and sounds
(4) You'll g-You'll g-You'll get weird appara-apparitions o-o-or-or-apparitions 
(5) It's kind of fun
(6) To try to type
(7) A sentence
(8) A long sentence is difficult
(9) Without
(10) Making a sound
(11) If you succeed
(12) That is really hilarious
(13)

(1) Another game could be
(2) Make another sound
(3) Like say the sentence in reverse
(4) Or say another long sentence
(5) While typing your original sentence
(6) But that could steal your faith
(7) Associating verbs with words 
(8) Walking and walking and talking and talking
(9) Talking and talking and talking
(10) Eating and eating
(11) Eating and eating

(1) You are cheating if you say the word that you're tying
(1) You are cheating if you say the word that you have yet to type
(1) You are cheating if you say the next word
(1) You are not cheating if you say the word that is 1 or 2 words ago
(1) You are not cheating if you say the word that is long ago past

(1) You can restart the sentence
(1) If you catch yourself cheating

(1) One smooth trick you could try is to (1) delay the word that you are typing . . or pronouncing in your mind . . and so . . (1) difficult . . becomes . . diiiiiiiiiiiiiiiiiiiifffffffffffffffffffffffffffiiiiiiiiiiiiii----- (and you never have to finish it)
(1) If you delay the word long enough then maybe you didn't even say it at all right?


[Written @ 17:45]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:40:19]

- 📝 [commentary] I published the Ecoin 2.0 project to NPM and the real version number is ecoin@0.0.6. I may change this to 2.0 in the future or 2.0.0 to be more specific to try to follow the 'semantic versioning' rules [1.0]

[0:40:19 - 1:35:46]

- 📝 [commentary] I created a simple web server example using Express.js and Node.js and started the web server using Node.js. You might get a better understanding of javascript if you take this course: [2.0]

[1:35:46 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By E Corp
https://www.npmjs.com/package/@ecorp-org/ecoin

[2.0]
JavaScript: Understanding the Weird Parts of JavaScript
By Anthony Alicea
https://www.udemy.com/course/understand-javascript/


