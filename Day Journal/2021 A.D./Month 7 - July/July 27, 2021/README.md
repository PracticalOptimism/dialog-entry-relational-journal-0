

# Day Journal Entry - July 27, 2021


[Written @ 17:13]


Key Features:

(1) Distributed Computing
(2) Universal Basic Income
(3) Open Source
(4) Anonymized Identity Support


....

Key Features:

(1) Distributed Computing

- The Practice: Uses technologies like IPFS, Dat and Braid Protocol.
- The Principle: A peer-to-peer network distributes control to the network rather than a few monopolies.
- Your Mission: Learn more about decentralized technologies like IPFS, Dat and Hashgraph.

(2) Universal Basic Income

- The Practice: Everyone receives a payment every 60 seconds. 
- The Principle: We are all creative.
- Your Mission: Dream big, dream small. Afford to live through it all.

(3) Open Source

- The Practice: Anyone can rebrand the code base and start their own currency.
- The Princple: Let’s create a world we each individually love.
- Your Mission: Modify the code to your heart’s content.

(4) Anonymized Identity Support

- The Practice: Anyone can have an account.
- The Principle: We are all citizens of a global community.
- Your Mission: Know you belong to a global ecosystem. Live in the future without borders.

[Written @ 9:53]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:23:05]

- 📝 [commentary] I wrote notes about "Pornography 101" which is a theoretical alternative reality notebook course on "1" Pornographic topics "2" How to be a child and still be into pornography "3" How to be an adult and try not to involve other psychic influences into your pornographic eras "4" How to be okay even if you're dealing with a lot of sexual trafficing into your life even if Eugenia Cooney isn't around you any more since she is around other women and doing xyz with whoever on the internet online for free -_- "5" what a weird world we live in. We have social stigmas but basically they are our friend and our jailer. We have social stigmas but they are our friends and our jailers. I don't know. Those are all theories that I've written and maybe they will stand the test of time by being a social stigmas that is thought about or not. [1.0]

[1:23:05 - 3:14:25]

- 📝 [commentary] I wrote notes about 2 courses (1) Gigantomastia Theory and (2) Gigantomastia Practice. [2.0] [3.0] Both of these courses are introductory courses for college students. Gigantomastia can be studied by advanced elementary schoolers or advanced high schoolers but basically the answer is that you can learn more about gigantomastia. Gigantomastia has its benefits and its disadvantages. Benefits include horniness vibes forever. Disadvantages include having to be nervous and shy and reserved about invisible things that don't stop creeping up on you. Invisible Things are difficult to find. But for some reason. They are still around. If everyone in the community loves their gigantomastia, then maybe it's not the social atmosphere. Maybe it's not a social stigma. But maybe instead it's a social stigma just to exist. Just to experience life. L.O.L. [2.0] Gigantomastia fairy boat women transport reality to life. Life. Is Love. Love is Gigantomastia. [2.0]

[3:14:25 - 4:10:21]

- 📝 [commentary] I wrote notes on "How To Show Accidents With Pride" [4.0] I hope this course will be a primer course for (1) accidents (2) accidents (3) accidents [4.0]

[4:10:21 - 7:22:02]

- 📝 [commentary] I wrote notes on "Nipple Slip 101" which is a course for "Nipple Slips". The notes are incomplete because I have to leave the library where I'm at. I will have to come back another time and write more. [5.0]

[7:22:02 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Pornography 101 Journal / July 27, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%207%20-%20July/July%2027%2C%202021

[2.0]
Gigantomastia Theory Journal / July 27, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/2%20-%20Gigantomastia%20Theory/Day%20Journal/2021%20A.D./July%2027%2C%202021

[3.0]
Gigantomastia Practice Journal / July 27, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/3%20-%20Gigantomastia%20Practice/Day%20Journal/2021%20A.D./July%2027%2C%202021

[4.0]
How To Show Accidents With Pride - Day Journal / July 27, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/4%20-%20How%20To%20Show%20Accidents%20With%20Pride/Day%20Journal/2021%20A.D./July%2027%2C%202021

[5.0]
Nipple Slips 101 Journal / July 27, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/5%20-%20Nipple%20Slips%20101/Day%20Journal/2021%20A.D./Month%207%20-%20July/July%2027%2C%202021


...............



