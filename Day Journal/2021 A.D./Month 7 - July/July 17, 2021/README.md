

# Day Journal Entry - July 17, 2021


[Written @ 11:09]


Key Features:

(1) Distributed Computing

- The Practice: Uses technologies like IPFS, Dat and Braid Protocol.
- The Principle: A peer-to-peer network distributes control to the network rather than a few monopolies.
- Your Mission: Learn more about decentralized technologies like IPFS, Dat and Hashgraph.

(2) Universal Basic Income

- The Practice: Everyone receives a payment every 60 seconds. 
- The Principle: We are all creative.
- Your Mission: Dream big, dream small. Afford to live through it all.

(3) Open Source

- The Practice: Anyone can rebrand the code base and start their own currency.
- The Princple: Let’s create a world we each individually love.
- Your Mission: Modify the code to your heart’s content.

(4) Anonymized Identity Support

- The Practice: Anyone can have an account.
- The Principle: We are all citizens of a global community.
- Your Mission: Know you belong to a global ecosystem. Live in the future without borders.

[Written @ 10:41]



**"Free Housing"** Means (1) free housing like a physical house.

In algebraic terms it could mean something like "free shelter" or "free mind ability" or "free ability to explore" or "free open dialog solution" or "free creative powers"

**"Free Food"** Means (1) free food like physical food.

In algebraic terms it could means omething like "pussy" "vagina", "fruits" "oranges" and even other things that can be thought of as worth consuing like "books" and "animated television shows" and "movies" and "pornography" and even hateful things that you might not agree with but other people think is food like "bullying" and "fighting" and other entertainment sports like "fuck you" and "fuck your neighbor" and other nuclear armageddon type dystrophies of concern.

**"Free Cleaning Supplies"** Means (1) free cleaning supplies like physical cleaning supplies like Windex or window cleaners or even a broom and mop or even other things like toilets and toilet plungers and toilet systems and even air cleaning systems.

In algebraic terms it could mean something like "stop using the F Word Please" or "stop saying naughty things all the time" or "Please don't do those things that I have told you not to do but also here is a manual for you to follow steps to avoid doing those things in a fool-proof way"

. . 

In other algebraic terms, try to kill things for free and get away with it.

. . 

"bullets" and "guns" can be considered "free cleaning supplies" for societies that want to eliminate poor people or people of a certain ethnicity or skin color or religious background or religious denomination.

. .

"bibles" and "harmonicas" can be considered "free cleaning supplies" for trying to brain wash people that pornography and having sex all day isn't the way of the lord.

. . 

"napkins" and "spray machines" are able to clean hard wood surfaces and even remove odors from the air by capturing them in misty environments.

. . 

Cleaning supplies are not only for you and your safety like wiping your bottom of feces but also for your neighbors like "yea, thanks now I don't have to smell that stuff any more"

. . 

"Virtual Reality" is a spray cannon for your mind so you can ignore your history and go back to "yes"

. . 

**"Friends have always been "here and there"** is a weird one. Ghosts are like creatures that are "here and there" and so -_-

. . 

**"Girls like different people"** is interesting.

**"A wife partnertnerrrrrrrrrrr might commit"**

. . 

The algebraic meaning of these last 3 items are possibly obsucre and require individual and blessed attention to prescribe new meaning.

What does it mean for a "girl" to "like" "different" "people"

. . 

"What does it meenananaa" to "comemeitie"

. . 

Commitittitititititititititititititititiitititiititititiititititiitititiitititititiitititiititititiitititititiititititiititititiititititiitititititiitititititiiitititiititititiitititititiitiitititiititititiititititiititititititiititititiititititiititititititiititititititititiitititititititiititititititiitititititiititititiitititititititiitititiititititititititiitititititiiititititiititititititititititititiitititititititiitititititititiitititittiitititititt


. . 


Notes @ Random Thoughts On Random Things

An analysis of the quality of life that I have had in my own personal life.

Quality of Life:

- Free Housing
- Free Food
- Free Cleaning Supplies

- Friends have always been "here and there"

- Girls like different people

- A wife partner might commit

An analysis of the quality of life that I suppose society could have.

Quality of Life:

- Learn
- Knowledge
- Education

An analysis of the quality of life that aliens like the Pleiadians have for the people of Earth.

Quality of Life:

- 

[Written @ 9:13]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:31:49]

- 📝 [commentary] I didn't do anything . . 

[1:31:49 - 1:55:46]

- 📝 [commentary] I wrote some notes in the day journal [1.0]

[1:55:46 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Day Journal Entry / July 17, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%207%20-%20July/July%2017%2C%202021

