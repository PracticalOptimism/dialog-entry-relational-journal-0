

# Day Journal Entry - July 8, 2021

[Written @ 20:52]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:44:00]

- 😴 [break-time] - I rested and took a break I rested and took a break

[0:44:00 - 1:07:47]

- 📝 [commentary] I wrote about a random idea that I had . . "Animations" and "Robotics" working hand in hand to make robotics that are well "animated" in the real life and can "update" their "animations" [1.0] [I'm sorry the notes aren't professional but I was short of information to apply more effort. I should have possibly applied more effort. I will consider updating the post with more ideas once I've evaluated or re-evaluated certain specific or non-specific ideas . . in general one idea is related more to having a specific description for the audience as to what was meant by 'counting' but counting is something that I have taken notes on at [2.0] and so re-iterating this idea was something that made the notes 'non-negotiable' in trying not to repeat certain ideas . . also I'm lazy and not a nice person and a nice person would have been more informational to include other nice ideas like references to [2.0]] [Sorry again for the long words]

[1:07:47 - End of Video]

- 📝 [commentary] Salutations

References

[1.0]
Development Journal Entry / July 8, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%207%20-%20July/July%208%2C%202021

[2.0]
Development Journal - Technological Ideas / Light-Based Computers
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/light-based-computers.md



