

# Day Journal Entry - July 14, 2021

[Written @ 13:52]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:22:28]

- 📝 [commentary] I wrote notes on the "Identity Idea" that came to mind today [1.0]

[2:22:28 - 3:21:56]

- 💡 [initialized] I created the initial Desktop Application for the Ecoin 2.0 Codebase [2.0]

[3:21:56 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal - Technological Ideas / Identity Idea
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/identity-idea.md

[2.0]
Ecoin
By E Corp
https://gitlab.com/ecorp-org/ecoin




[Written @ 9:46]




