

# Day Journal Entry - July 3, 2021

[Written @ 14:12]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:57:59]

- ✅ [completed] I wrote some program related things for Ecoin 2.0 [1.0]

[3:57:59 - 4:49:53]

- 📝 [commentary] I took a break from the computer and rested my mind

[4:49:53 - End of Video] 

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By E Corp
https://gitlab.com/ecorp-org/ecoin


