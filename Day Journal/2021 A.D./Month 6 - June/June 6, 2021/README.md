

# Day Journal Entry - June 6, 2021

[Written @ 18:19]

getAuthenticatorKeyByRecoverableInformationTable (recoverableInformationTable: any): Promise<AuthenticatorKey> {
  //
}

// Step 1: email
// Step 2: emailConfirmationCode

. . 

// Step 1: anonymousEmail
// Step 2: recoveryPassword

. . 

// Step 1: anonymousEmail
// Step 2: neighborhoodEmailList
// Step 3: 



[Written @ 11:44]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:28:12]

- 🚧 [work-progress] I worked on the `Ecoin 2.0` library. I worked on '`file systems`' like '`nodejs`' and introduced the initialized idea of '`qr code`' as being a file system as well as the traditional file system ascribed with digital computers. I'm sorry for not doing a good job. Maybe I could have done a better job.

[1:28:12 - 2:44:29]

- 📝 [commentary] I was away from the keyboard. I went out to eat some lunch. I didn't do anything on the live stream.

[2:44:29 - 6:09:55]

- 🚧 [work-progress] I worked on the `Ecoin 2.0` library.

- 💡 [initialized] Some work that is left to be done includes:
  - (1) Create, Update, Delete and Get List for Authenticator Keys
  - (2) QR Code File System Integration For Accessing Authenticator Keys
  - (3) Create Portable Message
  - (4) Add a Secondary Replacement System For Cryptography that doesn't use the name "Firebase" and so it's a "general" authentication system that doesn't use "Firebase" by default. Although we will have support for Firebase and other possible Authentication Providers as well.

[6:09:55 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By E Corp
https://gitlab.com/ecorp-org/ecoin



