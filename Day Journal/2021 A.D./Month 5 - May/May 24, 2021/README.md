

# Day Journal Entry - May 24, 2021

[Written @ 20:50]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[]

- 📝 [commentary] 

[]

- 📝 [commentary] 




[Written @ 13:33]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 4:35:09]

- 📝 [commentary] I wrote an initial version of the Ecoin 2.0.0 codebase using the 'listless' 'architecture' style of coding. `The Listless Architecture` is basically '`The Light Architecture`' but it has a different name since so many things have changed since my initial impressions of what 'The Light Architecture' would look like. The Listless Architecture is called what it's called because it's a list that can keep going but for some reason 'reversing' the list of re-indexing it is useful like 'updates' and so a 'listless' means you don't have to worry that your list is a list and so you can update the list.

[4:35:09 - End of Video]

- 📝 [commentary] Salutations



