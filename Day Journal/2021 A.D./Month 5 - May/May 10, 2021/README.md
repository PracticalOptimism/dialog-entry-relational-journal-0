

# Day Journal Entry - May 10, 2021

[Written @ 18:18]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:05:22]

- 📝 [commentary] I wrote notes [2.0]

[3:05:22 - 3:34:43]

- 📝 [commentary] I watched cartoon animations [3.0] [4.0]

[3:34:43 - 4:00:58]

- 📝 [commentary] I wrote notes [2.0]

[4:00:58 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
High School Wrestling: CHS vs. JFK, January 11, 2020
By WoodbridgeTV
https://www.youtube.com/watch?v=5SEiarCdj2g
[I watched this video at nearly 3:05:22 minus 40:00 minutes]

[2.0]
Development Journal Entry / May 10, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%2010%2C%202021

[3.0]
Gantz- Opening full- super shooter
By Fuzne
https://www.youtube.com/watch?v=iGqiwr3HnCY
[I watched this video at nearly 3:33:52 minus 20 minutes]

[4.0]
Hunter X Hunter - Ending 2 | Hunting For Your Dream
By Crunchyroll Collection
https://www.youtube.com/watch?v=tK-d2LJlaaQ
[I watched this video at nearly 3:33:52 minus 20 minutes]



