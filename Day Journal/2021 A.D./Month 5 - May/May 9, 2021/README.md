

# Day Journal Entry - May 9, 2021

[Written @ 19:13]


Key Features:

(1) Distributed Computing

- The Practice: Uses technologies like IPFS, Dat and Braid Protocol.
- The Principle: A peer-to-peer network distributes control to the network rather than a few monopolies.
- Your Mission: Learn more about decentralized technologies like IPFS, Dat and Hashgraph.

(2) Universal Basic Income

- The Practice: Everyone receives a payment every 60 seconds. 
- The Principle: We are all creative.
- Your Mission: Dream big, dream small. Afford to live through it all.

(3) Open Source

- The Practice: Anyone can rebrand the code base and start their own currency.
- The Princple: Let’s create a world we each individually love.
- Your Mission: Modify the code to your heart’s content.

(4) Anonymized Identity Support

- The Practice: Anyone can have an account.
- The Principle: We are all citizens of a global community.
- Your Mission: Know you belong to a global ecosystem. Live in the future without borders.


[Written @ 14:51]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 2:44:27]

- 📝 [commentary] I wrote notes about an algebra system for 'interesting information generalization tactics' right? [2.0] Specifically the notes were possibly meant for 'pamper' [3.0] but they could theoretically possibly be useful for [smirk emoji] [evil grin emoji] [dangerous evil grin emoji] [butterfly Shaiapouf emoji] :D sparkles and sunshine :D . . 🦋✨

[2:44:27 - 4:16:59]

- 📝 [commentary] I wrote more notes about '`the algebra system`' mentioned above [2.0] . . algebra is a general term maybe meant to mean 'locate something' which is like 'use an index' which is like 'street address' or 'number of sheep in a farm' to know which farm you are in and to know that you are not at the right farm if you counted less than you thought you had . . 'thinking' is like an algebra system because it can be like 'yea, I forgot' and so 'keep track of the things that you like' is a nice 'number method' (1) '`growing`' (2) '`inspiring`' (3) '`aging`' (4) '`family relations`' each of these things are 'additive onto themselves' and so for example 'growing' can be applied to 'family relations' and 'family relations' can be applied to the topic of 'growth' such as this group of text that is describing familiar patterns with words on the topic of 'yea, I guess I could have grown from reading that' . . and if you have 'algebra' that means 'add' 'subtract' and so of course that means you can just repeatedly add and subtract without friction or loss of interest and so you have 'infinite number generation' or 'infinite interest generation' because 'family relations' can 'inspire' you to create more 'things of interest' (which can possibly be flat mapped to 'family relations' for a consciousness type . . that could possibly relate to your own personal consciousness type right? . . and of course this '`things of interest`' label gives you leeway to invent your own algebra instead of using the 4 list algebra system that I typed here for you to read . . ) . . Algebras are really cool because you can say for example 'apply that force here' and so for example 'look at the force of the 'hardness property of a rock or a stone or a granite surface and say 'apply that force of what that means to another system like 'air' and say that 'yo, this air is rock hard' or 'yo, this air is really pitch black like a granite surface' and so you can apply various algebras or identities or identification units from various concepts or ideas or projections of the psyche' onto other projected items of interest . . Such as 'there is a social relationship over there' now 'try to immitate that in this video game over here' 'but replace every character with a pair of 'geese' that 'fly' every '2 meters' and 'use the toilet' in the 'sky'' And if that force is useful for the fuel of life that you're trying to create then maybe someone else will use that algebra system and be like 'yo, apply that geese matrix to my bicycles over here' 'yo, my shoes could use an emoji with that geese matrix bonding relationship on the front heel' 'yo, you know what, those shoes don't have enough blah blah blah . . why don't you smack some 'type set theory onto that with this or that algebra of interest'' . . 'yea, I got you . . easy does it . . '

[4:16:59 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Shawapf playing violin
By Jvero
https://www.youtube.com/watch?v=O3Ci9w2rnB4
[Song that I listened to near 2:42:12 minus 5 to 10 minutes]

[2.0]
Development Journal Entry / May 9, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%209%2C%202021

[3.0]
Pamper
By Jon Ide
https://gitlab.com/PracticalOptimism/pamper

