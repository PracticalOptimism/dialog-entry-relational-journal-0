
# Day Journal Entry - May 12, 2021

[Written @ 21:19]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:20:03]

- 📝 [commentary] I wrote notes [1.0] and worked on 'Pamper' [2.0]

[1:20:03 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / May 12, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%2012%2C%202021

[2.0]
Pamper
By Jon Ide
https://gitlab.com/practicaloptimism/pamper



