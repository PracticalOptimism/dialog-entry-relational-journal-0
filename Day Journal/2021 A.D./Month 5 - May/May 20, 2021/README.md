

# Day Journal Entry - May 20, 2021

[Written @ 19:33]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:09:18]

- 📝 [commentary] I'm sorry for the sentence that I wrote in Ecoin #562 that could have possibly offended someone [1.0]

[0:09:18 - 1:01:43]

- 📝 [commentary] I introduced various aspects of the project 'Ecoin' so far. I introduced 'listing items'. [2.0]

[1:01:43 - 3:58:08]

- 📝 [commentary] I wrote notes on 'Our Very First Toilet' [3.0]

[3:58:08 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin #562 - Live development journal entry for May 19, 2021
By Jon Ide
https://www.youtube.com/watch?v=fI-zqWV9N3A&feature=youtu.be

[2.0]
Development Journal Entry / May 20, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%2020%2C%202021

[3.0]
Our Very First Toilet
By Jon Ide
https://gitlab.com/PracticalOptimism/our-very-first-toilet



