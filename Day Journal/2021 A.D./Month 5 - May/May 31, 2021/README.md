

# Day Journal Entry - May 31, 2021

[Written @ 19:29]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 2:06:23]

- 🚧 [work-progress] I wrote some software for the project so far. `universal-basic-income-1-community-agreement-check.ts` was the primary file that I worked on. This file belongs to the latest Ecoin 2.0 file structure. [1.0]

[2:06:23 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin


