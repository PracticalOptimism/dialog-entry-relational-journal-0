

# Day Journal Entry - May 8, 2021

[Written @ 14:49]

Notes @ Newly Discovered Ideas

I was saving the 'timeline annotation legend tree' to the 'YouTube Description' for the Live Stream for Ecoin #551 . . and in my vision . . I was slightly disoriented for a few milliseconds or something like that . . it was a fraction of a second . . and I saw that the text displayed in the `description box` or the `description window` or the `description area` . . showed that some of the text was 'highlighted' . . 'highlighted' . . O _ O that is really a strange idea O _ O 'what does it mean for something to be . . O _ O . . 'highlighted' O _ O . . it is like a ghost idea for me when I was going to the next page O _ O . . it well it wasn't a ghost idea . . it simply reminded me of how the highlighting works in 'readme.md' renderers like what Gitlab does with their 'readme.md' renderer and shows that the text is 'gray' or 'highlighted gray' . . I am now starting to imagine that if . . (1) YouTube highlighted text like a readme editor or a readme or markdown renderer . . technically I think it's called a 'markdown renderer' since the file format is called 'markdown' and 'README' is possibly one of the post popular names that I'm familiar with anyway . . and so 'README.md' or 'README markdown' and 'README' is possibly meant to mean 'Read Me' or 'read me' to possibly suggest an introduction to a reader who would like to learn more about that computer program . . that's my hypothesized idea of what the origin of the word 'README' is and so it's possible 'Introduction.md' could have been the initial idea . . or 'Warning.md' or 'Attention.md' or other ideas O _ O but 'README' is possibly a 'highlighting' O _ O term O _ O . . O _ O that is active and loud and pronunciating right O _ O ? . . but how do words and even ideas become highlighted in our attention ? . . why do high lights look so cool ? . . O _ O . . 

A tutorial has highlights right? . .

Well so maybe in thinking about 'the-light-architecture' 'highlights' . . well . . what do those possibly mean . . 

(1) space order <br>
(2) time order <br>
(3) need to know order <br>
(4) nothing else #1 order <br>
(5) nothing else #2 order <br>

. . 

Alright . . well those are a few ideas when I start thinking about 'the-light-architecture' that help orient me to thinking about how to 'organize' new ideas . . since all the ideas are really possibly very useful but I just don't know how to 'organize' them . . 

Uh . . organizing them can possibly suggest 'they belong in a family with other ideas' . . 'they belong in a grouping or in an ordering with other ideas' 'families' 'families of ideas' 'families' . . 

And so 'families' of ideas like the orders of interest that are listed (1) through (5) . . are possible indicator points that help highlight certain locations of interest for all types O _ O right? O _ O and since it's a finite list it helps us know that we are possibly going to have a nice indexable family right ? . . a family that you can take a photograph of an index in a book ? . . right ? . . perverts . . right ? . . 

Okay . . initially . . my first thought is that I don't really know where 'highlight' goes in this index ordering list . . 

'highlight' reminds me oh O _ O outline . . Outline . . reminds me of . . 'Outlier' . . outlier is an interesting term O _ O . . 

Outlier . . Outline . . Highlight . .

Highlight reminds me of yellow marker highlighting schemes . . and those are very effective for taking notes . . and even parts of the YouTube website use highlights already : O . . take a look at the '`Public`' '`Private`' '`Unlisted`' indicator under a video circa May 8, 2021 when this note listing was made . . 

There is a highlighted item that looks like [1.0]

[1.0] Example Highlighting from YouTube Video
![YouTube Highlight Example](./photographs/youtube-highlight.png)

. . 

Anyway . . I thought I would write this comment here . . I don't really know what highlights uh . . 'introduction' is like possibly a settings item and so for example . . 'settings' are uh possibly like things that are uh . . being related to or defined for a particular program or something like that . . 

-_- I have to think about it some more . . but for some reason . . if you highlight some text . . you're possibly sending the 'clairvoyant message' that 'settings are involved' so it's like 'toggle your mind in a particular way to make sure you are indicating this as a useful item' . . 'toggle your mind' -_-

'toggle your mind' can mean 'do some research on this term or -_- ' . . well that's a basic example . . 

because settings are more involved and can mean it's like a 'video game item of interest' and so if it's highlighted with nice sparkles around the game item then that would be a particular type of highlight that says 'woah, look that item could be interactable' and so if you interact with a highlighted item it's like you're playing around with settings . . right? . . 

right? . . 

It's like . . when you ask the question right? . . you're also possibly highlighting something for the user to consider and so play around with the settings of what you think is possible and see if that is the setting that you're wanting . . or something like that . . 

`is that part of the game?`

`to understand that . . is that part of the game? . . `

`well . . `

Settings are . . updates . . so it's like . . '`update`' function name . . so uh . . that solves that mystery am i rite? . . AM I RIGHT? . . 

. . 

Set . . update . . 

. . 

Settings as a term isn't all that clear to me along where it works in our programming theory training course idea development track . . 

for example . . why do you have . . 'update' and 'set' . . which are really quite close enough and you could say 'set' could be a verb . . and so . . 

. . 

update and set are equivalent or possibly relatable to equivalency if we wanted to make that assumption or something like that -_- . . but at least . . we have to consider . . the history of computer programming has also used the word . . 'settings' . . which is like . . a seemingly interesting way to say . . '`a list of things that can be updated`' or '`a list of things that can be set`' or '`a list of things that can be mutated`' or '`a list of things that can be transformed`' . . 

Settings . . gives you the 'list' idea in some sense right ? . . it gives you a 'noun' or an 'item' idea from what was once a 'verb' or an 'action' idea right ? . . a 'verb' or an 'action' or an 'algorithm' or an 'algorithm evaluation' idea right ? . . 

Settings . . [Update]

Creatings . . [Create]

Deletings . . [Delete]

Gettings . . [Get]

. . 

Why do you have nouns like 'settings' ? . . where are its counterparts . . 'creatings'? is that the counterpart? . . 'deletings' . . 'gettings' ? . . hmm . . well . . it is interesting . . 

. . 

An intention to update . . Setting . . 

An in-process of being updated item . . Setting . . 

. . 

`Set`: Not yet set (function name, `please call me`)
`Setting`: In process of being set (Processing, `be right back`)
`Set`: Completed set (`game, set match, I win`)
??: ??

. . 

Okay . . by writing these 4 items . . I am following this part of 'the-light-architecture' . . called . . '`tally-insignificant-item-endpoint-indicator`' . . 

These items were inspired by 'endpoint' idea from trying to organize the list of things where we can say 'our project has endpoints and that we can achieve the completion of those endpoints'

```js
class TallyInsignificantItemEndpointIndicator {
  unmeasured = null
  measured = null
  complete = null
  annoyedBy = null
}
```
Well if you want to complete a task . . you may not know how to measure its ability to be completed . . it may be so fantastical and fancy that you are lost with where to start . . 

After you know you are lost . . list that item in '`unmeasured`' . . 

If you know where to move forward with respect to some 'coordinate' points -_- whatever that means for your particular language of interest in whatever you're trying to apply . . then '`measured`' is the place where you can place the item of which you have a complete measurement of what needs to happen . . and when it needs to happen if time is a relevant aspect for that type of information . . 

But you're not really finished with the task . . you only have a measured keypoint listing of what you THINK you could possibly achieve . . there may be some 'unmeasured' aspects which is lame O _ O . . 
            
```md            
          O   O
O      O        O
       O         O
O       O       O
          O   O
```

: O 

. . 

O _ O it's very difficult to 'concatenate O _ O ' with :O unless I tell you that I would like to have a specific emoji that shows O _ O (wow that is really surprising eyeballs) and :O (oh my gosh my mouth is so wide open because I'm surprised) . . and in combination that could equal . . Oh my gosh my eyes are huge . . and my mouth . . my eyes are bigger than my mouth . . 


Algebra class:

(1) O _ O                       Open eyes emoji

(2) : O                         Open mouth emoji

= 

. . 
```md
          O   O                 Big eyes, Big mouth emoji, so shocked
O      O        O
       O         O
O       O       O
          O   O
```
. . 

:O . . 

. . 

Is not so shocked it's so small . . :O is so small it's not so shocked you need words to help you highlight the emotion in your body :O . . writing words repeatedly is not useful right . . just write a big emoji O _ O :O O _ O . . 

. . 

O _ O : O O _ O . . could be a replacement O _ O algebra O _ O 

. . 


'`complete`' means you have achieved O _ O . . 

. . 

'`annoyedBy`' means you have O _ O . . been annoyed by some things and possibly those are very relevant to list for the rest of the team to be aware of in case anyone runs into a related idea . . O _ O . . 

. . 

So you can do this repeatedly -_- define new tasks -_- repeatedly . . for O _ O new projects . . and of course . . for the same project . . O _ O . . 

. . 


`Set`: Not yet set (function name, `please call me`)
`Setting`: In process of being set (Processing, `be right back`)
`Set`: Completed set (`game, set match, I win`)
??: ??


. . 

Well in theory if we can flat map onto that idea of . . `insignitifant item endpoint indicator` then we can possibly have a nice idea . . right ? . . 

well check it out . . it's easy to say this isn't a bad place to flat map since the other places we could flat map -_- well whatever . . do more research right ? . . just look over those and see if the idea that you're trying to flat map onto makes sense there right ? . . -_- . . 

. . 

for example . . FOR EXAMPLE . . check out what we have . . 'setting' is like a 'noun' version of 'set' . . and 'set' can be assumed to be a 'verb' in the way we're using it right now right . . okay cool . . so in where . . where . . do have a . . weird idea like . . 'setting' which itself sounds like 'I am currently setting something' or 'I am currently doing that thing' . . so it's like a 'active' verb as well as a noun O _ O . . 'I am setting' . . but when I personally have been thinking about the word 'setting' or rather 'settings' more specifically I start to think to myself 'that looks like a noun in the way ti's being used' . . and yet the 'singular version' of 'settings' could possibly be either a 'noun' or a 'active verb' right? . . O _ O that's so strange . . 

I am only now discovering that it's possibly 'active verb' after writing these notes . . but for what reason ? . . 

well anyway . . what reason would make sense if we can relate it to the neighbors that it has . . and so . . it's like . . 'can you flat map that onto a neighborhood listing?' . . 'is there neighbors for that word to start trekking around in our codebase very nicely and easily and conveniently and we can keep an EYE on that matrix idea O _ O . . 

. . 

Okay well . . since it's an 'active' verb like . . 'in process of being' or 'in process of doing' . . and since we already have that endpoint indicator in our list of ideas . . for 'the-light-architecture' . . we can say that is where the term 'setting' can go . . since it seems to behave as . . an 'active verb' as well as its 'noun' counterpart -_- uhm but the noun I don't really have an idea where that is flat mapping to right now . . but the 'active verb' I seem to have the idea that it could flat map to . . this object . . item . . data structure . . data structure type . . '`insignificant-item-endpoint-indicator`' . . or '`insignificant-order-indicator`'

```js
class TallyInsignificantOrderIndicator {
  unordered = null
  inProcessOfBeingOrdered = null
  ordered = null
  blockedBy = null
}
```

. . 

I am sorry . . `Insignificant Order Indicator` could fit our hypothesis even more than Insignificant Item Endpoint Indicator . . Well . . Insignficiant Order Indicator was first inspired by Insignificant Item Endpoint Indicator . . uh . . so that's possibly why I am currently writing about why I was inspired to think about . . 'Insignificant Item Endpoint Indicator' . . 

. . 

'unordered' 'neutral' . . neutral . . is what is sort of meant be . . the first use of set . . since we are trying to get across the idea that 'we are calling you to action and you have not taken action yet . . ' . . 'hey you! . . SET! . . '

`Set`: Call to action
`Setting`: Action in process
`Set`: Action completed
`Incomplete`: inaccessible ability

. . 


`Set`: Not yet set (function name, `please call me`)
`Setting`: In process of being set (Processing, `be right back`)
`Set`: Completed set (`game, set match, I win`)
??: ??

. . 

Okay

`Get`: Call to action . . get your ice cream here . . 
`Getting`: In the process of getting . . 
`Retrieved`: Complete
`Inaccessible`: Unavailable information

. . 

: O from this we are able to say 'Getting' or maybe even 'Loading' or 'Transpiling' are possible . . co-related words for 'set-and-setting' counterpart words right ? . . for example . . get and loading are very familiar and so we now have a THEORY about how to invole the word 'loading' which is so similar to the intention of the word 'getting' : O . . we are good mathematicians . . look how we have improved the user interface of our computer . . we now have a way to say a 'loading' page is very useful besides just a 'settings' page which would allow us to say 'things that are currently being loaded behind the scenes ought to possibly be visible for the user to see that these things are in the process of being loaded . . 

`(1)` Infinite Loops are possibly things that are continually being . . loaded or evaluated if you want to write that message <br>
`(2)` Functions that are retrieving information from network protocols . . like database or artificial inteligence algorithms that need time to think . . could possibly be items that are like 'loading' . . O _ O : O O _ O . . <br>

And well for me personally this little side adventure we've played here has helped me think about the word '`loading`' since it is something that is available in computer game user interfaces . . and seems to be quite useful to communicate to a player of a video game or something like that . . and especially the term 'getting' isn't necessarily that popular and so using the term 'getting' is like . . maybe restricting the access to the audience we can reach by our computer program . . and so . . yea . . yea . . yea . . 

. . 

`Loading . . . `

. . 

Hmm That is quite a useful page even for debugging purposes . . 

. . 

'what are all the things that are 'in-transit?''

. . 

unmeasured -> measured -> complete -> annoyedBy <br>

Get -> Loading -> Loading Completed -> Inaccessible Information <br>
Update -> Setting -> Update Completed -> Invalid Operation <br>
Create -> ?? -> ?? -> ?? <br>
Delete -> ?? -> ?? -> ?? <br>

Recommend -> ?? -> ?? -> ?? <br>
Scale -> ?? -> ?? -> ?? <br>

. . 

Here is an exercise for you the audience if you are watching this

(1) What is a counterpart of 'Creatings' that is (1) popular and (2) user friendly . . ? . . 

(2) What is a counterpart of 'Deletings' that is (1) popular and (2) user friendly . . ? . . 

Can you think of any other verbs that we may want to say . . they are in the process of becoming themselves ? . . and so for example . . 'get' is in the process of being 'received' because it hasn't necessarily be 'processed' or 'completed' yet because it is still 'in-transit' . . 

. . 

'recommend' . . 'scale' . . what do you think ? . . 

. . 

-_- well I haven't finished this exercise myself . . and so you are watching me take notes in realtime as I am still thinking and developing this architecture :O and of course Seth and Pleiadians are helping me with things . . 

Well . . I am tired and would like to go get some food to eat now . . but if you find good words for these things . . please feel free to write about that . . and also you are a good mathematician . . 

. . 



[Written @ 10:14]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:00:27]

- 📝 [commentary] I wrote notes in my development journal [1.0]

[3:00:27 - 4:27:11]

- 📝 [commentary] I wrote notes on technology and ideas [2.0]

[4:27:11 - 5:47:10]

- 📝 [commentary] I wrote notes about '`get`', '`create`', '`update`', '`delete`', '`recommend`' and '`scale`' which could have '`insignificant item endpoint indicators`' . . This was a spontaneous discovery inspired by the idea of '`highlights`' . . [3.0]

[5:47:10 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / May 8, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%208%2C%202021

[2.0]
Inventions and Ideas
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas

[3.0]
Day Journal Entry / May 8, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%205%20-%20May/May%208%2C%202021

