

# Day Journal Entry - May 28, 2021

[Written @ 12:36]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[x:xx:xx - x:xx:xx]

- 📝 [commentary] 

[x:xx:xx - x:xx:xx]

- 📝 [commentary] 



