
# Day Journal Entry - May 2, 2021

[Written @ 9:13]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:41:42]

- 📝 [commentary] I typed wrote about 'data ownership' or 'website ownership' as well as 'item changer' [1.0] [2.0]

[3:41:42 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / May 2, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%202%2C%202021

[2.0]
Development Journal Entry / May 3, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%203%2C%202021



[Written @ 9:05]

Notes @ Newly Discovered YouTube Channels

[1.0]
Men, STOP Hooking Up || A Jewish wife talks about sex!
By Classically Abby
https://www.youtube.com/watch?v=95wp_Qsz7vc

[2.0]
Ye are GODS: Annalee Skarin
By Giving Voice to the Wisdom of the Ages
https://www.youtube.com/watch?v=oONGn1y_XQc

[3.0]
Jamie Butler- Human vs Spiritual Awakening
By Jamie Butler The Everyday Medium
https://www.youtube.com/watch?v=BfCWbOCPAKQ

[4.0]
Detox Your Life! How To Cleanse Your Body, Mind + Life To Manifest What You Want (SP + More!)
By Mandy J Ross
https://www.youtube.com/watch?v=pxvSPrsbbsM

[5.0]
MK8.3 Entitled Millennial Unicole Unicron says we are entering a New Age
By Omni Eros Magick Kool-Aid
https://www.youtube.com/watch?v=xk_t4sKXj24



[Written @ 9:03]

Notes @ Newly Created Words

precile
[Written @ May 2, 2021 @ 9:03]
[I accidentally typed the word 'precile' instead of 'precisely' . . I'm sorry . . I'm not sure what this word 'precile' should mean at this time . . ]

[Written @ 8:54]


Notes @ Newly Discovered Quotes

"
They imagine that they know something . . that they can't possibly know . . 

In this way . . a Spiritual Mentality . . no matter how secular it may seem . . no matter how . . inviting . . no matter how . . hedonistic . . no matter how . . self-indulgent . . 

In this way . . a spiritual attitude . . leaves all of us . . blind . . Ultimately . . it cuts you off . . From . . the full range . . of . . the experience . . of your . . instincts . . It cuts you off . . From the process . . of learning . . 

Cause all of that . . begins . . and involves . . that most . . fundamental . . sense . . of . . doubt . . that you can't know . . That you can't trust other people . . 

That you don't even know what it is . . that . . you . . want . . Let alone . . what other people want . . Let alone . . what will make the two of you happy together . . 

That that's a very fundamental doubt . . that should never be taken away from people . . and replaced . . with . . faith . . 

Faith . . allows you to . . live your life . . with the false sense . . of confidence . . That you already know . . precisely what it is . . That . . you never . . can . . tell . . 
"
[1.0 @ 20:02 - 21:03]


[1.0]
Hitomi Mochizuki & Abigail Shapiro: the shallowness of spirituality.
By à-bas-le-ciel
https://www.youtube.com/watch?v=wrkbyujLR80



