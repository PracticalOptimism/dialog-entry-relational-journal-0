

# Day Journal Entry - May 21, 2021

[Written @ 21:55]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:19:08]

- 📝 [commentary] I typed notes about 'error correcting' the 'typed listing items' for each of the 'listing items' for a project [1.0] In general . . 1.x.x.x.x.x.x is not a bad method. 'x' represents a number like 1, 2, 3, etc. . . 1.2.3.4.5.6.7 . . is a possible correction listing . . for how to correct 'type' lists . . I'm not exactly sure but in maybe a theoretical circumstance of interest . . one may need a new 'decimal' item so as to distinguish their ordinary decimal versioning indices with another type of 'theoretical' 'origination' device that is like . . 'decimal' 'versioning' possibly isn't effective anymore . . I am not necessarily having a good time imagining that case right now since you could always add a decimal for that item of interest instead of introducing a new type of 'decimal' distinguishing notation . . which is like another 'hold' 'in place' syntax of some kind of holding the other names of the versioning list in place . . or something like that . . That is a topic that I can talk about as well . . 'holding in place' but I have only wrote 2 or 3 words of notes on that and don't know much about that topic . . [perospective-wait]

[1:19:08 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / May 21, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%2021%2C%202021




