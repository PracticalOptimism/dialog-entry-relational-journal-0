

# Day Journal Entry - May 1, 2021

[Written @ 14:04]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:21:29]

- 📝 [commentary] I wrote notes on various thoughts and ideas on 'the-light-architecture' [1.0]

[3:21:29 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / May 1, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%205%20-%20May/May%201%2C%202021




