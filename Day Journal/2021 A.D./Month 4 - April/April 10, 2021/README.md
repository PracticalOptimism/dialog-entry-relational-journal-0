

# Day Journal Entry - April 10, 2021




[Written @ 23:20]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:59:03]

- 📝 [commentary] I wrote notes for the README.md file for 'the-light-architecture' [1.0]

[0:59:03 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - April 6, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%206%2C%202021





[Written @ 2:12]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:11:48]

- 📝 [commentary] I wrote notes on 'Project Intended Usecase' and 'Project Accidental Usecase' [1.0]

[3:11:48 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - April 6, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%206%2C%202021



