

# Day Journal Entry - April 28, 2021


[Written @ 17:50]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:43:17]

- 📝 [commentary] I worked on an updated idea for the data structure for the 'project website service' for Ecoin [1.0]

[1:43:17 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / April 28, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%2028%2C%202021


[Written @ 6:23]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:53:55]

- ✅ [completed] I added a new `TallyImportantMessage` data structure [1.0]

[0:53:55 - 3:37:42]

- 💡 [initialized] I added the 'consumer-ready-user-interface-item' usecase for 'the-light-architecture' project [2.0]

[3:37:42 - 4:21:05]

- 📝 [commentary] I don't know

[4:21:05 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
The Light Architecture / Commit for initializing the latest 'TallyImportantMessage' data structure
By E Corp
https://gitlab.com/ecorp-org/the-light-architecture/-/commit/b9c9fb4469ca716767dbb13192309cd16aecd4bf

[2.0]
The Light Architecture
By E Corp
https://gitlab.com/ecorp-org/the-light-architecture

