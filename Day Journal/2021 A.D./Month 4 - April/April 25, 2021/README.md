

# Day Journal Entry - April 25, 2021

[Written @ 23:37]

### Maze running . .

(1) how can you get to a position in the maze <br>

(2) how many steps will it take you to get there? <br>

(3) do you have runtime constraints like new obstacles are introduced randomly and sufficiently necessary to stop you from making progress in your goal? <br>


. . it's like (1) playing basketball is a compartmentalization of a 'Maze Runner Tally' or a 'Maze Runner Organizai' . . 'Organzai' could possibly be an alterior motive spelling since 'Organi' sounds interesting and possibly strange . . so . . remove the 'i' and say . . 'Organzai' or 'Organzi' as I have mistakely typed . . Maze Runners are amazing . . you can say . . a lot of problems can be solved by 'maze runners' . . (1) public transportation can be solved with maze runner algorithms and data structures . . the algorithms will transport the data structures where they need to go . . and when they need to go there . . data structures can include . . people . . or buses . . or bicycles . . algorithms can include . . walking . . or using wheel bearing systems or using flight transportation . . etc . . you can imagine so many possibilities because you have a metric for tallying your ideas :O

. . 

### Types of Tally Systems

Maze runners . . a type of tally system . .
1 Square Sheet of paper . . a type of tally system . . 

Ferris Wheel . . a type of tally system for . . O_O

. . 

### Examples of Tallies

`Micromouse` is an example of a Maze Runner Tally . . or a Maze Runner Organizai . . 

`Origami` is an example of a 1 Square Sheet of Paper Tally . . 

. . 

Maze Runner Tallies like 'Micromouse' inspire me to think of . . 'search algorithms' . . what do you think a 'search algorithm' does? . . In theory a 'search algorithm' or a 'get response for recommended item algorithm' is a type of algorithm that traces a set of text data and or possibly a corpus of text which is large in size and needs to be traces forward and backward in different proportions to try to discover ideas like 'what' 'why' 'when' 'how' . . and answer those questions in a minimum length runtime . . and so those answers to those questions can be like 'subgoals' of the micromouse computer program that runs through the maze and tries to find where those things are in a text paragraph . . and a human translator . . or a human judge or a human . . judiciator or judgementlord or committee-apprentice-demon-monster-machine-with-2-or-3-heads-or-two-or-three-tongues-or-scissors-for-tongues-ideas can also be involved in slicing through the thoughts of the small little micromouse as it tries to subdivide the world in its own little maze of yama-yama-yama and tries to introduce terminology to describe yama-yama-yama and yama-yama and yama and yama . . and all of those orders of proportionalities are like yama to a yama . . and so yama . . 

and all of those orders of proportionalities are like 'dice' to a 'professor' and so . . 'throw some dice' . . 

'why?'

'I don't know'

'I don't know'

'I don't know'

'I don't know'

'I don't know'

'why?'

'yes'

. . 

### Questions For The Audience / Ideas For The Audience To Consider


(1) What other examples of things could you do with 1 square sheet of paper? Besides Origami . . can you think of other things you could develop besides shapely creatures? . . 

That is a mathematics question left to the audience to answer . . be active and support one another . . 

. . 

(2) Can you imagine what type of interesting things you could do with a ferris wheel? . . How can you organize ferris wheels to do interesting things? . . can you think of an example of a 'Ferris Wheel Tally'? . . Come up with your own name and be original . .

. . 




[Written @ 22:49]

Notes @ Newly Created Words

organzai
organzi
[Written @ April 25, 2021 @ 23:30]
[I typed an alterior motive generation of 'organizai' to pronounce the word more interestingly like a 'noun' could be pronounced with ease if it has so many few syllables . . or something like that . . 'organzi' . . 'organizai' . . 'organzai' . . "Organizai" is quite advanced but I also still like it . . "Organizai" . . it sounds like a torepdo or a samurai sword and the complication makes it quite useful . . for advanced 'Tally' systems or something that's O_O]

Notes @ Newly Created Words

organizai
[Written @ April 25, 2021 @ 22:50]
[I accidentally typed the word 'organizai' instead of 'organization' and this word 'organizai' reminds me of the word 'origami' in some way . . I think the term 'zai' perhaps influences me to think of 'Chinese' or 'Japanese' culture with 'zai' being a possible relation to other terms like 'guo' and 'zhao' and 'zao' and 'zi' and 'xi' and other similar terms and alphanumeric or alphabetical words uh . . from uh . . the uh . . Pinyin . . Chinese Phonetic . . system . . for pronouncing or enumerating characters in . . 'romanized' or 'roman-alphabetized' or 'grecco-roman' or 'greek-roman-alphabetized' or 'grecian-roman-alphabetized' and other types of related base languages for 'English' or something like that . . I'm sorry I'm not very much a historian on this topic and so . . that is an approximate development of my understanding . . highlighting . . 'Pinyin' was my main point . . 'Pinyin' search term is useful . . uh . . well . . uh . . as far as 'organizai' . . I'm now thinking that perhaps . . 'organizai' is also a nice uh . . counterpart term for 'tally' since they both relate to 'organization' of . . 'information' or . . a . . 'data structure for organizing information' . . 'origami' is also a really cool word since it's also like a basic . . 'data structure' format machine since if you possibly familiar with the word of . . 'Erik Demaine' . . then you'll possibly know about interesting . . Origami related . . mathematics topics . . [1.0]]

[1.0]
Erik Demaine's Folding and Unfolding: The Fold-and-Cut Problem
By Erik Demaine
https://erikdemaine.org/foldcut/


[Written @ 22:32]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:26:44]

- 📝 [commentary] I prepared a conversation to talk about 'the-light-architecture' and got distracted by showing videos

- 📝 [commentary] I watched [2.0] [3.0]

[1:26:44 - 4:10:57] 

- 📝 [commentary] I wrote notes on various topics like (1) a playback repeatable list data structure for user interfaces (2) expansion panel grid (3) castes of itemization lists for each of readability (4) a review on things that I've researched before this episode with regards to how it relates to the 'the-light-architecture' project [4.0]

[4:10:57 - End of Video] 

- 📝 [commentary] Salutations

References:

[1.0]
The Light Architecture
By Jon Ide
https://gitlab.com/ecorp-org/the-light-architecture

[2.0]
micromouse classic
By asrebro
https://www.youtube.com/watch?v=hNtXHv1n48Q

[3.0]
The five fastest MicroMice at APEC 2019 USA
By MicroMouse
https://www.youtube.com/watch?v=_cAIwvlA_XA

[4.0]
Development Journal Entry - April 26, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%2026%2C%202021


[Written @ 14:22]

[Copy-pasted from the YouTube Live Stream Chat]

Tally . . tally is a specific name that we can use to denomate a type of information organization data structure . . I just made it up right now . . and possibly you may like it as well . . a tally is short to pronounce like 'dog' or 'planet' and it is a 'noun' but it is also related to interesting ideas like 'numbers' and 'counting' and so 'a tally' could be what we call 'the light architecture' data structure


[I wrote this message a few minutes ago now . . and it was awesome :O]

[Copy-pasted from the 'the-light-architecture' https://gitlab.com/ecorp-org/the-light-architecture]


A `tally` is the technical name for an 'information organization data structure'.


[Written @ 7:20]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 2:02:23]

- ✅ [completed] I updated 'the-light-architecture' README.md [1.0]

[2:02:23 - 9:22:59]

- ✅ [completed] I worked on 'the-light-architecture' [1.0] This is a presentation where I started working on 'the-light-architecture' codebase and not only the README.md file which was mostly what I had been working on for the past few days.

[9:22:59 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
The Light Architecture
By Jon Ide
https://gitlab.com/ecorp-org/the-light-architecture



