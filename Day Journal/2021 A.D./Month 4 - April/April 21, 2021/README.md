

# Day Journal Entry - April 21, 2021

[Written @ 10:44]

- [item of interest completed]
- [item of interest worked on]
- [item of interest successfully completed]
- [item of interest delayed]
- [item of interest kept a secret]
- [item of interest open to public but not right now should you apply it please be kind and follow these rules or else negative consequences like punishment and ridicule for administering ideas that are too new and too easy to misuse for bad actors am I right or are you a mean person]
- [item of interest that is interesting]
- [item of interest that is interesting to consider for prepubescent people]
- [item of interest that is interesting for elderly social members]
- [item of interest that is interesting for warning cops and police officers of weird and socially incongruent events]
- [item of interest that is like you wouldn't have thought about that unless I spelled it out for you]
- [item of interest that is like yea maybe that is interesting but mathematics is difficult would you please spell it out for me instead]
- [item of interest that is like yea, thank you alien pubescent society team squad that takes care of human beings without their acknowledgement]
- [item of interest that is like that would have been interesting to work on but it was such a long time ago that those thoughts were being related to here and so it's like why would you think about that right now?]
- [item of interest that is like you are interesting to write an item of interest list that is so long that it won't be so much practical for your video description list because it would have been possibly set-complete in a particular environment of interest right? right? right?]
- [item of interest that is like that is so interesting]


[Written @ 5:57]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:03:15]

- ✅ [completed] I introduced digital currencies to the README.md audience for the Ecoin README.md file . . [1.0]

[1:03:15 - 1:23:12]

- ✅ [completed] I introduced digital currency exchanges in the Ecoin README.md file . . [1.0]

[1:23:12 - 1:38:38]

- ✅ [completed] I introduced product resource centers in the Ecoin README.md file . . [1.0]

[1:38:38 - 1:49:31]

- ✅ [completed] I introduced network recycling centers in the Ecoin README.md file . . [1.0]

[1:49:31 - 2:07:48]

- 📝 [commentary] I ate food for breakfast 1 boiled egg and a few graham crackers . . and I drank some orange juice

[2:07:48 - 2:57:11]

- ✅ [completed] I introduced notes about digital currencies as a common medium of exchange . . [1.0]

[2:57:11 - 4:11:46]

- 📝 [commentary] I spent some time away from the keyboard . . I wrote notes about network protocols and how items of interest in a photograph such as a pixel area region of the photograph that is a 'dog' or 'cat' or 'person' according to a person who labels the images with rectangles or something . . these '`items in the photograph`' are possibly being able to be interpretted as 'communicating' with one another through a network protocol such that 'you can assume that a person walking around in the park has the message to send to the park area that: 'I could have possibly walked with a cat' or 'I could have possibly walked with a dog'' . . 'I could have done this' or 'I could have done that' is a signal to the artificial intelligence system that 'ahh, yes, I'll be more alarmed or I'll keep an eye out for any animals like 'cats' and 'dogs' because that could be advantageous for finding nearby items of interest like maybe I was originally searching for a 'ball' and so a 'dog' neighborhood next to a 'human' neighborhood . . next to a 'building' neighborhood and so on . . can all share these '`network protocol messages`' to the artificial intelligence system saying that 'oh, yea, look out for that item . . ' or 'oh yea, don't forget about that thing . . ' . . 

[4:11:46 - 5:26:36]

- ✅ [completed] I wrote notes about network recycling centers . . [1.0]

[5:26:36 - 6:24:40]

- ✅ [completed] I wrote notes about a Universal Basic Income . . [1.0]

[6:24:40 - 6:56:26]

- ✅ [completed] I wrote notes about a New Social Economy . . [1.0]


[6:56:26 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - April 21, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%2021%2C%202021



