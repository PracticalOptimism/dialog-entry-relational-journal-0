


# Day Journal Entry - April 4, 2021


[Written @ 23:06]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:13:20]

- 📝 [commentary] I wrote notes on 'the-light-architecture' README.md format and data structure template [1.0]

[3:13:20 - 3:57:45]

- 📝 [commentary] I'm watching Unicole Unicron on YouTube while eating a peanut butter and jelly sandwich . . be right back [2.0]

[3:57:45 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - April 4, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%204%2C%202021

[2.0]
Cam Church - Morality
By Jon Ide
https://www.youtube.com/watch?v=1vGKifj_yJI



[Written @ 1:47]

Notes @ Newly Discovered Concepts

That-that space
[Written @ April 4, 2021 @ 1:47]
["that-that" space is like O _ O . . that is a new concept for me . . but it's like . . "that that" is a type of concept that I've been familiar with for the past history of my life where I've come across rare circumstances where I repeat the same word . . back to back . . in one-after-the-other order . . and it's like O _ O . . "de-ja-vu"? right? . . but it's like O _ O . . when you read the sentence it could seem kind of strange . . depending on your tone . . so it's like O _ O . . "I can't believe that that guy did that thing" . . "that that" . . well . . O _ O . . it's still kind of interesting . . In tone (1) it's like O _ O . . it makes sense . . "that guy" . . In updated tone where you use "that-that" with a hypthenated pronunciation . . like "table-top" . . "road-sign" . . "cloud-air" . . then maybe "that-that" a style of "that thing could possess that other thing" . . "that guy over there can possess the being of "that" . . that that person is a "that" . . since maybe they are "far away" or "distant" or "distanced" from the speaker . . " . . or something like that . . right? . . well . . "right?" . . Well . . I'm not sure . . well . . "that-that" seemed like a good idea like it could help me organize thoughts about "that-that" like spaces where it's like O _ O . . why did that happen like that ? . . O _ O to be honest . . I'm now confused why that-that space makes sense but it's like O _ O . . that could be interesting ]



