


# Day Journal Entry - April 3, 2021

[Written @ 22:33]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:28:59]

- 📝 [commentary] I brushed my teeth to prepare to speak with you here today. I used toothpaste and a tooth brush and some mouth wash. I hope you will receive my message with clean air in your nose.

[0:28:59 - 3:13:31]

- 📝 [commentary] I wrote notes for 'the-light-architecture' [1.0] README.md

[3:13:31 - 3:45:15]

- 📝 [commentary] I wrote notes about 'space' [1.0]

[3:45:15 - 4:16:03]

- 📝 [commentary] I wrote about 'time', 'need to know', 'nothing else #1' and 'nothing else #2' [1.0]

[4:16:03 - 6:04:43]

- 📝 [commentary] I wrote notes about 'an algebra for eny', 'an introduction to you', 'an introduction to proportionality metrics', 'an introduction to areas of interest' [1.0]

[6:04:43 - 7:11:18]

- 📝 [commentary] I wrote notes about 'ideas recommended for you' [1.0]

[7:11:18 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - April 3, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%203%2C%202021




[Written @ 4:56]

Notes @ Newly Learned Words

Giant Voice Hug [1.0 @ 13:21]

[1.0]
the one habit that is changing my life: set systems rather than goals
By Rowena Tsai
https://www.youtube.com/watch?v=WK-sZjuXA6A


[Written @ 4:08]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - ]

- 📝 [commentary] I watched [1.0]

[0:00:00 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Something Went Wrong - Where Have We Been?
By Squirmy and Grubs
https://www.youtube.com/watch?v=fGrslJsQYkc




