


# Day Journal Entry - April 15, 2021

[Written @ 23:28]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:57:18]

- 📝 [commentary] I didn't do anything

[0:57:18 - 4:16:38]

- 📝 [commentary] I wrote notes about things that I've been thinking about for some time period since the Ecoin project started (1) windmills (2) poop disposal systems (3) electromagnetic 'free-energy devices' like searl generators and how they are built like things that get involved in the physics that I know about today which seems to also possibly be relatable to things like (3.1) 'gears' and (3.2) 'pulleys' and 'combinations of gears and pulleys' which are like possible force fields which are differentiated by the force they apply like (3.a) 'you HAVE to do that or else you are going through a tough time to resist me' and (3.b) 'you don't have to do that but you can come along if you want to' which is like possibly why life is interesting with things like chores and interesting ideas like 'is that what I want to do?' 'should I do that?' and 'aliens influencing the world with, yea this is a good gear to ride for a while even if it looks like a pulley for you' . . gears and pulleys . . and so . . existence . . is maybe like a gear right? but I have also heard terms from aliens in my mind saying that it is really a pulley that looks like a gear . .

- 📝 [commentary] The notes on (1) windmill (2) poop disposal system (3) electromagnetics (4) aeroneumatics (5) robotics transportation highway (6) poop measurement system (7) bearings and why they are incredible and why they have built a lot of our modern world with wheeled transportation and they are like gear systems that are electricity maps mapped on structural rigidity elements like metal alloys. In short, maybe you could also imagine that 'bearings' are a basic battery system that is like 'yea, electromagnetic batteries can be transformed to structural dynamic batteries' 'houses' are also like batteries with people being transported to and from houses like electric currents that supercharge a room with potential things to think about. [1.0]

- 📝 [commentary] A complementary topic to 'gears' and 'pulleys' are 'combinations of gears and pulleys' which 'bicycle chains' are a really good example of a 'gear and pulley combination system' Those chains would really kill you if you proportionated them to other aspects of physics or even social dynamics where there's not enough 'give and pull' You would possibly have a whip lashing effect from aliens to calm you down. You would be socially manipulated to relax and not be so barbaric in how you ape through the jungle forest of the world and of course in your mind that could be possible triggers of ape-terroristic devices that would be misnomers to call you a real human being anymore since you would be so primitive and riled with energy to eat delicate rip-wire fruits from jungles of 'people won't like you, right?-gear-pulley-mechanism-thought' right? right? pull along? are you coming along with this idea? do you believe that?

[4:16:38 - 6:59:11]

- 📝 [commentary] I wrote about (1) electron guns (2) electric skate guns (3) electric skate proportionality guns (4) other types of electric systems that are proportional to 1, 2 and 3. and a (5) peace accolade system [1.0]

[6:59:11 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal - Technological Ideas
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas/idea-list




