

# Day Journal Entry - April 5, 2021

[Written @ 21:58]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:11:19]

- 📝 [commentary] I wrote notes on Project References for the README.md for 'the-light-architecture' [1.0]

[1:11:19 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - April 5, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%205%2C%202021





