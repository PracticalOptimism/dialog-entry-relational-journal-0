

# Day Journal Entry - April 16, 2021

[Written @ 22:31]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:32:56]

- 📝 [commentary] I wrote notes about (1) Elementalizer and (2) Personal Feedback 101 [1.0]

[1:32:56 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Dialog Entry Relational Journal 0 / Development Journal - Technological Ideas / Inventions and Ideas
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas




[Written @ 5:23]


(1) Distributed Computing

- The Practice: Uses technologies like IPFS, Dat and Braid Protocol.
- The Principle: A peer-to-peer network distributes control to the network rather than a few monopolies.
- Your Mission: Learn more about decentralized technologies like IPFS, Dat and Hashgraph.

(2) Universal Basic Income

- The Practice: Everyone receives a payment every 60 seconds. 
- The Principle: We are all creative.
- Your Mission: Dream big, dream small. Afford to live through it all.

(3) Open Source

- The Practice: Anyone can rebrand the code base and start their own currency.
- The Princple: Let’s create a world we each individually love.
- Your Mission: Modify the code to your heart’s content.

(4) Anonymized Identity Support

- The Practice: Anyone can have an account.
- The Principle: We are all citizens of a global community.
- Your Mission: Know you belong to a global ecosystem. Live in the future without borders.


[Written @ 1:59]

Notes @ Newly Created Word

Transop
Transoperation
Transoperating
[Written @ April 16, 2021 @ 1:59]
[I accidentally typed the word 'Transop' instead of typing the word 'transportation' . . 'transoperation' is a possible continuation of the word 'transop' which makes me think of a possible medical term language from an alternative probable reality where I've heard the term 'post-op' but 'transop' is still being left in my ego mind to think that I possibly haven't heard that word or that I possibly don't remember because I'm still a transop memory patient learning more about transop . . what does the term mean? what could it mean?]

[Written @ 1:49]

Notes @ Newly Created Word

Transportan
[Written @ April 16, 2021 @ 1:49]
[I accidentally typed the word 'transportation' as 'transportan' . . transportan really looks like a cool word to me . . I thought I would write it down . . it looks very cool . .]


[Written @ 0:36]


Notes @ Newly Created Word

systemaire
[Written @ April 16, 2021 @ 0:40]
[I was writing about the word 'systemare' which is newly created and realized that 'systemaire' was the theoretical probable word which was also around the corner of my interest . . and so . . around the corner of my interest with respect to this typographical error orignated word . . 'systemare']

systemare
[Written @ April 16, 2021 @ 0:37]
[I accidentally typed the word 'systemare' which reminds me of the theoretical (aeroneumatically available at runtime from high vacuum thought provisioning scientists) probable . . theoretical probable word 'systemaire' . . 'systemaire' or 'questionaire' are those types of words wire 'aire' and so they are interesting to think about in a theoretical perspective . . I'm not sure . . I'm sorry . . but at least . . 'systemare' even looks really cool like it could be a real word . . 'systemare' is also now starting to remind me of 'nightmare' which ends with 'are' . . so]
['systems are' was the originally intended words to be typed. I was originally typing for the post published at [1.0]]

[1.0]
dialog-entry-relational-journal-0 / Development Journal - Technological Ideas
By Jon Ide
https://gitlab.com/practicaloptimism/dialog-entry-relational-journal-0

