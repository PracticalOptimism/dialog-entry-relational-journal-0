


# Day Journal Entry - April 8, 2021

[Written @ 23:57]


Key Features:

(1) Distributed Computing

- The Practice: Uses technologies like IPFS, Dat and Braid Protocol.
- The Principle: A peer-to-peer network distributes control to the network rather than a few monopolies.
- Your Mission: Learn more about decentralized technologies like IPFS, Dat and Hashgraph.

(2) Universal Basic Income

- The Practice: Everyone receives a payment every 60 seconds. 
- The Principle: We are all creative.
- Your Mission: Dream big, dream small. Afford to live through it all.

(3) Open Source

- The Practice: Anyone can rebrand the code base and start their own currency.
- The Princple: Let’s create a world we each individually love.
- Your Mission: Modify the code to your heart’s content.

(4) Anonymized Identity Support

- The Practice: Anyone can have an account.
- The Principle: We are all citizens of a global community.
- Your Mission: Know you belong to a global ecosystem. Live in the future without borders.

[Written @ 23:25]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:53:53]

- 📝 [commentary] I wrote notes on 'Project Usecase' for 'the-light-architecture' project README.md file . . [1.0]

- 📝 [commentary] I typed some notes on discovering the 'recommend' syntax for 'recommend-loop' as an alternative to 'for-loop' :O

```javascript

// Syntax Type #1
recommend (activityDescriptionObject; recommendedResultIndex; recommendedResultItem; warningLog) {

  // was that thing illegal or racist or culturally insensitive?
  console.log(warningLog)

  // was that thing indexable so you can show me the order of the result?
  console.log(recommendedResultIndex) // -1 means 'please wait because this might take some time' check warningLog for more information

  // was that thing able to be represented as an object?
  console.log(recommendedResultItem)
}

// Syntax Type #2
const recommendedResultObject = await recommend(activityDescriptionObject)

// Example Usecase #1

recommend ('food to eat at 10 am'; index, item, warning) {
  console.log('recommended food: ', item)
  deliveryService.printFood(item)
}

// Example Usecase #2

await recommend (`print food for me and deliver it to me in 10 minutes; if you don't know my phone number, use 123-1234-1234 to contact me in case you aren't able to fill the order. Also you can contact me about anything else and also expect conversations to occur in case we need to discuss other topics of interest like why the heck is my food not here yet? . . also i'm sorry for barbarizing the human language, just get me my food and make sure you have secret ways to contact me if you need to . . use password '123-hello-world' if you need to contact me . . in a secret code that i will remember . . but also feel free to spy on me and all my neighbors to try to get the cleanest clearest way to contact me that my food has arrived or else just keep pinging me for endless number of days because im still waiting on that food you recommended . . thanks for the food . . sorry for the long message . . onnly recommendation egines that are fabircated with lies and schemes will not be reasonable enough to find me my food in an ordered time delay that doesn't require so much chit chat`)

// Examples Usecase #3

recommend(`send me an email or voice mail if you keep track of other ways to contact me and also if you remember who I am or my name and what things that I normally ask for or whatever if you need me to describe my styles of thinking or whatever`)

// Example Usecase #4

recommend(`send me a video game that I will like`)

// Example Usecase #5

recommend(`send me 10 video games that I will like`)

// Example Usecase #6

recommend(`send me a history of the video games that I've played`)

// Example Usecase #7

recommend(`if you don't know where to print things for me like results about what the recommendation does, just use (1) my 3D printer (2) my television (3) my camera in my bed room (4) my friend tom who always walks over to the recommendation booths in public arenas and gets random messages to deliver to people (5) my desktop computer which is set up to receive random things you send (6) ask permission if you need it by reaching out to my email (hello-world@hello-world.com) or my phone number (123-1234-1234) or walk up to my face at my house address (123 living a life street in new pick me now city)`)

```

[3:53:53 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - April 6, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%204%20-%20April/April%206%2C%202021





[Written @ 22:39]

Notes @ Newly Discovered Thoughts

'recommendation-loop' . . This thought . . was inspired by . . a . . 'for-loop' . . or the 'for-loop' syntax . . 

'for-loop' could look something like this . . depending on your programming language of choice . . 

for (let i = 0; i < 10; i++) {
  // do something here
  // example: console.log('number index: ', i)
}

. . 

a 'recommendation-loop' could look something like this . . 

recommend (something-for-that-topic) {
  //
}

. . 

I'm not exactly sure what should go inside of the curly braces for this . . type of operation . . O _ O . . but if we experiment O _ O or maybe go offline for 10 minutes then maybe a recommendation will come up O _ O . . 

. . 

recommend ('nice hat', i) {
  console.log('a nice hat: ', i)
  break
}

. . 

recommend (`a nice movie that continues this movie: ${movieExample}`, movieItem) {
  console.log('here is a nice movie: ', movieItem)
}


. . 

a nice 'this or that' that 'does this or that'

. . 

a nice 'this or that' that 'imitates a bad version of this or that'

. . 

right?

is that a good syntax ring for a recommendation system?

. . 

a nice 'airplane' that 'doesn't fly but you can kick it and it flies 2 meters up and then dives into the ocean nearby where it can collect some sea shells'

. . 

O _ O

. . 

recommend (syntaxStatement, resultItem, warningLog) {
  if (warningLog.isRacist) {
    continue
  }

  showOnTelevisionComputer(resultItem)
}

. . 

recommend ('a robot that walks', resultItem, warningLog) {
  if (warningLog.hasMoreThan10Legs) { continue }
  robotPrinter.printResult(resultItem)
}

. . 

recommend ('a robot that walks and has less than 10 legs', resultItem, warningLog) {
  robotPrinter.printResult(resultItem)
}

. .

recommend ('a movie about weird people that find a weird treasure from a weird island and everyone in the movie is a hot n-ary-hair-colored woman', resultIndex, resultItem, warningLog) {
  deliver.now(resultIndex)

  // don't 
  if (resultIndex > Infinity) { break }
}

. . 

recommend ({
  textMessage: 'fix the typographical errors in this text and then evaluate the expression',
  itemMessage: '

    recommend ('a food that i will like', recommendedItemIndex, recommendedItem, warningLog) {
      rcmmen ({ textMessage: 'something to print this out in realtime for 10 times in a row, and stop the recommendations after 1, and please make sure that this item actually works and isnt not a good printer :O or else return 0 items and dont tease meeeeee', itemMessage: recommendedItem, numberOfRecommendationsPreferred: 1 }, printResultIndex, printResultItem, warningLog2) {

        let ourLocalPrinter = printResultItem

        // print the food that i like
        ourLocalPrinter(recommendedItem)

        
        // no more than 1
        if (printResultItem > 1) { break 2; }
      }
    }
'
})

. . 

recommend ({
  textMessage: 'print the food that i will like, you know my address, dont you? send me a message on my phone if you dont know -_- heres my number 123-1234-1234. also if you need to be told, allow me to communicate with you to talk about that food order i was talking about . . if you need a secret private password to communicate between you and me, just use hello-world-123 yeaaaaaaaaaa'
})






[Written @ 19:25]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:01:50]

- 📝 [commentary] I didn't do anything . . 

[1:01:50 - End of Video]

- 📝 [commentary] Salutations



