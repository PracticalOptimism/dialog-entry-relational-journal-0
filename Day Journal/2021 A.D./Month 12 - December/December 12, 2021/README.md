



# Day Journal Entry - December 12, 2021



### [Written @ 9:22]




Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 5:35:48]

- 🚧 [work-progress] I worked on showing notifications for when a transaction has stopped due to an error and also updating the way the animated currency label looks [1.0]

[5:35:48 - End of Video]

- 📝 [commentary] I took notes on topics of interest

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin

