


# Day Journal Entry - December 18, 2021


### [Written @ 21:34]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:28:09]

- 🚧 [work-progress] I worked on synchronizing the daemon-1 with the web browser. I used the technique talked about here [1.0]

[2:28:09 - End of Video]

- 📝 [commentary] The live stream shut down for some reason. I had internet connection problems. I'm sorry.

References:

[1.0]
Time zones and offsets
By Luxon
https://moment.github.io/luxon/#/zones



### [Written @ 19:40]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:11:05]

- 🚧 [work-progress] I worked on synchronizing the time with the daemon and the web browser

[1:11:05 - End of Video]

- 📝 [commentary] Salutations






### [Written @ 11:57]



Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - End of Video]

- 🚧 [work-progress] I worked on getting the transaction processing to work on the production daemon [1.0]

- 📝 [commentary] I had internet troubles and the internet disconnected. I'm sorry.

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin






