


# Day Journal Entry - December 14, 2021




### [Written @ 20:41]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:37:24]

- 📝 [commentary] An Introduction to Numbertron [1.0]

[0:37:24 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Dialog Entry Relational Journal 0
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas




### [Written @ 19:13]

"`Why does Ecoin exist?`"

Ecoin is a digital currency that supports a Universal Basic Income.

Ecoin exists to make the lives of entrepreneurs like `Jacque Fresco` and `Roxanne Meadows` better.

`Dream Big. Dream Small. Afford to Live Through it All.`

A Universal Basic Income is a step towards a better social structure. "`The Venus Project`" presents the world a better social structure.

Go do research into "`The Venus Project`" and learn about `Jacque Fresco` and `Roxanne Meadows` and watch the many films (3 or 4 feature films) and videos that they have made on YouTube.

The Fundamental Ideas Are:

(1) Free food for everyone
(2) Free housing for everyone
(3) Free computer laptops and robots and games for everyone

(4) Pay for these things with
(4.1) An educated populace
(4.2) Generous People That Donate Their Time For Free
(4.3) Open Source Software Developers
(4.4) Robotics Engineers
(4.5) Alien Technology - Dialog Entry Relational Journal 0 / Technological Ideas

Things To Avoid Are:

(1) Fighting with other people
(2) 

Education Material:

(1) Libgen
(2) Your fellow person that knows about Ecoin
(3) Dialog Entry Relational Journal 0 / Technological Ideas

Ecoin was live streamed on YouTube for many years.

You can watch those live stream videos for free online here:

https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g/videos

"`What can I buy with Ecoin?`"

Try buying a pizza from your favorite pizza shop nearby.

If you're having trouble finding a place to spend your Ecoin:

(1) Ecoin has a product resource center where you can buy goods and services. Visit the website https://ecoin369.web.app and scroll to the "Product Resources" section to find places to shop.

(2) Other websites on the internet can integrate with the "Ecoin" codebase by using our "JavaScript Library". Search `your favorite e-commerce store` to see if they support Ecoin by looking for the "Ecoin Supported Here" Label or Text at the bottom of the page.

(3) Internet Services that have access to "HTTP" can use the "HTTP" Gateway in their favorite programming language.


""













### [Written @ 18:47]

Question (1):

"`Why does Ecoin exist?`"

Ecoin is a digital currency that supports a Universal Basic Income.

Ecoin started as a project to help game developers earn a living by helping pay their salaries so they can make cool and awesome video games.

Video game developers have a tough time making a living especially since (1) they can experiment and end up making a bad game (2) they don't have a lot of time to make a game from day 0 without an income.

Ecoin gives game developers an income from day 0.

Now let's get serious. Game developers are poor. But who cares. The whole freaking world is poor. Everyone is out of money. No one is rich because everyone is (1) like 'what am I supposed to do with my life?' and (2) 'like what the hell is Jeff Ya-hoo Bezos-Gates doing with all his money?' (3) Do celebrities still matter any more?

Game developers are good people. But their life is quite shit. Their life is (1) take care of the kids (2) take care of the wife (3) think hard about this game feature (4) throw my hands in the air because I gave up on all those important things. (5) And finally, you might even cry because your game wasn't as good as you thought it would be or you wanted more people to join the game studio.

Now let's get even more serious. Games are interesting. But if everyone is really poor. Thanks to Bezos and Gates and the Illuminati and all those Ya-hoos that like throwing money around every which way.

Now let's get more serious.

The answer is not Ecoin. Ecoin is just a gateway to help game developers and then Jon Ide, the developer of ecoin, thought to himself 'hey you know what, it shouldn't be called 'game-coin' it should be called 'e-coin' like that one show 'Mr. Robot' that shows a digital currency' A digital currency.

A digital currency for everyone.

E-Coin.

(1) All the branding of the Ecoin project is stolen from the show "Mr. Robot" which is quite iconic. Good job team on making that work. And as "team" I mean the team that made the television show. Go promote their work. Buy merchandise from them. I'm sure they will like it. And hopefully they don't throw me in jail for stealing that sci-fi science fiction branding from their 'fake' world.

(2) But Ecoin isn't the answer to the world's problems.

(3) "The Venus Project" is a way better answer that you should do more research.

The Venus Project. That is a great resource for you to search for.

Unicult. That is another great resource for you to do research for.

The Tree Sisters. That is another great resource for you to do research for.

"`What is The Venus Project?`"

(1) Roxanne Meadows might have a different way of describing this project than, I, myself, Jon Ide who published this project to the internet.

(2) Watch the "Ecoin" Live stream videos on YouTube for Free: https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g/videos

(3) The Venus Project applies science and technology to build a social structure that (1) uses no money (2) people volunteer their work (3) like how open source software developers donate all their time for free, in a lot of cases, to do excellent work (4) Go donate your engineering and math and science skills to The Venus Project (5) Build cities that feed and house everyone for free without the use of money

"`What is Unicult?`"

(1) Unicole Unicron is a pop star cult leader who leads the Cult called "UNICULT"

(2) She wants to make the world a better place.

(3) Go join UNICULT.


"`How can I accept Ecoin payments on my personal or business website?`"

(1) 







### [Written @ 17:42 - 18:06]

EL[2, 1] = 

ELEL1 = 

EL(E[1]) = 

EL(10) =

E[1, 1, 1, 1, 1, 1, 1, 1, 1, 1] =

.....................

E[1] = E1 = 10
E[1, 1] = EE[1] = EE1 = E10 = 10,000,000,000
E[1, 1, 1] = E[E[1, 1], 1] = E[10,000,000,000, 1] = EE...EEE1 = (10 million E's followed by a 1)

....................

E[1, 1, 1, 1] = E[E[1, 1, 1], 1, 1]

....................

E[2, 1, 1, 1] = E[E[E[1, 1, 1], 1, 1], 1, 1]

....................

E[3, 1, 1, 1] = E[E[E[E[1, 1, 1], 1, 1], 1, 1], 1, 1]

....................

E[3, 2, 1, 1] = E[E[E[E[2, 1, 1], 1, 1], 1, 1], 1, 1]

....................

E[3, 2, 4, 5] = E[E[E[E[2, 4, 5], 4, 5]], 4, 5]

....................

Remember:

E is the x 10 ^ N

SO

EN is . . . like . . 10 ^ N

So 

E100 is . . 10 ^ 100

And of course

EE100 is 10 ^ (10 ^ 100)

....................

EEE is 10 ^ (10 ^ (10 ^ 100))

....................

Remember that

(1) Google's YouTube has X amount of petabytes in their data stores. It doesn't really matter but I did the math and it seems to be less than E20 number of bits . . 

(2) E20 number of bits . . that's so small

(3) Think about the size of (1) EL[2, 1] . . Do you remember that L means 'length'

EL is like . . E and then . . the length of the array that follows EL . . so for example . . that array that gives us those calculations from above . . 

EL3 . . That is . . E[1, 1, 1]

EL4 . . that is . . E[1, 1, 1, 1]

.....................

Anyway . . 

ELELELELEL1

Has the same notation

EL[4, 1]

. . that means there are 4 'EL's After the first EL . . so for example

'EL' and then followed by 'EL' 'EL' 'EL' 'EL'

......................

EL[4, 1] = ELELELELEL1

......................

-_-

......................

ELLL is different from EL

......................

ELLLLLLLLLLL is different from EL and ELL and ELLL

......................

Basically you can add more and more letters of "L" that make the number larger and larger

......................

And then of course you can invent new letters or new characters

......................


























### [Written @ 17:07]


Use Decimal.js For The Following Values:

(1) Digital Currency Amount
(2) Transaction Currency Amount
(3) Transaction Currency Amount For Account
(4) Transaction Order Index




### [Written @ 10:02]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 7:18:05]

- 🚧 [work-progress] I worked on updating the animated currency label [1.0]

[7:18:05 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin







