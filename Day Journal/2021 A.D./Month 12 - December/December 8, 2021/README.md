


# Day Journal Entry - December 8, 2021


### [Written @ 21:47]

Finished Live Streaming.

Maybe ~ 9 hours in total of live streaming today.


### [Written @ 16:04]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 5:20:36]

- 🚧 [work-progress] I worked on the account and transaction statistics [1.0]

[5:20:36 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin


### [Written @ 14:43]

Notes @ Newly Created Words

[1.0]
optoptionsion
[Written @ December 8, 2021 @ 14:44]
[I copy-pasted the word 'options' into the word 'option' by accident. The word 'optoptionsion' was what resulted. it was just an accident.]



### [Written @ 12:28]

[OBS Stopped]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:58:27]

- 🚧 [work-progress] I worked on adding event listener for accounts and transactions for the Ecoin network. [1.0]

[0:58:27 - 1:23:45]

- 😴 [break-time] I took a break

[1:23:45 - 2:42:00]

- 🚧 [work-progress] I worked on adding event listener for accounts and transactions for the Ecoin network. [1.0]

[2:42:00 - End of Video]

- 📝 [commentary] The video ended abruptly. I didn't realize the live stream disconnected. I'm sorry.

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin



### [Written @ 11:27]

[OBS Stopped]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:59:29]

- 🚧 [work-progress] I worked on updating the add event listener codebase for Ecoin for accounts and or transactions [1.0]

[0:59:29 - End of Video]

- 📝 [commentary] The video ended abruptly. I didn't realize the live stream disconnected. I'm sorry.

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin





### [Written @ 9:37]

Question: "How many days do you think there are until we have a released version of Ecoin?"

Answer: 100.

Question: "We are on Ecoin episode #739. How many more episodes do you believe there are until a released version of Ecoin?"

Answer: 100.

......................

Prediction.

Ecoin #839.

That should be one of our latest versions of the Ecoin live streams that has a ready to go version of the codebase.

......................

Plan:

Transactions. [December 2021] (30 days)

Product Resources. [January 2021] (30 days)

Job Employment. [February 2021] (30 days)

Currency Exchange. [March 2021] (30 days)

Digital Currency Alternative. [April 2021] (30 days)

Security. [May 2021] (30 days)

JavaScript API. [June 2021] (30 days)

.......................

30 days x 7 = 210 days

.......................





### [Written @ 9:35]

[No Internet Access]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:16:00]

- 🚧 [work-progress] I worked on showing account and transaction updates for statistics [1.0]

[1:16:00 - End of Video]

- 📝 [commentary] The video ended abruptly. I didn't realize the live stream disconnected. I'm sorry.

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin



