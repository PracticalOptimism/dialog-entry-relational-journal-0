

# Day Journal Entry - March 20, 2021

[Written @ 23:50]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:36:16]

- 📝 [commentary] I watched [1.0] [2.0]

[0:36:16 - 1:43:10]

- 📝 [commentary] I wrote notes in the development journal entry for March 21, 2021 [3.0]

[1:43:10 - 2:04:35]

- 📝 [commentary] 

[2:04:35 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
getting boozy on a Tuesday with my siblings *vlog*
By Claudia Walsh
https://www.youtube.com/watch?v=KbWqy3y-7zE

[2.0]
Day In The Life w/ My Sister!
By Claudia Walsh
https://www.youtube.com/watch?v=figAI4SOIkk

[3.0]
Development Journal Entry / March 21, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%203%20-%20March/March%2020%2C%202021


