

# Day Journal Entry - March 16, 2021

[Written @ 19:15]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:23:38]

- 📝 [commentary] I watched [1.0]

[0:23:38 - 1:44:19]

- 📝 [commentary] I watched [2.0]

[1:44:19 - 5:28:02]

- 📝 [commentary] I worked on problem #1 presented at [2.0 @ 47:57 - 51:22]

[5:28:02 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Bright Horizons
By Charles Hoskinson
https://www.youtube.com/watch?v=T7Da6sFAxuI

[2.0]
Hedera Community Town Hall | March 2021
By Hedera Hashgraph
https://www.youtube.com/watch?v=gY_Odc5STpc


[Written @ 18:24]

Notes @ Newly Discovered Quotes

"
Was the walking dead . . a documentary . . rather than . . a fantasy . . ? . . No . . It didn't . . come . . to . . pass . . 
"
[1.0 @ 5:47 - 5:53]


Notes @ Newly Remembered Word

Regelia [1.0 @ 2:40 - 2:43]

[1.0]
Bright Horizons
By Charles Hoskinson
https://www.youtube.com/watch?v=T7Da6sFAxuI

[Written @ 2:16]

Notes @ Inspiring Thoughts Inspired By [1.1 @ 1:44 - 1:51]

"Tailors who make clothes could also be 'senior' engineers in a sense."

Notes @ Newly Discovered Quotes

"
It's time . . to . . shine . . my senior engineers . . oh . . not . . senior . . engineers . . 

But . . like . . uhm . . seniors . . who . . happen . . to be . . engineers . . 
"
[1.1 @ 1:44 - 1:51]


[1.1]
Why Microsoft Word is the best IDE for programming
By Joma Tech
https://www.youtube.com/watch?v=X34ZmkeZDos&ab_channel=JomaTech


[Written @ 1:17]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:53:31]

- 📝 [commentary] I wrote notes [1.0]

[0:53:31 - 1:00:48]

- 📝 [commentary] I watched [2.0] on YouTube

[1:00:48 - 2:10:43]

- 📝 [commentary] I wrote notes [1.0]

[2:10:43 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / March 16, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%203%20-%20March/March%2016%2C%202021

[2.0]
Why Microsoft Word is the best IDE for programming
By Joma Tech
https://www.youtube.com/watch?v=X34ZmkeZDos&ab_channel=JomaTech



[Written @ 0:51]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:07:10]

- 📝 [commentary] I watched [1.0] [2.0] [3.0] on YouTube

[1:07:10 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Shane Reported For Bullying?
By Squirmy and Grubs
https://www.youtube.com/watch?v=h8ensQTI-tk

[2.0]
We Are Celebrating Today!
By Squirmy and Grubs
https://www.youtube.com/watch?v=DhqLUvgZvBQ

[3.0]
Reacting To Our Sexy Photoshoot Pictures
By Squirmy and Grubs
https://www.youtube.com/watch?v=x9nGIDV-P74



