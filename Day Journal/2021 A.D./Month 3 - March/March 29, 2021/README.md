

# Day Journal Entry - March 29, 2021


[Written @ 23:03]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:45:00]

- 📝 [commentary] I wrote notes on a style of project architecture that uses (1) 'game-character' (2) 'game-object' (3) 'game-object-action' for some of the main things to think about [1.0]

[0:45:00 - 2:36:11]

- 📝 [commentary] I wrote notes on an introduction to a project file-system data structure that's like 'woah, still experimental research, sorry about that' [1.0]

[2:36:11 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Notebook For Course Development Journal / Anna / Day #4
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Notebook%20For%20Course%20Development%20Journal/Notebook%20Courses/How%20To%20Make%20A%20Computer%20Program/Course%20Development%20Log%20%231/Course%20Development%20For%20Retarded%20People/A%20Weekend%20Project%20To%20Work%20On/A%20Project%20For%20Myself%20The%20Author%20Writing%20This%20Book%20Series/Day%20%234%20-%20March%2029%2C%202021



[Written @ 20:43]



