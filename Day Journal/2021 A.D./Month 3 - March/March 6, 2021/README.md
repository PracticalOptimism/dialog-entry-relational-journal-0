
# Day Journal Entry - March 6, 2021

[Written @ 22:54]

Notes @ Newly Created Words

fexam
[Written @ March 6, 2021 @ 22:53]
[I accidentally typed the word . . "fexam" . . instead of . . "for example" . . I'm sorry]

[Written @ 20:10]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:34:40]

- 📝 [commentary] I didn't do anything . . 

[0:34:40 - 1:41:29]

- 📝 [commentary] I wrote notes in the Development Journal Entry for March 6, 2021 [1.0]

[1:41:29 - 4:00:15]

- 📝 [commentary] I wrote notes in the Dream Journal Entry for March 6, 2021 [2.0]

[4:00:15 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / March 6, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%203%20-%20March/March%206%2C%202021

[2.0]
Dream Journal Entry / March 6, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Dream%20Journal/2021%20A.D./Month%203%20-%20March/March%206%2C%202021


[Written @ 19:44]

[Copy-Pasted From Development Journal Entry / March 6, 2021]

Now, Now, Now
A Poem By Jon Ide

Where Did Now Come From?
Where Did Now Go?
Where Is Nowing Heading To?
How Did Now, Know?

Now looks like itself but all of time is itself an instance of Now
Now looks like itself but you have to agree to call it that

If you look with open eyes to the now
You will see with closed eyes to the now
The now will look like me if you look through me
The now will look like you if you look through you
You have to close my eyes to let you do you
You have to open my eyes to let



