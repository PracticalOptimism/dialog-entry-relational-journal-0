


# Day Journal Entry - March 19, 2021


[Written @ 22:32]

Notes @ Newly Discovered Books

The Self-Sufficient Life and How To Live It [1.0 @ 2:47 - 2:50]


[1.0]
How to Break Free from Society : Communes, Money, Building the new world!
By Unicole Unicron
https://www.youtube.com/watch?v=dO8WQz-RAZk




[Written @ 22:22]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:23:35]

- 📝 [commentary] I didn't do anything . . I'm sorry

[1:23:35 - 2:14:32]

- 📝 [commentary] I watched [1.0]

[2:14:32 - 3:05:57]

- 📝 [commentary] I typed a template representation for the README.md for 'the-light-architecture' codebase [2.0]

[3:05:57 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
How to Break Free from Society : Communes, Money, Building the new world!
By Unicole Unicron
https://www.youtube.com/watch?v=dO8WQz-RAZk

[2.0]
The Light Architecture
By E Corp
https://gitlab.com/ecorp-org/the-light-architecture


