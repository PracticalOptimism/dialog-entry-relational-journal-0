

# Day Journal Entry - March 25, 2021

[Written @ 19:13]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:05:21]

- 📝 [commentary] I took notes on a style of program development that is like 'interesting' and so it's like . . 'hmm . . I think that would work' . . but it's like . . you'll let yourself look like you haven't known another way of programming after 4 or 5 steps in the program . . or maybe 6 or 7 but I'm not really sure . . the program is like . . :

(1) yes, I agree there is a problem

(2) yes, I agree there is a solution

(3) yes, the solution is like
  - (3.1) do some research
  - (3.2) ask a question about your research topic
  - (3.3) ask a question about the dilemma in question
  - (3.4) ask a question about the proportionality measuring spama which is like a dialectic diagraph of 'wow, I seriously have to consider a new approach that is at least seriously considerable in areas of interest proportions no greater than many magnitudes or less' and then you have to speak in a toned language that supports that type of creativity characteristic measure which is like javascript or any of the other of myriads of programming languages that are very useful or popular in the culture of interest :O.

(4) ask a question like 'did I solve this problem before or do I have it in cache memory which is orders of magnitude more considerable than the previous aspect dilemma because of inconcernability crisis problems like 'is it really seriously something that can be replicated once or twice or more than once or twice when number spaces are less likely to occur in certain spans of thoughts<?>' Well that it is considerable to say that 'yes, cache memory is easier and better said in a programming language like javascript where the memory model is perhaps something like 'yea, I can agree that caching that is timely and effective on various orders of magnitudes worthy enough for the cache to make sense for variable delay efficiency try-catch-block statements''

(5) Do you agree on the enemy's statistics for offending you? If you do then you better say that you have no better tactic to defend yourself in protecting your codebase's affirmation criteria

(6) And now you are really out of luck because of your enemy and their discourse to stop you and stop you and stop you and there is hardly any delay in between the stops and so you're like 'well, I really can't get through to the proper solution now, can I? Well I really wish I had a god to assist this god hopping strategy of 'wow, I would really like to complete my video game statistic wouldn't I?'' and finally

(7) [Seven, ] which is really like infamy because you're going to get in a lot of 'eximana' trouble for what you've done


- 📝 [commentary] Notes are available at [1.0]

[3:05:21 - 6:09:28]

- 📝 [commentary] I wrote notes on things like 'yea, that seems interesting but it's like, can I still work on that one program again?, well the answer is no not really because I need to talk about things like 'have I already solved that problem?'' [1.0]

[6:09:28 - 6:54:14]

- 📝 [commentary] I started working on a ping pong game [1.0]

[6:54:14 - 8:26:29]

- 📝 [commentary] I continued writing notes for the ping pong game that I'd like to write to help construct a nice 'yo, let's make a game' tutorial for people that are like 'yo, I need to make a game or something' and so it's like 'yo, is your idea proportional to making a game? if it is, then pretty much we've got you covered but it's like, yo, tell us what you know about game development. yo' [1.0]

[8:26:29 - 9:22:12]

- 📝 [commentary] I had a conversation with a text-editor simulation of myself and they said things about text-editors, that's so cool. Now I have an idea of what a video game is like :D

[9:22:12 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Notebook For Course Development Journal / A Project For Myself The Author Writing This Book Series
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Notebook%20For%20Course%20Development%20Journal/Notebook%20Courses/How%20To%20Make%20A%20Computer%20Program/Course%20Development%20Log%20%231/Course%20Development%20For%20Retarded%20People/A%20Weekend%20Project%20To%20Work%20On/A%20Project%20For%20Myself%20The%20Author%20Writing%20This%20Book%20Series



