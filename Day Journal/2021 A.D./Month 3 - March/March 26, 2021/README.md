


# Day Journal Entry - March 26, 2021

[Written @ 5:16]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 3:05:21]

- 📝 [commentary] I took notes on a style of program development that is like 'interesting' and so it's like . . 'hmm . . I think that would work' . . but it's like . . you'll let yourself look like you haven't known another way of programming after 4 or 5 steps in the program . . or maybe 6 or 7 but I'm not really sure . . the program is like . . : [1.1 and supplementary notes at [2.0]]

- 📝 [commentary] Notes are available at [1.0]

[3:05:21 - 6:09:28]

- 📝 [commentary] I wrote notes on things like 'yea, that seems interesting but it's like, can I still work on that one program again?, well the answer is no not really because I need to talk about things like 'have I already solved that problem?'' [1.0]

[6:09:28 - 6:54:14]

- 📝 [commentary] I started working on a ping pong game [1.0]

[6:54:14 - 8:26:29]

- 📝 [commentary] I continued writing notes for the ping pong game that I'd like to write to help construct a nice 'yo, let's make a game' tutorial for people that are like 'yo, I need to make a game or something' and so it's like 'yo, is your idea proportional to making a game? if it is, then pretty much we've got you covered but it's like, yo, tell us what you know about game development. yo' [1.0]

[8:26:29 - 9:22:12]

- 📝 [commentary] I had a conversation with a text-editor simulation of myself and they said things about text-editors, that's so cool. Now I have an idea of what a video game is like :D

[9:22:12 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Notebook For Course Development Journal / A Project For Myself The Author Writing This Book Series
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Notebook%20For%20Course%20Development%20Journal/Notebook%20Courses/How%20To%20Make%20A%20Computer%20Program/Course%20Development%20Log%20%231/Course%20Development%20For%20Retarded%20People/A%20Weekend%20Project%20To%20Work%20On/A%20Project%20For%20Myself%20The%20Author%20Writing%20This%20Book%20Series

[2.0]
Day Journal Entry  / March 26, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%203%20-%20March/March%2025%2C%202021

Additional Notes:

[1.1]
This comment is left-field edited to comply with the YouTube video description length guidance of requiring less than 5000 characters. Please visit the reference link for more information for the respective location for a continued description.



[Written @ 4:04]

Woah


