

# Day Journal Entry - November 8, 2021

### [Written @ 10:33]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 3:17:36]

- 🚧 [work-progress] I spent many hours trying to figure out (1) which provider exists that provides free node.js application hosting without cost for a good period. Back4App seemed like a good solution but then I realized it failed since my Firebase code wasn't able to work as expected. A lot of other providers like Amazon Web Services required a credit card or my phone number which I'm an anonymous person on the internet and am not interested in sharing my real phone number with anyone. I would very much like to stay anonymous. My full of heart project which is hopefully good for the world to receive is meant to be good for the world and good for people to receive. It's not meant to be negative or nasty or bad. I don't want people to remember Jon Ide as a monster. Jon Ide was a random stranger on the internet trying to help in the way he could. Services like Linode, Amazon Web Services, Google Cloud Platform, Microsoft Azure, simply made things difficult by requiring too many configuration steps or by simply requiring too much personal information.

[3:17:36 - End of Video]

- 📝 [commentary] Salutations



