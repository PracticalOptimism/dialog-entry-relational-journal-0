


# Day Journal Entry - November 10, 2021


### [Written @ 9:32]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 1:10:45]

- 🚧 [work-progress] I configured start-daemon-1.js and daemon-1.js as two separate files. [1.0]

- 🚧 [work-progress] I configured Back4App to have a Cron Job or Job for starting the daemon. [1.0]

[1:10:45 - 7:16:12]

- 💡 [initialized] I initialized transaction expectation definitions / unit tests [1.0]

[7:16:12 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin





### [Written @ 9:32]

Notes @ Newly Discovered Videos

[1.0]
Are you ready to hear the song of the couple’s???come and join us
By NARRIEKIM
https://www.youtube.com/watch?v=gdg-Q6oV38I

[2.0]
October 2021 Wrap Up | 25 Books
By Bookish Realm
https://www.youtube.com/watch?v=D__Tf0m1Raw

[3.0]
The Goal and our Journey by Uma Mullapudi_07-11-21
By ELEGANT HEARTS
https://www.youtube.com/watch?v=1Vbo3KNPLlQ

[4.0]
How This Kenyan Woman Built 200 Homes In Kenya?
By WODE MAYA
https://www.youtube.com/watch?v=TPeLQigTsFM

[5.0]
Nori Carbon Removal Marketplace - CEO Paul Gambill In Hitechies Podcast
By Hitechies Your Startup Tech Insights
https://www.youtube.com/watch?v=4WmtdPoKy_I







