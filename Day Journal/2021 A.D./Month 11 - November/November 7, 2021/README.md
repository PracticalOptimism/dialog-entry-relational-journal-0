

# Day Journal Entry - November 7, 2021


### [Written @ 14:10]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:36:40]

- 🚧 [work-progress] I worked on trying to synchronize the universal basic income time from the daemon with the time from the local web browser environment.

- 🚧 [work-progress] I ran into some problems when trying to publish data to the firebase realtime database using the firebase-admin package. I was trying to publish data on the Back4App platform.

[2:36:40 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin

