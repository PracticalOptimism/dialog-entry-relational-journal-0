

# Day Journal Entry - November 6, 2021

### [Written @ 9:29]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 5:52:06]

- 🚧 [work-progress] I did a lot of research into places where I can publish a daemon for processing the Ecoin Network's Transactions. Back4App (https://www.back4app.com/) was the result that I concluded on that was one of the easy to use solutions that seemed to work. [1.0]

[5:52:06 - 7:13:38]

- 🚧 [work-progress] I worked on synchronizing the time between the daemon and the web browser client [1.0]

[7:13:38 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin






