


# Day Journal Entry - November 28, 2021


### [Written @ 14:10]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:33:08]

- 🚧 [work-progress] I struggled to understand (1) should I simulate the idea of (1) when the currency amount for an account changes, track down the transaction id that created that change in the currency amount. (2) If you do simulate the tracking down of the transaction id on the client side without sending messages to the Firebase database then that would be quite efficient. But I couldn't overcome the idea of (1) requesting the Firebase database for a list of the transactions for each account just to see which transaction produced which difference in the account currency amount. I'm sorry. Showing the transaction id is very epic but I think I missed out on trying to work out how to complete this feature without making so many transaction requests. The number of transactions on the network is expected to be large. I don't necessarily want to program this feature at this time. Maybe in the future. It is quite neat to see the transaction id of which difference in the account currency amount is there. Thank you for reading. [1.0]

[2:33:08 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin



