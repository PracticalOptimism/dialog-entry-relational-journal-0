

# Day Journal Entry - November 4, 2021

### [Written @ 9:45]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 7:52:16]

- 🚧 [work-progress] I worked on showing the latest transaction notification (1 New Transaction Received)

- 🚧 [work-progress] I worked on showing updates to the account currency amount using a difference value from the old currency amount to the new currency amount (+1)

- 💡 [initialized] I started working on showing notifications for when a transaction is received (New Transaction Received From 'Account Name' of 10 Digital Currency Amount)

[7:52:16 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin


