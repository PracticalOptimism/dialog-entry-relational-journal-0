

# Day Journal Entry - November 12, 2021


### [Written @ 9:56]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 6:07:06]

- 🚧 [work-progress] I worked on expectation definitions. I created digital currency accounts and digital currency transactions using the current working codebase. Next I plan to write expectation definitions for how the transaction processing should work. [1.0]

[6:07:06 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin



### [Written @ 9:55]

Notes @ Newly Discovered Videos


[1.0]
NYC LIVE Exploring Statue of Liberty to World Trade Center (November 10, 2021)
By ActionKid
https://www.youtube.com/watch?v=U1Hp8fP411I




