

# Day Journal Entry - February 7, 2021

[Written @ 23:03]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:59:44]

- 📝 [commentary] I took notes about my thoughts on "nature" [2.0] and "ecoin" [3.0] the two libraries that I'm working on at this time . . but . . I have been really not sure which to work on . . and so . . I suppose . . "nature" could help make "ecoin" nice . . uh . . but also . . "nature" . . could be useful for other things . . uh . . which would make ecoin fun to work on . . because you also have other information involved . . [1.0]

[0:59:44 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - February 7, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%202%20-%20February/February%207%2C%202021

[2.0]
Nature
By E Corp
https://gitlab.com/ecorp-org/nature

[3.0]
Ecoin
By E Corp
https://gitlab.com/ecorp-org/ecoin


