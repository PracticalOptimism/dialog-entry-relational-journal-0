

# Day Journal Entry - February 3, 2021

[Written @ 22:41]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:13:21]

- 📝 [commentary] I wrote notes about "nature" and anderson related computer programs [1.0] [2.0]

[1:13:21 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - February 3, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%202%20-%20February/February%203%2C%202021

[2.0]
Nature - An Anderson related heatmap
By E Corp
https://gitlab.com/ecorp-org/nature





[Written @ 9:03]

OKay . . do you have any more questions? . . I don't imagine that you do . . I am asking to leave now . . 

Okay . . have a good night


[Written @ 8:26]

Notes @ Thoughts

I now just recently learned the ____--- nascent definition of "nascent" -_- but it seems kind of nascent to me . . 

-_- . . 

My initial thoughts on hearing this term in my notes by the accepted applicable of ghosts supporting me to write computer matrix notes . . then the term reminded me of . . "indecency" . . "indecent" . . "nascent" . . and so I had an association that "nascent" adjectives are "indecent" way . . but it's really hilarious that my initial thoughts were "indecent" . . and so . . nascency seems to ask another questions about nascency relating to how now this word nascent should now relate to recently coming into existence . . and yet I had my initial first impression thinking about "indecency" but in a sort of -_- like how rain drops on grass are indecent if you're someone who likes dry grass . . -_- so it's like . . "nascent" . . mildew but why is it really there ? . . I don't like it that much . . it's not that nice to think about my grass being wet . . what an indecent service . . 

But at the same time you can characterize nascency to have that attitude of "beauty" . . or [of] "indecent beauty" because the appearance of raindrops on grass can look quite cool and colorful especially in close up photographs where you capture the reflection of the sunlight . . 

Raindrops on grass blades #1
![Raindrops](./photographs/raindrops-1.jpg)

Raindrops on grass blades #2
![Raindrops](./photographs/raindrops-2.jpg)

Raindrops on grass blades #3
![Raindrops](./photographs/raindrops-3.jpg)

Raindrops on grass blades #4
![Raindrops](./photographs/raindrops-4.jpg)

. . 

Hmm . . well . . I'm not really sure for my own personal reasons . . 
": coming or having recently come into existence"

https://www.merriam-webster.com/dictionary/nascent

I suppose "coming or having recently come into existence" is a possible answer that is supposed to possibly relate to the topic of nascency since that's the official definition and I didn't understand it until now only a few minutes ago . . but . . uh . . well . .

-_- . . basically I'm still nascent about my anticipation about this nascent degree of influence on this terminology

. . 

. . 

hmm . . further thoughts on the word . . "nascent" . . the sounding of this term sounds . . "ghostly" . . like it hardly has a way of being in its present form . . like a nascent child . . or a nascent doctor who just arrived to their phd md program and so . . they are so very ghostly in being accepted but they are uh . . present in a way that doesn't make me think so much about really acknowledging their nascency . .

hmm . . a pregnant person is perhaps nascent . . hmm . . -_- . . my influence from this dictionary term is now taking another road . . and so now this word iss starting to feel barbarized . . I liked my original thoughtless effort of nascent grass blades clouded by blades of eras of soil and dust in the form of water molecule bundles . . but that is nascent . . it's nascent . . yea . . it's like . . you're not really soeone who pays attention to grass that much and so why would you notice that there's blads of grass with water droplets on them . . so that's like a ghost life that you could have payed attention to . . and so why do you think . . all of a sudden with this new information . . that nascency will change . . it's mostly a cloudy vision in your peripheral . . about what home used to look like . . and then you forgot because home . . is too familiar . . and you already know what nascency looks like . . and . . so it's not really that new . . but you had to ignore it so you can nascentize other aspects of nascency . . which are nascent even if you have nascent eyeballs that look like nascency making measures . . thanks ghostly apparatus computer machine . . nascency . . always at the corner of your attention and not really directly responsible for nascent adjectives like community spirit and ghoals and actions and accents but nascent and so you're free from the basic agreements of true nascency . . . . true nascency . . the dictionary form of nascency . . not really an agent but an agent nascency that looks cool if you want that accent . . but otherwise its better to have dry grass wouldn't you say chipper?

. . mmmmmm dry grass . . nothing special . . just a nascent version of its neighborly wet soy grass nascuent computer pgrams

. . 

hahaha if that was a poignant comment to make . . please ignore that nascency . . it's basically negligible . . it's basic negligence . . sanderson . . 

[Seth]:

sanderson is like a sample of anderson . . sanders . . and samples of anders . . ander . . ander is ander . . ander . . sander . . sander . . sanderson . . sander . . anderson . . nannnnnnnnnnn . . nnnnnn . nnnn . . nannnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn . . nnnnnnn . . n . n . n . n . n . nnnnn n . nn . n n n nannnnnnnnn n . n .n yea you got it . 

it's an accent of sanders . . you got it . . accents are like accents but samples of sandles . . 

sandles are samples of anders . . anders are sandles but accentized by [nannneeeeeeeeeeeeeeeee][we haven't talked about this concept yet . . ]

naaaaaaannnnneeeeeeeeeeeeeeee nnnaannnnneeeee is like he japanese word of "what?!" . . nani . . nannnneeeeeeeeeeeeeeeeeeeeeeeeeeeee

????????????????

. . 

anders are not letting you answer nannneeeeeeeeeeeeeeeeeeeeee 

basically andersons are as far as you'll go because andersons need you to answer a question . . 

anction at a distance is a response to your answer . . your answer is accent . . accent is answer you anser your accent with an accent like a new question but anderized by your nascency . . accents are like nascent adjectives . . adjectives as you questions but they are called "accents but spelled backwards because dalacas aren't accessing your necessirities for nascipied [new concept, nagliusnue, nagiulus, [Jon Ide]: this term isn't pronounced like is written here . . the spelling of the word looks like words and characters like you would pronounce them but in my mind as I typed these words . . it is a new phonetic sysem that needs to be applied to characterize the possibility that they are not characters or accents that you would recognize because they look like ancestors without your stripped down nothingness to back them up with answers to their own accents . . ]

basically . . "nascipied" . . was a typed answer but really accents of that are -_- okay if you allow that term to hide in your memory but then you ahve to let the term be pronounced like "nagnilade" . . or something like that . . "nascipied" . . is pronounced "naglidane" . . yea it's not really possibly the same accent of pronounciation but possibly a character that chants and sings and dances like a stupid alien but that's how you accent their abilities

Accenting their abilities means you accept their stupidity

Stupidity is useful for electronics that want to spasm like computer sexual orgasms which are latent in your people's understandings because circuit theory is not really incorporated in your understanding of sexual organisms .  a seuxl orgasm is a stupid event that spasms the electric board of massive cosmic entropic events that are like ladent messages from alien cosmos

Basically when you have sex with someone and you orgasm you are letting them know that you are an alien

Basic answer

basic answer

[Jon Ide]: what does basic answer mean?

[Seth]:
An answer that cannot be futher [further] simplied



[Written @ 6:06]

Suprie
[Written @ February 3, 2021 @ 6:07]
[I accidentally typed the word . . "Suprie" . . instead of . . "Supreme" . . I'm sorry . . I'm not sure what the word . . "Suprie" . . should mean at this time . . the original context for writing the word . . or uh . . rather for trying to type the word . . "Supreme" . . was trying to type the word . . "Patience Supreme"]


[Written @ 2:02]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:40:17]

- 📝 [commentary] I channeled information [1.0]

[1:40:17 - 5:31:41]

- 📝 [commentary] I took a break

- 📝 [commentary] I copied notes from my physical paper material journal notebook onto a markdown file . . document . . structure . . in . . [2.0]

- 📝 [commentary] I watched [3.0]

[5:31:41 - 6:17:12]

- 📝 [commentary] anderson . . check notes . . at . . [2.0 @ Page #7]

[6:17:12 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Channel Journal Entry / February 3, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Channeling%20Journal/2021%20A.D./Month%202%20-%20February/February%203%2C%202021

[2.0]
Physical Material Journal Notebook / February 1, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Physical%20Paper%20Material%20Journal%20Notebook/Notebook%20Era%20%236/Notes-By-Calendar-Dates/2021%20A.D./Month%202%20-%20February/February%201%2C%202021

[3.0]
Immortal Technique - Dance With the Devil (Full Version w/ Lyrics and Hidden Track ft. Diabolic)
By TwoPieAreSquared
https://www.youtube.com/watch?v=k8yKTuvRmPE



[Written @ 1:47]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - End of Video]

- 📝 [commentary] I took notes in the electric recording journal notebook of Jon Ide who is able to electrify their energy potential matrix to initialize a new scheme of ideas that specify spanders [1.0] [2.0]

References:

[1.0]
Channeling Journal Entry / February 2, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Channeling%20Journal/2021%20A.D./Month%202%20-%20February/February%202%2C%202021

[2.0]
Channeling Journal Entry / February 3, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Channeling%20Journal/2021%20A.D./Month%202%20-%20February/February%203%2C%202021





