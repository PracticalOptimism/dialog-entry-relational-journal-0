


# Day Journal Entry - February 28, 2021

[Written @ 9:39]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:12:20]

- 📝 [commentary] I introduced the topic of aya [1.0]

[1:12:20 - 1:58:19]

- 📝 [commentary] I took notes in the development journal notebook [1.0 @ 10:51, 11:21]

[1:58:19 - 7:48:05]

- 📝 [commentary] I transcribed physical paper material journal notebook notes to the notebook era #4 [2.0]

[7:48:05 - 9:02:55]

- 📝 [commentary] I'm taking a break [3.0]

[9:02:55 - End of Video]

- 📝 [commentary] Salutions

References:

[1.0]
Development Journal Entry / February 28, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%202%20-%20February/February%2028%2C%202021

[2.0]
Physical Paper Material Journal Notebook / Notebook Era #4 / February 26, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Physical%20Paper%20Material%20Journal%20Notebook/Notebook%20Era%20%234/Notes-By-Calendar-Dates/2021%20A.D./Month%202%20-%20February/February%2028%2C%202021

[3.0]
Running Away, Caught in a Snow Storm, Chaos (Story 71)
By Isabel Paige
https://www.youtube.com/watch?v=ACZooQX6rmA




