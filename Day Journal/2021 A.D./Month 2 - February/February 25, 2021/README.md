

# Day Journal Entry - February 25, 2021

[Written @ 23:53]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:22:57]

- 📝 [commentary] I read [1.0] and watched [2.0]

[1:22:57 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
[YouTube Post]
By Squirmy and Grubs
https://www.youtube.com/post/UgwOY6Bb9xjViim8Ie14AaABCQ

[2.0]
Episode 5: Why We Send NUDES
By free the girls podcast
https://www.youtube.com/watch?v=VPS5CCAlaT0


