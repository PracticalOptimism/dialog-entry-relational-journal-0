

# Day Journal Entry - January 19, 2021

[Written @ 22:48]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:46:03]

- 📝 [commentary] I watched [1.0] [2.0]

[1:46:03 - End of Video]

- 📝 [commentary] Salutation

References:

[1.0]
What Your Voice Reveals About You 👀
By The Gem Goddess
https://www.youtube.com/watch?v=GQBop8PZwa0

[2.0]
The Secret to Make Manifestation Work EVERY TIME
By The Gem Goddess
https://www.youtube.com/watch?v=CLK2yU-uLoY



