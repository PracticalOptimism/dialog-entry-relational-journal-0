

# Day Journal Entry - February 1, 2021

[Written @ 21:59]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - End of Video]

- 📝 [commentary] I didn't do anything . . 





[Written @ 0:44]

Notes @ Newly Created Words

Affluvia
[Written @ February 1, 2021 @ 0:48]
[I accidentally typed the word . . "Affluvia" . . instead of typing the word . . "Effluvia" . . because . . I didn't know how to spell the word . . "Effluvia" . . and so . . now my mistake has brought me to a newly discovered word . . or a newly created word . . called . . "Affluvia" . . I'm sorry . . I'm not sure . . what this word . . "Affluvia" . . should mean . . at this time . . I have seen the word . . "cow" . . come to my mind . . and possibly the shape color pattern of a cow that is like a blot shaped color pattern . . with a white background and a black surface blot area . . that is possibly . . a meaning for this word . . "Affluvia" . . I'm not sure . . that's just one example of the possibilities . . but I suppose other . . "Affluvia" . . could be ascribed to this meaning . . meaning that for example . . maybe . . an . . "Affluvia" . . cow pattern should possibly be allowed to have many shapely forms and expressions . . and still be identifiable . . as an . . "Affluvient" . . uh . . shape color pattern for the same type of idea . . of a cow pattern shape color or something like that . . "dancing with the stars" . . "dealing cards with your friends" . . these are possibly affluvient . . terms for . . "are you carrying a wound of pickles and pears for your friends to eat without you knowing they are carrying those pistols of income appetizer events?" . .]


Notes @ Newly Learned Words

Andics [1.1 @ 7:06]

Effluvia [1.1 @ 7:09]

Notes @ Newly Learned Music Lyrics

"
Change our religion . . like . . you change . . our underwear . . 

Change our religion . . show god . . they . . really . . care . . 

Change our religion . . when I try . . something new . . 

Use your critical thinking skills . . to . . question . . all you eat, drink, buy . . think, say . . and do . . 
"
-- The Vegan Teacher
[1.1 @ 7:15 - 7:31]


[Written @ 0:27]

Notes @ Newly Learned Quotes

"
You know . . the famous . . idiom . . 'We are what we eat' . . suggests . . how significant . . diet is . . 

It changes . . you . . it changes . . your consciousness . . 

It . . creates . . different . . cultures . . 
"
-- Russell Brand
[1.1 @ 4:11 - 4:20]


[Written @ 0:20]

Notes @ Newly Created Terms

Sincerety
[Written @ February 1, 2021 @ 0:26]
[I accidentally typed "sincerity" . . as . . "sincerety" . . I'm sorry . . I'm not sure what this term . . "sincerety" . . should mean . . at this time . . ]

Notes @ Newly Learned Terms

Unblinking Sincerity [1.1 @ 3:05]


Notes @ Newly Learned Phrases and Quotes For The Ageless Nascency of Human Beings

"
She's sort of being . . semi-ironic . . I suppose . . 

People don't always like being . . preached at . . idn't . . it ? . . 

With . . sort of . . anything . . Like . . I'm always . . telling people . . my opinion . . right ? . . 

I'm doing it . . right now . . 

But . . I hope . . it's done . . from a perspective . . of . . acknowledged . . fallibility . . and . . the kind of . . a sense of inquiry . . that . . I might . . be . . wrong . . 

I've certainly . . been  . wrong . . before . . 
"
-- Russell Brand
[1.1 @ 3:16 - 3:37]


[Written @ 0:15]

Notes @ Newly Learned Messages For Humanity

"
It's god has a message for you . . and . . here it . . is . . 

Why the fuck aren't you vegan yet ? . . 

Hmm ?
"
-- The Vegan Teacher
[1.1 @ 3:07 - 3:15]


Notes @ Newly Learned Music Lyrics

"
Eating animals is wrong . . McDonalds . . 

Hurting animals is wrong . . McDonalds
"
-- The Vegan Teacher
[1.1 @ 2:33]



[Written @ 0:09]

Notes @ Newly Discovered Videos

[1.1]
That Vegan Teacher - why's it blowing up?!
By Russell Brand
https://www.youtube.com/watch?v=Tvi5S6gg33k

[Written @ 0:06]

Notes @ Newly Discovered Videos

[1.0]
"Yoga and Embodied Movement" Sunday Service for January 31, 2021
By WellSprings Congregation
https://www.youtube.com/watch?v=YZVX6c6hR48
[Discovered by . . Video . . Recommendation . . for . . [2.0] . . video index . . number . . 1]

. . . .

[2.0]
Ecoin #450 - Live development journal entry for January 31, 2021
By Jon Ide
https://www.youtube.com/watch?v=HVVpWx2GpF4

