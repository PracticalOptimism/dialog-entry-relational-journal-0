

# Day Journal Entry - February 17, 2021

[Written @ 10:57]
- 📝 [commentary] I typed notes in the development journal entry for February 17, 2021 [1.0]

- 📝 [commentary] I cancelled the wedding proposal introduced in an earlier live stream episode [2.0] [3.0.1] [4.0.1] [5.0] [6.0] by "Seth" the consciousness personality essence that live streamed through Jane Roberts.

- 📝 [commentary] The original marriage proposal was to "get married to all the women on the planet and to be their one and only husband (with exceptions to individual marriages such as pre-existing family member marriages and marriages with sinful folk)". I don't mind getting married but one person is enough.  Also due to my suicide breakouts, I have been thinking that this is a core reason for the breakouts because I have been under a lot of pressure thinking about the negative consequences. 

. . 

- 📝 [commentary] I typed notes in the development journal entry for February 17, 2021 [1.0]

- 📝 [commentary] I cancelled the marriage proposal introduced in an earlier live stream episode [2.0] [3.0.1] [4.0.1] [5.0] [6.0] by "Seth" who is an energy personality essence that channelled messages through Jane Roberts and Robert F. Butts in the 1900s.

- 📝 [commentary] The marriage that was proposed by "Seth" was considered to be "a marriage to all of the women on the planet (with the exception of family relatives and sinful folk) in exchange for a cool program like Ecoin". I have had many suicidal breakouts where I feel headaches and can't stand the thought of marrying people for many reasons like (1) racism (2) warfare (3) discomfort (4) missing a previous romantic partner (5) even the lifestyle is really unpredictable for me how to organize safety and health and other ideals for this style of being which is really unfamiliar to me. Because of my breakouts, I've decided to cancel the marriage proposal to try to feel better and maybe work more on the project and not feel so depressed and suicidal. I was enthralled by the idea for a few months but it's really something I don't want to think about any more.

- 📝 [commentary] Please do not ask me on a date. Please continue your normal social affairs and don't be weird about this topic. It has been cancelled and no surprises are very much welcome and appreciated. There are plenty of alternative social maneuvers such as (1) traditional ma-and-pa style one-on-one agreement proposals (2) polyamorous relations (3) wait for your soulmate who isn't born yet style relations



[Written @ 8:58]



[Written @ 6:20]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:53:31]

- 📝 [commentary] I typed notes in the development journal entry for February 17, 2021 [1.0]

- 📝 [commentary] Please do not seriously consider the marriage proposal by "Seth" that was introduced several episodes ago in this live stream video series (I'm sorry, I don't remember which episode :/)

[1:53:31 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry / February 17, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%202%20-%20February/February%2017%2C%202021

