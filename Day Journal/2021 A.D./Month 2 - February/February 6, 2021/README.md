


# Day Journal Entry - February 6, 2021

[Written @ 8:53]

Notes @ Random Thoughts

The image on the right of the screen . . showing the photographic album cover for "Mirabai Ceiba" . . "Gurmukhi Mantras"

. . 

Well . . what happened to me . . a few minutes ago now . . I was looking at this image . . 

. . 

But . . it started changing . . because . . you can take into account our conversation on peripheral vision sights from this live stream . . you are able to start reasoning about . . things in the corner of your eyes . . that are ladent . . and not apparent . . but you can walk around those ideas . . like you are walking in a space . . in your living room with your legs or your wheelchair or your vehicle of transportation or something like that . . 

. . 

well . . it's like . . it's an album space . . where you can . . uh . . periodically sample various versions of that album image . . and share those thoughts and insights . . 

. . 

There's an algorithm somewhere that could allow you to transform images into video games . . and so you can walk around in the image . . or something like that . . 

. . 

And transofrm images into videos . . so you can watch movies . . or something like that . . 

. . because of ladent event transformations you can hypothesize theoretical next place locations like a star map


[Written @ 3:51]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 4:17:16]

- 📝 [commentary] I transcribed notes from my physical paper material journal notebook [1.0] . . the notes were about "the star tree data structure" . . which is a possible generalization to a lot of other data structures . . 

[4:17:16 - 4:58:24]

- 📝 [commentary] I watched [2.0] [3.0] and parts of [4.0] [5.0]

[4:58:24 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Physical Paper Material Journal Notebook Transcript Notes For February 5, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Physical%20Paper%20Material%20Journal%20Notebook/Notebook%20Era%20%234/Notes-By-Calendar-Dates/2021%20A.D./Month%202%20-%20February/February%205%2C%202021

[2.0]
Rendering Hyperbolic Spaces - Hyperbolica Devlog #3
By CodeParade
https://www.youtube.com/watch?v=pXWRYpdYc7Q

[3.0]
Hyperbolic Geometry in HyperRogue
By Center of Math
https://www.youtube.com/watch?v=lX5eCfRSCKY

[4.0]
Visualizing the sphere and the hyperbolic plane: five projections of each
By David Madore
https://www.youtube.com/watch?v=xHvAqDuWG2M

[5.0]
Topology For Beginners: Topology Meets Hyperbolic Geometry
By Richard Southwell
https://www.youtube.com/watch?v=bcogfFkcZbo



[Written @ 0:23]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:13:21]

- 📝 [commentary] I watched [1.0] [2.0] [3.0] while eating food . . I ate 3 burritos . . egg burritos with mayonaisse . . I was hungry . . well . . uh . . -_- . . 

[1:13:21 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
How to Pitch Your Hedera21 Hackathon Project: Tips and Best Practices
By Hedera Hashgraph
https://www.youtube.com/watch?v=0Vu4UD33WCk&ab_channel=HederaHashgraph

[2.0]
Shane Did Some Research
By Squirmy and Grubs
https://www.youtube.com/watch?v=WyKkNGbbQHY

[3.0]
We Need To Talk About Shane's Legs
By Squirmy and Grubs
https://www.youtube.com/watch?v=Bs6eeLiBVbw


