


# Day Journal Entry - January 30, 2021


[Written @ 23:56]

Notes @ Newly Discovered Related Words

Belonging . . 
Longing . . 
[Originally I had typed . . "longing" . . thinking that was what the announcer said . . at . . [1.1 @ 2:36] . . and then . . later realized . . then had said . . "belonging" . . which is uh . . only related to . . "longing" . . with . . "be" . . as a prefix . . which is quite interesting . . ]

Notes @ Newly Created Words

objectivication
[Written @ January 30, 2021 @ 23:59]
[I wrote the word "objectivication" instead of typing the word . . "objectification" . . I thought I heard the word . . "objectivication" . . uh . . but . . I guess I was wrong . . I thought . . [1.1 @ 3:28] . . the announcer . . read the word . . "objectivication" . . ]

Notes @ Newly Learned Words

objectivation
[Written @ January 31, 2021 @ 0:01]
[1.1 @ 3:28]


Notes @ Newly Learned Words

Museology [1.1 @ 40:35]


[Written @ 23:47]

Notes @ Newly Developed Notes

These Notes Are For [1.1] . . 

(1) Feeling for others . . [1.1 @ 2:22]
  (1.1) What are the new solidarities that have emerged from the COVID crisis?

(2) Lonely . . Together . . [1.1 @ 2:28]
  (2.1) Can we emotionally connected in the digital world ? 

(3) Old empires . . and . . new empire . . [1.1 @ 2:33]
  (3.1) 

(4) Living with one another . . [1.1 @ 2:36]
  (4.1) Is there a new sense of belonging and citizenship ? . . 

(5) Speaking with . . each . . other . . [1.1 @ 2:41]

(6) Is free speech . . still a right ? . . [1.1 @ 2:43]


. . 

Panel #1: "Looking at Others"

Artistic View:
- The history of art can be read as "the objectification  of women" . . And as we fight against . . structural stereotypes . . what . . new . . representations . . narratives . . and . . relationships . . are emerging ? . . 

- So . . how do we understand them . . and each other ? . . 

- How is the female gaze . . that we can now experience . . in some contemporary works of art . . revolutionary . . ?

. . 

We will have a female . . panel . . 

As the masculine presence . . I will no doubt . . be chipping in . . at the end . .

. . 

Hilary Robinson . . 

Professor of feminism . . art . . and theory . . 






[Written @ 23:33]

Notes @ Newly Discovered Videos

[1.1]
Replay Day 4 - Night of Ideas 2021
By FrenchInstituteUK
https://www.youtube.com/watch?v=QTR3cBXMUOw
[Discovered by Video Recommendation . . from . . [2.1] . . video index . . number . . 1]

[2.1]
Ecoin #449 - Live development journal entry for January 30, 2021
By Jon Ide
https://www.youtube.com/watch?v=tBcoBsxMY9U&feature=youtu.be

[Written @ 0:12]

Notes @ Newly Discovered Video

[1.0]
Coop Cinema, Episode 3 "Empowering the planet"
By EURICSE
https://www.youtube.com/watch?v=YgnNkOVZL-E
[Discovered by video recommendation from . . [2.0] . . video index number . . 1 . . ]

[2.0]
Ecoin #447 - Live development journal entry for January 28, 2021
By Jon Ide
https://www.youtube.com/watch?v=GRe_mpl2Ijs

