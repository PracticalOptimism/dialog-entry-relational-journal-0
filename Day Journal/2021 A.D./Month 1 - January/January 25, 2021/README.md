


# Day Journal Entry - January 25, 2021

[Written @ 17:36]

Notes @ Newly Discovered YouTube Channel

SAM THE ILLUSIONIST
https://www.youtube.com/c/SAMTHEILLUSIONIST/videos

[Written @ 0:02]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 2:28:57]

- 🚧 [work-progress] I updated the "page title" component to initialize (1) page title statistics (2) page action list (3) page news updates

[2:28:57 - 3:09:12]

- 🚧 [work-progress] I updated the "your account", "network status", "settings" and "about" pages with a "page title" component that is meant to be a standard design template for interfacing with websites. I'm not really sure if it's really good but it's okay for now.

[3:09:12 - 3:40:34]

- 🚧 [work-progress] I reviewed the website version so far on my cell phone . . I think it looks okay . . 

[3:40:34 - End of Video]

- 📝 [commentary] Salutations



