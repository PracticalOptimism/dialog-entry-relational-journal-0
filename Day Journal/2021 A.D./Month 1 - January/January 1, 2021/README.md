
# Day Journal Entry - January 1, 2021

[Written @ 20:35]

Notes @ Newly Discovered Poems

[copy-pasted from Development Journal Entry - January 1, 2021 @ 19:54]

"
You can continue to see the light

You can constitute the forest

You shore your way to new paradises

You forest your canopoies with new endeavor

You are neither here nor there

You are here in there

"
-- Seth
[Recorded on behalf on Jon Ide for the live stream of Ecoin #420 for January 1, 2020]


[Written @ 19:01]


Notes @ Newly Discovered YouTube Channels

Oddtom
https://www.youtube.com/channel/UCAHKfTnKdODiOL2wVZzhvDg/videos
[Discovered by [1.4 @ Video Publisher]]


[1.4]
It's Not Personal: A Summer Wars Theory
By Oddtom
https://www.youtube.com/watch?v=yCXt9RbjyNc
[Discovered by YouTube Search Result for "summer wars oz love machine" . . Search Result index . . number 12 . . of 12 videos . . 0 playlists . . and 0 channels . . of the search result list . . ]

[2.4]
Inuyasha the Movie 3: Swords of an Honorable Ruler
By YouTube Movies
https://www.youtube.com/watch?v=oZCzcS9N10E
[Discovered by [1.4 @ Next Video Recommendation]]



[Written @ 16:39]

Notes @ Thoughts

I like the sound of . . Quotes @ 1.3 . . with . . the music . . in the background . . from . . [2.2] . . I'm not sure . . at which point . . [2.2] . . starts . . where I liked the . . sound of . . [1.2 @ 17:19 - 17:48] . . but it really sounded cool when I heard these two things together . . because I think I felt like I was in a movie . . and . . there was something magical about the way . . the . . speaker . . was speaking . . that made the message . . resounding . . and harmonic . . and angelic . . anc poius . . 

Notes @ Newly Discovered Quotes

[Quotes @ 1.3]
"
Place your hands on your beautiful heart with me and let's just be still together, for a few moments.

With no thought . . no intention . . no desire . . and no expectation . . 

But just being naked . . open . . and raw . . in front of our lord . . our . . creator . . 

I am . . 
"
-- Anna Brown
[1.2 @ 17:19 - 17:48]

Notes @ Newly Learned Words

Queendom [1.2 @ 18:56]

[1.2]
Control is an Illusion
By Anna Brown
https://www.youtube.com/watch?v=NwTCJd7-Rh4

[2.2]
[ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music
By Nhạc sóng não chính gốc Hùng Eker
https://www.youtube.com/watch?v=4MMHXDD_mzs


[Written @ approximately 15:45]


Notes @ Newly Discovered Information

What inspired you to make study streams?

(1) lack of friends to study with
(2) procrastination habits
(3) teachers telling you you will not be this or that . . you cannot do this . . you cannot do that . . I was like . . I'm gonna prove them wrong . .
(4) I always wanted to be on youtube . . having a channel and stuff . . 

[1.1 @ 2:05:09 - 2:05:39]

[1.1]
HAPPY NEW YEAR - Study With Me *6 HOURS* (with talking during breaks)
By Study Vibes
https://www.youtube.com/watch?v=c_fevTU9tYs&ab_channel=StudyVibes


[Written @ 13:49]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:53:17]

- 📝 [commentary] I live streamed Seth and took notes on ideas and thinkings related to the topic of the live stream and how the live stream has changed over the past few months and from its initial inception [3.0]

- 📝 [commentary] I watched [1.0] [2.0]

[1:53:17 - 3:06:54]

- 📝 [commentary] I watched [4.0] [5.0]

[3:06:54 - 5:08:24]

- 📝 [commentary] I wrote some notes in the development journal [3.0]

- 📝 [commentary] I watched [6.0]

[5:08:24 - 7:06:31]

- 📝 [commentary] I wrote notes in the development journal [3.0]

[7:06:31 - 7:30:34]

- 📝 [commentary] I am taking a break . . be right back . . 

[7:30:34 - End of Video]

- 📝 [commentary] Salutation


References:

[1.0]
The Invisible Appearing Visible
By Anna Brown
https://www.youtube.com/watch?v=vrk71GDPMfk

[2.0]
Everything Is the Beloved Talking to Itself
By Anna Brown
https://www.youtube.com/watch?v=5woNUJe79Ho

[3.0]
Dialog Entry Relational Journal 0 - Development Journal Entry - January 1, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%201%20-%20January/January%201%2C%202021

[4.0]
The End of The Road Message
By Anna Brown
https://www.youtube.com/watch?v=f28-evHnX3c

[5.0]
Control is an Illusion
By Anna Brown
https://www.youtube.com/watch?v=NwTCJd7-Rh4

[6.0]
It's Not Personal: A Summer Wars Theory
By Oddtom
https://www.youtube.com/watch?v=yCXt9RbjyNc


