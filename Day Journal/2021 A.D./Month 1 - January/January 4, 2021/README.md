

# Day Journal Entry - January 4, 2021

[Written @ 22:02]

"
Like . . me . . kind of . . playing the superficial game . . I can only . . do that . . for . . like . . an hour . . and . . then I'm dead . . 
"

[I read this quote . . uh . . and . . in my peripheral vision . . I thought I saw the word . . "children" . . appear in my vision . . in place . . of this group of words . . and so . . uh . . this group of . . words . . in this paragraph . . makes me uh . . think . . that was I saw was like . . an emotional suggestion . . to look at this . . paragraph in terms of how it relates . . to . . "children" . . uh . . ]
[hmm . . I'm not sure . . I guess . . uh . . -_- . . I don't know . . -_- . . children . . -_- . . ]
[-_- . . I wonder if children feel like this -_- . . amirite? . . -_- . . ]
[I don't know what to think about this topic at this time -_- . . I don't -_- -_- know -_- what -_- to -_- think -_- aboout -_- this -_- topic -_- at _____- this -_---------- time -------------------------------------------------------------------------------------------------------------------------__---------------------------------------------------------_______----------------------____________________________________________________----------_________________________________________________________________________________________________________________________________________________________-----]

[Written @ 21:36]

Notes @ Newly Learned Phrases From A Video Game Of The Future

"
Like . . me . . kind of . . playing the superficial game . . I can only . . do that . . for . . like . . an hour . . and . . then I'm dead . . 
"
-- Ricky Sanchez
[1.0 @ 12:48]
[I was inspired to think that this could be a game lyric . . in a video game somewhere . . because of the way the phrase sounds . . and how . . "game mechanics" . . are sort of related to the topic . . uh . . "playing the superficial game" . . and . . "then I'm dead" . . uh . . dying in a video game . . is a superficial game mechanic . . and so it's an interesting topic to think . . that . . this could possibly be a quote . . in a video game somewhere . . uh . . something like that . . I'm not sure . . but it sounds really cool . . "for like an hour" . . so it's like . . playing a video game for 1 hour . . kills your video game character ? . . could you imagine . . a video game where . . after every hour . . of play . . you die . . and so . . that's an interesting idea to explore for a possible video game . . and uh . . so what types of death do you think could be possible to explore after all those types of deaths ? . . or is there only one type of death ? . . is there . . a type of type of type of type of type of type of type of type of type of type of type of type of type of death . . that relates to boring repetitive activities like "commit forever sleep" [1.2 @ 1:52] . . ? . . uh . . this also reminds me of the topic of . . death . . in the video game . . "world of warcraft" . . uh . . "De Other Side" [2.2 @ 0:30] . . is a place that seems to be new uh . . -_- . . uh . . I'm not really sure . .]


[1.2]
Finding Positivity in a Negative World
By dannphan
https://www.youtube.com/watch?v=MiWJOFaE9IE&ab_channel=dannphan

[2.2]
Shadowlands - Max-Level Dungeons
By World of Warcraft
https://www.youtube.com/watch?v=De3rtQhZF6w&ab_channel=WorldofWarcraft

Notes @ Newly Learned Quotes

"
Well . . that's the other thing . . Amanda . . Is like . . That's where . . it's . . it's . . no . . pat . . on . . my back . . at . . all . . 

It's just . . one of my natural . . freebies . . Is like . . I have an . . unlimited . . un-untapped . . and unlimited . . ability . . to hear . . people's . . deepest . . most vulnerable . . spot . .

For whatever reason . . it just . . hits me . . and it doesn't . . pull my energy down . . 

Other things . . pull . . my energy . . down . . big time . . 

Like . . me . . kind of . . playing the superficial game . . I can only . . do that . . for . . like . . an hour . . and . . then I'm dead . . 

But like . . you know . . all that . . energy around me . . I felt like . . it was . . my . . if it was a . . if . . it was a . . just . . a bunch of words . . in a room . . like . . let's . . imagine . . words . .

I was the poet . . putting the words together . . in the right way . . just to s- . . just to have everything . . harmonize . . it was so . . cool . .
"
-- Ricky Sanchez
[1.0 @ 12:27 - 13:08]


[Written @ 21:32]

Notes @ Newly Remembered Phrases

Holding Space [1.0 @ 12:01]


[Written @ 21:23]

Notes @ Newly Discovered Probable Song Lyrics

"
I have so much to purge inside me . . 
"
[Inspired by [1.0 @ 6:30 - 6:33]]
[I was inspired to . . think of this . . phrase . . spoken . . in the video . . as a song lyric that could possibly exist . . in a rock song . . or something like that . . It sounded . . really cool when I heard . . this . . and I was inspired to think . . that it sounds like a lyric from a . . rock song or something like that . . ]


[Written @ 20:50]

Notes @ Newly Learned Probable Alternative Quotes . . To . . [1.1]

"
The only exclusivity factor . . that's thrown . . in there . . is . . how . . bubbly . . are you willing to be . . 

Because . . that's like . . the ticket in . . 

And that's not the ticket in . . for you to get into our group . . It's . . ticket . . for you . . to . . get . . into . . you . .
"
[This probable alternative . . quote . . to . . [1.1] . . is inspired by the one word . . "vulnerable" . . changed . . to . . "bubbly" . . in my mental . . flash of vision . . and so for aexmple . . it was an hallicuinatory event . . where . . my . . eyes saw the word . . "vulnerable" . . as . . "bubbly" . . and uh . . that inspired me to re-read the quote . . with the word . . "bubbly" . . introduced . . instead of the word . . "vulnerable" . .]


Notes @ Newly Discovered Quotes

[1.1]
"
The only exclusivity factor . . that's thrown . . in there . . is . . how . . vulnerable . . are you willing to be . . 

Because . . that's like . . the ticket in . . 

And that's not the ticket in . . for you to get into our group . . It's . . ticket . . for you . . to . . get . . into . . you . .
"
-- Ricky Sanchez
[1.0 @ 29:40 - 29:50]

[Written @ 20:23]

Notes @ Newly Learned Words

astrals (plural) [1.0 @ 26:28]

[Written @ 18:25]


Notes @ Newly Discovered Quotes

"
I've known for a while . . when I was in Sedona . . in fact . . I've known for a while . . that I am meant to be a networker . . 

I heard that . . And I was like . . what the heck . . does that . . mean ? . . 

And so . . of course . . I was looking at it . . in the 3D sense . . like . . and it . . just . . kind of . . never . . fit . . And then . . I started . . When I was in . . Sedona . . I got . . unplugged . . I didn't feel . . like . . I was . . on . . on the planet . . at all . . 

And . . had . . so many . . crazy . . experiences . . and downloads . . which . . is . . where I got most of my understanding . . of the astrals . . 

And . . met . . through . . my friend . . who is . . very . . 5D . . met people . . who were . . in that . . grid space . .

And one other person . . who's like . . Have . . you . . ever seen the . . movie . . uhm . . Dr. Strange . . the new one ? . . Is that what it's called ? . . Dr. Strange ? . . Why am I thinking the name's off ? . . 

[Ricky Sanchez]: Yea . . Yea . . I . . I know his story . . Yea . .

Yea . . well . . the . . There's . . a villain in that . . That . . can like . . He's . . the one at the very beginning . . that's . . like . . going through . . that . . 5D space . . That's some sort of war . . I can't remember . . the guy's . . name . . 

[Ricky Sanchez]: I haven't watched that movie . . now . . again . . I love that movie . . 

This . . guy . . in Sedona . . is like that . . guy . . Like . . just . . like . . that guy . . 

But I wouldn't . . say . . he's . . he's . . not an evil villain . . He's like . . "what are the good ?" . . But anyway . . My friend's like . . "You need to talk to him about . . this grid you keep seeing . . " . . Because . . I kept getting . . this . . huge vision . . of . . like . . a grid . . and I would move through . . different . . networking spaces . . in it . . and I would meet all these grid workers . . And but . . I could go through . . any part of the grid . . And I . . I mean . . I was seeing . . full on visions of it . . And . . my friend . . Karis . . was like . . "I only know one other person . . who has described that place you're talking about . . I have to connect you with him . . " . .

And so . . she introduced me . . to him . . And I started . . telling him what I was seeing . . He was like . . He . . hold . . He pulled . . out . . this huge book . . of . . everything he's drawn out from the same freakin' . . visions he's had . . 

[Ricky Sanchez]: Woaahhh! . . 

And he was like . . "I've been mapping this out . . for a while . . " . . I have never met . . anyone else . . who has . . the kind of access . . that you have . . 

I've never seen . . anyone be able . . to . . a- . . access . . all of it . . "

[Ricky Sanchez]: Oh . . my . . god . . 

Which . . I don't even know what that means . . But . . I just remember . . when I was talking to him . . like . . "Okay . . I'm not crazy . . Someone else . . literally knows exactly . . what I'm talking about . . " . . 

And it has to do . . in . . my mind . . I knew . . very clearly . . it has to do . . with . . who's pulling the strings . . behind . . the scene . . 

We often don't know . . because . . people don't know their power . . They don't actually know what they're really doing . . Half . . the . . time . . their . . power is being used apart from their conscious will anyway . . 

So . . Uhm . . My . . entire . . point of . . this . . is . . that . . this . . has been . . brewing . . in me for a while . . This concept of . . like . . getting people . . together . . 

And I have . . I have . . the craziest . . network . . of people . . Like . . In . . every . . realm . . you can . . imagine . . I . . I just . . weirdly know . . a lot . . of freaking . . people . . From . . every . . different . . sort . . of . . walk . . of life . . 

And I have . . the network . . I've thought . . so many times . . I have . . I know enough people . . that do . . enough . . things . . that . . we could really . . create . . whatever . . we wanted . . Like . . My brother . . created a new social . . media . . platform . . And it's like about . . to . . We're . . about to launch it . . But . . He . . realized . . once you talked to me . . that . . like . . It would be . . I-If he catered it . . to influencers . . like us . . especially . . because . . a lot of them are being censored now . . 

A lot . . of . . i-influencers are . . they just . . don't like . . the algorithms . . and how . . they're run . . Uhm . . It- . . And I'm just like . . I mean . . that's just . . one example . . But . . I'm like . . 

We could create . . what . . We could literally create . . our entire . . own . . thing . . 

You know . . what I mean . . ? . . Like . . We don't need . . anyone . . To do . . We . . We could do it . . We . . just . . need . . to connect . . 

That's like . . all . . And I think we have . . the . . power . . and . . the . . money . . and . . the . . the . . influence . . 

We have everything we need . . We just need to come . . together . . 
"
-- Amanda Flaker
[1.0 @ 26:05 - 29:24]


[1.0]
New Earth Abundance + Co Creative Magick (w/Ricky Sanchez)
By Amanda Flaker
https://www.youtube.com/watch?v=0FA-DkLTu-8&ab_channel=AmandaFlaker


[Written @ 15:37]

Notes @ Random Poem Ideas

Notes @ Poem Idea

A good deed unwritten
A good deed unsaid
A good deed unread

There are unprovements
There are unsciences
There are understandings
There are improvements
There are undersciences
There are underwritings
There are understatements
There are understatements

. . . 



[Written @ 13:40]


