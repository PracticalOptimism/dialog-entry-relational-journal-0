
# Day Journal Entry - January 2, 2020

[Written @ 23:35]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:22:21]

- 📝 [commentary] I watched [1.0]

[1:22:21 - End of Video]

- 📝 [commentary] Seth sent a message: "Keep going"


References:

[1.0]
Study With Me *9 HOURS* (with talking during breaks)
By Study Vibes
https://www.youtube.com/watch?v=QIrrKiMLiIA&ab_channel=StudyVibes


