

# Day Journal Entry - January 10, 2021

[Written @ 12:06]

Organic Basics
[Newly Discovered Organizations . . discovered from . . [1.0 @ 0:00 - 1:54]]

[1.0]
it's time to leave my cabin
By hannahleeduggan
https://www.youtube.com/watch?v=cApfzmHCwyw

[Written @ 12:02]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 2:02:24]

- 📝 [commentary] I typed some notes for Seth [1.0]

- 📝 [commentary] I typed some notes for myself [1.0]

[2:02:24 - 5:19:45]

- 📝 [commentary] I worked on a document template or a document data structure for transcribing my physical paper material journal notebook notes [2.0]

[5:19:45 - 6:06:22]

- 📝 [commentary] I transcribed notes from my physical paper material journal notebook and typed them into a text editor program using Visual Studio Code . . Those notes are available at [3.0]

[6:06:22 - End of Video]

- 📝 [commentary] I took notes on the light architecture at [1.0]

- 📝 [commentary] I showed a photograph of the (1) "Flower of Life", the (2) "Rodin Coil" symbol from "Vortex Based Mathematics" (3) the "Fractal Field" website by Dan Winter [4.0] and (4) the "FlameInMind.com" website by Dan Winter [5.0] . . which are all seeming to resemble topics that relate to . . "embeddability" . . or . . "infinite nestedness" . . or something like that . . which are possible orthogonal descriptions for describing the topic of . . "hyperorthogonality" . . which is related to the topic of "the light architecture" which is a theoretical data structure that relates to the topic of "orders of magnitudes" of hyperthogonals being constructed and represented by a single data structure organization matrix that doesn't necessarily have finite strength components but that is possibly a way to implement it in a physical digital computer today that would allow further support for embedabbility of the topics of interest and would possibly be useful for custrouctious [constructions] of various varieties of computer algorithms that constuructions [constructions] new organizations of organizatino hiererchiesie [hierarchies] of syboeprls [symbols] by not necessarily relating to those things in previously identified terms but alwasys [always] always [allowing] ueiru [for new] giepifue [interpreations [interpretations]] to be created at uidpiwe [runtime] and opeife [forever] dynsiepif [dynamically] and withidueurpe [and without resting]

- 📝 [commentary] Salutations

References:

[1.0]
Dialog Entry Relational Journal 0 / Development Day Journal / 2021 A.D. / Month 1 - January / January 10, 2021 / README.md
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%201%20-%20January/January%2010%2C%202021

[2.0]
Dialog Entry Relational Journal 0 / @Document-Data-Structure / Notes-Organization-Formats / Physical-Paper-Material-Journal-Notebook-Page-Transcript / README.md
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/%40Document-Data-Structure/Notes-Organization-Formats/Physical-Paper-Material-Journal-Notebook-Page-Transcript

[3.0]
Dialog Entry Relational Journal 0 / Physical Paper Material Journal Notebook / Notebook Era #3
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Physical%20Paper%20Material%20Journal%20Notebook/Notebook%20Era%20%233/Notes-By-Calendar-Dates/2021%20A.D./Month%201%20-%20January/January%208%2C%202021

[4.0]
Fractal Field
http://fractalfield.com/

[5.0]
FlameInMind.com
http://flameinmind.com/

[Written @ 4:46]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 6:19:19]

- 📝 [commentary] I watched [1.0]

- 📝 [commentary] I accidentally fell asleep . . 


[1.0]
Hannah's Birthday Livestream
By Squirmy and Grubs
https://www.youtube.com/watch?v=zj4SYgWbWTQ



