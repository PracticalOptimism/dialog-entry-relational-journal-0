
# Day Journal Entry - January 3, 2020

[Written @ 15:51]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 1:24:53]

- 📝 [commentary] I took notes on various ideas that I have so far in my development of (1) the light architecture and (2) the love algorithm

[1:24:53 - 1:53:33]

- 📝 [commentary] I took notes for Seth and Robert F. Butts in [1.0]

[1:53:33 - End of Video]

- 📝 [commentary] Salutation

References:

[1.0]
Dialog Entry Relational Journal 0 - Development Journal Entry - January 3, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%201%20-%20January/January%203%2C%202021



[Written @ 3:10]

Notes @ Newly Learned Words

Passive Reading [1.2 @ 1:00:00 - 1:02:00]

[1.2]
Study With Me *6 HOURS* (with talking during breaks)
By Study Vibes
https://www.youtube.com/watch?v=nGPnOTIqjFA

[Written @ 2:21]

Notes @ Newly Discovered YouTube Videos

SeniorConcerns
https://www.youtube.com/user/SeniorConcerns/videos
[Discovered by [1.1 @ Video Publisher]]

[1.1]
New Years Eve Virtual Gala
By SeniorConcerns
https://www.youtube.com/watch?v=5BPawQBYC-E
[Discovered by the video recommendation in autoplay for . . [2.1]]

[2.1]
Ecoin #421 - Live development journal entry for January 2, 2021
By Jon Ide
https://www.youtube.com/watch?v=hAyZUjNEbvk&feature=youtu.be



[Written @ 2:16]

Notes @ Newly Created Words

Mistake Driven Development
[Written @ January 3, 2020 @ 2:16]
[I am going to make a mistake while doing this and that is hopefully the intention if you are the audience and catch me make a mistake that you are only an idiot to assume that you are correct in identifying my mistake]


[Written @ 2:12]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00]

- 📝 [commentary] I am starting this live stream on the basis of the idea that I won't explain things . . Explaining things on the live stream is a long process and I don't want to perform explanation experiments simply for the user of this video to follow along in a logical stream otherwise that is not necessarily logical because there are no superfluous explanations that would make the reasoning more normalized . . I am sorry if you are confused when that occurs but it is easier for me to identify myself on the live stream without having to explain each mistake and item of courageous potention that I make and don't ask you the reader to also understand this mistake or maneuver on my journey to develop this project forther or something like that . . Mistake Driven Development is what this process is being referred to at this time . . There are possibly errors and mistakes that can happen and you are always welcome to point them out in the notes for the chat or on the notes for [1.0]

[1.0]
Dialog Entry Relational Journal
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0





