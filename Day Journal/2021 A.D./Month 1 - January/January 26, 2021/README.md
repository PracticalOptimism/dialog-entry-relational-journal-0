

# Day Journal Entry - January 26, 2021

[Written @ 21:21]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:44:18]

- 📝 [commentary] I didn't do a lot of useful things. Mostly I wrote nonsense in my development journal notebook. [1.0]

[0:44:18 - 1:19:14]

- 📝 [commentary] (1) my experiments for self suicide are possible to fail because it is a method that I am still learning about (death by forced consciousness excitation from the body which seems to require a charging process for you to excite your consciousness in repeated cycles, basically I have never heard of this process before but I am learning about it now from intuitition and learning about orders of magnitude of expressing my body to be illusioned out of focus and so I am no longer an earth citizen but can possibly travel to other lands of consciousness) (2) it is possibly helpful to share this project for others to learn about the progressions beeing made (3) if my suicide fails it is boring to listen to what people think about an incomplete project (4) listening to boredom makes me more suicidal (5) finishing the project or dying are the current parallel paths I'm currently living out. When I get off the live stream, I chant suicide death mantras for my death to be peaceful and so I can leave. If I die with successful application, then it is painful for people to not necessarily know about the progressions made by this project. Sharing the project upon my death is left to my brother who will see a suicide note of names and people to share the project with (A) Unicole Unicron (B) Paget Kagy (C) Andrew Yang (D) Elon Musk (E) Jack Dorsey (F) . . That's all the names that I have so far . . anyone else will probably be anthalogical relations that I don't necessarily know about but are possibly as enthusiastic about the ecoin project

- 📝 [commentary] An incomplete project is possibly a necessarily sufficient device to create and so there's possibly no need to be embarrassed by that possibility. I'm sorry to startle you if that is an aspect of concern for this topic that you are reading

- 📝 [commentary] If suicide fails. I'll probably try again on the next night by practicing more consciousness excitation processes to leave my body. I'm not exactly sure how this project will develop. I am not really excited for living and so working on this project is a complete waste of time and really hurts my body. -_- I'm not complete with "the light architecture" and "the love algorithm" which are really interesting aspects to the project. Actually I like to take notes on my physical paper material journal notebook about these topics still and I continue to make progress everyday. I'm proud of myself for being excited in this aspect

[1:19:14 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Development Journal Entry - January 26, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%201%20-%20January/January%2026%2C%202021


