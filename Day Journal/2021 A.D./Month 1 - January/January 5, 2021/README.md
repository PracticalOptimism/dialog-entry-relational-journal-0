

# Day Journal Entry - January 5, 2021

[Written @ 8:49]

Notes @ Newly Learned Phrases

Shoulder Gap
[This is a counterpart to . . "Thigh Gap" . . that came to mind . . as I was patting the shoulder of my younger brother . . and telling him that he was doing a good job or something like that . . I'm not sure what . . "Shoulder Gap" really is yet . . it only came as an idea . . uh . . I'm not sure if the gap is the space between the shoulders . . uh . . or uh . . possibly it could be the space between the shoulder muscle . . like the deltoid muscle which I was rubbing for my brother . . to show comfort and support . . and uh . . the clavicle bone . . which is nearby this area . . and so . . in my having touched this area . . for my younger . . uh . . brother . . who is a baby . . then . . uh . . well . . uh . . I don't know . . uh . . it only came to me as a flash of insight . . the word . . "Shoulder Gap" . . and then . . later I realized that it's possibly a counterpart related word to the word . . "Thigh Gap" . . although the word . . "Thigh Gap" . . can have connotations relating to the sexuality related things of humans since some humans have a sexual fantasy for thigh gaps . . and thigh gapped people . . or uh . . more uh . . commonly uh . . observed from myself . . uh . . thigh gapped women . . or something like that . . uh . . but the sexual relation was not intended when I was initially relying on my brother's shoulder for a soft baby shoulder to . . touch and feel . . because baby shoulders are smooth and consistent with love and compassion for all types of human beings . . ]

[Written @ 8:43]

Notes @ Newly Learned Phrases

"It has peels on it that you can eat."
[This phrase was said to me by my baby brother sibling . . just a few minutes ago . . as they were holding . . an . . apple . . that was . . before . . sitting on my desk . . and then . . baby brother . . handed the apple to me . . and said . . "it has peels on it that you can eat" . . uh . . something like that . . This was a newly learned phrase for me . . and I was surprised . . that he said that . . uh . . well . . also there was the company of a banana . . sitting next to my desk . . uh . . next to my apple . . uh . . and uh . . well . . I didn't notice that uh . . in the earlier days of my life . . I had not eaten the peels of a banana . . uh . . like I am readily eating the peels of an apple . . it was such a surprising new discovery that I decided to writ eit down here in my journal notebook]

[Written @ 5:09]


Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 7:57:52]

- 📝 [commentary] I watched [1.0]

- 📝 [commentary] I accidentally fell asleep . . and the videos . . [2.0] . . and . . [3.0] . . played automatically in the background . . Thanks for watching if you did . . And I'm sorry for consuming so many resources #youtubeisthebest . . xD . . XOXO

[7:57:52 - 11:18:00]

- 📝 [commentary] I woke up from a dream . . I had accidentally slept . . and am preparing to write what I dreamt about in my dream journal

[11:18:00 - End of Video]

- 📝 [commentary] Salutations

[1.0]
New Earth Abundance + Co Creative Magick (w/Ricky Sanchez)
By Amanda Flaker
https://www.youtube.com/watch?v=0FA-DkLTu-8&ab_channel=AmandaFlaker

[2.0]
Detox Your Soul | 528Hz Love Frequency Music | Deep Healing Miracle Tone | Spiritual Meditation
By Spirit Tribe Awakening
https://www.youtube.com/watch?v=haq0K7PzJTk&ab_channel=NatureHealingSociety

[3.0]
Good Morning Meditation Music 528Hz😍 Wake Up Fresh & Happy
By Nature Healing Society
https://www.youtube.com/watch?v=ijPmjObXyS0&ab_channel=NatureHealingSociety


