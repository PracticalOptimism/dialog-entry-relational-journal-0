
# Day Journal Entry - January 11, 2021

[Written @ 23:52]

Notes @ Newly Learned Poems

"
[1] Happiness is sharing silence with her,
[2] when words become unnecessary
[3] replaced by understanding gestures of the eye,
[4] just existing, close to her,
[5] thinking about the speed at which
[6] the universe is expanding
[7] and how numbingly unlikley it is--when you step back
[8] and look at the big picture--that I
[9] am the person
[10] holding her hand
[11] on this rainy evening
[12] in July.
"
[1.6 @ 8:50 - 9:18]

Notes @ Newly Learned Poems

"
[1] Time leaps ahead tonight,
[2] and while we rocket
[3] forward in our sleep,
[4] the trees know nothing of our manipulations with the sun.

[5] All they know--deep dark dogwoods craving warmth--
[6] is that the mornings have been less crispy lately,
[7] and somewhere,
[8] tucked beneath covers
[9] in the house they overlook,
[10] there's a boy
[11] dreaming about possibilities
[12] and her face.

[13] He knows the world is malleable in this season of change.
"
[1.6 @ 7:12 - 7:42]


[Written @ 23:35]

Notes @ Newly Discovered Organizations

Able
https://www.able-now.com/
[1.6 @ 2:52 - 4:34]

CalABLE
https://www.calable.ca.gov/
[Affiliate Resource URL: https://www.calable.ca.gov/squirmy]
[1.6 @ 2:01 - 4:34]

[1.6]
The Romance Between Us
By Squirmy and Grubs
https://www.youtube.com/watch?v=BOUvT5k8Mhs

[Written @ 23:10]

Notes @ Newly Discovered Live Stream

[1.5]
The Subconscious And The Words "I-Am" (Use This)
By YouAreCreators
https://www.youtube.com/watch?v=CiKjX2LTjOI
[Discovered on the front page of YouTube . . or the HomePage of YouTube . . as the . . 4th video of a group of 4 videos . . if I remember correctly . . it was on the uh . . top of the page . . which is a series of 4 videos that were displayed first before other content . . and after the search bar and header bar . . or something like that . . ]

[Written @ 23:02]

Sextrovert
[Notes @ Typographical Error In My Initial Hearing Judgement Of [1.3 @ 25:23] . . "Sex Avert" . . seems like the proper correct term that was spoken . . or said . .]

Notes @ Newly Discovered Or Re-Discovered YouTube Channels

Vishuddha Das
https://www.youtube.com/c/VishuddhaDas/videos
[Previously Known As "Koi Fresco"? or "Koi's Corner"? . . I'm not sure . . I'm sorry . . ]


Notes @ Newly Pending Videos To Be Watched

[1.4]
World Chess Champion Magnus Carlsen's verdict on The Queen's Gambit
By chess24
https://www.youtube.com/watch?v=Z8XbdDgmJA4

[Written @ 22:28]

"
So, I decided. That, I choose. Not to be afraid of these people. I'm going to choose. Not to follow what it is that they want me to. And choose. Not to react in a way that they intend for me to react.

So, rather than running and hiding, or hating these people. I am going to go and see if I can sit with them. Meet with them. Talk to them. And see. Is it possible for you to hate. As easily. In person. As it is to do behind the keyboard. Or as it is. To do. You know. On the Phone.

Uhm. And also. Is it possible for me. To sit with people. Like that. People that I've been. Afraid of. Most of my life. People that I have. Also. Hated. Most. Of My Life.

You know. Can we sit down. Can we recognize each other's humanity. Is that possible? And is it possible for me. To Hold on To My Humanity. In the process of doing that.
"
-- Deeyah Khan
[1.3 @ 13:37 - 14:28]

"
[Deeyah Khan]:

What . . Am I . . ? Am I . . ? . . What am I ? . . Why are you breathing like that ? . . 

[Russell Brand]:

[A.1]: Because I'm processing all of this information

[Because this is priceless and valuable information]
[Notes @ Typographical Error In My Initial Hearing Judgement of [A.1]]
"
[1.3 @ 14:28 - 14:34]

[1.3]
How Love Will Defeat Hate | Russell Brand & Deeyah Khan | Under The Skin
By Russell Brand
https://www.youtube.com/watch?v=gP-ekeEQXkc

[Written @ 20:25]

"
Why are we doing the kind of news item that we do any other year . . when it's clear that we're at some point of great reckoning . . reflection . . and change . . Where there are serious things to consider . . 

The important truth is . . That you can . . radically improve your life . . by . . changing . . what . . you . . believe . . But . . it's . . not . . a . . simple thing . . there is some . . complexity . . to . . it . . Making . . meaningful . . change . . to the way you drink . . use drugs . . smoke . . eat . . exercise . . treat . . the people . . that . . you . . love . . 

These things . . require . . a kind of . . hmm . . I would say . . deep . . communing . . with . . who you . . actually . . are . . 

Who . . you . . really . . are . . 

A process . . to . . HONOR . . the . . INVISIBLE . . world . . within you . . that is . . HARDER . . to . . TRACK . . 

This . . can't be achieved . . simply . . by . . scratching out . . a few . . trite objectives . . on the back of an envelope . . Or . . eating . . a green . . thing . . every . . day . . 

This . . again . . is . . how . . our culture . . takes from us . . something like . . our ability . . to . . envisage . . and create . . new . . realities . . from . . our . . imagination . . And . . sells it . . back to us . . as some . . ordinary . . Slightly . . tiresome . . doomed-to-fail . . cultural . . exericse . . 
"
-- Russell Brand
[2.1 @ 6:35 - 7:44]

. . 

Striking out a few . . trite objectives
Scratching out . . a few . . trite objectives
Striking out . . a few . . Scratch . . objectives . . 

[Written @ 20:04]

Ladder of Indifference
[Newly Learned Words @ [2.1 @ 4:42]]

Washboard Abs
[Newly Learned Words @ [2.1 @ 4:48]]

Pandemic Pounds
[Newly Learned Words @ [2.1 @ 5:33]]

Notes @ Newly Watched Videos

[1.1]
Clima Astrológico del 10 al 17 de enero 2021
By Monica Eyherabide
https://www.youtube.com/watch?v=lXwvQdgXfAQ

[2.1]
New year, same old b*llsh*t?
By Russell Brand
https://www.youtube.com/watch?v=8fWYUP_cdzc

[Written @ 18:51]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list

Timeline Annotation:

[0:00:00 - 0:11:41]

- 📝 [commentary] I took notes in my dialog entry relational journal [1.0]

[0:11:41 - 1:36:07]

- 📝 [commentary] I watched [2.0]

- 📝 [commentary] I took notes on [2.0]

[1:36:07 - 2:15:50]

- 📝 [commentary] I did random stuff -_-

[2:15:50 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Dialog Entry Relational Journal 0 / January 11, 2021 Day Journal Entry
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%201%20-%20January/January%2011%2C%202021

[2.0]
The Romance Between Us
By Squirmy and Grubs
https://www.youtube.com/watch?v=BOUvT5k8Mhs

[Written @ 1:32]

Notes @ Newly Discovered YouTube Channels

MSNaz
https://www.youtube.com/channel/UCtWpEc-kBJYXAwnNgkzYcJQ/videos
[Discovered by [1.0 @ Video Publisher]]

Notes @ Newly Discovered Videos

[1.0]
Test All Things - Sacred Time
By MSNaz
https://www.youtube.com/watch?v=XxrFAnN8X3Q



