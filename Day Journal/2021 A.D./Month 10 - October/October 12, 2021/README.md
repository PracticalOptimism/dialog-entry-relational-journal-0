
# Day Journal Entry - October 12, 2021

### [Written @ 9:16]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:18:50]

- 🚧 [work-progress] I worked on completing the search result for accounts and viewing the account page [1.0]

[2:18:50 - 5:39:14]

- 💡 [initialized] I added a notification list component

[5:39:14 - 6:30:03]

- 😴 [break-time] I used the bathroom and got some food to eat. I ate 3 yogurts. I also sat in the sun for a good amount of minutes.

[6:30:03 - 8:26:39]

- 🚧 [work-progress] I added a qr code generator feature. I also added a qr code reader feature. I haven't tested how the two work together yet but will do so next time. Thanks for watching!

[8:26:39 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin



