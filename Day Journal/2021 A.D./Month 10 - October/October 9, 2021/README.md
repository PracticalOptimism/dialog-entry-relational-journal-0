

# Day Journal Entry - October 9, 2021

### [Written @ 10:03]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:41:36]

- 🚧 [work-progress] I worked on integrating firebase with the account description items [1.0]

[2:41:36 - 3:20:09]

- 😴 [break-time] I used the bathroom and got some food to eat.

[3:20:09 - 6:32:55]

- 🚧 [work-progress] I integrated the account description data to the account description description list component to show the data

[6:32:55 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin




