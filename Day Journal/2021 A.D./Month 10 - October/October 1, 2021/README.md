
# Day Journal Entry - October 1, 2021

### [Written @ 9:29]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:06:40]

- 🚧 [work-progress] I wrote notes and planned and designed things on a sheet of paper or multiple sheets of paper while off screen. [1.0]

[2:06:40 - 3:08:18]

- 😴 [break-time] I took a nap to rest and think a little bit more about what I'm doing next

[3:08:18 - 4:51:43]

- 🚧 [work-progress] I worked on creating the account and transaction data structures for (1) the description and (2) the statistics [1.0]

[4:51:43 - 6:03:22]

- 😴 [break-time] I took a break while thinking about things

[6:03:22 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin




