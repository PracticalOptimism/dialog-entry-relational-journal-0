


# Day Journal Entry - October 6, 2021


### [Written @ 9:10]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:34:09]

- 😴 [break-time] I used the bathroom

[0:34:09 - 4:37:30]

- 🚧 [work-progress] I initialized a working 'updateDigitalCurrencyAccount' feature to update the property id of a digital currency account. [1.0]

[4:37:30 - 5:52:34]

- 😴 [break-time] I ate some food and took a break

[5:52:34 - 6:57:25]

- 🚧 [work-progress] I worked with algoliasearch for a few minutes before realizing you have to pay for having more than 10,000 free search requests. If I am to make this initial first release of the project successful, it would be useful to have a search feature like that provided by Algolia or Algoliasearch. I think I need to get a job and so I have to prepare myself to be in the work force soon. If I don't get a job, I won't have money to pay for the search feature. If I do get a job, I will have money to pay for the search feature. I love working on things for free. But now it looks like I really am not allowed to have everything for free. Free labor is easy. (1) I am a random person on the internet who spends my day time at the local community library and type things on my computer. I love when things are free and handed to me like (1) a free internet connection (2) free bandwidth to publish to YouTube. I don't like when I come up to arbitrary requests for money. Now Firebase has the "free" spark plan which initializes a sort of "free" spirit just like with YouTube. In General. Google is God. Google is Good. Google Has allowed me to come far without money on this project. On the other hand. Working without money is useful for a punk kid like me who doesn't need to rely on mommy or daddy to pay for all the house bills. And yet on the other hand, a source of income that is going to pay for the search feature is way better to work with than not having a search feature. So in essense. I'll have to stop playing my charades and try to find a job that will afford me (1) a new apartment (2) new internet connection for 24 / 7 support (3) money (4) money to pay for things like firebase and algoliasearch and depressingly enough this is not the eratic life style that I could have otherwise lived by (1) being a homeless man (2) being a homeless man who gets to enjoy free resources (3) being a good person who enjoys meeting new people (4) especially homeless people (5) homeless people have a good sense of reality (6) if you're homeless you have no money and rely on people's generosity. I am relying on your generosity to be a good reader and to read this far. That is quite a universal basic reading income that you're providing me by reading this far. Thank you so much.

[6:57:25 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin




