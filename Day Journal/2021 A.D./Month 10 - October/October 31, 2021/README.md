

# Day Journal Entry - October 31, 2021


### [Written @ 14:12]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:28:55]

- 🚧 [work-progress] I worked on adding a way to view received transactions on the your account page [1.0]

[2:28:55 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin



### [Written @ 14:11]

Notes @ Newly Discovered Videos

[1.0]
Quantum Gazing - Hold On!
By quantumjim45
https://www.youtube.com/watch?v=0w54nMH4LCM



