

# Day Journal Entry - October 21, 2021

### [Written @ 9:08]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 8:05:38]

- 🚧 [work-progress] I worked on blocking accounts, signing out and signing in and creating an account and having a password recovery email link [1.0]

[8:05:38 - End of Video]

- 📝 [commentary] I took notes on topics of interest

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin




