
# Day Journal Entry - October 4, 2021

### [Written @ 9:12]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:16:24]

- 🚧 [work-progress] I initialized working with Firebase in the Ecoin 2.0 codebase

[2:16:24 - 2:28:59]

- 😴 [break-time] I used the bathroom

[2:28:59 - 4:13:59]

- 🚧 [work-progress] I worked on adding a settings view for the account information [1.0]

[4:13:59 - 4:47:23]

- 😴 [break-time] I took a break

[4:47:23 - 7:51:03]

- 🚧 [work-progress] I worked on initializing the settings page for 'account information settings' [1.0]

[7:51:03 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin




