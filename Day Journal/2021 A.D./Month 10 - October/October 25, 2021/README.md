

# Day Journal Entry - October 25, 2021

### [Written @ 9:31]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 3:17:26]

- 🚧 [work-progress] I worked on (1) transaction (2) product resource (3) job employment position (4) currency exchange trade pages for the ecoin website [1.0]

[3:17:26 - 7:36:35]

- 🚧 [work-progress] I worked on the "Create Digital Currency Transaction" function [1.0]

[7:36:35 - End of Video]

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin



