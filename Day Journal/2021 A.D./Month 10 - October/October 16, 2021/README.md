

# Day Journal Entry - October 16, 2021

### [Written @ 9:11]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 5:52:19]

- 🚧 [work-progress] I worked on fixing issues that I was having with the data table showing information correctly on the network status page.

- 🚧 [work-progress] I added the data in the values tab of the account statistics section on the network status page.

- 🚧 [work-progress] I fixed issues with the total active accounts not showing correctly. 

[5:52:19 - 7:33:20]

- 🚧 [work-progress] I initialized a few different functions for adding groups.

[7:33:20 - End of Video]

- 📝 [commentary] Salutations






