

# Day Journal Entry - October 14, 2021


### [Written @ 14:46]

Notes @ Newly Created Words

Axosi
[Written @ October 14, 2021 @ 14:46]
[I accidentally typed the word 'yAxosi' while trying to type yAxisLabel. or 'yAxisLabel'. Axosi sounds interesting. 'it's a trap' is the first words that came into my mind. Maybe it's a suggestion from ghosts that this word should mean 'it's a trap' or 'trap'. Axosi.]

### [Written @ 9:16]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 0:37:42]

- 🚧 [work-progress] I added a function to get digital currency account network statistics such as for getting a data table of values.

[0:37:42 - 0:48:04]

- 😴 [break-time] I used the toilet.

[0:48:04 - 7:35:42]

- 🚧 [work-progress] I added bar graphs to the account statistics section of the network status page. I also worked on initializing the data table for the account statistics. [1.0]

[7:35:42 - End of Video]

- 📝 [commentary] 

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin




