

# Day Journal Entry - October 8, 2021


### [Written @ 16:20 - 16:03]

People don't really like currencies.

Creating more currencies is essential for other worldly applications.

But Jacque Fresco had a quote once that said

"People don't care about money" "They care about access to resources"

Ecoin is possibly popular in the future because it enables people to have access to resources.

### [Written @ 9:51]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 6:14:40]

- 🚧 [work-progress] I worked on adding description settings for (1) transaction resource center description (2) product resource center description (3) job employment resource center description (4) currency exchange resource center description

[6:14:40 - 6:17:03]

- 📝 [commentary] I gave a short speech about access to resources

[6:17:03 - 6:35:49]

- 🚧 [work-progress] I added the (5) digital currency alternative feature settings

[6:35:49 - End of Video]

- 📝 [commentary] Salutations






