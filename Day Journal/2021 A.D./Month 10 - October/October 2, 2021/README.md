
# Day Journal Entry - October 2, 2021

### [Written @ 9:14]

Timeline Annotation Legend Tree:

- ⏳ [not-started-yet] - Project Task Not Started
- 🚧 [work-progress] - Active Development on a Project Task
- ✅ [completed] - Completed Project Task
- 🚫 [cancelled] - Cancelled Project Task
- 🤔 [delayed] - Delayed Project Task to Consider more Thoughts
- 💡 [initialized] - Initialized Project Task
- 📝 [commentary] - I wrote comments not necessarily related to the project task list
- 😴 [break-time] - I rested and took a break

Timeline Annotation:

[0:00:00 - 2:32:28]

- 🚧 [work-progress] I wrote a long message about my conclusion of the effort that I called 'the listless architecture' The Listless Architecture was a project archiecture that I used. After many months of trying to find a nice architecture for my coding projects including the Ecoin project that I was working on. "The Listless Architecture" was one of the project architectures that I stumbled upon after my many months of research. It was quite a good architecture.

- 🚧 [work-progress] The names of the folders used in the "listless architecture" did not come to me naturally. Names such as "game-evaluation-loop" and "game-introduction" and "game-manifold" and "simple algebra" were channeled from a ghost partner who helped me write the architecture. The ghost was named Seth. I am very sorry that the "the listness architecture" approach didn't work out for me. I hope that it can work out for you or that iterations on it can work for different projects that you work on in the future. [1.0]

- 🚧 [work-progress] Because I didn't like the architecture and specifically because I relied on unknown concepts having to come together to make complete sense. Those were the stumbling blocks that prevented me from going further. In terms of a good research project, The Listless Architecture may still be useful.

[2:32:28 - 3:15:14]

- 🚧 [work-progress] I initialized the Ecoin 3.0 codebase which uses the code from Ecoin 1.0.

[3:15:14 - 4:19:51]

- 🚧 [work-progress] I started the development server for the previous Ecoin codebase and started thinking and planning around how to continue development using this codebase again.

[4:19:51 - 5:20:37]

- 😴 [break-time] I got something to eat and I took a break

[5:20:37 - 7:00:08]

- 🚧 [work-progress] I tried to work with the Ecoin 2.0 codebase.

[7:00:08 - End of Video]

- 📝 [commentary] This was quite a journey. (1) I thought I could use the Ecoin 1.0 codebase again (the light architecture) (2) I then realized that I liked things about the Ecoin 2.0 codebase. (the listless architecture) (3) I think -_- It was quite a challenge. I'm confused on where to go.

- 📝 [commentary] Salutations

References:

[1.0]
Ecoin
By Ecorp
https://gitlab.com/ecorp-org/ecoin





