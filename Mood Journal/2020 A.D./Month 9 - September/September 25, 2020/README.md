

# Mood Journal Entry - September 25, 2020



| Time Period | Written @ Time | Emoji | Emotion(s) |
| -- | -- | -- | -- |
| xx:xx - xx:xx | xx:xx | Emoji | Emotions list |



[Written @ 0:30]

Goals for how I would like to feel today:


* I would like to feel grateful for what I have today 




Things that I am grateful for:
* My mom who takes care of me by buying food and having water in the house, and providing me a place to stay
* having a house to live in
* having a shower to be able to take a shower
* having a toilet to be in walking distance to being able to move my bowels in a sanitary place
* I am grateful for the food we have (bread, peanut butter, pasta)
* I am grateful for having clean drinking water
* I am grateful for having a peaceful living environment in our neighborhood with other people
* I am grateful for a relatively peaceful planet
* I am grateful for having a computer
* I am grateful to be able to use the internet
* I am grateful for abundant resources like YouTube and Instagram and Gitlab and Facebook that provide services for free and are relatively peaceful in terms of their efforts to really try to align towards human values that we can all live by . . I don't know much about these topics but from my own point of view people are taking care to do their best and I'm personally not always going to be there to say what the best means and so its better to assume they are trying when that's what they say they are doing and to trust in their efforts or something like that . . free things are really quite cool . . in alternative realities . . maybe these things are really dreams to be able to upload videos and videos and videos and YouTube doesn't charge you a penny for the resource . . and it would be a nightmare otherwise and maybe those nightmares are being lived out by alternative versions of ourselves and so maybe its really even difficult to be a people in union for the topics of abundance or something like that . . well . . in any case . . its still a topic for discussion whether abundance is well for all . . these things are up for conversation and yet they are still quite reasonable and for myself in the personal measure I am quite grateful for all the resources we have and for all the great people that make these resources available and think about the wellness of each of us in some respect or in the respect that makes the meaning for them to think about as self soverignty to make decisions maybe be the ideal for each individual to come to their own creative understandings but that is something I odn't know about and now I'm only rambling on about things that I'm not very familiar with or topical to discuss in my excited mood at the moment

* I am grateful to have electricity
* I am grateful to have lights working in the house and so I can stay up and watch the Joe Rogan podcast by night . . this topic of lights being invented and allowing people to stay up later than usual is in discussion here [1 @ 60:03]


Notes @ Quotes
"
A friend of mine said to me . . he's totally right . . 

And it's something that I've discovered over the past 5, 10 years . . That if you want a super power . . the super power is to listen . . 

Is to really listen . . to what . . what people are actually saying . . and not what you want them to say . . or . . or . . how you . . you know . . To really really . . properly listen . . because . . people are just not listening . . to people anymore . . 

And I have . . you know . . in one respect . . I wri- . . I write books . . and I write really long articles . . and when I do those interviews . . with people . . I interview them for a long time . . and I transcribe it all myself . . I don't use a computer . . or pay anyone else to do it . . And . . it's so dull . . and I hate it . . but that's where I really understand what people are saying . . 

And that's where I get all the ideas for structure . . 
"
-- Jenny Kleeman
[1 @ 01:10:10 - 01:10:48]


Notes @ Quotes
"
The style of interviewing is a kind of . . a . . a . . a dance that you do with people . . instead of . . asking a question where you think the answer might be interesting . . and then you really listen to the answer . . 
"
-- Jenny Kleeman
[1 @ 1:11:27 - 1:11:36]



[1]
Joe Rogan Experience #1539 - Jenny Kleeman
By PowerfulJRE
https://www.youtube.com/watch?v=EED-LEWOCv0&pbjreload=101

