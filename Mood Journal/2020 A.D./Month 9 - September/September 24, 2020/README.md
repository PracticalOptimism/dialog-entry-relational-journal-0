
# Mood Journal Entry - September 24, 2020




| Time Period | Written @ Time | Emoji | Emotion(s) |
| -- | -- | -- | -- |


[Written @ 23:53]

I felt . . good today . . when I woke up in the morning . . I woke up from a dream . . and . . in the dream . . I saw a lot of things . . there were many scenes . . throughout the dream . . 

Well . . maybe I will write about them some time in the dream journal entry . . 

I had a cry after waking up from the dream . . I was tearful . . because I was excited about . . the creative posibilities . . that dreams . . enable . . and that you can . . well . . really I was crying because . . I think I am starting to believe more and more that we are living other lives and . . so for example . . when I die . . or . . if I die one day . . I can go to live other lives . . and those other lives . . uh . . if or when . . uh . . I'm not sure which word to use . . the future holds so many opportunities and possibilities . . in terms of what we can achieve we human longevity technology if we want to live for more than 100 or more than 1000 years . . it seems . . biological . . and consciousness related . . if we have the interest in achieving these things . . and maybe also if the environment is socially stable enough for people to have the time to pursue these things . . in their professional or personal lives . . uh . . 

I felt . . like I . . was living these other people's lives . . I was proud of what I was experiencing in these other people's bodies . . uh . . uh . . In one of the dreams . . I was a worker . . at a . . station . . somewhere surrounded by snow . . and . . uh . . I was logging in . . to get into the building . . where I worked . . and the password . . was to enter . . a button . . I had to press a button . . and then speak into the microphone . . 

The button said something like . . "love" . . and . . I pressed that button . . and then . . I spoke the word "10" . . into the microphone . . I was . . uh . . when I look back on this dream . . I . . think . . I am proud of the sound of my voice . . it was a gentle soft voice . . and gentle soft sounding voice . . from this other person who I was living as . . in this dream body . . and I spoke the word . . "10" . . and . . uh . . well . . the whole experience was magical . . and the task was so simple . . to press a button . . uh . . in my waking life . . it makes me think that maybe I'm taking a lot of experiences for granted . . that I'm not . . experiencing the moment . . in the magical way that it would be experienced . . from a visitor . . someone else who is only visiting . . in a . . dream body vehicle . . or something like that . . and so for example . . uh . . anything at all in my life could really be very magical . . uh . and I can take it into account and be happy and joyful . . in the present moment . . and that joy can be resonated throughout the whole universe . . and whereever someone wants to have a happy or joyful dream memory . . this version of . . this version of myself can also be uh . just happy I guess . . I can just be happy and satisfied with . . the magic of being where I am today . . 

. . 

Well . . 

Uh . . 

I uh . . spent some time . . watching pornography later in the day . . and that was also enjoyable . . 

Uh . . I have a pornography diary journal on my other computer account where I track the thoughts that I have . . and the newly discovered genres of pornography that I run across . . uh . . or come into . . 

Uh . . mostly . . I'm still watching gigantomastia . . macromastia . . nipple slip . . nipple piercing . . [nipple rings are rarer than nipple piercings . . uh . . I think . . I'm not sure . . rings are . . round . . and uh . . maybe that's the main distinction in my mind . . and so . . uh . . well . . mostly I see . . piercings . . that are . . uh . . linear . . in comparison to round rings like . . circular discs . . uh . . like the ring around someone's finger for a marriage . . uh . . those types of rings . . for nipples are quite new to me . .]

Uh . . while getting on to live stream . . I was . . nervous . . well . . In the beginning of the live stream I was nervous . . I am quite nervous . . sometimes . . I feel like I am wasting a lot of time talking . . 

Other times I feel uh . . just nervous . . and I don't have any thoughts in my mind . . and am afraid to type things into the computer . . uh . . the thought of having someone read what I write is kind of strange to me . . 

I don't know what people think . . and I'm trying not to offend people . . when I say things . . I would like to also be . . educating . . on thoughts that I have . . and not really representing myself as a person . . in the world . . mostly I would like to be a presentation of a certain genre of people who are alive . . or who are experiecing the life on Earth . . in the present of 2020 A.D. as it is known . . I don't know . .

Uh . . yea . . so . uh . . I hope I'm not being offensive . . or hurting anyone by writing messages like this . . or anything else that I do . . if I talk about sexual related topics for example . . I know the sexual topic is taboo for a lot of people . . and I think that is uh . reasonable thing . . and uh . well . . there could be other topics that are taboo that I'm not aware about . . uh . . and maybe I am infringing on the culture uh . . that thinks it is taboo to talk about those things . . uh for example . . maybe writing down your goals could be a taboo thing to do in some cultures . . uh . . 

Maybe it's uh . . well . . I don't know . . more about going along with the rhythm of life or the flow of life . . and not always measuring time and things like that . . or not always . . reviewing your own thoughts or something like that . . 

It is reasonable . . I think there are many cultures and there are many beliefs . . and . . uh . . . . there are many things . . I'm trying not to be so offensive to the people that believe these various things . . uh . . but that is difficult to do since I am really ignorant in the diversity of people out there with all the thoughts and beliefs . . uh . . well uh . something like that . . I'm not sure even how to type sometimes . . is it that I'm taking too many assumptions and granting myself a privilege to speak at all . . these resources of computer resources like storage and bandwidth . . are things uh . . I'm taking for granted . . 

I'm taking for granted this privilege to work with uh . . resources like . . YouTube and Gitlab . . and I'm really grateful every time I am going on here . . and I would like to be grateful to have a computer . . and to have these resources available . . uh . . but just typing makes it really strange in a way that maybe its not really going to come across all the time . . uh . . or . . maybe its not really represented in one or two things that I say . . things that have the ability to offend people because of the differences in how . . uh .  information is organized in our minds or something like that . . 

uh . . 

Well . . 

I got less and less nervous . . over the period of the live stream . . 

but not really . . Actually I didn't get not so nervous . . until I started writing . . in this daily mood journal . . uh . . normally I don't write here . . and I would leave something like an emoji response . . 

Uh . . well . . the emojis are nice . . . they were an experiment for me to see how writing in a mood journal could feel like . . and yet . . they uh . . . well . . I'm not sure . . maybe writing my moods and feelings is really useful . . and so . . uh . . now for example . . I would say . . I really . . uh . . . excited . . or thoughtful . . uh 


I would really like to try this bullet journal mood tracker for mental health idea by Unicole Unicron [1]




[1]
The Best Bullet Journal Mood Tracker for Mental Health (BPD, Depression, Anxiety)
By Unicole Unicron
https://www.youtube.com/watch?v=eU4uqrjqzpE







