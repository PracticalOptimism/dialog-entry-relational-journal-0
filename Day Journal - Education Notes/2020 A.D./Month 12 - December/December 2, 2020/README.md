


# Education Notes Day Journal Entry - December 2, 2020


[Written @ 1:30]


### Realtime Notes While Watching [1.0]

- [Starting To Write Notes @ xx:xx]
- [Ending Writing Notes @ xx:xx]
- [Last Updated Notes @ end:date]

. . . 


. . . 

[Commentary on The Notes Taken So Far]:


. . . 













[1.0]
RESIST | Communities Fighting For Justice
By Patrisse Cullors
https://www.youtube.com/watch?v=HKKz6QVdwFQ&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc

[2.0]
RESIST | Meet The Activists Disrupting LA’s Unjust Justice System / part 1 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=2DI2pY4Fn40&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=2

[3.0]
RESIST | Why We Fight For Justice / part 2 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=lclBCzXrBBo&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=3

[4.0]
RESIST | JusticeLA Takes A Direct Action Against Institutional Racism in LA / part 3 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=rQKyqgtGz_A&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=4

[5.0]
RESIST | LA County Students Demand Changes to School Policing / part 4 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=ucinZPhAq0I&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=5

[6.0]
RESIST | How “Cash Bail” Criminalizes Poverty In Los Angeles / part 5 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=nJjO2ENpwig&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=6

[7.0]
RESIST | We Know The Justice System Is Broken. Let’s Try Something New / part 6 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=iOoYbhz-BV4&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=7

[8.0]
RESIST | Fighting ICE Raids in Los Angeles / part 7 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=1X4OA0QXUoU&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=8

[9.0]
RESIST | Injustice, Resilience, Power: Trans Lives in LA’s Prison System / part 8 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=nWPMNJW6qZ0&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=9

[10.0]
RESIST | Activists Confront LA’s Top Politicians / part 9 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=cWS4eXyifOs&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=10

[11.0]
RESIST | “Why Didn’t LA’s Prison System Protect My Son?” / part 10 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=XF44dNBsB6o&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=11

[12.0]
RESIST | Bringing The Prison Abolition Movement To Antelope Valley / part 11 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=xqvdmJtLwQo&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=12

[13.0]
RESIST | Politicians Betrayed Us, So We Fight Harder / part 12 of 12
By Patrisse Cullors
https://www.youtube.com/watch?v=6TPWW5mg4lo&list=PLCNGpm1QRsbKshCmjmd5IJ51TKb6Z3JNc&index=13










