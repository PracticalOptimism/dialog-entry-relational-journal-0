Newly Created Words / Terms



Insed

Insted
[Written @ July 21, 2020 @ 19:15]
[accidentally typed “insted” instead of “instead” and this new word resulted . . I’m not sure what it should mean yet . . but it seems like a new word . . . here it is . . . . ]

Insead
[Written @ July 21, 2020 @ 19:14]
[accidentally typed “insead” instead . . . . of “instead” . . . . I’m not sure what this new word should mean yet . . . . at least recording it here will allow me opportunity to think about it in the future if I ever come back to review this diagram in my diary entry notebook dialogue . . something like this . ]

Broguth
[Written @ July 21, 2020 @ 19:14]
[accidentally typed “broguth” instead of “brought” . . . . . . not sure what this new word should mean yet. . . . it seems like a new word . . too me anyway . . . hmm . It also reminds me of . Uh . A name of a character in a fantasy role-playing game like D&D (Dungeons & Dragons) or something like this . . “Broguth” . . or “Brogarth” . . . . . . “Brogarth” the betrayer . . . . Broguth the contender . . . something like this . . . very cool . . . soothing for the spirits of fantasy role playing genre expeditions whose minds are creatively content to create new role-playing scenarios . ]

Smask
[Written @ July 20, 2020 @ 4:16]
[Wearing a smask is a term to refer to wearing a smile that’s a mask to otherwise share feelings of happiness even if one doesn’t necessarily feel happy . A smile + a mask is a nice tool to avoid loneliness from creeping more frequently than is necessary . I’m wearing a smask now . . smirking . . the term smirking sounds like the term smask . . . it’s like ironic smiling . To smirk . In irony . A smask is like a smirk but isn’t necessarily physically expressed like a smile shows the curvature of the corner of the lips . :) . :( a smask can be a feeling like holding onto the idea of being happy when you’re not necessarily thinking that you’re happy in some sense . Anger smasks are useful for working on projects when you otherwise feel angry but want to be happy and get your work done . You can wear an anger smask to encourage yourself to grow away from the pain that resulted in the anger . It’s . A sadness smask that can help you come back the next day to realize it was just a phase in the moon cycles or a rhythm of nature that made you feel so angry or sad and then you don’t have to be so sad . A smask is an obscure concept meant to be a recovery mechanism that helps one secure a lively present hood experience thinking more about a smask 

Pro Websiter
[Written @ July 16, 2020 @ 8:28pm]
[I was looking for a name to call the website for Ecoin in the ecorp-org/ecoin repository under the precompiled/consumer/web-browser directory hierarchy and typed “pro-websiter” . . . . . “websiter” is a new term as well and so a “pro websiter” can be someone who visits websites quite frequently . ]


Websiter
[Written @ July 16, 2020 @ 8:26pm]
[accidentally typed “website” as “websiter” . . . . . . I like the sound of this word “websiter” . . . it sounds like someone who visits like a website . ]

Oris
[Written @ July 15, 2020 @ 7:30pm]
[accidentally typed “ormis” as “oris” . . . not sure what this new word should mean . . . . . . . . . . . . . ]


Ormis
[Written @ July 15, 2020 @ 7:29pm]
[accidentally typed “promise” as “ormis” . . . . . . . . . . . not sure what this new word “ormis” . . . . . . . should mean . . . . . . ]


Melloactively
[Written @ July 15, 2020 @ 17:21]
[Mello- meaning “futurity” or “relating to the future” . . . . . . . a word “retrospective” means to look back in time . . . . . . . . . a few minutes of research in “looking into the future” relating to an antonym for “retrospective” . . resulted in few results . And so . Neo- meaning “new” was tried to produce “neospectively” or “neoactive” . . . . this word sounded strange or different and not so nice to relate to retrospective . In some sense . “méllo̱n” means “future” . . . and so the word “mellonspecitvely” or “mellonactively” was also tried . . . . . . Melloactively or Mellospectively . . . without the “on” sound or “on” character sequence . . . . . sounds nice . And could be nice to adopt as an antonym for retroactively . Something like this . 

Forgivenment
[Written @ July 11, 2020 @ 10:59pm]
[Accidentally typed “forgiveness” as “forgivenment” . . . . . this word looks really cool . It reminds uh . It reminds me of the word “government” . . but with “forgiveness” . . . . . forgivenment . . . . . . . foreign government . . . . . . . . . . . . . foreign . . . . . . for. . for. . . uh . . yea. . . uh . it’s . Yea . I guess forgive uh . Kind of looks like foreign if you squint your eyes . Not sure . . . it’s a psychological distance measuring device to look at the similarity of these two words to one another . . . . . . . the meaning might be different but the appearance of characters could be read like uh . An art painting I guess . Anyway . Not sure what this new word “forgivenment” should mean . . . it seems like a new word anyway . . . . forgive . . . I . . I like forgiveness . . . . uh . . . I was typing the name “Total Forgiveness” which is a show by “Ally Beardsley” of “College Humor” and . . hmm . . . well . Yea . I guess it’s . . well . . I really like the title “Total Forgiveness” it’s really cool . I’ll leave this here for now . . not sure what else to say . “Total Forgiveness” is really cool . [3 @ 2:55 - 3:27]]


elso
[Written @ July 9, 2020 @ 9:42pm]
[a typographical error of “also” . . ]

scirp
[Written @ July 9, 2020 @ 9:37pm]
[accidentally spelled “scripts” incorrectly and resulted in “scirp” . . not sure what this new word should mean . . . . . . . it looks like a new word . . . but need to do research to see . . . . well . . . . scirp could mean “to do research” . . . . . . hmm . . scirp . . I’m scirping . . . hmm . Seems . . . .hmm . . scirp . . . skirt . . . skirt is a term used that means something like . . . too get out of here . . or to leave immediately . . or to disappear from a scene or location or site . . . . skiirrrttt . . . I’ve heard this term being used this way . . . . . scirp . . . . . scirp . . . . scirp . . . . . . hmm . . . . skirt is like the sound a car makes when the tires are rubberized against the pavement . . . . . . . skirting . . . . . . . . . . . . . . . . . freewheeling? . . something like this . . . . . . . . . . . . maybe it’s like . . if your car tires are rubberizing against the pavement . . it’s a symbol that means your car is traveling very quickly . . and so . . . if your car is traveling very fast . . . . . .. . . you are wanting to go somewhere . . or leave a location very soon . . . . . skirttttt . . . . skirrttt . . . . . . . . . skirt-skirt . . .. . alright . Well scirp . . scrupulous . . . . . . hmm . Well . . I’m actually not sure what this term “scirp” should mean . . . . it sounds too similar to skirt . . . . skirt-skirt . . . . . . . . . . and “skirt” itself also relates to “dress” . . . . . . . . or “short dress” . . . well . . . . scirp . . . . . . . . not sure . “To do research” . Hmm . . seems weird strange or just personal taste that I’m not a fan . Research seems more like . . . elegion . . . or alaegon . . . something elvish . . hahahaha . . hahaha . . elvish . . . high class . . high fashion . . . . . . rich . . tasteful . . . skirt . . . . . seems . . deviant . . . . . . . . . . . . . . . . hooded . . dark . . . . . . rascally . . . . . . . . . . . . . . . haha . I suppose both could be meaningful hahaha . . so . . let’s try this . . scirp . .  oh gosh . . is scirping about ants . . . I’m scirping on the topic of ant biology . . . ahh . . quick research maybe ? . . fast . . . quick . . . . . . . not so . . elvish or in-depth research . . . . . . . . . . . something to get started . . . . . scirping . . . . . . . . . . . . . breadth-first research . . . . . . . just to scurrow the basket of what’s available before delving deeper into the mines . . ? . . something . ]

bouat
[Written @ July 9, 2020 @ 9:17pm]
[accidentally spelled “about” as “bouat” . . . this term reminds me of the word “Borat” . . . which . . If I’m not mistaken is an American movie about a foreigner named “Borat” . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . well . . . the foreigner seems to be from “Kazakhstan” . . . . . . . . . . well . . it is a comedy genre film . Right . I’m not sure what it’s really about . . or whether I watched the movie or a scene here or there from somewhere . . . maybe youtube . . not sure . . . . . . . I remember people talking about it . . but this was many years ago . . more than 8 or 10 years ago . . . I was in middle school or a young teenager . . in early high school . . . . . . . . . . . . . . . . . . in an American school district . . ]

funciton
[Written @ July 9, 2020 @ 8:52pm]
[accidentally spelled the word “functions” incorrectly . . . . . . . . . . funciton's . . . constant function name declaration string values .. . . . . so if you’re sitting in a computer programming class and thought you named your function funkyTown . . . and you go back to look and see your function is called tongueMucousSquirrel . . . . . . . . . . . then funkyTown was an appropriate abbreviation for what you meant but it’s really semantically based and not the static type checker’s fault for fumbling the football of your meaning into the hands of the error disarrangement management machine that discards any sense of human reasoning patterns to think more of the pascetelian dactrines that pedicalirony can’t seem to regret recreating in the pestipal pedistol pastik porcupine pinewindfblower flower mix cake that seems to be the skinniest dip in the hardest pool of the winder treadmill lake. ]



Mocah
[Written @ July 9, 2020 @ 8:20pm]
[accidentally typed “mocha” as “mocah” . . . not sure what this new word should mean . . ]


Cof
[Written @ July 9, 2020 @ 8:09pm - 8:14pm]
[accidentally typed “cof” instead of “for” while . . . . . . . . . . saying “hey” . . . a lot . . . .  . . . “hey” . . “hey” . . . “hey” . . . “hey” . . . “hey” . . . “hey” . . . “hey” . . . “hey” . . “hey” . . “hey” . . “hey” . . . “hey” . . . “hey” . .  .”hey” . . . “hey” . . . “hey” . . . “hey” . . . “hey” . . . .  . . . . how do youou say . . . . hey . . . 100 times ? . . . .  .. . .  cof 100 . . .  . . cof 99 . . . . . you can say hey any number of time . . . cof 9 thousand 9 hundred ninety nine . . . . cof . . . . . . . . . . . cof two thousand . . . . . cof . . . . . . . . three . . . . . . . . . . . . . hey . . hey . . hey . . . . cof 1  . . . hey . . ]

Ebene
[Written @ July 9, 2020 @ 8:06pm]
[accidentally typed the word “benefits” as “ebene” . . . not sure if this is a new word . . . . . and if it Is . . what should it mean . . not sure . . . . . . . . will have to find out later . . . but it does look like a cool word . . . . . . and sounds interesting . . ebene . . . . . . . . . . .. . . hmm . Sounds like ebony . . . . . . . . . black . . or dark . . . something . . . . . . . . or it could be a a chemical compound. . . . . . . . . . . . . . . ebene . . . . . . like pyrene . . . Rene . . . is REEN . . . . . . not like Reh-nehy . . . . .  . Reeeen . . . . . . Bean . . . . been . . . . . . . or beh-nehy . . . . . . .  . . . . . . . . . not sure how to pronounce this term . . . . . . eh-beh-nehy . . . . . . . . . . . . . I’m a fan . ]

Code bode
[Written @ July 9, 2020 @ 6:14pm]
[wow . Is it the evening where I am . . 6pm? . .. . . it’s so sunny outside today . . it feels like it could be the morning as well . . . . 6am ? . . that would make sense . . . AM . PM . AM . PM . What is the difference here ? It feels like a nice day with the wind blowing . . ]

Juy
[Written @ July 8, 2020 @ approximately 11:51pm]
[Accidentally spelled “July” as “Juy” . . . . . this typographical error led to a new fruitful term . . it looks quite cool . . . . . . . . . . not sure what meaning should be ascribed to it. . . . . or if there’s an existing meaning for this word . . . . well . .. . . . . . . I’m not receiving any immediate results on this new word sequence character sequence . . not sure what it should mean . ]


Pollicat
[Written @ July 8, 2020 @ 11:49pm - 11:51pm]
[Accidentally typed “pollinator” as “pollicat” . . . . . . “pollicator” could be the extended word that I could have typed if I continued to spell the typographical error with the ending of the original word I was planning to type . . . . . . Pollicat . . . . . . Pollicator . . . . it . . sounds interesting . . or looks rather cool . . but I’m not sure what this word should mean at this time . . . . . ]

Gren
[Written @ July 7, 2020 @ 9:31pm]
[This could be the name of a boss in a video game . . . “Gren” . . . . . . . . . . . . . . . . Gren is . . . . hmm . . sounds like . . . . a boss . . . philosopher king ? . . . . . . . . . . . . . . . . . . . . Gren . . . . the great . . . the gracious . . . . . . . . . . . . . . . . . . . . . simple Gren . . . . . . . grounded Gren . . . . . . . . . . . . . . . . .and you have to fight Gren like in dark souls you fight giant tall humanoid creatures with weapons . . . . . . .. . . . Gren . . . is gracious . . . . . . . . . . . . . . and yet . . . . . what circumstances . . in this video game world would make Gren so worthy of contention . . . . . well . . it depends on the game I suppose . . . .  . . . . . . hmm . . not sure . . there’s plenty of room for creativity . . . . Gren just sounds like a cool name . . . sorry if your name is Gren and this is absolutely offensive to talk about Gren as a person like this . . . . I was just reminded of a tall person with large body mass . . . . . and they . . are kind . . and . . thoughtful . A king of sorts . . but also . . . this . . large . . largeness characteristic . . reminds me of a lot of bosses in Dark souls . . . they are tall . . and big . . and so . . that gave me the idea . . that maybe Gren . . is an opponent . . . someone you could be tasked to battle in a video game . . . even though they are nice . . that makes you wonder . . what could they have done . . to aquest a challenger . . . . . . Gren . ]


Og
[Written @ July 7, 2020 @ 9:29pm]
[Og . . . . I spelled “of” incorrectly . . . . . and this . . . resulted in “Og” . . . . . . . . . while typing on my keyboard on my MacBook Pro . . . . . . . . . . “Og” . . . . . . . . . . . this sound reminds me of Ivory . . . . . . . Og . . . . I . . think . . green when I see this word . . . and the sound . . . . . is . . . . . . . . . . . thick . . . . . like a ivory tusk plant . . . . . . . . . . . . . . . . . with veins . . . . . . . . . . . . . thick . . . . . . strands of tissue . . . . . . . . . . . . . . . . . . . . . . hairs of plant vasculature . . . . . . . . . . . . . . . . . . . Og . .  Green God . ] 


Thoi
[Written @ July 7, 2020 @ approximately 9:18pm]
[not sure what this word should mean . . . . . . . I was trying to type “thought” . . .. . . . . and accidentally typed “thoi” instead . . . . . . . . . . this new word seems . . like the name of an important person . . . . . . Thoi . . . .. . . . . . like Thoth . . . . . . . this name reminds me of a god . . . a God . . a God . . . . a person of great significance . . . 


itendidty
Itendidity
[Written @ July 7, 2020 @ approximately 9:18pm]
[tried to spell the word “identity” . .  . . attempted to spell the word “identity” . .  and failed to do so . . as a result . . . the word “itendidty” emerged as a typographical error . . . . . . it sounds kind of interesting . . . Iten . . . . didty . . . . . . . . . . . . didity might be more easier to say . . . from my psychological perspective at this time . . . . itendidity . . . . . . . . . . . . . . . . . . . . . . itendidity . . . . . . iten . . . . didity . . . . . to attend to something with all your heart . . . . . . to itendidify . . . . . . . . . . . itendidify . . . . . . . . . . . . to attend to something will all your might . . . . . all your courage . . passion . . aspiration . Inspiration . Something like that . . . that sounds like a good . . uh . Word for this sort of meaning . . . the word itself sounds strong . . . and sparkly . . . the spark . . . . that’s another meaning for this word . . . . the spark . . . . . . . . . . . .  . . . itendidity . . . .  . . . . . . . . . . . hmm now some use cases . . . . ]
[usecase #1]: itenditify your whole heart . 
[usecase #2]: be yourself . Move with passion . Be courageous . Your identity is free from your itendidity . . . . . . . your fight is who you are in the present moment and who you are is free from fighting . You got this . You’re awesome . Do your best . Itendidify your greatest honors  . . . . ]
[usecase #3]: Itendidify your greatest honors . [translation]: spark your courage . 


turm
[Written @ July 7, 2020 @ approximately 9:18pm]
[almost wrote the word “turm” with a “u” instead of an “e” . . . . as the original word I meant to spell was “term” . . . . . . . . . not sure what this new word “turm” should mean . . . it reminds me of a . . . . . “tremor” . . . . . . . . . those “giant tremor worms in that one movie . . . . . . . . . . . . “ . . turms . . . well . . that’s one possible itendidty . . . of this word . . . but it’s not necessarily the only one . . . . just an idea. . . . not sure . . . . . . . . will leave this term here for now and see if it can serve . . . . . . uh . I don’t know . . . I’ll thoi . . . I’ll think of something later . . . . something . . something . . . . . . . . . . . . something something . Something . Something . Something . Something . Something . Something . Something . Something . Sseogthin . ]

milliesecond
[Written @ July 7, 2020 @ 9:09pm]
[Accidentally typed “milliesecond” with an “e” instead of “millisecond” . . . . . . . . . . . this term reminds me of the Song . . . “A millie” . . . by a great variety of lyrical artists including Lil Wayne and Asher Roth . . . . . “A Milli” seems to be a song that Lil Wayne initialized into the popular culture and a lot of other musicians and artists created their own variation of the song . . . . . it’s quite a cool idea . . . . a Milliesecond . . . . a Millisecond . . . . Millie . . . apparently even though it reminds me of the “A Milli” sound track series . . . . . .  . . . . . . . A Milli . . . . . . . . . . is spelled without an “e” . . . . . . . . . . . . hmm . . . . . . well . . . . . I was going to suggest that there should be a term 


Guari
[Written @ July 7, 2020 @ 6:54pm]


Guardi
[Written @ July 7, 2020 @ 6:53pm]
[An abbreviation for “Guardian” this is a new spelling . 


Consue
[Written @ July 7, 2020 @ 6:52pm]
[Written by guardi


Belons
[Written @ July 7, 2020 @ 6:46pm]
[Belon is a word that means “faith” . . . . I discovered this new word while intending to type the word “belongs” . . . Belons are like bello systems thet pump and breath air into the heart and lungs of the most faithful people on the planet . It’s a myth that Belon people breath air like children breath water sacrificing their every neck to bring people new food and water resources . Water is important to be potable and unique for all people to consue

notificatino
Notificatino
[Written @ July 7, 2020 @ 6:46pm]
[Every word is unique and belon

Ephemeral Statistics
[Written @ July 7, 2020 @ 5:30pm]
[definition]: Ephemeral Statistics are any statistics gathered over time ? 

[examples]: time of day, position of a group of players in a multiplayer game, record of quest log activity for a given player in a video game . Number of completed quests, number of abandoned quests, at any given moment in time . Transaction history list for a given bank account . 

[example]: Transaction history list for a given bank account . 


Final Statistics
[Written @ July 7, 2020 @ 5:30pm]
[definition / hypothesized meaning / guess at meaningful terminology for this concept]: Final Statistics are extrapolated from ephemeral statistics for a particular use case . One might want to rely on final statistics to present a particular viewpoint while the ephemeral statistics data could be a burden to represent in any particular usecase . 
[definition #2

[examples]: 

[example]: To show the account balance history for a given bank account, the ephemeral statistics of the transaction history list for the aforementioned bank account would be useful to extrapolate from . One might need to use an summarized list of the account balance for any given moment in time which in order to do so would require a calculated value from the original transaction history list . The transaction history list might be prohibitive to carry around all the time with more information than you might need . . like the memorandum and the sender or recipient of the particular transaction . . . and so . . . for your particular account . . one can calculate the account balance history without information like the memorandum of each transaction . . and the sender or recipient of each of the transactions . . and so the final statistic remains to be the account balance history . . or in other words . . the amount of currency the account had at any given moment in time . . . which is a final statistic extrapolated from the original ephemeral statistic of the transaction history list . . . . . 

Defi
[Written @ July 7, 2020 @ 5:53pm]
[accidentally typed “defi” instead of “deif” . . . . not sure what this new word should mean yet . . . . . . . . . . . . . . . . . . I defi . . . moving on to get to other things To do . . . . . . . . shoud . . . is a new word. . . but I really should . . go . . defi could be an abbreviation for definitely .. . . . . . . . . . . . I defi should go . . do other things right now . . defi out . ]

Deif
[Written @ July 7, 2020 @ 5:52pm]
[accidentally typed “deif” instead of the intended “definition” . . . . . I intended to write “definition” but my fingers slipped and I typed “deif” on my keyboard instead of the originally thought-of “definition” . . . a grey eyeball . . . . deif . . .. . . . . . . . that’s the new meaning of this word . . . . . . . a grey eyeball . . ]

Mayhe
[Written @ July 7, 2020 @ 5:44pm]
[accidentally spelled “maybe” as “mayhe” . . . . not sure what this new word should mean yet . . . . something something]

Vau
[Written @ July 7, 2020 @ 5:42pm]
[accidentally typed “vau” instead of “value” . . . . . . . not sure what this new word should mean yet . . . . . . well . . maybe . . it’s not a new word . . it seems like the Hebrew alphabet already has a character by this name . . . . . . . . . [2]]

Unived
[Written @ July 4, 2020 @ 6:19/6:20pm]
[Accidentally spelled the word “Unive” instead of “United” . . . . . hmm . . the autocomplete feature of my computer notepad editor . . spelled the word “Universes” . . . . . . . . . . . . . . .. well . That’s really cool . . I didn’t see that at first . . . . . . . . . . Unived . . . . Unive . . ive . . . ive. .. . . u . . . . u . . nn .. . . nn . . . nnnnnn . . . nive . .. nive . . yea . It reminds me of a knife . . knives . . .. . unives . . . . . unives . . . . unived . . . but it also reminds me of olive . . . . . . olive . . like a black olive . So cool. . . . . . it’s like . . . war and peace . . . knives and olive branches . . . . . Unived . Maybe this term could mean a shower of universes of war and peace . . huh . I have this image in my head as I think about it . . . it’s like . . a blue sky . . . . peaceful . Beautiful . . and then there’s light . . . and the sky . . is raining light . . and the light is like water . . the water is . . like a stream of universes . . universes with conflict and duality . War and peace . Where all sorts of people live . All sorts of stories are told there . From all sorts of cultural backgrounds and untold mysteries of jungle dwelling people white and black and mixed skin colors . . skinny people, smart people stupid people brown people red people fox people bread people fat people bread toaster oven people computer people thinking people running people . . tribes of runners where the whole community goes for runs together in the morning in large packs . . . tribes of hunting people . . I’m a vegetarian myself so I’m like bleh . But yea . Lots of meat eating people yum . Juicy steak . Juicy delicious meatloaf from wild animals like new types of elephants or rhinoceros . Ducks and chickens and cats . And hairless pigeons . People that don’t eat . People that don’t sleep . So many tribes and religions and cultures and thinking and thoughts and books and wisdom and writings and wow . That’s weird to think about . I kind of get a headache thinking about all that stuff . But uh . It’s interesting anyway . . how in our own way . It’s a special place to have a universe like ours . Such splinters of joy and creativity . So many ways to live . Let’s go running some day . Is that the message of the universe ? Hehehehe so philosophical . . sorry . Running is a cool metaphor those . Awesome . We’re back :D]


presen
[Written @ July 4, 2020 @ 6:17pm]
[awesome . Presen means awesome . presen. . . awesome . Alright I’m hungry I’m gonna go eat . That’s presen ! . hum . This is a weird word to mean awesome . Presen ? . . kind of sounds like prison ? . . . . . . -_- . . . .. . hope you go to prison ? . . . .  ugh . . .  .. . . . . . . presen . . press . . . en . . . . . . pressssss . . . . eeennnnn . . . hmm . . yea . Maybe some words shouldn’t mean some things just because of how how they sound and feel to the audience . . in this case how it feels for me . . . . uh. . it just feels weird . Personal dialogues are a way to calibrate your thinking to see how you feel when you think . . . . your thinking is interesting to calibrate your patterns to preference and ease of learning and personal development . . . yo u know ho w yo u can so m e t I m e s co m u nice with e a spouse in different ways and they can understand you ? . like pumpkin can mean sweetheart . . or nice person . Or good person . Or manly lady can mean . Hmm . . you’re being tough today why don’t you relax a bit. ? . . or it could mean . You’re a hardworking person . . good job . . manly lady . . or it could mean other things depending on the people who are using this terminology between themselves . . . a presen is a persona name . Like a language of a person that they use to communicate with other people . persona name a persona name . A persona name . A persona name . A persona name . hmm .. . . . persona name . . I like the idea . . not sure what it’s about though . . a persona seems like a personality . . . . . . . or a presentation image of how someone presents themselves ? . . . .uh . . . a name . I’m so sorry . I’m gonna go eat . ]

indep
[Written @ July 4, 2020 @ 6:17pm]
[indep is a word that emerged accidentally from a typographical error while attempting to write “intended” . . . . . . . . . . . . “indep” reminds of the word “independent” . . . . . . . . . . it could perhaps be the short-hand meaning for the word “independent” . . . . . . indep . . . in depth . . . . . . . in depth . . . interesting . Today is Independence Day for the United States]


dDio
[Written @ July 4, 2020 @ 6:14pm]
[accidentally added a “d” in front of “Dio” and “dDio” was the result . . . . . . . . . . . this new word is interesting to read . . . . like McNugget . . or MacField . . or MacFarland . . . . . . . there is a capital letter in the middle of the word . . . . . . dDio . . this is such a cool word . ]


Dio
[Written @ July 4, 2020 @ 6:13pm]
[accidentally typed the word “Dio” instead of “doing” . . . doing was the originally intended word . . . and . . well . . maybe Dio the President gets things doing . . . .  . . . . . . I’m not sure what this word should mean . . . . . . . . . . . . Dio .  . . . . . it sounds like the name of a president . . . Dio . President Dio . ]


timean
[Written @ July 4, 2020 @ 3:59pm]
[today . A time traveller’s mission is to reach today . A timean is a time traveller who searches for todays that are meaningful for them . There is meaning in each thought . There is meaning in each time sequence . . . . today is today which today means you are awesome ! Today means you are awesome ! Today means you are awesome . ]


timein
[Written @ July 4, 2020 @ 3:57pm]
[okay . Time is interesting. . I misspelled the word “time” and typed “timein” . . . . . alright . I’m not sure what this word should mean right now . . . It’s an interesting word . . . time . . Ean . . . . Time - Ean . . . is how you pronounce this word . . . . . . . time-ean . . . timean . . . . . . . this is awesome . Time stuff . Theory . Theoretical devices . Ideological . Ideals . We each have ideals . Ideals for a good life . . . . alright . Well . That’s all for today . ]

teimini
[Written @ July 4, 2020 @ 3:50pm]
[what if alternate realities cannot exist within certain observable constraints . . for example . . sequences of information aren’t entirely supported or considered the methodology in how you would perceive that place . Place I call it . But place and time are meaningful only to human beings of the kind that I present myself and perhaps you the audience reader also identify with . Place and time are ordinary for us . Moment by moment . Week by week . We develop our knowledge . Our understanding . Or personal challenges and missions and reasons for living . Our every mistake . Captured . In a stream of events . Linearized ? So it appears . However . Perhaps not . Perhaps not . Perhaps our other choices made a difference as well . The other lives we led also took on their own challenges and missions . And time . As a concept . Of meaningfulness from looped sequences for waking and sleeping and waking and eating and thinking and waking and sleeping and hanging out with friends . And waking and sleeping and thinking and feeling because of our perceptive focus on individual matters . Perhaps the individual themselves takes on the matter of another sort of form like a person body of people . Perceived from the point of view of a person body categorizer who sees all the people of the person that any person can perceive themselves to consider . All the choices pushed to one knife edge where the concept of time endures itself in a blade perfect entity time machine body . Teimini . . as a term . . seems quite new . . . . . it seems to suggest . . smallness . . . . size . . “mini” .. . . . “Miniature” . . . . . . . . small . . . . well . . I have no idea what this term should mean . . . . it sounds cool though . ]

teimin
[Written @ July 4, 2020 @ 3:49pm]
[an alteration of “time” which is a different sort of time but organizing is operated strange . . . this is a mispelling of the word “time” . . . . . the definition of this word . . . not sure yet . . . but the meaning of a different sort of time would be interesting . . . . . ]

whev
[Written @ July 4, 2020]
[“whev” was a misspelling of “wherever” . . . . 


aline
[Written @ July 4, 2020]
[alien misspelled into “aline” . ]


hain
[Written @ July 4, 2020]
[to marry an alien person . I’m having a hain wedding . I’m having a hain wedding . “Ahh . You’re marrying an alien .” That’s correct . She’s from space . I’m going to meet her family one day . She communicates with me using telepathy . “Telepathy you say . . that’s a good way to keep in contact . How convenient . Do you have a job to support their income ? What does she do for work ? “ She works as a bartender for space order galactic systems where she treats all other people with the same respect any other civilization would prefer to keep orderly conduct with space citizens from other worlds for operating in a creative constraint universe where everyone can be free to create whatever they like . Something like this . . . . . . . you’re a doctor in the next future . . a health professional who knows how to treat the body . And can approach other people to diagnose their bodies and how they’re doing as a common courtesy of man to communicate well being and intent to take care of one another . All you need is a doctor to check up on you to see how you’re doing . ]


roii
[Written @ July 2, 2020]
[I was writing about the newly created word “roiginally” and typed “roi” . . . . . . as in “roiginally” . . . . . . but . . . . for some reason . . . I stopped writing at the “roi” and added an “i” . . . . . the “i” is interesting . . . . . . I’m not sure what roii should mean . . . . . but it looks like it could be the name of an important figure . . . . . . Roii . . . . . . . like Cole . . . . . Cole is an interesting name . . . . it sounds big . . . . . . . . Cole . . . . . . . . . . . . . . Cole . . . . Roii . . . . . . Roii . . . . . . . . . . . . . . . . . . . Roii . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . The Leader of Roii . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Cole . . . the leader of Roii . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . something something . I thought it should be cool enough to name your son Roii if your name is Cole ]

roiginally
[while attempting to write the word “originally” . . . . . . . I typed the text “ro” . . . . . . . . . “ro” . . . . . was meant to be “or” . . . . . . . . . . . too spell the word “originally” . . . . . . . . . this typographical error resulted in this new word “roiginally” . . . . . . . not sure what this new word should mean though . . . . . it looks . . . hmm . . roiginal . . . . . . . . . . . . . . . . . . . . roii . . . . 


reallly
[Written @ July 2, 2020 @ 7:40pm]
[accidentally typed “reallly” with 3 l’s instead of “really” which was the word I originally intended to write . . . . . . . . . . . not sure what this newly created word means . . . . “reallly” . . . it doesn’t seem that different from “really” . . . . . . . . . . . . . . . . . maybe it’s a race car ? . . . a certain type of race car ? . . it sounds really fast . . . . . . . . . . . . . . . . . . . . like a reality shifting race car . . . . it’s reallly . . . . . . fast . . . reallly . . . . . . . . . . . . . . . . . . . . reality . . . . . . . . . . reallly . . . . reality . . . . . . . . . . . . . . . . . . . . . . lll . . . . yea. . . . the 3 lll . . . . . really mean something . . . . . . . . . the 3 “L’s” really mean something . . . . it’s fast . . speed . . . . . . . .]

Sory
[Written @ July 2, 2020 @ 7:39pm]
[accidentally spelled “sory” instead of “sorry” . . not sure what this new word should mean at this time . . . . will have to think about it later . . . . . . . . . . . . . . . . it looks cool though . . . 1 r instead of 2 r’s . . . that’s really interesting 

Wir
[Written @ July 2, 2020 @ 7:37pm]
[accidentally typed “wir” instead of “wri” while attempting to spell the word “written” . . . . . . . . . . not sure what this new word should mean at this time . . . . . . . . . . but it seems new for now . . . . . . . . . . . . . . . . . . . sorry. . . . .hmm . That’s a good . Word . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . Sorry . ]

sson
[Written @ July 2, 2020 @ 7:48pm]
[accidentally typed “soon” as “sson” . . . . . not sure what this new word should meanasdv df we werwolf sdg f dgwqwewe wr erwer asdf xv332  34 w e sd ddv vf e  hdf wqwe123 2 34 were pdf. v fs were 23 were awe f dsdsdf rt req w2132 23 ever sd fsdf xv xcv fg gg r rt tt a asd fsdf wer a qweq xv cxv fgfg f got rt were w q134 were er pdf xcv xcv gf gah h hsfs9 a9 00234-9 were were sdfka f Alfa ado sdfo xvc xcv sf h h aoq012 03 23 2393 34 4 5 a a xxv xxc s fs dfs a pdf we rt y q wee 1 213 awe and ad xxc c vc b vb cab x vxc pdf er were q eq 1 1 qweiqwe9as af pdf z xcx cd fsdf9wer werii343 2 new af sdfxv sdf9a9asd ffw9wr er923 23 trot rt art we a pdf pdf were rrr raw were q qw 12312 3123 awe oaoxvo xvi sdf9 was woe-0r23 3023u I j api I pwe I ewer vsdfsdfsdflj sdfsddfsfsdf asfj pi user pi pi weary yf ery823-riruiwer iv jgk dfgpq[p9qw-02384utu ri kjsd pp j sdl . Zx pdf pep qpwe rj sdfl z dip  . b. Pdf a sd a qp10 aureus 8b vsdf psdop a. A . A . A . a.  a. a. . a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. a. A. a. a. A.a a. a. a. A.a .a . a.a .a .a .a .a a. a. A.a . b. b. b. b. b. b.b .b b. b. b.b b. b. b. b. b. b.b .b .b . c. c. C.c .c c. c. C.c .c c. c. c.c c. c. c. c.c .c c. c. c.c .c c. c. C.c .c c. c. c. c.c c. c. c.c c. c. c.c c. c. c. c.c c. d. d. d. d.d .d d. d. d. D.d .d d. d. d.d d. d. d.d .d d. d.d d. d. d. d.d .d d. d.d .d d. d. d.d .d d. d. D.d . e. e. E.e .e e. e.e .e e. e. e. e.e .e e. e. e. e.e e. e. e.e e. e. E.e .e e. e.e .e e. e. e.e e. e. ee. e. e. e.e e. f. f.f .f f. f. F.f f. f. f. f.f .f f. f.f .f f. f. F.f f. f. f.f .f f. f. f.f f. f. f.f .f f. f.f .f f. f. f.f .f f. F. f. g. g.g .g g. g. g.g .g .g .g g. g. g. g. g. g.g .g g. g. G.g .g g. g. g. G.g .g .g g. g. g. g. g.g .g .g g. g. g. g. g.g .g g. Gh . . .h . h. H.h.h .h .h h. .h .h h. h. h. h. h. h. h.h .h .h h. h. .h h. h. h. h. h. h. H. h.h .h .h h. h. h. h. H.h .h .h .h h. h. h. h. h. h. h. H.h .h .h h. h. h. h. h. H.h h. H.h h h. h. h.h .hih i i.i .i i .i .i .i .i . .i .i .i .i .i .i . I.i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i .i i .i .i .i .i .i .i .i .i i. .i .i i.j .j j..j.j.j.j .j .j j. j. J.j .j .j j. j. j.j .j j. j. j. j. J.j j. j. j. j.j .j j. j.j .j j. j. J.j j. j. J.j j. j. J.j j. J.j jk .k.k.k.k.k.kk k kk. K k.k k. k. K.k k. k. k.k k. k.k k. k. K.k k. k. k.k k. k. k.k .k k. k. k. k.k .k k. k.k k. K.k .k k. k. k.k k. k.k .k k. k.k k. k.k .k k. k.k .k k. k.k k. k.k . l.l l.l. l. l.l .l l. l. L. l.l l. l. l. l.l l. l. l. l.l l. L .l. l.l l. l.l .l l. l. l.l l .l. l.l.l l. l. l.l l. L .l.l .l l. l. l. L.l l. l. l.l l. l. l.l l. l. l. l.l l. l. l. l.l l. ll. l.l l.l l.l l. l.l l. l.l . . . .mm. m. M.m m. m.m m. m. m. m.m m. m. m. m. m. m. m.m .m.m .m m. .m .m m. m. m. m. m. m. m. m. m.m .m m. m. M m. m. m. m. m. .m m. m. m.m . m.m m. m. m. m.m m. m. m.m m. m. m. . n. n. n.n. n.n n. n.n n. n. n. n.n n. n. n.n n. n. n. n.n .n n. n. n. n. n. n. n. n. n.n n. n. n. n. n. n.no. .o o .o . O.o. o.. o. o. o. o. o. o. o . o. o. o. o. o. o. o. o. o. o. o. o. o. o. o. o. o ..o .o .o . o. o. o. o. O . p. p. P .p . p. p. p. P .p .p . p. P p . p. p. p. p. P .p . p. p. P . p. P . p. p. P . p. p. .  p. P . p. p. P .p .p .  . q.q . q. q. q. q. q. q.q .q .q .q q. q. .q q. .q .q .q .q .q q. q. q. q. q. q. q. q. q. q. q. q. q. q. q. q. q. q. q.q .q .q .q .q .q .q .q q. q. q. q. q. q. q. q. q. q. q. q. q.q .q .q .q . q. r. r. r. r. r. r.r .r .r .r .r .r .r. r. r r. r r. r. r. r. r. .r .r .r .r .r .r .r .r .r r. r. r. r. r. r. r. r. r.r .r .r r. r. . s. s. s. s. s. s.s . s.s .s .s .s .s .s s. s. s. s. s. s. s. s. s. s. s. s. s. s. s. s. s. s.s .s .s s. s. s. s. s.s .s s. s.s .s .s s. s. s. s.s .s s. s. s. s.s . t. t. t. t. t.t .t .t ..t t. t. t. t. t. t. t. t. t. t. t. t. t. t.t .t.t .t .t t. t. t. t. t. t.t. t. t. t.t t. t. t. t. t. t. T u .u u. .u .u .u .u .u .u u. u. u.u. u. u. u. u. u. u.u .u u. u. u. u. U .u .u .u . u. u. u.u .u .u. u.u .u .u u. u.u u. ..v v. v. v. v. v. v. v. v. v. v. v.v. v. v. v. v.v.v . v. v. v. v. v. v.v .v v. v. v. v. v. v.v v v. v. v. v.v .v v. v. v. v.  .w w. w. w.w. w. w. w.w w ..w w. w. w. w. w. w. w. w. w. w.w .ww. w. w. w. w. w. w. w. w. w. w.w .w .w w. w. w. w. w.w w. w. W .x .x x. x. x. x. x. x. x. x. x.x .x .x x. x. x. x. x. x.x .x .x .xx .x .x x. x. x. x. x. x. x. x. x. x. x.x x. x.x. x. x. x.x .x x x .x .x x. .. y.y . y y. y.y . y.y .y y. y. y. y. y. y y. y.y. y. y.y. y.y .y .y y.y .y . yy. y. y. y. Y y. y.y .y .y y.y y .y y.y .y y. Y .y .y . z. z. z.z . z. z z. z. z.z. z. z. z.z z. z. z. z. z. z. z.z . z. z. z. z.z .z . z. z.z. z. z.z.z .z z. Z . Friends are more important than the alphabet . The language you speak is important to your family and yet the language you speak is a family of languages from every country in America to Greece to Paris to Japan to France to Italy to the whole Universe that influences how the language is created . The whole Universe is a language . We create meaning out of it with friends speaking in different dialects from other worlds to other time lines and time periods . There is meaning in each thought and each peril is a triumph in terms that no one understands . Everyone understands but no one understands the meaning of the language . Love is the closest concept we can find to machine our hearts 

seeon
[Written @ July 2, 2020 @ 7:37pm]
[accidentally typed “seeon” instead of “soon” . . . . . . . . . . . . . . . . . seeon . . . I’m not sure what this word should mean yet . . . . . . . . . . . . . . seeon . . . . . . . . . . . . . . . . . . . . . . . . . . I’ll see you soon . . . respectfully yours . . . . . . . . . . . . . . . . . . . . . . . . . seeon . . . . . . . . . . . . . . . . . . . . . . . . . . . . . respectfully yours . . . yours respectfully . . . . . . . . . respectfully dear . Endearing . Endearing our common respect to one another . Yours is ]


Morning:meaning
[Written @ July 2, 2020 @ 2:25pm]
[I was writing a script for an earlier word that was newly created and couldn’t figure out whether to use the word “important” or “interesting” and thanks to the use of my computer, I could back pace and erase the word as I would like to show that it could be animated and the meaning is important . . . . [morning:meaning] . . . . . . . . I tried to consider the possibility of a word that could be like there are many possible meanings to this particular word or sentence paragraph or that this phrase can be expressed in different ways . . . . . . . . . . . . . . . . [morning:meaning] is a sunlight word meant to philosophical weather of conversation . . . . . . . . . . . . . ]

Emea
[Written @ July 2, 2020 @ 2:22pm]
[accidentally typed “meaning” as “Emea” . . . . . . . not sure what this new word should mean yet . ]

Everyonbe
[Written @ July 2, 2020 @ 2:19pm]
[accidentally typed “everyone” or “everybody” which I sometimes am not sure which one of these words to use . . . . “everyone” seems highly spiritual . . . . “everybody” seems down to earth . . . and sometimes I’m not sure which word to use in a conversation . . . . . . I like being down to earth . . . but I’m also a spiritual person . . . . . . . . . I like to think about the spirit . Creativity is the meaning of life . I’m not sure what to say about creativity . But it’s important to all spiritual practice . . . this new word “everyonbe” has an [morning:meaning] meaning to me . . . . . . . . . . . . Onbe means “only being” . . . . everyonbe . . . . is only being . . . . only being . . . . . . . . . . . . . . .  . .  . . . . .  . . I’m not sure what this means . . . . or why this should be the meaning of this particular word . . . . . . . it just looks like a meaningful word to me even if it was discovered by accident in my typographical error while writing the text “Hello Everybody” or “Hello Everyone” . . . . . . . . during the live stream conversation with viewers on my YouTube channel for the Ecoin live streaming even for this day only being . ]


Notes @ Newly Created Words
Setar
[Wirrten @ July 1, 2020 @ 2:00pm (approximately)]
[accidentally typed “Setar” when I had originally intended to type “start” as in “Start running infinite loop program” while working on typescript code for Ecoin . ]


Notes @ Newly Created Words
Shaer
[Faer could perhaps be considered as the pronoun for any identity one would like to assume when being referred too . Perhaps . I’m not sure what to think of this word at the moment but started experimenting to see what other words could mean the same thing . Shaer seems like a good one as well . Also I like how it is pronounced like “share” . . . Shaer . . a pronoun we can all share . . . “Shaer did this” . . “did you hear what shaer did?” . . “Shaer went to the grocery store to get cupcakes” . . . “he went to the grocery store to get cupcakes” . . . “shaer went to the grocery store to eat cupcakes” . . . ]
[Written @ June 29, 2020 @ 3:41pm approximately]


enverioble
[Created @ June 18, 2020 @ approximately 10:56pm]
[returned “variable” and “encapsulate” concatenated in a particular way that makes the world look new and healthy. Perhaps instead of saying “you are healthy” one could say “you look alive” or “your eyes look alive” or “your countenance is strong” . Something like this . Cool dentures . Hehe . ]


Jue
[Created @ June 10, 2020 @ ~ 7:43pm (approximate)]
[accidentally spelled “just” as “Jue” . . . cool new word . . not sure what it means yet .. could be the name of a planet . . it’s really pretty . . . pretty word . . . pretty planet people . . . Jue . . reminds me of “June” . . . June is also a pretty word .. . . . . ]

shure
[Written @ June 8, 2020 @ 6:52pm - 6:57pm]
[accidentally spelled “sure” . . . as “shure” . . . . . not sure what this word should mean those . . sure . . sure . . shure . . . . . . . . . . . . . . are you from the shure . . . . ? . . . or is it shire? . . are you from the shire? . . . . . . . . Baggins? . . . . . a Baggins’ . . . a Baggins . . is . . . Bagginses . . . . . Baggins . . . ES . . . . . . . .it’s the plural word for Baggins . . . . . . . . . multiple Baggins . . . . . Baggins’ . . . but the apostrophe looks kind of weird . . you have to be an English speaker to translate the sound of this character . . . . . . . . . . . . it’s the “ESSSSS” sound . . . .. . BAGGGINSSSS . . .ESSSSSSSS . . . . . . . . so its like you’re stuttering . . . . which makes it sound cool . . . . or rather that’s my thought at this time . . and also . . it’s not necessarily true . . . it’s just . . . thinking look . . a thought . . thinking . . thinking . . thinking . Thought . . . . Baggins . . . . thought Baggins . . . . . . . . . the Baggins’ like to think . . . . . . . . silly hobbits . . . . . . . . . . . . . . . . . . . . . . . . happy hobbits . . . . thinking hobbits . . . . . . . . . holy hobbits . . . . please share the light of your thoughts . . . . . thinking stinking hobbits . . . . . . . . . .  . . . . . . . .  your thoughts smell like thought . . . . . . . . . . . . . . . . . . . . . . . . . think less . . eat more . . juicy . . smelly fish . . like golems’ . . from the lakes nearby . . . . . . . sweet smelly fish . . . . . . . . . juicy . . . rawwww . . . . . . . . .wringly . . . . . . . . . . juicy sweet wrinkly fish from the snake piled lake of the neighborhood swinglers . . . . . . . . . . . . . thinking less makes more fish come from the swimming pool . . . . . . . . . . thinking . . stinking . . . . . sorry . . sad . . . . . . .  .. . . . . stupidddddddddd . . . hobbits’ . . . . . . . . . . . . . shure . . . I’ll come to the thinking park with you . . . let’s go to the nearest partk . . .. . . . . . . . . . . . . . . . . let’s think some more . . . thinking thinking . . no thinking . . . . . . .  ]

Shold
[Written @ June 8, 2020 @ 6:51pm - 6:58pm]
[accidentally spelled “should” as “shold” . . . in doing so . . . . . . we have a new word too work with . . . not sh. . . . shhhhhhhhh . . . . . . . . . . . . this is a secret word . . leave your comments in the description below of what you think this new word should mean . . . . . give it any meaning you like . . . . . it’s yours now hobbitssssss . . . ]

Partk
[Written @ June 8, 2020 @ 6:49pm - 6:58pm]
[accidentally spelled “park” as “partk” . . . this new word looks quite cool . . . . . . . . reminds me of “park” . . . . . . . but different . . . a thinking park ? . . a park where people go to think? . . . . . a thought park ? . . . philosophers and religionists going into thought constructs like water holes at the local water park nearby . . . ? . . . . . . . . yea . . maybe . . . not sure .  Not sure . . . . . . . . . Partk . . . . . . . . . . . . . the pronunciation is so similar already to “park” . . . . . hmm . . that’s tough . . not sure what to do about that . . . . . . . ahh right . . . . so I suppose if the pronunciation is already so similar to this other word “park” . . . . maybe the meaning should be . . not sure . . ]

Jeun
[Written @ June 8, 2020 @ 6:24pm]
[Is this a new word ? Or a new country ? The Jeun nation will be a sea bearing people. They live on the water and underwater as well. A new human society will be built named . . . hmm . . well. . . Jeun is the name of the nation or country or state . . . and yet . . . . . . . . . hmm . . . . what should the people call themselves? . . . . . . Jeunipers ? ? . . . . . . . Jeuniflowers? . . Jeunibeings? . . . Jeunisuns ? . . . Unison? . . ahh . . the Unison people . . . we are the Unison. . . . . . . . . . Unison . . Uni . . U . . . We are . . U . . . . we are here in peace . . . . . we bear gifts of great hope . . . . . . . we are peace lovers . . . . we make love to the water . . . . and water brings us hope and dry land . . . . . . we think of water when we need to . . and attach to the water when we think . . . . . . electroseasonal beings . . . . . . seasoning our thoughts with water molecules as we succulate in our thinking thoughts . Thinking soundingly and cournuptuously into our new ever thinking thoughts . . . . . . . . . . . . . . . Jeunicon . . . J is for Joy . . . E is for Everlasting . . .U is for Unison . . . I is for Indicative . . . . C is for Consistency . . . . . . . . . . O is for Observant . . . . . N is for Neighborly Niceness . . . . . . . . . . . . . . . . . . . . . . . . Jeunicon people do deserve another world from which to habituate their enduring thoughts . . . . . . . . free from landdwellers . . . . . . . . . . . . . . . . . . . they are a Jeunicon . . . . . that’s a usecase for this everlasting term for a new kind of people . . . . . . . . . . . . . . . . . . . . . . she is a Jeunicon . . . . . I am a Jeunicon . . . . . . . . . . . . . . . . . . . June . . . EE . . . Cohn . . . . . . . . . . . . Jun - E - Cohn . . . . . . . . . . . . . . . . Jun - E - Cohn . . . . . . . . . . . . . Joo - knee - Cohn . . . . . . that’s a . Uh idea of how you could say this word . Not final . . just made it up . . ehehe . . no disrespect -_- I honor your people . ]

Chanel
[Written @ June 8, 2020 @ 6:32pm - 6:38pm]
[Wait . . is this a new word? . . . . . . I’m not sure . . . . . Channel means “To form or cut a channel or channels in; groove” . . . I have also heard of a channel as a way to . . . provide . . . a way to something. . . like a channel of information . .. . . or an information channel . . . like a close friend whispering something in your ear . . . or maybe a drainage pipe in the sewer nearby which continues to informatize your earplugs in front of your face with the latest scents of jeuwcy wonders of how everyone has been eating their latest suppers and in what conditions and moods they were in . . . . with the latest sweet . . swoppy . . . .. . . scenting . . descenting . . . . . cheesy . . smelly . . . . aroma . . . of mass confusion . . . . . . . proving the whole truth of how existence came to be just for a moment for you to smell how thinking tastes when you stay near a sewer pipeline that provides this latest information on dietary health choices by everyone in the neighborhood . . . . . . . . . . . poopy . . . right . . . . . sorry . . . . what a mess . . and yet this beautiful name “Chanel” . . reminds me of a flower . . a yellow flower . . . . . . . a sun flower perhaps ? . . . . not sure. . . . . . . . . and yet . . a rose . Of a particular kind. . . a yellow rose? . or white? . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . “Chanel” . . . . . . passionate “Chanel” . . . . . everlasting “Chanel” . . considerate “Chanel” . . perfumous “Chanel” . . . . . . . . Coco Chanel . . . . . . Jealous Chanel . . . . Jenius “Chanel” . . . . . . . . . . . . . Joyful “Chanel” . . . . . . . . Jubulent “Chanel” . . . . . . . . . . Jamming “Chanel” . . . . awesome . ]

Tei
[Written @ June 7, 2020 @ 4:35pm]
[accidentally wrote the word “Tei” when intending to write “their” . . . . . . . . . . not sure what this new word should mean . . . . sometimes I’m not sure what gender to call people since their own individual preference may not be clear to me without having consulted their view point in this manner or perspective . . . . . . . . . . . . . . . . . . “his” . . . “hers” . . . “x

Thei

ImmeasuraBeautyble
[Written @ June 6, 2020 @ 8:11pm]
[added the text in “Immeasurable” to read “Beauty” at the 9th or 10th character depending on how you start indexing whether from 0 or 1 . . . . . . . . . . . if you start as “I” being the first . . or number 1 character in the word “Immeasurable” . . . then . . . . . . . . “Beauty” starts as the 10th character . . . . . . . . . . . this looking like it could be the name of a girl in a video game . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Immeasura Beautyble.  . . . . . . . . . . . . . ]

holl
[Written @ June 6, 2020 @ 2:02am - 2:04am]
[accidentaly wrote “hello” incorrectly . . . this is a variant . . not sure what this new word should mean at this moment . . . ]

holle
[Written @ June 6, 2020 @ 2:02am - 2:04am]
[accidentally wrote “hello” incorrectly” . . this is a possible variant . . . . not sure what this new word should mean though . . ]

aopstra
[Written @ June 6, 2020 @ 2:02am - 2:04am]
[accidentally wrote “apostrophe” incorrectly . . . not sure what this new word should mean though . . . . yet here it is . . . . . it looks cool . . it reminds me of the word “aorta” . . . . . . . . . . . very cool . . and it also looks like it would belong in the medical category specification text rhythms of the modern era . . . . . . . . . . . ]

apostra
[Written @ June 6, 2020 @ 2:02am - 2:04am]
[accidentally wrote “apostrophe” . . incorrectly . . not sure what this new word should mean though . . . . . . . . needs more thought . . . . ]

party’e
[Written @ June 6, 2020 @ 1:59am - 2:04am]
[accidentally wrote “party’s” incorrectly . . . . not sure what this new word is . . . I just like how it looks . . . . . . . . . how often does one see an apostrophe-e in their language specialist program ? . . . . . . “party’e” seems like a new kind of participation program for how we stylize our language speech syntax and not just have “’s” or apostrophe-s . . . . . for the text words  . . hello’s . . . hello’e . . . . not sure how to pronounce this new word . . . . ]

meautiful
[Written @ June 6, 2020 @ 1:49am]
[. . . I thought about the word “meaning” . . and “beautiful” . . . and thought about how to combine these two words . . . . “meanutiful” ? . . . “meautiful” . . . . ? . . . . . . . meaningful beauty . . . . . . . . . something like that . . . . . meaningful to an individual . . or meaningful to a group of individuals . . . or meaningful to . . . a collective that is otherwise identified as unique ? . . . . like the body of a human is made of multiple compartmental organisms that don’t necessarily have a name . . . all billions and millions and many thousands of these organisms. . . . . not necessarily naming in how humans like to use English naming conventions for example . . . . . . . . . but still the body is meaningful to a human being that takes care of their body . . . . . . . . . . each body part at a time . . . . . . hehehe . Something something like that I don’t know .  Sorry . . ]

beautifu
[Written @ June 6, 2020 @ 1:48am]
[ . . . . accidentally forgot to add “l” to the end of the word “beautiful” . . . and now there’s a new possibility for a new word that comes from this text description . . . . a new meaningful content . . . . . . . something. . . . . ]

Anno
[Written @ June 6, 2020 @ (approximately) 1:48am]
[another new word . . . . . accidentally spelled “another” . . incorrectly . . . . . my fingers slipped as I was attempting to write the word “another” . . and now . . a new word “anno” has emerged . . . . which looks very very interesting . . . . this could be the male version of “Anna” . . or “Anne” . . . . . “Anno” . . a masculine name . . . . . very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very very . . . ad infinitum . . . very very very very very very very very very very very very very very very very very. . . . ad infinitum . . very very very very very very very very very . . infinitely cool . . . . . . . . . . . . . . . . . . . . what a discovery . . so beautiful . . . . this is a beautiful name . . . ]

Anoyth
[another new word . . . . accidentally spelled “another” incorrectly . . . and resulted in “anoyth” . . . . . . . . . . . . anoyth means whatever it wants . . . . . . . . . something like that . . . ]

wannt
[Written @ June 6, 2020 @ 1:45am]
[anoyth . . . another new word . . . . . . . . . accidentally spelled “wannt” incorrectly while typing on a keyboard on my MacBook computer here in A.D. 2020 in the month of June . . . . and now a new word has arrived . . . . . . not sure what it means yet . . . . . . . . . . . . . . it was a typographical error for the word “want” . . . . . . . . . “want” was the originally intended word to be expressed and now I’ve incorrectly spelled it by accident to produce this result . . . “wannt”. . . . what should this mean ? ? . . . not sure yet . . . . . . . . . . . it could mean anything. . . ]

Reup
[Written @ June 6, 2020 @ 1:39am]
[accidentally spelled “republican” incorrectly . . . “reup” is the result . . . and this word looks quite cool . . . maybe the name of an individual? . . . . . . . . I love it. . . ]

thw
[Written @ June 6, 2020 @ 1:16am]
[accidentally wrote the word “the” wrong . . . . . and now “thw” is a new word . . . not sure what it should mean right now. . . ]

Writte
[Written @ June 6, 2020 @ 1:16am]
[Accidentally wrote the word  “written” incompletely . . . . . and now we have a new word . . . . . “writte” . . . not sure what this word should mean at this moment . . . ]

Accidientally
[Written @ June 6, 2020 @ 1:16am]
[Accidentally typed the word “accidentally” incorrectly . . . the result of this accident was a new word “accidientally” . . . . . not sure what this new word should mean at this time. . . . . . . ]

Replublicans
[Wrote the word “republicans” incorrectly and accdientally . . . . . . . . . . . accidentally . . . . . . . . . . . the word “plub” is visible in this word which gives me a reminder that that is a cool word . . “Plub” . . . . . . “plub” . . . . “plub”. . . . like water dripping from a faucet. . . . . . . . . . . . . . . . . nice sounding name . . . . . . . . “hey, Plub . . how’s you’re day been so far? . . do you plan on getting married one day ? . .” . . “hey Plub, how are your social values. . . let me know what you think about social income taxes . . why don’t you Plub . . . “ . . . . . . “hey Plub . . . go read these 10 business books . . . go read of how to become an entrepreneur . . . . go own your vote Plub . . . “ [sarcastic intent is visible here for those that read subtext . . the subtext is that plub is a synonym to “pleb” or “plebeian” . . . something like this . . not really sure what that word means either to be honest . . “noob” . . . “amateur” . . . . “student” . . . . . either way . . aren’t we all students to certain artistic endeavors of the world . . . . . . not sure if plub is a bad word or not . . maybe a compliment . . . . . . . I like to learn . I am a plub . ] . . . . and yet . . plub remains a newly lovely name for human beings that like to “plub” . . . .  . . . replublicans . . . . not sure what this word should mean . . maybe something to re-education systems  ? ? ? . . . like re-educating people with respect to new value systems for a society that is transitioning to new ideals ? ? ? . . . . peace . . . friendship . . joy . . . . . . . for all people . . . free  . . resources . . for all people . . scientists . . engineers . . for all people . . . . . . multidisciplinary people . . . for all people . . . . . entomologists (people who study ants) . . . . . videographers (people who study videos) . . . . . . linguists (people who study languages) . . . software engineers (people who study programming computers) . . . . . roboticist (people who study robots) . . . . . . . . . entrepreneurs (people who study leadership and building things for people that the people need at a practical level and not just researching like some mathematician god heads that sit in ivory towers thinking of theoretical concepts and not getting down on their knees to pray to the people that exist for today and need a new society based on the studies they’ve made for over the past many hundreds of years already . . get with the program . . your research would benefit from a new civilization hierarchy . . no more political discourse . . say you quit your job and stay home and work on Roxanne City or do something . . have farmer friends help and support your life for free as you build the new Roxanne City and other cities like it all over the world with peaceful protest at the forefront of any effort to disband the group hierarchy for peace and prosperity for all people . . . . . . . dissenting will be met with peace and sorry . . . I’m sorry but this is what I think . . peace . . . and love . . and kindness. . . and always take care of your own needs first as an individual and don’t follow what Jon says because he says it . . not your leader . . he’s not your leader . . Elon Musk is your leader if you elect him by a unanimous vote . . . . for each year he doesn’t get elect . . the next year an election will be held for the same purpose of voting him into power to oversee the governments of the world and how we can get through this time for a peaceful world union . . . . . in all countries of the world . . . . . . . . . . . . more people volunteer . . . . . . more people help . . . . . . . . . . . . . . . . . . . start a garden . . . get some food . . . . plan with the engineers . . . communicate through twitter . . youtube . . . . instagram . . . . . . facebook . . . . . . . . . . . I’ll write more about this later I’m sure . . but leave a comment on anyone’s post relating to this topic such that they know you’re here for peace so people don’t need to be shy in talking to one another . . “I am here for peace. Would you be interested in collaborating with us in this or that effort. We are familiar with your work in this or that field and would love you to join us at this following address or email us for any further correspondence.” . . . . . . . . . . right . . . . . that ]

Learder
[Written @ June 6, 2020 @ 1:36am]
[accidentally spelled the word “leader” incorrectly . . not sure what this new word “leader” should mean . . . . . . ]

Replic
[Written @ June 6, 2020 @ 1:14am]
[created this word while meaning to write “replublic” as in “democrat or republic” .  . . . . . something like this . . . . ]


the you-you
The you-you
[Written @ June 5, 2020 @ 9:33pm - 9:35pm]
[it sounded like you could interpret this part of the vocal language speech syntax of this video clip [1 @ 47:32 - 47:35]  as saying something like “the you-you” . . . sounds spiritual . . . wonder what it means . . . . . . . . . . . . .sounds cool . . ]

[1]
Hack Club AMA w/ Elon Musk
https://www.youtube.com/watch?v=riru9OzScwk 

wront
[Written @ June 5, 2020 @ 2:26pm - 2:27pm]
[. . . accidentally wrote “wrote” as “wront” . . I meant to write “wrote” . . . . and accidentally wrote “wront” instead . . . not sure what this new word should mean . . . ]


mangol
[Written @ June 5, 2020 @ 2:26pm - 2:28pm]
[. . . . . accidentally wrote “Mongul” as “mongol” . . . . not sure what this word should mean . . . this I’m not sure about at this time . .] 

mangu
[Written @ June 5, 2020 @ 2:24pm - 2:28pm]
[. . . accidentally wrote “mangu” when I meant to write “manual” but stopped before writing out the full word “manual” . . . not sure what this new word means . . it reminds me of mango . . and mongul . .. . . ]

liket
[Written @ June 5, 2020 @ 1:42pm]
[. . . . . . this word looks new . . and I’ve retrieved it from accidentally spelling “like” . . . as “liket” . . . . . . . . . . . well the original statement I was trying to write was “something like this” . . . and I suppose I accidentally wrote the “t” in “this” too soon without the space character to separate the “t” from the “like” aspect of this statement . . . . . . . . . . . . . a small like? . . . like . . . . . . . . . . . . . . . in proportion to something else? . . . . . not sure . What should this new word mean? . . ]

Elbos 
[Written @ June 5, 2020 @ 1:38pm]
[ . . . . this word looks new and I’ve retrieved it from accidentally spelling “elbows” as “elbos” by missing an “w” character . . . . . . . . this word reminds me of an elven city name from Skyrim or a similar video game aesthetic environment like Lord of the Rings or something like this . . . . . . . . not sure . . it sounds cool . . ]

slou
[Written @ June 5, 2020 @ 6:49am]
[. . . . . . . . accidentally typed “slou” when I meant to type “solution”. . . . . . . . . . . ]

skiiboard
[Written @ June 5, 2020 @ 5:22am - 5:24am]
[. . . . . . skii board . . . . I’ve never being skiing being . . . . . but I wrote this word . . . while thinking of the typographical error I made when typing the word “keyboard” . . . I accidentally wrote . . . . “skeyboard” . . . . . . . . . . . skey? . . how do you pronounce this part ? . . . . . sk-eh-y ? . . . . . . skii? . . . . . . . . . . . . . . skxi?? > . . . . . like sexy? . . . something like that? . . . . . . . . . . well . . . skiiboard came out from this explorative endeavor . . . . and now there are more words to play with . . . . .  something something]

antoth
[Written @ June 5, 2020 @ 5:21am - 5:29am]
[. . . when attempting to write the word “another” . . . . my pen fingers slipped on the skeyboard . . . . . I’m not sure what this new word should mean . . oh by the way . . I meant to write “my pen fingers slipped on the keyboard” . . . . . . . . I think it is meaning how fingers are like pens because they are long and thin . . . . . . . . . . and also they are writing utensils to eat your thoughts from the mind without needing to spill the thought away from forgetfulness because you forgot to eat properly with a pen knife . . . . . . and so your fingers are sharp instruments that help you think like a human pen . . . . and your keyboard is something like a pen as well since the thoughts are already pre-arranged to help you topically organize how that thought will spill by arrangements of the symbols of meaning in idyllic capacities like telling a story book rhythm by the heartbeat of your thumbs something like that . . not sure what this new word should mean . . ]

coumm
[Written @ June 5, 2020 @ 5:20am - 5:29am]
[. . . .  this is a new word built from the typographical error when I typed “communication” incorrectly . . . . . . I’m not sure what this new word should mean . . . . . . . . and yet here it is being recorded to be placed in a queue of thoughts that I recorded for me to re-visit when I think to do so at ant. . . . . . . . . . . . . . . . . . ]

volic
[Written @ June 5, 2020 @ 5:15am - 5:17am]
[. . . volic . . .  I was writing the word “vocal” . . . . but spelled this much far ahead before stopping  . . “vol” . . . . . then . . I thought to myself . . . . . well . . . . what if you could finish this word “vol” . . . . . but it relates back to the term “vocal” which was the original term I was planning to write . . . . . . . . . . . “volic” . . is one result . . “volac” . . . . . . . . . is another result . . . . . . . . and now we have more new terms to play with our understanding of our place in the universe . . . thoughtful . . . I wonder what kind of universe allows you to play with new terms without thinking of when to stop laying your standing symbols stone wall high . . . . . . . . arches and aqueducts . . . . gosh they’re gorgeous . . . . . . . ]

scultpures
[Written @ June 5, 2020 @ 5:02am - 5:05am]
[. . . accidentally typed “scultpures” instead of “sculptures” . . . . . . . . as I had originally intended to type “sculptures” . . . . . . . . .. and now this new typographical error with relation to “sculptures” seems to be a new word kind or category in terms of the hypothetical spaces of words that typographical occur when one encourages their children to learn in a particular language and type in a particular way . . and thus I am now here where I played my part to make ends meet for the world that needed this word to exist in the future when it plays an important part in political discourse on this or other far away planets where “scultpures” are significant to the people and their way of life . . . . . for now . . not sure what this word means . . just a coincidental error that it’s in our present experience to think and endure our particular throat drum noise sound effects . . o]

Accinde
[Written @ June 5, 2020 @ 5:00am - 5:06am]
[I was writing about the word “Mentini” . . . which is a relatively new word . . and now . . . I accidentally typed “accidentally” as “accinde” . . . . . . and now there is this new word . . . . and I’m not sure what it means yet . . . it seems pretty cool in terms of the appearance of the characters in harmony with one another . . . . . . . something about it reminds me of Greek philosophy . . . . . . . . . . . . . . Ak-cinde ? . . . Ass-in-deh? ? ? . . . Ak-sindeh? . . . how do you pronounce this word? . . . . . . . . . . not sure . . . . it reminds me of . . . . . . olive leaves . . . . . how pleasant they are to look at . . in greek scultpures . . . . ]

Mentini
[Written @ June 5, 2020 @ 4:59am - 5:08am]
[. . . . I was writing about the word “enclavement” . . . which is a relatively new word . . and now . . . accidentally typed “mention” as “mentini” . . . I meant to write the word “mention” . . . . but my fingers slipped as I was typing and this new word “mentini” was a product that caught my thought interest in how precious it appeared as part of the garden of precious ideas that spring from the thoughts and gardens of our mental curious bespoken attentiveness to beauty and thought stymies . . . . . ]

enclavement
[Written @ June 5, 2020 @ 4:57am - 5:10am]
[. . . not sure . . . . . . was talking about something about possessing a name of some kind like the “Doctor” honorary status symbol for when you receive a doctorate degree and then in describing the possession of the entitlement . . thought to create a new word to suggest the type of possession-ness of that by Eric Weinstein or Dr. Eric Weinstein in that since at this moment I’m not exactly sure on which of these would be preferred from their statement of reference . . in terms of how they would prefer to appropriate themselves with respect to my own participation in mentioning their own personhood with respect to how they are organized with the idea of presenting themselves with respect to other personhood people from how they would present themselves . . . . . . . . . . . . . . . . . rulers and protractors . . . . . ]

:period
[Written @ June 5, 2020 @ 4:53am - 5:30am]
[accidentally added a “:” colon mark in front of the word “period” . . . . and asked myself in my mind “I wonder if that could be a word” . . . . . . . . the “:p” are the first two characters which reminds me of a smiling face with a tongue out . . . . . but also . . . . . . . . the word itself looks ominous . . . weird . . . scary even . . perhaps . . . . . . . . is it because of the colon character appended to the front of the word? . . or rather . . to the left most character of the word? .  . . something along those lines? . . . . . . . . . . . how do you really describe things in relation to one another? . . . . . . . . . you know. . . I heard a talk once by . . . Eric Weinstein . . I believe his name is . . . . . he was being interviewed. . . . . by . . . . . . Lex Fridman . . . . . on the Artificial Intelligence podcast that Lex operates . . . or maybe it’s Dr. Lex . . I think they have their PhD . . . in artificial intelligence . . . . sorry Lex . . . or rather . . Dr. Lex . . . . . . . . maybe . Something . . not sure . . . hypothesized existence of a doctorate degree . . . in any case. . . . . Dr. Eric Weinstein . . . . . if that is the honorary enclavement of the name of your possession . . . . . .  . . . . Dr. Weinstein spoke of a topic called “Gauge Theory” which relates to the topic of relationships . . . ahh right. . . . how do you really describe things in relation to one another? . . . . . . . . . . . maybe the term :period . . . can help us answer that . . . . . when someone thinks to relate it to something . . . . . . . . . . . . . . . . . . . yea . . I guess it reminds me of the keyword “this” in the popular programming language “javascript” . . . . . . . . . . . . and then how would you pronounce “:period” . . in vocal terms ? . . . . . . . . . . . . . “color period” ? ? . . . . . “pound period” ? ? ? . . . . hmm . . “:” might need a new sound . . . . . . . . . . . if you don’t say the word “colon” . . . . . . . . right now . . for me . . it’s just a silent sound . . . . . . . . . . if I don’t say the word “color” . . when I see the symbol “:” . . . . there’s a sort of silence . . . like “hmm” . . or “uhm” . . or “something” . . or “humph”. . . or “hmmph” . . . . . . or . . “**sigh**” . . . . . . . . . . . . . hmm . . . what if instead of sounds . . . . “:” . . could be a picture of something like a bird . . . . . . . . . . . . . but instead of identifying the name “bird” . . . we only have the image in our minds . . . . . . and then say “period” . . . . do you think having mental imagery in mind when we communicate focuses our methods of communication . .  yea . Okay . I’m sorry . New word . Good bye . ]
[not sure that ending my message with “good bye” is preferred here . . “see you later” ? ? . . . not sure . . . . . . . “I’m sorry” . . I’m sorry . Yes . I’m sorry . Most of the time I’m sorry . Offending people is tough to think about . . . . . and so in my thinking I think that saying that I’m sorry should correct things in some respect . . . . . . . . I’m sorry for wasting your time . I’m sorry for having you think of me in a way that means you’re perhaps . . . . suffering . . I’m not sure . . . what should I be sorry for . . I’m happy to be myself. . . . but also I like to think that others are also happy to be themselves and so if it’s possible to perhaps think of myself as something like another person . . saying that I’m sorry means to mean . . I’m sorry for . . . . hmm . . . . . . . . . . . . . . . I’m sorry . . . . . . there are things that happen that happen that I don’t necessarily think about . . and that you have thought about and that I missed from my thinking . . . and you’re thinking is extraterrestrial and new and enlightening and thoughtful and exotic and exciting and beautiful and magnificent and marvelous and unique and it’s highly likely that you’re thinking something new that I can’t think of at this moment and so that makes me a sort of hypocrite in saying something since I’m not exactly sure what to say that helps you think that makes you think that something is something that thinks like something that something like thinking that something like thinking something like something like thinking something something like something avoids offending you or something like that . . . . . . . . . there’s so many layers to you . . which layer did you stop me to consult my intelligence? . . . . . . . . “are you an intelligent person?” . . . . an idiot? . . . . . . . . . . . . . . . . . . . . . . yes . . . yes . True . Yes . . yes . True . Yes . . yes .. . . yes . That is correct . You are correct . . . correct . Yes . . true . . true . . correct . . yes . . absolutely . Ideally , yes . . correct . . . yes . . . yes . . influentially . . yes . . . . correct . . . . yes . . that is correct . . yes . . yes . . yes you are correct . Ideally . Yes . Yes . Absolutely . No . Not negatively . . positively correct . . . . spectacularly . Unapologetically . .well . I’m sorry actually . . . yes . . . and yes . That’s correct. idiot. . yes . . . . yes . . . . . you are correct . Yes . You are correct . . . that’s right . Absolutely . . relatively . . minority reportingly . . . yes . 

vajavascript
[Written @ June 5, 2020 @ 4:50am]
[was writing a property reference statement and while attempting to access a property key name named “javascript” . . . . . . I think I overwrote the name slightly . . . where I clicked somewhere . . . maybe in front of the “javascript” . . . . or I was erasing something . . . I’m not exactly sure and would perhaps need to view the tape recording of this video file stream at this moment on youtube . . . . . . . . and yet . . . . . . . . . I suppose my clicking around and fumbling to delete . . . or to add a property name appended to the “.”  . . . or “period” . . access property method reference call I suppose . . 

expore
[Written @ June 5, 2020 @ 3:20am - 3:29am]
[this word was created when I accidentally spelled the word “explore” . . . . . . . . . . . . . . . . . . . I’m not sure what this new word “expore” should mean . . . . . . . . . and yet here it is . . . . ripe for mining the opportunity to give it a new meaning and symbology in our native culture here on Earth today in the year 2050 of the new decade . . . . . . . . . yes . 2050 . . and then and now are equally enduring with their own self amusement and clear presence in themselves . . . yes . . memories how pleasant the daffodils . . the ever raining thoughts of inspiration . Inspiring our every moment to sparkle a new halo . ]

substivision
[Written @ June 5, 2020 @ 3:20am - 3:23am]
[this word popped into mind earlier about 10 seconds ago perhaps . . . and I’m commenting now on its possibility of being a new word . . . . not sure . . what it should be the meaning of relating to being the meaning of being the meaning of being the meaning of being the meaning of being the meaning of being the meaning of being the meaning of being the meaning of meaning to be the meaning of a being meaning the being of being the meaning of being the meaning of being the meaning of the meaning of being the meaning . . . . . . . . . . . a quick idea comes to mind that perhaps this word should mean something related to “substitution” . . . . . . but . . replacing . . something interesting with something I’m not sure . . . . . . . quite strange to be honest . . . . . . . . . . . . . . . . . . . . . . . . “substividon” . . . . is another word that came to mind . . which is really the same word but written differently to suggest a way to pronounce the word “substivision” . . . . . . which was the original word . . . but I didn’t know if I was pronouncing it correctly from the spontaneous time when I thought of this new word . . . . . it just came to me and now here it is . . it looks fun and new and interesting and lots of physical properties to explore . . . . 


substitude
[Written @ June 5, 2020 @ 3:14am - 3:19am]
[accidentally typed “substitude” with a “d” instead of a “t” . . . . this new incorrectly spelled version of “substitute” . . . . . . could mean something in the far future of the world here on Earth . . . . . . . . . planet XYZ-Omega-Beta-Omricron-Stella-Orbital-Configuration-Status-E_M_Beta_Star_Sigma_S_Cross_Over_Y_M_L_T_M_S_K_R_Y_O_U_O_V_E_R_L_V_E_T_H_E_S_P_A_C_E_S_H__IP:128427-Omega-P-Cross-Over-M-R-D-Delay-Radar-Signal-Type-Meyta-Starship-Fiftyone-eighty-dash-delta-monochrome-starline-starstyle-signal-peacock-studio-melon-collecting-star-shp-shape-star-status-mega-buillion-mathematical-star-arrangement-alphabet-star-seven-nine-one-eight-six-six-sorry-cant-help-im-under-water-im-in-a-tunnel-can-you-hear-me-im-sorry-is-it-too-late-to-es-cape-the-matrix-no-no-in-i-go-then-so-sorry-goodbye-bonnystlystispoeuickdmwbrjlspzoudlfypqpemdhfixuppoduekloxpdodufkoxodpsudjdkfnjxfislerbalsdldospeirnrosidnfpsudylzidufnenrkslspaisps[fifnsldifuaoepqp3pe0sdldfkekruapajfjdpwpridufalafnspfsd[ssifnwlerkdkflslalals]

supdate
[Written @ June 4, 2020 @ 9:12am - 9:12am]
[update . . . . with an “s” in front . . . . . not sure what this word should mean . . . ]

coint
[Written @ June 4, 2020 @ 8:36am - 8:37am]
[accidentally spelled “coint” when trying to write “continue” . . . . . . . . it’s incorrect spelling of “continue” . . . I’m not sure what this new word should mean . . . it reminds me of the term “coin” . . . . . . . ]

comti
[Written @ June 4, 2020 @ 8:35am - 8:36am]
[accidentally typed “continue” incorrectly as in the “continue” keyword in javascript programming language . . . . . . . . . . and now we have this new word . . . and I’m not sure what it means yet . . . . . . or what it should mean rather . . . . ]

defailt
[Written @ June 4, 2020 @ 7:54am - 7:56am]
[accidentally typed “defailt” instead of “details” . . . . . not sure what this new word should mean . . . . D . . E. . . F. . A. . . I . . L . . . T . . . .  . . . “details” is an html tag you can write in markdown files to get drop down lists of content . . . . . and I’m working on the documentation for a program called “program-utility” . . . . . . . . . at the moment . . . . ]

Doatta
[Written @ June 4, 2020 @ 7:29am]
[accidentally spelled “doata” incorrectly and created this new word from already existing new word . . . . . . not sure what this new word should mean though . . ]

Doata
[Written @ June 4, 2020 @ 7:29am]
[accidentally spelled “data” incorrectly . . . . . and . . . . . . . . . . . . . spelled it as “doata” . . . . . which is a mispelling of “data” . . . and so . . . . . . . . . . . “doata” . . . . because it looks new to me . . . . seems to be a newly created word . . . . . . . . . . . . . . . . . . . . . . . not sure what it means though. . . . or rather . . what it should mean . . . . . will have to figure that out later . . . maybe in 100 years or something like that . .not sure .  . ]

retrun
[Written @ 9:43pm]
[accidentally typed “retrun” when I meant to write “return” . . . . . not sure what this new word should mean . . . . ]

maes
[Written @ 9:18pm]
[accidentally typed “Maes” instead of “makes” . . . . when I was trying to type the word “makes sense” . . . . . ]

Funsies
[Written @ 8:57pm - 9:00pm]
[I don’t think this is a new word since I’ve heard many other people use it . . . for example Brad Fitzgerald . . . from one of his talks on the Perkeep storage system / Camlistore project . . . . they said “funsies” once . . . . . . . . also maybe I’ve heard to one or two or three more times here and there from other people . . but Brad is the main inspiration I think. . . . . . . . . . . . . . I think it means . . . . . . . . . . . “just for fun” ? ? . . . . . . . . . . yea . . Brad said something like “just for funsies” or something like that . . . . . . . . . . . it’s something to do with fun . . . . . . . . . . or maybe experimenting. . . . . . ? . . . . . . like experimenting with a program? . . . . . “is that for funsies??” . . . . . . . yea . . maybe that’s a good . Uh . First order approximation of a definition for this term . . . . . . . . ]

Onw
[Written @ 8:46pm - 8:46pm]
[accidentally spelled “onw” when I meant to spell “now” . . . . and now we have a new word . . . . . well it looks new to me anyway. . . not sure what it should mean . . . . . . . ]

Aom
[Written @ 8:31pm]
[this is a new word . . . . . .  . it looks pretty cool . . . . ]

Explositions
[Written @ 8:07pm]
[accidentally typed “explositions” instead of “explosions” . . . . this new word looks to be a combination of “exposition” and “explosion” . . . . . . . . . . . . . it’s like . . a dialogue . . . . . made of . . . . . explosions? . . . . . . . hmm . . . . . . sounds like an action film . . . . . . . . . . . where the . . . . . . . ….. . . . . . . . wow . . . . . . I’m not a huge fan of violence . . but the idea I have in my mind . . is . . explosions like nuclear blasts . . . . . . . . . . . . . . and . . . . . . you use . . those to write a book . . . . . . . . . . . . . . . . . . . . . . . . . . . yea. . . I guess people are thinking of doing something like that for . . how to make Mars a habitable planet . . . nuking mars . . . . . . . . . . . I’m not very invested in this idea at this time . . . . . in my personal thinking logic statements . . . . . . . . . . . . . . . . . . and in this regard . . probably . . . . people can find another way to make mars exportable as an explorable planet . . . . . . . . ]


Haite
[Written @ June 2, 2020 @ 6:02pm]
[was going to type “hate” but was almost going to type “ha” and then an “I” . . . . . but I didn’t do that . . and then I wondered to myself . . what if I did add an “I” after the “ha”? . .  that would be a new way to spell “hate” . . . . . . . . the original inspiration was when I was typing the text “I pride my business on its bigness . . . don’t hate me because I’m so ambitious . . [2 @ 10:12 - 10:16]” for the video [2 @ https://www.youtube.com/watch?v=rJYLSrjFUxk  ] 


deried
[Written @ June 2, 2020 @ (approximately) 6:39am - 6:39am]
[accidentally misspelled “deried” .. . . . was intending to spell “derived” . . . . . . . . . . . . this looks like a new word too me . . . . . . . . . . . not sure what it should mean . . . ]

boolnea
[Written @ June 2, 2020 @ 5:42am - 5:52am]
[accidentally wrote “boolnea” instead of “boolean” . . I intended to write “boolean” . . . . . . . . . typographical error . . . . . . new word . . . ]

javasciprt
[Written @ June 2, 2020 @ 5:42am - 5:52am]
[accidentally spelled “javascript” as “javasciprt” . . . haha . . . this word looks strange to me . . . but oh . I guess it’s a new word and I’m a new word creator that makes me original and new and real . . and thoughtful . Thank you me . . . ]

Opint
[Written @ June 2, 2020 @ 5:23am - 5:33am]
[“point” typo]

Oculd
[Written @ June 2, 2020 @ 5:23am - 5:33am]
[“could” typo]

Erveset
[Written @ June 2, 2020 @ 5:23am - 5:33am]
[when looking at the word “Everset” . . . . . . . I think I accidentally typed it wrong at some point. . . . and “Erveset” is the result . . . . “Everset” seems .  Well . . it could easily be a new mathematical concept related terminology touch point . . . not sure what this could mean either . . .  ]

Erves
[Written @ June 2, 2020 @ 5:23am - 5:33am]
[was going to type “Erveset” after creating the new word “Everset” . . . . or rather . . “after creating the word ‘Everset’” . . since . . how long does a word last as being “new” . . when you are already experiencing it? . .  how many seconds to milliseconds to nanoseconds old do you need to be to be . . . . . . . . . . . . . “Erves” is a word now so is it really new . . in any case . . . . . here it is . . . and I’m not sure what it should mean ? . .. ]

Everset
[Written @ June 2, 2020 @ 5:23am - 5:33am]
[when I looked at the word “Everest” in one of my previous newly created word dialogues or descriptive text . . I thought I was reading the word “Everset” which . . . . . . . . . . . . . well. . . . there’s a few strange things that came too mind when I thought about this new word . . . (1) it looks like a mathematical term . . . (2) . . mathematical term related to a mountain? . . . . . a snowy white mountain? . . . . . not sure . . . . . . . . . . . . . . . well . . (1) came to me after (2) did . . . . . . later I started to think that a mathematical concept called an “Everset” could be a could idea . . . . this term reminds me of . . “Everquest” . . which is a Massively Multiplayer Online Role Playing Game or MMORPG . . . . . . . . . . . . . Everset . . . . . . . . . .cool . ]

Itnerest
[Written @ June 2, 2020 @ 5:23am - 5:28am]
[accidentally typed “itnerest” instead of “interest” . . . not sure what this new word should mean . . . . . reminds me of the word “Everest” . . like the mountain . . . Everest. . . 

Eash
[Written @ June 2, 2020 @ 5:22am - 5:24am]
[accidentally typed “easy” as “eash” . . . not sure what this new word should mean . . . ]

Jeck
[Written @ June 2, 2020 @ 5:19am - 5:24am]
[was almost going to write “check” . . but started typing the letter “j” . . . . . . . . . and didn’t completely write out “jeck” . . but it was a hypothesized word that I could have written but didn’t . . . . . . . . . . . . . but then I was like . . hey . . what the heck . . what if “check” . . started with a “j”. . . . . . . . . . . . . . . well . . . . . .this word turns out to be a surname . . . . . . . . . . . . and now that also gives me new thoughts about what words are. . . . they’re not always something that needs to mean something . . . . . . like the name “Adam” . . doesn’t need to mean so much . . . . . it can be a designation . . . . if you’re not familiar with the origin of the name or the history or etymology of the term . . . . or “Daniel” . . . or “Luke” . . . . or in Chinese “Xie” . . . or “Xing” . . . . . . . . . . . . . . so these could be just names of people . .  . . . and so maybe names of people are not necessarily easy to define . . . . . . . . . . . . . well for example people are not necessarily easy to describe . . or any particular individual rather . . the interests can be very diverse and even what a person does as a career or hobby or job isn’t all there is about that person. . . . there is so much about a person. . . ]


Functino
[Written @ June 2, 2020 @ 5:17am]
[accidentally typed “function” as “functino” . . . . . this word reminds me of “infinito” . . another misspelling that I made a while back . . . . . . I think I made that mispelling not sure would need to check my notes again . . . ]

Sor
[Written @ June 2, 2020 @ 5:11am]
[accidentally typed “sor” instead of “so” . . . this new word looks interesting . . it reminds me of “sun” .. . . . . . . . . . . . . . . . . . . . or maybe “soul” . . . . the soul of the sun? . . . the soul of a star? . . . . . . . . . . . . . . . . . . . . . . . . a sor . . . the soul of a cosmological being . . . . . . like a galactic sun . . . . or galactic star . . . . . . . . . . . . hmm . . . . . . . . . I suppose in terms of soul essences . . or soul personality essences. . . . . . a star . . . or galactic object . . having their own personality . . . . . . that’s kind of a new concept for me . . hmm . . . . . . . . . . . yea. . I’ll have to think more about this word . . and what it means for the sun to have a personality . . . . . . . . . . .]


Soncs
[Written @ June 2, 2020 @ 5:02am]
[Accidentally typed “soncs” when I meant to type “constants” . . . . . . . this new word . . . . . thanks buddy . . . . . . . ]

Tio
[Written @ June 2, 2020 @ 4:43am]
[accidentally . . . . . . . . . . . spelled “topic” as “tio” . . . . . . any thoughts ? . does this word meaning anything to you? . . not sure . . ]

Tiop
[Written @ June 2, 2020 @ 4:44am]
[accidentally . . . spelled “topic” as “tio” . . . . thought to add a “p” . . at the end of the word “tio” . . . . . . . should this new word mean anything ? . . . . . what do you think? . . what could this word embody to help us understand ourselves more as human beings who use this new word to communicate in our everyday lives? . Not sure. ]
[possibility #1]: Not sure.
[possibility #2]: May the grace be with you. Forever and always.
[possibility #3]: May there be enough courage within you to last a lifetime or two or more.
[thoughts]: when writing this message I had one person in mind and that is Theodore Nelson. Ted Nelson is a courageous person who has endeavored to create new instruments of the mind for the whole of humanity. His work stretches the possibilities of what people can do and takes us further than we’ve been by many eons. Connected pages by visible parallel links are a concept that has been ignored for far too long by the great mathematicians and engineers of our present era and finally there is a way to make them real by using the troy architecture which enables you to take a hierarchy of connected programs and encapsulate them repeatedly by this method of naming conveniences. It may take a while to build this new utopia but I think any engineer out there that is interested in building the great Ted Nelson’s Xanadu project ought to give it a try by taking advantage of the highly recursive nature of this program architecture that double loops onto itself with every bit or byte of the program that each user eats free of royalties. 



Firne
[Written @ June 2, 2020 @ 4:06am]
[accidentally misspelled “friend” . . . . and ended up typing “firne” by mistake . . not sure what this word should mean . . . . . . . looks too be a new word. . . . . . but not sure yet . . ]

Aquest
[Written @ June 2, 2020 @ 4:06am - 4:10am]
[I wanted to use a different word from “acquire” . . . . . . and so thought this new word “aquest” . .  would work too mean something similar like “to seek to quest” . . . . “to seek to receive” . . . . . . . . “to journey to retrieve” . . . . . . . . which seems to suggest more of an ongoing process of acquiring something . . a foreverness . . . . . of the thing that needs to be acquired . And that thing itself cannot truly be satisfied with itself in any sense of its acquisition . . . like tasting a sweet piece of candy from the ice cream shop might leave you wanting more . . . . . and that information of the sweet taste stays with you over time . . . . and maybe you want to come back later to retrieve a new understanding now that you’re an adult and can afford to purchase many more pieces of that candy . . . . . . and you horde the fruits of knowledge for that particular gummy sweet candy that you don’t share with anyone because you’re lost in a maze of forever seeking what the candy means in new delightful ways like trying it with different beverages or at different times of the day . . or with different beings like slugs and anteaters that crawl by so that the thought of eating the candy is new even when you thought you had already understood what the candy means to you . . . . ]


Anthides
[Written @ June 2, 2020 @ 4:02am - 4:14am]
[A word that means similar thought perspectives . . . . . . . . like regional thinking perspectives on certain issues or topics of the world or concerning the nature of things . . or something along these lines . . not sure yet . . . . . . . . something to do . . . . ]

Familial Anthisesis
[Written @ June 2, 2020 @ 4:01am - 4:19am]
[a word that means family relation by anthides . . . . . . . . . . a familial anthisesis is someone with a thought perspective parenthood relation to you like a teacher . . . . . . . . . . . . .. . . . . rather then acquiring information through blood donation like from a father or mother familial relation . . . . you can aquest information from a familial anthisesis who is like a mentor . . . . . . . . . . . . . . a close friend . . . like a childhood friend who teaches you about childhood friend experiences can be antheisesis partner for these types of experiences . . . . a swimming partner in your high school swim team doesn’t need to know you for long if they can see you care to swim as thoughtfully as they do  . . . . and so the blood is not the deepest way to be connected to someone . . but other reasons can be just as deep . . . like an entrepreneurial partner that you met somewhere and you two friends get along quite well. . . .all the business transactions and memories of going out to eat and talking about things . . . . . maybe you feel closer to this person that is your business partner than you have ever felt for your father or even your own child related wonder person . . . and so how will you describe this person to your friends . . they are a familial anthisesis . . . . . ]
[example usecase]: they are my familial anthisesis . . . 
[thoughts @ Written @ June 2, 2020 @ 4:15am]: hmmm . . . . . anthisesis . . hmm . . the meaning is really cool . . . . . . thought perspective related . . . . but it’s already so closely linked to antithesis . . . . . . . . . . . . . . . . . . . . . . just the terminology already sounds so close . . I mean . . not the terminology but the way the word sounds . . . . . antithesis . . . anthisesis . . . hmm . . . . . . . . . . . . . . . . . . . also it’s kind of a long word . . . . .  not sure . . . . maybe this is not the right word to have this meaning set out for it . . . . . . . . . . another word might work better at this point . . . . . . . . . . . not sure what that other word should be though . . . . . . . . . . . . hmm . . . . . . . . . . . . . . . . . . anthide? . . .. . . . yea maybe . . . familial anthide? . . . . . sounds a lot easier to say already . . . . . . . . . . . . . . . . . . . . . . . yea. . . familial anthide . . that’s what this meaning will be ascribed to now . . . well in my own world I guess . . . . . . . . . . . . . . . . . . or unless someone uses this word to communicate with me . . then I will have to recall this definition . . . in that regard . . . otherwise . . . . . . . . . . . here it is . . to think about . . . . . . . . . . . . . . . . . . probably other words are there that mean the same thing here. . . . . . . . . . . . . I . .  . agues I . . just like playing with . . . quests for naming things . . . and . . . making the names mean things . . . . . . . . . . easy . 

Wmo
[Written @ June 2, 2020 @ 3:54am - 4:14am]
[accidentally spelled “women” as “wmo” . . . . not sure what this new word should mean . . . . . . . . . . . or really how to pronounce it . . . . . . . . . . . . . mo . . the “w” is silent? . . . . . . . . . not sure . . . . . . . . . . . . wee-moh . . . ? . . . . . . . . . . . . . . . . . not sure . . . . . . . . ]

Sonsum
[Written @ June 2, 2020 @ 3:42am - 4:14am]
[accidentally “consumer” incorrectly. . . . . . . . I thought I was typing the word “consumer” . . . and my hands must have slipped and spelled this new word “Sonsum” . . . . . . this is a really cool new word appearance . . it looks like “sun” . . . . . or reminds me of “sun” . . . . .. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . also . . the etymology makes me feel like it is . . . . originating from Korea . . . . . . . . . . . . . . . . . . . . . like Somni 451 . . . . . . from the great movie “Cloud Atlas” . . . . . . . . . . . . . . very cool new word . . .. . . . . . the meaning? . . I’m not sure . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .Don’t give up . . . . . . . . . . . . . . . . . . that just popped in my head . . . . . . . . . . . . . . . . . . . . . . . . . yea . . so that sounds like a really cool new word with a cool new meaning to suit . . . . . . . . . . also . . . . when I look in my peripheral vision as I type these words . . my top right hand corner of the screen reminds me of . . . that lady . . . . . . . . . . . . . . . who plays Somni. . . . . . in the movie “Cloud Atlas” . . . I believe her name . . . . . . . . is Doona . . . Doona Bae . . . . . . . . . . . . . . . . . . . . . . . . . . . . that’s what I see in my peripheral vision . . . so maybe it’s also a cool sign . . . . . . . . . . . . . too show this I. . . . . . . . . . ]
[definition]: Don’t give up
[example usecase]: . . Hey you, Sonsum!
[alternate meaning]: Do your best, sam! Do your best, Chester! Do your best, Rick! Do your best, Alex! Do your best, Rae! Do your best, Isabella! Do your best, Onyx! Do your best, Starfire! Do your best, Omi! Do your best, Victor! Do your best, Stephen! Do your best, Ornicoperu! Do your best, Aubrey! Do your best, Heather! Do your best, Hentai! Do your best, Emu! Do your best, Easter Island! Do your best X AE A-12 [X AE A-Xii] . . . . . . . . . . D. . . . o . . . y . . . o. . . . . u . . . .r . . . b . . . e. . . s. . . t. . . . e . . . .v . . . . . . e. . . .r . . .y . . . . o . . . n . . . e. . > !> .! >! >! 
[thoughts]: an endearing way to tell a person to do their best. Or to be their best. Where everyone can share a common name called Somni . . . . . . . . 
[example usecase]: . . . . . . . . Sonsum Somni! . . . . . .  [this means, do your best, whoever you are . . . but the whoever you are is sort of representative of the human race I guess. . . . like Somni in the movie “Cloud Atlas” . . . is sort of a representation of all the peoples thoughts and aspirations for freedom and independence and creativity and expansion and development and growth and unbounded potential energy and continued learning and education and experience and wisdom and curiosity and heart break and trust and thought and reason and logic and men and women and worlds of knowledge . . and much more . . . . and it’s also a romantic thought to think we are all connected through these various things . . . . . . . . . and so maybe if you forget someone’s name . . . . . . . . . . . . . . maybe someone you think you should remember their name . . . . . . . . . . . . it is also a respectful symbol to use the term . . . “Somni” . . . . . . . . . . . . . . . . . . . . . .. . . . . . . or if you are . . . . . . . . . . . . . . . . just wanting too call everyone by the same name . . . and not focus on naming things . . . . . . . . or naming people . . or abiding by people’s given names at birth . . . . . . . . it can be . . just a courtesy . . . . . . . . . . . . . . . . . . . . . or preference . . . . . . and it shouldn’t offend anyone . . . . . . . . if you hear yourself called “Somni” . . . you don’t need to be fallen aback to memories of war or great torment . . . or thoughts of unease . . . . . . . . it’s like being called “Uncle” . . . . or “grandfather” . . or “grandmother” . . or “great nana” . . . even if you’re not a parent. . . . or someone who is by blood related to the person naming you these things . . . . . . . it is not just blood relations that we make with people but also familial antithesis. . . . antithesis ? . is that the right term to use here/ . . . . . no that’s not the right word . . . . . . . . . . . . familial . . . . . . . . . . . .anthisesis . . . . . . . new word . . . . . . ]


Dho
[Written @ June 2, 2020 @ 3:35am - 3:35am]
[accidentally spelled “Dho” instead of “should”. . . . . . not sure what this new word should mean . . . ]

Shul
[Written @ June 2, 2020 @ 3:26am - 3:28am]
[accidentally typed “should” as “shul” . . . . . not sure what this new word should mean . . . . . . . . . reminds me of “Shulman” the musical artist . . . . . . ]

Gou
[Written @ June 2, 2020 @ 3:26am - 3:28am]
[Accidentally typed “Group” as “Gou” . . . not sure what this new word should mean . . . . ]


proces
[Written @ June 2, 2020 @ 2:05am]
[accidentally spelled “process” incorrectly . . . . and wrote “proces” instead . . . not sure what this new word should mean yet . . . . . . . . ]

nerovu
[Written @ June 2, 2020 @ 2:02am - 2:04am]
[accidentally spelled “nervouse” . . incorrectly . . . and spelled “nerov” . . . . . . . . . . and added a “u” to continue what the word could be . . . . . . . . . . . . . “nerovu” . . this word reminds me of “neuro” . . . . . . . . . . not sure what it should mean yet . . . . ]


nerovuse
[Written @ June 2, 2020 @ 1:58am - 2:04am]
[accidentally spelled “nervouse” . .. . . . incorrectly . . . . . . . . and spelled “nerov” . . . before stopping myself to finish the rest of the word . . . . . .”nerovuse” was the result . . . and now here is a new word . . . . . . . . . . . not sure what it means yet . . . . ]

nervouse
[Written @ June 2, 2020 @ 1:57am - 2:04am]
[accidentally spelled “nervouse” when I meant to write “nervous” . . . . . . . . . not sure what this new would should mean . . . . . . nervouse . . . . .. . . . .. . . .nervouse . . . . . . nervouse. . . . . not sure . . . thinking it reminds me of . . . chocolate mouse . . . mousse . . .  . . mousse . . . . . . . . . . . mousse has the “eyh” sound at the end of the word which makes it sound like a fancy process . . . . . . nervouse could be a fancy word for saying “nervous” . . . but for fancy people . . . . . . . . . . . . . . . . . . . . . . . vouse . . . is pronounced “voo-z” . . .  vooz . . . . . . . . . ner .  . . vooz . . . . . . . . . . . . . . . . . . . . . . . . . . ner . . . vooz . . . . . . . . . . . . .  .  . . or . . . in French . . . ner . . . . . vou  . . . . . . . ner vou . . . ner vu . . . ner vu . . . . . . . . . . . . . the “s” is silent . . . . . ]

accidetally
[Written @ June 2, 2020 @ 1:45am - 1:46am]
[accidentally spelled “accidetally” . . . . . . when I meant to spell “accidentally” . . . not sure what this new word should mean . . . . ] 

accidentaly
[Written @ June 2, 2020 @ 1:44am - 1:46am]
[accidentally spelled “accidetally" . .  . . . . . . when I meant to spell “accidentally” . . . . not sure what this new word should mean . . .  ]
[Updated Written @ August 31, 2020 @ 2:39 - 2:42]
[Update to description]: [accidentally spelled “accidentaly” . .  . . . . . . when I meant to spell “accidentally” . . . . not sure what this new word should mean . . .  ]
[Updated description description]: I accidentally spelled “accidetally” in the original description of the word origin . . . and the update changes . . “accidetally” . . . . to “accidentaly” . . . with one . . “L” . . . I’m sorry for this typographical error . . . and yet that is the way this word description originally should have been written perhaps . . ]

enlighting
[Written @ June 2, 2020 @ 1:44am - 1:46am]
[accidentaly spelled “enlightening” wrong . . . I meant to spell “enlightening” . . . . and yet I accidentally typed “enlighting” . . . . not sure what this new word should mean . . . . ]

Thit
[Written @ June 2, 2020 @ 1:40am - 1:46am]
[accidentally spelled “this” . .  . . . and wrote “thit” instead . . . not sure what this word should mean . . . . . thit reminds me of the book about “twits” . . . . . . . those . . . uhh . . I guess . .they’re like witches? . . . . . . .  . . . . . . . not sure . . . . . . . maybe the book itself is called “Twit” . . . ]

Pentance
[Written @ June 2, 2020 @ 1:38am - 1:46am]
[thoughts]: pentance is like a thought that no one has thought to consider in great detail in order to avoid particular thought sounds of unnecessary detail of ordinary development of a psychological thought construction . . . . it’s like getting into the technical talking points of how a bicycle works isn’t necessarily something that everyone knows how to do . . . . . . . . . . . . . . . . . . . . . if you are a bicycle enthusiast . . these thoughts come natural to you . . . . . . . . pentance for your bicycle thoughts? . . . this phrase can mean something like . . . . do you have a chance to lighten my attitude on this topic . . . . ? . . . . . . . lighten my attitude means something like . . encourage me to learn more . . . . . or . . . help me think in terms that are most particular for your own persona at this opportunity perspective . .. . . . . . . . . . . . . opportunity perspective is like . . . not necessarily just the point in time the person is sharing the thoughts with you . . . . . but also the psychological mood of the environment in which the thought is being shared . . . . and this could really effect how the message is being received by the participating parties in the educational enlighting process . . . something like that . . . ]

Manueger
[Written @ June 2, 2020 @ 1:35am - 1:48am]
[accidentally added a “g” to the word “maneuver” as I was attempting to type it . . . . . now this new word looks really interesting and also sounds interesting . . well it sounds more interesting than it looks . . . maybe this word could mean . . people that sound more interesting than they look? . . . . . . . ]
[thoughts on a definition]: manueger . . . . . . a person who sounds more interesting than they appear . . . for example a cricket insect sounds quite interesting if you study cricket architecture and language which their sound language is like an architecture in how they can build and construct civilizational thoughts using sound patterns . . . . . . . . . . and so to a human being who doesn’t study crickets . Maybe they are not looking that interesting to be in a romantic relationship with . . . but if you take a chance to listen to what they have to say . . maybe you will remember those moments when you are a kid sitting out in your backyard . . . . . . laughing with friends . . . . and remembering the sound of crickets in the distance . . . . in the close distance . . . the crickets and their friends talk so much . . . and yet in our daily lives we hardly to stop say hello and even a handshake . . they are only small insects after all . . . . . . . . at least they have interesting things to say . . . . . . . things that remain around in our memory experience long after we grow up and leave them alone from our daily experience . . . . . . . something like that . . ]
[thoughts]: manuegers don’t have to be human . . . they can be anything that doesn’t look very interesting or appealing . . but the sounds are interesting . . for example it can also be an intellectual conversation with another human being . . . they might look bald or without facial hair and not very attractive to you personally but the sense of the things they say makes you feel wonderful and enlightened . . . . . . . . . . probably they can be butt ugly to look at but at least their words and things they say are very enthusiastic and encouraging for your romantic easy to consume sound wave monitoring psychological technological systems developed at runtime with your pomegranate head body ear drums and sausage music nasal snout]
[example use cases]:
- “Look mom, a Manueger! Let’s go listen to what they have to say!”
- “Hey honey, your manuegering is quite interesting to listen to tonight. Do you mind telling the story of how the eagles fly again?”
- “Hey manueger, thanks again for coming to this local seminar. Your thoughtful pentance in this topic is highly amusing and well sought after . . thank you again so much!”
- 



Manuever
[Written @ June 2, 2020 @ 1:32am]
[accidentally spelled “Maneuver” wrong . . . . . . . . . switching the “u” and the “e” was my original mistake . . . . . . . . a new word that looks similar and could mean something completely different has landed on earth today . .  not sure if anyone else has looked into this space craft technological device. . . . . . maybe it will land on mars someday with another meaning from when it arrived . . . . . . words that change meaning? . . . that’s a new thought? . no? . maybe / / . . not sure . ]
[thoughts on a definition]: maybe this word could mean to helicopter around in thoughts . . . .  like a train going by a train station and then leaving just moments later . . . . . but then the thought changes to something like the train is moving like a helicopter and not like a train as it originally was presented in the premise documentation model of the script . . . . so if you can imagine a vertical take off and landing train spinning around its central axis . . . . . not like going in a snake like maneuver as it originally had gone]

Exepcata
[Written @ June 2, 2020 @ 1:22am - 1:24am]
[was working on the documentation for the previous three new words “expeca”, “expecat”, “expecatation” . . . . . . . . and accidentally spelled “expectation definitions” wrong.  . . . again . . . . . . . . . but a cool new word came from this accidental mystery typing manuever . . . . . ]

Expeca
Expecat
Expecatation 

[Written @ June 2, 2020 @ 1:20am - 1:32am]
[accidentally typed “Expeca” while attempting to write “expectation” . . . . . “expeca” could then be “expecat” which could then be “expecatation” . . . . not sure what these words should mean . . . . . . . was working on "exepcata” ]


Functino
[Written @ May 31, 2020 @ 7:18pm]
[accidentally spelled “functino” while attempting to write “function” . . . . looks like a cool new word . . . ]


Hoteol
[Written @ May 31, 2020 @ 3:54pm]
[accidentally spelled “Hoteol” instead of “Hotel” . . . . . . . . . . . . . . . was attempting to write the song by Michael Jackson titled “This Place Hotel” . . . . . . ]

Teim
[Written @ 31, 2020 @ 3:29am - 3:30am] [should be May month of original writing, forgot to add; Written @ May 31, 2020 @ 3:53pm - 3:54pm]
[accidentally spelled “teim” while I was trying to spell “time” . . . . . . . . this new word . . . . . also reminds me of the word “time” . . . . . . . . . . . . it seems . . . tick-tock. . tick-tock . . . . . tick-tock . . teim . . . . ]

Tiem 
[Written @ 31, 2020 @ 3:29am - 3:30am] [should be May month of original writing, forgot to add; Written @ May 31, 2020 @ 3:53pm - 3:54pm]
[mispellings of “time” . . . . not sure if it’s a new word . . a quick duck duck go search suggests this word “tiem” is a new word . . . . . . . . . nothing came up to my attention to see that it was already taken . . for now I will say this is a newly created word . . . ]

mudle
[Written @ 31, 2020 @ 3:26am - 3:30am] [should be May month of original writing, forgot to add; Written @ May 31, 2020 @ 3:53pm - 3:54pm]
[accidentally typed “mudle” when trying to type “module” while working on typescript project called “object-utility” at this time . . . . . ]

beback
[Written @ May 28, 2020 @ 11:38pm]
[I was going to write the words “be back later” . . . and accidentally forgot to type the space character “ “ . .  . . . between “be” and “back” . . . . . this “beback” . . . seems to be pronounced behb-ack . . . . behb-ack . .  it reminds of of . . . debulba . . . . . or something like that . . that old lady from “spirited away” . . . . . “yubaba” . . . . that’s her name . . . . “yubaba” . . “bebaba” . . . “beback” . . . something like that . . there’s a psychological connection there in my mind at this time . . . . . . . the words just seem similar or someone . . . the pronunciation relations are around the same area of thinking or something like that . . not sure . . . ] 


Versionf
[Written @ May 28, 2020 @ 5:21am]
[accidentally spelled “versionf” . .. . . . instead of “versions” . .  as I had originally intended . . . adding the “f” at the end of “version”  . . . looks bizarre . . . . . . . . . if I remember correctly  . . . . aren’t there words that end with . . . multiple consonants? . . in a sequence? . . . . . . . . bamf . . . . . . . . ]

deeploy
[Written @ May 28, 2020 @ 2:22am - 2:24am]
[accidentally wrote “deeploy” instead of “deep” . . . . . I was attempting to write something like “deep object” . . . . and I suppose my fingers slipped and this new word looks . . . . like “deploy” . . . . . . but . . . . . . . . . “deep” . . instead of “dep” . . . . ]
[second comment @ 2:25am - 2:29am]: I suppose I was attempting to type “deeply” . . . . and accidentally typed an “o” . . . . between the “l” and “y” . . . . . . . . . . . was attempting to saying something like “deeply referenced” . . or something along those lines . . I’m presently writing documentation for “@practicaloptimism/object-utility” . . . . “deeply object referenced?” .. . . . . “object is deeply “ . . object is deeply referenced? . . . hmm . . . . I’m not exactly sure why it is . . or has been an easy typo for me to make at this time . . . . . . I’m thinking about objects . . . . .  javascript objects and how the properties . . . . . . . . . are . . uhh . . . . . . . . . . referenced . . . . . . . . uhh . . . . . . . to the same object . . . . if you don’t make a deep copy . . of the object . . . meaning . . creating a new object that doesn’t move the child objects but only the property names and values . . . . . . . . but with new objects . . something like that . . ]
[third comment @ 2:31am - 2:35am]: I suppose . . . now it seems . . . well I was probably trying to write something like “deep clones” . . . . . or “deeply cloned” . . . “deeply cloned” objects in javascript are clones of one another but they are not “shallowly cloned” . . . . in other words . . . . . . . . . . . hmm . . . . . . . . . . . . they are different without occupying the same place in memory . . . . . . . . . “deeply cloned” . . “deeploy” . . . . seems like a reasonable sort of mashup of these two words . . . . . . . . the “lo” in cloned . . . . . . . . . is a sort of . . . . landmark . . . . . . .”loned” . . . . “lo” . . . . pronounced . . “Loh” . . . . . . . Deeply . . . Deep. . . Loy . . . . . . Deep . . . Loh . . y . . . . . . Deep . . Lohy . . . Deep Loy . . . Deeploy . . . . this could be how I came to write this accident new term . . . . . 

tangest
[Written @ May 28, 2020 @ 2:14am]
[accidentally wrote “tangest” instead of “tangent” . . . . . . . this new word reminds me of . . . . . . . . . . . . . empress? . . . tempest. . . yes. . . tempest. . . . . . tempest. . . . . tangest. . . . . . hmm . . . . not sure what this new word should me . . . ]


Obej
[Written @ May 27, 2020 @ 7:07pm]
[accidentally typed “Obej” when trying to type “object” . . . . “obejct” . . . “obejt” . . . these are other alternatives I suppose . . . . . Obej reminds me of an Egyptian goddess or god person .. . . . . . like obelisk . . . . . the etymology seems . . . related . . . . the etymology or rather the . . . organization of the character sequences . . . something like this . . . it’s like painting with words and letters . . . . . . . . and seeing . . . . . . . . new cultures . . . . . . . . from accidental word creation process . . I guess in the case of Egypt it’s not necessarily a new culture . . but perhaps knowing the culture makes it easier to re-discover it . . in new words and wrappings of context descriptions . . . . . . not sure . . . is it possible to discover a new culture? . . . not sure . . . . probably . . . new ways of doing things . . . . . . and yet also it seems . Perhaps . . . . . . . . the culture . . . was already familiar just . . . . . . . unseen . . or focused upon . . . . . . . maybe . ]
[thoughts]: To sleep . Perhaps that should be the meaning of this new word. Sleeping 


Rasolve
[Written @ May 26, 2020 @ 4:31pm]
[accidentally typed “raso” while intending to type “resolve” . . . . . that it looked cool . . reminds me of . . . . raspberry . . . . . . . . ]


Sxiksevenaeitarah
[pronounced]: six-seven-eight-ah-rah
[Written @ May 26, 2020 @ 4:02pm - 4:13pm; time to write description included]
[a combination of “6” “7” “8” . . and “Sarah” inspired by “Fivarah” . . . . . . 678arah . . . . . . . . . a name for someone combining the numbers “678” and “Sarah” . . . removing the “S” in “Sarah” . . . . to add “678” in its place” . . . a short hand way of saying that is “replacing the 678 with ’S’ in Sarah” . . . ]

Fivarah
[Written @ May 26, 2020 @ 4:02pm]
[a combination of “5” and Sarah” . . . “5arah” but spelled without numbers. . . . inspired by the comment by Kaleigh Carey on this internet post on Quora . . https://www.quora.com/Are-you-allowed-to-legally-name-your-child-with-numbers-and-symbols-Why-or-why-not?share=1 ]

Beautifly
[Written @ May 26, 2020 @ 3:19pm]
[a combination of “beautiful” and “butterfly” . . . . inspired by the “Raku” programming language image photon layer from wikipedia . . . . “beautifly”. . . https://en.wikipedia.org/wiki/Raku_(programming_language)]

Poroject
[written @ May 26, 2020 @ 2:14pm]
[accidentally typed “Project” as “Poroject” . . . . . . looks like . . . . . or rather . . kind of reminds me of “Porko Rosso” . . the cartoon animation television movie . . . television film by Hayao Miyazaki . . . ]


Applentzia
[written @ May 22, 2020 @ 3:00am - 4:06am]
[meaning]: To appreciate someone with large breasts. To appreciate someone with giant breasts. A person with large breasts is sometimes a person who also appreciates large breasts. A person with large breasts is sometimes a person with applentzia.
[example]: I am applentziatic.
[example #2]: I am applentziatic of my high school math teacher.
[example #3]: The school counselor has applentzia for her students.
[example #4]: Images of a foreign woman are going around on the internet and sparking applentzia amongst all the men, women and children of the world.


Kurl
[written @ May 22, 2020 @ 3:00am - 4:06am]
[meaning]: Kurl is someone with gigantomastial breasts. A person with gigantic breasts with respect to their physical body proportions is a Kurl. “I love Kurl” is the expression to talk about people of the group of large gigantic great breasted people. Kurl is a singular term or expression without a plural counterpart. “Kurls” with an “s” is inappropriate terminology and using the singular term or expression is the proper etymology for the being of a Kurl or group of people who are Kurl. I love Kurl can mean I love people who are Kurl or I love the phenomenon of someone having large breasts. In other words, I love Kurl can relate to a group of people or the idea of Kurl in general which is people with large breasts.
[example]: I love Kurl.
[example #2]: She is a Kurl.
[example #3]: Oh, did you notice that she is a Kurl?
[example #4]: What are your thoughts on the Kurl phenomenon?
[example #5]: What are your thoughts on Kurl?
[example #6]: Is Kurl a good beauty standard?
[example #7]: Are there very many Kurl in your home town?
[example #8]: Did you grow up with a Kurl mother?

Jetta
[written @ May 22, 2020 @ 3:00am - 4:06am]
[meaning]: Jetta is a book that isn’t published or written due to circumstances that prevented the book from being published such as stopping from writing the book or throwing away the manuscript or burning the paper or information source or a unforetold story or death of an individual who would have otherwise written a book and distributed it for others to consume.
[example]: I have a few Jettas on my mind.
[example #2]: I sure hope it’s not a Jetta that you’re keeping from everyone.
[example #3]: Are you going to publish your Jetta, yet?
[use cases]: A Jetta, This Jetta, That Jetta, Her Jetta, My Jetta, His Jetta, etc.





[1]
Gender Pronouns
https://uwm.edu/lgbtrc/support/gender-pronouns/
￼




[2]
Vau - 6th letter of the Hebrew alphabet
https://www.yourdictionary.com/vau


[3]
Ally Beardsley’s “Total Forgiveness”
https://www.youtube.com/watch?v=ADlSydMZhIU 








