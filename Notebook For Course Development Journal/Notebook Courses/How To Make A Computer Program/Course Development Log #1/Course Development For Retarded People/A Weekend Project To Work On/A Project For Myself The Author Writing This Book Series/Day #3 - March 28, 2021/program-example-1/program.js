
// call the functions :D
start()
stop()


// define the start function
function start () {
  console.log('hello')
}

// define the stop function
function stop () {
  console.log('goodbye')
}



/*

Instructions On Starting
The Program:

- Use a command line
program like iTerm
or Operating System
Specific command
line interface like
'Command Prompt'

- Use the standard
navigation functions
like 'cd' for 'change
directory' to find
the folder of interest
where your program
file is (ie. program.js)

- Use the standard
file listing function
like 'ls' for
'list files' to show
the list of files or
folders in the current
working directory
where you're navigated

- Install node.js at [2.0]

- Use the
'node program.js'
command to start
the program.js file :O


[1.0]
A Beginner's Guide To the UNIX Command Line
https://www.osc.edu/supercomputing/unix-cmds

[2.0]
Node.js
https://nodejs.org/en/


*/