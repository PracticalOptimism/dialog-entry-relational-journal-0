
# A Weekend Project For Myself The Author Writing This Book Series


Start is a nice proportionality aya . . 

. . 

A lot of animation programs -_- seem to apply that sort of thing -_- but it's like :) yea cool . . what if we could overgeneralize around that single function and then say 'yo, that's a pretty good codebase'

. . 

start this
start that
start this
start that
start this
start that
start this
start that

. . 

- /start

. . 

typing

your program could start typing itself

. . 

- /be-born

. . 

- /start-breathing

. . 

well . . 

. . 

when your program starts . . it's really an interesting big deal . . 'what are you starting' is a question and so 

- daemon
- website server
- documentation server
- command line interface bandwagon function
- laser light shows to mars
- robot walking around like a human being
- robot thinking
- robot cooking

. . 

it's like . . start is a good place to start for nooby programmer with problems with automation . . using cocoa-script.js

so for any order 1 developer: start.js is all you need in your codebase :)

Order 1 Codebase Developer:
- start.js

Order 1 Codebase Developer Aspect Reality With Different Programming Language:
- start.yama-script

Order 1 Codebase Developer Aspect Reality With Different Programming Language Part 2:
- start.wasm

. . 

Order 2 Codebase Developer:
- /start
  - time-ordered-thing-1.js
  - time-ordered-thing-2.js

. . 

Do you look like a javascript monkey? Sometimes you don't know which brains to pick and so you ask your colleague, hey yo Bombas? do you think this brainless monkey can type javascript like you do?

So bombas responds. . yea maybe

well

time-ordered-thing-1.js is started by your colleague, hello-person-1
time-ordered-thing-2.js is started by your 2nd colleague, hello-person-2

. . 

well one codebase could be written differently from the other and so it's like . . yo call it a time ordered sequence pair . . where the two individuals can compete and compare their time-ordered start functions . . or their time ordered banana eating competitions where they start and compare various start and compare streaks of starting and starting and starting

. . 

-_- start was so much fun when you only had 1 file :O

. . 

start.js // . . : / what happened to those good ole days?

. . 

so now with a directory tree . . things become like -_- what do you mean we haven't started yet? . . is this the start of a big problem on our hands? . . why don't you just have 1 file called start.js inside of /start directory?

- /start
  - start.js

(1) the question is proposed, what scarrryyyyyyy clowns will show up next in our start directory?

- /start
  - i-am-ready.js
  - i-am-ready-2.js
  - /i-am-not-ready
    - /i-am-not-ready
      - /i-am-not-ready
        - /i-will-be-ready-in-2-years
          - /i-will-be-ready-in-2 months-after-my-2-years-parent
            - /i-will-be-ready-in-2-days-after-my-2-months-parent
              - /i-will-be-ready-in-1-second-after-my-2-days-parent
                - /i-am-ready-but-because-my-parents-arent-ready-then-my-colleague-developer-is-like-yo-where-do-you-place-this-folder-in-the-data-structure-is-it-always-going-to-be-not-ready-or-invisible-until-we-know-where-its-parents-are
                  - as-soon-as-my-parents-are-ready-i-will-be-a-spongebob-character-that-is-like-yo-did-you-want-to-genetically-introduce-me-to-your-species-yet-or-is-that-a-meme-because-your-technology-isnt-there-yet.js

-_- well that's like 

if you don't think you have the necessary pre-requisites, can you still add that material to your codebase? . . 

. . 

it's like we have an idea . . but the in-between steps are like -_-

. . 

how do you get spongebob squarepants to walk around in real life?

-_-

. . 

it's like you don't need to know what the next steps are . . 

. . 

what are the next steps for this program

. . 

anna is a time ladent computer programming language like 'i don't know what this could be but it could be what i think it is so it's like, yo'

. . 

'yo, this could be what i think it is . . but it's like hmm i wonder what that is . . '

. . 

. . 

it's like 2 days later our codebase could change and we're like 'yo!'

. . 

strt

str

starrrrtt

start.js

. . 

stttttaaarrrrrrrrrrrrrt

starttte

start.sb

start.ml

start.mx

. . 

-_- starting things is like the best thing ever . . but stopping things is possibly also really important . . 

start.js
stop.js

. . 

stop is like O_O yo . . are you just gonna keep coming up with new ideas . . or are you gonna stop and make your codebase look like 'yo, i work right now'

. . 

start.js
stop.js

. . 

it's like a line segment with 2 points . . and line segments are O_O kind of interesting

. . 

line segments are like 'yo, start. yo, stop'

start.js . . stop.js

. . 

stopping is super effective when you want to start your animation loop but it's like you don't necessarily want it to run on forever . . and so it's like

. . 

- start.js
- stop.js

those are really energy effective for any nooby programmer starting out their career . and they can probably stop there because it's like /table-flip-io [1.0]

[1.0]
TABLEFLIP
https://tableflip.io/

[2.0]
https://github.com/tableflip

. . 

alright

. . 

i'm sorry

. . 

- start.js
- stop.js

. . 

when we get solid sponsorship from functions like this . . we can be like 'yo' . . 

. . 

your mom will ask you 'hey, did you make a program today?' and it's like 'yo, i started . . xyz . . and i stopped xyz . . so yea . . '

. . 

alright well . . that's a pretty good program . . you can be proud of yourself . . it's like . . yo . . i'm a real expert now . . (1) i can start a web server (2) i can stop the web server . . 

. . 

(1) i can start a daemon process
(2) i can stop a daemon process

. . 

(1) i can open a text editor
(2) i can close a text editor

. . 

(1) i can start typing a letter
(2) i can stop typing a letter

. . 

(1) i can sleep until i figure out what to do
(2) i can figure out what to do but not know how to do it

. . 

(1) i can know how to do something
(2) i can forget how to do something

. . 

. . . 

. . 

okay that's a pretty good place to start

- start.js
- stop.js

COOL!

do you like it?

i hope you do . . it's like O_O yo . . that's interesting . . but it's still like . . yo . . we're not newbs right? . . how do you make a video game with more than 2 files? it's like -_- with pair programming in the community . . we have nice people breathing down our necks to say 'yo, split up that file architecture into a gazillion files and so it's like, yo can you start sharding those cubes so we can like runtime connect that stuff?'

. . 

well that's a good stopping function for all of us isn't it?

if you have a nice person with a beard asking you to do things it's like 'yo, alright i'll stop and listen to you'

. . 

alright grandma . . what kind of codebase do you want to look at for ya futu-a . . 

. . 

alright

. . 

// program.js

start()
stop()

function start () {
  console.log('a')
  console.log('l')
  console.log('r')
  console.log('i')
}

function stop () {
  console.log('g')
  console.log('h')
  console.log('t')
}

. . 

- program.js

. . 

. . 

Directory Structure Order Type 1

- program.js

Directory Structure Order Type 2

- start.js
- stop.js

. . 

:O

. . 

so if you wanted to . . you can concatenate start and stop . . to be in . . program . . so it's like :O . . yo!!!!!

. . amazing . . look how easy it is to program :D

. . 


[Journal Entry Continued @ March 28, 2021 @ 23:45]

- making a text editor
- what are your tiles?
- what are your tile sequences?
- what are your tile sequence order mappings?
- what are your tile sequence order arrangement mappers?

A text editor looks pretty cool . . 

. . 

-_-

. . 

-_-

. . 

[jon ide]: i really don't want to call my programs tiles -_-

[jon ide 2]: yea, well it's like -_-

[jon ide 3]: what do you mean by -_- jon ide 3?

[jon ide 4]: well, i think he means that there is a generalization of space and time like 'yo, we could possibly recreate experiences from the olden days just by all of a sudden 'believing that that is our new past life experience'

[jon ide 5]: yea, but look at the git log history and you might agree that no events are really changed

[jon ide 6]: yea, but maybe look at the git log history again and see that all events are created all at once and forever for all possible types of events and event sequence log pairs so it's possibly like -_-

[jon ide 7 sama]: well, yea that's what i've been reading in one of my books by 'Jane Roberts' and 'Seth' and 'Robert F. Butts' that 'all reality exists in the so called 'moment-point' which is 'now''

[jon ide 8]: well, that's all very interesting but what does that have to do with (1) ecoin (2) codebase data structure (3) timely meal preparation tactics?

[jon ide 9]: timely meal preparation tactics can come with an 'up-front' cost which is like 'yo, do you agree to suspend your belief in this or that area until the time comes?'

[jon ide 10]: well yea, it's like

[jon ide]: yea, i'm the real jon ide

[jon ide]: i'm the real jon ide

[jon ide 12]: no, you're all fakes

[jon ide 13]: yea, fakes

[jon ide]: alright you're right

[jon ide 19]: fakes

[jon ide 20]: alright, a nice codebase architecture would be like 'yo, this is an example of what 'could be''

-_- well this is all really spookkkkkyyyy science

-_-





