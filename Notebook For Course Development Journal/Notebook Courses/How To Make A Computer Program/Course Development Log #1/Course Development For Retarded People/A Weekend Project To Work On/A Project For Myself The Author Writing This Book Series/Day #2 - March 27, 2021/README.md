

# A Weekend Project For Myself The Author Writing This Book Series

/project-directory-tree
  - /project-directory
    - /welcome-to-this-project-directory-tree
    - /attend-to-this-directory-if-you-are-new-to-this-project
    - /attend-to-this-directory-if-you-want-some-help-with-this-project
    - /attend-to-this-directory-if-you-want-comfortable-information-to-calm-your-nerves-when-you-find-errors-with-this-project
  - /project-artificial-intelligence-spy-computer-program
    - /project-directory-updates
    - /project-directory-things-that-are-being-spied-on
  - /project-security-spy-computer-program
    - /here-are-some-things-that-you-are-not-allowed-to-access-during-the-development-of-this-project-directory
    - /here-are-some-things-you-can-quickly-find-in-our-project-directory-tree


. . 

- create new timelines as you need to
- create new lines with ordered items as you need to
- start from 1 and move to the end of the line for each line that you create

. . 

- your timeline can start with 'i don't know'
- aya is a short-hand notation for 'i don't know'

. . 

aya aya aya aya aya aya

is a good first timeline to begin with

. . 

your game is in good hands if you document your development process . . 

(1) what do you know
(2) what do your teammates know
(3) what do aliens know that you can beg and preach them to reveal to you

. . 

timelines are fun to create
each of these lines of text in this text editor, environment, can
  - be considered as it's own unique individual timeline

also timelines can be thought of as 'comma-separated' and , so, time, is , like, whatever, you, want, it, to, be

- time
- can
- start
- like
- this
- as
- well

. . 

aya is a good timeline because it can be like

aya aya aya

. . 

okay if your directory tree looks like 'aya aya aya' it's like -_- maybe that's the best xD

- /aya
- /aya-2
- /aya-3
- /aya-4

which are all meant to be the same aya but it's like -_- there are timelines about 'not hyperinflating the same character sequence in the same bandwidth of tree timeline dialogues and so in other words, don't name different files the same name' which is like -_- don't know . . maybe the names of the files under the directory tree syntax log could have said 'yea, but we can use random numbers to represent the real names or the IDs of the files so if you really wanted to you could use the same name multiple times xD'

- /aya // underneath the hood it's like (1.1.12039494)
- /aya // underneath the hood it's like (1.3.4.3.2222)
- /aya // underneath the hood it's like (1393.3.2222)
- /aya // underneath the hood it's like (347774.284843)

. . 

you can ignore the last paragraph since it's like -_- /shrug-maybe-i-would-like-to-delete-that-paragraph-and-so-it's-like-in-a-PLEASE-IGNORE-ME-Timeline

. . 

// comments are like, yo, compiler, please ignore me

. . 

/*
Yo, compiler, please ignore me
*/

/*
ignore this
*/

. . 

- /space-order
- /time-order
- /need-to-know-order
- /nothing-else-number-1-order
- /nothing-else-number-2-order

. . 

-_-

. . 

need-to-know is a big deal . . basically . . you have to write down all the things that you know

. . 

if you team is just starting out in software development that is a really cool thing to start documenting

. . 

- /need-to-know-order
  - /what-do-we-know
  - /what-do-friends-know
  - /what-do-the-aliens-know

-_- this is maybe a difficult timeline to believe because it's like -_- . . when we apply the so called 'we are one' principle -_- it's like -_- . . 'we' 'friends' 'aliens' -_- are we not all -_- the same -_- ? well it's like -_- whatever . . maybe there are ordered proportions with one of ourselves that are interesting to observe or discover or implement as part of the diagonalization strategy of our hierarchical codebase . . am i rite?

-_- it's like whatever -_- maybe we can deal with this type of data structure since again . . i myself am really not observing all the possibilities right? i'm an ignorant fool banana that needs to learn-at-runtime what is good and bad strategy of programming computer [minus the hypopotomus jokes]

. . 

-_- well the education series is really bad if you think about it because it's like -_- i don't like jokes in education series so it's like -_- well let's just subscribe to sub-mayonaise jokes that are midly offensive for the sake of the live stream but if we are publishing a dictatorial edited edition, those jokes should be thrown out as soon as possible because mayonaise is gross and offensive to some cultures of human insect hybrids like myself

. . 

i'm sorry with how this has gone so far . . but that is also an example of the random potential that 'need-to-know order' is all about . . -_- it's like 'if you know something . . you are going to get away with a lot'

-_- you could be a really demonic person and start spamming nuance comments on the internet and peoeople would be like 'yo'

-_-

. . 

okay well it's like -_- i don't really know the best strategy at what it means to 'need-to-know' but it's like -_- . . if you know something . . let me know . . right 'friend'? . . right 'alien'?

. . 

- /need-to-know-order
  - /what-do-we-know
    - /we-know-about-software-engineering
      - /we-know-javascript-is-interesting
      - /we-know-games-are-interesting
      - /we-know-interesting-things-about-math-and-timelines
      - /we-know-interesting-things-about-be-kind-and-patient-with-your-friends
        - /we-know-things-about-kindness
        - /we-know-things-about-patience
      - /we-know-that-aliens-are-like-what-the-fffffff
      - /we-know-that-wow-am-i-having-a-headache
        - /if-we-told-you-things-it-might-give-you-a-headache-so-please-dont-ask
        - /you-are-annoying-and-i-dont-like-you
      - /we-know-things-like-you-are-me-and-i-am-you
  - /what-do-friends-know
    - /our-friends-know-wow-you-should-learn-javascript-its-so-popular-love
    - /our-friends-know-wow-you-should-learn-typescript-its-so-popular-love
  - /what-do-the-aliens-know
    - /our-alien-friends-are-like-hmm-interesting
    - /our-alien-friends-are-like-hey-nice-idea
    - /our-alien-friends-are-like-would-you-like-to-know-our-opinion-on-javascript-just-give-us-a-chat-sometime

. . 

okay there are a lot of timelines you can create 

. . 

one of my favorite timelines these days as a so-called 'need to know' person . . is the timeline of 'ordered proportionalities'

you can probably predict my ordered sequences of commentary if you are an alien and you can watch the replay of what i will type in 2 seconds from now or 5 minutes from now . . but that order of proportionality metrics is possibly not availble to you if you are a human who hasn't already seen this video or if you are a human who doesn't have another time proportionality metric of 'yo' 

right?

right?

well i don't know

. . 

hmm . . 

time order is still like -_- i really have no idea what that looks like because in reality it seems like 'you create your own timeline' is the order of proportionality aya that 'you the reader of this broadcast message' will adhere to -_- -_- i really don't know but for example it's like . . in practical terms that means something like . . if I recommend a list of folder names . . for the directory tree structure for your project . . it's just going to be something like 'yea, that works for that usecase that you had in mind but i have another topic in mind which doesn't seem to easily map to what you're talking about . . do you have some special knowledge that i don't have? do you need me to know something before i can upgrade codebase data structures?'

well . . in all honesty . . no i don't really know any special data structure but i do have many measurements that i've taken and so it's like . . yo . . this video is just like a tutorial of things that i've learned about so far . . up to this March 27, 2021 which is like -_- nicely proportional to my current age as a human being of 27 years old [peace-sign-emoji-haha-smiley-face-aya-emoji]

well you are possibly aya-years old and so it's like -_- hmm . . i don't really know what to recommend to you 

it's going to have something to do -_- of what you know

. . 

so you'll ask yourself stuff that i've never even asked before and it's like -_- well i'm sorry this dumpy data structure fell apart on you . . 

. . 

- /space-order
- /time-order
- /need-to-know-order
- /nothing-else-number-1-order
- /nothing-else-number-2-order

. . 

- /space-order
- /time-order
- /need-to-know-order
  - /this-is-what-me-and-my-co-developers-know
  - /this-is-what-the-local-developer-community-knows
  - /this-is-what-the-global-developer-community-knows
  - /this-is-what-the-galactic-developer-community-knows
  - /this-is-what-the-O_O-developer-community-knows
- /nothing-else-number-1-order
- /nothing-else-number-2-order

. . 

orders are kind of fun . . but it's like -_- maybe you don't like them for your own personal reasons which is understandable . . 

or also maybe you know something that i don't know . . 

but i uh . . 

i don't know

i have so type of feelings for them so it's like hmm . . i wonder if saying 1, 2, 3, 4, 5, 6, 7, 8, 9 is not a bad thing to prefix your names with . . to suggest some type of special type of order or something like that :O

. . 

s1, space order 1
s2, space order 2
s3, space order 3

. . 

t1, time order 1
t2, time order 2
t3, time order 3

. . 

n1, need to know order 1
n2, need to know order 2
n3, need to know order 3

. . 

ne1_1, nothing else number 1 order 1
ne1_2, nothing else number 1 order 2
ne1_3, nothing else number 1 order 3

. . 

ne2_1, nothing else number 2 order 1
ne2_2, nothing else number 2 order 2
ne2_3, nothing else number 2 order 3

. . 

there's a special syntax for when you make typographical errors . . 

if you make a typographical error, for example, when typing something that you think is "important" then you can say something like

"yo, i better restart that or else it's not really as important as I thought it would be because i want to make 0 mistakes when typing that special perfect stuff"

and so it's like

"yo, i better type that special special stuff without any typographical errors"

well it's like a sort of -_- i would hate to call it this but -_- pseudo-algorithm because it's like -_- in a half-ghost state where you are partaking in a -_- 'wait, shut me up for a while until i get this perfect' style of lifestyle . . which is like -_- not everybody likes that sort of stuff -_-

-_- i don't know -_- . . 

. . 

space orders are so cool because 'you can actually type stuff' like a nice haircut on your head which everyone can read at compile time

'''''''
| o o |
|  _  |
 \___/

which everyone can read at compile time

compile time is like 'yo, yea i saw that right?'

'look at the state of this variable'

let x = 


at runtime you don't really know what x is right? it's like -_- hmm is it really 1 ? . . it's like -_- i guess i could compile the answer in my eyeballs by taking a look at the memory structure underneath right?

. . 

i guess x is like -_- i guess

. . 

but it's like x is -_-

. . 

well runtime stuff is really weird and you don't always necessarily want to say 'yea, compile that runtime state to something that i can read with my eyeballs compilation engine'

'yea, compile that runtime stuff so i can read it in the computer'

. .

imagination-to-real-life compilation could be like O_O do we really want those things flying down from the sky because this or that person imagined it to be so? well we better have a physics for that ready to go if people are serious about that baombarmed scehem of sceicen 

. . 

okay . . well

. . 

some of these things are like -_- wow . . i really just want to work on my program right now . . why do i need to write out a list of -_- . . 

- /space-order
- /time-order
- /need-to-know-order
- /nothing-else-number-1-order // these could be like dream realities
- /nothing-else-number-2-order

. . 

I had a dream 1 or 2 nights or days ago . . according to whatever scheme you want to read that dictation notation . . nights . . days -_- 

. . 

1 or 2 days ago . . in 24 hour terms O_O . . and not necessarily in proportionality aya terms like O_O 1 day meaning 1 lifespan or something like that O_O or 1 day meaning one human evening O_O or 1 day meaning one earth solar cycle or 1 earth year or something like that O_O . . or 1 day meaning a variety of other opinionated 'normalized day meaning terms' O_O . . 

. . 

I had a dream 1 or 2 nights ago O_O . . 

One of the things that a group of kids in a school classroom told me . . or it was maybe 1 kid or uh . . O_O it was like a chubby kid in class that was talking . . and I was like . . standing up . . O_O and it seemed pretty chilled and relaxed . . they were like 'yo, ecoin is like a meme . . people start memeing on it with . . bombardment techniques like Distributed Denial of Service attacks' and it's like 'yo, do you want to let that stuff happen?' 'you should implement this thing called Vistaz' or 'Vistas' which is like a 'proportionality aya computer program' that lets those schemas-scheme and the network stay online for long enough for other 'normal people' to use the network

. . 

well 'Vistaz' or 'Vistas' is a new term for me but wow O_O it sounded so cool when learning it from these grade-school kids in class . . it was so cool :D

. . 

well in that dream it was like . . (1) real life (2) -_- people talking to me casually as though they're -_- watching me -_- from afar -_- in dream realities that are like -_- normally you don't have a key to this space -_- -_- but for this dream you'll have a key -_-

. . 

so it's like 'nothing else #1' could be like 'a dream reality' where you don't know or can't imagine the schemessssssssss

. . 

nothing else #2 is like . . 

1 is like 'yea, look at that'
2 is like 'yea, but now categorize that but then do that again without looking at what you had previously done; which is kind of a contradiction because 'previously' suggests 'you had already known something before which was supposing your way to move forward with that'' and so it's like 'no, you don't really know what this is'

. . 

1, 2, 3

. . 

1, 2, 3 is like a triangulation scheme that says something like

(1) start with 1
(2) move forward
(3) complete

. . 

2 is like . . if you move forward . . you're not moving forward from anything . . and so it's like . . 'no, you haven't moved there'

. . 

completed them

. . 



- /space-order
- /time-order
- /need-to-know-order
- /nothing-else-number-1-order
- /nothing-else-number-2-order
- /<i-dont-know>

. . 

yea -_- personally i'm not really sure -_- where to go from here . . the idea is -_- whatever -_- it's really going to possibly depend on what you know -_- but it's like -_-

now i'm onto a particular type of time-scheme

'what if the developer could say something like, yo, 1, 2, 3 my codebase is done'

'1, 2, 3 my codebase is done' is like . . 'yo, look at that . . yo . . look at that . . etc etc . and then it's like -_- well i guess for a first time developer it could be like -_- that could have taken me a while . . but at least any repeated orders will be like :O . . easy enough . . easy enough . . easy . . enough . . '

. . 

Order 1: first time developer is like O_O what the heck is this?
Order 2: second time developer is like p_p alright, i see
Order 3: third time developer is like :O easy enough 
. . 

Order N: nth time developer is like :D yo

. . 

alright

. . 

I'm not really sure . . but . . maybe it's like -_- . . 

. . 

// set up your entire codebase framework now
// type in all the console.log statements only
// type in some fake comment code like /* yea, update that */
// type in some more fake stuff as you need to
// type in that real smorgasboard whatever that's like, yo

. . 

. . 

/*
tell me about what time mana you have

[jon]: what do you mean by time mana?

[isabel]: your time mana is like a stupid clock

[jon]: i see, you want me to count stupid ideas?

[isabel-2-fake-version]: yes, fake

[jon]: there [are] a lot of stupid ideas but it's [like] yo, i want to copy that one

[isabel-darling-clock-3]: alright, you're an idiot

[jon]:

[isabel-darling-clock-9]: alright, good work team

[jon]:

*/
/*

development is like -_- yo

space order games are like :O yo . . i love your super graphical animation schemas . . please enhance my life with that stuff and stop focusing on your codebase fork organization which is like a parallel reality version of whatever because i don't care about that stuff as a loyal customer . . i wanna see that seasonal spam of 'wow, alright i love this game so much it's super elite, thank you so much for faking everything and letting me play this super elite game emissary machine enigma'



*/
/*

super graphical animation schemas are like O_O oh my gosh . . that's what i came for

*/
/*

super graphical animation schemas are like O_O yo . . am i really allowed to use a keyboard that's brainwashed with hypnotic drugs and slimes and slugs plastered all over my forehead to program-slash-play this video game on how it's supposed to treat my eyeball nightmare machines?

*/
/*

super graphical animation schemas are like O_O oh my gosh i don't even need to breath air

*/
/*

*/

alright well . . 

. . 


O_O alright . . cool animations are like O_O

. . 

yea but your codebase doesn't really need to be a cool animation xD

. . 

right?

. . 

wow

. . 

okay we want our game to be elite . . but our codebase is like -_- 'do i also need to be elite?'

. . 

-_-

. . 

proportionalities are cool xD but it's like O_O yo O_O i'm gonna have to learn what those are xD

. . 

it's like . . 

. . 

oh my gosh O_O a runtime created codebase is like [face-palm-emoji] that builds all of the codebases we're considering all at once without a flaw in the proportionalities of available codebase runtimes xD look at that O_O

. . 

O_O

. . 

overgeneralization-aya
overgeneralization-aya
overgeneralization-aya
overgeneralization-aya
overgeneralization-aya

. . 

which is like 'yo, are you proportional to one of these over generalizations?'

. . 

. . 

. . 

okay . . 

. . 

I really don't know . . it's like maybe I'm trying things out . . and so it's like . . a proportionality constant of 'start'

. . 

start

. . 

- /start

. . 

- start.js

. . 

/* 

// file-name: start.js
// file-version: order 1, just an animal trying things out

[developer]: yo, program. Start.

*/

. . 

/*

// file-name: start.js
// file-version: order 2, just an idiot thinking about brains

[developer]: yo, program. Start this stupid thing already. Do that thing that I really want you to do. stop lazing around! animal!

[program]: yo, alright.

[program]:

console.log('yea, this is that thing my boss was asking me for')

*/

/*

// file-name: start.js
// file-version: order 3, yea, now remove the funny talk

console.log('yea, this is that thing my boss was asking me for')

*/

/*

// file-name: start.js
// file-version: order 4, yea, now write some real code -_- newb

/*

alright well, here's some real code but it's in a comment so it's like it probably doesn't work. It's just like . . yea i want to say that i really like programming and so it's like i need to follow my programming orders and not write nonsense specific suits without editing my typesetting

// call the start function
start()

// define the start function
function start () {
  // yea, get that one thing from the network
  // yea, get that one thing from the network
  // yea, check if that one thing looks like 'yea'
  // thanks for that, so now just like uh, return 0
}

*/

*/

/*

// file-name: start.js
// file-version: order 5, yea, now write some real code, no weird comments

// call the start function
start()

// define the start function
function start () {
  // yea, get that one thing from the network
  // yea, get that one thing from the network
  // yea, check if that one thing looks like 'yea'
  // thanks for that, so now just like uh, return 0
}

*/

/*

// file-name: start.js
// file-version: order 6, yea, now write some real code, be specific


// call the start function
start().catch(() => {/* */})

// define the start function
async function start () {
  // yea, get that one thing from the network
  const thatOneThing = await fetch('/whatever')
  
  // yea, get that one thing from the network
  const thatOneThing2 = await fetch('/whateeevvverrrrrr')
  
  // yea, check if that one thing looks like 'yea'
  if (thatOneThing !== thatOneThing2) { return }

  console.log('yea')

  // thanks for that, so now just like uh, return 0
  return 0
}

*/


