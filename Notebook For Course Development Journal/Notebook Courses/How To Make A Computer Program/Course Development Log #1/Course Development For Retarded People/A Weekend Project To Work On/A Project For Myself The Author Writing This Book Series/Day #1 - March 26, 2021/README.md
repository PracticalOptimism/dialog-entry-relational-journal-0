

# A Project For Myself The Author Writing This Book Series

A myth is a story that we can all believe.

A myth is a science fiction class of phenomenon that is attractive and scary and interesting and frightening and horrifying and exciting and exaggeristic and phenomenological and we can all believe it because it seems so real.

If you like stories, it is really cool to draw photographs.

Photographs are fun to make.

. . 

I would like to play a game with myself to create a photograph of a memory routine that we can all play as project courseware developers.

I don't know how this will go but I think it is thoughtful to think about this sort of thing.

Uhm

. . 

. . . 

. . 

There's a homework project due in 

. . 

. . . 

. . 

If the homework project is not completed by the deadline, there is a big deal about this like 'I don't agree with you that the homework is very important'

It is very crucial to have a working algorithm for you to complete your homework project.

If you can complete your homework project, you can get a good grade because your algorithm printed the correct solution for your particular class.

For your particular class, or for your particular class lesson, you are learning about graph data structures like 'how do you get from point A to point B without skipping out on the few other graph edges that are causal or accidental or important?'.

Well, if you can complete the graph traversal algorithm then you can say your project is finished just like you intended or asked for or planned

If you can plan a specific set of events to occur with probability 1 in some eventually consistent respect, that is pretty nice for you to say 'yea, I agree, I do create my own reality'

Is that correct? I don't know but I suppose you and I can suppose that together right? I'm not sure.

Okay, well, it's really difficult to get started anywhere without maybe aspecting a few decision metrics like

'where are we?'

'where do we want to be?'

'where are we going (do we want to be dead)?'

. . 

I took notes on a style of program development that is like 'interesting' and so it's like . . 'hmm . . I think that would work' . . but it's like . . you'll let yourself look like you haven't known another way of programming after 4 or 5 steps in the program . . or maybe 6 or 7 but I'm not really sure . . the program is like . . 

(1) yes, I agree there is a problem

(2) yes, I agree there is a solution

(3) yes, the solution is like
  - (3.1) do some research
  - (3.2) ask a question about your research topic
  - (3.3) ask a question about the dilemma in question
  - (3.4) ask a question about the proportionality measuring spama which is like a dialectic diagraph of 'wow, I seriously have to consider a new approach that is at least seriously considerable in areas of interest proportions no greater than many magnitudes or less' and then you have to speak in a toned language that supports that type of creativity characteristic measure which is like javascript or any of the other of myriads of programming languages that are very useful or popular in the culture of interest :O.

(4) ask a question like 'did I solve this problem before or do I have it in cache memory which is orders of magnitude more considerable than the previous aspect dilemma because of inconcernability crisis problems like 'is it really seriously something that can be replicated once or twice or more than once or twice when number spaces are less likely to occur in certain spans of thoughts<?>' Well that it is considerable to say that 'yes, cache memory is easier and better said in a programming language like javascript where the memory model is perhaps something like 'yea, I can agree that caching that is timely and effective on various orders of magnitudes worthy enough for the cache to make sense for variable delay efficiency try-catch-block statements''

(5) Do you agree on the enemy's statistics for offending you? If you do then you better say that you have no better tactic to defend yourself in protecting your codebase's affirmation criteria

(6) And now you are really out of luck because of your enemy and their discourse to stop you and stop you and stop you and there is hardly any delay in between the stops and so you're like 'well, I really can't get through to the proper solution now, can I? Well I really wish I had a god to assist this god hopping strategy of 'wow, I would really like to complete my video game statistic wouldn't I?'' and finally

(7) [Seven, ] which is really like infamy because you're going to get in a lot of 'eximana' trouble for what you've done


This is a video description for [1.0]

References:

[1.0]
Ecoin #505​ - Live development journal entry for March 25, 2021
By Jon Ide
https://www.youtube.com/watch?v=19hx3M2An2Y

. . 


dialectic diagraph
A dialectric diagraph is a systematic type of computer science approach to say something like 'do you agree with combat statement?'

In electrical engineering, a dialectric is a computer component that is like 'do you agree with this computer statement?'

For a dialectric like air or gas in the atmosphere, the question can be asked between two electric plates like 'copper' or 'aluminum' and the question reflects the answer of 'why do you have to tell me about electric potential differences between two electric plates?'

If you have 2 electric plates like 1 is made of copper and 1 is made of copper as well . . that is . . a good capacitor plate system where you can say . . 'place a dialectric between the two plates' and ask the question of 'how strong is this dialectric?' . . 'the dialectric constant is a value that is normally measured to be greater than 0 or 1 or whatever -_- depending on the measuring system' it's normally a positive number from what I remember in electrical engineering class . . -_- but also I am not an expert in this topic so I'm very sorry

If you have dialectrics like titanium dioxide . . that might be a good one to use for saying something like 'yea, these two plates will rarely ever communicate with one another with electric strong capacitance activities like sparks of electrons flying on from one plate to another' . . and so if the dialectric isn't strong enough . . like say 'thin air' for example . . then it's possibly likely to have a short circuit where the plates are able to electrify one another . . 

Well . . a dialectric diagraph is like 'a diagram that shows you 'graphs' of dialectric potential energy metrics' like

imagine a submarine of energies like

'copper energy diagraphs'
'aluminum energy diagraphs'
'something-something energy diagraphs'
and so you have a variety of materials to consider but it's like

'yo, what's like the corner of radii between all of these various different potential copper-aluminum pair windings that are like 'yea, we'd love to test all the various material combinations all at once' within various environmental conditions' all at once and forever and ever so when the environmental circumstances of the planet change it's like 'well, we can always have a base metal of materials to test our graphics related epplication theories on''

well if the things don't work out like you had previously calculated or something like that then it's possibly likely that your material science has really changed dramatically and so your material diagraphs are really slow and potentiating enough 'exima' to really secrete the probable proportionalities that you would have otherwise expected from those material conclusions

'exima' is like a made up word to mean 'yea, well it's like a fake materiall illusion but you don't necessarily know that yet, so it's really fine to not assume that stuff anyway'

. . 

corner of radii between all of these various different potential copper-aluminum pair windings means to say something like

(1) material element #1
(2) material element #2
(3) material element #3
(4) material element #4

(A) dialectric material A
(B) dialectric material B
(C) dialectric material C

. . 

corner of radii says 'material element #1' and 'material element #2' are connected by 'dialectric material A' and 'material element #3' and 'material element #4' are connected by 'dialectric material B' and so

combination #1: element #1, dialectric A, element #2
combination #2: element #3, dialectric B, element #4

and the 'corner' is specified to be 'are there angles of interest' to consider when looking at the various charge potential windings between these two different types of combinations?

combination #1 is like 'X, Y, Z, X, Y, Z, X, Y, Z, X, Y, Z' and those are the numbers involved in how you read the diagraph chart for that combination . . but those readings aren't necessarily uniform and comparable in a one-to-one reading of how you would map those numbers to compare . . to combination #2 . . without having like 'an oscilloscope device that says that the two readings are able to be read in a nice way like 'yea, just look here and look there and you can compare and contrast the two numbers in a one-to-one map reading in some sort of way' like saying the height of one kindergarten student is comparable to the height of another kindergarten student . . but we don't necessarily have the measurement equipment to say 'which planet are these kindergarten students on?' 'do the body composition ratios really change so drastically to say that those students aren't necessarily so easy to compare and contrast in those various different environments?' 'and so imagine possibly something like a car train of small people that are like really short when they are in a car but when they are outside of the car and standing on the planet, they could be really tall and so their proportions are possibly difficult to calculate or compare in and outside of the vehicle so it's uh 'incommencerable' if you want to call it that 'but personally I'm not sure how that stuff works in practice and so it's like /shrug-my-shoulders ¯\_(ツ)_/¯ and I hope that was a good enough explanation of things for you'

'X, Y, Z, X, Y, Z, X, Y, Z, X, Y, Z' are symbolic tree references to embed the idea of '1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12' or something like that where X, Y, Z etc. are numbers and not necessarily horizontally orthogonal . . meaning they are repeated in their symbolic meaning across the chart of listed index pair windings . . and so X doesn't necessarily equal X along the chart and it's only meant as an example to showcase the ideas of 'continued sets' like 'yea, just keep going like that' like . . etc. etc. etc.

Here are some more examples of 'continued sets that are like representative of example 'unwindings' of XYZ-representation continued sets notation 'X, Y, Z, X, Y, Z, X, Y, Z, X, Y, Z'


'12, 9393, 847, 2774, 8383, 29292, 39393, 57, 48484, 292, 84, 2'
'1, 3, 4, 5, 3, 2, 3, 4, 5, 3, 2, 4'

123-representation of continued sets is possibly interesting to consider as well :D

1, 2, 3

. . 

okay, it's like 'yes, we are complete'

(1) one usecase is to end mathematical algorithms with 1, 2, 3
(2) another usecase is to say, argument (1), conditional suggested approach at unwinding the problem (2), complete (3)
(3) another usecase is to say something like 'orders of magnitude' 
  - (3.1) aey, aye, eay, eya, yea, yae (orders of combinatorial yama)
  - (3.2) 1, 2, 3, 4 (orders of ordinal numbers)
  - (3.3) 1, 2, 4, 8, 16, 32 (orders of powers of 2)
  - (3.4) 1, 10, 100, 1,000, 10,000, 100,0000 (orders of powers of 10)
  - (3.5) X, Y, Z, X, Y, Z, X, Y, Z (orders of aya-aya-aya)
  - (3.6) Triangulation-Order (a special type of order that's like 'yea, repeat all of the things that I've just said but it's like a joke or a trick or a paradox because you can't split your consciousness up into enough pieces of time aware paragraph syntax statements to really triangulate or orderize those variable effective original locations which are like 'is there a time paradox, I thought I counted you unlimited numbers of time already, how can you not be already an answer to the triangulation dilmma of trying to order those variable triangles in ordered sequences like 1, 2, 3 but it's like you forgot to say '1' was really like a thing about 'yea, please slow down' since '3' is like yea, please don't finish anything [else][except-][the][one][] one is like [no-time-syntax][yes-time-syntax-because-orders-can-say-now][no-time-syntax-because][orders][orders-are-like-numbered-sequences][numbered-sequences][numbered-][no-number-is-like-now][now-number][now-now-now])

1, 2, 3 is an ordered proportion :D
5, 22, 86 is an ordered proportion and so we could replace our understanding of orders and say something like 'yea, that's as easy as 5, 22, 86'
1 thousand, 8 million, 99 billion is an ordered proportion . . 

If you have uh . . 3 numbers uh . . in a list from least to greatest then that is an ordered proportion . . but that is only one type of ordered proportions you could say . . you could say :)

1, 3, 2 is -_- hmm . . well at first it doesn't necessarily look like an ordered proportion because well . . it's not in least-to-greatest order . . and so it's like /scratch-my-head-emoji 'how is this an ordered proportion?' well it's like you can probably find one if you look long enough . . but if you want my personal advice let's go through one or two examples of what that could look like

1, 3, 2 . . okay so 1 is like . . 'yea, look at me . . ' and '3 is like . . okay . . well that's unexpected . . I'm greater than that number after me' . . so it's like . . you need to toss a dice in the air to see if you really want to say something like 'yea, make that number after me look like a number . . that should be before me . . ' . . 

okay . . our dice has 3 numbers on it . . 1, 2, 3 and so let's roll it . . 

our first roll is like : 1 . . :O
our second rolle is like : 2 . . :D
our third roll is like : 3 . . D:

okay well . . 

xD 

100, 200, 150 is another order that is like 'is this an ordered list?'

well you're right . . it's like it's not really ordered . . 

least-to-greatest order is like 1, 2, 3
power-of-2-or-greater order is like n^0, n^1, n^2, n^3, etc.
existential-crisis order is like O_O, O_O, O_O, O_O

finding pokemon in the museum is probably like O_O . . but they are all pokemon so it's like 'well, there's probably an order here right?' . . 

aya proportionality agreements are like 'yea, but I don't necessarily know what that order is'

'trucks', 'cars', 'trains' are like 'well, what in the world is the order?' 'is it, one is greater than the other? is it power of 2 order?' 'existential crisis order' because it's like 'well, do we really know what they are?' 'it's like -_- a toy example of what it could be right?' it's like

okay




. . 

. . . 

. . 

-_-

(1) The problem is to have a computer program written in javascript that can tell us things about the future of programming languages :D by introspective analysis like 'yo, that's a cool new feature right?'

. . 

(1) I like the idea of having javascript say something like 'hey, by the way, that's a pretty cool feature of previously acknowledged statistics that you can take advantage of in this computer programming level environment right?'

. . 

(1) I like the idea of 'hey, that's a cool video game that is like 'wow, let me see . . I guess all the basic video games are like . . 1, 2, 3 . . easy to make now right? . . haha . . I love it . . '

. . 

(1) I like the ideo of being able to make a video game with like 1 or 2 packages because you're like really efficient and really effective . . and your idea is easy to compartmentalize without having to overthink about where everything goes and when you should put it there . . 

. . 

(1) I like the idea of playing video games like world of warcraft . . but they can have programming language environments embedded within them that allow you to . . work on your video game . . while playing inside of a video game . . sort of like how . . text . . editor . . environments are like video game environments . . where . . your . . text . . editor . . is like . . an . . Massively Multiplayer Online Role-Playing Game (MMORPG) where other players can join and leave your text editor enviornment and type the things they like and while also following content privacy and security rules like 'please follow this or that guidance rule' . . 

. . 

(1) I like the idea of playing a video game but it's like . . I also want to say something like . . 'yea, is there like a real strategy for me to know when and where to stop when I have like 'an interesting idea'' . . or should I always keep doing massive hardcore research and try to spam all the internet forums for advice and latency ladent addetives like 'hey, are you available to help me expertise the nonsense out of my problem dilemma?' . . 'please help me expertise the nonsense . . yea I don't know if I agree with those dilemmas being a real thing type of statements . . from my codebase . . yea, like why do this or that syntax statement have to say 'I occupy this or that much time in memory to compute'

. . 

(1) Do you like ball games? Or do you have to always spam the same different old addages of 'yea, png file . . yea . . javascript file . . yea . . mp4 file . . yea . . ' . . well . . those are questions about network connectivety problems which are not necessarily compilable in near-time efficiency when you have one massive data structure type that carries the energy-efficient tandents that let you sub-divide the program into small pieces and require them only at runtime when the network subdivides the task force into multiple time-sequence available megabytes or bandwidth efficient data protocols like http which fetches information at runtime without having to stream a large chunk of 'this or that single data structure type' from the server like a single html file . . from the server or something like that . . well . . ball games are fun anyway . . and so many little small tiny balls are really stuper effective . . and so any one single ball is possibly detrimental to our further progress . . right? well . . now we know that our game out to say something about . . 'yo, I would love to be split up into many little pieces' . . right ? 

. . 

(1) problem? I don't really know . . It's like . . I would like to say something . . about . . 'what the helllllllllllllllllllllllll

(2) solution? 'whattttt thheeeeeeeeeeeeeeeeeeeee

. . 

(1) problem

(2) solution, yea I think there's a good solution to this since I've been working on a file system architecture for computer programs but it's like -_- hmm . . is there like a 'limit' or a 'limiting point' where I can say . . 'yea, if You think about this long enough . . you'll get a nice conclusionary statement of what your architecture should look like . . or what it ought to . . or should resemble' . . but that's like . . 'hmm . . is there an algebraic expression for expressing this limiting function? or for expressing this summation ?' . . or something like that . . 

Hmm . . 

If I really had to involve algebraic expressions . . my first thoughtful variables could be something like . . 

(1) what the helllll time metric

(2) wow . . that's stupid . . time metric

(3) hey . . do you think 1 or 2 or 3 or 4 or 5 or a finite number of iterations of this algebraic expression will have the developer finish the game without arbitrary clause measures like 'yea, you need to keep programming to get more and more features out of this cartoon train program you're trying to build' . . which is like a semi-clause that's translatable to 'yea, please stop telling developers to believe in themselves and reach for their dreams . . just say something like . . yea . . here's an algebraic expression for htat . . you just need to calculate your thoughts for degree n . . where n is like a finite number like 1, 2, 3, 4 or 5 where you can finally say something like . . 'yea, that's a pretty simple game . . I like it . . I can't believe I made that game in less than 1 week with traditional old javascript' . . a game like world of warcraft is really satisfactory for that type of usecase . . it's like 'are you serious? . . I made world of freaking warcraft . . in less than 1 week?' 'I mean . . I'm sorry for swearing . . but wow . . that's like . . holy wow . . ' . . -_- . . well really the game might not really be like world of warcraft because of copy-right issues and things like 'asset management' relations like 'draw this cartoon character 50 times before you get the proportions right . . ' 'draw this animation 500 times before it starts to feel just right' . . 'learn about this or that scripting language for efficiency gains like network script' . . 

. . 

(1) problem

(2) solution

(3) yes, solution
  - (3.1) do some research <3
  - (3.2) ask a question about your research topic
  - (3.3) ask a question about the dilemma in question
  - (3.4) ask a question about the proportionality aya

. . 

(1) problem: yea, computer program repository yea? yea? Isn't it like a video game for structuring your codebase? yea, that would be really cool to say 'yo, check it out . . grandma made a robot to bake cookies' . . 'grandma made an mmorpg to play football with her grandkids' 'grandma made an mmorpg to play soccer with her friends'

(2) an easy codebase structuring language

(3) yes, solution

. .

Names for (3) . . Trial #1

-_- -_- naming things is so interesting ? . . like how do you decide to name name name name

name name name name name name name name name

Names for (3) . . Trial #2

research, research topic, research dilemma, proportionality aya, dialectric diagraph, 

. . 

(1) grandma can make computer programs

(2) an easy structure for grandma to learn and share with her peeeeeps

(3) yes, solution

(4) did I solve this problem before?
  - maybe it's in cache memory
  - maybe we need to re-calculate it like 'the mandelbrot set' where we wouldn't necessarily want to store all the images of the mandelbrot set in a database . . and instead re-calculate it at runtime right?

. . 

Names for (4) . . Trial #1

cache memory, incommencerability re-calculations, cache memory calculations

. . 

(1) problems are easy . . just say . . yea

(2) yea

(3) yes, solution

(4) did I solve this problem before?
  - maybe it's in cache memory
  - maybe we need to re-calculate it

(5) the enemy? the enemy?

Names for (5) . . Trial #1

the enemy, enemy, defend yourself, codebase affirmation criteria, no better tactic, offending you, offensive tactics, offensive enemy tactics, your enemies tactics, your enemy, 

. . 

(1) problems are easy . . yes . . yes . . yes

(2) yes

(3) yes, solution(s)

(4) did I solve this problem before? maybe I have, maybe I need to solve it again.

(5) the enemy? the enemy?

(6) too many stops from the enemy, please help

Names for (6) . . Trial #1

please help, help, the enemy is too strong please help, please help the enemy is too strong, 

. . 

(1) problems are easy . . yes . . yes . . yes

(2) is there a solution? maybe, right? 

(3) if you think there's a solution, then -_- why not give this paper algorithm a try and fold your own airplane?

(4) did I fly this airplane before? can I fly this plane again or do I need to rebuild the same one from scratch?

(5) the enemy? the enemy? are your enemies like airplanes?

(6) too many stop from the enemy, please help so I can navigate this new airplane trajectory terrain

(7) infamy

Names for (7) . . Trial #1

infamy, I am infamous, I am not popular, eximana, eximana trouble, for applying this solution we might get in trouble, for applying this solution there is an exile in ability to apply itself somewhere but we dont necessarily know if that is a real thing, exile, we are criminals for having solved this problem, we are criminals for having created this video game, 

. . 

. . . 

. . 


I wanted to create a computer program about how to create . . a computer . . program . . 

(1) Make a computer program about how to make a computer program

(2) I think a good computer program can start like 'a file system-based' computer program where certain syntax things can be taken into account like file system files and importing other files

(3) yes, a solution is to make a file system from javascript 
  - (3.1) why do you have to do research right now?
  - (3.2) why do you have to research a question?
  - (3.3) why do you have a dilemma on creating this?
    - (3.3.1) I like nice ordered files that are like . . yea . . I can pick up on the order pretty quickly and then move on from there and explore the file hierarchy with relative ease like an MMORPG video game :D
    - (3.3.2) I like pressing buttons in the keyboard that are like sweet action bar items like 'wow, cool spell. . that's like a double hazard spell when you can as for-bolt [fire-bolt, antonym] like that' 
    - (3.3.3) I like player versus player combat where it's like 'yo, those are some cool stats, but it's like those are some really cool stats, tell me more what your favorite number is'
    - (3.3.4) alright

. . 


Let's make a ping pong game

. . 

okay

. . 

Characters and Actions

. . 

There's N number of characters in our game
There are M number of actions in our game

. . 

[customer reports]: ping pong? ping pong? ping pong? ping pong? ping pong? ping pong? ping pong? ping pong? ping pong? ping pong? what is ping pong? it's like a game about warfare right? whoever wins is like, yea that was the winner of the war right? well that's an interesting first game to develop, but it's like . . yo . . who is your enemy as you're playing this game of ping pong? . . 

[customer reports]: ping pong is like . . there's a war but it's like, there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . and there's a war . . 

[customer reports]: there's a war . . but it's like . . there's a war . . and there's a war 

[customer reports]: mechanics

[developer reports]: hey everyone, we're making a ping pong game, is anyone interested in purchasing the game for 'real world money' [mr-krabs-voice-over-and-animated-gif]?

[customer reports]: hey everyone, I'm feeling a little sick of playing this ping pong game [sick-face-emoji] . . for some reason I can't get over the fact that I get headaches . . do you think you could fix that

[customer reports]: Hey, do you think you can stop sponsoring yourself as a hero of Azeroth and say instead that you're an enemy of the state? Your game made my daughter cry . . I really don't like your game company . . you all should be banned and made illegal for what you've done . . I'm sorry but now I'm even starting to cry . . you're the worst . . you're the worst

[customer reports]: Hey, do you think your garbage game is really made for the top three AE sports of this century? You should stop spamming your development log updates on the internet. This is my internet. This is my bandwidth. Please. Stop it. Stop it. For the sake of at least me and my one-membered ness community vote spama for you butt-ass who keeps posting these nasty updates. I'm so sorry but get a job. Get a job. Get a hobby. Stop playing around with these butt-ugly animations on your cartoon video game that's just like 'yo, you should have taken 4 or 5 video game tutorials before starting to apply your stanky breath all over the internet with your indie-5 negative 5 degrees syntax on your nasty nasty nasty nasty nasty nasty nasty nasty nasty nasty nasty for-each-nasty-i-write-type-two-more-nasty-nasty-nasty-nasty-for-i-is-1-to-infinity- 

for (let i = 0; i < Infinity; i++) {
  console.log('nasty nasty')
}

[developer reports]: 

. . 

hmm . . well . . when all else fails . . I think . . we can ask for help! :O

Hello god. Please help us.

[Name of God Here]:

Hello, I am a god of rain and thunder. I hear you would like to have some help on your video game. 

[Jon Ide]:

Yes, I would. Please and thank you.

[Name of God Here]:

Okay, well listen up. Your game needs spaces and pauses. You're ordering your aspects all wrong all the time and so you can't expect your game aspects to be really considerate to one another all right all the time and so it's like better for you to say that you are chasing like 'a fire horse creature' that is a characteristic creature from your new mythology training course that teaches you that 'a fire horse creature' is a creature that 'spams several different realities onto one map in a simultaneous ordered syntax like 'i don't agree with your reality but there is definitely a mapping from one reality to another without overlaying what that one other reality looks like''

And so it's like 'Hello, Your Name is Jonathan Ide' and 'Hello, Your Name is Jon Ide' are overlaying themselves rapidly and repeatedly without you even knowing it but it's like either aspect is incorrect and improper to represent in your space and time memory co-routines and it's instead rather more appropriate to say 'I need to know which order of Jonathan-Ide Ama to apply in which order and under which given syntax' but it's like, you're not gonna know that if you don't already have the asked question of 'who is listening to Jon-Ide Ama' and who is agreeing with the pronounciation of what is spelled and when it is spelled 'Your customer is a loose gun trigger who doesn't agree with everything you say right off the bat or right from the get-go of what your language and terminology says or reveals about your agencies or of your nature or of your ama-anna or of your anna-anna-n'

Well, that is a get-go solution to say you can't solve this problem with worldly-thinking because it's not a worldly problem.

You got it. Stick to your old book of tricks and trades to figure out which of these whens are better to fix this or that thing at this or that time. It's like a bi-directional diagraph that is specific to a particular usecase . A nice Need-To-Know Chart of 'you need to know this or that syntax language' to develop 'this or that syntax tactic' is very easy to consider and so 'Blender' and 'Unity' and 'Unreal Engine' and 'Godot' and 'PlayCanvas' and 'Three.js' and 'Pixi.js' and 'Melon.js' and 'Love2D' and other various game engine topics of interest are really effective at moving forward your canvas to paint a nice portly picture of 'here'

. . 



Space Order Diagraph: names, spaces and special syntax
Time Order Diagraph: times, times and special enemies
Need-To-Know Order Diagraph: know, know and special knowledge
Nothing Else #1 Diagraph: 
Nothing Else #2 Diagraph:

. . 

There is so much special syntax. 

!, @, #, $, %, ^, &, *, (, ), -, =
~, `, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0
_, +, [, ], {, }, \, |, ;, :, ', "
,, <, >, ., /, ?, 

. . 

a, b, c, d, e, f, g, h, i, j, k, l
m, n, o, p, q, r, s, t, u, v, w, x
y, z

. . 

There is a really big order about special syntax but it's like -_- wow . . you're really over-doing things aren't you?

aa, bb, cc, dd, ee, ff, gg, hh, ii, jj, kk
ll, mm, nn, oo, pp, qq, rr, ss, tt, uu, vv
ww, xx, yy, zz

. . 

it's a really big order . . it's like -_- developmental you could call it . . so it's like . . listing out all the various variations here is going to be deadly to the live stream entertainment saga :O right? well?

. . 

conversations like this . . are part of that 'so-called special syntax' order of space orders O_O it's like -_- existential crisis order so it's like O_O why am I reading orders of aya?

. . 

aa1, aa2, aa3, aa4, aa5, aa6, aa7, aa8, aa9
bb1, bb2, bb3, bb4, bb5, bb6, bb7, bb8, bb9
cc1, cc2, cc3, etc. etc. etc.

. . 

-a-, -b-, -c-, -d-, -e-, -f-, -g-, -h-, -i-
-j-, -k-, -l-, -m-, -n-, -o-, -p-, -q-, -r-
-s-, -t-, -u-, -v-, -w-, -x-, -y-, -z-

. . 


Time is such a special syntax character -_- but it's like . . well that's how animations in video games are so goooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo[order-times-500]ooooooooooooooooo[order-times-600]oooooooooooooooooooooo[order-time-ya-wow]ooooooooooooooooooood
xD . . thhhhhhhhhhhhhhhhhheeeeeeeeeeeeeeeeeeeeeeeyyyyyyyyyyy''rreeeee
ssssssooooooooooooooooooooooooooooooooo
gggggggggggggggggggooooooooooooooooooooooooooooooooooooooodddddddd

. . 

Animations are sooooo gooodddddddd . . it's like . . wooooooowww thank you soooo much for that realllllyyyyyyyyyyyyyyyyyyyy cooooooooooooooolllllllllllllllllllllllllllllllll animation :D thank you so much!

. . 

But it's like . . 

Orders of Powers of 2 . . - Or -
Orders of Exponentiations of 2 - Or -
Orders of Exponents of 2


2^0 = 1 second
2^1 = 2 seconds
2^2 = 4 seconds
2^3 = 8 seconds
2^4 = 16 seconds
2^5 = 32 seconds
2^6 = 64 seconds
2^7 = 128 seconds
. . 

Recall . . 

Orders of Ordinal Numbers - Or - 
Order of Natural Numbers

1 = 1 second
2 = 2 seconds
3 = 3 seconds
4 = 4 seconds
5 = 5 seconds
6 = 6 seconds
7 = 7 seconds
8 = 8 seconds
9 = 9 seconds
. . 

combinations of these orders are really good . . to get started with (as I could imagine) so it's like . . yea . . you can use summation principles like . . if you're trying to get a X second smirk where X is like a whole number . . like 55 or something like that . . 

There's a formulate for that like you can just sum the powers of the orders you're using with their own unique coefficients

c0 = coefficient number 0
c1 = coefficient number 1
c2 = coefficient number 2
c3 = coefficient number 3
. . . 
cN = coefficient number N

Your Number = c0 * 2^0 + c1 * 2^1 + c2 * 2^2 + c3 * 2^3 + . . . + . . . cN * 2^N

Summation Of (cM * 2^M) from M = 0 to M = Infinity

. . 

E_from_0_to_infinity(cM * 2^M)

. . 

well uh . . 

something like that so you need to decide what your coefficients are . . and so . . for example if you number is . . 55 . . 


55 = c0 * 2^0 + c1 * 2^1 + c2 * 2^2 + c3 * 2^3 + . . . + . . . cN * 2^N

c0 = ?? // this is on the order of 1
c1 = ?? // this is on the order of 2
c2 = ?? // this is on the order of 4
c3 = ?? // this is on the order of 8
c4 = ?? // this is on the order of 16
c5 = ?? // this is on the order of 32
c6 = ?? // this is on the order of 64
c7 = ?? // this is on the order of 128
. . 

well we have our list of coefficients (sometimes called 'constants' since a lot of the time we use them as 'single numbers and not necessarily mathematical expressions like algebraic formulae like f(n) = n^2 or f(m) = sin(m) or something like that')

?? means that we don't know what the coefficient (or the constant) for that order should be . . and so we'll say something like . . ??? /shrug-emoji . . we just want to have the summation be 55 right? -_- well there are courses about this on the internet and you will possibly be instructed better than this but here is a short hand . . tutorial for that sort of information . . I'm sorry -_- if you are already -_- involved in that science fiction fan fiction movie film

(1) don't look at orders that are larger than our number, in this case, the number 55 . . and so orders like 64 . . 128 . . and so on . . will all have a coefficient of 0 . . 
- c6 = 0
- c7 = 0
- c8 = 0
- etc. etc. etc.
- cN = 0 for N > 8

(2) our numbers for the various orders need to sum (in combination with their coefficients) to the number 55 . . 
- (2.1) look at the largest order . . and decide that it will lead the cause to determine a nice grouping for us . . to determine a nice order . . 
- (2.2) okay . . c6 (Order 6) is our current largest number that is less than 55 . . (I think in mathematics circles this is called the 'infimum' since it is the 'greatest lower bound of the set' and which our set happens to have the name 'orders of powers of 2') so . . order 6 is the greatest lower bound of 55 . . and so . . for example . . 32 is the greatest number that is lower bounding 55 with the set of other lower bounds like 16, 8, 4, 2 and 1 . . . but it's like that set is not uh . . including a number that is greater than 32 and so 32 is the greatest number of this list of numbers and so it's considered to be a special ordinal number of supreme mathematical importance called the so called 'infimum' in this case of mathematical hierarchical organization which is like 'yea, that's pretty special indeed right?' since you could have other so called orders-of-supreme-ordered-specialness like 'yo, what happened to prime number supremacy?' 'can you have ordered supreme prime coefficients with respect to the ordered number of interest?' and so for example 'do you think you can narrate the story of 'factors of 55' that are co-prime with 55?' and say that is quite a nice order to look at? . . well co-prime factors are always like 'yo, we don't agree that you're supposed to factor with non-1 digits yo'

// Tangent conversation for introduction research topics of interest for students who are happening to exist on the order of the spectrum of that interesting topic.

[security guard]: [arms-crossed-emoji] what makes you numbers co-prime? Are your prime factors non-overlapping? Well if they are then you look pretty good to me . . 

co-prime-factor-list(1, 55) = [], [5, 11] // co-prime?: yes!
co-prime-factor-list(2, 55) = [2], [5, 11] // co-prime?: yes!
co-prime-factor-list(4, 55) = [2, 2], [5, 11] // co-prime?: yes!
co-prime-factor-list(8, 55) = [2, 2, 2], [5, 11] // co-prime?: yes!
co-prime-factor-list(16, 55) =  [2, 2, 2, 2], [5, 11] // co-prime?: yes!
co-prime-factor-list(32, 55) = [2, 2, 2, 2, 2], [5, 11] // co-prime?: yes!

[security guard]:


32, 16, 8, 4, 2, 1

(1) 1 * 32 = 32 // infimum of remainder set

55 - 32 = 23 // remainder set: [16, 8, 4, 2, 1]

(2) 1 * 16 = 16 // infimum of remainder set

23 - 16 = 7 // remainder set: [4, 2, 1]

(3) 1 * 4 = 4 // infimum of remainder set

7 - 4 = 3 // remainder set: [2, 1]

(4) 1 * 2 = 2 // infimum of remainder set

3 - 2 = 1 // remainder set: [1]

(5) 1 * 1 = 1 // infimum of remainder set

1 - 1 = 0

. . 

- (2.3) Which powers of 2 did we multiply by 1?
  - 1 * 32 = 32
  - 1 * 16 = 16
  - 1 * 4 = 4
  - 1 * 2 = 2
  - 1 * 1 = 1
- (2.4) Sum
  - 32 + 16 + 4 + 2 + 1 ~proportional-to~
  - (32 + 16) + (4 + 2) + (1) ~propotional-to~
  - (48) + (6) + (1) ~proportional-to~
  - (48 + 6) + (1) ~proportional-to~
  - (54) + (1) ~proportional-to~
  - (54 + 1) ~proportional-to~
  - (55)

Proportionality laws are like xD

Is that reallllllllllllll
or is that real
is that
reall?
is that r.e.a.l.e?
or is that rel?eal?
or is that r-e-a-al-al
or is that rrrrrrrrrrrr-E-E-E-a-A-L
or is that xD-R-ExD-R-E-A-A-ML

. . 

Proportional ideas are like :O wow . . I didn't knecessarily know you could do that with words . . wow . . 

do you think ping pong is like that? do you think we can type stories and be like 'yo, get those orders of magnitude to sum up to my video game idea yo'

'sum it up' 'sum it up' it's like '1 2 3' 'sum it up'

'get those proportionalities that are large before you get those small proportionalities so it's like 'yo, are we getting the bang for our buck?''

'are we paying for what we came for?'

'are we paying for what we came for?'

'get those large proportionalities in here'

'what's the hold up?'

'who knows what a ping pong table is?'

'who knows what a ping pong table is?'

'who knows what a ping pong table is?'

'who knows what a ping pong table is?'

'we need those proportionality legs'

'we need those proportionality legs'

'we need those legs'

'ping pong tables'

alright that's as far as I got without going crazy

. . 

yea well it's like

(1) I don't know a lot
(2) ping pong is like -_-
(3) dice games

. . 

dice games are fun [evil-grin-face-emoji]

. . 

random number generation is fun [evil-grin-face-emoji]

. . 

yea well a random database hierarchy is interesting

- random-number-1.js
- random-number-2.js
- random-number-3.js

. . 

cool game

and so in our random-number-1.js file we can say something like

/*
file-name: random-number-1.js
*/
/*
hi, we would like to order 1 random number 1 .js file please
*/
/*
[text-editor]:??
*/
/*
oh I'm sorry, aren't you a computer a.i. system that can understand what I'm saying? ahh yes. I would like to order 1 nice random number 1 please. a good javascript version of random number 1 please. thank you :)
*/
/*
[text-editor]: ahh yes, that's like yea, I think I can get that up for you . . Math.random() . . is that good?
*/
/*
I love that stuff. It's a great time
*/
/*
[text-editor]: will that be all for you today?
*/
/*
no. In all seriousness I'm having a tough time writing a game in javascript it's like. Can't you do that for me please boss?
*/
/*
[text-editor]: sure why not. What type of game are you looking at?
*/
/*
Order me with nice orders like 'yea, cool'
*/
/*
[text-editor]: ahh, text-editor orders are like 'yea, well, i need to think about that'. Alright let me see. 'yea, cool is like' 'hmm, well'

Let's take a closer look at 'yea, cool'

it's like 'you're not gonna really get 'yea, cool'' you'll get a half baked version of it like 'yea, i don't have that feature in my text editor yet so it's like yea you'll have to wait for some gauuuuudddyyy developer to say 'yea, alright let's adddddddd that feature aaaaaddddddd another feature adddddddddddddddddddddddd' . . 

and so it's gonna be like . . yea ''aadddddddddddddd

okay but your adding isn't as good as their adding so it's like you're gonna like it if i spell things out for you like 'what are the additives?'

(1) add a nice way to edit things for later
(2) add a nice way to say thank you to your local developer
(3) add a nice way to warn people if you are mistreating the software you're using for otherwordly purposes

Those 3 orders will get you in trouble but it's like . . you really only want to focus on the big order first . . so order 1 is your best order . . 

(1) okay so your game is basically a text-editor but you don't need to memorize that formula

Your game is to (1) order a list of things in a chart like you want to say 'yea, i like that' 'yea, i like that' 'yea, i like that' 'yea, i like that'

yea well . . a text editor is like 'yea, i like that' for a lot of people that use text editors and if you really think about it . . it's like all the network traffic on the internet is text-editor approved . . look at all that software being text-edited on a keyboard from someone's computer right? I'm not sure but that's like an order of proportionality to think about how software farms work . . it's like 'yo, build a nice text editor so we can all have a good time'

okay well that's a long explanaition, do you need some more time to think about things?
*/
/*
O_O wow that's really amazing. Oh by the way. I just discovered I_I as a nice character emoji. You know that was completely by accident. Do you think that's like a really cool feature to add in games? ACCIDENTS?
*/
/*
[text-editor]: I'm sorry sir I can't answer that. Do you have any other questions for today?
*/
/*
How do you build a text editor?
*/
/*
[text-editor]: Okay, very good question

You need to say 'you have text'
You need to say 'you have numbers to keep track of where the text is on the page of paper formula syntax machine'
You need to say that there is a delay between when you type and when words appear on the page
You need to say that your delay is dialogizable and so what you do today in realtime can be registered in a future time like tomorrow or the next day or the next day like a computer dialogue where your friend needs time to respond . . but that is really a complicated formula and so really what you want to say is that you have a computer algorithm for 'please, look at this algorithm in 2 hours from now'

. . 

Okay your best bet is to say there are 3 folders

(1) you keep track of records of characters like 'how are you syntax characters like (1.1) hey welcome to the text editor (1.2) hey are you new to the text editor environment? (1.3) hey do you want some help with text editing? (1.4) hey do you like getting a fix of hot tea or a comfortable beverage while waiting for your text editor to communicate the right error messages?'

(2) if your text editor is really joyful and grand then it should start spying on all the characters that are being typed right away at runtime immediately without delay and let all the players of environment know when that single character is updated

(3) if your text editor is secure, you will really need the same notification feature from (2) [item 2 from this list] to let your healthy development team know that it cannot access this or that development feature when attempting to access or register events for the name of that single itty bitty character in the whole of the text editor environment (3.1) ordered combinations of text character encodings are also able to be represented by text editors to quickly sort through information gatherings like 'index search bluebooks like search engine crawling that spans and lets you inspect your graph of indexed items'

*/























































