
# Information-Energy Source Code Compiler
## Day Journal Entry - September 15, 2020


[Written @ 19:20]

Information as energy . . supposes that perhaps . . there is a way . . to transform . . uhm . . any information . . for example . . a photograph . . of a person . . or a photograph . . of . . a cat . . or a photograph . . of a car . . or a building . . or a house . . and you can transform . . any of these photographs . . into . . various other forms of energy . . such as . . a wind . . energy . . pattern . . or a light energy pattern . . or an acoustic energy pattern . . 



[Written @ 18:40]

When I think about . . information-energy formats . . like . . wind . . and solar . . and geothermal . . and electrical . . energy formats . . 

I think about . . source code files . . like . . a file of text . . or . . a file . . with . . javascript code . . or . . uhm . . maybe even a music file . . or a video file . . and so . . file extensions . . are a common . . notion . . or topic . . that one could possibly relate . . to information . . energy formats . . 

hello-world.wind-energy
hello-world.chemical-energy
hello-world.solar-energy
hello-world.light-energy
hello-world.geothermal-energy
hello-world.nuclear-energy
hello-world.elastic-energy
hello-world.sound-energy
hello-world.electrical-energy 

Uhm . . 

Interestingly . . enough . . a lot of . . compliances . . for common house hold objects . . are using . . electrical . . energy . . and so . . a microwave . . and a refrigerator . . are . . uhm . . using . . Many Watts or KiloWatts of energy . . from the wall . . power system . . to power the house . . something like that . .

And so for example . . if you have a solar panel . . which . . could be considered . . an information . . source code compiler . . that transforms . . light information . . into . . electrical . . information . . or . . electrical energy information . . electrical potential energy . . information . . uhm . . electrical potential information . . uhm . . there's a potential . . 





[Written @ 18:35]


Information-Energy Source Code Compiler . . is the idea that . . you could possibly take information . . in the form of one . . so-called . . energy format . . or energy factor . . and transform that . . information . . into another . . type of . . information . . energy . . or . . information-energy . . format . . 

And so for example . . one can take . . Wind Energy . . in the form of . . uhm . . waves of sound . . of a certain quality . . in the Earth's atmosphere . . or something like that . . however . . one defines wind . . I suppose . . I'm not sure at this time . . and in any case . . you can build a source code compiler . . like a wind mill . . uhm . . well . . uhm . . . . uhm . . wind . . uhm . . can be transformed . . by the wind mill . . and . . transformed . . into . . electrical output . . and . . the electrical output . . can be . . and information energy format . . that . . a microwave . . or . . a television set . . can use . . in their daily operations . . or something like this . . Uhm . . that's the theory . . As far as I've understood this . . and of course . . in my day journal here . . I am giving myself space to play with ideas . . and these aren't necessarily . . the facts . . of how things work . . but . . really observations . . and comments . . in terms of how I've understood things so far . . 

Something . . 

[Written @ 18:03]

Mechanical Energy

Electrical Energy

Thermal Energy

Radiant Energy

Sound Energy

Chemical Energy

Nuclear Energy

Gravitational Energy

Elastic Energy


![Physics Types of Energy](./Photographs/physics-types-of-energy.png)


[Written @ 17:57]

Information-Energy is an idea that relates to the topic that information and energy are interchangeable . 

That is to say . . You can transform information . . into energy . . and . . you can transform . . energy . . into information . . Kristof de Spiegeleer . . talks about . . "" which is an idea that relates to the idea of . . information being equivalent to energy . . 

I haven't done a lot of research . . into the topic . . in relation to . . Kristof de Spiegeleer's work . . and yet . . I think I was inspired to think about this idea . . in relation to watching the video at [1 @ 15:13 - 15:15] . . 

"I first called it I.T. [Information Technology] as energy but no one understood it."
[1 @ 15:13 - 15:15]

"Well . . we used to call it . . I.T. [Information Technology] as energy . . But okay . . we thought it was maybe . . a little bit too technical . . But it's really like . . I.T. like energy . . you go to the power plug . . and you just use it . . and . . thats was our vision . . Can we make I.T. . . as easy as going to power plug . . and just put . . and device in . . and you just have the power . . and you don't have to think about all these complicated things . . that was the v- . . the vision . . yea . . "
[2 @ 16:52 - 17:15]



[1]
Decentralizing the internet with ThreeFold
By John Koestier
https://www.youtube.com/watch?v=yBjRfmb75Uo


[2]
Building a Decentralized Global Web with Kristof de Spiegeleer of Three Fold
By Girl Gone Crypto
https://www.youtube.com/watch?v=qAhCBJV2sj0

