

# Gratitude Journal Entry - October 25, 2020

[Written @ 20:02]


### What I'm Grateful For:
- I am grateful for my mother
- I am grateful for having a house to sleep in and take showers and eat food
  - I am grateful for my mother who provides these things [*2]
- I am grateful for my father
- I am grateful for my siblings
- I am grateful to live on a planet with friendly people
- I am grateful for the ability to work on my own projects at this time [*1]
- I am grateful for the ability to dream
- I am grateful for the ability to eat
- I am grateful for the ability to see
- I am grateful for the ability to hear
- I am grateful for the ability to touch
- I am thankful for the ability to smell
- I am thankful for the ability to drink water
  - I am thankful for having water in the house to drink
  - I am thankful for having water
  - I am thankful for the ability to experience water
  - I enjoy water
- I am grateful for the ability to visualize [*3]
- I am grateful for the ability to imagine [*3]
- I am grateful for the ability to think [*3]



### What Are My Accomplishments for Today:
- I have accomplished breathing today
  - breathing air is an enjoyable task . breathing air while consciously focusing on the breath can also be an enjoying activity for me . 
  - 
- I smiled today
  - smiling gives me a good feeling . I have forced myself to smile before . But smiling for the sake of smiling itself is something that I feel makes me smile more naturally and the smile is more automatic . . and I don't feel like I have to force the smile to come to my face . . [*4]
- 
- I have accomplished taking time to think about my life
- I have accomplished imaging other versions of planetary life
  - imagining is something that I enjoy to do and sometimes I get nervous but imagining allows me to be involved in alternate histories of life where I don't need to be nervous
- I have practice gratefulness meditation
  - being grateful feels good [*5]



### Things That I could Improve on:
- I'm not going to focus on this right now



[*1]
Even though I feel that working on my own projects at this time isn't really that practice because maybe I should get a job and help my mom pay for the rent at home . . or do something . . But at the same time . . I'm not really excited to work somewhere . . to be honest I'm really not interested in interacting with people most of the time . . Ideally . . a society could exist where I could sleep in all day and dream and write in my dream journal . . when I wake up or something like that . . if if I need something to eat . . I can do that . . or maybe if I need . . uh . . to do something outside like running or walking I don't need to worry about health safety like the dangers of being in a war zone or in a neighborhood that is where people perform violent behaviors or violent practices . . 

Well . . my projects uh . . Ecoin being one of my main projects at this time . . and the other projects relating to creating artificial intelligence algorithms . . and taking notes on . . ways to create new technologies like sound based technologies that make transportation of resources more efficient . . or make the creation of resources more efficient or something like that . . uh . . I've taken a lot of notes from research that I've done in this area of acoustic sciences as well as robotics and artificial intelligence and dream art science . . uh . . well there are uh . . uh . . well . . these are my hobbies . . 

uh . . on the one hand I feel like my projects really are possibly considerable as a waste of time . . and uh . . especially sometimes I don't feel so great in working on the projects on my own and yet at the same time the freedom of being byself allows me to not have arbitrary constraints like safety concerns or maybe even concerns of what amount of time to spend on the topic or uh . . I don't know those are some arbitrary things that I just made up right now uh . . well . . uh . . especially safety . . safety uh . . maybe I take it for granted a lot to be in a room with teammates working on a project together . . well at least in being alone . then I am responsible for my own safety and I am allowed to interpret the safety of myself in my own terms and those terms can change whether it's uh . . sleeping or dreaming or dream journaling uh . . maybe for someone else it's not really that great of a practice to sleep in all day . . uh . . well I feel more it's like a uh . . ability like engineering where you can uh . . do a lot of different things and create new thought patterns that are able to uh well . . to be honest I'm really young at this ability to use dream patterns and to study the nature of dreams and so I won't say so much right now . . uh . . well for example . . I discovered a new sport that people could play . . which involves . . having a stick with a basket at the end of the stick . . and you can place a ball inside of the basket . . and then swing the stick . . and someone else with a stick that has a basket at the end of the stick is responsible for catching the uh . . ball . . and you run around with the bal . . well uh . . hmm . . I guess it's like a hockey stick with a basket and you catch the hockey puck or something like that uh . . and you catch it like you would catch a .  uh . . american football . . or uh . . a baseball or something like that . . uh . . well . . this sport may exist already . . but to me at the time today that I had the dream about it . . it was really new . . and I've discovered other things from my dreams that seem new to me . . and I take notes on them . . uh . . well . . the idea is that these things are able to influence uh . . my life and so I can think of life in new terms and so the outcome of how my life evolves can be inspired from alternative versions of human history that don't necessarily come from what is happening here on the Earth that I'm familiar with . . 

uh . . well . . although I'm happy to work on projects on my own . . I also do get anxious sometimes . . uh . . well . . uh . . it would be ideal to have systems in place that are something like house givers or care takers . . uh . . in the way that I imagine that going for the people of Earth today I imagine a lot of computers or robots . . uh . . automated equipment that is able to make the work of having more resources available to people more of something that can be done by uh . . 0 people or uh . . a handful or people as long as there is an initial creation of those robots or computers that can help in performing the tasks . . 1 person with the proper equipment and the motivation or inspiration can plant a farm for 1000s of people which is quite cool . . especially if they have technology that helps them plant food for even more people . . and so . . like farmers today . . I hear the story that . . a handful of farmers can take care of millions of people . . uh . . or maybe hundreds of thousands of people because of the tools they have available to them today . . which is quite impressive . . and so that can imply or suggest that uh . . well . . uh . . at some time you can have maybe 0 people . . and a handful or robots . . take care of the agriculture . . or a handful of computers take care of the agriculture . . environmental cautious . . environment sustaining computers or something ilke that . . computers that take into account all sorts of parameters to eekp the planet safe for all of the people and all of the organisms to have a life they can enjoy . . 


[*2]
If I didn't have my mother right now . . I feel like uh . . well . . one option is to live with my father . . uh . . well . . my father uh . . uh . . I'm not that excited to live with my father at this moment . . the question relates to wanting to work again . . my father wants to push me to work . . uh . . well . I understand his point of view and I'm very grateful to have a father who cares for me like this to want to encourage me to work . . uh . . well . . it's fair and I don't have any complaints . . Uh . . well . . uh . . I'm not really sure . . I suppose it's always a possibility . . uh . . I'm not really opposed to it . . 

I think my whole attitude around this topic belongs to the realm of ideas where I'd like to not bother people that I know in my life and so even for friends that I have . . it's the idea that I would be a burden to sustain . . in a way for example uh . . well uh . . hmm . . isn't it again going to come to the topic of working . . ? . . I am not uh . . well . . I've applied to some jobs already in the past few months and I have received so few responses and I'm not very proud of the process of being hired which exists today . . how come it isn't just accepted by default and if I need training to learn some things you can assume that that ability is there for me to have . . it's not so difficult to learn something new especially in a family that loves and supports you in doing so . . is the family culture of love and support the idea for a company or is it something more like we'd like to not have an idiot or someone who will want to leave the family because they are too ego minded or not wanting to contribute in the way that we would want them to contribute . . 

well also at the same time I'm not very excited with the work that people are doing at some companies the processes could be more synchronous with the ideas of various other places . . more cohersion and alignment towards the topics of sharing and communicating and not really the topics of profit or trying to earn a living -___-- uh really this topic is complicated and I've flattened it out or pastened it here in a way that maybe makes things too simple to consider and a life experience in the latrine of the current society is really more proper to consider the idea for oneself in what one prefers versus what is being offered by the pale statement idea paycheck slums of the society paste that exists today throughout the world ecoenvironment [delete this paragraph since it is too uh . . inconsiderate of certain parameters that one could wish to take into account before writing a message like this or something like that] [this message is not a part of history . it is written and is meant to be ignored for the sake of keeping peace and not inciting too much thinking in negative thoughts or something like that]


[*3]
I was listening to a gratitude meditation one day . . and the words . . "I am thankful for the ability to visualize" . . "I am thankful for the ability to think" . . These words were spoken . . [1] . . something like these words were spoken . . "I am thankful for the ability to visualize" . . "I am thankful for the ability to visualize" . . this . . quote . . really . . uh . . I was in a half-waking . . half-dreaming state . . where I was asleep uh . . well . . I think I was dreaming mostly . . and the sound . . of this meditation at [1] . . came through . . and I got the feeling in my body . . 

wow . . when it comes to artificial intelligence . . computers that can think . . or computers that can visualize . . one day . . uh . . well . . the way it felt in my dream . . it was as though . . a computer was saying this in my ears . . "I am thankful for the ability to think" . . and so . . imagine that a robot said to you that they are thankful for the ability to think . . or a computer said that . . it is quite an endearing statement in my mind . . 

"I am thankful for the ability to visualize" . . a computer saying something like this . . well . . uh . . a human saying something like this uh . . well . . maybe for a human . . a human can take these things for granted . . the ability of visualizing . . the ability of . . thinking . . and yet . . uh . . computers today . . uh . . artificial intelligence algorithms as far as i'm aware in terms of the public released versions of computer algorithms that can think and visualize uh . . well . . they are not as advanced as humans possibly in what I imagine . . or they are not as advanced uh . . well . . uh . . in the day of artificial intelligence . . when computers can reason and think . . uh . . well . do you think they will also take it granted . . or do you think they will also be grateful for these abilities . . 

thinking . . visualizing . . touching . . feeling . . 

these are really amazing abilities . . and so . . uh . . well . . uh . . right . . that's really incredible that a computer can be grateful . . uh . . it is an endearing thought when I think about it . . it is nice to be thankful and give thanks for something that you enjoy or something that you like . . I don't know . . to me it gives me good feelings to show thanks . . because . . well . . without the thinking and visualizing . . uh . . well . . I'm not sure . . uh . . maybe there are other things . . uh . . I don't know . . the topic of . . "thank you" is also an interesting topic . . I've thought about the topic of how long humans have given thanks . . and maybe if this idea of thankfulness for what you have is really a long lived characteristic across human history across wars and changes of the species . . or at least . . how come thankfulness is a thing that needs to be communicated . . and why did our society start to say thanks . . why shouldn't the default be that we are already thankful and we are already kind to one another and no one needs to say thank you any more . . it can be implicit in the way that we live our lives or in the way that we walk around and eat food and drink water . . and so why shouldn't the language of "thank you" really go extinct ? . . why should the language of "thank you" survive ? does it need to survive ? . . in a peaceful society . . is this type of language used ? . . or is it . . . . does it look different ? . . are the translations the same thing ? . . how would you translate . . ? . . if no one is saying "thank you" ? xD . . this is awkward uh . . I'm not really uh expressing this in a way that is really uh able to uh for example there are too many scenarios to consider that my words are trying to start with a presumption or assumption that is really the problem in translating the experience of such a culture that goes without "thank you" . I imagine that it is really in the experience itself and those things of the society aren't necessarily translatable especially in terms of pscyhological observations of thought for example . . the customs are based on psychological understandings that aren't necessarily native to the psychology of a human being like myself who isn't trained to perceive the information of the life in their world as they are experienced or trained or something like that . . 



[*4]
I played around with the thought . . of smiling . . it was really not really something that I felt that I knew what I was playing around with . . for example . . my eyes were closed earlier today . . and I was laying on the couch where I normally sleep . . and . . well . . in my day dreaming . . in my day time sleeping habit . . in this journey . . in this . . day . . sleeping . . uh . . activity . . that I like to participate in . . uh . . well . . my uh . . my thoughts . . uh . . it can begin . . with something like uh . . I'm thinking about this or that topic . . and uh . . well . . I felt the topic of smiling came up to me . . uh . . and uh . . for example . . I would like to smile more but forcing myself to smile feels more uh . . not really the smiling that I feel that smiling should maybe feel like or something like that . . In smiling maybe I should be more happy and actually feel the effect in my heart . . or in my body or I should be . . u h. . well . . the feeling in the mind of a rush or energy or a rush of joy or maybe a rush isn't the best way to uh . . well . . it's possibly an individual property and I'm not sure . . for myself . . I've felt uh . . well . . uh . . the idea was unlocked somewhere in my mind after a while of laying there on my couch . . laying there and having thoughts that aren't really based in words all the time for exmaple the thoughts are feelings and uh . . shapes and effects or something like that . . shapes of thoughts are combined in a way that feels like lightning bolts combining or something it can be a shocking effect where . . I realize something . . uh . . something like that . . uh . . the realization is based on many shapes of movements of thought patterns in reasoning capacities or something like this . . the reasoning is automatic and so it doesn't necessarily feel like coming to an idea uh . . uh . . well . . uh . . it feels more like the reasoning language of intuitive knowing or intuitive feeling or feeling . . uh . . a feeling language of knowing things or something like that . . maybe that's a way to talk about this uh topic uh . . in any case . . it uh . . after uh . . maybe 30 minutes or an hour . . I felt the idea that smiling for the sake of smiling and smiling just to smile is a useful practice and that it is easier to hold the smile and the smile can be more automatic or something like that . . 

This is advice that I've heard before on YouTube . . uh . . well . . uh . . the name of the YouTuber isn't coming to mind right now uh . . [2]



[*5]
Sometimes I don't feel very productive or that I'm not really contributing in the way that I should contribute . . uh . . for example . . for the past few days . . for the Ecoin live stream . . I haven't really . . uh . . programmed anything . . mostly . . I've been sleeping . . or uh . . well . . I'll do something random which I didn't expect myself to do on the live stream . . uh . . some of the things are unplanned . . for example . . writing this series of paragraphs is really unplanned . . I expected . . I would come on the live stream and start programming the ecoin source code and now here I am writing about the things that I'm grateful for . . 

Well . . I feel that I've noticed this idea that . . the . . being grateful has a sort of magical property it seems like there are a lot of things happening behind the scenes of my mind and my attitude or my feelings that I'm not really aware of . . or I'm not really always uh consciously knowing why things are working the way they are . . for example  . . why . . am I spending so much time doing non-Ecoin programming related things . . well . . uh . . uh . . (1) I feel nervous to get on the live stream a lot of times . . (2) I'm uh . . always learning to improve uh . . but that . . uh . . I don't really know what that means sometimes . . uh . . well . . uh . . [*6]


[*6]
being nervous about source code is something I feel sometimes for example . . the feelings I have about the relationship of the data structures or the relationship of the files and how the files and directories are related to one another . . the naming conventions for the files . . uh . . or uh . . well . . there are things that can come up which I didn't even know about and all of a sudden there are new parameters for me to take into account when thinking about computer programs . . and uh . . well this suddenness . . uh . . maybe makes me feel like . . uh . . well . . "I need to take a step back to re-approach things with this new information in mind" . . "I need to sleep . . I need to relax . . I need to take time out to breath . . I need to . . take time out to realize that everything is going to be okay . . I'm going to be fine . . " . . or . . uh . . well . . that really is possibly a part of what I feel uh . . well . . uh . . hmm . . 

In the past maybe I've associated my behavior of sleeping . . with mental laziness . . or my behavior of relaxing with . . just not thinking enough about the topic . . and not caring enough about being productive . . or something like that . . 

Well . . uh . . being grateful . . taking time to realize how much I already have . . and all the things that are already here and the things that are already so wonderful that I can appreciate . . 

Taking time to sleep and relax . . being thankful for . . the trees . . being thankful for the ability to rest . . being thankful for the ability . . well . . the ability to learn for example . . I'm not always comfortable with something . . I'm not always comfortable with doing something . . especially for example . . when I realize . . oh . . wow . . maybe I need to take this or that into account now . . oh . . wow . . hmm . . well . . if this or that were taken into account and also this or that hypothetical structure that could exist in the future were taken into account maybe this uh well could be considered blah blah blah something like that . . 

Uh . . well . . uh . . being grateful really uh . . has magical properties for me . . uh . . for the very least for me . . uh . . I don't know but I think other people also get the same feeling that being thankful has some interesting properties or effects . . there's the magical intuition that comes into play which are variables or things relating to how reality works maybe or the human biological system that aren't really understood without the understanding of thankfulness or something like that . . I'm not sure . . well

right . . so uh . . sometimes when I am not feeling like I'm being the most productive . . uh . . well . . uh . . really . . I enjoy taking the time away . . from uh . . things that I'm normally doing . . I enjoy . . taking the time away . . and . . uh . . well . . being thankful always seems to have an effect where my work is even further improved . . or maybe I am able to be more productive by taking time away and appreciating the life that I have already and appreciating the things that are already here for me . . 

I'm not sure . . even uh . . well . . yea . . gratitude . . uh . . which improves my performance . .

Well uh the performance improvement isn't always in the way that I expect . . for example . . I'll have thoughts about something that I didn't expect to have thoughts about right away . . while performing gratitude meditation . . And so the linear . . step by step approach of trying to improve my life or trying to improve my progression in a work that I'm doing . . for example . . uh . . well . . it's uh . . it's uh . . well thinking about many topics uh . . well . . being grateful seems to maybe . . focus the topics down to a way that is really uh . . magically reasonable . . or a magical suggestion which provides new insights on maybe how I should approach this or that problem which I didn't necessarily think about knowing how to approach when I was trying to think . . about the problem . .

I'm sorry


[1]
Morning Gratitude Affirmations- Listen For 21 Days! (432Hz)
By YouAreCreators
https://www.youtube.com/watch?v=JEDGFaXYIX8&ab_channel=YouAreCreators


[2]
YouTube Video
[Possibly Find Later]
[Smiling for the sake of smiling is important]




