

# Gratitude Journal Entry - November 5, 2020


[Written @ 9:18 - 9:22]


Things I am grateful for:
Things I am thankful for:

- I am thankful to have air to breath
- I am thankful to have water to drink
- I am thankful to have food to eat
- I am thankful to have a brain to think
- I am thankful to have a body to move
- I am thankful to have arms to type
- I am thankful to have legs to walk
- I am thankful to have a mind to think
- I am thankful to have the ability to imagine
- I am thankful to have the ability to breath
- I am thankful to have the ability to sleep
- I am thankful to have the ability to dream

. . . 

- I am thankful for my mom


