

# Ambient Video With Music


. . 


### Music Type

-- F

Fireplace Sound, Rain Sound [2.0]

-- S

Soft Peaceful Relaxing Music [1.0]


. . 

### Video Type


-- A

Ancient Library Room [2.0]

-- Y

Young Boy Holding A Glowing Moon [1.0]

. . 


. . 



[1.0]
Beautiful Relaxing Sleep Music | Fall Asleep in 6 Minutes (12 HOURS)
By Dream Musicas
https://www.youtube.com/watch?v=_BUmI4c2G6g

[2.0]
Ancient Library Room - Relaxing Thunder & Rain Sounds, Crackling Fireplace for Sleeping for Study
By New Bliss
https://www.youtube.com/watch?v=IvJQTWGP5Fg&ab_channel=NewBliss


