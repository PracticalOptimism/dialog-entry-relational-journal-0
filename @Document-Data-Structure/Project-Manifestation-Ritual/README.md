

Emotional Vibrational Set Point
Natural Vibrational Set Point


Charging The Water

- (1) Repeat The Affirmations 10 Times or More
- (2) Imagine Yourself In That Life, Living That Life, What It Would Feel Like
- (3) Charge Until Your Natural Vibrational Set Point Changes; You Feel Like You've Convinced Yourself That This Is Your New Natural State of Feeling




. . . 

Water does hold the vibration of words . . 

Water does hold whatever vibration is . . attached . . to it . . 

The moon pushes and pulls the . . tides . . of the ocean . . 

Animals . . act weird . . during the full moon . . 

People . . act weird . . during . . the . . full moon . . 

Sacred State . . Internal Geometry . . 

Saying Affirmations To Water . . 

Directly For Manifestation . . 

New Moon In December to do my manifestation . . 

All the things that I wanted to manifest in my life . . 

0 dollars to my name . . 

Literally 0 dollars . . 

I honestly wrote down so much . . 

As much affirmations as you want . . 

As little affirmations as you want . . 

3 pages . . written down . . of . . I am . . Statements . . 

Present Moment . . 

The Key to Manifestation is Writing It Down In The Present Moment . . 

I want to live my . . dream life . . in the future . . 

It will be in the future . . It's a time . . away from me . . it's not right now . . 

I want to . . Uhm . . Meet the love of my life in a year from now . . 

It's always going to be . . in a year from now . . 

Write it in present tense . . 

"
I am so thankful . . to be working from home . . 

And making this amount of money . . every month . . 
"

Avoid words with a negative connotation . . 

The movie . . the Secret . . 

Don't write things like . . "I'm so happy to be out of debt" . .

Debt subconsciously in your mind . . re-iterates . . debt . . the word . . debt . . 

When you're scripting . . read through the sentence . . look at . . each and every word . . and make sure that each and every word is positive . . 

"I am so happy and abundant . . I am overflowing with money . . I have more than enough money . . to buy . . everything that I need . . and . . everything . . that . . I want . . whenever . . I want it . . "

. . 

Abundance . . 

Each and every word is very positive . . and actually affirms the future that you do . . want to create . . 

Each word . . is very . . powerful . . and has its own vibration . . associated with it . . 

. . 

Get a jar . . or a cup . . and . . fill . . it . . up . . with . . a bunch . . of . . water . . 

. . 

Tequila bottle . . friends . . bottle . . 

Potion bottle . . ripped off the label . . scrubbed the bottle . . scrubbed it out . . thoroughly . . because . . alcohol . . is usually a lower vibration . . 

. . 

Manifesting new things . . new life . . close out old things . . complete . . old things . . 

Took this water . . held it in my hands . . closed my eyes . . 

Actually . . I didn't close my eyes . . first . . I read the first sentence . . And so . . if my first sentence was . . "I'm living my dream job . . and I work from home . . making . . X-amount . . of money . . per month . . "

I would . . hold . . the water . . and I would first . . read that out . . and then . . I would just say it . . over and over . . and . . over again . . until . . I felt . . it . . click . . 

And I would almost . . imagine . . myself . . in that life . . living that life . . what . . it . . would . . feel like . . 

And I . . it's kind of like . . watching . . a movie . . but . . in your mind . . And then . . charging . . the water . . with it . . 

Just . . feeling . . that energy . . flow . . towards . . the water . . It doesn't matter . . if you . . uhm . . are actually doing it . . correctly . . or not . . 

The water . . still absorbs . . it . . It absorbs everything that's around it . . 

Whatever vibration . . of anything around it . . and your words . . have vibration . . and so . . it will . . get picked up . . by . . the water . . So . . you don't need to worry about it too much . . the water is . . picking it up . . It's totally fine . . 

Uhm . . But . . yea . . so . . you wanna . . sit there . . and basically . . charge . . that water . . 

And . . so . . what I did . . is . . I . . repeated . . each . . affirmation . . like . . probably . . about . . 10 times . . Until I just felt it . . really . . click . . and resonate . . in my soul . . until . . I felt . . like that was my . . natural . . vibrational . . set point . . 

Because . . your . . current . . set point . . Your natural . . vibration . . right now . . is what makes you feel . . what you're currently . . feeling . . 

What you want to do . . is that you want to repeat these affirmations . . to the point where . . you feel . . like you've almost convinced yourself . . that this is your . . new . . natural . . state . . of . . feeling . . 

Whatever emotions . . those . . new . . uhm . . manifestations . . bring you . . Embody . . those . . emotions . . right . . now . . And the water . . will be . . embodying . . it . . as . . well . . 

And so . . you want . . yourself . . and the water . . to be embodying . . and resonating . . with those . . uhm . . those . . affirmations . . 

So . . repeat them . . until you feel . . like . . your natural . . vibrational . . set point . . has . . shifted . . to this new vibration . . 

And then . . what you want to do . . after you're . . done . . going through . . all of your . . manifestations . . and . . charging your water with . . those . . brand new . . manifestations . . And you feel them . . really resonate . . with your soul . .

Uhm . . what I did . . is . . I . . took a few . . sips of the water . . and . . I . . drank . . a . . few . . sips . . because . . as you do this . . the whole . . point of this . . is to then . . remind . . your inner water . . your inner self . . to . . take on . . this new vibration . . Because . . this water's . . now . . charged . . with that . . new vibe . . And so . . you're really feeling . . and . . filling . . your . . body . . with . . that . . new . . energy . . Because . . this . . already . . resonates . . with that new . . energy . . 

Then . . what I did . . I left it out . . on . . my window . . sill . . to charge . . in the . . New Moon's . . Energy . . Because . . the new . . moon's energy . . is . . very . . powerful . . And it charges . . the













[1.0]
I Manifested My Dream Life Using... WATER?!
By The Gem Goddess
https://www.youtube.com/watch?v=jAM3oxIxtG4




