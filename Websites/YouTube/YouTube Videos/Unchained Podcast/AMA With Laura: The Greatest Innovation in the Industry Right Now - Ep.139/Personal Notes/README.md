
# AMA With Laura: The Greatest Innovation in the Industry Right Now - Ep.139
By Unchained Podcast

https://www.youtube.com/watch?v=39ST6O7I3Mw&pp=QAA%3D

### Personal Notes
[For education and learning purposes . . ]
[writing notes to learn about a topic is a great way to learn . . when it comes to watching YouTube videos . . or videos in general . . I like to take notes . . in realtime . . as I watch . . and listen . . And so this way . . maybe I can learn a lot more . . and retain more information . . on what was discussed . . ]



[Written @ September 28, 2020 @ 17:27]

Unconfimred . . 

the marking names in crypto . . 

Laura Shin . . started covering crypto . . 5 years ago . . Forbes . . journal . . 

. . . 


Nexo's instant credit line . . 

. . . 

Crypto.com . . waiving crpto fees . . 

. . . 

Today's guest is me . . 

. . . 

I got a bunch of . . interesting quests from you all in twitter . . 

. . . 

First question . . "What was your process for learning about crypto?" . . 

. . . 

I probably have an advantage that a lot of ordinary people do not have . . because . . I'm a journalist . . when writing for Forbes . . that gave me license to just email people . . and say . . "hey . . I'm writing an article for Forbes . . can I contact you" ? 

. . . 

Technical understanding . . and were in the thick of things . . when it comes to the topic of crypto . . 

. . . 

Why bitcoin was such a big deal . . to get myself fascinated . . and send me down the rabbit hole . . 

However . . at the time . . there was also . . DCG . . blockchain . . bitcoin . . I decided to go through some of these modules . . this is 5 years later . . and everything is so different . . 

The technology was so new at that point . . for DCG . . everyone was still learning what the technology was . . and how it can be used . . 

right now . . in 2020 . . coindesk . . and the block . . are some resources . . and there are so many more resources . . coindesk . . and the block

. . . 

What's going on . . where things are headed . . who the really good people are . . post something . . or interviewed like on a show like mine . . If you find who the good people to follow are . . or who the good media resources are . . then you'll definitely be covered . . all the way back in 2018 . . 

. . . 

One of my better skills as a journalist . . I feel that I can suss out who the good sources are . . and . . sometimes it's a trial and error . . 

When I go to that person . . that's the perspective . . I'm getting . . when I go to this person . . 

 . . . 

 
"How do you prepare for an interview?"

. . . 

I read a lot . . watch all the different shows . . to understand all the different considerations regarding those topics . . wrapping my head around the information that . . [is being conveyed???]

. . . 

The information is coming in the approrpriate order . . Step back . . look at the bigger picture  . . what are the tangential issues that relate ? . . 

. . . 

Why Bitcoin Now series . . Larger . . macro forces and their effect on Bitcoin . . 

. . . 

Libra . . Regulator . . ALl the different aspects of Regulation . . all the different happeneings where their actions can effect was going on . . 

. . . 

Not zoom in too closely . . to the interview . . but also . . be able to zoom out . . [take a look at the bigger picture?]

. . . 

Perfect opportunity to ask about that . . I asked . . what they called . . "uncomfortable questions" . . the audience . . comes first . . 

. . . 

[Pseudo-contraversial interviews??? ]

. . . 


Crypto-benji asks . . 

"What is your pre-interview ritual?" . . 

. . . 

I like to have the structure of the interview . . or the flow . . what information needs to be presented . . 

. . .

When I do a podcast . . I think kabout . . an article . . in reverse . . key questions are . . go to other people . . explaining that . . then maybe go back to . . the original . . people . . and ask them . . 

. . . 

Saving the actual interview for the people . . involved . . at the end . . getting the criticisms . . in advance . .

. . . 

Having a picture in my head . . of where to go next . . give them enough time . . to answer . . uncomfortable questions . . 

. . . 

The toughest interview . . I've done so far . . the very first one with Vitalik . . in January . . of 2018 . . Only started the podcast in the summer of 2016 . . and . . started to do them . . every other week . . 

. . . 

Print journalism . . can't cover how many articles I've written [2 decades of journalism . . at least . . was mentioned at the start of the video . . ]

. . .

Now . . when I interview him . . I know . . more . . what are all the issues . . I'm not the world's most technical person . . 

What are the questions that I can ask . . in a way that my audience can understand . . and still get a technical understanding . . at least at a high level . . to understand what all the different changes to the updates . . of Ethereum . . how they will effect them . . [how it will effect the audience . . ]


. . . 

What are the most uncomfortable situations of being a journalist . . 

. . . 

Journalists . . are kind of like Refs . . {referees} . . we're close to the game . . as can be . . without being in the game . . 

Some sources as friends . . so even within the friendships . . there is a line . . that both . . I . . and other sources are aware of . . 

. . . 

I get a lot of requests from people . . they want favors . . or introductions . . I've had to say no . . to so many of these requests . . 

If I go to a source . . and say . . "so and so . . is looking for a job . . " . . if they perceive that they're doing me . . a favor . . 

Positive . . coverage . . 

won't give them coverage . . or positive . . coverage . . 

most of these people are very chill . . and it wouldn't be . . a big deal . . 

. . . 

That makes me feel bad doing so . . the vast majority of people in different positions . . I might be more strict than the average person . . I don't know . . being cautious . . is probably better than being looser with these rules . . have a blanket rule . . just say no . . 

. . . 

There are times where I've done that . . and I feel that the person does . . take it more personally . . I need to do a better job . . explaining . . that as a journalist . . I really don't want to be . . too involved [really don't want to feel too involved?. . . ]

. . . 

Of course I'm friendly with a lot of sources . . of course we would chat a little bit . . or how things would go for us . . even then . . when they come on the show . . they are my guest . . no questions are out of bounds . . sometimes we have the personal and friendly relationship . . but when they come on the show . . no cutting slack . . . just because of friendship . . 

My audience comes first . . that's how I've chosen to handle all of this . . 

. . . 

"Outside of your paying job . . what hobbies do you most enjoy doing . . "

. . . 

Teach Yoga . . I love yoga . . Yoga . . changed my life . . Yoga is amazing . . 

A close second . . is dancing . . . [music . . ]

I love reading . . [I'm a writer . . ]

Closely related to Yoga . . I am into spiritual stuff . . Pranayama . . most of my friends . . are writers or yogis . . 

. . . 

ZW also asked . . "Do you own any crypto" . . 

I own a little bit of ETH . . for work . . I did in the path . . own a little bit of bitcoin and ether . . 

. . . 

[what equipment do you use ? . . . - - ]

[talks about microphone . . talks about stand ? . . ]

. . . 

Paul Rosatto asks . . "What is the storyline of your book and when will it be published?" . . 

The storyline is Ethereum . . I will publish it next year . . [I don't have a title ? . . . ?]

. . . 

[pre introduction to the question . . ]
"Where would you put your money?" 

. . . 

[talking about coinbase . . and talking about . . their experience . . they thought that coinbase was wrong . . and think now that maybe they were wrong . . ]

Crypto needed an on ramp . . bigger than charles schwab . . 6 years . . Not going to limit to 5 . . having a clear leader . . Kaitlyn Long . . She has a particular background . . understood bitcoin early . . now . . she's gotten amazing regulations . . 

. . . 

Chris John Carlo . . . another person who was a regulator . . regulatory side . . obviously . . 

. . . 

liquidity mining . . shield farming . . Synthetics was the first Crypto network . . Alex Glazteen . . Bitcoin will help his advocacy for human rights . . help the development of this technology . . my leaders to watch . . 

. . . 

Talk more about . . . 

[after advertisement from sponsors]

. . . 

Crypto.com 

. . . 

Nexo . . 

. . . 

Cash or stable coin . . 5.9 percent . . dividend . . is scheduled soon . . Nexo.io / token . . 

. . . 

Back to my . . A.M.A.

. . . 

How do you think the layer one blockchain will look ? . . Zero Sum or [tied all ships?]

. . . 

I think there will be . . [different types of layer ones . . . ]

. . . 

The future might look not that different from what it looks like today . . If people would have tried to predict the future of the internet . . back in 1998 . . they would have been wrong . . 

That's as far as my . . [prognosis . . goes . . ]

. . . 

[A question about conviction of the industry ? . . . what is the next technological innovation besides bitcoin or blockchain ? . . ]

. . . 

Crypto assets . . . now . . [regulators allow crypto assets to be stored ? . . .]

. . . 

Paul Tutor Jones . . bitcoin now in my portfolio . . it is a big deal . . people pay attention to . . a circle that has a lot of money to pour into crypto . . 

. . . 

Watching the 2017 . . speculative bubble . . . pretty disparaging . . if we just pan out . . and not look too closely . . about what's going on . . it really did get every day people involved . . a lot more democratizing . . than finance has been . . but on rose colored glasses . . because people who did get involved obviously did lose money . . 

. . . 

Look at the barriers to entry . . I.C.O.s were a way to [cut out V.Cs? . . ]

. . . 

A lot of walls that existed in finance . . . were . . [put down ? . . . ]

. . . 

"If not crypto . . what is the next tech rveolution . . ?" . . 

. . . 

I've been watching this . . GPT-3 . . which of my friends will be replaced by . . this . . GPT-3 . . I have a friend who has been working in . . A.I . . for a number of years . . "Laura . . people who don't work in A.I . . they are the ones that think this is gonna kill all the jobs . . " . . . "People who work in A.I. . . don't think that . . " . . 

"Don't be too wow'd . . there are still a lot of flaws in this . . it's not going to replace anyone soon" . . . 

. . . 

"Defi . . to determine if they are fads . . of they will [keep people's attention ? . . or keep up ? . . with attention . . of people ? . . ]"

[A question on De-fi? . . ]

. . . 

[Uniswap ? . . ] Uniswap is doing well . . there's a lot of demand . . for volume on these different automated market makers . . I expect to see that keep going . . 

Yam . . fiasco . . the dao . . instead of taking months to go through it . . it only took a few days . . 

Unnecessarily safe activity . . I think we will continue to see that space go . . 

Ethereum 2.0 . . when it switches to proof of stake . . We'll keep watching those trends . . and I'll have more people on the show to keep discussing those things . . 

. . . 

I do think things will continue to grow . . Let's say cloudy . . scams . . hacks . . phishing . . be careful . . safety of your money . . your own security . . don't be greedy . . be thoughtful . . remember that this is all very experimental . . I'm still seeing a lot of scams . . 

I myself am getting a lot of imposters trying to trick my friends . . 

. . . 

[??? question ? ? ? ]

. . . 

The early investors will have done their due diligence . . they will put their money there . . once the fly wheel gets going . . 

For other people who don't do their due diligence . . avalanche of activity and interest . . that results in the price going up . . but . . 

relevant for people trying to figure out when to sell . . 

. . . 

[??? Question ???]

De-Fi . . Wi-Fi . . sounds like . . internet . . wrireless interesting . . Wy-fi ? . . Y-Fi ? . . . 

That got people excited . . combined with this idea of a fair launch . . that took off so fast . . yam . . was another good example . . of that . . before that bug . . there was a moment where they thought that they could do something to save yam . . to deligate their tokens . . quorum on a certain vote . . there was this other bug . . they were gonna get enough tokens to pull it off . . 

It seems to me that during the pandemic . . these people . . are on the internet all the time . . 

. . . 

[??? a question about how fast things are moving . . do you ever get time to take a break ? . . relax ? . . ]

one foot in the present moment . . one foot in what my book is about . . I don't have time . . to reflect on . . all the things that are moving . . 

In 2017 . . I felt . . [everything was being inundated ? . . ]

. . . 

"Why would you by COMP? " . . . [a question . . ]

. . .

In the high-minded notion . . of the purpose of these coins . . let's be real here . . at least 90% of these people . . basically just want to speculate . . liquidity . . providing their tokens to be lent out . . doing this activity . . to earn . . this exchange token . . wel . . that's how it works . . 

eventually these tokens will make way to the people who are interested in participating in governance . . 

. . . 

Out of all the up and coming . . layer one protocols . . which will . . [come out soon ? . . come out first ? . . ]

. . . 

Community is what matters the most . . Ethereum . . it has that community . . in the moment . . it's facing a deadline . . [activity is ramping up . . ] . . . fees are getting really high . . 


. . . 

Can they pull of this transition fast enough . . to . . support these . . De-fi . . applications . . 

A de-fi . . ecosystem . . one that I've interviewed on my show . . maybe at a similar level of development . . of an Ethereum . . 2.0 . . is Poka dot . . 

Para chains could be devoted to De-Fi or something . . I'll have my eye on . . 

. . . 

Maybe if I come up with anything else . . project . . Serum . . of Salama . . 

. . . 

Thanks for tuning in . . 

To learn more about me . . and the answers . . to any of the questions that i answered . . [in this video]


. . . 

Unchained is Produced . . by me . . with help from . . 

Anthony . . Yoon . . Daniel . . Ness

. . . 


