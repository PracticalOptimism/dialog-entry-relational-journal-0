
🎬 Live Stream Series Description:

A new currency for a new era.

Key Features:

** Decentralized **

The Practice: Uses technologies like IPFS, Dat and Braid Protocol.

The Principle: A peer-to-peer network distributes control to the network rather than a few monopolies.

Your Mission: Learn more about decentralized technologies like IPFS, Dat and Hashgraph.

** Universal Basic Income **

The Practice: Everyone receives a daily payment.

The Principle: We are all creative.

Your Mission: Dream big, dream small. Afford to live through it all.

** Open Source **

The Practice: Anyone can fork the code base and start their own currency.

The Princple: Let’s create a world we each individually love.

Your Mission: Modify the code to your heart’s content.

** Anonymized Identity Support **

The Practice: Anyone can have an account.

The Principle: We are all citizens of a global community.

Your Mission: Know you belong to a global ecosystem. Live in the future without borders.


--

Sometimes I take personal notes on the development process . . or things that I'm thinking about . . throughout the live stream . . you can access that directory of information here . . 

Dialog Entry Relational Journal 0:

https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

