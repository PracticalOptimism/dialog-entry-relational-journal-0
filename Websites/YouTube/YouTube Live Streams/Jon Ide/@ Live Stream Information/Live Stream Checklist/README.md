
# Live Stream Checklist

Greetings

* [] Greet the viewers
* [] Plan the tasks to complete

Health and Comfort

* [] Have drinking water available
* [] Use the toilet to release biofluids and biosolids
* [] Sit or stand in a comfortable position
* [] Practice a breathing exercise for 5 - 15 minutes


Music

* [] Prepare a music selection or a music playlist for work (ie. spiritual music, energy music, bossa nova, etc.)


Work and Stream Related Programs

* [] Prepare work and stream-related programs: (1) live stream chat window, (2) music video window, (3) command line interface, (4) live stream timer, (5) web browser, (6) program text editor, (7) virtual private network (vpn), (8) notes application


Periodic Tasks

* [] Periodically check to ensure the live stream is still live or the internet video footage is still being recorded (ie. Check every 1 hour)

Salutations

* [] Thank the audience for viewing or attending the live stream
* [] Annotate the timeline of the current live stream
* [] Talk about the possibilities for the next live stream



Notes

Notes on Newly Discovered Words / Newly Created Words
* Sometimes I'll write down newly created or newly discovered words from typographical errors while typing on the keyboard


Notes on the Dialog Entry Relational Journal
* Sometimes I'll write notes in my dialog entry relational journal . . such as (1) "day journal entry" notes, or (2) "mood journal entry" notes and sometimes (3) "dream journal entry" notes . . to talk about how my day has been, how I've felt throughout the day . . or what my dreams were when I slept the day or night before . . [or maybe from a distant night ago that relates to a topic that's brought up or considered]


Notes on the Internet Suddenly Disconnecting
* Sometimes the computer network for internet in my household . . will be unavailable . . or . . my computer disconnects from the internet . . And this results in the live stream being stopped . . as my computer is . . offline
* Sometimes I'm not always sure . . if the live stream has already stopped . . for example . . due to a network connectivity error . . and so . . the development footage . . or the development video [of the part of the live stream that wasn't recored . . because my computer was offline . . from the internet . . ] is not always . . available to be accessed . . uh . . I'll do more research to learn more about what I can do about this . . 



