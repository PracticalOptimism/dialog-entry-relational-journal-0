

Timeline Annotation:

[0:00 - 1:13:00]
I watched videos of the YouTube Videos that I've seen today . . or rather . . for . . October 31, 2020 . . that related . . to the topic . . of Halloween . . [1] [2] [3]

[1:13:00 - End of Video]
I watched videos relating to the topic of halloween . . . I tried to think about the topic of halloween while also . . thinking about other things that relate to the topic of halloween . . such as . . cosplay . . and science fiction . . 

Here are some notes that I took while thinking out loud . . about the topic . . of halloween . . 

happy halloween, costume, larping, cosplay, suit, clothes, body art, ppe (personal protective equipment), prosthetic art, science fiction equipment, gear, model, actor, actress, acting, role playing, wearing makeup, playing dressup, playing pretend, fashion look, cultural uniform, creating your dream reality . . .

historical recreation / historical interpretation / time travelling to historical periods and researching and referencing the styles and designs of the clothes [ie. Bernadette Banner, Cathy Hay and friends] . . .

comic con, blizz con, renaissance festival, burning man, larping festival, lookbook, try on haul . . . video game conventions, movie and television show conventions, anime conventions, fashion shows, walkways, history festivals, school mascot events, costume contests

Some Video References [from 0:00 - 1:13:00]

[1]
MY HALLOWEEN 2020 LOOK
By Eugenia Cooney
https://www.youtube.com/watch?v=elh5R14hLmk

[2]
Fantasy, Sex, and Love Addiction Recovery Vlog
By Unicole Unicron
https://www.youtube.com/watch?v=HgzPXo0rIAY

[3]
WHICH GIRL HAS THE BEST COSTUME??
By Brandon Walsh
https://www.youtube.com/watch?v=sFjeOrn5Obo

(The Rest of the videos are quite uh . . well . . I didn't take notes on what I watched . . so please . . watch that area of the video [1:13:00 - End of Video] . . to learn more . . )


