

# Ecoin #308


Timestamp Annotation

[0:00 - End of Video]

👔 Edited Video Description:

(1) Updated the README.md file in https://gitlab.com/ecorp-org/ecoin

(2) Failed to write a dream journal entry

(3) Planning to have a Dream Journal Entry Live Stream Series separate from the Ecoin live stream series


😬 Realtime-Typing-based Video Description:

In this live stream development video . . I worked on . . the README.md file . . which previously showed markdown tables with tall item entries . . which weren't 


In this live stream development video . . I worked on the README.md file . 

The previous README.md file showed the Benefits, Features and Limitations lists as Markdown Tables . 

The Markdown Tables had row item entries that were quite tall and would take up 10 to 100% or more of the vertical space of the mobile phone display . . for only a single item in the Markdown Table . . which . . was not very pleasant for . . me . . Jon Ide . . to look at . . and maybe . . other users would feel similarly . . 

The updated README.md file . . shows each of the list items for the Benefits, Features and Limitations sections . . in a vertically-condensed format which should be easier for people to read through or look at even in the case where the user is randomly scrolling up and down on their phone very rapidly looking for information of a particular kind . . or something like that . . and so . . condensed information lists can make things easier to perceive . . or conceive . . when one is searching for information . . in a list . . uh . well . that's my understanding of the theory anyway . .

. . . 

I also updated the JavaScript API Reference section of the README.md file . . 

. . . 

I created the "Planning", "Work-In-Progress" and "Require More Support" Labels in the Gitlab Issue Board for the Ecoin project:

https://gitlab.com/ecorp-org/ecoin/-/boards


. . .

I had planned to write in my dream journal . . but I didn't accomplish that today . . I had some really cool dreams on September 19, 2020 . . I would love to share them . . 

. . . 

I would like to see if maybe . . If maybe . . I should . . or . . if I should . . keep a dream journal live stream series separate from the Ecoin live stream development series . . 

And so . . 

for example . . Jon Ide #123 Dream Journal Entry - September 20, 2020 . . . that could be cool . 

And if I would think about having a Day Journal Entry Live Stream Series . . I could work on that in the episodes titled . . Jon Ide #123 Day Journal Entry - Month, Day, Year


Time Journal Entry Video Series Title Formats:

Name #Number - Day Journal Entry for Day, Month, Year
Name #Number - Week Journal Entry for Week Number, Month, Year
Name #Number - Month Journal Entry for Month, Year
Name #Number - Year Journal Entry for Year

. . . . 

Dream Journal Entry #123 - Name
<Name> #<Number> - Dream Journal Entry for <Calendar Date>

. . . That would be a live stream journal entry where I can talk about the dreams that I have when I sleep at night . . . and how I interpret the meaning of the dream or something like that . . 

A lot of dreams I have . . relate to . . action, adventure, comedy, scary, sexual-related topics, teachings, information, . . technology, science, math, the nature of reality . . 

I think dreams are very practical . . uhm . . for example . . with a society like uh well . . our society seems to like to focus on technological achievements . . and I've uh . . interpretted a lot of my dreams . . to relate to technological developments . . uh . . well . . if they are interesting to you . . then you would maybe let me know in the comments of this video . . and certainly . . uh . with my experience in today's related technologies for society . . uh . whatever that means . . uh . I'm uh really not that experienced in a lot of things . . but in any case . . I hope that you are also impressed like . . I am . . but if you're not then I'm sorry . 






