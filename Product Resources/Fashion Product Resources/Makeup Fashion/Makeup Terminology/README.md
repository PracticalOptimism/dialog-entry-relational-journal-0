

Makeup terminology

Bronzer; Concealer; Contour Powder or Creams; Eyelash Glue; Eyebrow Pencils; Eye Primer; Face Powder; Face Primer

Foundation; Highlight; Lipgloss; Lip-balms, liners, primers and sticks; Makeup remover; Mascara; Nail polish; Rouge, blush or blusher; Setting Spray

Bronzer - gives skin color by adding a golden or bronze glow.

Concealer - covers any imperfections of the skin such as blemishes and marks.

Contour Powder or Creams - Used to define the face, for example, giving the illusion of a slimmer face or to even modify at person’s face shape as desired.

Eyelash Glue - Sticks false lashes to the eyes.

Eyebrow Pencils - Defines the brows.

Eye Primer - Prolongs the wear of eye-shadows and intensifies colors when used used with shadows.

Face Powder - Sets the foundation, giving a matte finish, and also to conceal small flaws or blemishes.

Face Primer - Applied before foundation and mostly reduces the appearance of pore size. Face primer also prolongs the wear of make up and allows for a smoother application of makeup.

Foundation - Evens out the skin color. Usually a liquid, cream, or powder. Of the all the types of makeup, foundation is often the starting block for makeup use.

Highlight - Draws attention to the high points of the face as well as to add glow to the face. It may contain shimmer

Lipgloss - Liquid lipstick in a sheer or transparent medium.

Lip-balms, liners, primers and sticks - Liquid or gel base and may contain alcohol to help the product stay on the lips which makes these items generally waterproof.

Makeup remover - Used to remove the makeup products, for example, before going to sleep. It may or may not contain moisturizing properties.



