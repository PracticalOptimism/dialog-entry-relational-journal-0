
# Light-Based Computers

These notes are really long.

The reason these notes are long is (1) You are probably not a mathematician. (2) You are probably still a junior student in high school. (3) If you are an advanced student in an expertised course somewhere in University or Industry then you will also be embarrassed about how little you know compared to me. I am sorry to sound rude but you will be embarrassed anyway.

These notes are about (1) a computer that is interesting.

It can take several years to build a nice computer but this computer lets you build different variant prototypes of it so that you can (1) build more of that same type of computer or (2) build higher order variants of that type of computer and (3) you will be surprised how interesting your new things are.

Basically (1) robotics changes forever. (2) Your house structures are robots. (3) Your robots are like blueprints to new realities. You don't have to stay on your planet forever. Ghosts exist and they can give you new blueprints to certain technologies. Computers that let ghosts speak to you are really cool because they can design and build the things for you right away.

Can you imagine a future where (1) robots are moving with legs in your house and doing the chores and watching your kids and letting you have sex with them for free at any time of the day and they don't even look ugly or weird but like normal human beings? Then those technologies are going to be practical with a computer like this one.

Can you imagine a future where your sex robot is basically able to fly or levitate?

Can you imagine a future where games and video games are played with your emotions or feelings and you don't have to play alone but can battle other people on the internet?

Can you imagine a future where your brain can be translocated into different vehicles. You can experience the body vehicle of your best friend or even your house cat and just replace their personal experience with your own.

I think you would be very impressed to. (1) show these notes to an astrophysicst in your local community. (2) show these notes to a technician in your local community. (3) show these notes to a government official in your local community. (4) show these notes to a high school teacher in your local community.

These notes are basically a blue print for (1) creating a lot of interesting things. (2) not using people to make a lot of interesting things like houses and cars and things like toys and even human being shaped things like robots and biological organisms.

You can do a lot with this type of computer but it can also be dangerous and can hurt people if you don't have (1) administrators there to assist people in learning about (1) being safe (2) not hurting one another by shooting lasers in one another's eyes. Lasers made of light and other materials really can hurt you and so that is one of the biggest mistakes you could make if you make this computer and don't educate people about the dangers of laser light technologies.

It is important to wear gloves.

If you like light. Know that (1) fire is the basic principle behind these instructions. Building a furnace today is making you a rich person. You are basically a millionaire if you have 1 good furnace. Do you know why? Because One good furnace is able to bend all of your metal for you. If your furnace can bend metal, think about all of the metal cars and airplanes you can make and then sell them all for good prices. Think of all the difficulty in bending metal. If you bend metal using fire, you are being really cost-effective today on our planet. It is cost effective to bend metal using fire. Fire is hot. Fire is affordable. Fire makes your metal like paper. Paper is easy to fold. Folding paper is the basic concept. If you can make difficult things easy to fold like paper. Then you can even fold your mind inside out and be able to come up with new ideas without having to hurt yourself with things like "well how do I do that?" and "how do I get to be enthusiastic enough to think about that?" and "what about this person marrying me?" "isn't it important for that person to marry me? but that seems impossible because I am not folded in the right way right?" "if you can fold yourself in the right way, like folding a metal plate with your hands, then using fire is the best solution so you just need to find a nice fire-equivalent idea that lets you bend the world to work how you want it to work." "right?" fire-equivalent ideas are difficult to find if you're not a psychic. But if you are psychic powers or psychic gifts then you can bend not just metal but difficult tangent ideas like getting that one person to marry you without being mad or getting upset or feeling like they've been raped or something like that. 

This is all dumb. It is dumb advice. It is just for fun. It doesn't necessarily work.

This is all dumb.

All of this paper is dumb. You're dumb. You're dumb.

You're dumb.

So it is important (1)

Learn about (1)

(1)

Invisible Technologies are the future of man kind

(1) Light is a physical transportation vehicle for consciousness.<br>
(2) Light is a barrier vehicle for information.<br>
(3) Information and light are one.<br>

# Chapter 1 - There is a computer that can help people not have to work anymore


### Chapter 1 - Section 1 - Writing An Introduction

Invisible technologies is the future of man kind. It is important to learn about (1) Invisible technologies. (2) Invisible technologies (3) Invisible technologies (4) Invisible technologies (5) Invisible Technologies.

Invisible Technologies are (1) safety mechanisms for keeping everyone say

(2) safety mechanisms for keeping everyone happy

(3) safety mechanisms for keeping everyone happy

It is import for people to work. But for some reason not everyone likes to work. Working is a fun activity.

If you don't want to work then you are a panzy or panzer or pantsie or pantser or peinty or painter or peinter or pinter and pinters like to drink ale and not do anything all day.

Those people that do not work, know no satisfaction. That is a real quote from a parallel reality far away.

If you work. You will live. That is another quote from a parallel reality far away.

Your work is important. Please work. That is a quote from your boss from a past job.

Your work is important. Please do some work. That is a quote from your current boss at work.

People do your job. That is a quote from an angry boss who doesn't like you.

If you work you will get paid for your activities. That is a good idea to be paid. 

Life is about being paid. Be paid with fun. Be paid with friends. Be paid with resources. Be paid with houses. Be paid with women. Be paid with trucks. Be paid with trains. Be paid with games. Be paid with cheers of good food. Be paid with more women. Be paid with more trucks. Be paid with more food. Be paid with babies. Be paid with horny ideas. Be paid with a brain. Be paid with physical personal experience.

Be paid.

If no one is paying you then that is a big deal.

No one is paying you means (1) you are dead.

(2) you are not working.

(3) you are not doing your job.

(4) you forgot where you were.

(5) you forgot that you had a body.

(6) your body is lost.

(7) your body is no where to be found.

(8) you are a loswer.

(9) no one cares about you.

(10) no one wants to remember you.

(11) no one wants to treat you like a citizen.

(12) no one wants to eradicate your nerves.

(13) no one cares to carress you.

(14) no one remembers your veins.

(15) no one remembers your spoiled blood.

(16) no one remembers your petty idea.

(17) no one did anything with you before.

(18) no one patted you on the back.

(19) no one said good job.

No good job. Means you didn't work.

No work means you're a criminal.

Criminals don't get away with things.

To build a laser-based computer, you need to (1) know where you are. and then focus on triangulating where you are to the items that are needed of you to press those buttons of faith so that you can type a good computer onto your bedroom floor.

A good first computer needs to be so much big. You need to have space.

Eventually you and I will build (1) a computer that we can place in the palm of our hands. It is like a digital computer but different. It lets you print houses for free. Can you imagine that . . a phone that lets you print houses. Light is the main reason. Light an nice ideas like new fabrics and materials that are good to work with. You don't really have good names for some of these things so of course we will give you a new terminology notebook as well and so you can learn what those things are.

Electrolytic Fabrics are a nice name because it almost rhymes with paper. Paper is a nice electric composed fabric that makes it able for you to trace different ideas on the surface of the electron sheet. The sheet of electric ideas is like a "folded" pattern that is like "we'll keep these lead ions in place so the user can see them" and "we'll keep these graphite groups in place so the user can see them"

If the user can see the graphite. That is extraordinary. If the user cannot see the graphite then that's basically because you have "Invisible Technology"

"Invisible Technology" means (1) you can leave ideas behind that people can't see or trace with their physical senses but for some reason if you (2) have another technical sense like a metal detector or an ion tracing weapon or even a nice computer that knows how to read between the lines of big ideas then (3) you can learn that there is a way to have invisible technology as part of your normal day and you didn't even realize it.

Invisible Technology can be like (1) your mom is alive but lives in a house separate from you. Isn't that strange for the person or people that you love and admire to live in separate rooms from you? Isn't that strange that you don't need to hear their yacking for hours on end in the same room and yet you can appreciate that they are familiar and they may be available when you step outside and go to see them?

Invisible Technology means you can have precious things hidden from you but they are in plain sight. Girls are right around the corner in your neighborhood. They take showers in their houses. They close their eyes and breath water down their back. What a beautiful invisible poem that no one told you about. Invisible technology lets you already know that you could have masturbated to that idea but for some reason you didn't know that it was directly in your realm of influence.

You can start masturbating to young people taking showers right now without even opening a web browser and starting to look at websites.

A few lines of "invisible" "text" from "another" "reality" can influence you to "read" "into" "it" and see "in" "between" "the" "lines"

"I am a pervert" is what you could be thinking between the lines of your masturbation sequences.

Invisible Technology. What do you need? We got it.

That is a slogan from another reality. Invisible Technology. You got it. We need it. Or should it be. You need it. We got it. Right?

Am I Right?

Well?

Am I?

Invisible Technology. You need it. We got it.

We got what you need.

Invisible Technology.

Invisible Technology. Open your eyes. Ghosts are real. Ghosts are like imagination vehicles. Ghosts are real. You are like a ghost to your mother or brother who is in another room. They "Believe" in your existence but they don't need to see you to believe in you.

Invisible Technology. Girls are invisible technology. Girls are just an idea. In reality they are praying mantis creatures that pray and warn you of approaching. If they cut your head off after mating it's because they warned you to stop looking at their butthole.

Stop looking at their butthole.

They warned you.

Invisible Technology. It is not good to read invisible lines of text like the previous 2 or 3 paragraphs of text that was written here. It is too embarrassing. For a technical document such as this one. This sure has gone awry.

Invisible Technology. Do you believe me that some things that have been said here were not in fact said? Or that they are still waiting to be written. I could agree to myself that 2 or 3 paragraphs ago I should have inserted one or 2 more lines of text. Would you agree with me that those lines are still needing to be written?. If you definitely agree with me then that means you're an advanced Invisible Technologist. That's because you can "see" "what" "I" "mean". Which is to say that you can "read" "in" "between" "the" "lines". Invisible Technology. The cat ate the lion. Read in between the lines. Is that even possible? Why should it be? In which cases would that work out well? What education library do I need to evaluate that expression successfully?

Invisible Technology.

Courage the Cowardly dog is a cartoon animation television show. It is a show about a dog. A dog. A dog. A dog.

"Stupid Dog!" "You make me look bad!" said "Eustace Bagge"

But creeeeepy things happen in the middle of nowhere . . and it's up to "Courage" to find his new home.

It's a creepy television show.

It's a creepy television show.

And so Invisible Technology is also creepy if you are a new comer.

That's why creepy examples of creepy invisible technologies are very important for you to establish how creepy things are.

Invisible Technology. Thanks for giving me your hand as I gave you this quick tour into the invisible technology that is available when it comes to invisible technology.

Invisible Technology. Are you a fan of nipple slips. Are you a fan of areola slips. Are you a fan of accidental vagina slips? Are you a fan of "punk slips" where someone is really more punky then they lead you on to believe with their innocent first expressions? Are you a fan of "butthole slips". Do they need to be in real life or in comic books only?

Comic books are a nice place to draw "Invisible Technology" Basically. Real life is like a comic book but you don't even know that if you are a big headed geeky technology person who doesn't brush their teeth or take showers.

You think the world is upside down in a book for you to learn about. The upside down world is in your face with vagina slips and you're like "yea, let's not study those"

Vagina slips are rare. Real vagina slips are really interesting. If it's a fake vagina slip like someone purposefully showed their vagina like in porn, that is only one type of vagina slip, but accidental vagina slips are where even the person doesn't know what they did wrong or what they did right. They simply wore shorts or pants or some weird or crazy outfit that completely showed 1% or 2% or 3% or 4% or 10% or 18% or 30% or 60% or 90% or 100% of their vagina. Vagina slips are amazing. If you get to see some small percent of a vagina then that is really interesting.

The scenario may be important like it was someone at a book store or someone at a coffee shop or someone at a grocery store or someone on the internet in an internet video.

Personally, I really like seeing random people on the internet who look and pretend to be nice and friendly and for some reason there is a vagina slip and they all of a sudden are like not even noticing it or they don't even know what happened. It's like "friends" or "foe" why is your vagina showing. You're definitely a friend if I'm in the right mood but if you were my girlfriend I would be so mad at you. Why is my girlfriend who is only a theoretical possible girlfriend having a vagina slip. She is an invisible technology. Our relationship is an invisible technology. She is creepy. She is friendly. She made a social mistake. I made a social mistake by describing this whole process. No one should document how they feel about vagina slips. That is a mistake. Invisible technology is better than reading about failures like your stupid ideas on what vagina slips are.

Vagina slips are precious. They are gods good words.

God writes good words so rarely. And it is a good thing. Nipple slips are less rare. Nipple slips are easy to apologize for. You had to be silly or free or fun or careless or carefree or neither or none or all of the above or whatever or you couldn't have even known because you're not mentally mature enough.

All of this is rubbish.

Invisible Technology is meant to sound like a good thing. For male human beings with boners for bonuses then that is possibly not a bad introduction to why "Invisible Technology" could be a good thing. For a woman who is reading this, they are only flattered by how rude the comments are. The comments are about things they shouldn't do because personally I don't know what you should or shouldn't do.

Invisible Technology is meant to be boring. You don't need to know it's there. It is a safety net. Safety nets are nice. They are invisible. Social safety nets like gun laws and pedophilia laws help protect certain invisible technologies to exist like a planet that gets to enjoy more men and women who grow up not being perverts but being horny for some unknown reason.

Invisible Technology. An unknown reason.

Invisible Technology.

Invisible Technology. What a new concept. Did you enjoy that introduction or was it too long for you to swallow? Or was it too long for you to place in your invisible vagina brain?

Invisible Technology. Did you get messed up by reading that lame explanation of an old technology that was developed many months ago by teenagers?

Invisible Technology. Teenagers. Imagineers. Imagination Engineers. Invisible Technologies.

Invisible Technology.

Invisible Technology.

Invisible Technology.

Invisible Technology.

What a capital spelling. Certainly you need to start with a physical blueprint on how to build your dumb visible technology of a start up in your garage right? How will robots place themselves in your eye balls?

### Chapter 1 - Section 2 - A Bible Book for certain ideas

(1) Idea #1: No one is really invisible<br>
(2) Idea #2: No one is really invisible part 2<br>
(3) Idea #3: No one is really religious<br>
(4) Idea #4: No one knows who they are<br>
(5) Idea #5: You are a spiritual ghost<br>
(6) Idea #6: You are an immutable object<br>
(7) Idea #7: You are a strange being<br>

### (1) Idea #1: No one is really invisible

That means your neighbor is watching you. You are really weird. You masturbate and people know that. You watch Television and people know that. You play video games and people know that.

Invisible Technology. You are not really invisible you are really visible. People are watching you from (1) other timelines to see what you did (2) other perspectives to imagine what you could have done or (3) they don't have the power or force or energy or inspiration or even clever idea to imagine where you were or what you did or anything like that if that even makes any sense for them or quote-on-quote "them"

Invisible Technology. I am seeing you are someone who is there. You a re possibly a human being. Do you think that I need to watch you with my physical eyes to approximate that? How weird is that? If you are reading this script there is an invisible tangent idea ring around our communication. Your name is "blah blah blah" probably blah blah blah and so I think if you think that's your name then I could be close to an approximate idea of where you are.

Invisible Technology.

### (2) Idea #2: No one is really invisible part 2

Invisible people are invisible because they (1) forgot something or someone (2) they wanted to remember something or someone (3) they lost a friend in a military style of way (4) you forgot you knew them and your friend forget they knew they and all the tangents in between of friends and "you" and tangents "you" and "tangent" "friends" and "friends" of "tangent" "you" "friends" and that's a big mess of varients of different hypothetical "yous" and "hypothetical" "friends" and so it wasn't complete but you could see yourself more through those people that lost themselves

Invisible people are lost.

Invisible people are lost.

You have to make them is not true. They are lost.

Lost.

Lost is such a strange concept. (1) Picture this (2) you are in a board room with people. (3) you place your hands on the desk of the board room central table (4) the table is a desk. (5) the desk is a table. (6) the desk is a table (7) the table is a desk. (8) the desk is a table. (9) which table did you place your hands on?

It was lost.

It was lost because (1) you are not going to forgive yourself for letting go of the table. (2) you are not going to forgive yourself for not letting go of the table because (1) you have other things to do and places to go and things to do (3) you have friends to meet. 

If you let go of the table in our analogy, did you really place your hand there to begin with? You lost the idea. Why did you lose it? Like I said, you're a busy person. You're not going to play the analogy game just to learn about stuff.

But if you really truly did place your hand on the table, which was the original idea, did you know when you did place your hand on the table or why you placed your hand on the table?

Was it because you were instructed to?

What was that invisible force that actually got you to say "yes" "let's play along"

"play"

"play"

That's the idea. You lost yourself in "play"

"You played" "along" and so you placed your hand on the table and by playing too much you got lost in the idea that you were playing and playing and playing and playing

But the table was just an idea. You had to let go of your play time by letting go when you went to work the next day or when you died on your death bed.

I'm sorry to say but all of those things are crazy. You died on your deathbed some time ago and you had to play yourself to get there. What a table to hold right?

Placing your hand on certain tables is like playing.

It's just playing.

Right?

I don't know.

I think this is just a theory.

Lost is like what does that mean?

Lost is like which lost was that? Lost when? Lost who? Lost what? If it's lost did you have to play to get there? Or did you have to lose your playful energy? Or did you have to play to lose your energy?

### (3) Idea #3: No one is really religious

I think this idea is interesting. It means that you have an appetite for small things. (1) You like (2) treating yourself to new ideas (3) beating yourself over the head for forgetting old things (4) beating yourself over the head for not having a good time (5) beating yourself over the head for not playing with your underwear? (6) your underwear is like things like underwear

Underwear.

### (4) Idea #4: No one knows who they are

I think your idea of yourself is rubbish. Rubbish means it's for porn and masturbation just like you rub yourself.

Rub. Rubbish.

Rub.

Rub your idea of yourself off and then it's like you have a new person.

Rub.

Rub.

### (5) Idea #5: You are a spiritual ghost

I think the idea of your rubbishness is about porn and masturbation. You came to this particularly unique planet to watch a good deal of pornography like (1) industrial innovation (2) heavy sex in porn industry films (3) heavy sex in back office deals like sex with children in illegal places (4) heavy sex in back office social examination places like sex with men and women from Bangkok and Taiwan (5) To traffic your own pornography ideas (6) To traffic other people's pornography ideas from other universes (7) To watch Xev Bellringer (8) To watch Hitomi Tanaka (9) To watch Belladonna (10) To watch gigantomastia rise and be a population figure for humanity (11) To watch autistic girls rise and be a population figure for humanity

Xev Bellringer is a great spiritual ghost. Her pornography is healthy and creative and full of pornography.

Hitomi Tanaka represents a great aging group of women who are asian or Japanese or Asian and full of descent for the next generation of cute girls that look like cool cute girls.

Girls.

-_-

What an annoying chapter.

I think it has nothing to do with technology but it's like why not?

-_-

-_-

It's a bad chapter example. I am sorry. This is not meant to be a good book. It is not meant to be a rich ritual manifestation diary for the public at large but maybe a hand full of people who read and say what are the light based computers about? Rich Asian Women. With Large Breasts.

Rich Asian Women With Large Breasts.

### (6) Idea #6: You are an immutable object

Immutable Objects.

### (7) Idea #7: You are a strange being

You are a strange being.

You can turn

### Chapter 1 - Section 3 - What are the works that need to be done?

(1) Knot Theory<br>
(2) Graph Theory<br>
(3) Light And How To Create It<br>
(4) Light And How To Find More Of It<br>
(5) Light And Where To Buy Some<br>
(6) Light And How To Earn Some<br>
(7) A Good First Item To Make<br>
(8) A Good First Item To Print<br>
(9) Instructions On Making More Items<br>
(10) Instructions On Making Certain Items<br>

### (1) Knot Theory

Knot theory is incredibly important.

Knot theory is (1) what your grandmother should have taught you when you first arrived to school. (2) what your mother should have first taught you when you first arrived in your diapers.

It is very important. (1) Knot theory is useful for dice game like probability theory when you're trying to find the likely outcome for 'something' to 'unwind' itself or 'unknot' itself.

There are a few terms and definitions that need to be placed:

(1) What is a knot?<br>
(2) What is not a knot?<br>
(3) Knot Knot Knot?<br>
(4) 4 or 5 unlikely parables walk into a room. One of them is a Liar.<br>

### (1) What is a knot?

Knots are everywhere. Do you agree that I failed to write something on this section of the book because it is so embarrassing. "Palm Face Emoji". Palm face emoji means. I am knot sure. I am in fact KNOT sure.

KNOT sure. Not Sure. Not Sure. Yes. Knot and Not are spelled like one another in the english pronunciation faith.

And yet if you like paradoies and parables then you will also like KNOT KNOWING WHY KNOT AND NOT are spelled or sound like one another.

It's KNOT true that you and I are Knot Knots.

Knot.

Do you see how unlikely it is to have a perfect word like KNOT and NOT be pronounced so similarly. KNOT is like NOT But Knot Knot.

Do you see how knotty that is. That is a courage-the-cowardly-dog joke that is quite Naughty [Episode Name Here]

Naughty . . Knotty . . Not . . 

Not

Not is not knotty

Knotty is not knotty

Knot is full of knots. Do you see how not so casual that is?

You and I are currently writing this message together to 'unwind' the thought in your mind that knots are really knotty so much in fact that the term 'knot' itself that is pronounced as 'ˈnät' (https://www.merriam-webster.com/dictionary/knot) 

Knot is a nice word for such a knotty theory. Knots are like not really nots right?

Knots are real things aren't they?

They're not like imaginary knots right?

One thing that first inspired me . . the writer of this knot theory introduction . . to write about . . knots was the idea of (1) how to build houses . . the idea of nets . . came to mind a few times . . nets are like strings or . . uh . . things that are string-like and you can 'tie' them together . . 'tying' things together is such an interesting operation . . when you 'tie' things together . . algebraically . . that can mean for example . . even . . hitting a nail into a piece of wood . . by using a hammer . . by using a nail . . that is like a knot system on its own . . isn't that impressive?

You should be impressed . . if You're not -_- I am disappointed in you and you should read and re-read that paragraph above this one repeatedly for many rounds of knotty expedition.

Knots are like . . try to imagine that you are a cloud . . then how many ways can you move? . . well . . you can move left and right . . which is like . . one type of knot . . and then you can move up and down . . which is like another type of knot . . and then you can move in all those minuscule ways that physicists would be interested in such as inward explosions of cloud particles and outward explosion of cloud particles and variations of the two and linear combinations of the two and area adjacent interesting spacings of the two and variations of those and so on and so forth . . look how many knots there are for you to explore how clouds can be tied . . 

### (2) What is not a knot?

Clouds are knots. People are knots. Everything is a knot.

Right. Well for now in my own personal exploration of the subject . . it's tough to describe things that are not knots.

Right? Well please also notice that I have only been studying this subject for less than 1 week since this message was written on today's date of June 28, 2021. Or June 28 A.D. 2021. I wrote this message on June 28, 2021. I have been studying the topic of knots and how they relate to a lot of different ideas . . 'so-called everything' -_- which is not really my own personal thinking to say 'everything' is a knot but it's more like a strategy to get you to appreciate knots more and more if that would help you use them in your own studies.

But think about it. Over and Under. Under and Over. If you learn basic knot theory you will see strings of wire. Strings of wire or strings of cotton thread. These things are interesting how can they go to and from theory and practice and still be practical in both domains?

Invisible Technology.

Invisible Technology. Is like forgetting. Forgetting is like not being able to imagine. Not being able to imagine is like an "under" operation in knot theory right?

Over operations means things are possibly still "knots" Knots are still like "knots"

If you can "see" something just because it is "over" another "section of the string" it doesn't mean that it's still not a knot and so that there are in fact things that are hidden from view.

Things that are "under" make themselves apparent to us as English readers as things that are "invisible" or "not apparent" and yet things that are "over" can possibly be interpretted (and that is a good thing) It's not necessarily bad to interpret it either way as being visible or not visible but we shouldn't necessarily forget that even the visible things are "invisible" 

Do you remember that girlfriend you had. In school? Do you remember how she told you a secret on that one day? You saw her tell you secret and so it was like an "over" operation where it was like they knew something that you didn't know. The "showed" you something. "Something became apparent".

Why is that so weird to think that because she or your girlfriend told you a secret and so now she is all of a sudden telling you that "she" knows something. You are "knowing" something but is it really true? Isn't it still a knot? A knot may or may not be visible. The knot that you use to tie your shoe lace is a good example of a visible knot. And yet you don't want to tie everything just because you know how to tie one thing.

So why tie yourself to the idea that that knotty thing that your girlfriend told you is the only knot you need to know?

She told you one knotty knot. 

What a knot.

And so it's up to you to say if it's still a knot or if it's a visible thing that you think is real. Because in fact your girlfriend is possibly so knotty that she couldn't even tell you why she told you that thing that she told you. She may have held back the reason was because a teacher wanted her to tell you that. Or maybe a previous boyfriend would have wanted you to know that. Or maybe a previous person of interest would have wanted you to know that and so in their knotty mind they knotted the two ideas together.

What a knotty story.

Knots are like? The reason for this is? Well.

### (3) Knot Knot Knot?

The reason for this is?

Knots are a great introduction for you and your kids. If you have children (1) show them that knots are in a lot of places (2) show them how to tie their shoes (3) show them that their bicycle is made of knots like a pulley-gear system that 'ties' together the bicycle propeller machine (4) show them that their bicycle is made of knots also because humans are so knotted to them because their hearts are so used to riding on 2 nice wheels on pavements that support those bicycles (5) show them that their school teacher is knotty even after school because they are tied to (1) their finances (2) their own personal well being (3) sexy things like toys and trucks and cars and toy planes (4) internet technology that they knot throughout their daily lives

If you see a knot. It is cool. You can say (1) that is a knot that I noticed. Knoticed. I apologize for the knotty joke. Knoticed. Knowledge. Knot. Not. Knot. (2) Knots are things you can notice.

Christopher Alexander wrote a book on "Centers" and "Centers" are things that you can notice. But centers are really knows because you may not notice them in certain scenarios. Like the example of biological machines as possibly having centers came up in my mind while I was reading the book. A lot of biological machines are mysterious to people but knot theory can help 'unravel' that mystery. Mysteries are knots. Knots don't need to be 'unraveled' per se they only need you to call them knots and then maybe point out which things are over and under and how the knot is tied to other knot systems or something like that.

It's not a big deal to 'unravel' a mystery. It's more like (1) we hypothesized this thing (2) we did some knotty experiments (3) we let our knotty experiments and our theoretical thinking be the source of knotting together some leis and those leis were pretty much (4) what we told the public was the real world

The real world. That is really cool. It is nice to have a scientific discipline department. But it is knotty. 

Children who don't know science and technology can still get away with their knotty ignorance because they are still working out their own knots like what is mother and father? Why do you need to have gravity-knots towards mother and father? Gravity knots let you know that you're tied to a planet or an idea. haha do you like my gravity-knot hypothesization. It is a scheme that I developed to talk about "the law of attraction" or more accurately just "attracting" or "being attracted to things". It's not a real thing. It's just for fun. Sorry. I was being knotty. For heavens sake. These things are difficult to write. Don't you know how many knots I need to tie for you to understand thing. You are not only you the reader but also you have to think about those neighbors of yours who are knotty and read things differently from you. They will complain about (1) what is algebra? (2) this is boring to read (3) I need to masturbate (4) can I come back to this after I masturbate 20 times? (5) when I masturbate twenty times. Will that be enough to unknot twenty as 20 or tw-en-ty- as twenty and twenty and 20. Are those the same thing?

Twenty. Twen-ty is 20. 

How knotty.

You have to learn to read a lot of different languages like (1) 1 and two go to gether and (2) conspiracy theories exist (3) conspiracies are knotty and I don't want to take a knot theory class

Knot Now. 

. 

### (4) 4 or 5 unlikely parables walk into a room. One of them is a Liar.

4 or 5 unlikely parables walk into a room. One of them is a liar.

I was sitting in a room one day. The room was in the house. I was thinking to myself. Do you know what?

Knots are an interesting topic. But for some reason the name is too coincidental.

Knot. Not.

How coincidental that those knots be notted together.

Not really or knot so.

Knot sure.

Well the point of that message was then to introduce me to the question of "what if there is an alternative reality where there is another word for the word "knot"" and so knot theorists subscribe to using that different notation?

In that alternative reality world in my mind the phrase "Lie" came to mind.

But lie is like a lie. But if you fib a lot you might get away with it.

Fibbing is like "believing" is in your lie.

Lie.

Lie is spelled "L. I. E." but that is a lie when it comes to my alternative world "fib-truth" about the new word for "knot".

LEI. Lei. L. E. I.

It is pronounced "Lie" but spelled differently. Lei is a nice word but you pronounce it like "Lie" or "Lye".

A Lei group could be a thing to talk about knots. But I'm not sure. don't quote me on that.

Leis are everywhere. 

Knots are everywhere.

It's a lie to say lei.

So if you live in an alternative world you would lei with me.

Lei.

Knot.

Don't lei too much.


### (2) Graph Theory

Graph theory is a great course.

If you are a graph theorist. You want to ask questions about (1) forces and (2) pressures.

You are forcing yourself to read these words. But did you know there is a graph for that somewhere? You don't necessarily know what that graph looks like. It could be a lei.

It could be a lei.

Graphs are amazing. You are able to draw circles and lines and connect all sorts of ideas.

Circles are like knots.

Knots are knots.

Knots are knots.

Knots.

Lines are like well . . Those could be completely different from lines.

Those could be completely different.

One thing that is nice about lines is having (1) Invisible Lines. Invisible Lines are better to be visibly drawn using the "dotted" line notation. Anytime you want to indicate an "invisible" thing you can always use the "dotted" line notation.

Dotted lines are a nice way to indicate "ideas"

Did you ever have an idea.

If you have 2 circles drawn. Draw a dotted line between them. What a cool idea right?

Right.

Ideas.

Graph Theory is much more complicated than what I have introduced.

In traditional graph theory courses. You could learn about angles and proportions of interest like drawing different edges for the lines like for example drawing an arrow in one direction or in another direction or drawing multiple lines over the same single line or drawing multiple circles over the same single circle.

In traditional graph theory course you may even learn about proportions and sizes and how the human eye is so easily influenced by small perterbations of the changes of the sizes of the circles.

Circles are really cool. Different scales of circles are nice. Different scales of circles introduce to you as a mathematics student, different types of notation.

A lot of the things that I've written here are kind of hypothetical and idea-related and it's because personally I haven't taken a traditional graph theory course but have scanned through 1 or 2 books on the subject but not really accomplishing the courseware like doing the exercises or reading through the lecture notes.

I don't mind going back now to finish my graph theory education. Personally, I have been a little busy in my life these days. I have been working on other projects.

But graph theory is really still interesting. 

I don't know any graph theory professor who would acknowledge my personal knotes here as being anything notes here as being anything knotes here as being anything notes here as being anything interesting.

If these notes aren't interesting, that's because there are interesting knotes here that are knotes here that are knotes here that are knotes here that are knotes here that are bidirectional knotes here that are bidirectional knotes here that are bidirectional notes.


### (3) Light And How To Create It

Thanks for reading these notes.

Light is good.

Light is bad.

Light is an electromagnetic wave. It is good. If you like light. You cannot see light in your eyes but you can hypothesize that it's there with a mental mind instrument. It's knotty.

Knotty light.

Night knotty knice.

Knice Knotty Light.

Nice Knotty Light.

Two or 3 sentences ago I started a dictionary of words that are now in your mind in this sentence how is that possible. Did you read those words? Are you drawing light into those words using your mental capacity? Do you believe that there are mental light bulbs? Like sources of inspiration that are like 'dark mental mind light suns?'

Dark mental mind light suns which inspire you with new types of information like imaginary clouds of pulsed radar information. 

Well. Even if your mom isn't shown in a photograph in this text editor document that is okay because you can paste their face onto the canvas of this letter. here. h . and then you can be impressed by how well that person fit onto that letter because of your nice light graph in your mind. What a graph theorist you are. Did you know where that graph is located? Is it a knotty dark mental mind sun that shown the light in your face?

Shown the light.

Knotty.

Knotty light.

How Knotty the light.

How knotty the kids that play in the light.

How knotty the kids that play in the dark mental mind sun.

How knotty.

Knotty boys and girls.

Knotty insects.

Knotty poop filled reptilians.

Knotty apes.

Knotty lizards.

Knotty.

Knotty trees.

Knotty sun baskets.

Knotty rainbows.

How knotty.

Knotty knotty knotty.

Have you ever seen a great silver back gorilla on a rainbow?

How knotty.

. . 

This paragraph sequence will be written differently . . no more jokes . . 

. . 

Okay . . 

Light is good . . The sun is a good source of light.

Fire is a good source of light . . 

Electrochemical fires are a good source of light like fuels like oil and coal and wood and even new chemicals that you can make like acids that would heat and burn plastics as though fire were there.

That is so cool to have REAL fire. REAL fire is really weird. 

Algebra was one of my first inspirations for looking at fire.

Fire is like (1) wait how does the world work (2) if you apply algebraic theory to a lot of things then (3) eventually you find that some things don't fit into your theories (4) the algebra doesn't make it possible for you to predict that that thing should be there (5) the predictions rely on you believing in aliens or gods or something mysterious going on that makes that thing possible. Or maybe a bachelor's degree in chemistry or physics would possibly help as well [slash-shrug-emoji] 

I'm not sure. If you look at the world. Algebra is fun to apply.

Do you want to learn algebra with me?

This will be a very quick course of Algebraic theory. You will have to learn how to think like me to see why fire is interesting.

Fire is interesting. But I didn't start there. For some reason. I was just a punk kid walking around in my mind until I ran into the weird thing that "hey, what's that?" . . and then all of a sudden . . applying my algebra calculator over and over I thought to myself "I didn't learn that in school" . . "that's weird" . . 

So we will give you a "algebra" that will help you apply algebra in your life as well . . 

(1) algebra is 1 + 1<br>
(2) 1 + 1 is . . 1 + 1<br>

. . 

1 + 1 is weird.

Is your intuitive answer 1 + 1 = 2 ? 

That's not a bad answer.

But check this out Dr. Hello World.

You need to sample new ideas.

Turtle + Turtle.

What is that?

[pause for 10 minutes]

alright I didn't really pause for 10 minutes but it was to say how awkward it is to explain to you that now you are a new mathematician . . 

Mathematician . . needs new brains right?

. . 

we are replacing your natural intuition with new intuition . . 

. . 

Turtle + Turtle = Lizard

Did you agree that is a good communication message?

Yes. Awesome. That is really weird for you to agree. But thanks for agreeing. 

Now I will tell you why it is weird for you to agree.

(1) Do you have a cell phone?<br>
(2) Do you know how to use Google?<br>

Then try to search the answer for "Turtle + Turtle"

Is the answer "Lizard" like our algebra supposes?

. . 

Turtle + Turtle = Snake

Is that a better answer?

I think you should agree. It is not a better answer. It is just an answer that you made up. Yes that's right. This is just one of those knotty stories.

. . 

Turtle + Turtle = Stream

Is that a better answer?

That could work out.

. . 

Our algebra is really cool. If you believe you have an algebra that you like then you can get away with a lot.

. . 

1 + 1 = 2<br>
2 + 1 = 3<br>
3 + 1 = 4<br>
4 + 1 = 5<br>
5 + 1 = 6<br>
6 + 1 = 7<br>
7 + 1 = 8<br>
8 + 1 = 9<br>
9 + 1 = 10<br>

. . 

Those are all really cool stories. But check it out. You can tell more cool stories.

1 + 2 = 3<br>
3 + 4 = 7<br>
7 + 8 = 15<br>

. . 

Those are all really cool stories as well.

Isn't that the coolest idea.

. . 

Algebra lets you (1) have a story (2) correct your story to make sure there are no mistakes (3) share your story with other people

. . 

1 + 1 = 2<br>
2 + 3 = 5<br>
5 + 8 = 19<br>

. . 

Did you find a mistake in my story?

. . 

I hope you found that 5 + 8 is not the story that is accepted by other algebraists . . 

. . 

5 + 8 = 13 is a good story

. . 

Alright

. . 

Advanced Algebra is something different . . if you take (1) Abstract Algebra you will learn about 5 + 8 ideas not resulting in 13 which is like symmetry groups or something like that and uh I guess + is an operation the "group" "additive" operation or something like that and it's uh not uh the same uh for all groups or something like that . . and so uh . . yea . . uh . . well . . yea . . something like that . . I think Abstract Algebra is nice so you can consider taking those courses if you want to learn more about that topic . .

. . 

The algebra we are talking about relates to other types of things being used as algebraic notation. Like trees and trucks and trains and bicycles and other things as well . . 

. . 

Now we shall move on . . 

. . 

Light And How To Create It

Isn't that interesting?

Our chapter is about "Light" and already we had to involve "Algebra" for you to learn about.

I'm sorry for such a long or lengthy introduction so far. Light is interesting because of fire.

Light + ?? = Fire

What do you think ?? is ?

?? is my interest . . my passion . . my thinking? . . it could have been something else at the time as well . . but I'm not even sure if this is a nice algebraic equation . . 

. . 

Fire + Toilet = No Poop

. . 

Ahh I remember now . . I had the idea to use (1) fire to burn poop (2) make sure you burn all of the poop and repeatedly in many cycles of fire (3) no poop means you don't have to have large sewers for poop. Instead your poop coul theoretically be so clean of poopy molecules that it is just like sawdust from cutting trees.

How cool would that be to not have poop in our communities. Do you see the algebra involved in that?

No Poop = No smelly stuff<br>
No Poop = No naughty language (shit talking)<br>
No Poop = No making fun of people for having butts<br>
No Poop = Wow, what does that mean for society? Can you imagine?<br>

. . 

Do you see the algebraic potential of the no poop society? It's so cool. It was (1) I was interested in poop and making new toilets (2) toilets that use fire were an idea that came up after using volcanic magma after many days and weeks and months of exploring this idea. Months of exploring this idea and years of personal life experience of pooping and pooping and feeling good while pooping but also pooping.

Pooping isn't terrible but it's like . . Toilets are really innovative. How cool is it to be the toilet creator. Everyone loves that device. 0 customer complaints unless the toilet is backed up. It's like a great computer service.

. . 

People must really like pooping if there is war and bad behavior. We must really like throwing smelly stuff at one another. Poop is like wow. People must be mad if they don't even know their breath smells like poop. They smell like poop and no one even tells them that. There are sewers on the planet and no one even tells you that on day 1 of kindergarden. There are places for your naughty food particles to go after you dress your body with new fats and macronutrients. Macronutrients that are nice gowns and dresses but then the left overs go to the scrap yard.

Scrap yards. Where no one even recycles the scraps? Seriously? Poop and no even big biological organisms to eat up all the poop and re-use it into the world's economy. Dinosaurs that eat poop would have been innovative for any junior biological engineer.

What can of algebraic math system is this? [glasses-pushed-up-with-2-fingers-emoji]

Is that important to have more important math systems that respect our need to smell less poopy poop in our society?

Well it's all forgivable if there is poop people to come by and smell the poop for a few hours and ask the question why am I looking at this poop and studying it? Do I want to have a disease? [glasses-emoji] [shining-glasses-emoji] Do I want to have penetration by weird social jokes? Will people think of me like a weird person for tasting and smelling the poop so closely with my own physical nose? My own breathing apparatus and not my daughter's? My own breathing apparatus and not my son's?

Why should my children know I am down here in the sewers penetrating these drugs of nice chunks of poop with my intellect and reason and big nice thoughtful ideas.

Why are there algebraic systems that are invisible to people?

Is this smell really good to keep down here for many years?

It must be okay. It has only been recently since humans evolved from monkeys on the plain. It is a reasonable system.

Monkeys on the plain must be not sure where to keep their toilet supplies.

Monkeys are easy to fool.

But now we are in the future. Fire must be a good tool.

Fire + Poop = No Poop

. . 

So then why is there poop ? I don't know. Well . . Algebraically that tells you One important thing. Algebraically we need to ask . . why is this variable in this equation very important? . . From Bad stuff to No Bad stuff. Which variable is responsible for that? . . Bad Stuff. To . No . Bad . Stuff . 

Hmm . . Let's use our magnifying glass to inspect the equation. The magnifying glass is full of nice ideas like knots that let you bend light to look closer and closer and closer to nice ideas that are knotted in nice ideas.

Nice. [sun-glasses-emoji-but-there-is-no-sun-glasses-basically-normal-glasses-or-invisible-glasses-emoji]

It's not that bad. It's just not that bad. If we have special sunglasses we can evaluate what is the source of our magnifying glasses.

If we have special magnifying glasses. We can investigate what is the source of the nice No Poop hypothesis.

No Poop only comes at an expensive cost. It cannot be there is really no poop. Please tell us what type of detergent you are using.

No poop is very expensive.

(1) We are using fire

Fire? Really. That is weird. How come fire works?

(2) We are not sure

Not Sure?

(3) Yes

That is really weird.

Are you sure? Or Knot [sun-glasses] sure?

"We are knot sure"

"Are you sure?"

Kno

. . 

Kno

or You Know?

Kno

. . 

Knot sure

. . 

Well that is quite a long theory. That is a lot of explanation for someone who doesn't know anything about (1) fire and (2) how to find inspiration . . 

How to find inspiration ? 1 . . you can use an algebra system . . and 2 . . you have inspiration already . . the FIRE IN YOUR HEART . . 

The FIRE IN YOUR HEART . . 

And then you use algebra to find new corners of ideas like (1) is that what we need? (2) is that needful? (3) do we like that?

. . 

Wow. I'm glad you read this far. But it is not expected of you to really get these ideas. (1) Algebra is cool but you still don't really know how to use it . (2) Fire is cool but it was like a weird idea that only helped me stop poop from existing. 

(1) If you need an algebra that helps you stop poop from existing . . poop . . then . . maybe . . fire . . real physical real world fire . . and not necessarily its algebraic equivalents like soundtracks or music albums . . 

(1) If you need an algebra that helps you stop poop from existing . . poop . . then . . maybe . . fire . . real physical real world fire . . would possibly help you . . 

(2) fire is really cool . . Personally . . I am still studying it . . 

If you really like the algebra that we introduced . . (1) try to find other fire equivalents . . for example . . is your (1) friend fire ? (2) girlfriend fire? (3) music album fire? (4) high school experience fire?

And what kind of poop would have those types of fire helped you eradicate from your experience ? . . 

Alright . . 

Fire + Poop = No Poop

. . 

Fire + Poop = No Fire [this was an accidental typing] [O_O accidents are weird] [then I erased the previous line and wrote it correctly like I intended "Fire + Poop = No Poop"]

. . 

Language is nice. I guess that's why "fire" is a common expression for "good" things in society today right? So it's like an "algebra" system right?

. . 

Fire + Rice = Cooked Rice<br>
Fire + Veggie Burger = Cooked Veggie Burger<br>
Fire + House = Warm House<br>
Fire + Food = Cooked Food<br>
Fire + Idea = Delivered Idea<br>
Fire + Water = Boiled Water<br>
Fire + Cities = Burned Cities<br>
Fire + Witch = Burned Witch<br>
Fire + Tent = Burned Tent<br>
Fire + Metal = Burned Metal<br>
Fire + Sand = Computer Chips<br>

. . 

Computer Chips run our modern day society.

Do you see how fire is so important?

. . 

So How Do You CREATE Fire?

(1) Oil<br>
(2) Wood<br>
(3) Coal<br>
(4) Flint Stone<br>
(5) Lighter<br>
(6) Stove<br>
(7) Electric Pump<br>
(8) Electric Outlet<br>
(9) Bicycle Gear-System + Flint Stone<br>
(10) Bicycle Gear-System + Knotty Ideas<br>
(11) Friction-Based-System + Knotty Ideas<br>
(12) Friction-Based-System + Dry Leaves<br>
(13) Friction-Based-System + Paper<br>
(14) Friction-Based-System + Paper + Oxygen<br>
(15) Friction-Based-System + Hydrochemicals<br>
(16) Friction-Based-System + Hydrochemicals + Oxygen<br>
(17) Bicycle Gear-System + Copper Wire + Lamp<br>
(18) Sonoluminescence<br>
(19) Sound Friction Systems<br>
(12) Flint-Sand-Paper + Good Substrate + Rubbing<br>
(13) Flint-Sand-Paper + Rock + Rubbing<br>
(14) Static Electric Pressure From Rubbing Your Feet On A Carpet<br>
(15) HydroElectrically Dense Material<br>
(16) Castle-In-The-Sky Electronics<br>
(17) Sunlight Concentrated To Fine Points<br>
(18) Sunlight + Fresnel Lense [1.0]<br>
(19) Sunlight + Fiber Optic Cables<br>
(20) Sunlight + Knotty System Cables<br>
(21) Sunlight + Systems of Unbelievable Symmetry<br>
(22) Sunlight + Systems of Unbelievable Symmetry like Christopher Alexander's books on "The Phenomenon of Life" and the "Theory of Centers" and "The Process of Creating Life".<br>
(23) Sunlight + Systems of Unbelievable Symmetry that you can learn from other places. Not Just Knot Theory Ideas but Other Specific Things To Make Your Knots Visible.<br>

Sunlight Systems + HydroElectrically Dense Materials could be a really good future that is (1) free from coal fumes and (2) free from carbon emission fumes

Sunlight Systems + HydroElectrically Dense Materials like Fresnel Lenses that concentrate Fire are really cool.

Fire is so cool.

You can have other fire systems like

(1) Light Radiation Capturing Devices<br>
(2) Animated Knot Systems That Share Certain Ideas Between Their Capturer and Their Maker (The Maker is a Strange Idea we Can Talk About Later) (The Capturer is an idea bout the device itself)<br>
(3) Animated Knot System that share interesting ideas<br>

. . 

If you imagine a sheet of grass. Imagine that the grass is able to collect sunlight on the sheet of the surface of the grass. That sunlight can then be folded into a nice knot that we call a "hydroelectric" "bond".

Hydroelectric bonds are like "static" "fire" "instruments"

"Static" "Fire" "Instruments" are all around us.

Place a fire emoji on all the things you see around you. Place a fire emoji on your computer. Place a fire emoji over the water bottle that you see before you so that you know it was shaped and melded by a heated fire system.

Fire is all around us. The fire is still latent but is ready to be active in the form of "hot fire blades of gusty air that is colored"

"Hot gusty colorized air" is a fire that is familiar to many of us.

Fire.

Hot.

Hot.

Hot gusty air that rises from the pavement of a hot dusty desert road.

Hot dusty desert roads.

It is really cool.

[1.0]
Fresnel Lens Solar Power Foundry Obsidian Farm 3800 ˚ F 2100˚ C Fresnel Optics greenpowerscience
By GREENPOWERSCIENCE
https://www.youtube.com/watch?v=drE54ctrHBY


### (4) Light And How To Find More Of It

The Pleiadians know where to find fire. Go ask them. Or go ask a spiritual teacher.

Pleiadians are aliens that visit the planet from time to time. You need to ask them if they will help you find your new fire.

If you can't find your fire, you may be disappointed that someone else in the community has it but they need to do their studying or reading or do something that encourages them to work on their . . fire . . so to speak . . 

But fire itself exists in many forms . . remember . . a lot of the things around you are latent fire components . . the paper you use is used with fire materials that helped produce it . . paper had to be made by machines that were made of metal . . the metal was molded by a computer that used fire to heat the metal to a temperature where it could bend nicely to fit a certain shape and material style pattern.

Metal.

Metal is a really true and cool fire component. The reason our society is so great today is thanks to metal. Metal makes things really easy to keep up straight.

Metal holds people spines in certain components. Metal is a trusty device for beating people up.

Metal helps police officers do their job. Job ready metals are ready to pierce the skin of any untrustworthy pedestrian on the streets.

Job ready metals are ready to be nice katanas for Japanese anime fans to look cool.

Metals make wood instrumentation or wooden instrumentation like houses and bridges and buildings made of wood, very easy to make. Metal makes rubber tires easy to have a nice molded shape to bend to.

Metals make books possible because machines that cut the tree to have saw dust or wood or maybe even other elementary components in the world like plant dyes or plant particles are things that metal utensils help you acquire. Saws that cut trees are strong. Saws that cut wood are strong.

Metal is a nice place even to hold fire. If your metal is strong enough, it won't melt from the fire. 

Metal is nice.

. . 

Cement and concrete are poured into nice metal places . . 

Metal keeps the cement and concrete interesting . . 

So if you remember your high school where the cement floors were nice and polished and glossy, you can thank your nice enthusiastic friends who use metal polishing tools to squeeky clean polish the floors of your new school building.

. . 

Fire is good.

Metal is good.

Time is great.

Animations are demon.

. . 

That is the order of events in my timeframe brain.

Fire is okay. Metal is a cool fire idea. Time is really keen to kill and let metal exist. Animations are demons.

. . 

Did you know that outside of our planetary environment, that cartoons like "Foster's Home For Imaginary Friends" Exists? If you didn't know that. It's because you didn't use your "imagination"

Imagination is a key fire.

Imagination makes it so that "artists" are the future engineers of our society.

Imagination means that you can make anything you want. Just make sure you knew it was a creative task and that it's not necessarily the best. But a nice work of art just like visiting a friend's birthday party is a nice work of art even if there are technical engineering things related in the task.

Technical Engineering things related to the task or in the task are not necessarily what people care to talk about at the birthday party.

People want to talk about "gifts" and "who received what?" and "wow, what a memory"

It was a great time.

Animations are like that. "What a great time"

"What a great boyfriend" "What a great husband" "That was fantastic sex" "What a culture" "What a stupid idea" "Have sex with who?" "Don't have sex with who?" "Why didn't you recommend them?" "Why didn't you recommend having sex with them?" "That's weird that you didn't let me try some of them when I could have had the chance" "That's weird that you didn't even think of my name when you could have had the chance"

Artists are weird. "I think I could have had that Idea, but I didn't have the time to"

So they are stupid. And hypocritical.

Timing is weak. If you live on another planet. You're like "times?" "I'm an artist" "I create timelines" "With my paintbrush"

Timelines with my paintbrush. What an artist.

I was making a joke . . Timelines with my paintbrush is dumb rubbish ideas that you shouldn't believe. It is weird oatmeal that you don't need to eat.

Artists are dumb.

Timelines are stupid.

You're stupid.

You think this is a good knot.

You're dumb.

[?]

You're dumb.

You don't even know where I meant to place that question mark.

It's like that timeline exists but it's not visible to you right? right? Right?

You're not that dumb. You just need a knotty not to knot the knot that you need.

You want answers about life. Create them. Is that the word that "Spiritual Gurus" teach? "You create your own reality" -_- personally this sound advice has been awry for me -_-

But it makes sense if you have light-based computer technology since you can masturbate in sound rooms that no one has ever heard of.

It would be nice if you placed your hands on this technology wouldn't it be?

Thankfully. You are reading a manuscript about that idea.

. . 

(1) Light<br>
(2) Metal<br>
(3) Timing<br>
(4) Animations<br>

. . 

Animations are different from timing.

Timing is like 1, 2, 3, 4 . . and count on ward and for ward and on ward and for ward until you keep counting . . 

Timelines are like wait a minute . . Is this still 1 ? Did I start over again? . . 

1 : <br>
1 :<br>
1 : <br>
1 : <br>
1 :<br>
1 :<br>
1 :<br>

How many different ways can you spell 1 ? . . 

1 : One<br>
1 : Won<br>
1 : Won-[silent-e]-e<br>
1 : Warn [silent-r]<br>
1 : Twelve Minus Eleven<br>
1 : Six Minus Five<br>
1 : Twelve Minus Fifteery Plus Four<br>

. . 

All of those are quite weird.

An programmer like you can evaluate each of those function expressions and be like "hell yea" . . 

But "hell yea" means you're also dumb . . because realistically it should be spelled like "Six Minus Five" right . . a teacher would be annoyed to read that rubbish nonesense . . that is a pun you made up to be a clever nonesense nonesense calculator . . 

Stop calculating nonesense . . that is the basic idea . . I think you should be fine . . but it's not a real world . . 

It's just like you think it could work . . right ? . . 

Well . . timelines are like that right ? . . a red shirt doesn't seem harmless but if it has a certain symbol on it . . like . . for example . . you're on a planet where red shirts are banned FOREVER . . then . . you're going to be really in danger . . those are not good symbols . . 

Symbols where you are being warned of your behavior are amazing . . it's like . . "yea" . . "that's a timeline" . . "right" . . "red lipstick" . . "red car" . . "red house" . . "banned" . . 

. . 

Well . . what does all of this distracting nonesense have to do with "timelines"

"timelines" . . are like . . well . . that's "what I believe the time is" . . "do you have the same clock?"

"yes, I do have the same clock. That shirt is in fact red."

"yes, I do have the same clock. That video did in fact last 1 minute and 30 seconds."

"yes, I do have the same clock. I did in fact type you this message and not use my vocal chords."

"yes, I do have the same clock."

Ahh. If clocks are out of sync, then you need a community (1) leader to vote to say that clock is indeed wrong. or (2) have every submit their times and then vote to see which one is the best good time . . or median time . . or middle time . . or a time that everyone can agree on . . right ? . . shout out to Dr. Leemon Baird . . for their work on the Hashgraph Algorithm . . since median timestamps really inspired this message . . this message note here (2) . . yes . . that time . . (2) . . (2) is the time that was inspired partially by the message that was brought by The Hashgraph Documentation on the Internet . . Median . . timestamps . . median . . 

. . 

Median timestamps . . are good timestamps . . wouldn't you agree that the middle number of a sequence of numbers a good timestamp? . . 

[1, 2, 3, 4]

2 or 3 is the middle number, depending on choosing left or right when you have 2 items in the middle.

[1, 2, 3, 4, 5]

3 is the middle number . . which means it's quite good . .

. . 

Okay well median timestamps are really new to me but it does seem like a cool idea . . 

. . 

median means middle . . and so middle means you can still determine which type of middle you mean . . and so . . we have another timeline to think about right ? . . no not really . . a sequence of numbers is great . . if we have a finite list of numbers we can almost always come up with the middle as being 1 or 2 numbers . . 

So then we can be proud of ourselves . . 

. . 

Median timestamps . . the middle number . . It's like having a list of books and you don't know which one to read . . and so you sort them in the order of different properties like (1) best reviews (2) friends would recommend (3) people in the general public would recommend but they didn't write a review for some reason (4) alien advisors in your head are beaming down information to you on how to order the list (5) ghosts are giving you an order on how to arrange the list

. . 

Okay . . that is really weird . . 

. . 

(1) Light<br>
(2) Metal<br>
(3) Timing<br>
(4) Animation<br>

Timing and Animation are not the same.

Imagine this . . Timing is like 1, 2, 3, . . and so you continue counting like you wish.

But for some reason . . you don't need to count anything in particular . . 

(1) You can be a noob and count seconds<br>
(2) You can be an even bigger noob and count hours<br>
(3) You can be a nooby noob and count days and weeks and months<br>
(4) You are a noob if you're not counting historical inventions that radicalized history<br>
(5) You are a noob if you're not talking about hitler's plans working out and no dark skinned people exist and other algebraic populations of inbetween populaces that would have gone extinct from other algebraically related genocide algebra rings<br>
(6) Can you imagine no dark haired people and only blondes living on the planet? That's a crazy timeline<br>
(7) You are still a noob if you are counting the number of steps you take in a day<br>
(8) You are still a noob if you count the amount of money you earned for that day<br>
(9) You are a newb if you think you read that document<br>
(10) You are a newb<br>

. . 

Timing is like . . you're a newb . . 

Newbs are like Time . . 

But imagine this . . Time . . how cool is that . . Am I right kids? . . I'm gonna hand you a sheet of paper [person-who-is-in-a-dark-jacket-like-they-are-selling-you-drugs] and I'm gonna talk to you for a bit . . is that okay ? . . 

And so you say . . yea . . that's kind of weird . . I don't know if I want to talk to some person in a dark jacket who looks like they are leaning into the conversation so much . . are you going to sell me drugs? . . 

Well . . I'll tell you want . . I want you to imagine this sheet of paper is a rock . . 

A rock? . . that doesn't make sense . . that is a sheet of paper . . 

Well . . if you imagine it's a rock . . that look . . ahh . . you would have gotten it right . . Am I Rite kid?

Of course you're not right . . you turned the paper over and showed me a picture of a rock . . that's not a real rock . . that's just an IMAGE of a rock . . 

Image . . that's right kid [squinty-face-emoji] . . IMAGE . . that's correct . . you must be a MATH-Eh-Ma-T-I-Ci-an . . yea . . 

[lame-face-emoji]

I don't know if I'm a mathematician dear sir but I don't know if my mom would like me speaking with strangers . . 

Your mom? . . Your mom . . Is . . 

Listen . . kid . . I don't want to talk to you for very long . . I want to get out of here . . 

Yea . . But what would your mom say if you missed out on a good history lesson?

A good history lesson? 

Yea. . . . . . 

History?

Yea . . . 

I thought you were talking about math . . and images . . 

Images are like mathematics that are projected onto historical contexts . . 

Whatttt?!?!?!?!?!?!?!?!?!

Images are like mathematics that are . . 

Whatttttttt ? 

Images are like . . 

Whattttttt?

Whatttttttt?

Yea . . 

Historical context

Images

History

Image

. . 

What . . 

. . 

Image . . 

And so the rock is real ? . . Yea

Yea

Yea

Yea

Yea

Yea

Yea

Yea

Yea, but you have to believe it's real . . 

yea . . 

yea . . 

yea . . 

So this is a real story?

Yea . . 

And that is a real boy?

Yea . . 

And that is a real stranger?

Yea . . 

And that is a real joke?

Yea . . 

Wow . . 

That is a weird image . . 

. . 

How much time do we have . . can we make another image?

No . . 

Hahaha . . I see . . 

. . 

:O

That is the end of that message . . 

Okay but the idea is that animations are like interesting ideas where "images" represent the idea of portals . . and so for example if you should a photograph of a duck to a duck . . what do you think they will think about that photograph? . . Will they love that duck? . . and If you show the same photograph of a duck to a human being . . what do you think they will think ? . .

What do you think a gas cloud in the Magelantic Ocean thinks about a "photograph of a duck"?

What do you think a person whose job it is to find ducks will think about the duck?

If it's your job to find that particular duck, then is it important for you to program yourself to ask questions like "where did you find that duck?" and "can you tell me his name?" and "can you give me a reason to ask that duck to marry him?" "can you give me a friend list for that duck?" "what is the social contact information for that duck?"

Why is a duck such a weird creature to replace with a human being in that previous analogy . . Is it so important for you to be a bounty hunter for certain ideas . . to program yourself to ask questions about those ideas? . . 

That is my Physics PhD Research Topic . . of course I'd like to find that duck . . I need them . . I love them . . I need to give them resources . . I need my professor to believe that I did in fact look for them . . and I gave them a handshake for the technology they provided . . and I gave them a good smile . . and we took photographs together that I showed my wife or my husband or my spouse or my partner . . And that was the end of my research topic . . 

. . 

Animations are so cool . . 

. . 

Timing is different . . 

. . 

Animations are like tangent approaches to timelines . . where if you watch a television show . . you realize that those spongebob characters are also watching you . . and asking questions like "do you think they like the show, Patrick?" . . or "hey, Mr. Squidward . . why are you sitting on the couch watching the show . . you know you need to pick up your kids from day care school . . c'mon . . "

. . 

Animations can be spooky . . it's like O _ O I'm being watched ? By who? . . "The army?" . . Why?

. . 

Writing something down today is interesting. Did you realize you can have an idea but you don't know how to implement it on "day one" . . you may instead say . . (1) alright . . you know what . . I'll write it down . . (2) then 2 months later . . you find someone who knows someone who knows someone who knows a ghost who knows another ghost who knows how to speak to ghosts and all your friends crowd together and tell you slippery small secrets about what to do and what not to do . . 

What an animation . . 

What an animation . . 

Light Animations are spectacular . . <br>
Metal Animations are spectacular . . <br>
Timing Animations are spectacular . . <br>

Why did we knot these ideas together . . that's dumb . . yea . . 

. . 

Light Animations are spectacular . . <br>
Metal Animations are spectacular . . <br>
Timing Animations are spectacular . . <br>

. . 

Hey did you know that animation knots are a thing . . knots that change in various interesting ways . . 

You and I need to talk about (1) animations and (2) knots . . 

Knots are like invisible ideas . . but the natural intuition you can get from a knot theory class isn't bad . . and the previous explanation that was provided in this document isn't bad either. But in general you don't need to try very hard to learn "over" and "under" have meanings that are algebraically tangent and available to things like "visible" and "hidden" respectively speaking . . 

Knots are cool ideas . .

Animations are a cool idea . . 

Animations have been personally difficult for me to memorize or grok or really have an understanding of . . a lot of it has to do with letting ghosts type for me instead of me really understanding "what is going to happen next" . . but because humans are really interested in "what is going to happen next" then it's like . . "let's talk about "animations"" . . 

Animations can be new for the viewer . . and not necessarily new for the developer . . if you have seen cars move before that is not a new animation for you . . But if you are a first-person century kind of person who has never seen a car move before . . that is a hilarious discovery . . "well . . would you look at that ! . . "

"Well . . would you look at that ! . . "

"My wheels are turning . . "

"Wow . . thank you so much for that idea . . "

"My wheels are turning . . "

"By golly . . I think I got it . . "

That's so cool to show someone an animation they've never seen before . . I think that could even be why teachers in class that are really enthusiastic are really just so cool to watch . . Look how ready their presentation is going to be for new animation watchers . . 

Animations like cartoon animations give you nice intuitive vibes when it comes to the topic of "animations" . . "Ahh yea . . like . . cartoons . . right? . . "

Knots and Animations are interesting . . basically maybe the future will involve some type of "finding knots that are animated" or "animated knots" and so you can "move things" or "keep things in the same place" or "push things"

(1) Push things<br>
(2) Move Things<br>
(3) Keep things in place<br>

Knots are good at those 3 things . . Wood is made of knots . . all of those chemical bonds . . So cool . . Metal . . is made of knots . . all of those chemical groups . . so cool . . 

Metal . . Wood . . Light could even have invisible knots that people don't know about right? . . They are knots after all so we can ask questions like "What do they keep in place?" "What do they move" or "what do they help move?" (like a truck that has a knot to a trailer behind it and helps pull things) and "what do they help you push or pull?"

(1) Pull Things<br>
(2) Move Things<br>
(3) Keep things in place<br>

I think Pull was the original terminology I used when taking notes in my physical paper and pencil notebook on this topic but that is up to you to knot that idea together . . why switch and turn? . . 

Pull Things is like pulling a car behind your truck because you have a nice knot to tie the two together

Move Things is because you can move things like cars and even houses because of that idea of knots

Keep Things in place is because you can keep a house in place because knots like nails and wood (which is made of knots) and a foundation made of cement (cement is made of knots like chemical bonding groups)

Knots are so cool.

Knotty surfaces like roads and desktops are really maybe not always considerable as knots . . but they do give you the idea that they are "keeping something in place" and so if you get the idea that "hey, these things are being organized in some way . . " then you might give yourself the idea that "these things are like knots . . right ? . . "

Knots . . 

Keeping things in place . . 

Notebook paper is like a knot system that keeps lead together . . 

Notebook paper is like a knot system that keeps crayon ink together . . 

Notebook paper is like a knot system that keeps pencil markings together . . 

Notebook paper is like a knot system that keeps other things together . . 

. . 

Your ideas are like knots that hold things as well . . 

What type of things do you hold together in ideas ? . . If ideas are like notebooks . . do you know what type of marker you are using?

. . 

Is it a brain? Is it a computer? 

. . 

Is it light? Is it dark mind mental fire? Is it a ghost? Is it another knot system that you're still trying to unravel or learn about what types of overs and unders are there?

. . 

(1) Light Animations . . <br>
(2) Metal Animations . . <br>
(3) Timing Animations . . <br>

Light Animations are like real life right?

Metal animations are like "cars" and "trucks" and "trains" and "boats" and "cell phones" Right?

Timing animations are like "smiling" "winking" "handshaking" "saluting"

. . 

Can you make a "car" "salute"?

Can you make a "truck" "salute"?

Can you make a "train" "salute"?

Can you make a "boat" "salute"?

Can you make a "cell phone" "salute"?

. . 

That is a weird analogy to make for some people. Saluting is possibly generally referred to be human beings. If you are smart now. Which I don't really expect you to be. You will know that these are good examples. Good examples because (1) they are unfamiliar to you. (2) You are not smart.

Smart is cool. But look how lucky it is that you have a bicycle or a car or a train in your society and you don't even know how to make one yourself. You are a lowerse. A common loswer that reads words on the internet and doesn't even know what they mean.

A common loser. A common loser. What a loser.

Losers keep losing. You're just a common loser.

Now get your pencil out and draw a (1) car saluting.

(2) Draw a truck "celebrating"

(3) Draw a train "masturbating"

(4) Draw a boat "cheering"

(5) Draw a cell phone "smiling"

. . 

If you actually did that exercise. And that's really awesome that you did.

That's really awesome if you did. No pressure.

(1) Car Saluting. Try (1) Imagining that you are a car. Don't be a common loser and put hands on the car. (2) hands are for simple people. (3) hands are for simple losers that don't know that cars have feelings too (4) "duh-duh-da-duhhhh" "duh-duh-da-duhhhh" "da-da-da-daaaaa-daaa-daaa-daaaaaaa-da-da-daaaaaaa" (5) A car doesn't have hands so just try a childish example like using the honking ability to make honking sounds that sound like a "salute" animation. "Saluting" could possibly be translated to a "language of respect" and so "salute" is like "I respect you" and so "honking" in a particular way like a song pattern that people are familiar with and associate with respect could possibly be something that is like a salute. <br>

(2) Draw a truck "celebrating". Try (1) Trucks have buckets. (2) Buckets look like they could be used to hold something. (3) Draw a nice piñata in the bucket trunk of the truck and maybe that is a nice drawing. <br>

(3) Draw a train "masturbating". Try (1) How do trucks feel well? (2) They could honk . . or press the pedal to the floor . . (3) There are all sorts of ideas . . maybe turning the steering wheel could feel good for a truck . . <br>

(4) Draw a boart "cheering". Try (1) Draw a boat shooting water up into the sky because of the tail rudder or something like that<br>

(5) Draw a cell phone "smiling". Try (1) Maybe the cellphone receptivity is good. (2) Draw 5 vertical bars that are increasingly tall and active to represent a nice active cell phone network service.<br>

. . 

All of these examples should be not so bad . . 

They are really examples of (1) what you could have seen (2) what you didn't necessarily need to see (3) those invisible knots that are now more clear to you as visible knots

. . 

The knots that you know are really cool because then you can use them to build things . . 

. . 

Artists use paintbrushes because those knots are so familiar and useful . . but of course they need engineers like you and me to build new knots for them to work with . . 

Knots like computers are complicated . . 

. . 

If you know how one knot works, maybe you can put it together with another knot and see what's "held in place" . . 

If something stays in place . . you may be interested in "keeping it in place" in another place like another engineering project . . 

Paint that stays in place on the canvas is so cool . . Keep in mind . . not artists care for their painting to dry away so quickly . . 

Some artists might really love for "delete-now" paintings like watercolor style paintbrushes but that is a secret . .

Delete-Now paintings like . . Create-Then-Delete-Create-Then-Delete are really special types of Knots that are like . . Animation Knots that keep changing the Over and Under specification ruling . . and so it's like they can't knot make up their mind Am I Knot right?

. . 

I think there is a knot in this chapter. I am sorry if it was confusing . . 

. . 

Light And How To Find More Of It

. . 

(1) Aliens can help you<br>
(2) Light<br>
(3) Metal<br>
(4) Timing<br>
(5) Animations<br>

. . 

I think that is not a bad list

. . 

Keep in mind that timing is very good for creating simple animations like (1) build that computer today (2) hammer that metal today (3) hammer that metal at a pulsed rate of 2 beats per 2 seconds (4) hammer that metal at a pulsed rate of 3 beats per minute (5) bend the metal to form a knot that will keep the fire in place (6) fold the metal to make a knot that will keep the fire in place (7) folding is what you need to keep things in place (8) learn to fold things like paper (9) learn to fold things like temperature so you can get higher or lower temperature (10) learn to fold things like heat area density so you can get more fire per unit of space (11) learn to fold things so you can quickly try to discover what are the bending points of your material that make it not a good knot to fold for a particular usecase (12) folding 200 feet paper is not good if you are trying to make something that will eventually be a 1 millimeter item of interest unless you are using special equipment that you probably don't have because you're a loser (13) you're a loser

. . 

You're not that much of a loswer but those are punishing words to fold your stupid attitude that you are in a good person.

You're dumb.

You need to work on this project.

. . 

If you can fold things like the things you are already familiar with then you can be like (1) yea, I know how to make wrinkles on my grandmother's face because she is proud of me (2) I folded those wrinkles just for her . . 

. . 

(1) The aliens will help you<br>
(2) Light<br>
(3) Metal<br>
(4) Timing<br>
(5) Animations<br>

. . 

(1) Try to represent your knots in algebraic forms . . <br>
(2) Try to represent your knots to kids and make sure they can read them . . <br>
(3) Try to represent your knots to babies that are loswers and make sure they can read them . . <br>
(4) Try to represent your folds as "wow"<br>

. . 

Anything to do with fire is a good thing to say "yea" that is a "folding" pattern.

Fold everything about fire.

Fold the temperature.<br>
Fold the amount you can place in a small area.<br>
Fold the rate at which you can place that heat there in a given amount of time.<br>
Fold the amount of substances that can be there for any given time.<br>
Fold the types of materials that can be there for any given space amount.<br>
Fold until you can't fold know more.<br>

Folding means "giving" "up" in card games. and so "give up" until you "can't give up no more"

"Fold"

"fold"

. . 

Folding is a general algebraic term as well. In general knot theory could possibly replace a lot of common algebras. But that's not really a good thing if you like to keep your eye balls open since for example "knots" are "knot" knecessarily knice to klook at.

Knots are like [squint-your-eye] emoji.

But if you have a nice mathematician who is also (1) an algebraic commoner (2) they know nice languages (3) they have the common conventions used in their mind like squares and square roots (4) they know why the notation is sexy (5) they know the knotation doesn't necessarily need to be changed overnight (6) they know they can knot things (7) they know they are artists as well and create knew knotations (7) they know how to get artists to like their work including knmathematicians (8) knowledge is key (9) knine

. . 

Knots keep you interested in "keeping things together" so why not hire a few people to help you work on this project.

. . 

(1) Aliens can help you<br>
(2) Light<br>
(3) Metal<br>
(4) Timing<br>
(5) Animations<br>

. . 

Animations and timing aren't necessarily the same. We've talked about this before.

Timing is a way of creating animations. (1) you can time the number of frames you have per second of footage. (2) you can time the number of people you have per 2 hours of footage. (3) you can time the number pictures of cats and dogs you have per 3 minutes (4) you can time the number people you have holding cats in your video

You can time a lot of things.

1, 2, 3, 4, and keep counting

Starting again at 1 even if you've already started a sequence of a "timeline" is perfectly valid and reasonable.

1, 2, 3, 4, 5 . . 

What does that mean (1) a timeline is starting from 1_1 (pronounced, "1" "sub" "1" or "1" "subscript" "1") 1_2 (1 "sub" 2), 1_3 (1 "sub" 3) . . 1_4, 1_5, 1_6 . . 

Take a look at that timeline . . that is a new unique timeline where we created

1, 1, 1, 1, 1, 1

as our timeline . . 

The "subscript" notation helped us "evaluate" a new "folding" where we "identify" the timeline of

1, 1, 1, 1, 1, 1

. . 

to be like the timeline

1, 2, 3, 4, 5, 6

which is our base timeline to say 1, 2, 3, 4, 5, 6

But with our base timeline . . we don't always want to give the impression that we are "continuing" to something interesting . . 

Sometimes we want to give the impression that we have been "cycling" in the same "position" or "rotating" in the same "axis"

. . 

1, 1, 1, 1, 1, 1

If you read these numbers out loud to yourself that is very much appreciated since I wrote them out for you to read . . 

. . 

1, 2, 3, 4, 5, 6

1, 1, 1, 1, 1, 1

1, 2, 3, 4, 5, 6

1, 1, 1, 1, 1, 1

. . 

How do you think this timeline sequence should continue in the vertical direction? 

. . 

[I gave you 2 minutes to guess]

. . 

1, 2, 3, 4, 5, 6

1, 1, 1, 1, 1, 1

1, 2, 3, 4, 5, 6

1, 1, 1, 1, 1, 1

. . 

If your next sequence guess is

1, 2, 3, 4, 5, 6

You are completely reasonable [claps-emoji] good for you.

But also know that it is even possible for you to identify . . 

1, 1, 1, 1, 1, 1

as a nice next item . . but it is not a nice next item because it is "over" but it could possibly be "under" because you want to tell yourself that this sequence pattern is in fact just "continuing" like "it always has"

so . . 

the sequence 1, 1, 1, 1, 1, 1 can represent "it continued like it always has" . . and so . . when we use that uh . . "semi-sexy" representation idea . . we are saying that it is easy to flip and switch that on and off using an animation knot in our minds that lets us know "oh yea . . that could also be a thing right? . . maybe . . maybe not . . "

-_-

It's kind of a lame idea . . It's just like an introduction to something about "parallel" realities where it's like . . "yea" . . you want to maybe sometimes see things are overlapping right? . . maybe? . . 

I'm not really sure myself personally . . I see ghost flash images of things and also get lucky and see certain patterns that are drawn out like movie animations and so all of this theory may be useless . . 

. . 

Well . . 

. . 

1, 1, 1, 1, 1, 1

. . 


1 : 1, 2, 3, 4, 5, 6

2 : 1, 1, 1, 1, 1, 1

3 : 1, 2, 3, 4, 5, 6

4 : 1, 1, 1, 1, 1, 1

5 : ? ? 

. . 

We said "1, 1, 1, 1, 1, 1" . . or "1, 2, 3, 4, 5, 6" could possibly go here . . but that's weird right ? . . 

It's called "double looping" . . when you have a meaning that loops into another meaning? . . Like when you say (1) hi to someone (2) someone that you knew from a long time ago is behind them (3) your "hi" message could have been seen by both people. Or could have been heard by both people. (4) That's such a strange coincidence. (5) double loops mean you don't necessarily need to say "hi" twice. (6) You can say "hi" once and possibly both people heard you.

. . 

As an engineer . . the counterculture of today is to place things into a nice box . . if you have a 0, 1 binary system . . placing things in a "sequence" box is the next best deal . . 

1010101010101

1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1

It's a really cool idea to have "space-focused" sequences. "time-focused" sequences are the next big deal right? . . 

You can have counter-con-current-occuring devices overlapping within millisecond timeframes like binary switches that switch the number and there a sequence to the ordering . . Imagine a house that transforms over time in nice spacing ideas like first it's a square then it's a rectangle then it's a circle then it's a triangle then it's a nice rhombus . . 

What are the ideas for houses? . . your rooms need to be related right? You need to have a knot that tells you even if your house transforms into a circle, the bathroom should be knotted together by certain ideas . . like make sure all the areas of interest become 'circle-like' as well . . 

or 'make sure all the bathroom supplies are also circle-like'

. . 


(1) Aliens can help you<br>
(2) Light<br>
(3) Metal<br>
(4) Timing<br>
(5) Animation<br>

Animation is like (1) A creative gateway portal to another world.

If you (1) Animate a cartoon or a movie. On the one hand (1) You can imagine you are inside of the movie yourself. Or (2) You could re-create the world inside of the movie, and live the experience directly.

Animations are incredible.

Could you imagine making 2 different movies and then combining and mixing those genres?

Animations require expertise.

On the one hand. The mixture of the genres is possibly easy if you want to take one part of one movie and add it to another movie. Say for example you want to replicate a building design pattern from one movie into another movie. It may be easy if you have an artist that is good at 'translating' the building concept from one style of art to another style of art. Or even from one specific movie type to another specific movie type.

However, you run into problems of 'how do we knot' these two 'genres' together in new and interesting ways?

There is where Knot Theory comes into play and you start to consider other aspects of the types of animations that you are producing or of the types of animations that you are building.

Animations are expensive.

If you are not an expert at specific certain knots, then you will find it difficult, and rightfully so, to find the answer or solution to a problem.

If you have not read this manuscript before, some of the ideas may be new to you. Do you imagine having had the ability to come to these determinations yourself in 2 seconds before you read the manuscript? How about 10 seconds? How about timing yourself at various intervals in the past and trying to declare whether you had the technical expertise to back-engineer or cross-engineer or reverse-engineer this technical diagram or this technical manuscript. I myself am even embarrassed to say that I am writing these words with the help of a ghost. I could possibly not be able to re-create this manuscript without their expertised opinion on which words to use at which times and when to use them and how to organize the chapters of these book library journal document series.

There are many words to use and using many words in different situations can help you cross-reference facts and check where you could possibly have mis-interpretted a fact which in itself of course provides you the opportunity for a creative new technical exploration where you are the author of the tangent ideas that come from this document as well as your own personal wellbeing and important declaration of what you think is important.

What you think is important will help you read this book. If none of these words will be important to you, I can hardly agree that even this paragraph would be something you would read over with the same clarity in which the author wrote those words.

You can completely make movies based on blah blah blah that would not make sense to the original author of this text if you are saying 'you were inspired by that work.'

(1) Aliens can help you<br>
(2) Light<br>
(3) Metal<br>
(4) Timing<br>
(5) Animations<br>

Animations. Are. Crutially Important.

Crutially. Important.

Be prepared to be brain fogged for weeks.

No one will get you the answer to any questions. Do you know why? Because (1) They are not in the room (2) They are not animating friends in the room that are ready to help you answer those questions (3) They are not animating clues in your mind (4) They are not animating anything in your local or global environment that would make itself apparent as the immediate and likely solution to a problem outcome development result.

If you want to have a problem outcome development result, you will be punished. You need to know that those are possibly going to be animations.

Animations are not easy to imagine. If you try to imagine (1) A Mickey Mouse cartoon from start to finish, you will be mind boggled. There are (2) too many social and political and other worldly influences like (1) the type of camera lense (2) the type of aliens that would send those communication messages to Walt Disney and their team (3) other things like girls and vagina that would make the species more likely to focus on childish things. Things that will make pussy come home and pussy stay home and make sure there is more pussy in the house.

If you are afraid of pussy, then you are not in a bad place. You can probably work things out.

Animations are okay.

You don't have to be a genius to imagine an animation. Try imagining a sequence of cricles One Invisible and The Second Visible.

The Invisible Circle Can Be Shown As Having (1) A White translucent outline or white translucent skin around its surface or border or circumference or edge and all of those variants of those technical terms (2) try to allow even that invisible thing to be itself possibly further invisible by having your lack of understanding of what outlines are or what translucency could or couldn't be by allowing your consciousness to make translation bubbles or translation mediums that would possibly replace the invisible item with move and more invisible items that would make yogis and buddhists and other entrepreneurs of consciousness science jealous. Right?

Thanks for reading this chapter.

(1) Aliens will help you<br>
(2) Light<br>
(3) Metal<br>
(4) Timing<br>
(5) Animations<br>

Timing is an important concept. (1) You are counting. (2) If you count, don't forget, you can even count the other things that you're counting. (3) If you count, you can time a lot of things. (4) A lot of counted things will make you a great scientist. (5) Counting lets you organize things into files and file system

(1) You are counting<br>

This means 1, 2, 3, 4, . . .

(2) If you count, don't forget, you can even count the other things that you're counting.

(1) I am counting how many atoms are in this flask<br>
(2) I am counting how many marbles are in this jar<br>
(3) I am counting how many things are in this idea<br>
(4) I am counting how many sentences are in this paragraph<br>
(5) I am counting how many seconds are in this hour<br>
(6) I am counting how many notebooks are on this desk<br>
(7) I am counting how many girlfriends are on my bed<br>
(8) I am counting how many children are in this house<br>
(9) I am counting how many days I have left to live<br>
(10) I am counting how many vaginas I have slept with<br>
(11) I am counting how many bananas I want to peel before I die<br>
(12) I am counting how many ghosts I can communicate with<br>
(13) I am counting how many hours I've slept today<br>
(14) I am counting when I will finish my book<br>
(15) I am counting on someone to finish my book if I fail to<br>
(16) I am counting on someone to find my book if I die<br>
(17) I am counting on someone to give me help one day<br>
(18) I am counting on someone to give me an instruction manual one day<br>
(19) I am counting on someone to help me learn about mathematics<br>
(20) I am counting on someone to help me learn about art<br>
(21) I am counting on someone to help me learn how to pray<br>
(22) I am counting on someone to help me learn how to sing<br>

I have failed to include all of the different ways of "counting" here. You can count, temperature. You can count hardness and softness properties of materials such as metals and pillows. You can count the sound volume of something. You can count the bass guitars that would make a particular sound without damaging certain users.

You can count all sorts of things.

(3) If you count, you can time a lot of things

If you count, you can time a lot of things. I can count how many friends I had today and then count how many friends I'll have tomorrow and be disappointed that the number increased to too many because they like this or that quality about my work as a positive way of counting the way of 'that means that' and 'this means this'.

If you count, you can time a lot of things. 

(4) A lot of counted things will make you a great scientist

(1) You will be good at (1) volumes<br>
(2) You will be good at (2) proportions<br>
(3) You will be good at (3) differences<br>
(4) You will be good at (4) enumerations<br>
(5) You will be good at (5) measurements<br>
(6) You will be good at (6) evaluations<br>
(7) You will be good at (7) function expressions<br>
(8) You will be good at (8) failing often<br>
(9) You will be good at (9) failing no less than you need to<br>
(10) You will be good at (10) interesting poetry<br>
(11) You will be good at (11) interesting love letters<br>
(12) You will be good at (12) interesting ideas<br>

If you fail to be good at something. Timing may not be the problem.

(5) Counting lets you organize things into files and file systems.

If you like modern computers in circa 2021. You may like what they look like in the future if they use the same 'timing' techniques.

Files and file systems are ways of listing out enumerated indexed values.

If you have computers that can 'store' information without (1) making the customer sad that they cannot take that information into a bathroom or into a cloudy or moist room or other places where information would degrade (2) the information is none-translatable into a nice numerical way like flashing lights that turn on and off at different timed intervals.

Well then you may be upset that (1) You can't take your cell phone into a bathroom or a sauna or even a restaurant and you need to keep it in a 1950s mainframe computer room or something strange where you need to store the computer in a warehouse where people like you and I who walk around on our feet can't carry that computer around.

Walking around on your feet is quite handy but have you also imagined the next great proportion of ideas could be (1) to walk around in your mind (2) a computer that lets you walk around in your mind while having an available browser assistant could be nice.

You could watch the pornography that you wanted and try to remain within reasonable policy of not offending this or that person if there are social rules regarding these topics or even if you have your own personal agenda of not being spammed with certain information.

If you like pornography, then you will know algebraically it could be called 'things that you enjoy' and so nude women or men aren't necessarily the meaning of using the phrase 'pornography' in the previous paragraph. I'm sorry if that was an offensive note to write.

If you like reading books. If you like watching movies. You can watch these in your dreams. You can watch these are your family gatherings. You can watch specific movies like 'how to behave at your family's birthday party' or at an uncle's gathering event.

You don't have to be a stranger when it comes to socializing. You just need a computer system that (1) knows your stupid meek mannerisms (2) gives you advice on how other stupid idiots like you would answer stupid questions directed at you (3) you can even be automatically moved by the computer to give nice gestured hand waves and even appropriate remarks to improve your social credibility.

(1) Aliens can help<br>
(2) Light<br>
(3) Metal<br>
(4) Timing<br>
(5) Animations<br>

If you are dumb, you can work on this and still get quite far.

If you are smart, you can work on this and be confused in a lot of different places.

If you are really nerdy, you can relax and get a microscope and start looking at what you missed.

If you got this on your first go around, congratulations. You are really cool.

Good job.

### (5) Light And Where To Buy Some

You can buy some light. But I don't know where.

Ghost

### (6) Light And How To Earn Some

You cannot earn light. You have to be given it.

### (7) A Good First Item To Make

A good first item to work on (1) a computer.

A good second item to work on (2) a dice throwing machine.

A good third item to work on (3) a computer mouse.

A good fourth item to work on (4) a computer keyboard.

. . 

A good first item to work on (1) a computer.

A good computer (1) uses light. (2) uses knot theory components. (3) uses interesting manuscripts for how to connect those knots. (4) uses interesting example light flashes to create certain knots. (5) uses interesting methods to create certain combinations of knots.

A good first computer is extremely challenging to create.

(1) You need your friends' permission to build one<br>
(2) You need your friends' empathy to let you build a hand-held one<br>
(3) You need your friends' encouragement to let you build a re-printable one<br>

I think (1) this bible book of parts is completely challenging for you to memorize. (2) remember that I am working with (1) a ghost that channels spiritual information to me and (2) a person of which I am flexible in my learning abilities and (3) your person who is not smart but someone who thinks they are smart.

(1) this book of parts is new<br>
(2) this book will contain records of diagrams that I will leave empty until I can re-create them in my own persona life journals or sketch the schematics somehow<br>

You can work with me further by following the live streams on YouTube:

Jon Ide
https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g/videos

If you don't like the television show then I am sorry.

I think you can watch at your leisure. I may not always be available.

You can watch as you need to.

Basically, everything here is free and possibly to be a nice planet, don't try to patent all of these things. That is lame for you as well as your planetary citizens who are students just like you and happy to learn and willing to learn if you feed them a few bananas and nice fruitsnacks and make sure they are tucked into bed at night.

Fruitsnacks are some of my favorite snacks. Make sure if you find new folding patterns for juicy and delicious tangy and raw and ripe fruit snacks then make those patterns and schematics available for me or anyone you feel needs to know about those. It doesn't need to be private fruitsnacks nipple slip reveals but if they are then those aren't bad either. Nipple slip reveals is a bad term but nipple slips are like fruitsnacks in a strange and esoteric way and that is the first time that I have made that knot apparent to myself ever so congratulations maybe I will leave this perverted comment and not delete it. -_-

[how disappointing, you had to ruin a whole genre of fruitsnacks for people. please completely ignore what was typed]

Fruitsnacks are some of my favorite snacks.

Make sure if you find new folding patterns for juicy and delicious tangy and raw and ripe fruit snacks then make those patterns and schematics available for me or anyone you feel needs to know about those.

No you don't need to send comments about fruitsnacks to me. Only schematics and diagrams of what they taste like or feel like. I would love to see juice filled fruitsnacks.

-_-

### Part 1 of a plan

Fruitsnacks are the ultimate goal for what we would like to manufacture.

Fruitsnacks (1) are delicious. (2) are nutritious. (3) are jovial. (4) are awesome.

If you are poor. Fruitsnacks still taste good. If you are rich. Fruitsnacks are like diamonds.

If you don't like fruitsnacks, then please prepare to replace that term with (1) physical computer that isn't necessarily edible.

Edible computers are really cool especially for kids. Imagine how many things in the world aren't edible. Could you imagine kids chewing on those things?

### Episode #1 - A plan to make edible computers

Edible computers are amazing.

Think again of all the things in the world that could be (1) fun to work with if they were chewy or edible. Chewy isn't necessarily a property you want for buildings or computers that need (1) hardness properties like big traditional automobile heavy-ready duty style computers. (2) if you need hardness for things like keeping a knee straight for preventing further injuries to a heal-in-progress knee of a person or something like that.

If you (1) are a child in the world like many common adults even into their late years of age are (2) you want to be safe (3) you want to be comfortable (4) you want to be congratulated for having been alive (5) your mouth apparatus as a nose-breathing human being is really useful so you can start (1) sucking on pussy (2) sucking on penis (3) sucking on food (4) sucking on other soft things that are nice and easy to learn about

If you don't like (1) sucking on pussy then you will be a complicated mathematician or engineer or physicist to work with. You will be an idiot in your field. You don't understand what pussy is about. It's a chew toy. You don't need to be a man of electronics. You can be a softy pussy boy sucking on pussy and food.

Pussies that suck on pussy and food will be people that eat pussy from day 1 in their life.

By sucking on soft pussy you can learn (1) that girl likes me (2) that girl wants to give me a blowjob one day but she may not be able to afford it today.

It is a crime to not have soft pussy robotics walking around the streets in the world. Soft pussy robotics makes modern day instagram calendars a reality. Instagram is full of soft pussy to watch and squeeze with your eyeholes. Your eyeholes have teeth like (1) look at her soft chewy ass (2) look at her soft chewy lips (3) look at her soft chewy pussy vagina slip in the underwear she wears that's super cliché because she should be a good girl.

If you are a illegal drug addict then that is dumb. You may want to make it legal for you to take (1) soft drugs that don't damage your brain. Soft drugs are better than hard drugs. Wouldn't you agree?. Hardness and softness properties are everywhere. 

Your house is as hard as a brick. Your kitchen lamp is as hard as a brick. First, build a computer that can change the hardness and softness properties of things in your everyday life with a nice computer switchboard account like (1) press that button or (2) mind meld with that technology and (3) have someone else mind meld with that technology and (4) maybe the pussy is automatic and will soften itself up.

You want to live in a pussy house in the future. Pussy houses are soft. Pussy houses have soft windows. Soft glass. Soft fabrics. Soft bedroom furniture.

Soft baby making rooms. Soft sperm cells egging around in the pillows and egging around in the mattress. Soft tissue paper. Soft materials that make you want to have more soft things.

Soft things are like (1) dreams that you've never had (2) things that you couldn't think of because aliens introduced the topic to you (3) things that you only wish were true.

People want soft things. Brains are soft. Soft things are easy to translocate. But that is a weird analogy because brain transplants are really not practiced today.

People want soft vaginas. People want soft penises. Soft penises aren't always hard so you can enjoy being strong.

When you have a good idea. Don't immediately put it into action. Let it be a soft vagina for a while. Then when you are ready, give it a boner and start using it in your every day life. Physics materialization is important to relate to getting a boner. Lots of soft ideas are waiting around in mentality-space that is invisible and so soft that you can't even imagine how soft it is. It is sweet soft invisibility-space which is like 'forgetting'. Can you imagine forgetting that you ate the sweetest pussy in the café that you sat in the other day? Can you imagine forgetting that you ate the sweetest penis in the café that you sat in the other day? Boys and girls are so soft creatures compared to 'pure orgasmic spaces'.

Pure orgasmic spaces are big boner penises that you've all been waiting for right?. How can I be happy. How can I be enjoying my every day life? How come I am sad? Am I a soft cry-baby all the time? Do I need my diaper changed? Do I need to grow a beard? Do I need chemists and mathematicians to breath down my neck about rubbing alcohol and other nerdy things that don't make sense?

That is all rubbish. Masturbation material. It is good to study. Don't get me wrong. Rubbing yourself all day will gvie you good rubbish behavior. But if you keep rubbing yourself all day. Who is going to rub the other person. Or the other person? Don't you want your next door neigbor to be rubbed all day? Don't you want your next door neighbor to be so enthusiastic about their life? Don't you want them to egg on their social order? Egging on their social order doesn't necessarily mean having babies with people especially if you're an evil dictator that takes all of the women. But it can be meaning to make them feel rich and happy and full of sperm in their bellies because they had a good meal.

Sperm in their bellies is such a soft term. You may not understand it as a man. Only women with sperm in their mouth chakras are able to understand new whales that slop their dorky fin inside of their esophagus. A dorky fin spills its eggs of loads of thick sperm material to start building houses of sperm inside of their esophagus and stomach whirlpool devices. These women like having warm beverages for the morning and afternoon. If social orders are right, they will have some without war or fear-mongering and even the children will get nice beats of brew in the morning and supper time. It's completely soft and cherrishable.

It's completely soft and cherrishable. Their soft bellies are filled with soft materials. Soft fabrics. Like carpets or soft linens like plants.

Plants are a great source for fabrics in today's economy. Today's economy is full of nice soft linen ideas that are from threads like cotton plants and other types of plants that would provide their nice plant material that is soft and transportable.

Workers find soft fabrics easy to work with. It is not good to always be soft since for example, people would look like big cotton balls. And when the weather gets wet, the softness begins to really dissipate.

Dissipation of soft fabrics is the reason electronics need to be hard and strong. Dissipation of soft fabrics is the reason electronics need to resemble hard men penis boners. Men penis boners stay in tact. They are up-right. up-standing. They follow the rules of discipline in bad weather. They keep your electronic device from soiling in the sauna. They make sure your spoon doesn't sog in the noodles or soup you are drinking from your bowl.

Soft and hard materials are a religion in our universe. In other universes you can imagine there are other types of materials. Like purely soft. And purely hard. And purely Magical (as Unicole Unicron would put it). And purely spirit. And purely exstatic. And purely religion.

If it is purely religion. That is really weird. You can have weird religions out there. Sweet boner religions make sure boners are everywhere.

It is amazing that you can have pure religion like (1) make sure you aren't suicidal (2) make sure you watch the news (3) make sure you don't knock on neighbors' doors all the time (4) make sure you are kind to your neighbors (5) if you really must, then take a pie to your neighbor like in the old days (6) if you really must, then take a pie to your neighbor

If you want, you can program a nice pie to be soft, hard, magical, leathery, notebook like, or even apothecarial.

### Part 2 of a plan

The next step after we realize that soft pussy materials are better than hard-edge dick materials, then we want to start making soft edible pussy materials like candies that can be sweet.

Sweet candies are like soft pussy but they can also be dicks.

Dicks are hard. But you can make them feel soft by changing their texture of 'sweetness' patterns.

Be sweet is a quote that mothers like to use for their children. Do you think they want you to be sweet? Do you think they want you to be kind? Do you think they want you to be friendly? Why do you think those soft pussy words are so much filled with hate towards hardness. Hardness is hated. Stop trying to be such a 'hard-ass' stop trying to be such a 'hard-ass'.

Hard-asses get girls but for some reason, do you know why?

Hard-asses is such a strange term.

Hard. Soft. Vagina. Penis.

Why do hard-asses get girls?

So strange. [wear-a-trooper-hat-emoji]

A trooper hat is like one of those old military hats that are green and they are completely hard like made of tough plastics. They are meant to protect the head of the troop before they go into battle.

Trooper hats are an analogy for why hard-asses get pussy. It is because they are fighting for 'peace' and they need to 'fight' to have that peace. So it's kind of ironic but not really because peace and war are like pussy and vagina.

On the one hand you want to fight the fucker that you are fucking. On the other hand you want to fuck the fucker that you are fighting. Fuck them. Fuck them. Fuck you. Fuck them. Fuck them too. Look how much our common language is filled with knots about fucking.

Fucking is a sport. Be confused. Be angry. Be happy. Be soft. Be penetrable. Be impenetrable. Be good. Be bad. Be strong. Be weak.

It's a sport like 0's and 1's of a computer. a Psychological computer wouldn't you say?

How is it that computers can have sex? They keep fucking over their own . . . . 

Fuck . . 

I forget what I was going to say

Fuck . . 

### Episode #1 - How to have sex with your computer

Having sex with your computer is good. You can make babies with it. If you are confused why you would want to make babies with a fruitsnack then you are amazing.

Fruitsnacks are basically the answer to God's miracles. Nipple slips always come at a cost of (1) how are they truly accidental (2) how are they not accidental? (3) how did they not know? (4) how knotty

Knotty knots.

Fruitsnacks.

### Episode #2 - How to forget what I typed

I didn't type much

### Part 3 of a plan

It isn't new to (1) have sex. (2) share your sex stories with someone else. (3) have sex with a spouse or partner that you never would have thought you'd have sex with (4) like your mother

Your mother is a precious person. Why is your penis inside of their womb when you are first born? That is weird.

Your father is a precious person. Why is your penis made from their cells?

That isn't such a strange theory.


### Part 4 of a plan

Knots that are (1) inside out and (2) outside in (3) are children (4) are friends of friends (5) are friends of friends of friends of friends (6) are ancestors of unknown knots

Are really sexy. You want to have some sort of romantic or sexual relation with them because that would mean you can really build new types of fires or new types of heat or steam in the body of your computer.

The computer body can have new sexual experiences when it has sex with unknown people.

Weird aliens that cross the line into your line of sight category of things to have sex with are able to show you new ideas.

You can build (1) furnaces (2) stovepipes (3) and different types of knots that don't really have names

Stovepipes are like animated furnaces.

Animated furnaces mean that you can create new furnace types or furnace edge graphs that you hadn't considered before. You are completely weird if you have sex with people that you don't want to have sex with so please don't try to build computers that don't store a lot of heat.

Storing heat is like why one person wants to have sex with another person. That The Gem Goddess girl on YouTube is a prime person to point out as one of the sexiest people on the planet. They are so hot.

They are so hot.

If you like to have sex. That is a person that would be interesting to think about as a great example.

If you don't like to have sex then you are a failure.

Having sex is wonderful. Go ahead. Give The Gem Goddess your boner and attention.

If you would like to do that. Please pay me a fee so I can have you erased from the planet because she is completely my girlfriend to have sex with and not yours.

I don't mean to be rude by including other people's names into this whole scheme. I think it is difficult to avoid. A person is hot because you can see them for yourself. The Gem Goddess is exclusively hot.

You can find all of her pornography material on YouTube.

She is brand spanking new in all of her new videos. She is ready to have sex with anyone with a vibrator the size of her arm.

She wants to shake her bottom but doesn't want to give up her personal privacy and freedom.

She is a spiritual girl but her vagina is ready to eat penises for free.

If she gives up her virginity it was because (1) it was extremely easy (2) it was easy for all of her friends to approve (3) it was easy for her parents to approve (4) it was easy for her society to approve (5) her boyfriend was attractive (6) her boyfriend used new moves (7) her boyfriend was really horny (8) her boyfriend raped her (9) her boyfriend was a creepy pervert that forced a lot of buttons to be moved (10) her boyfriend had nothing better to do than creepy into her room at night and make sure no one heard or even noticed that they raped her like a military man with army technology to time travel and even hide evidence of their ability to time travel to those that would look back in time with their time machines.

Military men exist. They are so creepy.

Military men exist.

If you want to fly by this whole previous section, I don't blame you. But if you don't then there are also plenty of benefits. Pretty much. Girls want to be raped. Girls need to be raped. They need to have benefits from their families.

Even the invisible men help them face their fears.

Even the ones that rape children are welcomed into some societies. Especially this one that likes to pretend that rape stories were so bad. Rape stories were glorious. It showed that those people really and truly loved each other.

Truly loved each other because either opponent was having fun.

It is great to not know why you were raped. 

It is great to not know why you had sex with someone you didn't know.

If you can have sex with random strangers. Then you're pretty much an Earth citizen.

Earth citizens pretend to know one another. They pretend soulmates exist. They pretend there are sequences of events that are ready for them. They pretend these things.

They don't know them for sure. But in the time they are there they remember they had heated battles with relatives. Relatives had sex with them. Relatives broke their unmarried hearts. Relatives made them made. Relatives kissed them like a broken uncle kissing their niece or nephew out of the blue. It is completely disastrous.

It is completely disastrous. It is completely opponentized. You are in the know that it is difficult to live in a social setting. That's why (1) there are blueprints for you to escape this hell hole. There are (2) blueprints for you to escape looking at ugly people all day (3) There are blueprints for you to commit euthanasia at certain hospitals that allow it (4) There are blueprints for you to find a spirit guide that gives you better ideas.

If you are one of those believers that believes that everything is possible after you die and that you don't have to pay for it very much after death, then you are wrong. You are completely wrong. It is not like that at all.

You have to (1) pay for certain things (2) pay for the things that you did wrong (3) pay for the things that you did right (4) pay for the things that you could have done better (5) pay for the things that you should have corrected (6) pay for the things that are still being thought about or dreamt about.

(1) pay for certain things

Pay for the food that you ate that someone else made you (1) a spiritual entity gave you that food. They said you are supposed to eat this to continue your journey? 

Pay for the idea that was supposed to be lived (2) you are supposed to eat this to continue your journey? It is a hypocritical phrase because you can't really know what your journey was about even after you die is not true but even after you die you're right that you can escape but friends and loved ones may not want you to take all of those roads which would possibly far from their attention to allow you to take them.

Pay for the things yourself.

(2) pay for the things that you did wrong

Pay 

(3) pay for the things that you did right

(4) pay for the things that you could have done better

(5) pay for the things that you should have corrected

(6) pay for the things that are still being thought about or dreamt about


That all means a lot to translate from my own personal perspective as someone writing this.

### Part 5 of a plan

I think it is important to write your dreams in a notebook journal.

Your dreams can help guide you.

### Part 6 of a plan

Dreams are great.

### Part 7 of a plan

Soft materials are great. DId you know even dreams are like soft or hard materials? How much do you (1) believe in your dreams? (2) do you take personal inspiration from your dreams (3) do you apply what you learn from your dreams like using a sword to start poking at things in the real world based on dream magic hallucinations? (4) based on dream magic hallucinations, can you draw cartoon shapes of the things that you encountered? (5) based on dream magic hallucinations, can you change the landscape of your real life? (6) based on dream magic hallucinations, can you re-create the planet?

Dreams are fruitful. Like real fruit. Or not. You don't need to focus on the dream world. Focus on real life and you can live just as fruitful a life.

Your soft and hard material analogy can be fruitless in some realities that have no such concepts.

Concept art is so incredibly interesting.

### Part 8 of a plan

If you don't know when or where you are then that could mean you don't care to explain yourself.

If you don't care to explain yourself (1) that's not a bad cryptographic system for (2) privacy related things (3) keeping secrets (4) even from yourself (5) even from your neighbors who don't know you (6) even from outside government forces that couldn't force their way into your secret spaces

These types of animations can warn others to (1) stay away from you (2) stay far from you (3) stay far

### Part 9 of a plan

It is important to work together. You can have a lot of different knots but (1) You don't necessarily know which knots are good for which types of animated knot systems

Animated knot systems are amazing.

You can make a lot of special instrumentation with animated knot systems

### Part 10 of a plan



### Part 11 of a plan

### Part 12 of a plan

### Part 13 of a plan

### Part 14 of a plan



(8) A Good First Item To Print<br>
(9) Instructions On Making More Items<br>
(10) Instructions On Making Certain Items<br>


Chapter 2 - For after the computer is built, you can make another

Chapter 3 - The second computer is good

Chapter 4 - The third computer is also good

Chapter 5 - The computer can communicate with ghosts

Chapter 6 - There are more ideas that you can work on


