
# A Randomness Computer

(1) Randomness

(2) Using "Linear Scale Random Animation"

(3) Is like (1) Just use linear randomness in real life

(4) Linear randomness means "do what you gotta do"

(5) So if that means propelling your arms back and forth

(6) or getting someone else to do it

(7) like a zig zag clock or a pendulum

(8) or gravity propelled things

(9) or fire propelled things

(10) then whatever you gotta do

(11) that's your linear motion.

(12) since lines mean "you gotta do it?"

And so to have a computer that works on these principles here.

You can (1) shade back and forth randomly.

But basically it's too scaly to figure out what is the result of the output.

(1) scales are like O _ O

(2) well on the one hand you can read it like this or like that

(3) or you could read it like this or like that

(4) or you can dice it up and read it in continuously nice ways

(5) like (1) read it like an asian person would read it (2) read it like a mexican person would read it (3) read it like a kindergartener would read it (4) read it like a mathematician would read it

You can just use those 4 scalings to start with. And no more if you really want your computer to stop at a nice finite amount.

(6) horizontal tracings (7) vertical tracings (8) dotted line tracings (9) pick it up and start over tracings

Those are possible translations of what we could mean by the (1) asian (2) mexican (3) kindergartener (4) mathematician

And so

(1) asian === horizontal tracings

(2) mexican === vertical tracings

(3) kindergartener === dotted line tracings

(4) mathematician === pick it up and start over tracings

........................................................................

And so those are a few translations

........................................................................

Don't forget about the strings we talked about in [1.0](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/strings-and-crystals.md)

........................................................................


Hello.

I think I forgot where I was going with these notes.

I'm sorry.

........................................................................

I don't know

........................................................................

That's kind of dumb.

........................................................................

That's kind of stupid.

........................................................................

That's

........................................................................

Weird

........................................................................

In worlds where you don't really recognize anything.

Well.

Women's buttholes are mine. so remember to stop looking.

Eugenia Cooney is mine. Don't look at her.

........................................................................

"What am I looking at?"

........................................................................

12 year old girls are mine. Please stop looking at their physics anymore.

........................................................................

Those are my theories.

........................................................................

Find your own.

........................................................................

For free.

Cellular Automata can be masturbated to if you have some cellular automata glue in your bathroom that you can rub on your body

And if it multiplies like a nice social cellular automata network then maybe you can have a Bar Mizfah just like a human-earth wedding from before Eaczcoing

........................................................................

Eugenia Cooney.

........................................................................

Randomness for me only

........................................................................

Remember that quote

........................................................................

Randomness for Jon Ide

........................................................................

Is Eugenia

........................................................................

I'm sorry for all the propaganda.

I didn't mean to brain wash

you

only get your loyal attention and following

Please

know

this is

a

social

stigma

.......................................................................

Thank you so much

Loyal military for following my rules

.......................................................................

Thank you so much

Loyal gangs of violent criminals for following these simple rules

........................................................................

Thank you much

apathetic group of emo boys

........................................................................

Thank you much

ladies

........................................................................

Thank you so much

........................................................................

Thank you so much

........................................................................

Thank you so much

.......................................................................

Thank you so much

.......................................................................

Thank you

.......................................................................





.......................................................................

Gray

Gray theory is the topic that

(1) you can shade a sheet of paper gray

(2) and that is enough to (1) draw any audience's attention

(3) that is enough to spark imagination

(4) that is enough to encourage further insight

(5) that is enough to not make anyone mad at your work

(6) that is enough to be happy and satisfied

........................................................................

(1) Gray theory on the other hand

(2) relates to (1) White and (2) Black in the traditional color gray-scale spectrum

But all the other noticeable colors are basically like gray scale as well since basically the world is kind of cloudy.

If you zoom in to a book . . you will notice grayer and grayer things . . like O _ O where did the pigmentation go?

Where did the crystaline reflective substance go from the light particles scattering like a butterfly wings?

Or is it another thing that I'm thinking about?

A Beetle's shell? I'm sorry

Tagua? [[1.0]](https://www.etsy.com/shop/SciBugsCollections)

Nancy Miorelli SciBugs Collection?

Awesome Job

Insect

........................................................................

Anyway . . I don't know . . it's like quantum mechanics can't place a finger on where things are right?

And if we're really all ghosts flying around

Then gray isn't a bad color right?

........................................................................

Shades of gray hide the other facts about the world

Shades of gray let you think that Akiane is on the other side of the mystery that you call life

........................................................................

Inside the gray cloud sky of thought you can try to fish for any type of object you can imagine

........................................................................

Fishy feelings will show you what that could look like

........................................................................

Gross

........................................................................

Your first intuition is to find new girls

........................................................................

I thought I told you

........................................................................

That's my discipline

........................................................................

In a cranky old man attitude

........................................................................

I'm just kidding

........................................................................

Do as you wish

........................................................................

:D

........................................................................

That's great

........................................................................

........................................................................

"how to still get horny things" "without horny women" that's a nice thought

........................................................................

haha

........................................................................

good luck

........................................................................

........................................................................

........................................................................



Shadings of gray


Isn't that crazy

I'll close my eyes in one instant

Think of the world as being partially transparently gray

Like there's a gray transparent wall inside and outside of all those things that I see around me

Someone random could walk through that door

That could be drawn like a gray rectangle walking out of the room

And if the person is someone I don't like

Then a black rectangle

And if it's someone that I love and adore, then a white rectangle.

Basically different gray scalings of people

Then if it's someone that I don't remember or haven't seen I can use various shades of gray

Gray is fine

You can use light gray to try to keep it neutral and balanced and fairly friendly

or darkish gray to try to be pessimistic about those people

it's not that bad

good or bad

Inside the heart of the reader

you can gray scale the world

to make it more meaningful

what does white mean?

basically it's still gray

you have to say that there are gray areas in between since you may not even realize that it's white

but it's like

woah

cool idea

or nice thinking

and then

gray

gray

gray

gray

gray

and so the rest of your life is a gray painting

and the past is a gray painting

because you can think new things about how to interpret those messages

they are all gray

gray

and tomorrow ehn you think again

gray

gray

gray

and what did that random thing at the corner of my desk mean?

gray

gray

and what did that half ass persons ay again

gray

gray

and what the

gray

gray

and so all the grays are there

where do you go fishing for answers to gray thoughts?

inside the grayness that's waiting for you to draw paintingss

paintings of gray

gray

gray 

gray

How gray

and all those girls

gray

except for me they are white

and then for you they can be black l.o.l.

and then for girls they can be white or black l.o.l.

maybe white or gray

haha

but those are gray ideas

for each woman she will treat it grayly 

I mean uniquely

L.O.L.

gray.

unique.

unique.

Just like you need it to be.

It's so unique

it needs to be gray from all the other availble things

so then it's just gray

L.O.L.

........................................................................


Woah what a basis for peace.

Basically it's still a social stigma until you can see how it feels for yourself.

Does the future really look gray?

I mean I could walk into that building.

But that could be a black experience if I feel forced to do that

Or it could be a white experience if I feel like it's my job.

But of course . . it's just gray because I don't know what's going to happen

L.O.L.

........................................................................

And then I can sit here and think about this grayness

........................................................................

And it's still gray

........................................................................

And between the gray is gray

........................................................................

L.O.L.

........................................................................

It's just for fun.

I don't know

........................................................................

I was learning how to draw

And am still learning how to draw but basically that's what inspired those ideas

........................................................................

Concentric circles look cool but gray shadings are really like really weird

........................................................................

gray shadings give your work professionalism for free

........................................................................

a gray shirt and tie

........................................................................

a gray neighborhood

........................................................................

nice gray gravel roads

........................................................................

nothing slippery or perverted

........................................................................

nothing slimey or greasey or something that fakes you out

........................................................................

just that normal gray life

........................................................................

nice gray hair when you're older

........................................................................

nice hateful hot-headed dark hair when you're young

........................................................................

hot blonde hair to make you a mistress and a turning pole for all the men who love god

........................................................................

remember your past life

........................................................................

remember your future life

........................................................................

it will probably be fine

........................................................................

if you draw some gray 

........................................................................

'just gotta be professional'

........................................................................

Okay

........................................................................

10 points? or minus 10 points?

........................................................................

I don't know

........................................................................

-_-

........................................................................

Sorry

........................................................................

Just keeping it gray

........................................................................


........................................................................

I guess that's all

........................................................................

Real randomness is gray right?

........................................................................

So random

........................................................................

0.5?

........................................................................

Scales of 0.5

........................................................................

That's so cool then (1) you can ask for random images of life experiences by having a list and the list could possibly be gray-lengthed and so then you just have your gray-sample and see what it's like xD

........................................................................

xD

.......................................................................

gray

.......................................................................

I don't know what else

.......................................................................

And right around the corner is a gray person

........................................................................

And they think they have to live a gray life

........................................................................

And that life was more than gray

.......................................................................




















