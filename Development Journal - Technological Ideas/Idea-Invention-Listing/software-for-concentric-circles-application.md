

Software For Concentric Circles Application








# Day Journal

### Day Journal Entry - August 9, 2021

[Written @ 13:55]

look how hypnotic this story is O _ O 4 pendulums      O _ O . . (1) I am inspired to share this with you (2) it has to do with questions like (3) how do you on one sheet of paper (4) have a pendulum? (5) that looks like it's moving (6) so basically that could mean things like (7) you can get hypnotic effects on your sheet of paper for free

. . 

(1) hypnotic imagery is a nice term (2) but basically drawing concentric circles all day (3) I don't know 😑 It's not really apparent that these are like hypnotic (4) not to me anyway (5) so anyway (6) If you then uh just draw a pendulum it's like O _ O I was surprised to discover that you can get interesting hypnotic effects and so I would like to show you the results of this imagery that I have found . . 

. . 

We start with 4 pendulums. But you can have more if you want. Or less . . if you draw 1 that is fine as well . . 

. . 

The pendulums are side by side as shown here

. . 

One thing that is beautiful about pendulums (1) is that they can be looked at (2) if you see them from this angle 😑 anyway this is all just magical stuff . . It's kind of weird to me . . but uh . . yea . . it was surprising stuff

. . 

(1) I will tell you (2) there is nothing so special about this particular set of lines (3) I think they are more like shadings (4) uhm . . so basically I drew like 1 to let's say 5 lines for each of these vertical oriented lines . . and so . . well . . uh . . okay . . let's look at the effect . . basically I was trying to see what a pendulum would do . . 

. . 

I know pendulums swing . . 

. . 

I forgot to draw important lines for you which I did for my very first pendulums so I'm very sorry for that . . here let me draw them now . . 

. . 

These arc-circle or arcs is basically a section of the circle . . a section of the circle . . of a part of the parimeter

. .

Okay those ars at the base or arcs at the base of the pendulum are representations of how the pendulum swings (1) for me this was easier to visualize (2) I used pencil and paper (3) and I also drew the base of the pendulum arc swing before drawing the "concentric series of lines" or "hallucinatory" "hypnotic line" sequences next to the central pendulum line . . 

. . 

(1) do you see how you can shape your eyes from left to right (2) and then from right to left (3) to see the pendulum effect playing out? (1) it is very convenient to be able to go back and forth like this (2) you can basically do this forever (3) so the hallucination is occurring in the perceiver's time and (4) and not in "the artist's time" . . so for example the artist could have drawn the pendulums separated from one another in different sequences which could possibly erase the effect so it would possibly not look so "convenient" to "hallucinate" or "convenient" to create the "hypnotic effect"

. . 

Maybe you can convince yourself to go back and forth between these pendulums. (1) You would then begin to see a pendulum swinging right? (2) Isn't that trippy? (3) to watch (4) movies (5) on a single sheet of paper? 

. . 

Do you think that is cool? (1) going back and forth on a line (2) is quite convenient (3) but don't forget (4) humans are so smart (5) and lines can have like different arcs and stuff

. . 

by the way, on youtube "," to go back in small increments, "." to go forward in small increments

. .

(1) As an artist (2) Andrew Tischler (3) Cesar Santos (4) Jono Dry (5) Akiane Kramarik these are the best artists we have in the world today. They are still young and yet they are still amazing.

. . 

Follow their work on their own portfolios

. . 

Sadly I am not as accomplished as them so this is advice coming from someone who is not accomplished

. . 

(1) As an artist (2) you can possibly take advantage of some of the studies here on pendulums. (3) Don't be shy to say there are 10 or 20 or 100 different uh . . (4) pendulums that are side by side (5) and that all means things like (6) gosh (7) angles (8) angles (9) gosh (10) but notice here in this 4 pendulum example . . 

. . 

We have 4 pendulums. but look (1) free hand drawing is so fun (2) we don't need to measure things (3) and so we can just say (4) ahh that's pendulous enough (5) look at that (6) and also look at that (7) it still looks good even in free hand 😆 (8) but basically (1) do you see how 'clipped' or 'cringey' this part of the drawing is? 2 and 3? those are so much very cringey compared to 1 and 4 . . 1 and 4 are just 'free' like nice free people that are visible . . such a free bird . . so visible . . no cringe . . (1) look how awkward that is (2) do you think anyone wants to draw that? (3) that is so cringey . . (4) but basically that is the answer to sexy actions (5) you can be a creepy pervert online on the internet for free (6) and just look at the results (7) as long as you have creepy birds like megan fox (8) they will be behind trees (9) and they want the world to know 'what is happening' behind that tree. (10) do you have pom poms . . it is good for a magician to put their fire plast out there . . but . . if it is deep in their pocket . . you can try to imagine a spell caster still only at the initial phase of the cast of the attack and it is still creeping out . . it's not really a nice clear shot like a photographer would take right? small creepy . . one pom pom is easier to draw than the other? the one out in the open is such a large circle . . the other is just like it's partially hidden by a curtain . . a curtain . . of angles . . angles are crazy . . if you can make angles that is awesome . . why is one pom pom hiding behind the leg? so awkward to draw that? do you trace a simple circle? for the whole leg? and then erase ? some of the parts? with a nice clear pom pom it's so easy. just draw that circle and don't worry about erasing right?

. . 

(1) Akiane (2) Question #1: what is the difference between 2 and 3? pause the video for 10 minutes. 

. . 

(1) it is nothing. they are similar haha

. . 

If you can draw 2 then you are really good

. . 

(1) Jono Dry (2) What is the difference between 4 and 1?

. . 

Pause the video for 10 seconds.

. . 

Okay your time is over

. . 

Nothing. That's correct. You got it. They are pretty much similar and the same thing

. . 

(3) Andrew Tischler (1) What is the difference between 1 and 2?

. . 

You have how much time?

. . 

You have how much time to answer the question?

. . 

mero time that's correct

. . 

You can watch the video now (1) or watch it tomorrow (2) or watch it two weeks ago (3) if you have time travel technology (4) or if you have a good friend that told you too early? (5) or if you have someone spying on you (6) and whispering things in your ear

. . 

What is the difference between 1 and 2? 

. . 

You have mero seconds to guess (i.e. 10, 20, etc.)

. . 

Okay you may pause the video

. . 

Okay (1) Did you pause the video? (2) Cesar Santos, would you like to answer for them?

. . 

That is what mero time is all about. You don't always have the time you wanted

. . 

For example (1) Imagine trying to paint a picture of a bridge (2) but bridges don't exist

. . 

(1) you need to be hella creative (2) or just wait (3) and if the person that invents bridges doesn't exist yet (4) that is sad (5) so then you are pretty much waiting for that one lady who wrote "Harry Potter" to exist because you know what "getting hella lucky" "to imagine" "harry potter" is dumb. right? so just 😑 . . 

. . 

Harry Potter is bridge to games like (1) what if (2) what if (3) what if 

. . 

Maybe 

. . 

Okay now your time is up. When should I tell you the answer?

. . 

0 seconds from now?

. . 

several seconds from now [stoic philosopher] [hmm-mustache]

. . 

mero seconds from now?

. . 

woah. Joe Rogan will reveal it on his next podcast O _ O that's quite a timeline

. . 

So 😑 

. . 

These are really amazing. (1) we are so hypnotized to believe in things like (2) angles. (3) and things like angles of incidence (4) these are wicked??

. . 

(1) is awesome to visualize right? (2) so uh (3) I guess to keep it simple (4) since uh (5) simplicity is good (6) we can say (7) it is just an angle (8) so just learn to draw the lini-ies at an angle. the lines- at an angle . . 

. . 

Good luck. (1) I think I just taught you about hallucinations (2) you can be like O _ O (3) wait (4) so swinging my eyes back and forth (5) I can see this painting in a new way (6) like watching a movie (7) and then (8) 

. . 

This is kind of an interesting movie O _ O 

. . 

But it can be kind of weird without the pendulum effect

. . 

What creates this pendulum effect in an image? How come we cannot transition from one representation to another and see how it's like a hypnosis effect?

. . 

(1) Have you ever tried to draw a hot girl (2) but you're using references all the time? (3) you need to study hypnosis (4) She's so hot (5) she's hot (6) You have to look at all those hypnosis ideas (7) society maybe (8) haha (9) and try to see it's like O _ O (10) hot (11)

. . 

Isn't it great to swing back and forth and back and forth. Do you know how to make a mistake?

. . 

(1) what is wrong here? (2) it's awkward . . (3) no way to swing back conveniently without skipping your eyes (4) . . right? (5) 

. . 

(1) hmm try to read from left to right (2) like a book (3) and I guess (4) personally I kind of get stuck. (5) if you find a psychological trick for getting over the 5th item here that might help me out a lot . . the pendulum effect isn't the same any more . . and if you loop this over and over like this . . uh . . where you don't just stop . . at the last so called frame . . and require the user to loop backward . . uh . . from the last frame in reverse to the first frame . . well . . uh . . basically . . I think that's possibly what gave the pendulum effect a nice . . movement effect to it . . 

. . 

(1) Your legs pendulate (2) your arms pendulate (3) your head pendulates (4) your penis pendulates (5) that girl's vagina pendulates by rubbery waved motions right? (6) lovely megan fox motions (7) all the love 

. . 

so make that pussy pendulate

. . 

to hypnotize all the viewers

. . 

Down arrows for free. Or arrows for short if you don't want to use down arrows. but don't forget (1) pendulums inspired that whole idea (2) so down is a natural direction (3) up arrows? (4) well that's the basis of a theory (5) for things like why akiane could have dopplegangers with nice foreheads (6) are arrows a nice signifier of beauty marks? we have to look around for example (1) one good reason is because pendulums tell us (2) they really could be (3) arrow tipped eye corners . . and then what would not be an arrow idea? something that's 😑 flat . . a circle? . . is that pendulous? . . hmm . . hmm . . circles at the center of our nice arrow-tipped eyes . . that's amazing. 'focus' on your soulmate so beautiful . . I have been drawing nice concentric circles for many days now . . and I know they kind of look hypnotic now when I look at them . . but pendulums the way that I've drawn them now well . . those theories about arrows . . hmm . . they are quite interesting . . it has only been just today that I realized something about them that is like       O _ O 😑 . .      O _ O wow 😑 

. . 

Do you know that yarns of strings are like pendulums . . and we have basic answers about yarns of string being so nice . . but arrows tell us things like O _ O the reason yarn is so cool is because it (1) has an arrow (2) arrows are like "supposing" directions right? but which direction? (3) a hypnotic direction (4) like possibly gravity related? (5) 😑 gravity?!?! (6) do you think mankind was influenced to be designed with "attraction" in mind like living on a gravitational planet and not for example just being spontaneously imagineered in a random thought-o?

. . 

random thought-o

. . 

(1) one other theory that I've been working on is (2) how do you use concentric circles (3) which are by the way easier for a beginner to draw (4) than anything you could possibly imagine right? (5) so a beginner simply has to ask questions like (6) how many circles do I draw that are nested into one another? (7) how far apart should they be? (8) and that's pretty much it right? but take a look at this idea . . (1) You can use (2) rectangles (3) I don't really know what a rectangle is . . (4) lines are like uh . . from the edge of the concentric circle to the center of the concentric circle (5) Isn't that interesting? (6) so it's like a pendulum in a way? (7) that invisible line from the edge to the center (8) is awesome (9) and you get it for free without having to draw it (10) just draw (11) one circle inside of another? (12) A nice concentric rectangle ring with 1 rectangle or 1 square inside of another and so on and so forth is awesome. But basically. This is all about hypnosis right? so it's like O _ O 

. . 

Take a look at this weird nonesense . . (1) if you have a nice concentric rectangle ring like this (2) you can basically make people believe 😑 (3) O _ O (1) you're on top of a pyramid (2) or you're looking inside of a room       O _ O . . what a trippy hallucination . . and maybe some other hallucinations are in there that are "quote-on-quote easy to find"

. . 

Look at this small ball inside of this room O _ O 

. . 

haha . . the idea is that (1) you can draw 1 ball that is small (2) you can draw another ball that is large (3) and for a strange hallucinatory reason right? (4) these could possibly look like they are "distanced" from one another . . (5) It's like drawing one person really small next to like "scaled" "hypnosis" rings and for some reason, a larger person on another side of the ring looks like they are taller or bigger than the other first person . . so it's like O _ O . . interesting . . 

. .

I am not exactly sure how this video works here (1) "These Rooms Are Actually Optical Illusions" (2) It's maybe something about hypnotic rings (3) to make you believe the environment is "normal" (4) but clearly when you enter the ring (5) it's scewed since inside the ring is different from standing outside of the ring (6) just like if you're having sex with Eugenia Cooney (7) That is a ring that only Jon will get to know (8) and if you're standing outside it will be like [slash-shrug] I will leave them alone and won't ask what it feels like to be with the most interesting woman.

. . 

awesome hypnosis. awesome hypnosis

. . 

I am quite tired. I am sorry.

. . 
























[I copy-pasted these notes.]
[These notes are from the YouTube live stream]
[I think I am trying to tell a story]
[The story is about (1) how to try to creative hypnotic imagery]

[1.2]
Time period of a pendulum depends on its length | Oscillation| Physics
By KClassScienceChannel
https://www.youtube.com/watch?v=02w9lSii_Hs




[Written @ 12:46]

The top and the bottom of the pendulum. It is a good place to start (1) as a mathematician you can appreciate the simplicity yes? (2) but of course I don't know what I'm doing (3) I'm very sory (4) I am mostly seeing it's like 😑 hmm kind of weird. (5) hallucinations are everywhere when you are drawing (6) a circle isn't always so 😑 uh . . the same right? (7) on nipples it's like 😑 nipples can be simple non-circles and still uh . . it's like 😑 hmm . . hallucination . . Here is a nice nipple on the bottom but it is like 😑 that's a weird type of circle . . it's like not connected in places . . what the heck . . and yet in comic books you can look at it and say 'yea' that is a circle . . or that is like a way of saying yea . . that is a nice nipple . . that is a nice circle . . that is a nice circle but 😑 it's basically 😑 wait . . what is a circle again? we are allowed to skip places?? so basically this can be involved in mathematics like 'limits to infinity' or things like that . . but really hypnosis is an interesting idea . . it's like a 'stand-in-place' infinity for free . . for example if you are hypnotized it is like 'yea' 'believe' and so if you show you so bullshit nonesense you can say 'yes' 'i believe' 'we are working with the same clues here' . . and so even if i tell you a tracing of an outline is a circle we can just go from there . . and so tracings which are like invisible outlines . . it just means that circles are possibly themselves invisible outlines of other more circular circles than what computers can outline for us . . so this circle or so called circle is only the outline of a possible circle . . in theory it is more like a bunch of small rectangles that are small enough to create the illusion that you are looking at a circle . . and illusions and hypnosis are ideas that we talk about . . so basically . . we can try to say 'all things are illusions' or 'outlines' of more 'illusions' which are still also only outlines of more illusions and so in theory we could have better magnetometers that attract better and better hallucinations of circles or more and more ideal representations of circles but we are only saying that circles are just illusions that we believe in somehow . . l.o.l.

[Here is a message that I copy-pasted from the live stream chat earlier. It is about illusions and hypnosis and how circles are really just ideas or outlines of other more ideal circles that are still themselves traces of more ideal circles and our illusionary representations can be as nice as 'i believe' or 'yes' 'okay']



[Written @ 12:11]

Those large pendulating breasts that you can design are really good.

(1) Make them go to the floor if you need.

(2) Don't even forget.

(3) Strings are basically good

(4) But you can say things like (1) In proportional realistic phases (2) They are supposed to be (3) metal

(5) Or in proportional realistic phases they are supposed to use skin tissue that looks like this

(6) If you don't know how to design skin tissue

(7) Again, lady, come back to the idea of designing things with strings

(8) Paper is a great idea

(9) But yarn in your hands lets you truly visualize things right in your mind or even in your physical visible hands

(10) You can hold an image of string in your mind and see yourself tugging and pulling on it

(11) When you husband tugs and pulls on your gigantomastia breasts

(12) They will be milked

(13) Milk will flow smoothly from your breasts and you can even describe things like (1) what type of flow pattern is it?

.......................................................................


This is a breast with a nipple.

Look at how simple it is to design using your own string system. (1) strings of yarn can be easier to work with than pencils and paper. Realize that strings of yarn can be truly morphed and twisted like you want to twist and morph your nice fat lobs of of love.

. . . . 
        .
         .
          .
           ....
           .  .
           ....
          .
        .
      .
.  . .

.......................................................................

. . . . 
        .
         .
          .
           ....
           .  .  ...............................................
           ....
          .
        .
      .
.  . .


Look at this squirting pattern.

It is a nice decent line that you can make from strings of yarn. Sitting right next to your lobed breasts.

.......................................................................


. . . . 
        .
         .                 . . .                       .
          .              .      .                  .      .
           ....        .          .              .         .
           .  .      .             .            .           .
           ....                    .           .             .
          .                        .         .                .
        .                            .      .                   .
      .                                . . .
.  . .


You do not need to be a complicated mathematician or a genius artist to draw this using strings of yarn.

If you really want to draw this. Start with a beautiful prototype of yarn.

(1) Then you can even choose your yarn colors to give it variety.

(2) The color is optional but it can be useful for communication to your staff of workers who want to help you grow your own lobes

.......................................................................

. . . .                          .
        .                      . 
         .                . . .
          .            .
           ....      .
           .  .     ....................................
           ....     .
          .           .
        .              .
      .                  .
.  . .                        .
                                .



Do you need to have a sprinkler system?

........................................................................

. . . .                     .
        .                 .         .
         .             .       . . .
          .          .      .
           ....    .      .
           .  .         .  . .   .  . . . . . . 
           ....     .  . .
          .          .     .
        .              .      .
      .                  .
.  . .                      .


Those are beautiful sprinklers

........................................................................

Sprinkler systems by the way are very interesting

........................................................................

I do not have notes on those unfortuantely but I will plan to write something at some time.

(1) basically (1) it is kind of like concentric circles. 

(2) but in real life sprinklers are not necessarily seen like this

(3) But basically you can think of them like strings

(4) if you have many streams of strings

(5) isn't that a sprinkler

(6) And they can wind and unwind like D.N.A. strings

(7) So it's like O _ O You could make instruments that wind and unwind strings like nice polymerase chain reactions?

(8) Well the idea is that polymerase chain reactions multiply genetic sequences

(9) so it's like taking a water bottle and transforming it into more strings of water sequences which are specific and not necessarily like random water strings swimming around together

........................................................................

........................................................................

........................................................................

(1) You can design your own body

(2) Those nice jaws and lines that a celebrity has can be drawn







[Written @ 11:33 - 12:11]

An artificial intelligence application is being thought of here.

(1) It uses (1) concentric circles (2) gray theory (3) line spacing ideas (4) and a lot of other random nonesense

.......................................................................

I don't think this software can really be finished

.......................................................................

(1) It is based on concentric circles

.......................................................................

But I think it can be developed to start really well

.......................................................................

And basically you draw concentric circles

.......................................................................

.......................................................................

Those circles are a basic metatron for information communication

.......................................................................

"Metatron" by the way if you don't remember correctly is a hilarious idea from the video game called "Zone of the Enders" . . it might be in "Zone of the Enders 2: Second Runner"

Basically (1) You don't need to pay attention to it. (2) It is a meme that I use to myself as a person in real life called Jon Ide that grew up with that shit called "Zone of the Enders 2" (3) It is basically good shit (4) I think the pleiadians want me to know that it is real (5) basically it is information that has the ability to destroy all (6) and that information from a video game (7) is just hilarious since video games are light hearted objects that you're not necessarily supposed to take seriously.

And also I'm sorry for using the word "shit" I think it is to entice you to listen to what I'm saying and not take it so seriously. It is not poop in my opinion althought the word shit is sometimes used to talk about "strings" that are "dense" in "being" "distrubuputce" or "disruptive"

.......................................................................

strings and crystals notes are here for you to learn more about strings

[1.0]
Strings and Crystals
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/strings-and-crystals.md

.......................................................................

Do you remember that basic introduction to how you can apply strings in your daily life?

to for example (1) make your own personal body design?

[2.0]
Pornography 101 - Day Journal Entry - August 2, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%208%20-%20August/August%202%2C%202021

(2) If you remember correctly

(3) Use strings or yarn to 

(4) Make a doll body

(5) It doesn't need to be super realistic

(6) Just give the stringed items new representational meaning like

(7) The length of this string is 10 centimeters 

(8) The length of this other string is 9.5 centimeters

(9) that means my breasts will be lop-sided

(10) Personally as someone making this doll . . I believe lop-sided breasts are the most hilarious type of beauty platform

(11) And I want to jump on that plank all day long without a care in the world

(12) I will have love-sided breasts and my husband will be impressed

(13) Give me love-sided breasts

(14) And so if you proportionate this string matrix to my real body

(15) Let this length represent maybe a 2x increase to real life standard

.......................................................................

Lady.

.......................................................................

These notes here on this particular page are not detailed enough

.......................................................................

(1) go read the notes from

[1.1]
Strings and Crystals
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/strings-and-crystals.md

[2.1]
Pornography 101 - Day Journal Entry - August 2, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%208%20-%20August/August%202%2C%202021


.......................................................................

Those are basic notes and the entire dialog entry relational journal has nice notebooks

[1.2]
Day Journal

[2.2]
Development Journal - Technological Ideas

[3.2]
Girl Related Things

[4.2]
Journal On Perverted Things / Pornography 101

[4.3]
Journal On Perverted Things

.......................................................................

.......................................................................

Thanks for reading over these notes.

.......................................................................


[Written @ approximately 11:32]

This software is ammazing.

I don't think you will see it being applied on August 9, 2021 when I am writing these notes.

(1) I don't have a lot of time on my hands.

(2) I am talking about Universal Basic Marriage a lot in this latest series of videos.

(3) I am visiting the library

(4) I don't have all the time in the world to live stream

........................................................................

(1) If I had more internet connection

(2) I would be live streaming for longer

(3) Basically this project is being halted for reasons like (1) Internet connection

.......................................................................

There are a million random things to nice

.......................................................................

.......................................................................






