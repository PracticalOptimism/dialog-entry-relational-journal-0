

# Random Hexagonal Scalar Computer

A Random Scalar Hexagonal Computer

(1) Use a crystal-light system

(2) Crystals and Lights can be Studied from [1.0] [2.0]

If you scroll to the bottom of the page you can read more about the references provided by [1.0] and [2.0]

. . 

(1) If you have a crystal matrix.

(2) You can split light.

(3) Light is formed of crystals.

(4) The light shard-crystals that split from a single beam of light

(5) Are basically able to be split even further

. . 

(1) If you split light into waveforms like 'blue' light

(2) 'red' light and 'green' light

(3) You can have a computer dialogue like 'combinations' of 'blue' 'red' and 'green' light

(4) Light crystals are able to show your computer how to trade different light intensities with one another or with a central light ability like a pulsating laser

(5) Pulsating lasers can have 'blue' light or 'green' light and 'red' light

(6) The Red Light is useful for things like sexual energy

(7) The Green light is useful for things like heart healing energy

(8) The Blue light is useful for things like intuition

. . 

(1) Basically you can form light dictionaries

(2) Light dictionaries are useful like the one Unicole Unicron has in her videos on the topic of her 'calendar' which you can find on YouTube [3.0] [4.0]

. . 

. . . . . . . . 

(1) This part is really annoying to read since (1) I am an amateur mathematician and scientist

. . . . . . . . . . . . . . . . . 

(1) Imagine a string <br>
(2) Like a cotton fabric string <br>
(3) Let's say that cotton fabric string or sheet of yarn <br>
(4) Or thread or yarn <br>
(5) Is your light particle <br>

. . 

(1) The light particle is like a wave <br>
(2) So the wave is non-ending <br>
(3) Basically this means the flexibility of the string, the way it can be twisted up and down and spun and rotated cannot be ending . . there are no real 'ends' to the light particle <br>

. . 

(1) The light particle can spin <br>
(2) The light particle can rotate <br>
(3) The light particle can relax <br>

. . 

(1) Light particles can go forward and backward <br>
(2) Light particles can swing on trees <br>
(3) Light particles can sing and dance with other light particles <br>

. . 

(1) This natural intuition should help you (1) Relax your expectations of what a light particle can and cannot do <br>
(2) It can be interesting <br>

. . 

(1) If your light particle is a jealous young man <br>
(2) Sitting on a couch <br>
(3) Then forgive me, but maybe they will fail their next exam <br>

. . 

(1) If your light particle is a young man <br>
(2) Who is not jealous <br>
(3) Then maybe they will pass <br>

. . 

. . . . . 

(1) It is all a matter of time and tension

. . 

(1) Time is how often you send messages to other light particles

. . 

(2) Tension is the durability of the light wave to participate in that light energy frequency radiation

. . 

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . 

. . 

(1) Time is like how many times you can pulse the laser per given amount of seconds

. . 

(2) Tension is like 'who are the individuals involved'

. . . . . . . . . . . . . . . . . . . . . . . . 

. . 

'Light' 'Crystals' are basically like normal crystals if you would like . . but they are light particles instead of . . physical real world particles . . 

. . 

'Light Crystals' . . can share information

. . 

'Light Crystals' can share information

. . 

That's right

. . 

To build a teleporter

. . 

Use your light

. . 

To transport other light

. . 

And that light may or may not arrive where you expected it to

. . 

But that light will arrive

. . 

O _ O 

. . 

...............................................

........................

....

(1) If you want to build an ordinary digital computer

(2) Using light crystals

(3) You can scale into the light

(4) With ordered frequencies

. . 

(1) These ordered frequencies

(2) Ought to be unique

(3) So that aliens don't interfere with them

. . 

(1) However

(2) To retrieve information

(3) You need to repeat the message 

(4) Of creating new light formulas

(5) Inside each and every crystal you visit

(6) New light formulas

(7) Are created

(8) Only by one frequency

(9) The green light frequency

(10) The heart frequency

(11) The heart frequency can be embedded inside of every other frequency

(12) The heart frequency is nice

(13) The heart frequency is special

(14) The heart frequency is family

. . 

. . . . . . . . . 

. . 

(1) Something about creating new frequencies

(2) And remembering what those frequencies were by doing some calculations with other frequencies

(3) Something about those processes 

(4) Allows for infinite memory computers

(5) Allows for computers that aren't greedy

(6) Allows for computers that don't need to slow down

. . 

. . . . . . . . . . . . . . . . . . 

...................................................

. . 
























References:

[1.0]
Strings and Crystals
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/strings-and-crystals.md

[2.0]
Light-Based Computers
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/light-based-computers.md

[3.0]
How to Use UniPlanner
By Unicole Unicron
https://www.youtube.com/watch?v=eySRQch7JFY

[4.0]
The Best Bullet Journal Mood Tracker for Mental Health (BPD, Depression, Anxiety)
By Unicole Unicron
https://www.youtube.com/watch?v=eU4uqrjqzpE


