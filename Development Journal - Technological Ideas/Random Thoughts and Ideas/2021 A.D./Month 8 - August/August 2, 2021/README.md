
# Random Thoughts And Ideas - Day Journal Entry - August 2, 2021


[Written @ 9:57]

Look around you.

(1) The world is full of color.

(1) You can see a person.

You can see what they look like

(3) You can see that they wear clothes

(4) You can see that there are places where they could have walked to

(5) You can see that they could have stood tall at that position

(6) You can see that there are other people that could have stood there

(7) This is quite an advanced theory

(8) Close your eyes if you don't want to look

........................................................................

(1) Sex is everywhere

(2) Even around us

(3) Even in front of our very eyes

(4) Right now

(5) As we look out into the world

(6) We see words

(7) Computers

(8) Laptops

(9) Phones

(10) Age gaps

(11) People

(12) Aliens

(13) Haircuts

(14) Braids

(15) Braided hair

........................................................................

Our eyes

Are open to the possibility

(1) I am turning right

(2) I am turning left

I see different things in my right and left

(1) people are on the right

(2) people are on the left

(3) These are all like different gray scale offerings of what is around us

........................................................................

(1) Look at an image of a cartoon manga notebook

........................................................................

It's a really long story

but basically my mind is starting to trace the thought

(1) if you want to draw a professional cartoon manga notebook page

You basically need to do 1 thing

(1) shade the page gray

........................................................................

Creepy thought

........................................................................

(1) If I start with a white sheet of paper

(2) now is the only moment is a theory but L.O.L. this is a hilarious idea . . gray all around you O _ O it's like O _ O what's your name again ? -_- Are you going to cheat me? Will you look at me when I didn't expect you to be there? -_- will that one random person walk out from the corner? -_- who the hell is our president now? -_- L.O.L. what the hell is actually happening. Who are these people? Why are they so -_- gray. Scale. paintings. -_- O _ O gray scale. It's like O _ O Am I in a museum? O _ O with gray? So the future is gray? And the past is gray. Well actually sometimes the past is black depending on your perspective you might be like a futurist who thinks O _ O I might as well live in the present moment . . O _ O the future is literally actually black as well O _ O since O _ O all the rooms around us are gray O _ O so it's just O _ O this dumb moment of O _ O my eyes . . my nose . . O _ O what am I actually look at? O _ O Gray stuff. Like keyboards. And gray stuff like interesting voice partners O _ O that one interesting voice. It's like gray. A nice gray. Gray. Gray. Gray. Gray. 

L.O.L.

Social Stigma but also scientific hypothesis.

(1) What does it take to draw a nice black-and-white comic book page?

(2) paint the page gray

(3) from a distance no one will tell the difference

(4) they will clap their hands

(5) good job

(6) you're an expert

(7) up close

(8) you have the perfect chance

(9) to keep the page as gray as possible

(10) and say to yourself

(11) move these colors here

(12) move those colors there

(13) which are good

(14) even a black page is gray

(15) black O _ O in this theory stuff is gray

(16) white is gray

(17) just walk to the left or walk to the right

(18) you have a gray photo

(19) and so if you have for exaple a comic book with literal black on all the pages which is just a complete black out page for all the pages

(20) then O _ O

(21) What do you mean that these represent gray tones?

(22) well the idea of black is like O _ O

(23) super stition and aggression

(24) So if it's angry or something like that O _ O

(25) And white represents

(26) Pure experience

(27) Where there is no experience of anger or pain or gratitude

(28) so then it's O _ O

(29) I can't even appreciate those gray realities over there

(30) The ones with Milena Velba's thighs

(31) Oh well

(32) L.O.L. That is a white reality to me xD

(33) L.O.L. I am just kidding

(34) Theories are hard to look at now

But check this out

Gray as a nice color for you to think about is nice

(1) because it could possibly improve Akiane Kramarik's work

(2) Gray is nice because it is a nice soft nutral tone

(3) it isn't too hot

(4) It isn't too cold

(5) a nice light gray can make you feel like "There is opportunity here"

(6) I can dance with this color and try to make it happy

(7) A happy picture with bright skies and rainbows

(8) Or even nice romantic soft flairs of other mystery colors

(9) like glow in the dark ideas

(10) l.o.l.

I don't know

It's all just a theory.

Also don't forget.

Grays are like a nice silver material

Computers that use gray

Are like

O _ O 

you can be inspired by that

Since like a lot of metals can be silver

And silver is like O _ O ball bearings can be colored silver or gray

And it is a nice neutral color

And 

O _ O 

If you take this theory even further

It's like

(1) you're in a room

(2) you have a transparent highlight of a gray background behind the experience you are having

(3) And then that makes it clear (1) I have a nice place to start my painting

(2) and possibly capture the fullness of it to some extent

(3) You can use scales of gray to capture more and more ideas

(4) remember how water is like possibly representable as shades of gray in a photograph

(5) or gradients of gray?

(6) So then you could possibly have weird things like

(7) Have gradients within gradients

(8) Spiked gradients within gradients like

0 to 50 percent gray on one side of the paper

and between that sheet of paper

0 to 25 percenter gray on the left side of the paper to the half way point

0 to 12.5 percent gray on the left side of the paper at 1 quarter way point

L.O.L

Full page length:
...................................................................
[full-page-length]
[0 to 100 percent gray gradient]
[start from 0% on the left]
[end with 100% black on the right]

...............................
[half-page-length]
[0 to 50 percent gray gradient]
[start from 0% on the left]
[end with 50% black on the right]

..............
[quarter-page-length]
[0 to 25 percent gray gradient]
[start with 0% on the left]
[end with 25% black on the right]


Black and gray are interchanged in words in this writing . . but gray is meaning literal gray which is supposed to be the base for black and white both.

L.O.L.

White experiences are only experienced through gray right?

L.O.L.

So for example a beautiful sunny day is like a white experience but you had to have clouds of gray for other ideas like "Is it raining outside" "Is someone holding an umbrella over my head" "Am I walking to the park?"

And so even if you are having a blissful and heavy love day of white colors . . then it's like O _ O what are all those gray clouds of "missed experiences" that you need to block out to experience all this lovely white?

"A man is beating up a dog in front of his yard" "on the other side of the world" "The man is pummelling his dog with his fists" and while you're there having your nice peaceful white shower of experience

That poor dog is being beat up.

Good thing you're ignoring it with your gray clouds of "I don't care"

L.O.L.

........................................................................

And so long as you don't care

........................................................................

It's like O _ O

........................................................................

A lot of things are gray

........................................................................

You missed that random person's funeral

........................................................................

How sad

........................................................................

Gangsters are doing crime

........................................................................

"I'm not a police officer"

........................................................................

Haha

That is a huge social stigma

........................................................................

That cute girl won't talk to me again

........................................................................

What a gray idea

........................................................................

And black is like the same as white right 

If you're having a not so good day

Well it's still gray right? Since someone on the other side of the world could fix that problem for you right?

Or maybe you don't feel that way?

If you're completely black O _ O

Woah

But c'mon this theory says that you're mostly gray since it's like O _ O tomorrow you might feel better

Or you just need a cup of frozen yogurt.

L.O.L.

Or gray frozen yogurt.

L.O.L.

........................................................................

And computers could really be amazing.

If you apply this theory with different gradients of gray embedded within one another

All you have is like

really brilliant computers that can think

........................................................................

Brilliant is an exaggeration

They aren't exactly white but you can try to exaggerate the whiteness of the computers

........................................................................

If you can imagine strips of gray vertical lines

And those strips of gray vertical lines represent different things like (1) different realities or (2) different people in your life (3) or different numbers

What is the possibility of elaborating on the grayness of those lines?

(1) it's like

(2) try to short circuit the idea of using rubber band systems to make gray lines

(3) random gray lines could be fine

(4) and try to replace all those random gray lines with more random gray lines

(5) and all the random gray tracings you draw on your sheet of paper

(6) are your new memory system

(7) so it's like O _ O

(8) If you shade on a single sheet of paper

(9) how many shavings can that sheet of paper hold?

(10) and those shavings basically represent

(11) your memory model

(12) and as long as you can shade more grayness 

(13) you can use a random sheet of paper

(14) and get all the colors

(15) by making the sheet black

(16) and adding more paper

(17) when you need to

........................................................................

L.O.L.

(1) have a sheet of paper

(2) paper is hilarious

(3) you're supposed to use paper for fun and for starters

(4) do you notice how if you shade a sheet of paper with a gray pencil

(5) then it will look gray?

(6) but basically that's what you're doing (1) when you write using words (2) when you draw (3) and all the things in between when you shade a sheet of paper

(7) so basically you just need to (1) "retrieve your embedded information" like "reading" 

(8) reading scribbles is hilarious

(9) So writing in english is good

(10) drawing a circle or a recognizable shape isn't bad

(11) but if you don't recognize it, you'll say "scribble"

(12) but you need to O _ O 

(1) look at the scales

(2) that we talked about

(3) how many times was the pencil lifted

(4) and restarted

(5) like having sex with Eugenia Cooney

(6) and then things like -_-

(7) hell I can't believe Eugnenia feels this way

And basically that's a really extreme example of a computer.

(1) it can be difficult to wrap your mind across if you build it to its algebraic extremes. Algebraic extremes like having sex with eugenia cooney is really like painters and artists really being happy and happy all day and not thinking about a lot.

(2) letting gods of Eugenia Cooney counterparts hug them and feel their bodies

(3) gods of Eugenia Cooney counterparts with goth looks and emo looks and nothing better to do then to dream and create

(4) might as well create my life with my perfect Jack Skellington future husband the girl says to Jon Ide only

(5) L.O.L.

Propaganda

L.O.L.

That is really funny.

Well you don't need to build that particular computer.

I'll build it for you.

And you'll be like working on other things.

And so then.

Uh.

Hmm.

Minimum level ideas below that extremely good computer

Are things like

uh

I don't know

rubberband balls that are interesting right?

-_-

rubberbands

You need to be careful

These are basically electrolytically active and can shock you to death very easily

That's why placing them in soups of other electric gates like nice metal encasings could possibly help you electrify them at runtime for different speeds of information acquisition.

L.O.L.

Yea.

(1) I don't know what I'm talking about

(2) These are interesting idea

(3) random computers

(4) uh

(5) 

L.O.L.

I forget where I was

.......................................................................

I am lost

.......................................................................

I think I need to get back to a string or thought that I had before

.......................................................................

Goodbye to this note

.......................................................................

See you later.

.......................................................................







[Written @ 9:09 - 9:47]

Today I was walking to the library and had the random thought

(1) wow

(2) if you are drawing on a sheet of paper

(3) take a look at what you're drawing

(4) if you're drawing a person

(5) like a woman with gigantomastia breasts

(6) or a 2 year old girl

(7) or a handsom tall person

(8) or a genderless blob

(9) or trees

(10) or forestry equipment

(11) or even buildings

I mean take a look at all the opportunities you have set out before you

O _ O

This shocking new thought was like O _ O wow [mind-blown-emoji]

Which by the way I have another theory that could be mind blowing that I had 2 or 3 days ago.

[Remind me to talk about it]
[It is about gray colors]
[Gray colors are like interesting]
[(1) they are like water]
[(2) water is colored gray if you look at it right?]
[(3) if there isn't like a uh . . algae or uh chlorine coloration???]
[(4) I don't really know what gives water its color]
[(5) I've very sorry if I'm wrong]
[(6) but basically when you color water on a sheet of paper]
[You can get away with a lot if you use gray as a nice color]

........................................................................

Okay well

Back to the original thoughts

(1) if you're drawing people

(2) or flowers

(3) onto a 2D flat canvas

(4) O _ O Strangely enough this could even possibly work for different types of canvases like O _ O

(5) O _ O You have no idea

(6) virtual reality O _ O 

(7)

........................................................................

Okay

O _ O One of the most brilliant things about this idea

(1) it uses yarn

(2) yarn is like strands of cotton fiber that you can find at the store

(3) strangely enough it is quite abundant for Americans which is where I am from . . The United States of America

(4) But unfortunately . . if yarn is really popular like this idea supposes (5) then O _ O fuck you let the kids use it first and don't hog all the yarn

(5) I'm just joking O _ O

(6) high schoolers and middle schoolers should use the stuff first

(7) take a ball of yarn

(8) imagine an idea

(9) and start to make the idea a reality using the yarn

(10) basically you have to fold and twist and turn the yarn

(11) and so if you use gray yarn as a base color that's really trippy

(12) you might want to use something like O _ O colorful different rainbows of yarn

(13) gray, white black all make it look like a water painting

(14) so if you want your nice idea to come out like a blue grass yard of water

(15) then maybe use gray and white and black

(16) but basically

(17) if your city or town needs buildings

(18) design those buildings with your hands directly in a small scale that is really useful like O _ O they should look like this

(19) but it's not only about your physical eyes and how comfortable you are with your looks

(20) basically become a programmer like me and learn to see beyond the 3d graphics of the world

(21) look at things like O _ O random objects

(22) L.O.L. basically learn to appreciate different random things around you

(23) "How would I appreciate this from another perspective?"

(24) L.O.L. What is the meaning of life?

(25) Appreciating random things

(26) And then you can of course use your nonesense nose and eyes and senses to recreate those things in new folding patterns

(27) folding patterns is a weak word. You are a creator. You can create much more than that if you twist and turn your mind upside down right?

(28) basically start with a simple video game

(29) make a player character from yarn

(30) measure out the length of their arms

(31) what will the length of their torso look like

(32) how many arms will they have

(33) will they have gigantomastia breasts?

(34) l.o.l. use just 1 string for each breast

(35) measure out the breast length.

(36) and then that lets you learn things like 'proportions' which is an important topic in mathematics

(37) 'proportions'

(38) proportions

(39) your teacher in your high school class room is basically a proportional scaled model of yourself right?

(40) they are X meters taller than you

(41) they have x-y-z different accolades from different universities which is another proportion that you don't pay attention to

(42) They masturbated more times then you

(43) they ate more dinners than you

(44) all of these "more" or "less" metrics just means they are like you but have "more" or "less" different "more" or "less" things that they pay attention to

(45) I have fucked 10 virgins in the past says one teacher

(46) and to that you say. Alright. My player character will carry a notebook about that topic in their heart

(47) Here is a string to represent their heart

(48) a circle yarn that is colored red

(49) A CIRCLE YARN THAT IS COLORED RED

(50) And there is another yarn that grows from that

(51) Just tie it together

(52) That has 10 yarns on that yarn

(53) And so you have that representation of 10 bitches fucked for the first time ever in their heart circle

(54) And of course probably if the person is happy of that accomplishment

(55) you can represent that with a happy yellow yarn next to that earlier nice 10-string-copied yarn

.......................................................................

Basically if you really want an example . . check this out..

........................................................................





This is the red yarn . . it can be a circle or a straight line . . however you like it . . 

[red yarn]
.............................................................
     .                                     .
     .[black yarn]                         . [yellow yarn]
     .[list of girls]                      . [represents]
     .[virgin eliminated]                  . [happiness]
     .                                     .
     .                                     .
     .                                     
     .
     .[rebecca marble, first girl]
     ..........................................
     .
     .[janica harlet, second girl]
     ..........................................
     .
     .
     .[stealy stealy, third girl]
     ..............................................
     .
     .
     .[claudia walsh, fourth girl]
     ............................................
     .
     .
     .[claudia walsh 2, fifth girl]
     ..........................................
     .
     .
     .[claudia walsh 3, sixth girl]
     ...........................................
     .
     .
     .
     .[claudia walsh 4, seventh girl]
     ...........................................
     .
     .
     .
     .[claudia walsh 5, eigth girl]
     ...........................................
     .
     .
     .
     .[claudia walsh's mother, ninth girl]
     ............................................
     .
     .
     .[claudia walsh's grandmother, tenth girl]
     ............................................
     .
     .
     .
     .
     .
     .
     .
     .


........................................................................

This is only an example

........................................................................

Impossible relations like (1) stealing a girlfriend's mother's virginity might be something you uh . . couldn't really explain well.

(2) so maybe there are other mathematics theorems that (1) you need to learn about (2) you haven't given yourself the time to express (3) things that you could theorize about in the future

To fill gaps in your abstract string object

representation of goals or ideas or random abstract things

you could always just leave Dot Dot Dots in your mind and wait for someone else to maybe have their own stringy knot object to help you out

........................................................................

You can make characters using 1 line strings for "torsos" 1 line strings for "legs" and "1 line strings" of the arms

And all of a sudden . . if you really want to be a good artist and get sweet positions or sweet flexture positions in a photograph

You can just make a nice knotted doll which takes you 5 minutes if you're working with 1 string for each arm body proportion or something like that

And if you have more models to work with, you just need to measure their height to length or other ideas ratios

And then scale that down onto your nice yarn model

That nice yarn model

Can then help you twist and turn it to flat map it onto a page of paper

That page of paper can (1) have the advantage of having new and unique positions for you to work with

(2) animations like cartoon animations can benefit a great deal from this

(3) even nice new arms can be created at runtime so to speak

(4) your character can have many representations depending on the scene

(5) in one scene they are 8 feet tall compared to everyone else

(6) in another scene they are 9 feet tall

(7) in yet another scene when there is a shrunken person effect, maybe a silly animation like when cartoon characters are "cute-ified" they can be super small with big heads and small tiny arms that are fat and chubby like a nice chubby small kid

(8) and so you can make all of these dolls very quickly and easily

(9) you'll be able to make 100's or thousands

If time allows you

(10) and your animation can be so unique and magical

(11) and you can place all those characters in your backpack

(12) and transport them anywhere you walk to right?

(13) and then get them out again when you're ready to draw

(14) And of course you can even try to uh . . fold them in ways that are uh . . like you can tape the structure in place

(15) That might require some thinking on your part

(16) Say you make a miniature building model

(17) you can then try to keep it in place

(18) like an architect would do

(19) with a small scale city in their basement

(20) and with cars and railways

(21) And 1 string models of people walking around

(22) So then basically you can create fake worlds

(23) And then if you need to represent them in other places

(24) You can do that

(25)

........................................................................

Represent them on sheets of paper

........................................................................

Represent them in your neighborhoods

........................................................................

Represent them in computer graphics systems

........................................................................

Represent them in not a girls vagina because I told you already that she's my girlfriend and not yours

........................................................................

L.O.L.

........................................................................

But basically this should be a nice illusion for you to work together

........................................................................

And don't forget

Strings of yarn are just the beginning

Strings of thought

And strings od light

And strings of other materials are possibly fun to work with

Like metal

And wood

And if you use strings of yarn to start with

That's really amazing

Since they are affordable (1) and they are easy to work with with your hands on your desk

(1) Don't forget to stop messing around and build robots that walk around cities to pick trash up first

(2) then you can build your own Dr. Octopus arms for appendages

(3) then you can build simple cartoon golf cart mounts like pets from world of warcraft

(4) but don't go too far so people can think if that is the type of photograph we should all be painting together

(5) L.O.L.

(6) fat people

L.O.L.

........................................................................

You can make all sorts of things . . just try it out 

Don't forget theories like

(1) rubber is also really cool.

Rubber is basically hard to work with.

(1) It is too sticky

(2) It is too bouncy

So mentally you don't feel as comfortable or free form when you work with it like you do with strings.

Rubber is bouncy though . . and it is timeless

(1) try having yarns made of rubberbands

(2) It's really goofy

(3) But your electronics equipment would be very different

(4) basically it would be hectically better

(5) L.O.L.

(6) Super intelligent rubber band electronics.

(7) buildings that can sprint and jump and run in place

L.O.L.

These are all things that you can theorize about

Like rubberbands are strings within strings that are stringy

........................................................................

So please be careful and have fun

........................................................................

Yes you should also work on different materials to possibly replace paper and other things

........................................................................

`Rubber band systems like hydroelectronic plates or sticky rubbery glue that is able to play other roles thanks to light frequencies could possibly be a good start`

........................................................................

Which is basically a good idea right?

Car frames could be build in realtime by human hands and then placed in light rooms that make them concrete.

L.O.L.

If you have light rooms that let you scale down versions of the rubberband to different scales. That's hilarious.

That means you can work on a model .  -_-

Then it shrinks and stretches

-_-

-_- and basically that's hilarious

........................................................................

Oooey

Gooey

........................................................................

It's all just a theory

........................................................................

I don't know if it helps you.

........................................................................



........................................................................


Summarizing Idea

4 Listed Ideas


- Rubberband systems (Line 455)

- Cartoon animations can improve if you use string yarn models before drawing

- Your pencil-hand-drawings can improve if you use string-yarn models before drawing

- It's only maybe a big deal. If you're looking for certain effects.

- Otherwise using old familiar techniques is also fine.
















