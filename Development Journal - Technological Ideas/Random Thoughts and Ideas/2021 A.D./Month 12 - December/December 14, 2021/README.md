

# Development Journal - Technological Ideas - Random Thoughts And Ideas - December 14, 2021







### [Written @ 20:10]

(1) Today's episode may be long. I'm not sure.

(1) There'a a lot to teach.

(1) If you are watching. Congratulations. You are really gifted at watching a lot of videos.

.......................................................................

(1) Do you remember our topic about "E" that was talked about in the Day Journal Entry - December 14, 2021

.......................................................................

Do you remember E1 = 10. and E2 = 100. And E3 = 1,000.

Do you remember that E stands for x 10 ^ N

So

EN is equal to 10 ^ N

??

.......................................................................

Anyway

You can have awesome notation.

There's a course called "Numbertron"

.......................................................................

I haven't studied Numbertron for very long.

(1) 

But there are some really crazy numbers.

Like.

Uh. I don't know.

.......................................................................

EL[2, 1] = 

ELEL1 = 

EL(E[1]) = 

EL(10) =

E[1, 1, 1, 1, 1, 1, 1, 1, 1, 1] =

.....................

E[1] = E1 = 10
E[1, 1] = EE[1] = EE1 = E10 = 10,000,000,000
E[1, 1, 1] = E[E[1, 1], 1] = E[10,000,000,000, 1] = EE...EEE1 = (10 million E's followed by a 1)

....................

E[1, 1, 1, 1] = E[E[1, 1, 1], 1, 1]

....................

E[2, 1, 1, 1] = E[E[E[1, 1, 1], 1, 1], 1, 1]

....................

E[3, 1, 1, 1] = E[E[E[E[1, 1, 1], 1, 1], 1, 1], 1, 1]

....................

E[3, 2, 1, 1] = E[E[E[E[2, 1, 1], 1, 1], 1, 1], 1, 1]

....................

E[3, 2, 4, 5] = E[E[E[E[2, 4, 5], 4, 5]], 4, 5]

....................

Remember:

E is the x 10 ^ N

SO

EN is . . . like . . 10 ^ N

So 

E100 is . . 10 ^ 100

And of course

EE100 is 10 ^ (10 ^ 100)

....................

EEE is 10 ^ (10 ^ (10 ^ 100))

....................

Remember that

(1) Google's YouTube has X amount of petabytes in their data stores. It doesn't really matter but I did the math and it seems to be less than E20 number of bits . . 

(2) E20 number of bits . . that's so small

(3) Think about the size of (1) EL[2, 1] . . Do you remember that L means 'length'

EL is like . . E and then . . the length of the array that follows EL . . so for example . . that array that gives us those calculations from above . . 

EL3 . . That is . . E[1, 1, 1]

EL4 . . that is . . E[1, 1, 1, 1]

.....................

Anyway . . 

ELELELELEL1

Has the same notation

EL[4, 1]

. . that means there are 4 'EL's After the first EL . . so for example

'EL' and then followed by 'EL' 'EL' 'EL' 'EL'

......................

EL[4, 1] = ELELELELEL1

......................

-_-

......................

ELLL is different from EL

......................

ELLLLLLLLLLL is different from EL and ELL and ELLL

......................

Basically you can add more and more letters of "L" that make the number larger and larger

......................

And then of course you can invent new letters or new characters

......................




.......................................................................

EL[2, 1] is HUGE? Right???

Haha.

.......................................................................

EL[3, 1]
EL[4, 1]

.......................................................................

You must be so proud

.......................................................................

These are really large numbers

.......................................................................

You can draw circles on a notebook paper.

.......................................................................

And fill the page with a row of circles followed by many rows of these rows

.......................................................................

The first row could have 15 circles

The second row could have 15 circles

The third row could have 15 circles

.......................................................................

(1) The rows are there

.......................................................................

Remember that YouTube has less than 21 circles led by a 1

1 followed by 21 circles or (zeros)

1 followed by 21 zeros

1,000,000,000,000,000,000,000

.......................................................................

E21 is the most that YouTube has in number of bits for their storage capacity.

That is quite small.

.......................................................................

Why is it small?

.......................................................................

21 circles is not the full page of circles

.......................................................................

Only 2 rows of circles on the page?

.......................................................................

That is such a small amount.

.......................................................................

We could have 26 rows total on the page.

.......................................................................

15 circles per row

.......................................................................

If you imagine the circles represent zeros

.......................................................................

And you draw a 1 in front of all of those zeros

.......................................................................

So then

1 followed by 26 * 15 circles

.......................................................................

1 followed by 390 zeros

.......................................................................

E390 is a lot bigger than E21

.......................................................................

So then, YouTube doesn't beat this other company here called E390

.......................................................................

By the way that's a cool company name.

.......................................................................

E390 has a lot of data storage capacity

.......................................................................

But don't you think that's kind of weak

.......................................................................

EEEEEEEEEEEEEEE10

.......................................................................

E[14, 10]

That is

EEEEEEEEEEEEEEE10

.......................................................................

That is a much bigger number O _ O

.......................................................................

E[14, 10]

Means

First you have E10 . . the one on the far right . . 

And then you . . take the E of whatever that number is . . 

E10 = A

. . . 

EA . . . so what is EA??? . . probably a really big number right?

hahahaha

......

And then

You take that really big number

And place that in front of the next E

EA = B

EB . . . . . so that is really a big number right???

......

So then that is a big number right??

Well just imagine taking the E of that..

In any case . . you repeat this process for 14 E's right?

......


And so it's really cool

......

.......................................................................

E[14, 10]

.......................................................................

E[14, 99] = EEEEEEEEEEEEEEE99

.......................................................................

.......................................................................

E[0, 1] = E[1] = E1 = 10

.......................................................................

E[1, 1] = EE[1] = EE1 = E10 = 10,000,000,000

.......................................................................

So isn't all of that very interesting

.......................................................................

.......................................................................











