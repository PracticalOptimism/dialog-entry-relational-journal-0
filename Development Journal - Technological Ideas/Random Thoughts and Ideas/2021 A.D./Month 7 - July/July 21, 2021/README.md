

# Random Thoughts and Ideas - July 21, 2021


[Written @ 9:59]




[Written @ 9:45]

I have some random things in front of me now . . 

(1) a paper cup

. . 

The idea is that "wouldn't it be nice to be able to carry this paper cup while it still has nice cups of tea inside of it?"

I am carrying this paper cup around with me to be able to re-use it later

. . 

The paper cup -_- . . so cool . . if you use the 'string' theory idea or something like that . . 

. . 

Then basically the idea is that there is -_- whatever -_-

. . 

Strings are so amazing . . if I tell you a story about how to make something using strings you'll be like 'yea' 'that's a nice WEAVING' pattern . . and so then it's like -_- alright . . 'do you have a better WEAVING pattern' in mind then?

. . 

So that's why I am saying 'whatever' since you can probably figure it out . . how do you make a paper cup from a string system? . . probably you can make a prototype of a simple paper cup can't you? It's made of yarn but still you basically need the idea that 'it could possibly hold fluids' or 'it could possibly hold liquids' . . and of course you need a framework which is like 'yea' this is like a 'model' of a 'cup' and you're supposed to actually replace some of these 'invisible ideas' with 'real' 'paper' 'material' or something like that . . 

. . 

Just like you wouldn't necessarily want to make a whole blanket to make the walls of a model house

. . 

You would possibly want to make like a rectangle of a string system or something like that . . and so the invisible idea inside of the rectangle is the 'erect' 'wall'

. . 

And of course you can make more weaving patterns that you can think about

. . 

Like why shouldn't the wall have some intermediate strings to show the horizontal or vertical uh . . things that would be there . . 

. . 

And so you can have 1 or 2 or however many more lines of strings that you want

. . 

And of course . . remember . . this is like art class . . your string building prototype . . can uh . . take inspiration from other stringy ideas . . 

. . 

Like why not use pillars like cylindrical cones . . for the four-corners of the house . . 

. . 

Or whatever . . 

. . 

It doesn't have to be a straight string for all the corners you know . . 

. . 

You can replace all your straight strings with more complicated string systems . . like more uh . . different ideas that you come up with . . 

. . 

All the 'in-between' things like 'place' that there . . is then up to you to map out . . like 'that should be metal' and 'that should be stone' and 'that should be wood' . . 

. . 

And so on and so forth . . 

. . 

You can make scissors out of strings . . and then be like 'that should be a metal' 'that should be sharp' 'this should be a plastic handle' . . 

. . 

and all that took you was tying 2 strings together right?

. . 

To form an "X" pattern right?

. . 

Isn't that so cool?

Don't you like that way of building?

It should be like a pornography . . if you can plan out your entire staircase like that . . then you can have (1) traditional staircases (2) new types of staircases (3) whatever your friends and you can come up with

. . 

. . . . . . . . 

Okay well . . I have a problem on my hands . . (1) a paper cup . . 

How do you have it be able to hold fluids or liquids or things?

. . 

I haven't thought about this problem yet but I'm jogging my mind to see how far this "string" idea goes . . 

. . 

Well . . probably you can just have a cylindrical cone shaped thing xD . . and make sure that you fill the inside with liquid and that you use the right material right? xD So then wow . . it's pretty much like a bottle xD . . I was able to transform my 'paper' 'cup' into a bottle . . 

. . 

Or like a pouch or something like that . . a pouch of liquid xD . . 

. . 

The paper cup is amazing. the paper is quite thick

. . 

xD

. . 

A toilet made of paper ?? xD

. . 

That's amazing . . then you could even then dispose of the toilet . . after 20 uses right? xD

. . 

Thick paper?

. . 

[Written @ 9:09]

(1) Smoke and Mirror as a "Grease" and "Metal" System
(2) ?? - I forgot - I need time to remember - ??
(3) ?? - I forgot - I need time to remember - ??

. . 

I had a few interesting thoughts a few minutes ago and also this morning and possibly in the last few days

. . 

(1) Smoke and Mirrors as an analogy could also be related to the topic of 

"Grease" and "Metal"

Since Smoke is like "Grease" system . . 

Uh . . And mirrors could possibly be a "metal" "system" uh especially since they are also related to "light" and "ight" and "metal" seem to be related in some ways . . Possibly . . 

. . 

I was walking today and saw a pile of mud . . and the pile of mud didn't fit into some of my ideas about "strings" . . "how is mud like strings?" . . and so then I thought to myself . . for a few uh . . random non-linear thought seconds . . 

. . 

Uh . . the idea came up that "strings" are possibly related too much to "metals" and so for example . . "metals" are like a "standard" "defacto" way for "saying" "things" like "yes" "that is the way it is"

. . 

"Metal" "Can't" "be" "beat"

. . 

You can "trust" metal

. . 

"Metal" "is" "strong"

. . 

And so if metal is strong . . uh . . well . . yea . . uh . . Strings as an idea . . which those notes were taken here [Strings and Crystals](../strings-and-crystals.md)

. . 

Metals are interesting . . so for example . . strings are too much associated with . . metals . . since 'strings' are like 'you can trust me' . .

'I am solid'

. . 

'I am real'

. . 

And even though strings are solid and real . . well . . that makes them 'more' 'metal' like than . . uh . . otherwise . .

. . 

But so well then . . alright . . uh . . then . . Grease . . can you imagine 'grease' 'strings' or 'strings' made of 'grease'?

. . 

Alright . . so this pile of mud then is basically a 'new type of item' . . since Strings are more 'metal' like . . then . . 'grease-strings' are more grease like . . 

. . 

So 'grease' strings are pretty much the answer to having things like 'types' of 'grease' 'systems' that 'behave' like 'strings'

. . 

The Behavior of strings is talked about in the notes provided here [Strings and Crystals](../strings-and-crystals.md)

. . 

Strings in general seem quite cool to play with in your mind . . you can have 1 string or 2 strings or 3 strings and you can tie them up very easily . . and in real life . . you can buy them at a cheap and affordable rate . . and for goodness sake . . it is beginning to surprise me how wonderful they are in terms of building 'physical real world prototypes of things' like 'cars' and 'bicycles' and 'buildings' and 'people' and 'pulley systems' and 'transportation' 'systems' and all sorts of things can be made with yarn . . or cotton fabric strings . . and all you have to do is tie things together or lay them side by side . . and when you're done with your physical prototype . . of that thing of interest . . 

. . 

Maybe it's like a mandelbrot set . . with different colors and different scalings of things so it's very complicated but if you lay things out and measure them then maybe it's easy and possible . . it really depends on how 'stringy' your 'mind' is. Or how 'flexible' your mind is . . 

. . 

Scalings are so cool when it comes to strings . . yarn . . physical strings are so cool . . you can toss them around and they are soft and easy to handle . . children don't have so many problems with them except for possibly the dye that is used . . if they children are eating or chewing on them . . well . . uh . . 

. . 

You can use them to build prototype houses . . Even the prototypes can be 'to-scale' 'models' and so the scaling isn't a big deal . . 

. . 

Imagine building a Sky Scrapper building from real yarn . . isn't that a cool idea . . and then you and your construction team can be . . like . . yo 'I don't think this looks cool' . . so then . . all your effort in placing up the real world physical string model is like 'yea alright' 'so it's okay to have 'real world examples' made of 'strings'' and then 'replace the strings with 'metals' and 'woods' and other 'fabrics' that are strong . . 

. . 

Right . . so strings are so flexible that then your construction team can take that yarn model real-world-scaling prototype of a building and then move it to another location and start real-world-visualizing the way the building would look if it were continued to be constructed there . . 

. . 

And so then . . it's just a matter of time until some brilliant engineer on your team suggests 'let's make a whole neighborhood of 'real-world-scaling-buildings' and houses . . that would show what a whole neighborhood would look like . . it's like 'see it before you buy it right?' and guess what . . ? constructing all of this is cheap and affordable with plant fabric strings right?

. . 

And yarn is fun to work with . . 

. .

And you can cut and trim it very quickly to add more things or remove previous things.

. . 

Basically even your physical computer robotics systems can be made from strings . . I mean . . really think about it . . Puppets are computers that are basically like robot systems . . Aren't they really cool?

. . 

You can have puppets that move from strings being pulled here and there . . 

. . 

And they can move things from this warehouse to that warehouse . . and so on and so forth

. . 

Well basically all the cool ideas like cars and trains and trucks and airplanes can be made from strings . . An electric motor you ask? How do you make an electric motor from strings? . . Ahh . . that's where the idea of "grease-strings" might come into play . . otherwise you might have to pull the string yourself like . . "spinning the string around in your hand" . . 

. . 

"grease-string" systems could possibly be like "gravity" or even "smoke" or even "grease" . . 

. . 

If you have lines of the material that are nice . . uh . . and "string-like" I guess -_- personally I'm not really sure how to interpret "string-like" at this time . . since the idea of strings is like "possibly" "metal-like" . . and so . . uh . . "metal?" . . well

. . -_- on the one hand you can say "string-like" means something like -_- "stick together" or "in a row" or "sequenced" like you would place one book next to another . . but that is a sort of "handicap" idea since in your mind you are visualizing a book and books are like -_- if you want you can imagine . . 1 book is like 1 string . . and it can be a short string or a long string and you have a finite length . . 

But then think about it . . a second book isn't -_- like . . tied to the first book right? . . uh . . well maybe they use the same language in the book . . or the same author wrote the book . . or maybe the physical material of the book is the same . . but uh . . "tying" yourself to a book analogy for "strings" is like "objects" that "lay" next to one another are "tied" "together" . . 

. . 

In space terms it's easy to "tie" things together and say that is a "string" but uh . . it's kind of possibly a handicap idea to say that since you're not letting flexible other things be possible . . 

. . 

Like 2 people that don't like each other that stand next to each other might not consider that a string system for them to stand next to each other . . so then where does your theory really apply if it doesn't handle this usecase of "personal independent existence?"

. . 

Well if one string is "tied" to another string like a yarn is knotted to another string . . that can give you the idea that "that is a single string" and also it can give you the idea that that is "a 2 string system" where those two separate strings are tied to one another . . and yet . . for example . . uh . . it doesn't need to be stated "how" they are tied to one another . . 

. . 

That can be left to the imagination . . 

. . 

Two people holding hands does not make them a couple. That could be left to the imagination

(1) Maybe parent and child<br>
(2) Maybe sibling relation<br>
(3) Maybe prisoner and jailer<br>
(4) Maybe xyz and xyz<br>

. . 

-_-

. . 

Those are limiting beliefs if you take them too seriously to read those 4 physical real-world examples that are provided . .

. . 

But basically "grease-strings" are new

. . 

Grease Strings are new

. . 

. . . . . 

Smoke as a "Grease-String" could be really cool . . I mean . . smoke is like "greasey" right? . . but at the same time . . "how many strings are there?" . . You could possibly count them right? And all the knotty knots that are involved?

. . 

Fire as a "Grease-string" system could also be interesting to observe . . "what are the electrical outlets" that are involved in here?

. . 

Magnetic Grease-string systems could be like how those electromagnetic waves from magnets are formed . . how greasey . . 

. . 

Can you imagine manipulating those strings . . ? those soft greasey strings?

. . 

Or maybe they can be considered "metal" strings . . and so then it's like . . just turn and twist and do whatever you would do with normal strings . . 

. . 

Greasey strings change shape very easily right? . . 

. . 

That mud that I saw earlier today . . is what inspired these thoughts . . 

. . 

It's like You could treat the mud as "grease" . . but since strings are so cool . . well . . It's like "why look at them as traditional yarn-strings or metal-slash-yarn-type-strings" or "metal-like-treated-yarn-strings" . . 

. . 

If you treat them like "grease-strings" which is a new construction . . now . . since I was having difficulty treating them like "metal-slash-yarn-type-strings" . . if you treat them like "grease-strings" it's like a fat girl who can get fatter and fatter if you . . place more mud . . all over their body right?

. . 

Big mud piles are like fat girls . . 

. . 

Fat girls are like skinny girls . . they are like strings . . 

. . 

Strings can be like easy to twist and turn and you can lay a blanket over them at night when they sleep

. . 

But if they are really fat you can even add more fat to them . . not just by stuffing the inside of the string but also the outside of the string . . 

. . 

And so hence you have a grease-string system that gives you mud

. . 

And other grease-string systems would give you other fat bodied things like "big smoke clouds" or even "planets" that are "big fat people" xD

. . 

Isn't that an interesting species of humans to think about xD-_- when you can walk up to a fat person and pat them on the back or pat them on the belly and they become fatter?

. . 

What a nice community favor

. . 

To make those fat people fatter

. . 




