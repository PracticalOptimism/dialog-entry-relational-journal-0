

# Random Thoughts and Ideas - Development Journal - Technological Ideas - September 13, 2021








### [Written @ 12:49 - 12:49]

Runtime experiments

### [Written @ 12:49 - 12:49]

Experiments with heating and cooling systems

### [Written @ 12:48 - 12:48]

Gas pressure domes

### [Written @ 12:47 - 12:48]

Remember that order of magnitude scale gun we talked about in the "pressure" section of our notes?

Basically it allows AKiane to scale her photographs or paintings to be the size of a small hamster.

### [Written @ 12:47 - 12:47]

Theory is a great bandaid

### [Written @ 12:45 - 12:47]

Learn theory all day

and build light based computers at your local library. Go ask them to sponsor trailers of metal and other scientific equipment to come in.

Invite people to come to your electronics shops where you heat things up into concentric circle rings and you try to cool them down.

Can you try to build computers that do those heating and cooling operations at runtime?

Are you too afraid to because the government has mandates to prevent you from destroying the whole of society in one day because of your lazy experimentation.

That's good. Follow what the government says so people aren't build concentric-theory inspired bombs and pressure devices that escape common law

### [Written @ 12:45 - 12:45]

Goals are so last century.

### [Written @ 12:44 - 12:45]

And then you're back to where you started

(1) not knowing what your goal was

And that's great.

### [Written @ 12:44 - 12:44]

It could be so simple like 'step outside'

and then suddenly you have so many knotty ideas to consider

(1) should I forget that I had that goal to begin with?

(2) I need memory flashing devices to teach me to forget?


### [Written @ 12:43 - 12:43]

That is a big quote. Write that down on your calendar for me.

"Knowing where you want to end up is only the beginning"

### [Written @ 12:43 - 12:43]

Knowing where you want to end up is "only the beginning"

### [Written @ 12:42 - 12:43]

Write out a bucket list if you want.

Remember you are still alive. you can write 10 bucket lists tomorrow. 10 bucket lists the day after that D.ra.w. draw. all the circles you want

### [Written @ 12:41 - 12:42]

Then you set out your path and say (1) boy I better do that (2) I better do that (3) I better do that (4) and then you are back to your nice knotted beginning

### [Written @ 12:41 - 12:41]

A nice circle says (1) start here (2) and end here

Maximum and minimum

### [Written @ 12:39 - 12:40]

Do you know how to draw a circle?

(1) did you know that algebraically speaking a circle is quite a special shape?

(2) why is that?

(3) it is because when you even just place that nice pencil lead on the sheet of paper, it is both the start and ending point both the minimum and maximum point for you 

......................................................................

So the present moment is the best you can do theory is on point right?


### [Written @ 12:39 - 12:39]

Knowing your minimum and maximum is essential.

### [Written @ 12:38 - 12:39]

Then life is easy.

### [Written @ 12:37 - 12:38]

Our primer is almost finished. Once you know (1) the minimum point (2) the maximum point (3) you're supposed tofind a trace between those two points that lets you satisfy different values

(1) get over that hill

(2) avoid that hill

(3) do that thing

(4) avoid that thing

### [Written @ 12:37 - 12:37]

You know the rules in the house [naughty-finger emoji]

### [Written @ 12:37 - 12:37]

It's the least that a dictator could do.

### [Written @ 12:36 - 12:37]

Everyone will have their own movie studio and music studio to make all the hot fire beats they want

### [Written @ 12:36 - 12:36]

Kim Jong Un will be happy to teach you trace theory

### [Written @ 12:35 - 12:36]

You get free diaper changes from anyone in society itself.

### [Written @ 12:35 - 12:35]

You were hypnotized from day one to believe you had (1) a father (2) a mother (3) and that they were responsible of taking care of you and changing your diaper

In a society of the future (1) society is your mom (2) society is your dad (3) no more rape of violence

### [Written @ 12:34 - 12:35]

Concentric circle theory is hypnotic

### [Written @ 12:34 - 12:34]

Relationships of circles are completely disposed of.

Forget about hierarchy. No more nonesense about siblings and parents.


### [Written @ 12:32 - 12:34]

Trace Theory.

Knot Theory. Group Theory.

Concentric Circle Theory.

A quick primer on all of these individual courses shows you that (1) create more traces.

(1) A good start for each course is (1) group theory (2) a good end point for each course is (3) concentric circle theory

Concentric circle theory is basically group theory on steroids.

Steroids isn't the best joke but it used to be popular back in the early 2000s of A.D. years.

And in those days steroids mean (1) muscular (2) strong (3) strength.

So group theory gets super strong in concentric theory land


### [Written @ 12:12 - 12:32]

(1) Let us go over some quick refreshers

(2)....................................................................

(1) What is knot theory

(2) What is trace theory

(3) What is group theory

(4) What is concentric circle theory

........................................................................

I am not claiming that these is an algebra here of like linear-interpolation of these ideas or something like that but rather that these 4 ideas are there there could have possibly been more but I am reminded that these are important for some reason

.......................................................................

(1) concentric circle theory is a long topic. read previous notes on

.......................................................................

(1) knot theory is about knots.

.......................................................................

(1) group theory is about circles (and possibly relations between those circles)

.......................................................................

(1) trace theory is about traces

.......................................................................

Traces are invisible or dotted-lines that represent things. but it doesn't have to be dotted lines. solid things can be traces as well.

.......................................................................

Traces allow you to draw over the outline and then you can trace more of those outlines and keep tracing and eventually learn to draw (1) tracey things (2) or traceless things

.......................................................................

tracey things are things that have traces in the background like when a parent pushes you along on your bicycle that is tracey

.......................................................................

when the bicycle is ridden for you and you don't do anything that is a trace

.......................................................................

traceless ness is when there are no more training wheels

.......................................................................

no parent is there to push you

.......................................................................

you push on your own

.......................................................................

that is trace theory

.......................................................................

dotted line notation is good for that

.......................................................................

by the way dotted lines can be drawn like follows

(1) start from the beginning

(2) then draw the point at the beginning

(3) start from the end

(4) then draw the point at the end

(5) alternate back and forth from the beginning to the end

(6) until you have the inbetween dots

.......................................................................

. .  .  .  .  .  .  .   .  .  .  .

.......................................................................

(1) and so this line expample was drawn with the following trace

(2) o

(3) o                  o

(4) o  o               o

(5) o  o            o  o

(6) o  o  o         o  o

(7) o  o  o      o  o  o

(8) o  o  o  o   o  o  o

......................................................................

And so you see that trace says (1) it's good to start with a minimum (2) it's good to start with the maximum

(1) make the minimum step in one of the first few steps

(2) make the maximum (goal step) in one of the first few steps

And go back and forth in a random way 

......................................................................

Until you fill the gaps in between

......................................................................

Linear Scale Random Animation

......................................................................

Zig zagging isn't so bad

......................................................................

But theoretically you could have followed another trace that looks like this

......................................................................

(1) o

(2) o                  o

(3) o        o         o

(4) o   o    o         o

(5) o   o    o    o    o

(6) o o o    o    o    o

(7) o o o o  o  o o    o

(8) o o o o  o  o o  o o

......................................................................

Ideally you can say that is a perfectly good way to draw a dotted line

......................................................................

Also observe that the traditional way to draw a dotted line follows the following trace

......................................................................

(1) o

(2) o o

(3) o o o

(4) o o o o

(5) o o o o o

(6) o o o o o o

(7) o o o o o o o 

(8) o o o o o o o o

(9) o o o o o o o o o 

(10) o o o o o o o o o o

......................................................................

Not knowing where you're going is the reason for failure in the classrooms

......................................................................

Teachers. Please teach students the importance of trace theory

......................................................................

If you stard with the minimum point and the maximum point as the first 2 points, you will end up with a hecka good grade in the class

......................................................................

......................................................................

If you only know the minimum point. You will think -_- where are we trying to end up ?

......................................................................


### [Written @ 12:10 - 12:12]

Knot Theory

Trace Theory

Group Theory

Concentric Circle Theory (this includes the stuff about arrows-as-pendulums)



### [Written @ 12:07 - 12:10]

3 Algebras that Could Be Important

(1) Length (2) Heat (3) Coldness (4) Pressure

(1) Light (2) Metal (3) Timing (4) Animations

(1) Linear (2) Scale (3) Random (4) Animations

.......................................................................

.......................................................................


### [Written @ 12:04]

Linear Scale Random Animations [1.0]

[1.0]
Identity Idea
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/identity-idea.md

.......................................................................

Those notes on linear scale random animations should remind people of what this theory about

(1) Length (2) Heat (3) Coldness (4) Pressure looks like

.......................................................................

Just for some more algebras that people should remember [2.0]

(1) Light (2) Metal (3) Timing (4) Animations


[2.0]
Light Based Computers
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/light-based-computers.md


### [Written @ 11:57 - 12:00]

How long do you make something? (`The algebra of length`) 🙅‍♂️

How much heat do you apply (`The algebra of heat`) 🙅

How much coldness do you apply (`The algebra of coldness`) 🙅‍♀️

How much pressure do you apply (`The algebra of pressure`) 🙅‍♂️


These 4 four mighty kingdoms collapsed into one another to create time and space.

(1) The pressure to dream

(2) To imagine a creative vision so hot and enticing

(3) yet with invisible flairs of magmatic peace and coldness

(4) free lengths for humans to wind up and down with their mighty knotty minds

These were the very beginning of cartoon shows that inspired children to read and write from different and new scriptures. "You create your own reality" was a myth that was heard ages ago.

It was no from 2021 A.D. but much earlier. Only those remnants of humans who believed they were stuck in a time bottle believed they had read those words.

Others studied and read books.

### [Written @ 10:58 - 11:57]

I really don't know how far this algebra goes.

(1) How long do you make something?

(2) How much heat do you apply?

(3) How much coldness do you apply?

(4) How much pressure do you apply?

.......................................................................

.......................................................................

(1) It can refer to making computers and all you have to do is ask yourself (1) what are the limits of the computer. (2) a computer with 10 terabyte harddisk space is an idea

.......................................................................

(1) how much heat do we apply?

Heat and fire are almost always involved in industrial processes. Having a heat gun to melt stuff is really essential. Isabel Paige doesn't have one so you need to go buy her one but basically if you have a heat gun there are insurances you have to pay

(1) does it have unlimited fire like a universal basic income heat gun? Or do you need to stock it with coal fuels? Or do you need to stock it with new or existing energies?

(2) having a heat gun can be a tremendoes feature. (2) heat guns melt metal (3) metal becomes soft and malleable (4) then you can bend the right sword or building fabrication blueprints that you need (5) but also don't forget to use the SUN (6) fresnel lenses that heat-melt things are free universal basic income on heatguns

(3) Fresnel Lenses concentrate the heat rays of the sun [1.0] [2.0]

(4) Concentric circle theory talks all about the power of concentration. go read the notes [3.0]

(5) Basically maybe in the future it will be possible to concentrate new elements for sources of heat without the use of the sun's direct potential energy

(6) Then you can have lighters that are based on concentration of resources from other places.

[1.0]
GREENPOWERSCIENCE
https://www.youtube.com/c/GREENPOWERSCIENCE/videos

[2.0]
Fresnel Lens Solar Power Foundry Obsidian Farm 3800 ˚ F 2100˚ C Fresnel Optics greenpowerscience
By GREENPOWERSCIENCE
https://www.youtube.com/watch?v=drE54ctrHBY&ab_channel=GREENPOWERSCIENCE

[3.0]
Concentric Circles
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/concentric-circles.md


"How much heat do you apply?"

That is a good question. It is very important to distinguish types of heat.

(1) Have you ever seen a cartoon television show?

(2) have you seen how the frames are animated?

(3) The hot illusion effect of the animated frames makes spongebob look like he could pop out of the screen and start shaking your hand just like a hot person walking around on television gives you the illusion that they are a real person.

(4) It's just video frames but basically illusions are hot

(5) Concentrating illusions can be a way to concentrate heat

.......................................................................

How hot do you make something?

Ghosts with invisible potentials can concentrate ideas into your mind. You might be like "woah" what was that? "what the heck was that?" "heck" "hell" "hell" "hell is fire"

"What the hell is that"

"hot" "fire"

sources of heat can include hot music albums. "How hot do we make this music album" "10 knots?" maybe less so people's ears don't melt.

100 knots is a snatam kaur album

.......................................................................

knots is a good measurement

.......................................................................

knot theory says so

.......................................................................

knot the fire around their throats so we can cut their heads off

.......................................................................

and then that's a basis for how eminem improves his theoretical rap albums of the future

.......................................................................

you see measuring length gives a scientific approach to comparing music

.......................................................................

the knottier something is

.......................................................................

knotty. naughty. same thing right?

.......................................................................

knots are based on knot theory.

.......................................................................

go read the notes that Jon prepared at the 'light-based computer' observatorium.

.......................................................................

[1.2]
Light-Based Computers
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/blob/master/Development%20Journal%20-%20Technological%20Ideas/Idea-Invention-Listing/light-based-computers.md

.......................................................................

So knotty.

.......................................................................

Why does fire melt metal? Why do we use metal? Why is metal so hard? Why is metal so reliable? Basically heat answers all of these questions.

Metal is reliable because we trust in its heat.

Trust is another type of fire that holds social relations together. Such hot fire.

If you can elaborate on trust you can build new community components. Use concentric ring theory to develop new trusty metal-like things.

water materials that have metacular or metal-like properties could be like mercury except -_- with different things -_- like basically kill people type stuff -_- really it can be dangerous if you use your imagination too much. but hot is hot. hot is hot.

hot is basically like the imagination. when you measure the length of your imagination you say 'maybe not as long as einstein's right?' but hot is hot. hot is hot. 'maybe not as knotty as einstein's right?' well hot is hot. hot is hot. einstein had 2 knots in physics. how many knots did you have 0 or 1 or 2 or 3 or 4 or 5? it's up to you hot is hot. hot is not length but you can say that hot "HAS" length. you can say the length of a fire is hot or that the length of a fire is 2 knots.

.......................................................................

Knots can be difficult to determine concretely depending on your base points

.......................................................................

knottiness

.......................................................................

If you have a table of base points to start with then that's a way to have a basis

.......................................................................

Amotic chemistiry is based on knots of elements osf subatomic particles

.......................................................................

You have to knot that in your mind to read it how it makes sense

.......................................................................

And then if you don't make sense out of it then you have new knotty elements

.......................................................................

Amotic chemistiry is a new knotty chemistry field

.......................................................................

And you can knot its notations

.......................................................................

Draw graphs

.......................................................................

Libraries of books with knotty notations is good

.......................................................................

(1) try to measure the length of the things you're knotting

(2) who or what other people or places or things would benefit from being knotted with that theory

.......................................................................

.......................................................................

Concentric circles are interesting.

.......................................................................

It's tough for me to say at this point.

.......................................................................

I don't have enough technical expertise

.......................................................................

Teaching you what I know has its limits

.......................................................................

All you can do is shrug your shoulders

.......................................................................

.......................................................................

(1) concentric circles

(2) up arrows, down arrows, left arrows, right arrows

(3) (1) up arrows are called "hire someone"

(3) (2) down arrows are called "ideas"

(3) (3) left arrows are called "big"

(3) (4) right arrows are called "small"

And use that notation to communicate.

(1) experiment with that notation.

........................................................................

........................................................................

I don't know everything but that's what I've learned so far

........................................................................

........................................................................

Arrows symbolize pendulums. Pendulums swing.

........................................................................

Concentric circles symbolize nothing in particular

........................................................................

Basically it's an attempt at an exhaustive type of theory

.......................................................................

"pendulating nothingness"

.......................................................................

arrows and concentric circles

.......................................................................

Just draw up arrows all day and you'll feel good for the rest of the day

.......................................................................

"hire someone is the best you can do to feel good"

.......................................................................

.......................................................................

And if you are a scientist and mathematician just draw "down arrows" which are "ideas"

.......................................................................

If you tend to be creative and imaginative and don't care to apply your ideas

"left arrows" "big"

.......................................................................

If you tend to be practical "right arrows" "small"

.......................................................................

Computers are like 'right arrows' that somehow have concentric circles involved right. Try to draw a simple computer using the notation we've just described.

.......................................................................

You punks that's what I did last night on September 12, 2021 or rather this morning on September 13, 2021 at midnight. I'm not showing you my answer until I get the time to copy-paste what I wrote in my notebook which takes (1) scanning the notebook paper that I have

.......................................................................

Scanning is a long process

.......................................................................

.......................................................................

.......................................................................

Heat is an amazing property.

.......................................................................

"How much cool do you apply?"

Refrigerators are cool. Refrigerators are peace symbols

.......................................................................

Cool is peaceful

Coldness is peacefull

.......................................................................

When people are cold it can make you feel warm

.......................................................................

you don't feel harrassed

.......................................................................

hot blistering heat is harrassmenet right?

.......................................................................

Coldness is okay. (1) temperature coldness (2) how cool something looks can be a coldness (3) it's kind of like heat but if you have 2 poles of the same blade then you have pairing theory which means there's always a cold to your hot.

.......................................................................

.......................................................................

Pairing theory has been talked about before in copy-pasted notes from my cellphone.

(1) basically if you ask someone 'do you need your picture taken?'

(2) 'do you need a picture frame?'

a picture frame, like Akiane Kramarik has learned from her years of painting means that (1) there is a width. width is part of a pair.

Pairings are like 2 concentric circles distanced from one another. The stuff that connects the circles is invisible. And could be more concentric circles or arrows.

Pairings could be like the poles of an electromagnetic coil

.......................................................................

Pairing can be like 2 people standing side by side

Frames create pairs that emphasize the importance of your picture

.......................................................................

As long as you have frames in your life like 'parents' then you are important

.......................................................................

homeless people standing around a billionaire make the billionaire look important.

homeless people standing around another homeless person make the homeless person look important.

grass standing around a rock makes the rock look important.

frames make things look important.

frames.

frames of mind can make ideas look important.

ecoin is important only because there is a frame of (1) did you say that about ecoin (2) did you say that about ecoin

.......................................................................

pairings

.......................................................................

when you have pairings

.......................................................................

.......................................................................

.......................................................................

'how much cold do you apply?'

how do you get a universal basic income of cold?

It's like asking how can you carry a refrigerator around with you all day.

Just like with heat guns. People are having a hard time creating (1) cold guns.

Cold frost guns would be amazing.

If you are a scientist of physicist go create one so people can start pairing heat and cold and make new weapons and supplies.

(1) It's fun for kids to make candies that are cold

(2) making animals in the wild freeze up like frogs standing by lakes is hilarious

(3) freezing insects is hilarious

........................................................................

You can be a police officer and say 'freeze'

........................................................................

no need to pack the heat

........................................................................

A nice outfit of blue makes people freeze

........................................................................

So your outfit can be 'responsible'

........................................................................

hot and cold awareness should be a common language for people

........................................................................

'what keeps people cool'

........................................................................

what keeps people safe in nice cold environments

........................................................................

warm environments are about drama

........................................................................

(1) that person knows that person that other person kno[w][t]s that person

(2) that knot and that knot know each other

........................................................................

........................................................................

They're all knotted up in a frenzy of knots says the joker half-heartedly

........................................................................

How can you kill the citizens of gotham with ecoin-death-guns if they're all knotted up in hot headed homes

........................................................................

'I don't want to die mr. joker'

........................................................................

'that's hot'

........................................................................

Too hotty to

........................................................................

........................................................................

And finally we get to our final touch for creating bombastic weapons

........................................................................

"how much pressure do you apply?"

........................................................................

(1) Heat pressure

(2) Cold pressure

(3) Water pressure

(4) Gas pressure

.......................................................................

These algebras

.......................................................................

You can mix them up

.......................................................................

.......................................................................

.......................................................................

"How much pressure do you apply?"

(1) pressure can mean a great deal of things

(1) but remember we are going to measure lengths. lengths are like with lines on a ruler stick. do you know number theory is popular. but there are more lengths than besides number theory. what about the notations of algebras that akiane keeps in her basement. all of those marvelous paintings. what about those lengths?

(3) Akiane has practical things besides numbers to worry about (1) what is the pressure to apply to this brush stroke?

You can have new pencils that require less pressure. less attention. less focus. just automatic writing if you'd like.

technology is filled with marvelous new things.

It can be amazing.

.......................................................................

Pressure can even be simple things like (1) count (2) count the number of rocks you placed on a given concentric circle area (3) that pressure must vary depending on the length 

.......................................................................

.......................................................................

Here is one pressure people would like to learn about

(1) order of magnitude guns

(2) how can you scale an atom to be a balloon

(3) how can you scale a balloon to be the size of an atom?

(4) how can you do that with just the push of a button?

.......................................................................

knot theory tells us we need to have new lengths that we learn about

(1) like how time works like pendulums

(2) we might need to pendulate with different ancestors of ours to learn the answers to how scales swing

(3) if you can make a real physical car, why can't you really shrink it to the size of a pin on a needle

isn't order of magnitude pressure really a cool pressure to apply?

.......................................................................

.......................................................................

concentric circle theory

.......................................................................

that is a pressure that teaches you about different pressures

.......................................................................

how can you apply pressure to bring the cartoon character you know from a movie to life?

.......................................................................

Roger Rabbit could be Jon Ide [bow-emoji] if you let him

.......................................................................

bowing

.......................................................................

.......................................................................

.......................................................................

Jessica Rabbit is a cool moped to ride

.......................................................................

a one person moped

.......................................................................

A one husband moped

.......................................................................

release the pressure of young boys having to stand up and be manly men

.......................................................................

having sex with megan fox is the manliest that you can get

.......................................................................

if megan fox isn't available in a nice universal basic megan fox package then when will society pressure itself to see that dream

.......................................................................

how long?

.......................................................................

how much time

.......................................................................

what is the length of the vision for that dream to elapse

.......................................................................

megan wants to study aliens

.......................................................................

be an archaeologist

.......................................................................

not be a simple one-husband sex toy

.......................................................................


### [Written @ 10:56 - 10:58]

Saintsville

### [Written @ 10:56 - 10:56]

Too hot

### [Written @ 10:56 - 10:56]

Brittaney Louise Taylor

### [Written @ 10:55 - 10:55]

Too hot

### [Written @ 10:55 - 10:55]

Too hot is that one person that is like Eugenia Cooney

### [Written @ 10:54 - 10:55]

'heat can be like pain and pressure' just like when you don't feel good when reading something you feel too much 'heat'

jusutius when you feel
not good.
just like when you feel
not good.

too hot.

### [Written @ 10:52 - 10:54]

There may be other processes besides heat and cool but you can flat map them to 'heat' and 'cool'

### [Written @ 10:51 - 10:52]

Imagine making a bastard sword that required you to heat and cool at the same time but you had to heat and cool at different pressures at different lengths along the sword.

### [Written @ 10:50 - 10:51]

Coldness is 'how cold do you make it'

(1) social coldness

(2) temperature coldness

(3) other types of coldnesses

(4) how cool you are like a cool person

(5) or other types of coolness


### [Written @ 10:49 - 10:50]

Pressure can mean (1) social pressure (2) air pressure (3) heat pressure (4) water pressure (5) pencil pressure (6) and this can be measured by numbers just like 'length' if you want.

Pressure is a great way to determine how something reacts if you have the time to determine that.


### [Written @ 10:48 - 10:49]

Basically these 4 algebraic questions possibly could span a nice vector space of ideas such as (1) try to make that one idea a reality

I'm not sure. again this is theory.

(1) How long do you make it.

(2) How much heat do you apply?

(3) How much coldness do you apply?

(4) How much pressure do you apply?

### [Written @ 10:48 - 10:48]

(1) How long do you make it?

(2) How much heat do you apply?

(3) How much coldness do you apply?

(4) How much pressure do you apply?

### [Written @ 10:47 - 10:47]

"How much heat do you apply?"

"How much cool do you apply?"

"How much air pressure or water pressure or social pressure do you apply?"


### [Written @ 10:46 - 10:47]

How long is something?

This is just a theoretical idea so don't take it so seriously but basically because you can triangulate this thought with practical ideas like length of measurements. Good for you.

### [Written @ 10:38 - 10:46]

**How long to you make something**

# How Long Do you make Something?

(1) This question is supreme

(2) Length (1) firstly refers to physical length (2) secondly refers to the length of your attention span to capture the correctness of a device (3) picture yourself trying to make a specific cartoon movie (4) you might not have the right frame of mind (which is itself a length of its own) (frames) (lengths) (frames are lengths) (like the frame of a picture) (length defined by the frame itself being a length) (it's like a triangulation theory to triangulate length in many proportions) (the picture is as long as the frame used to contain it)

A view of the world as a 'length' is supreme.

(1) Joe Biden is a length. (2) Donald Trump is a length.

Who else is a length. Viewing the world as a length means (1) You can capture the entirety of an idea like the world into a single measurement.

(2) That means all those ants and bicycles and creatures that peddled around with their various thoughts are all measurements on your new mental ruler stick which has the potential to be physicalizable

(3) a view of the world as lengthy means (1) which proportion of length are you particularly focusing on (2) the length in 2021 or 2022 or 2023

Which length do you want to experiment with spanning

Or expanding on

Someone could ask you to write a book about the earth but you would only span a few meters like (1) 2021 (2) 2022 (3) or 2023

There are some lengths that you might not have access to like (2050) and so those lengths are still there and spanning them is still possible

It's not just possible to measure 2050 in 2019 it is even possibly advisable by aliens.

For example you can have people follow a book for what they should do or say in those later years and try to see if your measurement was appropriate.

You see time is really an illusion. the illusion of time is just that it's a pendulous motion. pendulous motions come from human perception.

The human perception is a length that hasn't been taught to you.

If you saw time all at once you would see humans enjoy or have meter sticks that like proportionating certain things like pain and pleasure

And those measurements are just that.

Measurements

and there are new measurements to be made.

Have you ever thought of yourself as a measuring stick walking around.

If you're a measuring stick you don't need any limits

You're not really anyone's goal keeper or goalie

you're not trying to cock block or do something to prevent someone else from measuring their own answer

But you're just meauring

measuring

measuring

measuring

measuring

measuring

measuring

measuring



### [Written @ 10:37 - 10:38]

These are 4 mathematically conjoinable questions that you can use to span quite a large set.

(1) Linear algebra spanning is quite a useful term to learn

(2) Linear combinations are quite useful things to learn about

(3) Solving problems using linear algebra is quite cool

### [Written @ 10:36 - 10:37]

(1) How long do you make something

(2) How much heat do you apply?

(3) How cool do you make something?

(4) how much pressure do you apply?


