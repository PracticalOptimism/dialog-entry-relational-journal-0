
# Inventions and Ideas

| A List of Inventions and Ideas | 40 Items Listed | April 15, 2021 (Initial Date Created) | December 14, 2021 (Date Last Updated) | Latest Item(s) Added: (N) Numbertron |


| A | 1 Items |

### Accidental Residual Energy

- There are plenty of accidents that happen from day to day
- New discoveries
- Old wounds re-discovered
- New thoughts
- New ideas

[See More Notes Here](./Idea-Invention-Listing/accidental-residual-energy.md)


| B | 1 Items |

### Big Head Mouthwash Brainwash

Basically the idea is force yourself to sleep more. When you sleep more you can find more things in your dreams. Woah.

If you're feeling suicidal. (1) It's not necessarily that bad (2) You're supposed to say (3) These are headaches because I am missing something (4) I am hungry for food that I can't see (5) The food is inside my hypnotic thoughts? (6) Is it merely a hypnosis? (7) I don't know? (8) Somehow I need to convince myself there is something. (9) Placing my head on this pillow even if my day is going great should be enough energy to say (10) Yes. I can get Molly back. (11) Yes. I can do that one thing. (12) Oh no. I might be able to convince myself blah blah blah. (13) And answers could one day be whereever on your doorstep right?

[See more notes on this topic here](./Idea-Invention-Listing/big-head-mouthwash-brainwash.md)


| C | 3 Items |

### Concentric Circles Idea

This ideas has been repeated in notes like

[[Illusions]](./Idea-Invention-Listing/illusions.md)

[[Identity Idea]](./Idea-Invention-Listing/identity-idea.md)

[[Needlessly Hypnotic Theory]](./Idea-Invention-Listing/needlessly-hypnotic-theory.md)

[[Randomness Computer]](./Idea-Invention-Listing/randomness-computer.md)

[[Strings and Crystals]](./Idea-Invention-Listing/strings-and-crystals.md)

[[Trace Theory]](./Idea-Invention-Listing/trace-theory.md)

### Combinational Ideas

This is a list of ideas that are combinations of other ideas that are listed here in this invention listing.

Wow. You can be so impressed to find things that you could have thought about (1) you said (2) I like it (3) but then you gave up (4) and said (5) I don't have enough energy to look at that (6) I don't have enough time to look at that (7) I don't have enough practical answers to look at that (8) Those are ghost theories (9) None of this makes sense (10) Speak to me Jon (11) This isn't coming together (12) What does this have to do with daughter and son marriages (13) I am the host of a simple wedding. What does this have to do with me?

Good luck.

L.O.L.

[Here are the notes for those topics](./README-COMBINATIONAL-IDEAS.md)

### Cylinders

[Here are notes](./Idea-Invention-Listing/cylinders.md)

| D | 0 Items |

| E | 11 Items |

### Elementalizer for Converting Poopy Salts to Sweet Salts For The Environment And Other Basic and Neutral Usecases

`Elementalizer is a technical device that converts resources into basic neutralized nutrients`. Neutralized nutrients are kept of atoms or particles or thoughts or ideas which are possibly electromagnetic wave forms that are interesting to think about. You don't have to create this device but it is very interesting to think about its usecase for (1) Eliminating poop waste by converting it into a salty material that has fewer and fewer poop particles that would help make the environment stinky because of stinky poop particles being around. Because this device is able to convert a box of poop into salty substances that are neutral or neutralized of poop and pee and urine and feces, you are possibly able to spam this device in sewer stations that have a lot of those things already and so then you turn or you convert or you rotate or you aeromaticize or aquamatacize or apply some sort of machinery or computer technology to tell the story of how that fecal matter is now a basic element that you can even add to your tea or even add to the skin of a young child person who has sensitive logical skin that would otherwise not find poopy material substances very interesting to live with.

An elementalizer uses other ideas listed in this journal notebook of inventions and ideas such as (1) aeroneumatic actuators and idea patterns like 'words' 'words' 'words' 'words' which are a good set of words to say that 'words' are neutralized into 'words' and so the 'word' 'pigeon' is possibly able to be neutralized to the word 'word' with a nice aeroneumatic brain of a human being who can reason about other possible applications of elementalizing. 'Word' is a nice word to say that a lot of 'words' map to and so 'word' can be a target language to speak and you can spell racist statements and sentences with 'word' and the words that are offensive or rude or non-binary towards synchronizing oppresive ideas 'word' 'word' 'word' which means 'yes, that might be a good thing to try out' or 'yes, that is not good' or 'yes, I am a hippopotamus' 'word' 'word' 'word' is like a barrier sequence of words that could be considered like a 'salt' material that says as long as you have as '`salt shaker`' like an aeroneumatic elementalizer . . then your words of . . 'I am a human being' or 'I am a hippopotamus' or 'I am a space alien' or 'I kill easily, you would die because of my presence' (such as stinky poop lying around on the couch of your living room for hours and hours without anyone sighting it as a toiletry item) and so a nice 'salt shaker' would take the 'poop salt material' or the 'poop material' and convert it into a nice salt material or a nice 'salt material' (minus the poop) and so that salt shaker would be very useful even for things like -_- (1) hungry underpriveleged babies around the world (2) birds and fish fighting to survive in aeroneumatically changing environments and (3) other ideas or things you can come up with or think of. These are all really delicate remarks and possibly easy to trash talk and so you'll also have to think of an elementalizer for the words that have been written here.

### Electron Gun

`Electron gun` is proportional to the idea of `universal basic income for electrons`. You don't need to run out of electrons. If you use a traditional capacitative battery that supplants electrons at runtime with a finite size swimming pool to select from, you are saying that you don't want to take advantage of proportional physics that says 'yo, there are more electrons to subdivide from your material components, will you accept other answers from those materials?'

Electron guns say you can `supercharge an electromagnetic coil like a copper coil` with an induced active potential to discharge electrons for free with the cost of (1) spinning a magnet around a copper coil very rapidly for many times and for a super strong magnet depending on your usecase for how long you want to wait for a large voltage of electron disposal and also other things like will aliens let you do that? aliens seem to think your inability to calculate safe measurements for the safety of your population species is irresponsible and so your machine will be shut down from space star aliens observing from a distance O_O.

Electron guns are useful for things like (1) having a long supply of electrons from only charging your coil for 2 or 3 hours or proportional time like 2 or 3 hours or 2 or 3 days or 2 or 3 months and then beyond that maybe it's really not necessary and already at 2 or 3 months you are not behaving like a normal citizen according to the space age faieries.

Searl Generators as talked about in this document are possibly also related to electron guns since a (1) windmill with magnets on it while tilted to proportionate or navigate a windmill force plate like (2) a set of magnets that are facing the windmill to take advantage of the tilt angle and other proportionality pressure metrics like (2.1) being able to start and stop the windmill and (2.2) being able to scale the various potentials of the activities of the windmill spinning radii without having to speed up or slow down but instead teleport very quickly or very rapidly to a new speed that is like O_O aliens don't want to allow you to do that on day 1 of the device being active :O

### Electric Skate Gun

Electric skate gun is like an electron gun that provides electric skates. An electric skate is like a bearing-based electromagnetic motor. Electromagnetic motors with bearings allow resources to be considered in proportional angles of one another. A wheel can spin in a proportional angle to a car. Cars don't spin like wheels right? So the bearing keeps the car still and not front-spin rotating like a wheel on the road.

Electrons that front-sin rotate are proportional to weird things like two-way bipedal electrons that say they rotate in all those circuit entry holes that are like sensitive to that sort of information and so phase shifting electrons from other realities is like 'did you allow yourself to see that phase shifted alternate reality electron from reality #2 or did you not have the elementary circuit property to identify that element?'

Electrons that

### Electric Skate Proportionality Gun

I don't know what these are. You will have to accept a response from Seth and the Pleiadians and their alien and spiritual friends that live stream this information to you live here today:

Okay, a proportional idea to the live stream that is the distraction

### Electric Skate Dancing Razerblade Gun

### Electric Skate Dancing Bandaid Flare Gun

### Electric Skate Dancing Flare Gun With Repeated Flare Charges Because Seth Lets You

### Electric Skate Dancing Flare Gun With Repeated Nose Nostril Flare Guns Because Igloos Let You

### Electric Thanks Weapon

### Electric Yea

. . 

### Electric Rubber

"`turn that way`" "`I'll hold still`" and "`you can still turn`" "`oh, by the way, you can turn the other way as well`"

. . 

That is an idea that came to me why reading the previous paragraph about "turning" I had accidentally mistaken "you can still turn" for "you can turn still" and then the words "snapped" "back" into "position" which is really awkward. It's like parallel realities are snapping on and off my eyeballs and also my thinking mechanisms like light switches that spring back to action like they are timed like how rubber is timed to "spring" "back" into position because of its rubbery effect.

. . 

Rubber and Grease are interesting to think about . . Now that I think about it . . Rubber is like weird and bouncy right? which is like "repelling" or "spraying" but "grease" is like "sticky" and "slippery" which is like "I'll stay around and you can join if you want"

. . 


| F | 1 Items |

### Fire Air Crushing Technique For Eliminating Poop

A process for eliminating waste. `Fire, Air and Crushing`. `(1)` Have some poop `(2)` blow wind at the poop to make it dry `(3)` the poopy wind result can be siphoned to a nice fire nearby to burn the farty gas `(4)` a closed furnace can work to prevent any farty air from escaping `(5)` very dry poop can be 'slammed' with a nice 'crusher' system with gears that can possibly be like 'very sharp teeth' that slam together back and forth repeatedly and slam the poop for the purpose of making rock-sand material that is dry `(6)` dry crushed poop can be very dry and very crushed if you repeat steps 1 through 5 for a very long time to ensure that the poop really is dry of moisture and that you can always capture the farty air in a nice fire that produces nice dry salty farted air that isn't necessarily going to fart into the atmosphere `(7)` by crushing the poop to be super dry and super dry you can prepare it to be burned in a fire where the super dry poop won't be watery and wet and try to prevent the fire from burning `(8)` the dry poop is burned into salty dry ashes . . This process should possibly ensure that even the poopiest poop can be super dried and super non-effective on the environment with super useful nutrients that can possibly even feed people with teas and nutrients for plants and other aspects of life and there won't even be any smelly smelly place because the dried ashes of crushed and dried poop can be super nice . . and be so clean on bacteria and poisoning compounds because of how 'autoclaved' and 'air pressure neutralized' the poop has been along with 'other ideas' 'Autoclaving maybe isn't the right term to use here' but that device should also possibly help to kill bacteria . . 'vacuum autoclaving' and 'high pressure gas bombardment of poop' can possibly create dangerous environments for living microorganisms of 'poop' but drying is super amazing and burning the fart is also really amazing. Over all this system can be built with supplies from home and so it's a really nice practice to possibly `get rid of 'sewers'` and just have 'dried poop particle remains from fire processes'. It is only just a theory though and so possibly it may not work very well in practice . . the goal was to have something that can be transported and eliminates poop . . transportation means that people can easily use it . . and also the elimination of 'poop' storage can possibly be a surface application for this method . . personally I haven't tried it yet . . but it could really work if the crushing is done properly . . and if the fire is 'electrolytic' enough to 'vaporize' or 'particlize' the remaining poopy dust from the dry crushing process.

'`Crushing`' '`Typing`' '`Pressing`' '`Stepping`' '`Hammering`' O _ O why are these elements useful in modern day mechanics? I suppose if everything was an automated dream land O _ O thought-steps would be an idea. Of course when I say 'elements' plural I mean 'one element' since they are all categorizable as '`algebraically equivalent`' which is summarized by '`just use a hammer to type that nail into the house`' or '`just step over the next line of code by pressing that button`'

Metals are interesting elements in our environment and just today on May 8, 2021 [4.0] when I am writing this note . . I think the idea was introduced to me that metals are like `really strong people` that `press things` or `know how to push buttons` or `mine minerals with strong muscles` or something like that . . and so . . maybe that is a trending idea that `people with large muscles` tend to work with `metals` . . and that can go . . algebraically with any 'metal' that you would like to imagine since transformatively or transitionally you can slide the term `metal` to mean 'meaningful item' and so 'metal' and 'meaningful item' could be like 'paint brush strokes' and so if you are a painter who paints on a canvas then you are a `muscle man` or `muscle person` for really cool ideas like 'bring that from those mines over there in the imagination landscape' right? right? O _ O [Studio Ghibli](https://en.wikipedia.org/wiki/Studio_Ghibli) is an example of nice homely mines that we could all visit right? Those are good metals for all good bench pressers.

| G | 0 Items |

| H | 2 Items |

### Hilltop Realities Idea

This is an expensive idea.

Basically you can create your own reality.

The notes for this are available here [Hilltop Realities Idea](./Idea-Invention-Listing/hilltop-realities-idea.md)

Please don't sleep with Eugenia Cooney.

I beg of you kind sir.

It is a simple request.

Of all realities. Why should mine be riddled with small misdeeds like that.

It's a simple request. And while you're at it. Don't sleep or have sex or even look at any of my girlfriends ever please. Thank you very much.

And here are those notes again for you casually.

If you click on this link then you agree and appreciate this invention list and you'll also agree to not sleep with girls in hilltop realities worlds [Nice Idea, I love it, I agree. Great. Give me Those Notes. Now. Now . Now. Thanks for not dying](./Idea-Invention-Listing/hilltop-realities-idea.md)

### Hypnosis as an Idea

Please.

These are really interesting notes.

[Go read them here](./Idea-Invention-Listing/hypnosis-as-an-idea.md)


| I | 2 Items |

### Identity Idea

I would like to introduce an idea. I'm not sure how important it is. It could be important. I think it is important.

(1) It is clear that time is important to me. But I don't always know why.

(2) I think it is clear that memories are important but I also don't know why.

(3) I think animations are weird.

. . . . 

[Here is a record of notes on things that are possibly interesting to think about](./Idea-Invention-Listing/identity-idea.md)

### Illusions

[Here are some notes on illusions as a topic and as a theoretical technology for virtual reality](./Idea-Invention-Listing/illusions.md)


| J | 0 Items |

| K | 0 Items |

| L | 2 Items |

### Light-Based Computers

(1) Light is a physical transportation vehicle for consciousness.
(2) Light is a barrier vehicle for information.
(3) Information and light are one.

Chapter 1 - There is a computer that can help people not have to work anymore

Chapter 2 - For after the computer is built, you can make another

Chapter 3 - The second computer is good

Chapter 4 - The third computer is also good

Chapter 5 - The computer can communicate with ghosts

Chapter 6 - There are more ideas that you can work on

These Notes Are Really Long. Please Take A Seat and Nice Tea Break For Many Hours Before Reading.

(Notes On Light-Based Computers)[./Idea-Invention-Listing/light-based-computers.md]

[Notes URL](./Idea-Invention-Listing/light-based-computers.md)

### Linearly Independent De-coupled Potential Activating Devices

`Linearly Independent De-coupled Potential Activating Devices`. Bearings are a great example of these devices. Bearings are known to be in cars, bicycles, and maybe even a lot of electric motor types that spin. A lot of things that spin use bearings to offload the weight of one element onto the bearing and allow an adjacent element to spin frictionlessly off the force of the presence of that original first element. Bearings are interesting for what I've ashamadely assumed are (1) steel or metal alloys are very `energy dense` and `structurally rigid` enough to carry or lift very heavy objects that could possibly be buildings and even more heavy items depending on the metal alloy or material alloy that's being used (2) Decoupling the torque axis of the spin by having a separator ring of magnets that aren't really magnets but are even more impressive because of their metal alloy properties. This magnet ring of spheres which again aren't magnets but behave like super strong magnets are able to occupy the force of 'I will let you spin on this axis without bothering your neighbor's spin' and so a 'relative polar axis' is formed where one ring (the inner ring for example) spins independently or irrespective of the other ring (the outer ring for example) and so these 2 rings in the bearing are able to be 'performance measurement devices' that can 'spring off one another' because neither one is tridirectionally coupled to the other without for example, a linear motor device connecting the two rings. Which is a lot of mathematical jargon to state that these 2 rings are able to measure one another's performance by for example, one is spinning at speed A and the other is at a relative axis of speed 0 with respect to speed A. And further bearings can also be measuring poles for A, B, C, D etc. which can all be proportionated back to one another since a relativity axis of 0 can always be supposed or garnered from measuring the various devices.

A bearing is a device that allows potential energy spins to take place at runtime and instantly for a lot of physical vehicles that use those devices. Spinning is easy and effective when you use a bearing. Robots that live in factories and spin their arms around are all using bearings otherwise the author of this post, in their respective time period of 2021 A.D. on planet Earth is embarrassed by having written this mistaken comment.

The popular world today of bicycles, scooters, skateboards, cars, trains, and other moving vehicles are populated with bearings (so my research has taken me to assume). If bearings are present in early human history, those are very impressive to change the culture overnight. You have wheels that can bear the weight of their car. You have wheels that can bear the weight of their car. You have wheels that can bear the weight of their car and any number of passengers that are able to withstand the high energy threshold of metal alloy steel or something like that. I'm not really sure, this is my hypothesis and studied observation or studied understanding at this time. I'm sorry for any mistakes. The greasy environment of the spherical bearing balls seem to act as a residue for low friction high axis spinning around an axis of interest.

`Aeroneumatics` or air-based vehicle pods or air-based motors or also known as vacuum pumps or suction cup techniques which increase and decrease the air pressure in a solid state environment are interesting devices. These are really incredible devices and really understudied in proportion to other ideas but then again so are windmills and so windmill systems on transportation networks that rely on let's call it `fly through the air game mechanics` are still needing to realize that aeroneumatics are amazing. In fact, one of Boston Dynamics' recent robot releases that moves boxes very quickly using aeroneumatics, was the key inspiration for me to start thinking seriously about 'why does air pressure work so well?' 'they are able to move those boxes very quickly?' 'when I tried to solve their problem of moving boxes on my own, I did not first come to aeroneumatics. And then when I moved past the internet article [1.0] title to read more about their actual techniques that they applied it seemed that this aeroneumatic design really slipped my mind. I did not intuit that aero-neumatics designs were really the proportional angle to get really fast restart times to press and grab boxes very quickly instead of having arms that would spin and rotate to angle themselves in the right position to grab the box'. Aeroneumatics is really impressive and if you proportionate it to other ideas, you also become surprised where you find aeroneumatics being used in a lot of places of interest. The aeroneumatics of your breathing up and down your breath and your chest expand and contract based on aeroneumatic lungs which are really impressive for collecting oxygen and decyphering or spilling out carbon dioxide and so it's like 'woah, where else is aeroneumatics being applied today?' 'why is it so interesting to have air that builds things up and down?' `Aquaneumatics or water-pressure based systems` are equally as impressive but I haven't even begun to ponder what those devices are about. That is really a mystery science for me (1) especially because water is like an electromagnetic system that is really weird. If you have water, why do things float down slowly due to gravitation forces? Does electromagnetic water forces really have such a strong impact on changing the gravitational forces? Why? Why is water not like air in this respect? It is the hydrogen bond system right? Well that is still very interesting to think about air airplanes and how those vehicles could be advanced. Do you think you could make the air around a real airplanes like water so there is sort of different structure of interest to deal with when flying through the air? That could be interesting. I really don't know. I'm now starting to imagine alien vehicles that propel through the sky in the cosmos very quickly because of aerogels style transportation mechanisms where they don't need to rely on air and wind friction in the similar sense of atoms but possibly in other senses like O_O what type of physics is that?

In thinking about (1) how to build houses and (2) how to allow bearing systems that build houses to rely on aeroneumatics . . the problem of 'why are aeroneumatic pumps really like single-celled chambers that increase and decrease air pressure?' 'can you have something like a random access runtime computer where the cells of the air pressure chambers are randomly and spontaneously charged with vacuum pressures like how capacitors can charge and discharge large electric volumes?' Well that is the first time I have ever related 'air' with 'electricity' in the sense that 'air pressure' and 'electric sensation pressure' are related or relatable since large volumes of either at any given time could be interesting and useful. Also, given the topic of 'bearings', 'material weight pressure' is yet another type of pressure that is also relatable to the various 'pressure magnets' of 'society's pressure observation ring' 'what are you being pressured to observe?' 'social pressure?' 'woah' 'that would be amazing' 'wow'

`A various pressure dynamic aeroneumatic chamber` would be a great invention for applying aeroneumatics to building things very rapidly like how 

| M | 0 Items |

| N | 1 Items |

### Numbertron

A course in numbers. Numbers can be small. Numbers can be big. But in essense. When you look at a number, what makes it small or big? How come 10 ^ 100 is "big?" (10 to the power of 100) or (ten to the power of one hundred). How come. ten to the power of one thousand is even bigger? How come ten to the power of one million is even bigger.

Well. In Numbertron Course, you can study large numbers like (1) How many videos are on YouTube?

You can go even further like, How many sands are on the beach?

You can go even further like, How many planets are in the Universe?

And of course you can study other things as well.

Whatever you can imagine, don't you think it's actually not that big of a number? You can always make it bigger. And so what are some of the ways of making numbers bigger? Powers. Factorials. But then you also need more notation for those things right?

### Nymphos

[Nymphos Walking Around](./Idea-Invention-Listing/nymphos.md)


| O | 0 Items |

| P | 3 Items |

### Peace Accolade System

`A peace accolade system gives social rewards for members of the community that are evaluated as 'peaceful'`. Being 'peaceful' is a scoring system rank term meant to mean 'you have satisfied the criteria we have defined on this list'

(1) You do your laundry [2.0] <br>
(2) You walk to parks and say hello to neighbors <br>
(3) You visit a historical museum and acknowledge ancestors <br>
(4) You do no harm to someone for 2 to 7 months <br>
(5) You get a point (+1) on your peace program accolade chart <br>
(6) All your friends and colleagues can see how many points you have <br>
(7) You start with 0 points and need 1 or 2 to get hired at any job <br>
(8) Everyone who is peaceful can stay over at random people's houses <br>
(9) Everyone who is peaceful can look forward to the future of the planet being peaceful and thoughtful towards the environment <br>

A company like this is useful to say that there is a video game in society that everyone can play that will give you a badge or a certificate or a bond or a key or a doorknob to access a lot of resources in society. A doorknob system represents the idea that other companies in the social structure can acknowledge the 'peace accolade' and so that 'we will provide more and more free resources for peaceful-accredited people'

Peaceful-accredited people earn a reward. The reward may or may not have long term impact on the social structure but a lot of people in this era of 2021 Earth Style Life shows the author of this invention script that you could really start a nice business that would possibly have all the homeless people housed in random areas throughout whatever town because possibly there is the possibility that more and more homeless people would relax and not say that they lost their peaceful honors because of a crime they committed.

A peaceful honor loss is a big event meaning you have to start from point 0.

Point 0 is where you need to rewind and start to acknowledge all the various aspects of why you failed.

You are a criminal.

But then of course again you can always start to climb back up the social peace ladder with more and more accreditation for free as long as there is an observation that you are satisfying the reward system's criteria metrics.

Well.

Laundry is an umbrella term meant to mean 'you satisfy a whole lot of deals about 'wow, I am super clean''

You brush your teeth for starters but then again some of these things are really needing to be half-observable and so measuring your gums and plaque scores are like 'wow, that person is really healthy'

Look at all the laundry that someone has done and you'll be like 'wow, they brush their teeth, they wipe their bottom, they look like a `social advocate for the species` which is really a racist thing to say but maybe that is because the species is really racist already and needs to say that they have standards that they want to meet'

A Plaque or a Certificate of Honor will be bestowed to you and will be available as a resource for others to collect as a physical prize or an online resource or however else peace can be measured by your association with that particular company or group.

So long as a good majority of social players in the economy know about the intentions and thoughts of the Accolade For Peace Restoration Committee then there are possible demonstrations that more and more community members are restoring peace in their community and the ones that are applying their transformation gifts like staying over at random people's houses for several months in a row will then be figure heads that showcase that peace is really possible and that working at farms for mining resources is really still possible even if you have a personal individual who has their own bicycles to turn. But of course. We want to say that this company really needs to pedal hard to acknowledge people that will step up and be exemplary even if they have been criminals in the past.

There is a need to showcase criminals as being restored so it's a useful thing to say that jails and prison cells will be less and less useful if you have jails and prison cells disguised as houses in nice neighborhoods where restoration is taking place at the cost of not having a weapon placed in your face for the next x-millenia because look how easy the criminals got it with their 200-point evaluation score.

200 points is pretty good. That's possibly many years.

Although this point accolade system for the purpose of peace and relaxing engenderment schemes like 'you are this' and 'you are that'. "You are peace" means there's possibly negative consequences like "will sexual orgasm be the same in a community with peace?" "do you need to fight or be in war or have contradictions that make spasming really fun?" "why wouldn't you want to spasm between life and death?"

If there are war hungry maniacs in the streets, then those dreams for a peaceful social structure . . or for a peaceful-accredited society . . may be . . latent in the species . . latent meaning . . there haven't been usecases for that knowledge yet right?

Free certificates are really amazing.

Paper is cheap to print and people like reading things about what is happening in the community so if they bicycle around and see peaceful-accredited people in the community that is really a big deal. The whole thought terrain for an individual could shift to things like hobbies and math and pornography and riding around being a weirdo in your mind without fear of getting shot or harrassed or eaten by police officers who spam you with a nice baton.

Robot police officers can have their own accreditation program that spams users with nice peace scores. Those are possibly sorry to have and not necessary because -_- robots are supposed to be unbiased and treat you like an ape no matter who you are.

Your peace score system is only for apes that want to treat each other like apes by ignoring social problems like homelessness and food fasting rituals that are involuntary and unnecessary right? right?

-_-

### Personal Feedback 101

There is a standard study course for all teachers and students on a campus. On a college or high school or middle school or elementary school campus there is the interest in educating people but that interest can be very interesting depending on the student-teacher composition. 'what is that teacher good at teaching?' 'what does that teacher like to teach?' 'what does that teacher think about when they are not teaching?' and a list of student questions can also be very similar. 'what does that student like to think about when they are not thinking about school related topics?' School related topics can be so time-all-consuming and so all-time-consuming and so O_O egg me on with more materials professor, right?

Well, a student-teacher conference that is dedicated to having teachers and students interact about subjects like 'how do we interact?' is very useful and effective.

A simple college-level course for each middle schooler or elementary schooler is very useful even for adults which is meant to be an elemental course for students and teachers alike to resignate with various topics like (1) rude behavior and (2) spamming strange subcommunication messages (3) dealing with kids or students that are overly political and standoff-ish or stand-off-ish and bothersome or bo-ther-some or borderline-bother-some or bo-ler-line-bor-ser-son or bo-ler-line-bol-der-son or bo-re-line-bore-le-son

There are a lot of bothersome behaviors but having a nice rule set of appropriate and inappropriate feedback is useful.

This invention idea was influenced by a dream that I had which is available and recorded in my dream journal entry at [3.0]

### Poop Substance Measurement System

`A Poop and Fecal Matter Measurement system` to test how much poop and fecal matter are in food products to possibly say that crops that are grown in fields with sewers nearby won't have human waste or animal waste from unknown sources inside of the final food product or something like that. `Dark`, `Dark Gray`, `Gray`, and `Shades of Gray` and possibly unlikely but `White` can be a supreme food type with no chance of poop but also no chance of anything possibly since chemistry is interesting.

| Q | 0 Items |

| R | 4 Items |

### Randomness Computer

[Notes are available here](./Idea-Invention-Listing/randomness-computer.md)

### Random Hexagonal Scalar Computer

[Notes are Available Here](./Idea-Invention-Listing/random-hexagonal-scalar-computer.md)

### Recommended Book Library For Spiritual Seekers

I, Jon Ide, have read a few of the books on this list. Some of the listings are YouTube videos or information you can find by searching Google or the internet. If you are also interested in Audiobooks, which you can read at night while you're sleeping or before you prepare to go to bed or while you're doing another activity as a sort of passive reading technique, then you can read audiobooks online on YouTube and the nice things about audiobooks is that you can select your own music to be in the background of the book reading in case you want to have (1) peaceful music (2) atmospheric music or other music types that interest you. For a lot of the Seth books I would recommend the video titled "[ Try listening for 3 minutes ] and Fall into deep sleep Immediately with relaxing delta wave music" By  Nhạc sóng não chính gốc Hùng Eker on YouTube at the URL: https://www.youtube.com/watch?v=4MMHXDD_mzs&ab_channel=Nh%E1%BA%A1cs%C3%B3ngn%C3%A3och%C3%ADnhg%E1%BB%91cH%C3%B9ngEker . . Play that video on Repeat for your audiobooks by Tim Hart Hart: https://www.youtube.com/user/Timhart1144/playlists

- (1) Seth, Jane Roberts, Robert F. Butts, (2) Barbara Marciniak, (3) Lacerta (Reptilian), (4) Alien Interview By Matilda O'Donnel McElroy, (5) Orion Council (Krista Raisa), (6) Andromedan Council (Alex Collier), (7) Mantis People (Simon Parkes)

- More Books Can Be Found In This Google Drive Resource Folder: https://drive.google.com/drive/folders/1qjYXtkvfnPomhfBOKuQc_A_H_O1ickJt



### Robotics Transportation Highway

`Robotics transportation highway`. Use drones. Use cars. Use trains. Use line-following robots. Use other trace materials that you can contribute towards dialing into like hippos and insects and beetles if they are already being thought of as transportation vehicles for networking resources. If you need water for example, maybe have food for insects in the environment to transport water in exchange for the food that you leave in trailed environments. Having automated systems like automated buses, trains, and other vehicles that don't necessarily travel above ground, but could possibly travel in underground areas like tunnels constructed for the purpose of high bandwidth energy transportation, then that is very useful to say that `line-following robots` are easy to develop and so all they need to do is go-straight and drop off packages at various locations along their destination. Their destination can be an infinite loop track which doesn't stop but has them collect runtime accessible resources that are randomly available in accordance with recommendation systems that propogate information on what to pick up and when to pick it up.

| S | 1 Items |

### Sex and The Sexual Mechanism

Chapter 1 - You can have sex with a lot of people

Chapter 2 - You can have sex as the only person on your planet to do that

Chapter 3 - It is possible for there to be peace while you are the only person to have sex on your planet


This note is quite lengthy, read more about it on another page

(Notes On Sex and The Sexual Mechanism)[./Idea-Invention-Listing/sex-and-the-sexual-mechanism.md]

[Notes URL](./Idea-Invention-Listing/sex-and-the-sexual-mechanism.md)

### Strings and Crystals

[Notes can be found here](./Idea-Invention-Listing/strings-and-crystals.md)

| T | 4 Items |

### Tackel-box theory

Notes are available here [Notes available here](./Idea-Invetion-Listing/tackel-box-theory.md)

### Thoughts And Ideas On Bicycles

Bicycle spokes are a weird invention. Personally I haven't looked at them very closely. From the note I write here, maybe don't take me very seriously.

Bicycles (1) Bearings (2) Spoked Tires (3) Air Vehicle For Transport (air pressure wheels) (4) Rubber tires (5) Body Frame

(1) `Bearings`. Bearings are special because (1) wheels use these to carry heavy loads. Loads can mean (1) weight or other interesting things depending on the type of bearing (electrolytic load, aeroneumatic load, etc. etc. etc. '`anything you can image O _ O`').

(2) `Spoked Tires`. Tires are a weird idea. (1) press against the item from any direction -_- and make sure -_- it doesn't break. Breaking is a weird concept. -_- . . overall my thoughts are that (1) build houses using 'spoked tires' because (1) they are light weight and (2) they area transportable . . transportable means the idea is O _ O weird O _ O enough O _ O to O _ O be O _ O computer-operable meaning you can compound the effect of house building with more and more spoked tires that spring the thing to life that you are building not like bricks because bricks suggest latency . . but more like springs O _ O right? O _ O it's just a theory O _ O tall houses that are built with spoke tires need to have the idea of (1) circles that push the edge of the circle in all directions and not just 'bricks' or 'wooden plants' that use 1 or a few directions like downward or maybe even horizontally but really if you push a building from the side with enough wind force maybe it would start to tilt like a tower so this is an example where building with non-spoked items is not the best . . because possibly you could advantage the wind and say that your device rotates in a particular angle with respect to its environment . . not necessarily a rolling building but a information translocation building that translates high enough force energy relations towards other usecases . . and so that's why spoked tires are weird . . why do you think a tall building should only care or think or ponder about gravity and other electromotive forces against the surface of the device when there are more angles to consider O _ O . . also . . spoked tires are cheap to make and they are able to be very good in supporting people to build houses that house everyone on the planet very affordably . . Metals are really useful for some reason O _ O but I'm not sure O _ O if you really want to make non-metal spoked tires you may consider elements that are like crystals or foams or plastic materials with a lot of bounciness . . that could work for the spokes . . and then tall nice houses with rooms that don't fall apart would be possible if you apply another idea like 'bearings' that bear the whole weight of the structures xD . . bearings bear the weight of the energy that is centralized by the spoked tires . . xD and so . . xD if your house bearing can hold the pressure of the walls up . . for useful energy usecases like 200,000 people for a simple house building, then those are good buildings O _ O maybe . . it's only a theory . . the practice may be very different . . but the theory of spoked tiredness is O _ O . . O _ O . . spoked-tired-ness . . O _ O is really possibly interesting O _ O . . 

(3) `Air Vehicle For Transport` . . Air Vehicles like bicycle tires are really O _ O . . interesting . . Aeroneumatics is useful O _ O `(1)` sound computers that make music `(2`) moving resources like air for heating a home with a vacuum pump like device `(3)` suction cup robotics that grab things with air pressure variations `(4)` windmills that rely on heat relations in the atmosphere for variational loading for electrical equipment `(5)` gravity relation instrument for interesting dynamics for 'above-water-robotics-simulations' since gravity seems to behave differently in 'fluids' O _ O `(6)` fire O _ O is a weird substance O _ O can you start fire in 'water'? O _ O why? O _ O I don't know the answer to this O _ O but sonoluminscence could possibly be interesting and related O _ O . . A transportable fire pit for poop is a useful invention . . O _ O . . Hopefully the ashes are not poop and crushed by 'air-water-siphoning-techniques' . . Air is really amazing . . Air is weird . . why is soft air so useful at high pressures? . . Well bicycle tires can be proportionated to nice vacuum pump arm actuators for robots except they are 'compile-time' ready and so with bicycle tires you don't need to runtime-reload the air pressure of the bicycle to rapidly reuse the transportation abilities . . 'vacuum-not-vacuum-high-pressure-vacuum-not-vacuum-high-pressure' that type of cycle can be very useful for a bicycle that wants to hop in the sky automatically . . Well that is quite cool to airate a system with 'vacuum-not-vacuum-high-pressure' devices since it can (1) aerate the room and get rid of naughty air that is meant to poison someone . . (2) transport heat and nice warm and nice smelling air that doesn't have poisonous effects :) . . 

(4) `Rubber tires` . . Rubber is a friction component but it's still interesting to think about (1) why is friction so good? (2) what does that mean to have a lot of friction? (3) does grease help rubber tires become less friction?

(5) A `body frame` is useful . . Metal is a good body frame. Plastic is possibly another good body frame. I don't know much about these . . Metal has interesting properties . . Metal has interesting properties . . but for some reason it's also really gross . . since mercury . . a liquid metal could possibly be hazardous to life . . I don't know . .


### Toilets that Use Magma

`Toilets that use magma` to eliminate the poop waste and fecal matter ideas involved in poop and waste fecal matter. Magma is interesting to think how it energizes poop.

### Trace Theory

[Notes and Thoughts](./Idea-Invention-Listing/trace-theory.md)


| U | 1 Items |

### A Universal Basic Income Digital Currency

A Universal Basic Income is a good idea in 2021 and Circa 2021 where (1) People need money (2) People want free housing (3) People want free food (4) People want free shelter (5) People want free clothes. Don't be an idiot and think that you wouldn't want free things either.

(1) Free things are good but first you need to build a nice (1) visually pleasing website like YouTube that is (1) a digital currency wallet for any user that comes to the website. The website needs to follow Material Design themes or Bootstrap themes for the website to look nice and appealing.

(2) You should try something new like (1) Don't expect Amazon or Alibaba or Aliexpress or Shopify to integrate your digital currency into their website on day 1. Imagine that you are a corporate officer. This new digital currency that could take over the US Dollar is really weird. Why should they accept it just because you put your new name on it. That is idiocy.

(3) Build a shopping mall resource into your website so (1) users don't need to shop anywhere else to use your digital currency.

(4) Build a decentralized exchange resource into your website so (1) users don't need to feel like they can't exchange your digital currency for their already familiar "US Dollars" or "Ugandan Shillings" or "Russian Rubles" or "European Euros". Don't be shy. Imagine that your website will be used by millions of different people from around the world. Don't be an idiot. Your website won't be good if it crashes all the time so hire 1 good engineer from somewhere where you trust and make sure you don't spill all the secrets about your currency on day one if you don't want news getting out about your secret idea.

(5) Your idea is to create a digital currency to upend the US Dollar which is being printed by the United States of America repeatedly as if there is a Universal Basic Income already. But people don't need to know that. All you need to know is that you have to (1) work and get a job to pay for your food, house, a car, marriage certificates and other boring things that people are getting tired of. If you are a modern person do some research on the trend called "I don't have a dream job" or "I don't dream of labor". Don't be boring. Take other people's advice. And possibly your own. Work is boring. So do the boring job or making this website work. People will figure out what to do next. You just need to place the product in their hands.

(6) Placing the product in people's hands is difficult if you don't know how to (1) program a computer and (2) program the computer to be efficient and effective at using resources. Computer programming is a discipline you can learn at Udacity.com or Udemy.com with paid courses or free courses that will teach you how to program websites very effectively and very efficiently. It's not a big deal. You can learn it in 6 months if you want to be an expert at it.

(7) If you are tired of your boring job, then it is not a new idea. People like working. People like helping out. But for goodness sakes, things are changing. People don't always want to work. People want to take time off. People want to sleep. People want to spend time with their families. People want to do boring things. People want to have sex. People want to even be polygamous. But it's just too difficult to consider all of these things if you're working all the time. You need to quit your job and work on this website. If you don't quit your job then you're going to be a loser and not have time to do anything.

(8) Explaining why your quit your job to a wife or spouse or marriage partner is a headache. You need support. If you have money saved up. Go live in a cheap bedroom. It's going to be difficult for the time that you need to spend your money because you might be confused for days and days but if you have a good partner they will support you anyway.

(9) A Job Employment Board on Your Website is really important. Imagine that employers today aren't accepting Bitcoin. For example, why does an employer not feel comfortable paying you in Bitcoin or Ethereum? . . Well some employers will but imagine that if everyone switches to using your currency exclusively over using digital US Dollars then why not have a job listing already ready for people to know that these or those jobs or this or that job or those or them jobs are already accepting your Digital Currency. Isn't that a sound idea?

(10) If your digital currency fails it's because (1) your website was ugly. Ugly people fail at getting married so apply the same logic that ugly people don't have sex. If your website isn't having sex. If your website isn't getting traffic. It's because it's ugly. Be a handsome website. Use the website yourself. Ask if it's good. If it's ugly then you're going to also feel that intuitively. (2) Having to sign up with usernames and emails and passwords should be a secondary feature and not a primary feature. If your website locks people out by requiring their name right away, that's really a hurdle that can be cut. A lot of websites do this already like "YouTube" and "Google Search" but you don't even realize it. You don't need to sign in to watch YouTube videos. You don't need to sign in to use Google Search. Why should you sign in to use a digital currency? Microsoft or Facebook or Apple or even Google could possibly fail at making this digital currency because (1) they force the user to sign in . . which is a personal gripe of mine that I will encourage you to avoid. If you don't need to sign in then the default name should be "Customer #123" or something simple and so the user can always go to their (1) personal settings and update those items that are needed. Emails are useful for recovering the information of the bank account or of the currency account or of the digital currency wallet account but for some reason you don't need to make it primary.

(11) Good luck on your digital currency. (1) if it's ugly then it was not a good website for people to use and your traffic numbers will reflect that and people will complain. (2) if you need to sign in then that's painful.

(12) https://gitlab.com/ecorp-org/ecoin is the digital currency that Jon Ide worked on for several months but the architecture of the website changed. The codebase changed. I don't necessarily encourage you to read the codebase but a few iterations ago the website looked really good. Mobile-first design pattern is really good. If the mobile experience is good then the user who uses their iphone or android or mobile computer device will probably be really happy.

(13) Here is a list of organizations that would be really cool to communicate with or learn more about if you really want to know why a Universal Basic Income project is quite a serious endeavor:

- The Venus Project, Unicult (By Unicole Unicron), Tree Sisters, Habitat For Humanity, Amanda Flaker, Paget Kagy, Elon Musk, Andrew Yang, Jack Dorsey

| V | 0 Items |

| W | 2 Items |

### Windmills on Vehicles Of Interest

`Windmills on car vehicles`, bicycles, airplanes and other mobile transportation vehicles that rely on wind-friction-based transportation networks. Electromagnetic windmills can trace electromagnetic curves in the air, aeroneumatic windmills can trace airomagnetic curves in the air like airpressure curves in the air but specifically meant to insight a magnetic action-at-a-distance mindset in your mind to say that most things are really just action-at-a-distance and so it's like real physics is like 'yea, maybe' 'yea, maybe' 'yea, maybe' 'yea, maybe' with a rubberband system tossing all sorts of 'movies that are like yea that could possibly be a believable movie' 'yea that could possibly be a believable movie' right in your face. Aquaneumatic windmills can trace aquamagnetic curves in the air like waterpressure curves in the air but specifically meant to insight lambda curves of references right back in your face to look right up this water paragraph and swim to the next aisle where you left off from your reading magnetic rrrrrr rrrrr rrrr. If you can transcribe that watery message, you are a really good scuba diver.

### Windmills That Electrify Their Own Propulsion

`Windmills that electrify their own propulsion using magnets`. An electromagnetic wind is an energy source that can be harvested by a windmill that has magnets that align with the magnetic pulse sensations of the electromagnetic wind. This is a gear and pulley system that starts itself. Gear and pulley systems are interesting to study. Gears are rigid and well defined and 'hey, you have to do that right now.' and pulleys are soft types of mechanical systems that are more like 'hey, you should follow me.' Gears spin and they ask you questions about 'you better not resist my energy' and pulleys ask things about 'you don't have to follow me, but maybe you will someday, if I pull for long enough or for eons away enough. maybe you'll come. maybe.' Air is a mix of gears and pulleys from my personal analysis but there is possibly a special theory involved there to say that there is one type of pulley that is really out of its way and won't let you exchange certain ideas without assuming 'yea, maybe this wasn't the right way, sorry, let's turn around' but that is a negligent way of describing an interesting mapping of 'how do physics elements relate to gears and pulleys?' [Research tells me that `Searl Generators` could be the name of these devices as they exist in the media that I'm familiar with today. I'm sorry if I'm mistaken]

| X | 0 Items |

| Y | 0 Items |

| Z | 0 Items |

| 0 | 0 Items |

| 1 | 0 Items |

| 2 | 0 Items |

| 3 | 0 Items |

| 4 | 0 Items |

| 5 | 0 Items |

| 6 | 0 Items |

| 7 | 0 Items |

| 8 | 0 Items |

| 9 | 0 Items |


| Minimum Description References | 3 Items |

[1.0]
Boston Dynamics unveils Stretch: a new robot designed to move boxes in warehouses
By James Vincent
https://www.theverge.com/2021/3/29/22349978/boston-dynamics-stretch-robot-warehouse-logistics

[2.0]
Inspiration for the term 'laundry'
By Jon Ide
[Dialog Entry Relational Journal 0 / Dream Journal / April 16, 2021]
[Resource URL]

[3.0]
Inspiration for 'Personal Feedback 101'
By Jon Ide
[Dialog Entry Relational Journal 0 / Dream Journal / April ??, 2021]
[Resource URL]

[4.0]
Or maybe the idea was introduced to me on May 7, 2021 if not May 8, 2021. . One of these two days seems to make sense. Plus or minus 1 or 2 days.


[5.0]
Pornography 101 / July 29, 2021
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Journal%20On%20Perverted%20Things/1%20-%20Journal%20On%20Perverted%20Classes/1%20-%20Pornography%20101/Day%20Journal/2021%20A.D./Month%207%20-%20July/July%2029%2C%202021



| Median Description References | 0 Items |

[1.1]


| Maximum Description References | 0 Items |

[1.2]



........................................................................

........................................................................

........................................................................

Notes about these notes:
- Linear combinations of all of these ideas are possible
- And so it's kind of weird if you (1) find something
- And then don't relate it to the other things that are listed on here
- Basically you need to use 'tackel-box theory' to wrap and unwrap the learnings that you made
- Tackel box theory makes it so that you can wrap new cords around existing cords and wrap those around repeatedly
- L.O.L. tackel-box theory was first described in the Pornography 101 course here [5.0]
- L.O.L. I hope those notes are useful. They are kind of funny and full of horny and perverted comments. Certainly you may not think to yourself as an inventor or idealist that pornography is hot enough to ensue its own nice theoretical constructs.
- L.O.L. The idea of invisible fish that float around in a classroom or closed room like in the show "Hunter X Hunter" is hilarious.
- Basically Tackel box theory says that maybe it's true even now that spirits or ghosts or flying apparatuses are floating all around us
- Basically they keep us alive
- So when they disappear
- We all die
- L.O.L.
- But it's just a theory. I don't really know how it works

.......................................................................

........................................................................

........................................................................

........................................................................






# Uncategorized Notes

Make Love To Jon All Day

Repeated Iterative Repeat Machine

Catch Up Criteria 

Iterations of "Repeated Iterative Repeat Machines" and "Catch Up Criteria"

Iterations of "Repeated Iterative Repeat Machines" "Needlessly Hypnotic Theory" and "Catch Up Criteria"

...

Software For Concentric Circles Application

...

Sprinkler systems

(1) Sprinklers

...

Reverse Hypnosis Idea

. . . 

String Concentricity Idea (`Written` > `strings and crystals`)

. . . 

That One Really Weird Idea #1 (`Written`)

. . . 


....

Pendulous Strings


