

# Insect


Variable Tree Legend Table

- ⭐ [completely] Completely A Permanent Beauty
- ✨ [partially] Partially A Permanent Beauty
- ❄️ [yes-but-also] More Hybrid With Different Beauty Types Speaking More Around Certain Areas Of The Head . . Shape . . Complex . . 


### Human Female Insects

-- F

Fei-Fei Li ⭐ [completely]


-- N

Nan Rong ⭐ [completely]




### Human Male Insects

-- A

Andreas Antonopoulos ⭐ [completely]

Andrew Ng ✨ [partially]



[1.0]
Nan Rong, PhD
https://www.vicarious.com/company/#15





