


# Journal Entry - December 20, 2020

[Written @ 10:37]

I recently discovered . . that . . one person can look like another person . . 

-__- -_- . . -_- I don't really know what I'm doing . . I'm sorry . . I thought my feelings would help me to categorize the order of people's beauty appearances . . in terms of their . . head-face-personality-interpretation structure in terms of using -_- or applying -_- . . my -_- -_- -_- personal -_- . . -_- feelings -_- . . to see-_- . . what are the possibilities -_- . . -_-

But . . how is it possible -_- that-_- one person can look similar to another person-_- in terms of the physical appearances . . and yet the . . 

"Beauty Type" . . or their . . 

Appearance of Personality Type is completely different from the person . . that was previously -_- considered -_- ? . . -_- I don't know if this is -_- real

Okay . . 

Person #1 . . looks like . . 
Person #2 . . in terms of their physical bone shape and physical uh attributes like the overall complexion of the contour of their facial features collected . . and interpretted by an individual -_- stupid description I know -_- but -_- 

. . 

One dog looks like another dog -_- . . but what does that mean ? . . even if they are identical twins . . that could -_- . . -_- . . not necessarily mean they have the same personality -_- . . right ? . . 

-_- . . okay well I think part of the development of the thing that I'm doing -_- looking at my feelings . . to determine the type of beauty category for the person -_- . . I think I'm trying to take into account -_- certain features that aren't necessarily visible to the -_- human eye perception and so those and features like bravery . . integrity . . authenticity . . -_- . . whatever those mean . . -_- .

-_- . . 

-_- . . 

honor . . -_- . . courage . . 

_--_-_- . . 

hmm . . honor and courage . . are interesting names . . 

Well . . Today . . I think it was today . . December 19, 2020 . . ahh . . well -_- I think then it was yesterday since . . December 19, 2020 was the previous day . . and I think the previous day . . -__ . . to December 20, 2020 . . is when I had this . . quote-on-quote "revelation" -_- but that's a weird way of writing this -_- it was -_- uh . . a thought that came to mind . . and then . . -_- . . I wrote it down . . -_- . . 

A person . . appeared in my mind . . they were a male person . . and appeared to have their arms crossed across their chest . . -_- . . uhm . . -__ . . crossed is up to interpretration if I don't describe that meaning -_- . . uh . . they crossed their arms like . . -_- . . -_- . . uh . . they were a police officer or something . . or they were looking down at you . . or a person that is overlooking you and studying you . . and they give you some advice or something like that . . or maybe they cross their arms like they're . . partially not open to the oversation and they want to stand strong . . and so those are possible interpretations of the meaning of the crossed arms -_- -_- . . -_- . . their crossed arms . . 

and so -_- . . their arms . . 

-_- . . 

the person had their arms crossed . . and I'm not exactly sure but I think I got the impression that they had a gray beard . . and were in their elderly years although they looked young and partially feminine in terms of their masculine beauty features . . or something like that . . something like that . . 

uh . . 

The person said . . or . . I heard the voice . . a voice . . said . . something -_- . . and I'm associating the sound to what the person said . . I'm not exactly sure if their lips moved . . to help me point at more information that they were possibly the one that said these things . . but . . I heard the voice . . I heard . . a particular voice . . -_- . . say . . to me . . xD . . -_- . . -_- I think they were talking to me -_- but then again I could have been having a day dream . . and the . . sound was directed -_- . . in -_- a way that I -_- felt like I was -_- in the responsibility corner -_- for -_- picking up -_- the soundwaves

"You can talk to honor" . . 

"You can talk to Honor" . . 

I wrote . . in my notebook the text that reads . . "You can talk to Honor." . . 

Later in the day . . I started to think about what this message meant . . 

I -_- . . was playing around with -_- . . uh -_- . . other things during the day . . articifial intelligence related things . . and . . the topic of . . this message came to mind . . -_- . . I don't have a lot of time to live stream -_- so -_- uh . . well I think YouTube . . stops the video -_- . . at . . 11:55:00 . . or something like that . . stops the video or . . uh . . doesn't publish the video if it's longer than that . . so I'm sorry -_- . . I think it's the latter -_- . . the video may not be published . . if it's . . super long . . like . . uh . . 11:55:00 . . or longer . . 11 hours . . 55 minutes . . and 00 seconds or longer . . 

Okay . . well . . the name . . "Honor" . . uh . . It could possibly be interpretted as . . a person . . 

And so for example . . "Honor" could have been a person . . in the history of human life . . 

"Honor" Could have been the name of a person . . and that person . . uh . . shared certain qualities with humankind . . or the humanspecies . . that . . was then . . a way . . to communicate . . those abilities . . to the rest of the species . . and so . . for example . . the ability of having . . "Honor" . . uh . . "Honor" for yourself . . or . . "Honor" for your . . tribe . . or for your people . . uh . . could have possibly originated . . from the qualities of an individual person who shared certain types of ideas or shared certain types of things that the people they required it of themselves to spread this idea . . or interest . . or something like that . . 

To be . . "James"
To be . . "Holy"
To be . . "Heather"
To be . . "Pride"
To be . . "Honor"
To be . . -__-

hmm-_- -_- I'm not sure -_- one moment there are a lot of possible names that could be associated with a person . . 

To be . . "Faith"
To be . . "Grateful"
To be . . "Beautiful"
To be . . "Glorious"
To be . . "Glory"
To be . . "Righteous" . . 

These qualities could have stemmed from the abilities of personality that human individuals shared with other humans . . in the history of human civilizations . . and so . . 

hmm . . 

I'm not sure . . 

-_- . . 

I'm really not sure -_- . . 

uh . . I'm not sure . . 

To be Not Sure . . 

That could be a name . . 

. . 

hmm . . the qualities of not being sure about something . . are possibly related to . . humbleness . . 

To be . . "Humble" . . 
To be . . "Fluent" . . 
To be . . "Respectful" . . 
To be . . "Kind" . . 
To be . . "Fair" . . 

. . 

hmm . . 

To be . . "Keen" . . 
To be . . "Wise" . . 
To be . . "Observant" . . 

Means you can ascribe with your consciousness . . particular qualities of focus that allow you to shape the activities of your daily life -_- right ? . . -_- . . possibly that's not necessarily the care -_- . . if you're "Not Sure" -_- . . -_- . . 

. . 

-_- . . 

-_- . . 

To be . . "Not Stupid"
To be . . "Stupid"
To be . . "Smart"
To be . . "Clever"
To be . . "Resourceful"
 . . 

what are these qualities . . and what do they mean to a human being ? . . 

What are these qualities and what do they mean to a rat ? . . a rodent ? . . 

What does it mean to be . . 

a mouse ? . . 

To be . . "Mouse"

. . 

[1.0 @ 0:09:04 - 0:30:00] suggests the possibility of "A Moment of Mouse" [1.0 @ 00:11:10] . . which . . in my present interpretation seems to suggest . . the quality . . of being . . peaceful . . A moment of . . "peacefulness" . . a . . moment . . of . . non-violence . . since possibly mice could be associated with . . being quiet . . or . . silent . . along with other qualities . . like . . quick-mindenedness . . especially if they're trapped in a corner and are looking for an escape . . they would possibly want a way to quickly escape and so thinking quickly could be . . something that a mouse could be interesting in doing xD . . 

Pinky the brain is a mouse xD . . -_- . . [2.0] -_- . . I said that wrong didn't I . . -_- . . I think I meant to say . . Brain . . in . . "Pinky and the Brain" is a mouse -_- . . -_- . . hmm . . -_- . . and I'm not sure what type of point I was trying to make -_- . . I think I wanted to say that smart people are quiet like mice -_- . . -_- . . and Brain in the "Pinky and the Brain" show -_- . . is possibly considerable as a quiet . . mouse . . -_- . . and I was trying to draw a correlation -_- . . but . . -_- . . Pinky is also a mouse -_- . . and they seem more . . -_- . . talkative -_- . . -_- . . so my correlation -_- . . is -_- . . -_- . . stupid -_- . . right ? -_- . . -_- right . . that's correct -_- . . 

To be . . "Candid" . . 

[1.0]
Emo Nemo | This Past Weekend #115
By Theo Von
https://www.youtube.com/watch?v=Y2LceDOQttg&t=481s

[2.0]
Pinky and the Brain
[Television Show]






[Written @ 6:30]

(4) I don't want to give the impression that I'm a homosexual male person -_- . . 


-_- . . 

-_- . . 

-_- . . 

Could be a category of beauty . . -_- . . 

-__ . . 

-__ . . 

-__ . . 

-_- . . 

-_- 

I'm not a homosexual person -_- . 

-_- . 

-_- . 

-_- . 

There is also the question of . . "Is it . . 'Boyfriend Beauty' . . or . . 'Boyfriend' . . or . . 'Boyfriend Beauty Type' . . or . . 'Boyfriend'" . . 

Boyfriend is fine . . do you agree ? . . 


[Written @ 6:20]

. . 

Possibly . . 

I should have . . 

A . . 

System . . 

For . . 

Identifying . . 

-_- . . The beauty category of someone -_- . . 

However . . 

(1) Sometimes I feel like the names could be offensive . . and since I'm making them up in realtime . . -_- . . according to my feelings . . -_- . . it's -_- . . not nice . . -_- . . to share -_- . . if people are going to be offended -_- . . 

(2) Possibly someone else feels differently about the beauty type of the person . . and so even if I have a name . . possibly someone else wouldn't agree on that name . . 

(3) I don't know how to think about the categories of my feelings when I have views of certain people -_- . . it's like I'm trying to take . . a map of my feelings of certain people and compress it into a few words such that possibly other people that would have the same impression or possible combinatistic terms being applied in terms of the feeling relations would also have the same terms being applied to the person and so for example mixing and matching terms would be a common feature since people can have all sorts of various categories of personality characteristics that could possibly be mixed and matched even if there are a lot of nuances involved like personality nuances like not always matching up exactly and so there for needing a new category of ideas to be splayed about in front of the audience to be considered for the partaking of appropriation in terms of if that type of item is a particularly good fit for that type of discrimination pattern or something like that . . 

And so maybe for example people can disagree on the meaning of an area of grass . . and . . whose property the grass belongs to . . and so a group of committed citizens somewhere can keep a recordbook of these timed-agreement patterns that a lot of other people agree on and so then that recordbook can be a source of understanding of a great group of people -_- . . although it's not only the only understanding source -_- and for for example . . one can take into account their own intuition about how they see the world . . and so -_- . . possibly -_- . . -_- . . stepping on someone's area of grass -_- . . -_- maybe -_- . . a secret -_- attempt at social -_- banishment -_- -_- but maybe not -_- if for example the people change the recordbook to reflect other possible sources of information such as for example . . the person who is said to own that property is seen to have not paid their latest taxes . . or something like that . . and so a property violation is not -_- . . valid . . or something like that -_- . . or maybe the planet is about to lose -_- . . it's ability to support itself due to an asteroid entering the atmosphere and so -_- recordbooks of property information are said to be void for the last few moments that everyone has together to pray and hold hands in any particular grass field they like-_- . . but that's not necessarily the case -_- . . -_- . . and maybe it is . . -_- . . and maybe it . . 

. . and maybe . . it . . 

and maybe it . . 

and maybe it 

. . 

. . 




[Written @ 6:06]

An Insect Type is . . horny for information . . insect types are collecting information for so much . . they forget to sleep . . they are like . . mosquitos sucking blood . . from information resources . . 

The insect type can sometimes look pale . . The insect type isn't necessarily un-social . . they may like to socialize . . like . . insects that like to socialize . . like . . 

The Insect . . Type . . gives me the impression . . of . . buzzing . . and . . speed . . and quickness . . and also . . the thick skin of an insect . . or what is called the exoskeleton in those Insect style technical terminologies . . The Insect Type . . is able . . to communicate effectively . . in terms of particular topics that they are highly specialized in . . 

The Insect Type Communicator . . Tells stories about patterns . . of information . . and where the information is going . . and . . tends to possibly study . . the future topics . . and not necessarily topics about the past . . tendencies . . although that's not necessarily the case . . for any particular Insect . . 



[Written @ 5:51]

-_- . . I don't know what to think about my naming prototypes for male beauty types . . -_- . . personally . . I suppose -_- . . my -_- . . -_- . . -_- . . heterosexual inclinations -_- . . could be involved -_- . . in . . -_- . . biasing . . the names towards . . 

Ordinary Person
Patrick
George

. . . 

Beauty Boy I suppose is a type that I . . feel like . . some people . . could possibly be associated with this category . . of beauty . . uh . . since perhaps . . maybe . . I get . . the . . uh . . personal -_- . . personal . . feeling . . -_- . . uh . . that . . the person . . -_- . . uh . . is interested in the idea . . of being a beautiful person . . but . . is also . . perhaps . . -_- . . ordinary . . in a particular sense . . ordinary . . beauty person . . but . . they are beautiful . . in the sense . . that they are . . a . . "Beauty Boy" . . type -_- -_- . . -_- this is really difficult to write . . I think each person is beautiful in their own ways . . uh . . but at least Beauty Boys are a way to describe the type of beauty for a particular type of . . male person beauty . . actually . . when i think about it . . there are also women who could possibly be ascribed with the . . "Beauty Boy" Category of Beauty . . 

uhm . . hmm . . 

Beauty Boy . . 

Drama Boy . . 

God Beauty . . 

Chris Hemsworth is a "God Beauty" type . . 

. . 

hmm . . I think there are a lot more categories of beauty types for . . men . . 

Well . . I think I am really not even sure about this who idea of categorizing beauty types . . and uh . . I was originally interested in . . the type of beauty . . which I initially called "Permanent Beauty" . . I didn't know that there could be so many people that could look similarly and be so beautiful in my perception -_- . . the name "Permanent Beauty" came to mind . . I must have been in a -_- . . it's complicated . . I'm not sure -_- I think I was . . having fun . . but not really sure -_- . . it's not like I -_- . . felt that I planned . . to call these people . . "Permanent Beauties" . . -_- . . the name . . -_- . . came to me . . by some sort of . . associative . . inspirational process . . where . . I was inspired . . in some . . way where the logic in my brain . . was automatic . . and my feelings were expressed in that sort of way . . and "Permanent Beauty" has been the name that I called . . this type of beauty . . for the . . past . . few months . . since that type of . . beauty-name-association-type . . process . . was first initiated . . uh . . more than . . 6 months ago now . . and I uh . . didn't uh . . focus on it . . uh -_- . . but still now . . after . . uh . . over 6 months has elapsed . . I'm uh . . well  . . I like the . . idea that I'm still -_- . . uh . . really interested . . uh . . in this name as an association uh . . for this type of . . uh . . beauty . . or for this type of . . appearance of a person's personality . . 

. . the appearance of personality . . 

the appearance of personality . . 

the appearance of personality . . 

maybe it is possible to see someone's personality by identifying the beauty types they are involved with . . or by identifying those types of beauties that . . uh . . their . . uh . . physical . . uh . . face . . expression uh . . or physical body expression seems to share . . but maybe it's also personalized . . and so for example . . if a human being like myself . . sees one person as having a particular type of name for this particular type of beauty . . maybe . . that isn't necessarily shared . . as an idea across the species . .

. . 

other people may think "permanent beauty" isn't the appropriate name . . for this . . type of . . personality . . appearance . . relation-profiling-group-matrix-symbol-categorization-process-denomination . . -_- . . 
-_-
-_-
don't
-_-

-_-
I'm playing around -_-
This is my personal notebook journal so don't listen to what I say

-_-



[Written @ 5:15]

[Copy-pasted from Notes @ Person Journal @ Permanent Beauty @ README.md]

-_- Permanent Beauty is a style . . of beauty . . that I made up in order to . . categorize . . the type of appearance . . of certain people -_- . . -_- . . I'm not exactly sure . . -_- . . how to . . relate the beauty type . . across different . . -_- . . ethnicities . . -_- . . and so for example . . uknown girl person #1 . . could possibly be a Permanent . . Beauty . . for the . . Asian . . or uh . . possibly . . Chinese Asian . . style . . of uh . . permanent beauty -_- . . but -_- . . uh . . that's uh -_- . . not what uh -_- I had in mind . . when first thinking about this style of categorization system -_- . . -_- . . uh . . for . . uknown girl person #1 . . I would say they are . . a uh . . Feminine Beauty . . which is a different style of beauty . . that I just made up -_- . . I am making terms up that fit my feelings . . and if those feelings are aligned . . in the way the word sounds or feels or . . what the word suggests . . then . . that seems like . . an okay way of doing things for now . . I don't know about measurements like the shape of the eye . . or the shape of the teeth . . or the shape of the personality of the people . . but those are things that I also try to take into account when thinking about the naming of the beauty type categorization personal association distinction -_- or something like that . . -_- . . Rose Type . . Rose Beauty Type could also be a beauty type . . which I uh . . hmm . . I'm not sure . . I'm uh . . -_- . . I think it could work . . in terms of describing someone who feels like . . they are . . a. .  "Rose Beauty Type" . . and uh . . so probably it's possible for other people . . to feel their own feelings and give a name for the beauty type . . "Makeup Girl Beauty Type" . . is another type you could use . . uh . . girls that . . uh . . wear makeup . . and uh . . give you the feeling that they really like their face . . in the type of way that suggests . . that possibly there are makeup routines being involved . . but that's not necessarily the only condition type that can cause the person to be involved in a "Makeup Girl Beauty Type" . . the . . name uh . . -_- . . is more like a feeling suggestion pattern . . and can possibly be individualized for any particular individual . . and so for example . . although . . these people . . in this group of . . categories of beauty . . are thought to me . . as the author of this message . . me . . uh . . -_- jon ide . . -_- . . uh . . -_- . . you may not . . uh  . . you the reader of the message -_- . . your-name-here -_- . . may not . . find that the . . -_- . . beauty type name is fitting for that type of individual or for that group of individuals . . 

-_- . . beauty is possibly unique for each individual . . and yet . . there is the possibility that . . beauty can be . . uh . . -_- . . sympathetically dialected -_- . . through the -_- social collective -_- understanding -_- . . 

For example . . -_- . . uh . . this list is . . possibly . . hopefully . . a list . . that can . . be . . an . . interesting . . starting point for people . . to learn more about the beauty types they find . . 

Uh . . "Appearance of Personality" . . -_- . . that could possibly be a way to look at the personality of someone by identifying their beauty type . . 

There are . . Goddess Girls . . that are so cool . . but it's not entirely clear to me . . when and when . . I am looking at a goddess girl -_- . . uh -_ to be honest this is a compeltely new topic to me -_- . . it seems like a way of thinking about all the orders of the types of beautities . . of an individual  . . un for example . . uh . . Mother Beauty Type . . is another type of beauty . . when the person gives you vibes -_- of -_- possibly being a friendly motherly person -_- . . or something like that . . -_- Mother Beauty Type . . in my personal interpretation . . which by the way is another beauty type that I made up . . right now . . while looking at the photograph . . on the right . . A person -_- . . I don't know -_- . . they give you motherly energy types . . and that they would take care of their children or something like that . . And that they think about their children's futures . . or something like that . . and it's . . uh . . maybe . . uh . . in uh . . -_- . . I guess . . uh . . complementary to the type of beauty that could be . . like uh . . Ditch Your Daughter At School Day Beauty Type . . which is a type of beauty type that could be like . . the person has wet hair . . and the hair's not combed . . the hair . . is dark colored . . and the person is pale . . and thin bodied . . and they possibly . . want to leave their daughter at school all day . . at Pale Middle School . . xD -_- xD . . Pale Middle School xD . . The personality type of a . . Ditch Your Daughter At School Day Beauty Type . . is someone who likes to be independent . . they have children . . these beauty types are . . beautiful in their own respect . . uh . . there are . . uh . . 




