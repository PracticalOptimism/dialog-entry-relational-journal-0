
# John Perry Barlow

https://en.wikipedia.org/wiki/John_Perry_Barlow


Barlow in August 2012
Born	October 3, 1947
near Cora, Wyoming, U.S.
Died	February 7, 2018 (aged 70)
San Francisco, California, U.S.
Occupation	
Lyricistessayist
Alma mater	Wesleyan University
Period	1971–95 (lyrics)
1990–2018 (essays)
Subject	Internet (essays)





Barlow serving as wedding minister at Mount Tamalpais on July 11, 2014
![John Perry Barlow](./Photographs/440px-John_Perry_Barlow_serving_as_wedding_minister_at_Mount_Tamalpais.jpg)
