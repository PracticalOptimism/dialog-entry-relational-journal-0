









[Written @ December 15, 2020 @ 12:41]


Notes @ Video [1.0]

UNITED STATES
Fixing Up My Cabin - cont. (ft. me going slowly insane)
322,504 views•Nov 22, 2020

30K

225

SHARE

SAVE


hannahleeduggan
369K subscribers
I promised you guys I would keep you updated on fixing up my cabin, so here it is in all of it's chaoticness. 🙃 I'm going to be completely honest and say I don't know what happened with this video... I think I might have started losing my mind near the end. But all in all, i'm super happy with how the mudroom/pantry turned out, and feel like it really makes the cabin THAT much cozier.
Thanks for putting up with me 😂♥️ 

00:00 - Morning
00:49 - Intro
01:32 - Day 1
04:00 - Reliving my crush on Jeremy Sumpter
05:12 - Day 2
06:02 - Snow Walk
07:37 - Back to Work
10:46 - Day 4
13:05  - Day 5
15:55 - Day 6
17:42 - Oh, you thought we were done?
21:00 - Before & After

Current Subscribers: 338K
___

♥️✨Ways To Support My Channel✨♥️

Shop My Closet: https://www.depop.com/hannahleeduggan

___

📱Socials📱

Instagram: http://bit.ly/3b4EKCj
TikTok: https://bit.ly/37lVDHP
Pinterest: https://bit.ly/2O27XEp
Facebook: https://bit.ly/2NWGSlW
Twitter: http://bit.ly/38SHWPF
___

🎶 Music 🎶 

Apple Playlist - https://apple.co/3nJCjv6

Babe - Sugarland (ft. Taylor Swift)
Don't Waste My Time - Dayon
Becoming - Michele Obama (Audiobook)
You Can Fly! You Can Fly! You Can Fly! - Peter Pan
Drunk In Love - Beyonce
Memories of Sardinia - Franz Gordon
Cry - Ashnikko (ft. Grimes)
Cradles - Sub Urban
Lullaby. - Niykee Heaton
Faking It - Kehlani
Started - Iggy Azalea
Something Good - Sound of Music
Without You Tonight - Tomppabeats
Be My Baby Tonight - John Michael Montgomery
Good Direction - Billy Currington
Head Over Boots - Jon Pardi
Drunk On A Plane - Dierks Bentley
American Kids - Kenny Chesney
Make Me Wanna - Thomas Rhett
No Matter The Season - Sara Kays
Chosen Last - Sara Kays
All For You - Kid Quill (ft Sara Kays)
Remember That Night - Sara Kays
No Right To Love You - Rhys Lewis
Talking To Myself (stripped) - Gatlin
Sophia - Juke Ross
I Forgot That You Existed - Taylor Swift
You Broke Up With Me - Walker Hayes
Atmosphere - Sunshine
Buzzin' - Shwayze
The Number None - Atmosphere
Sirens - Sublime with Rome
Back Around (acoustic) - Iration
Brown Eyed Girl - Van Morrison
September - Earth, Wind & Fire
Eye of The Tiger - Survivor
American Pie - Don Mclean
Cecilia - Simon & Garfunkel
Sounds of Patriots - Trabant 33
Glasgow Noon - Trabant 33

___

💁🏼‍♀️FAQ💁🏼‍♀️

*What kind of van do you have/how much did it cost/how many miles did it have on it when you bought it?*

My van is a 2001 Ford Econoline E250 that I found via Craigslis., I bought it with 67k miles on it - it was listed for $3000, but I got them down to $2400. 
My Van Build: http://bit.ly/2x3bv3M
My Van Build Cost: https://bit.ly/32SzUXF
Van Life Playlist: https://bit.ly/2D5CT4c

*What sewing machine do you use?* https://amzn.to/2EigFwH

*What are you filming on?* iPhone 11 Pro

*How do you make money?* I sell clothes on Depop (link above), and get ad revenue from YouTube! I also have a video on ways I've made money in the past; http://bit.ly/3d1451P


[1.0]
Fixing Up My Cabin - cont. (ft. me going slowly insane)
By hannahleeduggan
https://www.youtube.com/watch?v=n1KGCLPWMTw

