
# Artificial Intelligence
## Day Journal Entry - September 15, 2020


[Written @ 17:12]

Time-based associative algorithms 


Time is an interesting concept . . I'm not sure how to go about talking about this topic . . 

Time is an interesting topic . . And I'm not sure how to talk about this topic . . 

Time is an interesting topic . . and I'm not sure how to talk about this topic . . 

Time-based associative algorithms are thoughtful constructs when considering a program for human-related intelligence algorithms . . 

Time-based associative algorithms are interesting topics to relate to . . on the one hand . . time can be seen as an absolute measure that is uniform and constant for all of space-time based organisms or constructs . . and so the time today . . the time now . . is really . . a time . . that is . . always now . . and today . . everywhere . . for all space time organisms or something like that . . 

And so the reading of a clock . . reads the same . . here . . and now . . and . . when the clock is read . . somewhere . . else . . maybe on another planet . . . . uhm . . maybe . . the time can be read the same . . or maybe the time is read . . similarly . . and so . . 1 second . . or 1 minute . . or 1 hour . . occupies . . the same . . uhm . . object of interest . . or . . object of focus . . or something like this . . or something like that . . 

Uhm . Well . I'm not really sure . personally how to think about this topic of time . I'm really . Uhm . A researcher that's uhm . Experienced in uhm . reading about various interpretations of how time works . And well . I'm uh . Still really quite interested in learning more . Besides that . I'm uh . Well . When I think about time-based associative algorithms . . Uhm . 

Hmm . . "Time is an arbitrary measure of the movement of objects through space" . . "space is the focus of interest for any particular consciousness creature" . . These are the words read from a book called "The Alien Interview" . . spoken . . by Airl . . The interviewee . . or something like this . . or something like this .

In any case . . that's not an exact quote . . and I will have to reference the quote another time . . and provide a link to the page . . in the book where this is talked about . . but that's my current understanding . . Time is a measure of the movement of objects through space . . time is a measure of the movement . . of focus . . of a particular consciousness being . . uhm . . space . . is created by consciousness . . And so the space . . of a three-dimensional world . . as replicated or as demonstrated in video games . . like . . . . Uhm . . in 3-dimensional video game model simulation environments . . Uhm . . That is a particular way of uhm . representing . . observed reality by a human being for example . . 

Uhm . And our physical senses as human beings seems to relate to us . . the information of a 3-dimensional environment . . uhm . I'm not sure . I think it really depends on the individual . . Uhm . I'm really not sure . I am starting to believe more and more of the possibility of having a world where there are multiple realities overlapping one another . . and the focus of our consciousnes . . directs the perception . . or what is perceived . . while these alternate realities are still existing on their own even if the . . uhm . perceiver's focus isn't necessarily directed in their observation . or something like this . . uhm . . I'm not sure . . that description is also not really what I understand but is something related to what I think . . uhm . . Uhm . . well . . uhm . . hmm . . well . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . hmm . . well . . 

In those last . . few . . "hmm" . . statements . . I was thinking of . . how to continue the discussion from there . . uhm . can you guess . . what I would have written if I can go back to replace each "hmm" . . and replace the whole conversation with an english sentence that talks about something ? . . Well . . hmm . . is like . . a psychological space . . that maybe suggests thinking . . And so . . maybe when you read that . . series of . . words . . you thought to yourself . . "hmm" . . they must be thinking about something . . And maybe they're not sure what to say . . And so . . the words came out as . . "hmm" . . "hmm" . . "hmm" . . [paused for 60 seconds to think] . . and the pauses of time . . that it takes to think of the next thing to say . . aren't necessarily translated for you . . how they are translated for . . me the person who is writing the message . . and so . . even though . . maybe it takes . . a few minutes for me to write this message for example . . your space-time perception of how long it takes to consume the message . . can be perceived differently . . or . uhm . . well . . I suppose . . in other words . . it could take you . . more time to read the message . . uhm . . more time . . that . . it took me to write . . or maybe . . less time . . uhm . . 







