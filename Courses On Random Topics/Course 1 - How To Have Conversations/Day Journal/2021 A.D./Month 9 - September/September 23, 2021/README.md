
# How To Have Conversations - Day Journal Entry - September 23, 2021




### [Written @ 10:30 - 10:30]

Eventually Robotics could take care of the role of therapist.

### [Written @ 10:29 - 10:29]

Councelling means (1) talking (2) right?

So conversations for many hours at a time where you can doodle in the mind of your therapist is really fun.

24 hour doodles


### [Written @ 10:27 - 10:29]

Getting counselling is the next big thing

(1) How can I improve my life now that I need to punch that guy in the face

(2) Do I train for several months before doing all of that

(3) It should be allowed to say to the ring judge that 'hey, I punched him in the face, now I want to get out of the ring'

(4) and then the ring judge says 'alright, you're right.' 'do you have a problem with that?' the judge says to the offending partner 'no' 'yes' or something like that

(5) The fight ends sometime since the fight isn't that official.

.......................................................................

Fighting

.......................................................................


### [Written @ 10:13 - 10:27]

(1) "You did it. It is your fault that all of this happened to me"

is the standard role that people might see victims as perpetrating.

However, if you look closely at the world

(1) You are not apart from it.

(2) You are a part of it.

.......................................................................

Apart. A part. There is confusion in these words.

.......................................................................

(1) "You did it. It is your fault that all of this happened to me"

is the standard role that people might see victims as perpetrating.

.......................................................................

"I am the victim"

.......................................................................

"I am at fault"

.......................................................................

"I am at fault because I recognize that I am responsible for being able to recognize that I am responsible"

.......................................................................

"They punched me"

.......................................................................

When someone punches you. You are a victim.

.......................................................................

But if you look around and ask "what am I being a victim to?"

.......................................................................

(1) The person punched you

(2) But your mind is the one holding on to that reality

(3) So you are free from being the one to blame

(4) You did not do anything wrong by holding on to the thought that someone punched you

(5) You did not do anything wrong by blaming someone else

(6) You did not do anything wrong

(7) right?

(8) So you did not get punched

(9) right?

.......................................................................

This twisted, sick logic

.......................................................................

Is twisted and sick

.......................................................................

But it has social advantages

.......................................................................

Criminals that punch one another are (1) supposed to be blamed (2) And victims are also supposed to be blamed 

.......................................................................

(1) Because you wear a skirt that is too high all the time (2) that pussy that is sticking out (3) around men who have testosterone (4) sticking out of their thumbs

.......................................................................

Basically both people are to blame

.......................................................................

I applies to either side

.......................................................................

Both the judge is responsible for the safety of the pedestrian and for the safety of the accuser and for the safety of the criminal

.......................................................................

Because we are a 'social' tree structure

.......................................................................

Because we identify with being 'civilized'

.......................................................................

We can apply civility to all cases

.......................................................................

Man and Woman are both responsible

.......................................................................

The pizza driver the runs over a cat or dog on the street, both the cat and dog and the pizza driver are all responsible

.......................................................................

'is there too much leisure time for cats?'

'are pizza drivers doing too many routes during times when cats would be out on their leisure time?'

.......................................................................

'did that person get punched in the face because they had revenge for someone having sex with their wife?'

'did that person get punched in the face because they were trespassing on private property?'

'did that person get punched in the face because it was fun?'

.......................................................................

These cases are specific

.......................................................................

'should we allow people to punch each other in the face?'

.......................................................................

.......................................................................

.......................................................................

.......................................................................

Maybe what the person who had his wife cheat on them should do is (1) call the police (2) not punch the guy in the face (3) and instead shake the man's hand really hard (4) or maybe give them a slap on the back and say 'good job' (5) or maybe hire someone else to punch the guy in the face (6) or maybe society should have an official public officer that punches people in the face

.......................................................................

.......................................................................

.......................................................................

It's all a big deal.

.......................................................................

Public arenas where people can fight might be a good idea

.......................................................................

(1) You can fight the guy that ruined your life

.......................................................................

.......................................................................

Because it's in public and open no one could get hurt in private or closed spaces, right?

.......................................................................

.......................................................................

Hiring someone else to punch the person is also possibly acceptable

.......................................................................


### [Written @ 10:10 - 10:12]

(1) Taking the Victim's Route

(2) Taking the Perpetrator's Route

(3) Taking the Accuser's Route

(4) Taking the Observer's Route

(5) Taking the Judgement Route

.......................................................................

(1) There are a number of different routes you can take when having a conversation

(2) "I did it. It was my fault." says the victim

.......................................................................

Everyone is the victim.

.......................................................................

"I did it. It was my fault."

.......................................................................

The victim in a conversation takes responsibility for having perpetrated the crime.

.......................................................................


### [Written @ 10:09 - 10:10]

Did you already know this information?

I am sorry that I am repeating it again for you here.


### [Written @ 10:08 - 10:09]

Once you solve the battle of confusion, you start to realize (1) it is better to be confused all the time.

It is better to be confused than not confused.

(1) Confusion helps you prepare for more confusion.

(2) If you are a legal police officer, you are already prepared to face difficult situations. It is difficult to be confused.

(3) Prepare to be confused in a 24 hour conversation by confusing yourself repeatedly.


### [Written @ 10:01 - 10:08]

(1) There are a lot of ways to have conversations.

(2) 1 minute conversations, 2 minute conversations, 10 minute conversations and even 2 hour conversations.

(3) 10 hour conversations exist as well.

(4) There is even a medium for people to introduce 24 hour conversations

.......................................................................

.......................................................................

Let's start with 24 hour conversations.

.......................................................................

(1) If you had to sit with someone and speak to them for 24 hours.

(2) What would the result of that conversation be?

.......................................................................

24 hour conversations (1) have more words (2) take more time to explicitly declare what the conversation was about (3) because it takes so much time to speak then all the specific words and wordings and phrases and pauses between the words while each person is speaking is quite elite.

.......................................................................

Elitism in speaking comes from the heart.

(1) Hold your right pinky in the air so high that you have to say 'I speak formally' 'I speak formally' 'I speak formally'

And anyone who respects your elitism is possibly going to find you a pleasure to speak with.

(2) Hold your right pinky in the air so high that the person notices that you are an elite speaker.

.......................................................................

(1) 24 hour conversations are standard.

(2) Stand with someone for 24 hours.

(3) Or sit with someone for 24 hours.

.......................................................................

Write down notes or ask the other person for a notebook if you feel like there are too many bullet points to go over.

.......................................................................

If there are too many bullet points to go over.

.......................................................................

(1) Ask the conversationlist to shorten the conversation

(2) Can we pause for 10 minutes?

(3) Can we pause for 20 minutes?

(4) I am having a headache, can we pause for 50 minutes?

(5) I can't understand what you're saying at all, can we pause for 20 minutes?

.......................................................................

It is good to be confused.

.......................................................................














