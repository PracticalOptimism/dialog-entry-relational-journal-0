

# Education Journal Entry - October 24, 2020

[Written @ 4:06]

Notes @ Newly Learned Family Name

The Habsburg Family
[1 @ 0:00]


[1]
3245【03 overseas海外版】The Habsburg People in Mysteryハプスブルグ家の謎の人々＋火星人と談話するダビンチ、そして「奇妙な人たち」by Hiroshi Ha
By Hiroshi Hayashi
https://www.youtube.com/watch?v=HwEbnC_ey1g

[Written @ 4:04]

Notes @ Newly Learned Lesson on Octaves of Light
[Copy-pasted from the Day Journal Entry - October 24, 2020 @ 1:20]


"
Each of us is vibrating on a particular frequency . . Each of us is cultivating . . our own . . octave . . of light . . 

And what that means . . is that . . Just like you have on a piano . . where you've got . . an . . octave . . of notes . . And you've got all the notes . . in between . . When each person . . is . . in harmony . . with . . their true . . heart . . When each person . . is in alignment . . with . . their . . divinity . . 

They become . . an octave . . They become . . a pure . . harmonic . . energy . . 

So . . as there are only so many harmonies you can play on a piano . . in the universe . . there is . . infinite . . harmony . .

So imagine . . the beautiful sound . . If we're thinking of it in terms of . . sound . .

Imagine the beautiful sound that is created by entire planets that are operating in a harmonic . . way . . [*1]

And it's not just sound . . but . . it's . . it's energy . . sound is just a way for us to understand . . harmony . . 

Harmony can exist . . energetically . . as well . . 

. . . 

. . . 

So . . when we think of . . a friend group . . who really gets along . . Who really supports . . each other . . who . . is . . really . . in harmony . . with one another . . We can feel . . the beautiful . . vibrations . . that are coming off of that . . social endeavor . . 

We can feel the beautiful energy . . that is radiating out . . of each person . . as they . . feel . . supported . . 

And when we get an entire planet . . to feel that energy . . To . . support . one . another . 

And to vibrate on . . the highest possible reality . . That . . they . . can . . operate . on . 

Then . . we . . create . . a . . world . . that . . is . . not . . only . . fun . . to . . live . . in . . 

But . . we create . . a world . . that is optimal . 
"
-- Unicole Unicron
[1 @ 11:45 - 13:26]


[1]
Cam Church - Ultimate Reality and Personal Reality
By UNICULT
https://www.youtube.com/watch?v=bOyCwQqZWeY


[Written @ 1:23]

Notes @ Newly Learned Lesson On Octaves of Light



[Written @ 1:14]

Notes @ Newly Learned Camping Advice


[YouTube Comment from [1 by Jenna Rae]]

Jenna Rae
Jenna Rae
1 month ago (edited)

I recently went camping here in CA and noticed that sadly, the land is getting absolutely trashed. PLEASE take care of Mother Nature so generations to come can have the same beautiful experiences we have the luxury of having. 
- if you want to have a fire, be responsible! Dig a hole and build up dirt on the side to smother it with later. Add and layer large rocks around the outer ring to create a fire ring, this will protect wind from blowing embers everywhere. 
-before you leave, cover the fire with dirt and pat it with the back of your hands to make sure it’s cool 
-if you have to pee, choose a spot FAR away from another camp spot, nobody wants to set up camp on your pee puddle
-if you use toilet paper, simply throwing it in the woods is not chill and honestly it’s just lazy....it only takes a couple extra seconds to store it in a bag to pack out and dispose of later, or burn it in your fire 
-if you have to poo, also dig a deep hole in the woods (not near anyone else’s spot or future spot) and cover it when you’re finished
-don’t leave trash....please

Thank you and have a good time ❤️



[1]
A Weekend in my Life in Hawaii ~ we went camping ~
By Hitomi Mochizuki
https://www.youtube.com/watch?v=UsRavIE5NlM



