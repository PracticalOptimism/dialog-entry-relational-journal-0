

# Education Journal Entry - October 8, 2020

[Written @ 13:48]


## Notes While Watching [1]



Cloud Firestore data modelling

build more successful web and mobile apps

. . truly serverless app development environment . . 

. . made the right decision . . queries . . 

. . right decisions now . . 

documents? collections? queries? denormalization? security? limitations? nosql? pricing? performance?

sql database . tables . author . book . review . 

merge bits and pieces of the tables together using the language called sql . performance can be variable . can be fast or slow . how much data you have to go through . how your data is structured . 

schemaless in the nosql world . add data as needed . birthday field . 

code defensively . you can never really be guaranteed as to what kind of data you'll receive from the database . [fail gracefully]

queries are simpler . no joins . foreign keys . 

denormalizing data . bad and scary thing . you should really only have your data in one location . 

in nosql . denormalized data is expected . change in multiple places . 

how often is your data being read ? versus being written ? 

scale horizontally . data will automatically grow to [span] across these other machines . add more computers

sql databases scale vertically . more computational intensive machines ["bigger" and "beefier" machines] are required . [not just the number of machines available] . more and more improved supercomputers are required . 

nosql databases . big key-value stores . big json objects like the realtime database . documents and collections .


cloud firestore is about documents and collections . a document is like a dictionary or hash . key-value pairs . fields . strings . numbers . small binary of objects . json-looking things called maps .

documents are stored in collections . documents cannot directly contain other documents . but they can and often do point to subcollections which contain other documents which then point to other subcollections and so on and so forth . 







[1]
Cloud Firestore Data Modeling (Google I/O'19)
By Firebase [By Todd Kerpelman]
https://www.youtube.com/watch?v=lW7DWV2jST0



Notes @ Newly Created Words

subcollectino
[Written @ October 8, 2020 @ 14:06]
[I accidentally typed the word "subcollectino" instead of the word "subcollection" . . I'm sorry . . I'm not sure . . what this word . . "subcollectino" . . should mean at this time . . ]

