

# Education Journal Entry - October 25, 2020

[Written @ 21:46]

Notes @ Newly Learned Mindfulness Tools
Notes @ Quotes

"Have you ever been kind to yourself ?

Do you ever say mean things to yourself out loud or just in your head ?

In this tool we'll practice treating ourselves like we would a best friend with kindness and compliments .

Now, sit up tall in your chair .

Take a big deep breath in .

Spread your arms out .

Now as you exhale, say "I am wonderful" . [Give yourself a hug]

Breath in [Spread your arms out again], Breath out . [Give yourself a hug]

"I am kind" .

Breath in [Spread your arms out again], Breath out . [Give yourself a hug]

"I am loved" .

Breath in [Spread your arms out again], Breath out . [Give yourself a hug]

"I love myself" . 

"
[1 @ 0:08 - 0:53]

[1]
Mindfulness Tool: Hug Yourself
By mindfulnesstoolstv
https://www.youtube.com/watch?v=x-9J0Lzq6Iw



[Written @ 19:47]


Notes @ Newly Learned Mindfulness Tools

"Have you ever been kind to yourself ?

Do you ever say mean things to yourself out loud or just in your head ?

In this tool we'll practice treating ourselves like we would a best friend with kindness and compliments .

Now, sit up tall in your chair .

Take a big deep breath in .

Spread your arms out .

Now as you exhale, say "I am wonderful" .

Breath in, Breath out .

"I am kind" .

Breath in, Breath out .

"I am kind" .

Breath in, Breath out .

"I am loved" .

Breath in, Breath out .

"I love myself" . 

[1 @ 0:08 - 0:53]


##### Hug Yourself

Things to say:

- I am wonderful
- I am kind
- I am loved
- I love myself



[1]
Mindfulness Tool: Hug Yourself
By mindfulnesstoolstv
https://www.youtube.com/watch?v=x-9J0Lzq6Iw



[2]
Shira Notes- How to Hug Yourself
By Shira Notes
https://www.youtube.com/watch?v=e-ODod7lizQ

