

# Latest Thoughts

Last Updated On July 26, 2021 @ 8:35

Things to work on:

- Notes Provided For "Development Journal Entry - July 20, 2021" [Notes URL](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal/2021%20A.D./Month%207%20-%20July/July%2020%2C%202021)

Things that I've done recently:

- Talked about Inventions and Ideas
- Talked about Social Stigma
- Ecoin Episode #632






# Update History Log:

| Action | Date Action Taken | Time Action Taken | Description |
| -- | -- | -- | -- |
| Document Updated | July 26, 2021 | 8:35 - 8:51 | [[1.0]](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas/Random%20Thoughts%20and%20Ideas/2021%20A.D./Month%207%20-%20July/July%2024%2C%202021) `Social Stigma Notes`, [[2.0]](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas) `Inventions and Ideas Notes`, [[3.0]](https://www.youtube.com/watch?v=ZKCQ8vsp0OM) `Ecoin Episode #632` |
| Document Created | July 26, 2021 | ~8:35 | Document Created |









