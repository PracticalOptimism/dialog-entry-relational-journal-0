

# Development Journal Entry - June 14, 2021

[Written @ 20:21]

(1) databaseInsertItemByNameId
(2) databaseInsertItemByGraphicalInteractiveItemId
(3) databaseInsertItemByInterestingIdea


Thank you very much for reading this list

. . 




[Written @ 20:00]

- createAuthenticatorKey
- createPortableMessage
- getAuthenticatorKeyList
- updateAuthenticatorKey
- deleteAuthenticatorKey

Yes, okay now . . this list of functions . . 

Implementing them is strange . . 

(1) There are better places to write descriptor functions
(2) create item in database is a nice descriptor function for 'createAuthenticatorKey' . . and yet . . however . . we're kind of at our limit and don't want to write a 'create item in database in some different file' for some strange reason . . 
(3) This is a distinct function placement to destroy energy about 'not repeating oneself'
(4) Repeating oneself is very useful in this case

. . 

createAuthenticatorKey

- we need to save the authenticator key in some place of interest
- a place of interest needs to have a place id
- a place of interest is (1) file system (2) interesting database location (3) create a qr code (4) in local memory
- The 4 places we've listed are only the very beginning of an alphabetical list of places where we could continue the list and say 'wow, yea, that's a new possible place to store the information that we would want to store'

. . 

-_- well the list of functions we have is great -_- but it's kind of boring not being able to say we don't have the 'preparatory' database or interesting database functions that we would need . . and there for . . -_- . . 

. . 

It's possibly very important to start 

(1) databaseInsertItemByNameId
(2) databaseInsertItemByGraphicalInteractiveItemId
(3) databaseInsertItemByInterestingIdea

. . 

These are basic ideas

. . 

For some reason . . in previous live streams . . we've talked about (1) create (2) update (3) delete (4) get (5) recommend (6) scale (7) performAction . . these were called so-called "idempotent" ideas . . or "finite and collaboratively all-inclusive" . . and so for example . . you could (A) Include combinations of these ideas . . and say something like "well linear combinations of these ideas should span the whole set of ideas that are like 'yea, we have cool ideas'" . . 

But unfortunately . . I've been doing a lot of mathematics lately in my free time . . (and hence the long delays between now and some of my previous live streams . . I'm sorry . . long bouts of ideas are variably interesting over some period of time) . . time . . time . . time . . time . . time . . 

. . 

time . . 

. . 

I think it's like . . "Uniqueness" is an interesting concept . . and so . . for example . . having an alphabetical list of ideas like the list of function names that we've developed of a series of episodes . . in the previous Ecoin live stream video series episodes for example . . that is really quite intense to have come to a list of ideas that we had concluded in some way that a "finite list" could generate a "infinite list" of "interesting ideas"

Lately . . I've been thinking . . to myself . . I suppose . . the idea is that . . a finite list can create an infinite list . . but not all the ideas are necessarily "interesting" . . interesting can be a very complicated idea . . it is like it is an interesting idea . . and so it's not necessarily 'Tangentialized' by variably other interesting ideas like 'drawing line summation algorithms that sprint to some finish line to triangulate or trouble-yourself-into an idea or something like that'

. . 

'drawing line summation algorithms that sprint to some finish line to triangulate or trouble-yourself-into an idea or something like that'

. . 

So for example . . in previous ecoin-live-streams . . we had developed lists of 'idempotent' things or 'idempotent' data structures or 'data structures that . . collected finite lists of things of interest' 'things of interest' 'included' 'function names' 'uh. . . . ' 'and ' 'uh . . .' some other things . . like 'collections of ideas' that I thought were interesting from my own personal perspective . . and so it was like "wow, I wonder if these would be interesting to say that they form a 'spanning set' for certain interesting idea . . " but unfortunately . . after now some many days . . I have learned more about some interesting ideas . . 

. . 

Those ideas mostly have to do with "uniqueness"

. . 

So some things could possibly not uh . . -_- -_- -_- . . -_- it's boring to explain I think I'll let Seth explain.

Seth, do you have anything to say to the viewer?

. . 






[Written @ 19:57]


```javascript
createAuthenticatorKey
createPortableMessage
getAuthenticatorKeyList
updateAuthenticatorKey
deleteAuthenticatorKey
```

Here is a list of ideas . . these functions are interesting . . you don't need to establish any interesting ideas about them and yet they seem to be "required" or 'quote-on-quote' "required" . . quote-on-quote "required"

One reason these functions seem to be required is for the sake of "authenticity"

"Having these function ascribed in separate locations besides the place in the Ecoin 2.0 database data structure where those items are placed now is kind of interesting and weird and possibly different . . 

I'm sorry to say . . 

"


```javascript
async function createAuthenticatorKey (individualId: string, individualName: string, individualInformationTable: any, isCryptographicAuthenticatorKey: boolean, nameHere: boolean, providerServiceId: string, keepTrackOfThisKey: boolean, authenticatorKeyPermissionRuleTable: any): Promise<PortableCryptographyAuthenticationKey> {
  const authenticatorKey = new PortableCryptographyAuthenticationKey()

  return Promise.resolve(authenticatorKey)
}

async function createPortableMessage (messageContent: any, authenticatorKey: PortableCryptographyAuthenticationKey): Promise<PortableMessage> {
  return Promise.resolve(new PortableMessage())
}

async function getAuthenticatorKeyList (placeOfInterestId: string): Promise<PortableCryptographyAuthenticationKey[]> {
  //
}

async function updateAuthenticatorKey (serviceResourceOriginalLocationUrl: string): Promise<PortableCryptographyAuthenticationKey> {
  //
}

async function deleteAuthenticatorKey (serviceResourceOriginalLocationUrl: string) {
  //
}
```




[Written @ 17:10]

Copy-Pasted from the Ecoin 2.0 Codebase

async function getAuthenticatorKeyFromFileSystem (generalProtocolAddress: string, globalVariableTableId: string, synchronousOrAsynchronousFunctionEvaluation?: string, qrCodeResultPlayBackCollectionFunction?: any, qrCodeStopConditionFunction?: any): PortableCryptographyAuthenticationKey {
 // Function Implementation
}

  // Use QR Code File System

  if (!authenticatorKeyObjectString && isQRCodeFileSelectionAllowed) {
    // Developer Note:
    // I like my privacy.
    // I want to not let other people see me.
    // My camera would be visible while testing the
    // functionality of this part of the program.
    // Please forgive me for not live streaming
    // this area of the codebase.
    // Also I am a noob at programming this area
    // of the codebase
    // 
    // In general my understanding is along these lines:
    //
    // (1) Open the camera
    // (2) The camera is like a file system and so you want
    // to identify items using algorithms that can return
    // text string information just like a file system
    // QR Codes are cool algorithms for identifying particular
    // types of camera files such as squares that are black
    // and have special and unique patterns to represent
    // the files for that particular square.
    // (3) Another type of camera file system could be
    // an artificial intelligence algorithm that returns
    // the text "cat" or "dog" or "animal" or "person"
    // for each of the items that the artificial intelligence
    // system identifies in the camera view.
    // (4) More complicated and advanced file systems
    // are available such as if you were to imagine
    // that a pile of lava from a volcano nearby in your
    // local physical neighborhood has elements that could
    // be interpretable as file elements.
    // (5) If your mind wanders into other fields of interest
    // such as hallucinogenic areas and drug-induced meditative
    // states then you could also possibly represent each
    // finding as files even if the numbers or values of
    // the strings appear random like a '.png' image that
    // needs to be interpretted with other specific algorithms
    // or other specific metric evaluation techniques.
    // Specifically, keys of consciousness are possibly in the
    // hands of aliens and so you need to possibly communicate
    // directly with aliens that know which values to use
    // when interpretting certain facts or facades or things
    // that you observe.
    // (6) Teachers are important for that reason.
    // (7) Teachers interpret things and let you know
    // (8) Something about knowledge of the ancients
    // (9) 
  }


