

# Development Journal Entry - September 30, 2021

### [Written @ 12:43]

.......................................................................

Data Table Statistics

Bar Graph Statistics

Line Graph Statistics

Pie Graph Statistics

.......................................................................

Basic Statistics

Boolean Statistics

Inquiry Statistics

Line Graph Statistics

Pie Graph Statistics

.......................................................................

`Account Statistics`

.......................................................................

(1) Account Visited History (visited, transacted with) [`Data Table`]

.......................................................................

(1) Number of Accounts Sent A Transaction To [`Bar Graph`; number per day]

(2) Number of Accounts Received A Transaction From [`Bar Graph`; number per day]

(3) 

.......................................................................

(1) Total Ecoin Balance [`Bar Graph`; number per day]

(2) Total Recurring Transaction Income [`Bar Graph`; number per day]

(3) Total Recurring Transaction Exports [`Bar Graph`; number per day]

.......................................................................

.......................................................................

`Transaction Statistics`

.......................................................................

(1) Transaction History (sent, received) [`Data Table`]

.......................................................................

(1) Amount of Ecoin Sent [`Bar Graph`; number per day]

(2) Amount of Ecoin Received [`Bar Graph`; number per day]

.......................................................................

(1) Number of Recurring Transactions Sent [`Bar Graph`; number per day]

(2) Number of Recurring Transactions Received [`Bar Graph`; number per day]

.......................................................................

.......................................................................

`Your Shopping Statistics`

.......................................................................

(1) Products Purchased [`Data Table`]

(2) Products Received (vouchers) [`Data Table`]

........................................................................

(1) Number Of Products Purchased [`Bar Graph`; number per day]

(2) Number Of Products Received (vouchers) [`Bar Graph`; number per day]

.......................................................................

.......................................................................

`Your Work Statistics`

.......................................................................

(1) Jobs Applied To [`Data Table`]

(2) Jobs Received A Promotion For [`Data Table`]

.......................................................................

(1) Number Of Jobs Applied To [`Bar Graph`; number per day]

(2) Number Of Jobs Received A Promotion For [`Bar Graph`; number per day]

.......................................................................

.......................................................................

`Your Currency Exchange Statistics`

.......................................................................

(1) Trades Completed [`Data Table`]

(2) Trades Received A Promotion For [`Data Table`]

.......................................................................

(1) Number Of Trades Completed [`Bar Graph`; number per day]

(2) Number Of Trades Received A Promotion For [`Bar Graph`; number per day]

.......................................................................

.......................................................................

`Your Digital Currency Alternative Statistics`

.......................................................................

(1) Alternatives Visited [`Data Table`]

(2) Alternatives Received A Promotion For [`Data Table`]

.......................................................................

(1) Number Of Digital Currency Alternatives Visisted [`Bar Graph`; number per day]

(2) Number Of Digital Currency Alternatives Received A Promotion For [`Bar Graph`; number per day]

.......................................................................

.......................................................................

.......................................................................

.......................................................................

.......................................................................

.......................................................................

`Network Status Statistics`

.......................................................................

.......................................................................

.......................................................................

.......................................................................

`Digital Currency Statistics`

.......................................................................

(1) Number Of Ecoin Distributed [`Bar Graph`; number per day]

.......................................................................

.......................................................................

`Account Statistics`

.......................................................................

(1) Number Of Accounts Created [`Bar Graph`; number per day]

(2) Number Of Accounts Deleted [`Bar Graph`; number per day]

(3) Total Active Accounts [`Bar Graph`; number per day]

.......................................................................

(1) Recently Created Accounts [`Data Table`]

(2) Recently Deleted Accounts [`Data Table`]

.......................................................................

.......................................................................

`Transaction Statistics`

.......................................................................

(1) Number Of Transactions Created [`Bar Graph`; number per day]

(2) Number Of Recurring Transactions Created [`Bar Graph`; number per day]

.......................................................................

(1) Recently Created Transactions [`Data Table`]

.......................................................................

`Product Resource Statistics`

.......................................................................

(1) Number Of Product Resources Created [`Bar Graph`; number per day]

(2) Number Of Product Resources Deleted [`Bar Graph`; number per day]

(3) Total Active Product Resources [`Bar Graph`; number per day]

(4) Number Of Product Resources Promoted [`Bar Graph`; number per day]

(5) Number Of Product Resources Purchased [`Bar Graph`; number per day]

(6) Number Of Product Resources Sent By Voucher [`Bar Graph`; number per day]

(7) Number Of Product Resources Refunded [`Bar Graph`; number per day]

(8) Number Of Product Resource Transactions Liked By Customers [`Bar Graph`; number per day]

(9) Number Of Product Resource Transactions Liked By Resource Providers [`Bar Graph`; number per day]

(10) Like-To-Dislike Ratio Of Product Resources By General Audience [`Line Graph`; number per day (0% to 100% like)]

(11) Like-To-Dislike Ratio Of Product Resources By Individual Transactions [`Line Graph`; number per day (0% to 100% like)]

.......................................................................

(1) Recently Created Product Resources [`Data Table`]

(2) Recently Purchased Product Resources [`Data Table`]

(3) Recently Sent Product Resources By Voucher [`Data Table`]

(4) Recently Refunded Product Resources [`Data Table`]

.......................................................................

(1) Most Popular Resource Providers [`Pie Chart`]

(2) Most Popular Products [`Pie Chart`]

.......................................................................

.......................................................................

`Job Employment Statistics`

.......................................................................

(1) Number Of Job Applications Created [`Bar Graph`; number per day]

(2) Number Of Job Employment Positions Created [`Bar Graph`; number per day]

(3) Number Of Job Employment Positions Deleted [`Bar Graph`; number per day]

(4) Total Active Job Employment Positions [`Bar Graph`; number per day]

(5) Number Of Job Employment Positions Promoted [`Bar Graph`; number per day]

(6) Number Of Accepted Job Applications [`Bar Graph`]

(7) Number Of Rejected Job Applications [`Bar Graph`]

(8) Number Of Job Applications Liked By Applicants [`Bar Graph`]

(9) Number Of Job Applications Liked By Hiring Company [`Bar Graph`]

(10) Like-To-Dislike Ratio Of Job Employment Positions By General Audience [`Line Graph`; percent per day (0% to 100% like)]

(11) Like To Dislike Ration Of Job Employment Positions By Applicants [`Line Graph`; percent per day (0% to 100% like)]

........................................................................

(1) Recently Created Job Employment Positions [`Data Table`]

(2) Recently Deleted Job Employment Positions [`Data Table`]

(3) Recently Created Job Application Transactions [`Data Table`]

(4) Recently Accepted Job Applicants [`Data Table`]

(5) Recently Rejected Job Applicants [`Data Table`]

........................................................................

........................................................................

`Currency Exchange Statistics`

........................................................................

(1) Number of Trades Created [`Bar Graph`; number per day]

(2) Number Of Deleted Trades [`Bar Graph`; number per day]

(3) Total Active Trades Offered [`Bar Graph`; number per day]

(4) Number of Trades Completed [`Bar Graph`; number per day]

(5) Number Of Trades Promoted [`Bar Graph`; number per day]

(6) Like-To-Dislike Ratio Of Trade By Initiator [`Line Graph`; percent per day (0% to 100% like)] [keep a running count][of likes for each day][and calculate the like to dislike ratio][for N number of days]

(7) Like-To-Dislike Ration Of Trade By Pursuer [`Line Graph`; percent per day (0% to 100% like)]

........................................................................

(1) Recently Created Trade [`Data Table`]

(2) Recently Deleted Trade [`Data Table`]

(3) Recently Completed Trade [`Data Table`]

........................................................................

........................................................................

`Digital Currency Alternative Statistics`

........................................................................

(1) Number Of Digital Currency Alternatives Visited [`Bar Graph`; number per day]

(2) Number Of Digital Currency Alternatives Promoted [`Bar Graph`; number per day]

........................................................................

(1) Recently Visited Digital Currency Alternatives [`Data Table`]

........................................................................













