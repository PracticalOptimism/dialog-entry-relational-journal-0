
# Development Journal Entry - September 15, 2021


### [Written @ 16:13]







### [Written @ 16:07 - 16:12]

service-for-creative-free-sprawl

service-for-game-community-description

service-for-game-community-description-part-2

service-for-game-community-description-part-66-trillion-over

service-for-game-community-description-part-90-million

service-for-game-evaluation-loop

service-for-game-evaluation-loop-syntax

service-for-game-evaluation-loop-syntax-part-2

service-for-game-idea

service-for-game-idea-again-and-again

service-for-game-idea-part-2-through-8

service-for-game-idea-part-9-through-infinity

service-for-game-idea-thank-you

service-for-game-introduction

service-for-game-manifold

service-for-game-program

service-for-game-territory-information

service-for-interesting-information

service-for-introducing-nothing

service-for-introductory-text-sequence

service-for-simple-algebra

service-for-typescript-type-declarations


### [Written @ 15:29]


[Base-Of-What-We-Are-Starting-With]
[Vuejs-Web-Application-Performance-Base]

Default Vue.js Application With TypeScript + Vuetify

Vue.js 2.x (npm run build) (`production build`) (http-server ./dist)

57 - Performance
100 - Accessibility
93 - Best Practices
87 - SEO

.......................................................................

Vue.js 2.x , Vuetify 2.x

.......................................................................

Web Page Test (WebPageTest.org)
F - Security score
F - First Byte Time
A - Keep-alive Enabled
F - Compress Transfer
A - Compress Images
D - Cache static content
✓ - Effective use of CDN
Web page performance test result for
4761-96-233-194-205.ngrok.io
From: Virginia USA - EC2 - Chrome - Cable
9/15/2021, 3:47:24 PM

.......................................................................

Performance Results (Median Run - SpeedIndex)

.......................................................................

[Metric-Name] - [First-View-(Run-1)]

.......................................................................

First Byte - 2.058s

Start Render - 6.900s

First Contentful Paint - 6.886s

Speed Index - 6.936s

-- Web Vitals --

Largest Contentful Paint - 6.913s

Cumulative Layout Shift - 0.001

Total Blocking Time - ≥ 0.000s

-- Document Complete --

Time - 7.748s

Requests - 14

Bytes In - 1,021 KB

-- Fully Loaded --

Time - 7.914s

Requests - 15

Bytes In - 1,023 KB

Cost - $$$--													

.......................................................................





### [Written @ 14:48]


.......................................................................

Default Vue.js Application With TypeScript (No Vuetify Installed)

Vue.js 3.x (npm run dev) (`development server`)

28 - Performance
91 - Accessibility
80 - Best Practices
86 - SEO

.......................................................................

Default Vue.js Application With TypeScript (No Vuetify Installed)

Vue.js 3.x (npm run build) (`production build`) (http-server ./dist)

94 - Performance
91 - Accessibility
93 - Best Practices
86 - SEO

.......................................................................




### [Written @ 14:30]

```bash

npm install -g @vue/cli

vue create project-name # Select Manual or 'large-project'

# cd project-name

# npm run serve

npm install tslint-config-standard -D # https://github.com/blakeembrey/tslint-config-standard

vue add vuetify # Select Recommended


```


### [Written @ 14:10 -]

Default Vue.js Application (No Vuetify Installed)

Vue.js 3.x (npm run dev) (`development server`)

29 - Performance
86 - Accessibility
80 - Best Practices
79 - SEO

.......................................................................

Default Vue.js Application (No Vuetify Installed)

Vue.js 3.x (npm run build) (`production build`) (http-server ./dist)

97 - Performance
86 - Accessibility
93 - Best Practices
86 - SEO

.......................................................................

.......................................................................

Default Vue.js Application With TypeScript (No Vuetify Installed)

Vue.js 3.x (npm run dev) (`development server`)

28 - Performance
91 - Accessibility
80 - Best Practices
86 - SEO

.......................................................................

Default Vue.js Application With TypeScript (No Vuetify Installed)

Vue.js 3.x (npm run build) (`production build`) (http-server ./dist)

94 - Performance
91 - Accessibility
93 - Best Practices
86 - SEO

.......................................................................

.......................................................................

Default Vue.js Application With TypeScript (No Vuetify Installed)

Vue.js 2.x (npm run serve) (`development server`)

29 - Performance
91 - Accessibility
80 - Best Practices
86 - SEO

.......................................................................

Default Vue.js Application With TypeScript (No Vuetify Installed)

Vue.js 2.x (npm run build) (`production build`) (http-server ./dist)

94 - Performance
91 - Accessibility
93 - Best Practices
86 - SEO

.......................................................................

.......................................................................

Default Vue.js Application With TypeScript + Vuetify

Vue.js 2.x (npm run serve) (`development server`)

9 - Performance
100 - Accessibility
80 - Best Practices
87 - SEO

.......................................................................

Default Vue.js Application With TypeScript + Vuetify

Vue.js 2.x (npm run build) (`production build`) (http-server ./dist)

57 - Performance
100 - Accessibility
93 - Best Practices
87 - SEO

.......................................................................








### [Written @ 13:48 - 14:03]

```bash

npm install -g @vue/cli

vue create project-name # Select Vue 3.x // Vue 3 Preview

# cd project-name

# npm run serve // localhost:8080

vue add typescript

vue add tslint

vue add vuetify # Select v3 Alpha

npm install vuex@next --save

npm install vue-router@4 --save


```

(1) Lighthouse (Progressive Web App Support Test)

(2) webpagetest.org (Test The Web Page)





