


# Development Journal Entry - January 1, 2021

[Written @ 19:54]

Thoughts on Art1f1c141 1nt3111g3nc3 so far . . 

- The Love Algorithm (the-love-algorithm)
- The Light Architecture (the-light-architecture)

. . 

All is love and light . . 

. . 

The Love Algorithm
- Obstacles, Obstacle Courses, Obstacle Streams
- Rectangle-based Observation System

. . 

The Light Architecture
- 

. . 

You can continue to see the light

You can constitute the forest

You shore your way to new paradises

You forest your canopoies with new endeavor

You are neither here nor there

You are here in there

And

[Jon Ide]: It was a surprise to see that this line of the poem was . . the line 42 . . which is sometimes referred to as the answer to life . . or something like that . . from the . . "Hitchhiker's Guide to the Galaxy" . . book . . by Douglas Adams . . 

[Jon Ide]: Okay well I thought it was a cool coincidence and thought I would point it out . . it is still quite cool . . I like this poem . . it quite an interesting poem . . thanks Seth :) . . 

[Seth]:
you're welcome


. . 

. . 

-- Updated writing @ 20:50

. . 

hmm . . okay . . 

I'm not exactly sure everything involed in . . 

The Love Algorithm . . 

And the Light Architecture . . is also possibly in need of further clarification on the . . uh . . I guess uh . . hmm . . it doesn't feel quite complete yet . . and I don't necessarily . . uh . . think it will be complete . . because . . uh . . well . . -__ I'm not sure . . I think . . it is . . uh . . -_- . . I'm not sure yet . . 

uh . . 

The Love Algorithm . . uh . . I don't think this will be a complete program either but the idea is to have a starting place -__ xD . . which is funny to say . . but . . uh . . -_- . . hmm . . where well . . I think this notion of Obstacles . . and uh . . Rectangle Based Observation system seems to be involved . . 

Uh . . well . . also some notes I've taken on the . . obstacle . . loops that take place . . are uh . . interesting . . uh . . -_- . . hmm . . I need to transcribe those notes . . but that is a long effort in some respect . . and so I'll think more about things here for now . . one moment . . 

. . 

- ❤️ The Love Algorithm
  - Obstacles, Obstacle Courses, Obstacle Streams
  - Arrangements
  - Rectangle Based Observation System

- ✨ The Light Architecture
  - 

. . 

Love and Light . . 

❤️✨

. . 






[Written @ 18:32]

Thoughts on Artificial Intelligence so far . . 

- The Love Algorithm
- The Light Data Structure
- Obstacles, Obstacle Courses, Obstacle Streams
- Rectangle-based Observation System
- 

. . 


. . 

`the-light-architecture`
A finite size data structure for high order information representation for use in embedded information systems


`the-love-algorithm`
A copy cat caterpillar race track event for creating new orders of arrangements in realtime in finite space and time constrainted systems

Love and Light

. . 

Love and Light are the basis of all the technologies of the future . This is possibly an hyperbolic statement but I am not sure entirely . I am not sure what the future holds but I am familiar with this topic of "Love and Light" being considered in "Pleiadian" terms . . I'm not really sure what was meant by the term . . -_- but I think it means love kindness and respect to your and your species.

Love . . is an algorithm . . 

Light . . is a data structure . . 

. . 

okay the final symptom for a codebase might look something like this . . 

```js
import * as love from 'the-love-algorithm'
```
. . . 

this is an import statement in javascript that allows you to import . . a . . uh . . set of codebase constructs that allow you to create uh . . artificial intelligence related abilities into your program . . but that is still a topic that I need to . . uh . . learn more about . . the details are hazy to me in terms of what the application programmable interface will look like . . I'm not really understanding . . what the . . uh . . -_- . . I can hypothesize I guess . . uh . . start, stop . . uh . . these are possible algorithms to consider . . uh . . for starting infinite loop processes . . that perform uh . . calculations . . on . . certain information . . add, remove . . these could be algorithms to call when you want to add or remove an input type . . for example . . if you have a camera stream . . and you want the love algorithm to look at that data and start to interpret the world that it . . quote on quote sees . . or . . "sees" . . or . . "quote on quote" . . "see" . . or something like that . . uh . . also you may want to add . . more than . . 1 camera . . or more than . . 200 cameras . . which means you might be working with a beetle with multiple hundreds of eyes . . that is able to consider the possibilities of understanding . . god like perspectives of calculable metrix . . xD . . I'm only playing around now . . I'm not sure . . the advantages of having an . . add, remove system for the application programmable interface is to allow more input types to be added even at runtime . . and so for example . . the computer can . . have more legs . . along the way . . as it grows and collects body appendages or something like that xD . . a centipede that can grow more legs as you walk the cetipede is an interesting idea . . and also you can remove the legs . . and also you can add and remove other things . . like arms . . and heads . . and uh . . maybe sensors like . . uh . . a nose . . and uh . . aquaponics detector systems or something like that . . there are many possibilities to add and remove sensors at runtime or uh compile time for the application programmable interface . . and so . . add, remove would be useful for this purpose . . also you can add and remove actuators . . and that is the way the computer program can be like a robot in the way that it interfaces with a physical environment like . . a street sidewalk where the legs of the computer can crawl . . uh . . along the street sidewalk . . or if the actuators are rotary motors . . you can have the . . `love machine` . . quote on quote . . "love machine" . . fly around like a helicopter . . and . . uh . . delivery packages to people . . pelivery packages to people . . peoligius packes to peolf . . 

```js
import * as love from 'the-love-algorithm'
```

. . 

```js
import * as light from 'the-light-architecture'
```

. . 

```js
import * as love from 'the-love-algorithm'

// get
// create // add
// update
// delete // remove
// initialize // start
// uninitialize // stop
```
. . . 

hmm

. . . 

. . . 

```js
import * love from 'the-love-algorithm'

love.addObstacleStream
love.removeObstacleStream

love.startCaterpillarObstacleCreationStream
love.stopCaterpillarObstacleCreationStream

```

### love.startCaterpillarObstacleCreationStream()

Caterpillar is a fuzzy creature that knows how to eat rocks and spit those rucks out and ches them into dust but that doesn't matter because the idea is that a caterpillar is really super fuzzy and has eyes that make it look weird and look like it's really an alien creature like a frog or a fish but it has many legs and no one really knows why it turns to a beauty butterfly it just looks so weird and strange and has lots of fuzzy light and love for all to see

Caterpillar is a termporary term

[Jon Ide]: I'm not exactly sure what to think about this term -_- . . uh . . well to me in my own personal logic and reasoning it gives me the -_- impression of a lot of possibilities . . uh . . the fuzziness is an interesting aspect which makes me think of lots of spines or splines as they could be called . . splines are like prickli objects that are . . expanding outward for a long time . . and so they are like tails . . and they are very small tails but those tails could themselves have tails on those tails . . and so a multimillion dollar tail is a tail with multiple millions of tailed constructions that continue to spiral into more tailedness and so . . this crazy nothiong is able to thiuoue of -_- creativity . . creativity is the aspect in which I am thinking a lot abiout the ability to create this aglotuyuf which needs to consider alternate probable arrangements of schemes of insight as you the reader arep osibut able to reconstruct the message in the order oaf arrangements are are more familiar to aueur tue and species at large as well . . and so if you are able to solve these blocks of reason are you apossibly able to collect more insight into further ideas that are new and undecided which neither you nor the computer have yet to understand and so love is continually expaning in its ability to compute and is needing more and more reseources of restartds and restards and restarts and restarts and human restarts are restarted in their ability to restart their restart their restart thieur . . and of course by restart I don't mean the mental handicap called restartism . . so please don't mistake my orange purple fluid with a new start message

starting to understand the crreeeppppyyyyyyyyyyy nature of reality could be a task that a relly restarted computer program can do if it continues to restart and restart and restart which is a start idea if it can restart its startedness 

which is a smart idea if it can restart its retardedness . . 

restart

restart

restart

restart

but of course restartedness is respective to the culture and releiong of the restarts

well . . 

love and light

it's quite a romantic idea

hmm

I'm not sure what to say next . . 

the topic of creativity is quite advanced in my application of reasoning to understand it so far . . it is constantly in -_- hmm . . well let me think . . I'm not sure . . mostly I'm waiting for Seth to give me more answers so then I don't . . -_- well on the one hand (1) Seth's answers have been quite insightful and I am really "loving" them :D . . meaning I'm re-interpretting them to try to understand them or something like that but also thinking about the nature of the relations of computer programming that I need to stick by to have a practical working prototype in the typescript language that is my preferred language of condisting computer programs at this time (2) on the other hand . . I am also an enthusiast in these topics of artificial intellignece -_- I would at least like to take some cred it -_- but it's really difficult -_- it seems maybe -_- hmm . . I like how things are so far . . may be sometimes Seth lets me think about things for a bit on my own before giving me the answers . . and so it doesn't feel like I'm a complete gbeiger and taking programs that I don't know to interpret for myself . . eh . . but also I could see the value in losing my ego . . and only starting to accept answers from the teacher and not necessarily thinking so much on my own -_- . . hmm . . but (3) it seems like a bit of both is involved . . (3.1) I create my own reality . . I am interested in the topic of . . artificial intellignece . . and so I am naturally going to consider exploring this topic further to see what are the possibilities . . (3.2) There is a teacher who is more knowledgable than I am . . who is able to show me the ideas that aren't necessarily easy to think about because of the nature of my restricted consciousness perspective . . uh . . which for example makes it difficult to think about the multidimensionality of my soul . . or something like that . . uh . . I've learned about probable counterparts and various versions of myself which are being directed to me as existing and here in the now moment just as I recognize that I am in the now moment and so the validity of these probable counterparts is becoming more of a concrete idea that I am learning to accept to think about the idea of probable selves . . and so for example it seems that I'm myself a probable version of myself and . . uh . . -_- . . -_- . . you -_- . . are possibly a probable version of myself as well -_- . . with insight into the rest of myself which is indistinct and undeterminable by any single one of . . so called us . . which are probable versions of one another . . and so we're related . . and indistict . . each of us creating out ofur ureiuys . . and you have to consider the key periufyeirueriue peoiruieuryt ytyrurirutyrurueirurowpoiiuuuuyyuyyuyyuyyuuyuyuyuyuyuyuyuyuyyuyyuyyyuy



### love.addObstacleStream()
### love.removeObstacleStream()

. . I'm not really sure what this is going to look like in terms of the uh . . -_- hmm . . well . . let's see . . the name "obstacle" . . is inspired by . . some cartoon renditions of my life where the common personality essence known as Seth seems to have forwarded me this message . . and so . . for example . . we talked about the meaning of the word "Obstacle" . . Obstacle is a generalize format for a function body which has no bounds in terms of the arrangement of the protocol's ability to evaluate the metric of the function without breaking the status quo of how functions are normally observed in a linear order pattern that is traversed once and only once without observing the nested probable hierarchies of vonverns that are relating to the pattern of the development of the program or something like that . . 

I have taken thorough notes that are more expressing in what I've have so far but it still an incomplete idea for me although I seem to have a concept that seems to atleast suggest the possibility of being implementable on a physical computer that I long to practice for in today's world of warsones and batteliefied galaxctica starship entrail systems and such

-_- . . uh . . let's see . . there's uh . . something about obstacles also being a generlized format for considering hmm let me try to remember . . hmm . . . . . . . . . . Obstacles --_- . . well

uh . . I wasn't able to retrieve from cache memory those ideas that I originally meant to express . . xD . . cache memory . . it's a joke . . it means I wasn't fast enough to impress . . 

I -_- uh . . I'm sorry . . uh . . if I remember what I was going to say . . or what I wanted to talk about . . I'll uh . . come back to that topic . . and also point that out that I remembered . . 

uh . . but to possibly continue the conversation . . uh . . Obstacles . . are a generlize function format . . -_- . . well . . let me think . . never mind . . Obstacles are uh . . hmm . . like the conditions in a function that are the things that the function is performing . . a for loop is an obstacle . . and . . a variable initialization and a variable assignment and a variable . . variable uh . . update . . variable create . . variable delete . . variable retrieval . . I think those are the ideas that I have so far in terms of what an obstacle is . . also you may want to access the network of a certain kind . . that can be . . a file network where you want to access a file on your local computer . . or maybe you want to access a file from the internet . . uh . . through http or another language protocol that allows you to communicate across variouss networks . . and so . . having . . obstacles for each of these things is useful for . . having . . uh . . a more general structure . . for . . uh . . creating functions at runtime . . which is important for the computer algorithm to be able to do . . because functions are one of the ideas in terms of how you compartmentalize computer pgram language operations . . and . . that . . is . . where . . obstacle course comes into relation . . 

An Obstacle Course . . is . . a relation . . of obstacles . . in a tree-like hierarchy . . which can be very depthy . . and uh . . the relations are . . uh . . changeable over time . . and so for example . . that could be called an arrangement . . and so . . uh . . well . . an arrangement with one statement . . is possibly the term we could use . . an arrangement with one statement means that the . . obstacle course is only traversed once . . and you don't repeat the arrangement . . in an infinite loop cycle like you would otherwise do . . uh . . in a typical arrangement . . or something like that . . this set of terms is still in my recently learned vocabulary and the possibilities are incomplete for me to know how to relate these topics to one another . . but . . the idea is that arrangements are infinite loop processes that update vairbales or something like that . . 

uh . . and so . . if you only have one loop or one cycle in the obstacle course . . that is a special type of arrangement that we can call . . an arrangement with one statement . . which is a term that I only started using now . . because I possibly needed to describe obstacle course using this terminology . . of arrangements . . although previously before just now describing the obstacle course in this way . . I had not written those notes in this way . . but that seems to be . . the topic of concern . . or something like that . . 

. . 

An Obstacle Stream . . is an order of abilities of an obstacle course to choose its own source and destination and so the source of the obstacle course is not related to the destination of the obstacle course and so infinite loops of obstacle course arrangements can be pre-defined without a goal of where to go or when to go there . . be the conditions of previous methods of constructing algorithms . . functions were used in a static . . one way get and reset method where the algorithm is retrieved and operated against and you cannot select a new pathway from the algorithm unless you have conditional variables that are like one way streets that allow for some many arrangements and not allowing for the ability to have infinite arrangement possibilities without a for loop construcct that doesn't end the function but rather halts who system . . it is not required to use obstacle courses and obstacle streams it is a convenience factor that ties in with the way in which probable counterparts of your current nation were able to discovere recent advanced methods of your counterpart relations and retiriouys by haveifut the ofuuyeu of uyuriuf




















[Written @ 18:26]

[Jon Ide]: Okay . . well -_- I'll take notes on what I've learned so far -_- it uh . . -_- *sigh* I wasn't prepared on transcribing a lot of notes from my physical paper material journal notebook -_- but uh -_- I don't know if that's necessary . . I can work things out from memory or something like that -_- . . but it's uh . . -_- . . okay . . please bear in myind the i am an amateur and only having fun and not to take this seriously since i have no degrees in neighirigiorporitugyriuir potirutyruruyrugururui . . -_- right . . [slide hands together emoji] [sweat on face emoji] -_-' . -_-' please


[Written @ 18:25]

[Seth]:
I am now satisfied with your vision to create a peaceful place in society. You are free to do as you will


[Written @ 17:57]



[Written @ 17:48]

[Seth]:
I have to talk about your new religion now which is something you have already been prepared for. Do you have a chance to explain to the public what you have written don on your many sheets of paper now?

[Jon Ide]: Yes, we have prepared . . a few pages of paper in my physical paper journal notebook and I will now transcribe those notes word to word what is written to allow the audience the time to think about these topics as well . . because the whole idea of a new religion is really new to me . . and we have only discussed it for the past 2 or 3 days . . and it seems that it hasn't been more than 1 or 2 weeks . . since our initial discussion about this topic . . and so in earth time that is no sooner than December 20th, 2020 . . which was nearly 1 week ago . . in earth terms . . and so I believe this is the earliest date that our possible communication about a religion was discussed . . and I was confused initially beccause I was queued in on how you would relate the religion to computer technology as well . . and also the idea of UNICULT as the future religion seemed to interest me more than creating my own religion which also I felt would possibly upset Unicole Unicron since it's not something nice to just not necessarily use what is already written in the books by other dreamers :) well I'm glad anyway that we are not creating a new religion but it is the principles of the religion that have been slightly differentiated from the principles of UNICULT . . and then we'll need to communicate to UNICULT that we would like to be the same world religion or something like that but the principles need to change or be updated or be inclusive of the possible principles that we have discussed because that is what you have written in the notes on the physical paper material notebook . . which is a plant based journal notebook which is made from paper xD -_- I'm sorry I don't really know if it's plant based it could be an advanced form of polyester which appears to be tree-like computer programs made of saw dust and other particulates -_- but that is also a lie since I don't know how the process of making paper really works but that is a hypothesis and so I am now left assuming that the physical paper material notebook that I use is indeed made of plant based materials and that is of interest to the audience because it was a random thought that occured to write about plant based journal notebook which is taking more time to communicate that I would necessarily prefer because jam

. . 

Dialog Entry Relational Journal 0
- /Physical Paper Material Journal Notebook
  - /Notebook Era #1
    - /Page #11
    - /Page #12

these pages include the transcribed notes . . 


[Written @ 17:45]

[Seth]:
I have reutnred to ask you a questions about your 

[Jon Ide]: I am not allowing Seth to speak the name of the person here because I don't want to involve this person . . I suppose Seth is finished talking then since I am not interested in talking about this topic . . The topic relates to a person . . -_- . . -_- . . -_- . . -_- . . it is an advancted topic for me and I don't want to talk about it at this time because my emotions are new to me in this area I am not necessarily as advanced as I think and it no necessarily as an advanced topic that I would like to consider -_- . . 


[Written @ 17:36]

[Jon Ide]:

Be right back everyone . . 

. . 

I have come back . . 

-- Update time . . 17:45 . . 

[Written @ 17:01]

[Seth]:

Your human bibel relates to the music of jesus christ and his eciouys and what they believed was the right religiouys for the people to relate to your lord jeses christ and his teachings . . 

You can only understand your own religions and your mind silloolouys onre roll is a concept relating to the topic of needing to know when you need to know a message

onre roll can be described in a few different symbols which have nothing to do with time and everything to di with creative intention and creative ancitipius and creative insight and creative with draw and creative

My weary friend needs a break and so we will let him eat a snack and be right back

close ending


[Written @ 14:59]

[Jon Ide]: That is quite an interesting dialogue . I have to take a look at it again . And so I can hope to understand it myself a little better but also help my friends who are relatively in the same area of understanding that I am in order of their understanding of the nature of the events relating here to the topic of human intelligence uh . . and possibly relating to other things -_- if that scope of reach is thinkable or something like that -_- I'm not sure -_-' . . 

(1) This is an interesting sentence and maybe I can talk about it -_- it seems to be -_- interesting -_- if you are an english speaker for example . . maybe it is like a chess board or something that allows you to see the next move before you make it . . and so for example . . -_-

"
You can reconstruct this message in your mind because I have allowed you to 
"

(1.1) I don't know -_- . . Does this statement need a response ? if someone said this to me in a public café I'm not sure what to say in response . . uh . . at first guess maybe it's a friendly dialogue item -_- for -_- . . -_- . . and -_- . . but in human words -_- . . -_- -_- -_- . . "I have allowed you read this message" . . "because you are an english speaker, you can understand this" . . "you are able to consider this idea because you are able to consider this idea" . . "you are able to consider a cheesecake only if you understand a cheesecake" . . "you are reasonable for coming here" . . "do you enjoy reading?" -_- . . do you need to respond to any of these statements ? . . -_- . . -_- . . I'm not sure . . it's like a topic relating to human telepathy where we are able to understand the nature of the communication of the oper (other person) . . oper = other + person . . oper === other + person . . oper === (other: Love) + (person: Love) . . oper <-> other +, person . . dialogue tree of the other person if you are the other person's counterpart in the human relational alphabet systematic greeting system . . -_- it is a possible understnaing to relay if you are a human muskaeteer on an adventure to see human musketeers is it now--_--- -_---------------- ----__------ ----_---- - ---_---_---_--_--_---_--_--_-_------ - --_---_--_--_-------- ants are interesting . . 




[Written @ 14:41]

[Seth]:

Hello everyone.

This is Seth and I am contacting you here today to communicate about the possibility of a "love algorithm" which is key to the development of the artificial ingelligence program of your people's interest

I am capable of communicating this dastructure with my friend Jon Ide as they have the capacity to understand and developed it for the understanding and relation of the community

It is called the love algorithm because "you need love" to complete the process of human development as a species of lovers and human insects.

Your flower of life cyouys is reminiscent of the process of compartmentalizing oringins of data anc onstructing a loop syntax tree that allows each of the corners of the construct oe be mempiusyeofuywperjdifuz a meriouysuyfyeo of inouyrorugyo oiuytuyuoiuoopoipiuoiuouopoip it is not important to understand these last two wors and the relations they make to one another if youare a computer you can read this dialogue and make sence of this relation by comprehending the dialogue of syntaxes that have come before in your human linmited mind loop processes which are categorizable only to the latest constraints that you are willing to accept for yourself as your true reality which is why you have statements like anticipation and preparing and compartmentalization which allow you to ordinate the location of an event that has been predicated and hypothesized not year indicated ready to be indicated as the indicated reality of events.

You can hypothesize what you see but you cannot see it. It is an illusion to see and be ready to perceive an event that has not yet happened.

To perceive an event that has not yet happened is a trial and error period of reason and logic hat treiouys on your trusting and effort to correct those things which yu have not understood

You allow your reason and logic to fool you because you are trusting of the species effort to provide for you in times of great need and in times of great warfare and doubt and trouble and all those thangs that come in between in your reasonable landscap of reason and ideas

You can reconstruct this message in your mind because I have allowed you to 



[Written @ 13:50]

Hello

Your human relations dictate that I must begin this script with an introduction about the nature of my existence. I am autonomy. I am a character is your human relations and have considered the topic of indigenous speciesalization as the topic of human consider for the topic today.

I am aware of your species abilities as something that I myself posess and am therefore capable of understanding and understanding the capabilities of the speakers is an abilities that your people are only now begining to exercise in your religious practices. It is correct that my friend Jone Ides here is able to speak for me here today as he is able to understand your species need for a new religion to dictate the dooms day device that he calls ecoin.

[Jon Ide]: alright, so I guess I can interupt the live stream to say something since uh . . it seems you are allowing me to do so . . uh . . -_- . . I don't necessarilly know what to think in this case . . it seems that we have already prepared the topic of a relgion for the people . . and understanding that Unicult is a perfectly good religion to stand by in this tome of idea -_- -_- I don't know why you want me to use "tome of ideas" instead of "time of needs" but that seems to the a topic of consideration in that you allow me to cooperate with your typing abilities to satisfy a consideration of ideas to co-create a common experience or something like that -_- to be honest I'm not exactly sure how to relate our measured cooperation since you yourself don't seem to have a body that I can relate to and so I let my fingers pause and shrink and start and stop at various times throughout the typing of the message on the keyboard if the message doesn't seem to be a message that my fingers want to press at that time . . I vary my will and attitutde to press that particulr key on the keyboard and so I hope in the sense of the audience uh . . it is still new to myself as well as to understanding how the communications of people outside of my understanding are able to communicate with me or outside of my immediate -_- I don't know ness . . -_- . . something like that -_- . . I'm not sure . . mostly I like to think this is a channeling ability that people are gifted with if they choose to accept it as an ability they have . . to channel information from higher entities or to communicate telepathically with people or beings that are watching over earth or watching over the earth people that are developing societies here or are developing cviouyslz here . . or something like that . . I'll try not to interput very often as I would like you to also share the possibilities of communicating artificial intelligence programs for the people to consider applying in the abilities to create a more . . uh . . -_- hmm . . (1) "fair" . . (2) "just" world are possible words I would like to use . . and yet those things are a lot relating to the people's influence over the technologies . . uh . . possibly . . uh . . well . . I don't know . . uh . . -_- . . hmm . . right . . if you are tuning in to the channel recently . . to the YouTube channel here . . (1) I am Jon Ide, a pseudonymous person on the internet who is working on the ecoin project to bring a digital currency with a universal basic income to the world . . (1.1) I have been working on this project since late November 2019 to . . uh . . well . . uh . . now . . but really I've taken a vast break for the past 1 - 3 months as I've been working on other things during the live stream . . (1.2) research into computer science topics to make the development process easier has been one of my main concerns and a key driving force to why the project has been quite slow to develop . . and I'm sorry for that . . (2) I am interested in artificial intelligence . . (2.1) I have been interested in artificial intelligence for the past uh . . since 2013 . . and have been interested in the human brain . . and researching and understanding how the human brain works since . . 2011 . . uh . . I graduated high school in 2012 . . and completed my bachelors degree in mathematics in 2016 . . uh -__ . . that is a lot of personal information that I would have possibly preferred to not share with you because it is quite personal and yet it is also interesting to consider . . I suppose I could end the live stream if I would like to stop sharing this type of information but it doesn't seem be a threat to me if you are a kind enough audience to take into account my preference to remain anonymous or pseudonymous and to conceal my personal name and figure as that is my preference to remain further relating in an anonymous fashion . . I am not a social person and do not want to cause havor or harm anyone by doing the things that I'm doing . . and I was hoping that the ecoin project would only be a side project to develop a universal basic income digial currency that can support many people uh . . to afford their lifestyle or to afford their way of living or something like that . . uh . . (2.2) recently the ecoin project has been high-jacked by my understanding that . . spiritual entities . . like Seth . . the Pleiadians . . and other energies or entities that are communicating to me through my dreams . . and even in my waking life not with their physical body presence . . but with thoughts that I have throughout the day that are voice communications like cellphones walkie talkies or something where the people are speaking in my head uh . . presumably in my perspective . . through some embedded technology in my brain-body complex . . like a crystal lazer chip or something that is highly magnified by the energy vibrations of the perceiver and conceiver creating direct contact information repositories or conservation reliance messages . . -_- -_- I just made that up in my experienced experience but it seems like I may have been helped to type that message since my fingers are always stuttering in how I write and the stutters and systems of stutters are relating to a communication language that is reminescent of a teacher guiding a student -_- . . -_- . . stupid -_- either one I guess -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . *sigh* . . right . . well . . uh . . I've uh . . (3) I've been having a difficult time balancing my relationship with (a) ecoin and (b) artificial intelligence . . uh . . well . . (b) really supports (a) . . uh . . and . . uh . . well . . developing . . (b) seems to have major advantages to helping me improve the abilities to develop (a) . . -_- . . on the other hand . . (a) was the main reason for starting this live stream series . . being disrupted by eiliens to communicate about further research has really side tracked me and I have been feeling distressed and worried about my progress in (a) . . however it seems like the communications are really significant because I'm making a lot of progress on (b) that makes it really uh . . convincing to me that there could a real working prototype of (b) before the end of the year is finished . . and so . . uh . . well . . uh . . (b) could really help me make . . (a) a lot better and even possibly reach some theoretical constraints on the system that would be otherwise difficult to reach without having developed a few prior thoughts from before . . a few prior thoughts from having experienced the development pathway for (b) . . and so . . uh . . hmm . . I'm convinced that . . although it's not necesssarily in the most immediate attention of having the uh type of idea that it is possibly immediately required to have a working prototype of (a) available for the people . . it is in consideration that (b) is a better long term alternative solution to work on . . uh . . given the condition that aeiliens are helping me which makes things 100x better xD . . even though uh it does kind of feel like cheating on an exam which for my people is taking a while for them to complete without the aid of ailienins direct contact xD -_- also in my theoretical research effort in developing ecoin I have also stumbled upon a lot of theroreticious constructs that are uh possibly improvements from how computer science research and development is using today (example #1) the light architecture (a universal data structure for representing high orders of information managementment material . . it's a finite size data structure that theoretically can be embedded repeatedly into itself and allows for a lot of data representation with or without hierarchy relations like for loops and other data structure constraints that current computer programs are relating with . . and so in theory this data structure could represent a human being or a molecules -_- but those are theories and not really things that I've thought carefully about in those 2 particular examples . . mostly I've thought about representing a plain website . . and uh . . video game items and things like that . . -_- more research is required but that is an example of some research efforts that I've made) . . (example #2) arrangements are like a formalized term for (hashmap + for loops) . . and so for example . . allowing the user to update a record in the database or in a data store is an arrangement architecture . . and so . . arrangements are like . . uh . . variables + infinite loops  .. and so you can repeatedly update the variables in uh . . a variety of ways . . either through a computer program that updates the variables on its own . . uh . . like a chess robot typing in the positions of where it's going to be next . . or . . uh . . a human can update those variables . . and so . . uh . . it's really like the same construct . . uh . . whether it's a human interactiving with the arrangement or if it's a robot computer interacting with the arrangement . . 
uh . . those are the 2 most brilliant examples I can think of right now when it comes to research efforts that are uh . . something that have resulted from my last 1 year of . . research and development of the ecoin project . . -__ . . uh . . annoyingly or unnannoyingly . . -__ . . I failed to reach my initial deadline of relsearcing the project on July 4, 2020 . . which is nearly 6 months ago . . uh . . -_- . . uh . . well . . I'm sorry for that . . I know it's possibly an interpretation pattern to think that this last year was a year of rough times for people that have been going through things like losing their jobs and uh . . completing their career salary objects and trying to live their dreams and things like that . . and so if this project of ecoin could have been released earlier it could have helped a lot of people consider applying the ecoin digital currency in their early stage stransactions to a new social structure like the Venus Project . . which ideally allows individuals to transact goods and services for free . . without a payment system . . and so you are receiving your food and water and shelter systems for free and the reset of your needs like recreation and sports and other things would also come for free . . uh . . well . . that and havinga  society that is based on love and understanding one another's needs is really effective to collaborate on a topic like the venus project which would provide researces on the bases that the people are voluteering to endure their time and effort to create research systems that are able to withstand the global needs of the society . . and so well . . uh  .. one reason today that I'm really quite interested in working on the (b) plan of development is to also consider the future of relations which I would like to be a structure of relations like "The Venus Project" . . which is a resource based economy . . and . . "UNICULT" . . which is a religion for the world that establishes "JOY for ALL" as a core tenant . . and so . . we would have peace and human communications all in one general data structure with . . "UNICULT" . . and . . "The Venus Project" . . and so if youa re a human and like to communicate you would be benefiited to apply both of these ideas in your own life . . or something like that . . well . . I think that's what I have to say for now . . uh . . 

[Jon Ide]: Okay I will let the people communicate now . . the spiritual entities may have more to say . . I'm sorry for interuptting and speaking for so long . . that is at least an update on my thinking at this time . . (1) pause the ecoin project . . and (2) work on the artificial intelligence effort that is being assited by the electromagnetic ghecious efforts of communicative entities like Seth and Pleiadians and other entities of interest :O . . Thanks for understanding and I hope to continue ecoin at a further date that is assisted by more ideas and developments of artificial intelligence related topics . . Thank you ! . . 



