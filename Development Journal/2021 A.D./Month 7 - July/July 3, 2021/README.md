


# Development Journal Entry - July 3, 2021


[Written @ 18:58]




[Written @ 18:12]


[Written @ 17:56]

When we create WebRTC connections 

(1) We can look at (1) how many messages are being sent
                   (2) how frequently are those messages being sent
(2) How many errors are being communicated?

. . 

To turn off connections

(1) We can say (1) turn off high density message connections first
(2) We can say (1) turn off high error message density connections first

. . 

Those two should possibly help us (1) delete live streams that have a lot of failure rates

(2) 

. . 

Well all of that is really interesting

. . 

Our computers may be using a lot of random access memory resources and so we're like . . well . . how do we partition that for the programs?

. . 

(1) a list of which programs are important is very good
(2) 

. . 

(1) a computer program connection is a type of computer program
(2) 'this is really important'
(3) a normal user request is a type of computer program
(4) 'this is really important'
(5) one type of computer program that is a normal user request is something like (1) a program that is (1) a business application (2) something that needs to be evaluated for long periods of time (3) military grade security related things (4) 
(6) one type of computer program that is a normal user request that is possibly not so important is (1) hello world programs (2) tutorial programs (3) prototype programs (4) non production codebase programs (5) 

. . 

That's possibly not a terrible list . . (1) you don't want to stiffle innovation by saying that 'computer program A' is a 'copy-cat' of 'computer program B' and so 'stop computer program A' and so something like 'off-brand' or 'off-brand doppleganger' programs aren't being listed as 'possibly not so important'

. . 





[Written @ 17:45]

One of the difficult things with the list presented at [Written @ 17:32] or [17:32] is the topic of

(1) What does it mean
(2) To have
(3) something demanding
(4) a lot

. . 

That is quite a demanding

. .

Thing

. . 

But even when you try to determine "what is it that is so demanding?" it seems like possibly in trying to observe that calculation, you are possibly even going to make more and more demands to try to satisfy future calculation metrics that you could make.

(1) item.property1.property1A
(2) item.property2.property2A
(3) item.property3.property3A

. . 

There are now 3 properties right? or uh . . is that correct? 3 properties ? or are there in fact 6 properties? or 7 properties? 

. . 

item is one property // 1
item.property1 is another property // 2
item.property1.property1A is another property // 3

item.property2 is another property // 4
item.property2.property2A is another property // 5

item.property3 is another property // 6
item.property3.property3A is another property // 7

. . 

Are those correct?

Do you yourself . . yourself . . as someone who is possibly even more talented than me . . or more skilled than me . . or more mature than me . . or even less mature than me . . are you able to see more properties here . . that I have missed ? . . 

. . 

Maybe (1) the property of the time that is required to type each of these properties up . . right . . so that's like . . (1) how developer friendly is that property listing that you have . . does it take 2 minutes to spell out . . item.property1.property1A.property1AAlpha.property1AAlphaQue0.property1AAlphaQue0Que1.Property1AAlphaQue0Que1Que2.property1AAlphaQue0Que1Que2Que3 . . how long does that take to type out ? . . how long does that take to type out ? . . And does the developer have inconveniences like 'learning new type systems?' . . 'learning new number systems?' 'learning new organization patterns?' . . -_-

Well those are all thoughts to possibly consider . . that in exchange for properties like 1 push buttons that do a lot of stuff for you . . you're exchanging 1 push buttons for things like 'typing' and 'calculating' and 'evaluating' and 'manually performing for yourself or others or another other or another other other other other other'

and so on and so forth

. . 

How cool is that . . 

. . 

Do you see how it can be demanding for even typing a line of code . . copy-paste is so cool right? . . -_-

. . 




[Written @ 17:32]

One of our goals with our current computer prgram . . 

Is to try to create more 'computer' connections like WebRTC connections . . 

(1) create more connections
(2) make sure you continue to create more connections
(3) only stop if the computer is doing poorly

A poor computer is (1) if the computer can't keep up with its workload on other tasks while keeping all of those connections . . 

(1) Which connections do we stop?

(2) We could stop all of them at once O _ O

(3) We could stop 1 at a time

(4) We could stop a select few that seem to be demanding a lot from the connection listing

(5) We could wait for tasks to not be so interruptive

(6) 

[Written @ 17:31]

Amount of Random Access Memory Available out of Total
Amount of Hard Drive Space Available out of Total
Amount of Rejected Computer Connections out of Total
Amount of Accepted Computer Connections out of Total
Amount of Administrator Updates out of Total
Amount of Normal User Requests out of Total
Amount of Registered Error Messages out of Total

. . 


[Written @ 17:11]

A computer is able to increase the number of web addresses that it has . . 

. . 

But when do you stop increasing the number of web addresses?

. . 

(1) When the computer is doing poorly

. . 

A poor computer is sick and needs help. (1) the Random-Access Memory is being used too often. Thrashing is a term that is thrown around to possibly refer to how often the Random-Access Device does certain calculations -_- or maybe this refers to computer graphis -_- I don't know -_- I'm sorry for making you ignorant because of my ignorance -_-

A computer can be in a so-called 'poor' state because of (1) why is the computer going to not work well in the long term? (2) thrashing? using the computer too often? (3) why is the computer not going to work well in the short term? (4) attacks from other computer programs ?

. . 

Well the quality of the state of the computer can possibly be difficult to measure.

-_- but why?

. . 

Is the computer doing poorly if the following calculation is evaluated?

How much hard drive space is available? / How much hard drive space is allowed by this computer in total?

10 GB Free of 100 GB Total?

10 / 100

10 out of 100

?

Is that a good measurement of how poor the computer is doing.

Perhaps.

Perhaps.

It seems okay.

. . 

Amount of Random Access Memory Available out of Total
Amount of Hard Drive Space Available out of Total
Amount of Rejected Computer Connections out of Total
Amount of Accepted Computer Connections out of Total
Amount of Administrator Updates out of Total
Amount of Normal User Requests out of Total
Amount of Registered Error Messages out of Total


. . 

Benchmark Testing In Realtime like (1) measuring the amount of reads that you can do (2) measuring the amount of writes you can do . . in a given amount of time . . 

Uh that's kind of (1) Intense . . especially if you're doing that quite often . . 

. . 

Uh . . Benchmarking is really intense . . 

. . 

It's not a bad idea . . 

. . 

Uh forgive me but maybe I will try to avoid benchmarking at runtime since it seems to be something that could cause (1) more memory overhead when other computer programs need that memory.

. . 

However, Benchmarking at Runtime has benefits like (1) checking how 'poorly' the computer is doing very quickly compared to a few statistical hours ago or a few statistical seconds ago or a few statistical milliseconds ago depending on how interesting your computer is . . if you have super computers in the future maybe those types of millisecond statistics on calculating large numbers to benchmark how well the large numbers can be calculated in a few pico seconds or smaller amounts then that would be amazing . . 

Then you could say that the computer is doing `poor` if those numbers increase above a certain `arbitrary threshold` like `2 or 3 pico seconds above what you had been doing 2 or 3 hours ago` . . 

. . 

And of course if you're a future-tarian . . someone who likes to eat the future . . your arbitrary threshold can be something like . . 

-_- create your own glass ceiling right? One step at a time ? -_-

. . 

And so if you want to measure how poor you're doing . . maybe your default state is (1) poor until you reach your goal

. . 

and so then even the computer at start time is doing poor and so then it's like . . make sure your fan is being run very quickly and you're consuming a lot of electricity like certain computer algorithms . . and if you're not doing that . . then you're poor . . 

. . 




[Written @ 15:42]

I think that's all interesting and all but it's also kind of weird

. . 

Forgetting is kind of a weird concept . . I think in my notes 'forgetting' is like 'if you're a human' that will not focus on the item and 'recollect' 'it' from the past then it is easy for you to forget . . But for certain 'million dollar' 'computers' like ones that entrepreneurs would make if they wanted infinite memory to remember all sorts of things -_- which is like -_- so weird -_- why did we reincarnate to the planet when we could do that in other lives -_- you're so lame go away -_- . . but otherwise if you want to be a dumb entrepreneur that makes dumb devices that makes humans dumb then go ahead and be dumb

. . 

-_- I'm sorry -_-

. . 

[Written @ 15:38]

Numbers : 1, 2, 3, 4, . . . Etc . . Infinity

For-Loops : Until-Here, Until-Now, Until-Okay, . . . Etc . . Until-Ever

Doodle-Boxes : Forget-Now, Forget-Here, Forget-Okay . . . Etc . . 

Random-Doodle-Boxes : Bizzle-Blop, Bizzle, Bizzle, Bleep, Blop

Random-Godzilla-Doodle-Boxes : Etc

Random-X-Y-Z-Machine : Etc

. . 



[Written @ 15:30]

There is a thought that came to mind

while working on (1) this computer program

function getComputerIdentity (additiveAmount?: number) {

  let resultNumber = ''

  if (additiveAmount <= 0) {
    additiveAmount = 1
  } else if (additiveAmount > 0) {
    //
  } else {
    additiveAmount = 1
  }

  for (let i = 0; i < additiveAmount; i++) {
    let randomNumber = (Math.random()).toString(16)
    randomNumber = randomNumber.slice('0.'.length, randomNumber.length)
    resultNumber = `${resultNumber}${randomNumber}`
  }

  return resultNumber
}

. . 

That thought has to do with (1) for loops

For loops are interesting structures.

They are quite cool

For loops are kind of like a parallel reality version of a 'length' value.

If You think (1) "what does the length" of something mean?

Then you could for example, start to consider the "for loop" as a way to evaluate things that need you to think about a "length" or a length or a -'len-netgh-

. . 

Length could possibly be a 'random' value in some ways.

. . 

(1) Think about (1) how you think about your house
(2) How do you think about your car
(3) How do you think about the person you know around you

. . 

It is easy to say (1) they haven't changed
(2) they are not the same length as they were yesterday
(3) they are completely different

And of course you used a 'for loop to evaluate your claims right?'

'No?'

Well if you measure your house you may come to the same measurements as you did yesterday.

If you measure your car you may come to the same measurements you did yesterday.

Yesterday looks like a measurement that is like 'today'? right?

So if 'yesterday' and 'today' look the same because of how you've measured your 'now-moment-point-idea' right?

Then you're like 'yea, tomorrow' will also have the same ideas.

Tomorrow is like a 'random' machine today right?

Today it's like 'what will tomorrow' look like.

It's completely random right?

'What will tomorrow look like'

It's like -_- well possibly I'll be okay

The measurements won't go too far from expected right?

Or maybe

. . . . 

hehsoepfieps

woeifpoiweiof

owpeoifpoeipf

wepoifpoweopwe

fwieufpoiwepe

fiouweurugiue

werfhwej

erkje

werwk

werk

werk

werpw

wepfi

wekjfl

???

???

???

???

???

How many random placeholders can you have?

Some random numbers are more random than others

You could possibly have scales of random orders

like (1) one type of random order

(2) another type of random order

(3) another type of random order

and those are so randomly separated from one another

It's not only numbers that you can loop over

You can loop over random orders of new ideas

New random orders of random random random random right?


[Written @ 14:20]

Hello, I am Jon Ide.

I am working on Ecoin.

I would like to (1) have a webRTC connection between computers (2) have a WebSocket connection between computers (3) have an X-Ary connection between computers (X-Ary stands for 'arbitrary' type connection like 'HTTP' or maybe another protocol that comes a long one day)

. . 

There is a computer (1) 

The computer for a WebRTC Connection needs to (1) Get a Signalling Message From The Recipient Computer

The computer for a WebSocket Connection needs to (2) Get a WebSocket Web Address From The Recipient Computer

The computer for a HTTP Connection needs to (3) Get an HTTP Web Address From THe Recipient Computer

. . 




