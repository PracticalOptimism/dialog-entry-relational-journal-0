

# Development Journal Entry - March 9, 2021

[Written @ 16:18]

[Jon Ide]:

I haven't thought about it . . but I think if you look at the word "animal" it is something like . . you can use it for a "general" term to mean "animal of biology" or even something like "animal of mathematics biological creatures" which are like "theoretical creatures that exist in a soft [sort] of mathematical biological space where creatures are like algorithms or uh moving apparatii like edges and graphs and crayons and other ink-like landscapes like tastebud sensations and hypercoordinated space that spam more neusances than what is initially seen by the prescribed viewer piewer" so it's like "are you a neussance crature that creatures neussances addages like mew "

Mew

Mew

Mew

Mew

[Written @ 16:09]

[Person 1]: Are you an animal?

[Person 2]: I haven't thought about it

[Person 1, but it could be someone else]: Are you an animal?

[Person 2]: I haven't

[Person 2]: I can't answer that

[Person 1]: Are you an animal[no-question-mmmmmarrrrk]
[Person 1]: are you an [introduction??]

[Person 2]: No, I cannot

And the n-ser to the parable is the clause statement "I am an animal"

"It is my instinct to respond"

"No, I am instinctive"

"Animal"

No, instinctive

No




[Written @ 15:28]

Ayamenama
Ayamanema
Ayemenema
Ayemanene [Ayemanema]
Ayemanema [Ayemaneme, Ayemanem, Ayemaneme, Ayeman[pause-2-seconds][pause-x-seconds]anema, Ayemaneneme [Ayem[pause-9-seconds]aeymene], Ayen[n-is-pronounced-m]amena, [pause][pause][pause][pause][pause], [pause][pause][pause][pause][special-pause][special-pause][special-pause][aya], [aya-is-like-you're-dead because your language was not able to support your life span, like a marriage that undermines each person's integrity to their uhsband [wife, husband, unity word] because of ["of" pronounced "each"] them cheats the other with a knife in the back]]

Ayemanaema [Ayemanema, Ayemanene, Ayemanema] - each of these are pronounced the same

Ayemanema is how you pronounce the word, and the mispellings are ordinary and regular in "anema" "aenema" "aenema" "ae-eee-nema" "ae-[x-wise-repeat-of-e]-nema" "ae-eeeeeeeeeee-continued-nema"

Anemenema [Is Anemanema][Animal but different][Animal]

Anemenama [Animal-Like][Animal-Life][Pronounced "Animal-Like" but use the word "Animal-Life" Because it is more characteristic of "life-like creature"]

Anemanana [Animal-with-no-belief-system][I am animal with no belief system][It's supposed to be written like "I am an animal with no beef system" or "I am animal with no being system" or "I am an animal with no "animal-in-my-name" system" and so it's like "I am" but you have to say "I am an animal blah blah blah" with no "animal" because that is meant to be ignored by your language "pause" structure like "I can't believe I read that" but "pausing" lets you say it was an "animal" like you can say it was a hippopotomus that no one pays attention to because it's obviously not related to your religion of hippopotomus-ignoring-science-artificts[artifacts, but not nascent, nascent ascently]]

Ayemeneyema [Ayemenama, Ayemenem[e-but-ignore-it]ma[but-ignore-it], Ayemenemema but ignore the word because it's meant to be "ignored" but also like a "pause" so it's like "you have to believe it's like a "pause" or an "ignore" so it's like "ignorance paused for the sake of a story that was like if you kno" it's like that, you have to ignore the answer so it's l[ike] but without the [ike] so it's l but "l" is not answered with "l[ike]" so it's l y]

Ayemenemana [Answer my name but "my" is like "ignorant" of itself and so it's "like" you have to [invisible-pause][invisible pause is like you don't notice the pause][saying a ][and][answering][anderson][]]

It's like a time machine without the "p" in the "machine" so it's like "pachine" but not "answered by the original clouse" and so it's like having an answer before you see if it was in time to be asked or in time to be answered

Andering, is like a clouse satment about "ablivalence" which is like "ambient" "amblience" "ambience" "ambience" "ambience" "ambience" "ambience" "ambience" "ambience" "ambience" "ambience"

Ambience, anderson, anderson, anderson, anderson

Anderson

Anderson

Anderson

And that is a language introduction for today.








[Written @ 15:00]

Aya - I don't know
I - I
I - I am
I - I wish to become [to be]

I - pronounced "eye", \ ˈī \
Aya - pronounced "eye-uh" \ ˈī-yə \
  - "ah-ye" \ ˈī-yə \
  - "eye-ye" \ ˈī-yə \
  - "ay-ye" // Aye, is interesting, is has its own meaning


Aya is the name of a god "Aya"
"Aya" is the god of knowledge

Aya 

Aya

Aya

Aya

Aya

"I don't know"

"I don't know"

"I don't know"

"I don't know"

"I don't know"

[Written @ 14:40]

ayamana
anamenamana
anamenamana
anamenanana [anamenamana]
anamenamana
anamenamana

I am also working on a communication programming language for how people communicate with one another . . 

Anemanana [anamenamana] is a word that means "welcome" in 3 different ways [we, weys, ways]

Anemanana

You can think of it as a generalization of english to be more coheri [pronounced "cohesive"] [coherent, cohesive, cooperative, coordinative, considering, concerning, arroma [arrowa]]

Writing the word "coheri" is like you want to also associate the word "cohesive" but you have to use a different spelling and then you say something like "take the pronoun "I" and make that co-relative to "I" in another "Me" body type or in another "I am context variable" and so for example "I am" is like "I am my brother" "I am my son" "I am my neighbor" "I am my accent" "I am my ancestor [accentized ancestor, accentized accenster [pronounced "accent"], it's like a cooperative spelling effort, like "me and myself are typing this together and so it's like, what would we type?"]

So it's like, I've been learning this language with myself as the person communicating with these languages, which have been fairly foreign to me, but I am more and more starting to understand their usecase

[Seth]:
I am teaching you this language from a probable reality where your ancestors are yourself but they are from probable alternative workplaces if that is what you want to call it.

I am teaching this language to you my friend for the hopes that it is helpful for you to see you are a probable version of yourself. You are a probable ancestor of yourself. You are a probable relative of yourself. You are a probable communicator of yourself. You are a probable neighbor of yourself. You are a probable anticedent of yourself but you do not realize this [that] because your language structures is not letting you think in multitudinal hypercoordinated angles of beliefs like "self appropriated access corners of beliefs" which is like "are you okay?"

"Are you okay?"

"Are you okay?"

A basketball

A basketball

A basketball

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?

Are you okay?



[Written @ 14:37]

programs that we're considering working on:

nature
now
oicp (open internet communication protocol)
program-manager
password-manager
uniplexer
nanatube

. . 

ecoin





