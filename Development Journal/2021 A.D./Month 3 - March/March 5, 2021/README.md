

# Development Journal Entry - March 5, 2021

[Written @ 1:45]

The first-aya group of the bit is possibly an update to make to this program . . algorithm . . to say something like . . "well, we don't necessarily know if the first aya-wise group is available . . so let's say . . we want variable aya groups to look like they know . . what the spam for the rest of the groups look like . . " . . and so the algorithm because . . 

 . . 

 aya . . 



[Written @ 0:12 - 1:27]

bit-flipper
bit-flixer
bit-aya
bit-wow
bit-saya

. . 

bit-aya

. . 

bit-aya

bit-aya

bit-aya

bit-aya

There is a computer program for spanning a large set of availability metrics like (1) how many computers can you use before this program is broken (2) how many bits can you access ? 

(1) you can have a lot of computers
(2) you can have a lot of bits

aya is "I don't know" so it's like . . well . . you don't need to know how many "computers" you are using or how many "bits" you are using . . but hopefully the computer acts like 1 mechanical bit-operating computer with rapid availability according to the variable computer edge cases or something like that . . 

000
001
010
011
100
101
110
111
. . . 
aya (infinity)

Bit-A, Bit-B, Bit-C, Bit-D, Bit-E, etc.

0,     0,     0
0,     0,     1
0,     1,     0
0,     1,     1
1,     0,     0
1,     0,     1
1,     1,     0
1,     1,     1
. . . 
aya (infinity)

. . 

[A][B][C][D][E]etc.
[0][0][0]
[0][0][1]
[0][1][0]
[0][1][1]
[1][0][0]
[1][0][1]
[1][1][0]
[1][1][1]
. . .
aya (infinity)

[A][B][C][D][E][F][G][H]etc.
[0][0][0][0][0]
[0][0][0][0][1]
[0][0][0][1][0]
[0][0][0][1][1]
[0][0][1][0][0]
[0]etc.
. . . 
aya (infinity)

Computer data structures are like this . . in an instantiated form which is like "okay, follow this data structure format" which is like "bit representation of data structure"

If you look like your computer is very reliable, you don't need to know certain things like, "have, variably available computer registration units like memory addresses spaces for duplicating that bit across several computer mechanics areas" . . 

A bit-aya, doesn't assume reliability like, I know for sure that this number at this index space is 0

You look like you need to tell me which invisible spaces are allowed to be representative of your identity

Do that in space, and you will have a bit-aya which is very fast to know that "ahh, right, in case that is not available then I know these are saying the same thing over here . . and so I need to duplicate my data variably effectively for any usecase like 'I want to make more copies of this bit', 'My current copies are like . . being overwhelmed with onlookers and so I would prefer a load balancer type system that says you want to duplicate on a certain type of onlooking usecase like too much internet traffic . . so maybe like copying your homework to allow multiple professors to hold on to the page of work . . in case any arbitrary condition like 'I don't want you looking at the work while I'm looking at it' is also available'

You look like you have to look at the data and so it's like . . 'take a look at this . . and . . this looks like you don't need to know whether or not the way you look is available for you . . '

Let's see if a program like this will work . . 

(1) You need to carry a space for each bit
(2) You need each bit to be pair-wise adjacent to itself in aya

If you want to have very fast access to aya, you need to spacialize it to memory like . . 

(3) You need each first-pair-bit to say, I know how to talk about those yahoos in aya-space

. . 

Well, you don't need the first-pair-bit to say that, but its going to be like . . if you want, you can have others say that but it's uh . . not idempotent . . so it's like . . ehh . . it's up to you . . also uh . . eh . . it's up to you if any of the other n-pair-bits are like . . yea . . I know where those yahoos are too . . 

It's something about . . variable accessibility . . according to aya-constraints . . like . . wow, that is a very large number

. . 

If you look like a yahhoooo . . then you don't need to tell anyone that because they'll be like . . yea . . you can't look like that to my friends who want to know where you are . . you have to tell me that you're a normal aya . . 

like tell me your name and then I'll say you're not that big a deal . . but it's a big deal to say that . . and it means your space requirements are going to be . . aya-wise . . and so it's like . . wow . . you're uh . . well . . your computer should not look like aya . . unless it says aya implicitly . . so it's like . . bit-aya is not going to be useful . . for a computer like . . "hi, lets talk about bit-wise-space adjacency" . . it's more like a convenience metric to say that it's easy to fast-reload all the areas of interest . . without thinking about time-and-space requirements like memorizing where those things of interest are . . 

. . 

well . . 

(1) You need to carry a space for each bit
(2) You need each bit to be pair-wise adjacent to itself in aya

. . 

Bit-Space-A: {Bit-A},{Bit-A},{Bit-A},etc. // immediately available
Bit-Space-B: {Bit-B},{Bit-B},{Bit-B},etc. // immediately available
Bit-Space-C: {Bit-C},{Bit-C},{Bit-C},etc. // immediately available
Bit-Space-D: {Bit-D},{Bit-D},{Bit-D},etc. // immediately available
. . . Grow by A, B, C . . etc . . to X, Y, Z
Bit-Space-X: {Bit-X},{Bit-X},{Bit-X},etc. // immediately available
Bit-Space-Y: {Bit-Y},{Bit-Y},{Bit-Y},etc. // immediately available
Bit-Space-Z: {Bit-Z},{Bit-Z},{Bit-Z},etc. // immediately available
Bit-Space-AA: {Bit-AA},{Bit-AA},{Bit-AA},etc. // immediately available
Bit-Space-BB: {Bit-BB},{Bit-BB},{Bit-BB},etc. // immediately available
. . . Grow by . . A, AA, AAA . . etc . . to A-aya-wise
Bit-Space-A-Wise-1: {Bit-A},{Bit-A},{Bit-A},etc. // variably available
Bit-Space-B-Wise-1: {Bit-B},{Bit-B},{Bit-B},etc. // variably available
Bit-Space-C-Wise-1: {Bit-C},{Bit-C},{Bit-C},etc. // variably available
Bit-Space-D-Wise-1: {Bit-D},{Bit-D},{Bit-D},etc. // variably available
. . . Grow by . . A, AA, AAA . . etc . . to A-aya-wise
Bit-Space-A-Wise-2: {Bit-A},{Bit-A},{Bit-A},etc. // variably available
Bit-Space-B-Wise-2: {Bit-B},{Bit-B},{Bit-B},etc. // variably available
Bit-Space-C-Wise-2: {Bit-C},{Bit-C},{Bit-C},etc. // variably available
Bit-Space-D-Wise-2: {Bit-D},{Bit-D},{Bit-D},etc. // variably available

. . 

Grow by Bit-Space-Ala-Wise-Aya . . where Ala is Ala Ala Ala Ala, Allaa . . and Aya is aya . . 



A (1), B (2), C (3), D (4), E (5), F (6), G (7), H (8), I (9), J (10), K (11), L (12), M (13), N (14), O (15), P (16), Q (17), R (18), S (19), T (20), U (21), V (22), W (23), X (24), Y (25), Z (26)

AA (1 + 26), BB (2 + 26), CC (3 + 26), DD (4 + 26), EE (5 + 26) etc . . 

AAA (1 + 26 + 26), BBB (2 + 26 + 26), CCC (3 + 26 + 26), etc . . 

AAAA (1 + 26 + 26 + 26), BBBB (2 + 26 + 26 + 26), etc . . 

. . 


A, B, C, D, etc. . is a type of number system . . in the way this . . uh . . algebra expression of bits and bit spaces . . has been expressed and so for example . . A, B, C . . X, Y, Z . . but then you need to let yourself imagine other uh . . algebraic groupings like . . if you continued the alphabet . . what would that look like ? . . well . . uh . . AA . . BB . . CC . . DD . . etc . . XX . . YY . . ZZ . . is an example . . and so if you continue like this . . you can get an algebra . . like . . 1, 2, 3, . . etc . . and so . . AAA + AA + A . . is something like . . uh . . -_- . . Maybe . . (1 + 26 + 26) + (1 + 26) + 1 = 53 + 27 + 1 = 81 . . 

. . 

(3) You need each first-pair-bit to say, I know how to talk about those yahoos in aya-space

. . 


Well . . now it's like . . you don't know . . uh . . it looks like . . you have to say something like . . bit-aya . . again in this area of interest . . and so for example . . just repeat this process for each bit . . with variable availability . . 

So for example . .

well . . you want to say something like . . "I can't look like that . . so please don't let me ask about aya . . " . . "I can't say aya . . " . . "I need time trepidation events to spam aya . . " . . 

-_- . . well . . if you look like you need time trepidation events like . . please spam this list of ideas . . tell me more of that idea . . then . . in my opinion I think you can say something like . . alright . . well . . -_- . . you can ask me about space events like "yo, does that look easy to tell the audience in 0 time space efficencies?" well . . then that's the best way to spam aya in 0 time space efficiencies . . and so rule #3 here in this program is an optional rule that says you don't want your computer program to be slow . . like "yea, that's taking too long to access those variables . . -_-" 

-_- . . well . . this program in general really isn't fun to work with so it's like -_-' . . 

well it's supposed to be a generalization of bit sequence operations . . and so you can let yourself look at variably available bits . . and so it's like . . -_- hmm . . if this isn't available . . I want someone to tell me that so It's like . . we can make it available . . -_- immediately . . right ? . . so then it's like -_- . . okay . . let's say that's also part of the program . . and so all the bits are available for your program with "variable reliability equal to almost always likely . . because you're like not using a computer that looks like you rely on 1 type of subsystem to work . . but all variable amounts of subsystems that are available at variable runtime instantiations are also able to say that that subsample is available . . without having to stop the system . . at any aya . . or something like that -_- . . but if you're not an Aya Trump ZZZZZZZZZillionaire . . it's like yo . . please don't spam . . "

. . 

Aya, aya, aya, aya

Aya, Ya, aya, aya, aya , aya, aya, ayaya, ayaya, ayaya, ayaya, aya, aya, aya, aya, aya, aya, aya, aya, aya, aya, aya, aya, aya, aya, aya

aya, aya, aya, aya, aya, aya, aya



