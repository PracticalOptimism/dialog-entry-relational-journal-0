

# Development Journal Entry - March 17, 2021


[Written @ 23:12]

Okay . . well . . with OICP (Open Internet Communication Protocol) there is a problem . . names are like 'yea, give me a name' but it's like -_- hmm . . I don't know . . what to call you since all the names are like '-_-' . . right/!?!?!?! . . 

well the initial names we were looking at were . . 

database
network-protocol
event-listener
permission-rule
computer-program-manager
scale-matrix-amana
anana-anana-anana

. . 

but it's like -_- if you do this recursively for all the folders and files of the so-called traditional file system approach for writing software . . it's not necessarily going to look very well on your resumé because people might be like -_- what the hell?!? . . 

- /database
  - /database
    - /database
      - /database
      - /network-protocol
      - /event-listener
      - /permission-rule
      - /computer-program-manager
      - /scale-matrix-amana
      - /anana-anana-anana
    - /network-protocol
      - /database
      - /network-protocol
      - /event-listener
      - /permission-rule
      - /computer-program-manager
      - /scale-matrix-amana
      - /anana-anana-anana
        - /database
        - /network-protocol
        - /event-listener
        - /permission-rule
        - /computer-program-manager
        - /scale-matrix-amana
          - /database
          - /network-protocol
          - /event-listener
          - /permission-rule
          - /computer-program-manager
          - /scale-matrix-amana
          - /anana-anana-anana
        - /anana-anana-anana
    - /event-listener
      - /database
      - /network-protocol
      - /event-listener
      - /permission-rule
      - /computer-program-manager
      - /scale-matrix-amana
      - /anana-anana-anana
    - /permission-rule
      - /database
      - /network-protocol
      - /event-listener
      - /permission-rule
      - /computer-program-manager
      - /scale-matrix-amana
      - /anana-anana-anana
    - /computer-program-manager
    - /scale-matrix-amana
    - /anana-anana-anana
  - /network-protocol
    - /database
    - /network-protocol
    - /event-listener
    - /permission-rule
    - /computer-program-manager
    - /scale-matrix-amana
    - /anana-anana-anana
  - /event-listener
    - /database
    - /network-protocol
    - /event-listener
    - /permission-rule
    - /computer-program-manager
    - /scale-matrix-amana
    - /anana-anana-anana
  - /permission-rule
  - /computer-program-manager
  - /scale-matrix-amana
  - /anana-anana-anana
- /network-protocol
- /event-listener
- /permission-rule
- /computer-program-manager
- /scale-matrix-amana
- /anana-anana-anana

(1) how do you know when to make a file . . ? database.js ? . . database.txt ? . . since files are just databases . . why not right ? . . you place files text encoded information in a text file and you are using a database . . with variable effectiveness since it might not be formatted in a way that your algorithm can parse the file to say 'yea, that is a fast algebra syntax for me to start recording and updating all the things in the file . . so yea . . thanks for that . . '

(2) how do you have a list of things like . . database-file-1.js, database-file-2.js, database-file-3.js if always it's not really a list . . but a single data structure ? . . -_- . . the list is like -_- no . . don't have a list -_- . . no names . . no names . . everything is either (1) a database (2) a network protocol (3) an event listener (4) a permission rule (5) a computer program manager (6) a scale matrix amana [which is like -_- tell me how the heck to make this really effective :) like load balancers and other computer clock methods that clock things on and off depending on access criteria . . so it's not like you want software the spams your computer and makes your computer slow if that's one of your access criteria . . so runtime effectiveness on preventing computer degredation abilities is possibly a scale parameter one wants to account for in their compiled software distribution format . . and so if your software runs on a mobile phone maybe it's supposed to not assume there is this or that hardware unit that is otherwise fast and effective on other computer environments or something like that -_-] (7) an anana-anana-anana [which is like a surprising computer software environment like 'yo, is that a super intelligent artificial intelligence program?' or 'yo, is that an accidental hardware feature that was just newly discovered after tripwiring our program on itself at runtime?' so something like a multiduplicator clock system is something like you discover new computer architectures at runtime and you want to trip over those on purpose to expand the load on the computer system]

(3) this data structures really looks like -_- . . "database/database/database" -_- . . you can possibly write a manual on this and what it means . . but it's like -_- hmm . . is this a good program? do I need to read many levels deep in this if I want to expand the effectiveness of this computer program? "database/database/database/database/database/database/database/database/database/database" are supposed to have their own meaning by the law of the manual that would be written for this coding structure . . but it's like -_- hmm . . 

. . 

Names are interesting . . let's take a look at some names . . 

Name Number 1
Name Number 2
Name Number 3

. . 

Name

it's like a time dial. it's like a time clock. You don't really know how this name is pronounced without asking for things like special syntax characters like "what does '

N - character at index 1
a - character at index 2
m - character at index 3
e - character at index 4

. . 

Name for you and me are like . . pronounced "n-aye-mmm"

-_- it doesn't really mean anything right ? . . it's like -_- . . 

N - character at index 1
a - character at index 2
m

-_- I don't know what I'm trying to say . . there's something about 'parallel-realities' and how it's like -_- in a parallel reality . . the character at index 3 is -_- something like -_- unordered yet . . and so it could be something like 'h' . . and so -_- . . 

N - character at index 1
a - character at index 1
h 

in this parallel reality, for some reason, N and a are overlapping one another . . and so the pronunciation is like '??' I don't know . . maybe there is a special pronunciation for overlapping characters or parallel overlapping characters or something like that . . so uh . . maybe another representation could be . . 

N,a - character at index 1
h // not characterized yet, or ordered yet (maybe it's like invisible to our eyes because the author has not typed that character yet or something like that where we haven't identified that character yet and so it could be like a 'supposed' or 'anticipated' character but it's not 'actualized' or 'characterized in physical perception or something like that' or 'characterized by community agreement or something like that' 'or something like that')

coule
could

coule

c - character at index 1
o - character at index 2
u - character at index 3
l - character at index 4
e - character at index 5

but 'e' is like a so called 'anana-anana-anana' right? since -_- well we've looked at this example and you and I could possibly agree that it's possibly a 'typographical error' and so it's like -_- . . well . . -_- is it 'e' that is at index 5 ? or is really another character at index 5 ? . . 

invisible characters are like . . all of the characters right ? . . because you don't always know what the intention was ? . . right ? . . I don't know . . 

. . 

I think time is also interesting . . but it's like . . 

Give| me| a| good| name
Give me a| good| name
Give me| a| good| name

. . 

database-item-name-1
database-item-name-2
database-item-name-3

. . 

These are good names

. . 

database-item-store-name-1
database-item-store-name-2
database-item-store-name-3

. . 

well . . alright . . I think that's okay . . 

If you call everything 'item' it's kind of 'idempotent' right? which is kind of a funny feature xD . . 

if you have idempotency in this case maybe it's fun to spam your program on other usecases that you hadn't considered and so it's like . . yea . . look at that . . the program still works . . 

well if you name your program name something not necessarily idempotent -_- . . like -__ . . 'database-carrot-store-1', 'database-banana-store-1', 'database-t-shirt-store-1', then maybe those names are going to suggest certain things like 'yea, alright . . t-shirts . . yea, alright . . carrots . . yea, alright . . bananas . . yea, alright . . yea . . makes . . sense'

well it's good to make sense so it's like . . yea . . i like that . . 

'database-item-store-name-1'

. . 

well . . yea . . you don't always need idempotency . . it's just like . . yea -_- . . please don't do that . . all the time . . :D . . 

. . 

alright . . well . . introducing names . . is really cool . . because then you can have something like . . 

- /database-1
  - database-item-store-1.js
  - database-item-store-2.js
  - database-item-store-3.js
- /database-2
- /database-3
- /network-protocol
- /event-listener
- /permission-rule
- /computer-program-manager
- /scale-matrix-amana
- /anana-anana-anana

. . . .

well . . also we might need . . or we might want to look at what a database is . .

a database . . in however many days I've spent looking at them and not necessarily understanding all of the ins-and-outs of how they work . . is something like . . 

(1) item store
(2) item
(3) algorithms

. . 

an item and an item store are interesting . . let's maybe take a look at what distinguishes them . . (1) an item store is like a folder . . on your computer file system and so if you know what a computer file system looks like . . well . . okay . . well . . (2) an item is like a file . . but it's like . . a file contains things like text elements or uh binary encoded elements or something characteristic like that . . and so well . .

item store contains characteristic elements like files
item contains characteristic elements like text

text and files are differentiated by their representation in memory

text is like 'a string of characters in memory'
file is like 'a string of characters in memory' with extra things like 'meta-data' and 'file-name' and 'file-extension' and 'date-last-updated' and 'date-created'

and so it's like hmm . . well . . I guess they similar right -_-

-_- well yea but it's like file is hardcoded to have this or that feature by default depending on the file system . . 

and so if you wanted to add additional analytics to your file (which you maybe don't really neeeeeedddddddddddd) like the name of the user who last edited the file like their private key or something like that or maybe an index to a website where the file can be accessed which would maybe not be that great since staying http agnostic isn't that bad of a deal but it's like if files had that it would be a big deal to connect other files on the internet . . and so distribution gateways could also be listed as part of the file 'meta-data' along with things like 'yea, file type'

well . . 

It's not that big of a deal . . 

uh . . 

It's like you don't have to differentiate between an item store and an item since they are both text . . or something like that . . 

text . . 

but uh . . okay . . 

well for 'so-called efficiency reasons' you maybe don't want to say . . I will store the characters of . . 'hello world' in the item store at different item index values like . . 

let itemStore = {
  item-index-1: { itemId: 'item-index-1', itemValue: 'h' },
  item-index-2: { itemId: 'item-index-2', itemValue: 'e' },
  item-index-3: { itemId: 'item-index-3', itemValue: 'l' },
  item-index-4: { itemId: 'item-index-4', itemValue: 'l' },
  item-index-5: { itemId: 'item-index-5', itemValue: 'o' },
  item-index-6: { itemId: 'item-index-6', itemValue: ' ' },
  item-index-7: { itemId: 'item-index-7', itemValue: 'w' },
  item-index-8: { itemId: 'item-index-8', itemValue: 'o' },
  item-index-9: { itemId: 'item-index-9', itemValue: 'r' },
  item-index-10: { itemId: 'item-index-10', itemValue: 'l' },
  item-index-11: { itemId: 'item-index-11', itemValue: 'd' }
}

. . 

we would perhaps prefer to store the item as one and not several items . . and so . . 

treating an item store like an item is not necessarily . . nice :)

// this is maybe better to do . . since well . . our database friends at micoroowo would possibly like that a lot . . but it's like yea . . 

let itemStore = {
  item-index-1: { itemId: 'item-index-1', itemValue: 'hello world' }
}

. . 





[Written @ 22:20]

I am a little confused. I have to take a break. I'm not sure where this project is going.

(1) look at ecoin
(2) ecoin is a website
(3) oicp is helping make ecoin networking easy
(4) oicp is not fun to make

. . 

okay, well I think that is my problem. I have to make oicp but it's like . . it's not really fun to make . . it's like . . it's not fun to make . . it's not fun . . -_- but that is an excuse . . it is not a good looking codebase . . you have to tell me things like (1) where do I find a program file in this file system? . . (2) where do I place a file in this file system? . . (3) how do I connect one file to another using 'import' syntax or something like that? . . 

These questions are supposed to be answered by OICP by default because you don't need to ask those questions 

I think it's difficult to move forward now because I need to make some hard decisions like "just give things names" and be done with it.

Give things names. Names are the best. And those names need to exchange phone numbers like "where are you located?" and "when were you located there?" I'm sorry . . from there I'm not sure what else to provide for those files and how they are arranged and how they import one another or reference one another in those arrangements.

Okay . . from here I am not really clear on what to do . . a location is like a name . . and so . . for example . . "directory-name/directory-name-2" . . and that is a good name . . and so a file can be located at . . "directory-name/directory-name-2/file-name.txt" . . which is like . . the address location name for this file named "file-name.txt" but it's kind of weird esoteric naming method because it's based on the so-called "syntax tree" which is a construction like "do you believe that my name is meaningful in this or that syntactical landscape" and so "/" is like a special syntax to designate a special sort of syntactic environment but it's like if you have a good name then maybe you don't need to designate "/" and so -_- which other syntactic recourse methods should we apply to our directory file address names? . . well maybe ~ (pronounced "tilda") is a good one to designate a special type of file or directory name and so uh . . maybe something special could be applied in reasoning or computing on those files or folders . . but in uh . . the traditional industry practice . . I uh . . don't know of a uh . . really uh . . standard way of applying the ~ (pronounced "tilda") syntax . . 

/directory-name
  - /directory-name-2
    - ~file-name.txt // do something special
    - ~~file-name.txt // do something special, recursively
    - ~~~file-name.txt // do something special, recursively 2 times
    - /~directory-name-3 // do something special, this time for a folder
      - /directory-name-4
      - /directory-name-5


Do you have to have special syntax? I'm not sure . . I suppose if you want to apply a special algorithm or something to the name . . 

But it's like . . if you don't use a special syntax then . . it's possibly okay as well . . 

If you look at a name like "hello-world" . . well then maybe it's okay . . because it's like . . -_- . . is it really . . special??

Okay . . 

I'm not sure . . it's like . . "do-you-like-haircuts" . . this is also a name and it possibly really good . . but it's like . . yea . . we had to make a name . . so it's like . . yea . . just give me a name . . 

hmm . . well . . maybe my underlying point to make is something like a global naming system that is really uh . . -_- . . if you have one name . . that's good enough . . and if you want to make references to other names . . in that one name . . that is also fine . . 

And so for example . . 

file-name-1-file-name-2-file-name-3

and the reason to use folders is now obsolete . . because it's like . . a folder is like a list . . but it's like an indented list which isn't very different from a list . . it's like . . if you want to extend the indentation to more than the uh . . parent-child relation you can do that . . and so you can have uh . . -_- tri-linear indentations where you can have . . 2 or 3 or more lists of items for every folder . . and so its still like a list . . because it's like . . yea . . well . . it''s like you can try to subdivide that into a traditional parent-child folder structure . . and then possibly even go further and subdivide that into a list structure . . and so . . time parenthesised dialing clocks are like . . well . . 

. . 

okay . . 

okay . . -_- . . a list . . 

-_-

I took notes on this in my physical paper material journal notebook . . I suppose -_- I could transcribe those . . but that could take a few minutes >.< [remind me to take notes later right?]

you're going to look at how to name things . . 

(1) give something a name
(2) space and time events are useful for this
(3) it's like, -_- what the hell? is space and time even real?

yako . . 

It's like you have to tell yourself to agree to what you see is "okay" -_- . . what the hell?! . . 

right ? . . 

you are a good person
nosrep doog a era uoy

abc????

I don't know . . it's like -_- . . events are interesting things . . 

(1) tell me your name
(2) huh

yea

. . 















