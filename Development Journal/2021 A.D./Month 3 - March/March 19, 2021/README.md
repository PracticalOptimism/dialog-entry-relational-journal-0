


# Development Journal Entry - March 19, 2021


[Written @ 23:50]



[Written @ 23:49]


# Name of the Game

"No Game is like this one" description
- Very active message about the game

No Game is like this one but here are a few other recommendations

- Always where a hat when playing this game
	- Safety Rules and Regulations for playing sefely and fairly [and friendly]

- Alright here is the game board idea
	- Marketing materials and franchise bombardment aspects
	- Full paragraph lists of what the game is about. No Emojis because professionalism is part of the game

And Now a list of adjectives about what type of game you would like to make. This is like an intention description list, but you want to be specific about the intention like "no barbarism is very easy to characterisze" and so something like

This game is about (topic 1) (topic 2) and (topic 3) but if you want to relate it to other topics that is also reasonable and effective like (topic type 1 - alphabet soup of letters here) (topic type 2 - alphabet soup of letters here) and (topic type 3 - alphabet soup of letters here)

Answer the question about a table of contents

Answer the question about the way a computer program works

Answer the question about why the computer program is really interesting

Answer the questions about why friends are playing the computer program right now

Answer the questions about why friends are playing the computer program 2 days from now

Answer about the name of the program

End Of Program Description




[Written @ 23:48]


# Name of the Game

"No Game is like this one" description
- Very active message about the game

"No Game is like this one but here are a few other recommendations

- Always where a hat when playing this game
	- Safety Rules and Regulations for playing sefely and fairly [and friendly]

- Alright here is the game board idea
	- Marketing materials and franchise bombardment aspects
	- Full paragraph lists of what the game is about. No Emojis because professionalism is part of the game

And Now a list of adjectives about what type of game you would like to make. This is like an intention description list, but you want to be specific about the intention like "no barbarism is very easy to characterisze" and so something like

This game is about (topic 1) (topic 2) and (topic 3) but if you want to relate it to other topics that is also reasonable and effective like (topic type 1 - alphabet soup of letters here) (topic type 2 - alphabet soup of letters here) and (topic type 3 - alphabet soup of letters here)

Answer the question about a table of contents

Answer the question about the way a computer program works

Answer the question about why the computer program is really interesting

Answer the questions about why friends are playing the computer program right now

Answer the questions about why friends are playing the computer program 2 days from now (2 days from now is an antonym of "yea, a future that is probable and in a distant era from the one you know" it's like saying the verb "1 day from now" which is meant to specify "tomorrow" or "a late period in life" or "sometime later" but it's really more like "1 day" is representative of "well, whenever . . " and so . . "1 year in the terms of 365 days" can be like "1 day proportional to 1 year" and so "1 earth day could be 365 normal-earth cycles around the sun" and "1 day could also be 687 normal-earth cycles" . . and so 1 day could mean anything like "1 second from now" if you're also proportionating in those terms and so any specific time of date from now is really considered proportional to "1 day" which is like no longer really "1 day from the normal earth measurement system" or something like that) . . and so 2 days is like "yea, very long time and in fact, you can say something like 'too long but even longer than that'" which is like "yea, take a measuring stick like a ruler or a pool stick for billiards or uh maybe even a fork or spoon silverware item or maybe even your human hand and measure it out in front of you. If you measure your day based on the length of that measuring device, you are able to then understand that 2 days is, yea, your hand or measuring device is able to move that 2nd day forward so you never actually get to see it. It's just like an adjective to "yea, keep going" or "yea, keep running" or something like that. 3 days is like "completion" and so it's like "you completed a life cycle of existence" or something like that and so possibly -_- I don't know how to describe it but it's like "1, 2, 3" and then you're like done I guess. "1, 2, 3" in general in how I've been using it recently is like a measurement pattern for "yea, I agree" and "that's it" "it's finished" or something like that. So if you're working on a project, "1, 2, 3" is "the project is finished" or something "simple and straightforward" like that . . but it's also possible to not be satisfied with your project and so it's like "hmm, alright, 1, 2, " "let's start at it again" . . and so . . "1, 2" is like "incomplete" or "unsatisfactory" . . "1" is like "I haven't really started yet, but it's like an aspect or an idea or an intuition or a statement of what I would like to actually get started with at some point in the really theoretical future which is like maybe I'm not really interested in getting started but it's like 'yea, i'll not really tell you that but it's like, yea . . '" so . . "1, " . . "yea ,. . alright . . " . . it's like "bullsh*t" if you really want to use language like that but it's like . . yea . . it's not really in a way to be talked about . . sort of like a socalled "half baked thought" or "a thought in incubation" or "a thought that is proposed but not half ready" or "not nearly ready for proposal or intuitive development" or something really whacky like that . . which is like "yea, alright, it's really like precum" or "precompiled code" which is like "yea, it's really not even ready to be used" . . but "1, 2, 3" means the precompiled code is now a full baby that is ready to grow up on their own . . and so impregnating the idea was a successful endeavor where you have a working prototype program that's walking around and ready to impregnate more people with their ideas.





[Written @ 23:16]

