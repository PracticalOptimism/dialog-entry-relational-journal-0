

# Development Journal Entry - April 24, 2021


[Written @ 4:27]

This was an earlier README.md presented on the NPM website . .

[Copy-Pasted from NPMJS (https://npmjs.org/package/@ecorp-org/ecoin)]

Ecoin Ecoin
Ecoin Ecoin

A digital currency with a Universal Basic Income (UBI)

Website Resources
💾 Source Code | 🕹️ Demos | 🎬 Video Tutorial | 🔴 Live Programming Development Journal

Project Development Status Updates
npm version downloads coverage report pipeline status

This Documentation Page was last Updated on Wed July 29 2020 13:14:00 GMT-0500 (Central Daylight Time)
Installation
Node Package Manager (NPM) Installation
npm install --save @ecorp-org/ecoin
Script import from JavaScript (requires NPM installation)
import * as ecoin from '@ecorp-org/ecoin'
HTML Script Import
<script src="https://unpkg.com/@ecorp-org/ecoin"></script>
Getting Started
Text Description	Image Screenshot	Getting Started Source Code
Accessing the Ecoin Digital Currency Symbol		Reference URL
Creating an Ecoin Price Tag Label		Reference URL
Creating a Payment with Ecoin Button		Reference URL
Creating an Ecoin Accepted Here Label		Reference URL
Getting Started #1: Accessing the "Ecoin Digital Currency Symbol Icon" Url
const ecoinImageIconUrl = ecoin._project.constants.project.PROJECT_DESCRIPTION_OBJECT.projectImageIconUrl
Getting Started #2: Creating a "Payment with Ecoin" Button
const digitalCurrencyHtmlComponentAlgorithms = ecoin.usecase.digitalCurrency.algorithms.htmlComponents
 
// Specify the account id for the recipient account
const myShoppingMallAccountId = 'my-personal-shopping-mall-account-id'
 
// Specify the amount of payment to be received
const priceForTheProductBeingSold = 100
 
// Specify the text memorandum or reason for the purchase
const textDescriptionOfTheProductBeingSold = 'A pair of shoes'
 
// Create the new html component / document object
const payWithDigitalCurrencyButton = digitalCurrencyHtmlComponentAlgorithms.createPaymentWithDigitalCurrencyButton.function({
  recipientAccountId: myShoppingMallAccountId,
  digitalCurrencyAmount: priceForTheProductBeingSold,
  textMemorandum: textDescriptionOfTheProductBeingSold
})
 
// Add the html component to your website
document.body.appendChild(payWithDigitalCurrencyButton)
 
// Delete the old component if you need to replace the old values
// with a new component
digitalCurrencyHtmlComponentAlgorithms.deleteHtmlComponent.function(payWithDigitalCurrencyButton.id)
 
Getting Started #3: Creating an "Ecoin Accepted Here" Label
const digitalCurrencyHtmlComponentAlgorithms = ecoin.usecase.digitalCurrency.algorithms.htmlComponents
 
// Create the html component / document object
const ecoinAcceptedHereLabel = digitalCurrencyHtmlComponentAlgorithms.createDigitalCurrencyAcceptedHereLabel.function()
 
// Add the html component to your website
document.body.appendChild(ecoinAcceptedHereLabel)
 
// Delete the old component if you need to replace the old values
// with a new component
digitalCurrencyHtmlComponentAlgorithms.deleteHtmlComponent.function(ecoinAcceptedHereLabel.id)
 
JavaScript API Reference 🚧
The API reference documentation is a work-in-progress . For a guide on how to use the ecoin javascript library, please look in precompiled/usecase/digital-currency of the source code directory which will be an exact reflection of the structure of the algorithms, data structures, constants, variables and infinite loops exported by the ecoin javascript object . For example, ecoin.usecase points to the precompiled/usecase directory and ecoin.usecase.digitalCurrency points to the precompiled/usecase/digital-currency directory .

JavaScript Runtime Environments
🚧 Work-in-progress	✅ Supported	🚫 Not supported
JavaScript Runtime Environment	Node.js	Google Chrome	Mozilla Firefox	Opera Browser	Apple Safari	Bluelink Labs Beaker Browser	Microsoft Edge	Brave Browser	Deno
Supported ?	🚧	✅	✅	✅	✅	✅	✅	✅	🚫
To Resolve Problems and Submit Feature Requests
To report issues or submit feature requests with this project, please visit Ecoin's Gitlab Issue Tracker

About This Project
Ecoin is a digital currency with a Universal Basic Income (UBI).

Digital Currency is a currency that operates using digital computers. A digital computer makes trading resources fast and doesn't require hand-to-hand transactions from person to person. A digital currency makes trading currencies fast and effective for trading resources through a common medium of exchange across many resource providers and transaction medium influencers such as banks and governments.

Universal Basic Income is an infinite loop process that gives a given amount of currency to each individual of the digital currency community at every interval of the infinite loop.

Benefits
🤝 Complete Trading Agreements: Send money and receive money in terms of digital computer numbers stored in a database. The digital currency is useful when people use the currency to transact with other goods and services.

💎 Universal Financial Entrails: A Universal Basic Income supports many people to keep their standard of living above a certain threshold that lasts for the rest of the life of the individual and their personhood. Regardless of what career choices are made, an individual can support their hobbies and spend time pursuing activities that they enjoy and not necessarily their career tasks while still affording to have a livelihood in a comfortable environment that's supported by the universal basic income's unconditional payment of life long rewards. Hobbies can be the source of innovation and creative free sprawl, from a spiritual and technological perspective, that can give a civilization new periods of understanding of various phenomenon about the nature of life.

Features
🌎 Global Anonymous Support: The currency uses trackless random number generation to keep your identity a secret

📟 Distributed Computing: A distributed peer-to-peer network operated by Open Internet Communication Protocol (OICP) (work-in-progress)

🌳 Universal Basical Income: Receive a numbered amount of currency to your account on a regular basis

💹 Currency Exchange: Trade the digital currency for the environment currencies available through Stripe and PayPal (work-in-progress)

🛍 Online Shopping Mall: Always have a place to spend your digital currency (work-in-progress)

Limitations
🤓 Work-in-progress: This project is a work-in-progress. The project architecture and documentation are being updated regularly. Please learn more about the development life cycle by visiting our live programming development journal on youtube: https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g
Related Work
Related projects in the space of (1) digital currencies, (2) universal basic income digital currencies, (3) digital currency exchanges and (4) online shopping malls (well-known projects are listed first):

Digital Currency / Cryptocurrency Providers	Online Payment Providers	Digital Currency with a Universal Basic Income Providers	Currency Exchange Providers	Online Shopping Mall Providers
[1] Bitcoin, Satoshi Nakamoto et al., https://en.wikipedia.org/wiki/Bitcoin	[1] PayPal, Elon Musk, Peter Thiel et al. https://en.wikipedia.org/wiki/PayPal	[1] Circles, Andrew Milenius et al. https://github.com/CirclesUBI	[1] Coinbase, Brian Armstrong, Fred Ehrsam et al. https://en.wikipedia.org/wiki/Coinbase	[1] Amazon, Jeff Bezos, MacKenzie Bezos et al. https://en.wikipedia.org/wiki/Amazon_(company)
[2] Ethereum, Vitalik Buterin, Charles Hoskinson et al. https://en.wikipedia.org/wiki/Ethereum	[2] Stripe, Patrick Collison, John Collison et al. https://en.wikipedia.org/wiki/Stripe_(company)	[2] Mannabase, Eric Stetson et al. https://mannabase.com	[2] Gemini, Cameron Winklevoss, Tyler Winklevoss et al. https://en.wikipedia.org/wiki/Gemini_(company)	[2] Alibaba, Jack Ma et al. https://en.wikipedia.org/wiki/Alibaba_Group
[3] HBar, Dr. Leemon Baird, Mance Harmon et al. https://www.hedera.com/hbar	[3] Ecoin, Jon Ide et al. https://www.gitlab.com/ecorp-org/ecoin	[3] GoodDollar, Yoni Assia et al. https://github.com/GoodDollar	[3] Ecoin, Jon Ide et al. https://www.gitlab.com/ecorp-org/ecoin	[3] Shopify, Tobias Lütke et al. https://en.wikipedia.org/wiki/Shopify
[4] EOS, Daniel Larimer, Brendan Bloomer et al. https://en.wikipedia.org/wiki/EOS.IO	--	[4] SwiftDemand, Christopher Gregorio et al. https://github.com/swiftdemand	--	[4] Ecoin, Jon Ide et al. https://www.gitlab.com/ecorp-org/ecoin
[5] Ada, Charles Hoskinson et al. https://coinmarketcap.com/currencies/cardano	--	[5] Ecoin, Jon Ide et al. https://www.gitlab.com/ecorp-org/ecoin	--	--
[6] Ecoin, Jon Ide et al. https://www.gitlab.com/ecorp-org/ecoin	--	--	--	--
Acknowledgements
License
MIT

Keywords
digital currencyuniversal basic incomeshopping malldigital currency exchangecurrency exchange


[Written @ 3:46]

Ecoin README.md that was in transit and then we cancelled it because we had continued doing research and re-discovered another interesting format which is available at the Development Journal Entry for April 21, 2021

. . 

[Copy-Pasted From the Ecoin Codebase . . but not committed to the repository . . location at . . https://gitlab.com/ecorp-org/ecoin]

<h1>
  <a href="https://gitlab.com/ecorp-org/ecoin"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/15242461/android-chrome-512x512.png?width=64" alt="Ecoin" width="35"></a> Ecoin
</h1>

### 💭 (1) Project Intention Description

This project is intended to introduce the `ecoin` project codebase as a way of achieving a (1) **digital currency with a universal basic income** and (2) a **product resource center for goods and services** . 

### 🤷 (2) Project Anticipation Description

##### 📝 (2.1) Project Description
A digital currency with a Universal Basic Income (UBI)

[![Project Date Created](https://img.shields.io/badge/Development%20Started%20Date-November,%202019-blue)](#) [![Completion](https://img.shields.io/badge/Status-In%20Active%20Development-yellow)](#) [![Completion](https://img.shields.io/badge/Codebase%20Features%20Complete-1%20of%205-red)](#) [![Completion](https://img.shields.io/badge/Latest%20Development%20Update-December%209,%202020%2011:47%20UTC-yellow)](#)

##### 📬 (2.2) Project Contact Information

[![Email Address Contact Information](https://img.shields.io/badge/📨%20Jon%20Ide%20|%20Development%20Team-practialoptimism9@gmail.com-blue)](https://www.guerrillamail.com/compose?emailAddress=practialoptimism9@gmail.com)

##### 💻 (2.3) Project Development Process Status Updates
[![npm version](https://badge.fury.io/js/%40ecorp-org%2Fecoin.svg)](https://badge.fury.io/js/%40ecorp-org%2Fecoin) [![downloads](https://img.shields.io/npm/dt/@ecorp-org/ecoin)](https://www.npmjs.com/package/%40ecorp-org/ecoin) [![coverage report](https://gitlab.com/ecorp-org/ecoin/badges/master/coverage.svg)](https://gitlab.com/ecorp-org/ecoin/-/commits/master) [![pipeline status](https://gitlab.com/ecorp-org/ecoin/badges/master/pipeline.svg)](https://gitlab.com/ecorp-org/ecoin/-/commits/master)

##### 🌐 (2.4) Project Accessibility and User Experience Accessibility Metrics
[![Community Feedback](https://img.shields.io/badge/😃%20Community%20Feedback-Requested%20By%20Paget%20Kagy,%20Unicole%20Unicron,%20Elon%20Musk,%20Andrew%20Yang,%20Jack%20Dorsey%20and%20more-blue)](#) [![Lighthouse](https://img.shields.io/badge/🌐%20Service%20Website%20Lighthouse%20Accessibility%20Grade%20For%20Mobile-24%20of%20100-red)](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fecoin-f9c5c.web.app%2F&tab=mobile) [![Codebase Documentation](https://img.shields.io/badge/💾%20Codebase%20Documentation-10.50%20%25-red)](#) [![User Interface / User Experience](https://img.shields.io/badge/🖼️%20User%20Interface%20%2f%20User%20Experience-80.00%20%25-green)](#) [![Language Support](https://img.shields.io/badge/🌎%20Language%20Support-1%20of%207,111-red)](#) [![JavaScript Runtime](https://img.shields.io/badge/💾%20JavaScript%20Runtime-1%20of%209-red)](#)

##### 👤 (2.5) Project Consumer Resource
💾 [Project Codebase](https://gitlab.com/ecorp-org/ecoin) | 💻 [Project JavaScript Library](https://www.npmjs.com/package/@ecorp-org/ecoin) | 🌐 [Project Service Instance Website](https://ecoin369.com) | 📱 Project Instance Android App | 📱 Project Instance iOS App | 🤖 Project HTTP Server

##### 🗞️ (2.6) Project News Update Status Resource
🎬 [Video Tutorial](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g) | 🔴 [Live Programming Development Journal](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)

### 📋 (2.7) Project Document Table of Contents

**Note**: Search for the eyes (👀) to see the newest updated content . Click on the pointer (👈) to expand the dialog for more information .

* 💭 ([1](#)) Project Intention Description
* <details><summary>⁉️ (<a href="#">2</a>) Project Anticipation Description 👈</summary>
  <ul>
    <li>📝 (<a href="#">2.1</a>) Project Description</li>
    <li>📬 (<a href="#">2.2</a>) Project Contact Information</li>
    <li>💻 (<a href="#">2.3</a>) Project Development Process Status Resource</li>
    <li>🌐 (<a href="#">2.4</a>) Project Accessibility and User Experience Accessibility Metrics</li>
    <li>👤 (<a href="#">2.5</a>) Project Consumer Resource</li>
    <li>🗞️ (<a href="#">2.6</a>) Project News Update Status Resource</li>
    <li>📋 (<a href="#">2.7</a>) Project Document Table of Contents</li>
  </ul></details>
* 📝 (<a href="#">3</a>) Project Title
* 📝 (<a href="#">4</a>) Project Description
* <details><summary>✨ (<a href="#">5</a>) Project Benefits 👈</summary>
  <ul>
    <li>🤝 (<a href="#">5.1</a>) Complete Trading Agreements</li>
    <li>💎 (<a href="#">5.2</a>) Universal Financial Entrails</li>
  </ul>
</details>

* <details><summary>⭐ (<a href="#">6</a>) Project Features 👈</summary>
  <ul>
    <li>🌍+🎭 (<a href="#">6.1</a>) Global Anonymous Support</li>
    <li>👪 (<a href="#">6.2</a>) Create Multi-Party Currency Transactions</li>
    <li>🤖 (<a href="#">6.3</a>) Distributed Computing</li>
    <li>🌳 (<a href="#">6.4</a>) Universal Basic Income</li>
    <li><details open><summary>🛍️ (<a href="#">6.5</a>) Product Resource Center 👈</summary>
      <ul>
        <li>💼 (<a href="#">6.5.1</a>) Job Employment Resource Center</li>
        <li>🛍️ (<a href="#">6.5.2</a>) Products and Services Resource Center</li>
        <li>🤝 (<a href="#">6.5.3</a>) Currency Exchange Resource Center</li>
      </ul>
    </details></li>
  </ul>
</details>

* 🚫 (<a href="#">7</a>) Project Limitations
* 📝 (<a href="#">8</a>) Project Additional Notes
* 📬 (<a href="#">9</a>) Project Contact Information
* 🗞️ (<a href="#">10</a>) Project News Updates
* <details><summary>⚖️ (<a href="#">11</a>) Project Community Restrictions & Guidelines 👈</summary>
  <ul>
    <li>⚖️ (<a href="#">11.1</a>) Project License Agreement - MIT License</li>
  </ul>
</details>

* <details><summary>💾 (<a href="#">12</a>) Project Codebase 👈</summary>
	<ul>
		<li>📝 (<a href="#">12.1</a>) Introduction</li>
		<li>💾 (<a href="#">12.2</a>) Codebase Installation</li>
		<li>🎨 (<a href="#">12.3</a>) How To Rebrand The Project</li>
		<li><details open><summary>🔄 (<a href="#">12.4</a>) Codebase Development Process 👈</summary>
		<ul>
			<li>🔎 (<a href="#">12.4.1</a>) How To Find Files</li>
			<li>⭐ (<a href="#">12.4.2</a>) How To Create Files</li>
			<li>🔄 (<a href="#">12.4.3</a>) How To Update Files</li>
			<li>🚫 (<a href="#">12.4.4</a>) How To Delete Files</li>
			<li>🔎 (<a href="#">12.4.5</a>) How to Find Directories</li>
			<li>⭐ (<a href="#">12.4.6</a>) How to Create Directories</li>
			<li>🔄 (<a href="#">12.4.7</a>) How to Update Directories</li>
			<li>🚫 (<a href="#">12.4.8</a>) How to Delete Directories</li>
			<li>▶️ (<a href="#">12.4.9</a>) How To Start The Development Environment</li>
			<li>▶️ (<a href="#">12.4.10</a>) How To Start The Production Environment</li>
			<li>⏹️ (<a href="#">12.4.11</a>) How To Stop The Development Environment</li>
			<li>⏹️ (<a href="#">12.4.12</a>) How To Stop The Production Environment</li>
		</ul>
		</details></li>
	</ul>
</details>


* <details><summary>💾 (<a href="#">13</a>) Project Consumer Resource 👈</summary>
	<ul>
		<li>📝 (<a href="#">13.1</a>) Introduction</li>
		<li>📟 (<a href="#">13.2</a>) Project Command Line Interface Tool</li>
		<li><details open><summary>💻 (<a href="#">13.3</a>) Project JavaScript Library 👈</summary>
			<ul>
				<li>💾 (<a href="#">13.3.1</a>) JavaScript Library Installation</li>
				<li>💾 (<a href="#">13.3.2</a>) JavaScript API Reference</li>
				<li>✅ (<a href="#">13.3.3</a>) Supported JavaScript Runtime Environments</li>
			</ul>
		</details></li>
		<li><details open><summary>🌐 (<a href="#">13.4</a>) Project Service Website 👈</summary>
			<ul>
				<li>🌐 (<a href="#">13.4.1</a>) Project Instance Website</li>
				<li><details open><summary>🎬 (<a href="#">13.4.2</a>) Feature Visualization 👈</summary>
				<ul>
					<li>🎬 (<a href="#">13.4.2.1</a>) Digital Currency Features</li>
					<li>🎬 (<a href="#">13.4.2.2</a>) Digital Currency Account Features</li>
					<li>🎬 (<a href="#">13.4.2.3</a>) Digital Currency Transaction Features</li>
				</ul>
				</details></li>
				<li><details open><summary>📊 (<a href="#">13.4.3</a>) Accessibility and Performance Measurements 👈</summary>
				<ul>
					<li>📊 (<a href="#">13.4.3.1</a>) PageSpeed Insights</li>
					<li>📊 (<a href="#">13.4.3.2</a>) WebPageTest</li>
					<li>🌐 (<a href="#">13.4.3.3</a>) Language Support Measurements</li>
					<li>🌐 (<a href="#">13.4.3.4</a>) Internationalization Support Measurements</li>
					<li>🌐 (<a href="#">13.4.3.5</a>) Disability Assistance Technology Support Measurements</li>
				</ul>
				</details></li>
				<li><details open><summary>👤 (<a href="#">13.4.4</a>) Active User Usage Measurements 👈</summary>
					<ul>
						<li>📈 (<a href="#">13.4.4.1</a>) Google Analytics measurements</li>
						<li>📈 (<a href="#">13.4.4.2</a>) Hotjar measurements</li>
					</ul>
				</details></li>
			</ul>
		</details></li>
		<li>📱 (<a href="#">13.5</a>) Project Android App</li>
		<li>📱 (<a href="#">13.6</a>) Project iOS App</li>
		<li>🤖 (<a href="#">13.7</a>) Project HTTP Server</li>
		<li>🤖 (<a href="#">13.8</a>) Project Websocket Server</li>
	</ul>
</details>


* <details><summary>💾 (<a href="#">14</a>) Project Provider Resource 👈</summary>
	<ul>
		<li>📝 (<a href="#">14.1</a>) Introduction</li>
		<li>🤖 (<a href="#">14.2</a>) Artificial Intelligence Provider</li>
		<li>🔐 (<a href="#">14.3</a>) Asymmetric Cryptography Provider</li>
		<li><details open><summary>🔐 (<a href="#">14.4</a>) Authentication Provider 👈</summary></details></li>
		<li>💻 (<a href="#">14.5</a>) Computer Resource Provider</li>
		<li>🔐 (<a href="#">14.6</a>) Cryptographic Hash Function Provider</li>
		<li><details open><summary>🤖 (<a href="#">14.7</a>) Database Provider 👈</summary></details></li>
		<li><details open><summary>💻 (<a href="#">14.8</a>) Programming Language 👈</summary></details></li>
		<li><details open><summary>🤖 (<a href="#">14.9</a>) Project Compiler 👈</summary></details></li>
		<li><details open><summary>🔎 (<a href="#">14.10</a>) Text Search Provider 👈</summary></details></li>
		<li><details open><summary>🖼️ (<a href="#">14.11</a>) User Interface / User Experience Framework / Library 👈</summary></details></li>
		<li><details open><summary>🌐 (<a href="#">14.12</a>) Web Browser Website Framework / Library 👈</summary></details></li>
	</ul>
</details>


* <details><summary>💾 (<a href="#">15</a>) Project Usecase Resource 👈</summary>
	<ul>
		<li>📝 (<a href="#">15.1</a>) Introduction</li>
		<li><details open><summary>💱 (<a href="#">15.2</a>) Digital Currency 👈</summary></details></li>
		<li><details open><summary>🛍️ (<a href="#">15.3</a>) Product Resource Center 👈</summary></details></li>
	</ul>
</details>


* <details><summary>👪 (<a href="#">16</a>) Project Community & Volunteers 👈</summary>
	<ul>
		<li>👪 (<a href="#">16.1</a>) Present Volunteers and Community Members</li>
		<li>👪 (<a href="#">16.2</a>) How To Communicate To Project Volunteers</li>
		<li>👪 (<a href="#">16.3</a>) How To Be a Volunteer or Community Member</li>
		<li><details open><summary>👪 (<a href="#">16.4</a>) Community Feedback Measurements Members 👈</summary>
			<ul>
				<li>⁉️ (<a href="#">16.4.1</a>) Frequently Asked Questions</li>
				<li>💭 (<a href="#">16.4.2</a>) Community Comments that initially inspired the project development</li>
				<li>💭 (<a href="#">16.4.3</a>) Community Comments that continue to inspire the project development</li>
				<li>💭 (<a href="#">16.4.4</a>) Recent Feedback relating to the active development process</li>
				<li>💭 (<a href="#">16.4.5</a>) Recent Feedback on the Quality of the Codebase</li>
				<li>💭 (<a href="#">16.4.6</a>) Recent Feedback on the Quality of the Instance Website</li>
				<li>💭 (<a href="#">16.4.7</a>) Recent Feedback on the Quality of the Instance Android App</li>
				<li>💭 (<a href="#">16.4.8</a>) Recent Feedback on the Quality of the Instance iOS App</li>
			</ul>
		</details></li>
	</ul>
</details>

* 👪 ([17](#)) Project Usecase Provided By Related Community Projects


[Written @ 3:40]


Previous Ecoin README.md



[Copy-Pasted From Ecoin Codebase . . ]


<h1>
  <a href="https://gitlab.com/ecorp-org/ecoin"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/15242461/android-chrome-512x512.png?width=64" alt="Ecoin" width="35"></a> Ecoin
</h1>

A digital currency with a Universal Basic Income (UBI)

##### Website Resources
💾 [Source Code](https://gitlab.com/ecorp-org/ecoin) | 🕹️ [Demo Website](https://ecoin369.com) | 🎬 [Video Tutorial](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g) | 🔴 [Live Programming Development Journal](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)

##### Project Development Status Updates
[![npm version](https://badge.fury.io/js/%40ecorp-org%2Fecoin.svg)](https://badge.fury.io/js/%40ecorp-org%2Fecoin) [![downloads](https://img.shields.io/npm/dt/@ecorp-org/ecoin)](https://www.npmjs.com/package/%40ecorp-org/ecoin) [![coverage report](https://gitlab.com/ecorp-org/ecoin/badges/master/coverage.svg)](https://gitlab.com/ecorp-org/ecoin/-/commits/master) [![pipeline status](https://gitlab.com/ecorp-org/ecoin/badges/master/pipeline.svg)](https://gitlab.com/ecorp-org/ecoin/-/commits/master)

##### This Documentation Page was last Updated on Fri November 5 2020 8:48:00 GMT-0500 (Central Daylight Time)

##### Table of Contents


* [Ecoin](#ecoin)
* [About](#about)
* [Benefits](#benefits)
* [Features](#features)
* [Limitations](#limitations)
* [Progress Report](#progress-report)
* [Demo Website Screen-Captures](#%EF%B8%8F-demo-website-screen-captures)
* [Installation](#installation)
* [JavaScript Runtime Environment](#javascript-runtime-environment)
* [JavaScript API Reference](#javascript-api-reference)
* [Contributors](#contributors)
* [Related Work](#related-work)
* [Acknowledgements](#acknowledgements)
* [License](#license)

### About

**Ecoin** is a **digital currency** with a **Universal Basic Income** (UBI) .

**Digital Currency** is a currency that operates using digital computers . A digital computer makes trading resources fast and doesn't require hand-to-hand transactions from person to person . A digital currency makes trading currencies fast and effective for trading resources through **a common medium of exchange across many resource providers** and transaction medium influencers such as banks and governments .

**Universal Basic Income** is an infinite loop process that **gives each individual in the community a given amount of currency at each interval step** of the infinite loop process .



### Benefits
A list of benefits that are provided by a software that provides a (A) **digital currency**, (B) **a universal basic income**


* 🤝 **Complete Trading Agreements**: Send money and receive money in terms of digital computer numbers stored in a database . The digital currency is useful when people use the currency to transact with other goods and services
* 💎 **Universal Financial Entrails**: A Universal Basic Income supports many people to keep their standard of living above a certain threshold that lasts for the rest of the life of the individual and their personhood . Regardless of what career choices are made, an individual can support their hobbies and spend time pursuing activities that they enjoy and not necessarily their career tasks while still affording to have a livelihood in a comfortable environment that's supported by the universal basic income's unconditional payment of life long rewards . Hobbies can be the source of innovation and creative free sprawl, from a spiritual and technological perspective, that can give a civilization new periods of understanding of various phenomenon about the nature of life

### Features
A list of features that are **currently being worked on or have been planned for future development**

* 🌎 **Global Anonymous Support**: A global currency that doesn't require you to have status or citizenship in any particular country or be located or addressed from any particular location (ie. no government issued id required)<sup>1</sup>
* 📟 **Distributed Computing**: A distributed peer-to-peer network operated by [Open Internet Communication Protocol (OICP)](https://gitlab.com/practicaloptimism/oicp)
* 🌳 **Universal Basical Income**: Receive a numbered amount of currency to your account on a constant interval of an infinite loop process (i.e. every 1 minute, 1 day, 1 week, 1 month, etc.)
* 💹 **Currency Exchange**: Trade the digital currency for the environment currencies available through Stripe and PayPal
* 🛍 **Online Shopping Mall**: Always have a place to spend your digital currency


### Limitations
A list of challenges and limitations of the current software project

* ⚙️ **Integration Tests**: Integration tests aren't working yet .
* 💻 **Automated Browser Tests**: End-to-end browser testing isn't implemented yet
* 🤑 **Financial Limitations**: The project is currently maintained by a single individual volunteer who **doesn't have the financial ability to afford the bandwidth and data storage resources** by [Firebase](https://firebase.google.com/), [Algoliasearch](https://www.algolia.com/) and [Google Domains](https://domains.google/)
* 📁 **A Single Database Provider**: The current implementation of the Ecoin source code relies on the single database provider [Google Firebase](https://firebase.google.com/) which, in theory, **the operation of the network relies on trusting Google Firebase** to (1) keep the records of digital currency transactions and accounts and (2) permit any read or write operations that are assumed by the source code
* 🤓 **This project is a work-in-progress**: This project is a **work-in-progress**. There may be flaws in the software that have yet to be identified. If you contact any bugs or flaws in the software, please [let us know by submitting an Issue](https://gitlab.com/ecorp-org/ecoin/-/issues)


### Progress Report
Please visit the [Gitlab Issues Board](https://gitlab.com/ecorp-org/ecoin/-/boards) to learn more about completed, planned and work-in-progress tasks relating to this project


| ✅ **Completed (closed issues)** | 🚧 **Work-in-Progress** | 🗓️ **Planned (open issues)** |
| - | - | - |

##### Completed Tasks

* ✅ **Global Anonymous Support**: This task is completed as the digital currency code doesn't require a government issued id for an individual to create an account and transact the digital current

##### Work-In-Progress / Current Tasks
The current task is expected to be completed in **December, 2020** and yet the development team isn't always the best at planning on the arrival of a feature. Thanks for your patience and support as we work towards our goal of arrival

* 🚧 **Universal Basical Income**: This task is a work-in-progress. The codebase for this feature is currently being developed. This is **being worked on in November, 2020**

##### Planned Tasks / Not Yet Started
The development team isn't very good at planning. Hence, if any dates are missing for expected dates on the arrival or work-in-progress for the features of this project, please expect the work to start in **Novemeber 2020 or later**

* 🗓️ **Distributed Computing**: This task is incompleted. The plan is to build the [Open Internet Communication Protocol](https://gitlab.com/PracticalOptimism/oicp) software before continuing with this task. 
* 🗓️ **Digital Currency Exchange**: This task is incompleted. The plan is to develop an auction where users of the exchange can trade fiat currencies such as the US Dollar through Stripe and PayPal for Ecoin directly to one another through Stripe and PayPal API Keys
* 🗓️ **Online Shopping Mall**: This task is incopmleted. The plan is to develop an online ecommerce website to allow users to buy and sell resources from one another
* 🗓️ **Learn more about End-to-End Testing**: This task is incompleted. The plan is to have end-to-end tests for the project web browser website to ensure the clicks are being registered as expected and things like this. In general, the development team is still learning more about this area of software development.
* 🗓️ **Learn more about Integration Tests**: This task is incompleted. The plan is to have integration tests for the source code to ensure that things work together as many units or components working together as expected. In general, the development team is still learning more about this area of software development.


### 🕹️ [Demo Website](https://ecoin369.com) Screen-Captures

##### 🎬 Account Features

| View Account | Edit Account | Sign In | Sign Out | Delete Account |
| -- | -- | -- | -- | -- |
| ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) |

##### 🎬 Transaction Features

| Create Transaction | View Transaction | Edit Transaction | View Transaction History | Receive Universal Basic Income Transaction |
| -- | -- | -- | -- | -- |
| ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) |

##### 🎬 Digital Currency Statistics Features

| View Digital Currency Statistics | View Account Statistics | View Transaction Statistics | Another Ability | Another Ability |
| -- | -- | -- | -- | -- |
| ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) | ![Photo](https://media1.giphy.com/media/dWTv4gUmb0QxBHCkCi/giphy.gif) |



### Installation

##### [Node](https://nodejs.org/en/) Package Manager (NPM) Installation
```javascript
npm install --save @ecorp-org/ecoin
```

##### [Git](https://git-scm.com/downloads) CLI Installation
```
git clone https://gitlab.com/ecorp-org/ecoin
```

### JavaScript Library Installation

##### Script import from JavaScript (requires NPM installation)
```javascript
import * as ecoin from '@ecorp-org/ecoin'
```

##### HTML Script Import
```html
<script src="https://unpkg.com/@ecorp-org/ecoin"></script>
```

### JavaScript Runtime Environment 
A list of JavaScript runtime environments where the JavaScript library is expected to work

| ✅ **Working** | 🚫 **Not Working** | 🤔 **Not Tested But Expected to Work** |
| - | - | - |

| JavaScript Runtime Environment | Node.js | Google Chrome | Mozilla Firefox | Opera Browser | Apple Safari | Bluelink Labs Beaker Browser | Microsoft Edge | Brave Browser | Deno |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| Does the JavaScript library work in this environment ? | 🚫 No [Learn More]() | 🤔 Untested | 🤔 Untested | 🤔 Untested | 🤔 Untested | 🤔 Untested | 🤔 Untested | ✅ Yes  | 🚫 No [Learn More]() |


### JavaScript API Reference
A list of **algorithms**, **constants**, **data structures**, **infinite loops** and **variables** associated with the project codebase . Click on the _pointer_ 👉 to expand the dialog for more information . 

### API Reference: Algorithms

##### Digital Currency Account
Algorithms related to digital currency accounts .

<!-- Create account -->
<details><summary>👉 createAccount (firebaseAuthUid?: string, accountName?: string, accountUsername?: string, accountDescription?: string): Promise&lt;DigitalCurrencyAccount&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Delete account -->
<details><summary>👉 deleteAccount (accountId: string): Promise&lt;DigitalCurrencyAccount&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Get account -->
<details><summary>👉 getAccount (accountId: string): Promise&lt;DigitalCurrencyAccount&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Get account list -->
<details><summary>👉 getAccountList (): Promise&lt;DigitalCurrencyAccount[]&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Update account -->
<details><summary>👉 updateAccount (accountId: string, propertyKey: string, propertyValue: string): Promise&lt;DigitalCurrencyAccount&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>


##### Digital Currency Transaction
Algorithms related to digital currency transactions

<!-- Create transaction -->
<details><summary>👉 createTransaction (accountSenderId: string, accountRecipientId: string, currencyAmount: number, textMemorandum?: string, isRecurringTransaction?: boolean, isScheduledForLaterProcessing?: boolean, dateTransactionScheduledForProcessing?: Date): Promise&lt;DigitalCurrencyTransaction&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Delete transaction -->
<details><summary>👉 deleteTransaction (transactionId: string): Promise&lt;DigitalCurrencyTransaction&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Get transaction -->
<details><summary>👉 getTransaction (transactionId: string): Promise&lt;DigitalCurrencyTransaction&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Get transaction list -->
<details><summary>👉 getTransactionList (): Promise&lt;DigitalCurrencyTransaction[]&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

<!-- Update transaction -->
<details><summary>👉 updateTransaction (): Promise&lt;DigitalCurrencyTransaction&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>

##### HTML Component / HTML Element
Algorithms to create and delete html components . After creating an html component, you can add the component to an html document object with **appendChild(htmlComponentElement)** in any javascript framework or library

<details><summary>👉 createHtmlComponent (componentId: string): Promise&lt;HTMLElement&gt;</summary>

🚧 <b>Work-In-Progress</b>
</details>
<details><summary>👉 deleteHtmlComponent (componentId: string): void</summary>

🚧 <b>Work-In-Progress</b>
</details>

### API Reference: Constants
Constants related to the **digital currency usecase**

<details><summary>👉 HTML_COMPONENT_ID_DIGITAL_CURRENCY_PRICE_LABEL: string</summary>

🚧 <b>Work-In-Progress</b>
</details>
<details><summary>👉 HTML_COMPONENT_ID_PAY_WITH_DIGITAL_CURRENCY_BUTTON: string</summary>

🚧 <b>Work-In-Progress</b>
</details>

### API Reference: Data Structures
Data structures related to the **digital currency usecase**

<details><summary>👉 DigitalCurrencyAccount</summary>

<pre><code>
class DigitalCurrencyAccount {
  accountId: string = ''
  accountName: string = ''
  accountProfilePictureUrl: string = `${projectDirectoryStaticFileHostBaseUrl}/precompiled/usecase/digital-currency/data-structures/digital-currency-account/static-file-asset/default-account-profile-picture.jpg`
  accountUsername: string = ''
  accountDescription: string = ''

  phoneNumber: string = ''
  emailAddress: string = ''
  homeWebsiteAddress: string = ''

  // physicalLocationAddress: PhysicalLocationAddress = new PhysicalLocationAddress()

  accountCurrencyAmount: number = 0
  latestProcessedTransactionId: string = ''

  dateAccountCreated: number = (new Date()).getTime()

  firebaseAuthUid: string = ''

  constructor (defaultOption?: DigitalCurrencyAccount) {
    Object.assign(this, defaultOption)
  }
}
</code></pre>
</details>

<details><summary>👉 DigitalCurrencyTransaction</summary>

<pre><code>
class DigitialCurrencyTransaction {
  transactionId: string = ''

  accountRecipientId: string = ''
  accountSenderId: string = ''

  senderAccountCurrencyAmountAfterTransactionAccount: number = 0
  recipientAccountCurrencyAmountAfterTransactionAccount: number = 0

  transactionCurrencyAmount: number = 0

  dateTransactionCreated?: number
  dateTransactionProcessed?: number

  dateTransactionScheduledForProcessing?: number

  isScheduledForLaterProcessing: boolean = false


  isRecurringTransaction: boolean = false
  millisecondRecurringDelay: number = 0


  textMemorandum?: string = ''

  constructor (defaultOption?: DigitialCurrencyTransaction) {
    Object.assign(this, defaultOption)
  }
}
</code></pre>
</details>

### API Reference: Infinite Loops
Algorithms that are meant to be operated by an infinite loop process such as **setInterval**, **for loops** and other **infinite loop processes**

<details><summary>👉 initializeAccountListWithDigitalCurrencyAmount (optionObject: OptionObject): Promise&lt;void&gt;</summary>

<h3>Data Structures</h3>

<pre><code>
class OptionObject {
  senderAccountId: string = ''
  digitalCurrencyAmount: number = 0
  textMemorandum?: string = ''
}
</code></pre>

🚧 <b>Work-In-Progress</b>
</details>

<details><summary>👉 applyTransactionList()</summary>

🚧 <b>Work-In-Progress</b>
</details>

<details><summary>👉 applyUniversalBasicIncome (optionObject: OptionObject)</summary>

<h3>Data Structures</h3>
<pre><code>
class OptionObject {
  senderAccountId: string = ''
  accountCurrencyAmount: number = 0
  textMemorandum: string = ''
}
</code></pre>
🚧 <b>Work-In-Progress</b>
</details>


### API Reference: Variables
🚧 <b>Work-In-Progress</b>

### To Resolve Problems and Submit Feature Requests

To report issues or submit feature requests with this project, please visit [Ecoin's Gitlab Issue Tracker](https://gitlab.com/ecorp-org/ecoin/issues)

### Contributors
Everyone is welcome to contribute. There are many ways to contribute and **you are welcome to give us feedback beyond** what we've written here for you to get started

How you can contribute:
* 🤓 **Publish Software Updates**: [Publish software updates](https://gitlab.com/ecorp-org/ecoin/-/issues) to fix issues or improve the existing codebase
* 📝 **Create Documentation**: Create project organization information documentation
* 🚨 **Report Issues**: [Report issues](https://gitlab.com/ecorp-org/ecoin/-/issues) and flaws of the current software
* 💡 **Give Ideas**: [Give us your ideas](https://gitlab.com/ecorp-org/ecoin/-/issues) on how to improve the software
* 🤝 **Share Knowledge**: Share the project with your friends and family
* ❤️ **Donate Resources**: Donate to keep the demo project online for people to use [practialoptimism@gmail.com](practialoptimism@gmail.com)
* 🌎 **Where is Ecoin Transacted?**: [Let us know](https://gitlab.com/ecorp-org/ecoin/-/issues) what businesses and individuals accept ecoin transactions for their products or services. Customers and clients can find your business by having it listed on the community participant list on the public demo website
* 🛍️ **Shop using Ecoin**: [Use the Ecoin digital currency](https://ecoin369.com) to purchase goods and services
* 💭 **Use Your Imagination**: And many many more ways you can contribute

### Related Work
Related projects in the space of (A) **digital currencies**, (B) **universal basic income digital currencies**, (C) **digital currency exchanges** and (D) **online shopping malls** (well-known projects are listed first):

| (A) Digital Currency / Cryptocurrency Providers | (A) Online Payment Providers | (B) Digital Currency with a Universal Basic Income Providers | (C) Currency Exchange Providers | (D) Online Shopping Mall Providers |
| -- | -- | -- | -- | -- |
|[1] **Bitcoin**, Satoshi Nakamoto et al., [https://en.wikipedia.org/wiki/Bitcoin](https://en.wikipedia.org/wiki/Bitcoin) | [1] **PayPal**, Elon Musk, Peter Thiel et al. [https://en.wikipedia.org/wiki/PayPal](https://en.wikipedia.org/wiki/PayPal) | [1] **Circles**, Andrew Milenius et al. [https://github.com/CirclesUBI](https://github.com/CirclesUBI) | [1] **Coinbase**, Brian Armstrong, Fred Ehrsam et al. [https://en.wikipedia.org/wiki/Coinbase](https://en.wikipedia.org/wiki/Coinbase) | [1] **Amazon**, Jeff Bezos, MacKenzie Bezos et al. [https://en.wikipedia.org/wiki/Amazon_(company)](https://en.wikipedia.org/wiki/Amazon_%28company%29) | 
|[2] **Ethereum**, Vitalik Buterin, Charles Hoskinson et al. [https://en.wikipedia.org/wiki/Ethereum](https://en.wikipedia.org/wiki/Ethereum) | [2] **Stripe**, Patrick Collison, John Collison et al. [https://en.wikipedia.org/wiki/Stripe_(company)](https://en.wikipedia.org/wiki/Stripe_%28company%29) | [2] **Mannabase**, Eric Stetson et al. [https://mannabase.com](https://mannabase.com) | [2] **Gemini**, Cameron Winklevoss, Tyler Winklevoss et al. [https://en.wikipedia.org/wiki/Gemini_(company)](https://en.wikipedia.org/wiki/Gemini_%28company%29) | [2] **Alibaba**, Jack Ma et al. [https://en.wikipedia.org/wiki/Alibaba_Group](https://en.wikipedia.org/wiki/Alibaba_Group) | 
|[3] **HBar**, Dr. Leemon Baird, Mance Harmon et al. [https://www.hedera.com/hbar](https://www.hedera.com/hbar) | [3] **Ecoin**, Jon Ide et al. [https://www.gitlab.com/ecorp-org/ecoin](https://www.gitlab.com/ecorp-org/ecoin) | [3] **GoodDollar**, Yoni Assia et al. [https://github.com/GoodDollar](https://github.com/GoodDollar) | [3] **Ecoin**, Jon Ide et al. [https://www.gitlab.com/ecorp-org/ecoin](https://www.gitlab.com/ecorp-org/ecoin) | [3] **Shopify**, Tobias Lütke et al. [https://en.wikipedia.org/wiki/Shopify](https://en.wikipedia.org/wiki/Shopify) | 
|[4] **EOS**, Daniel Larimer, Brendan Bloomer et al. [https://en.wikipedia.org/wiki/EOS.IO](https://en.wikipedia.org/wiki/EOS.IO) | -- | [4] **SwiftDemand**, Christopher Gregorio et al. [https://github.com/swiftdemand](https://github.com/swiftdemand) | -- | [4] **Ecoin**, Jon Ide et al. [https://www.gitlab.com/ecorp-org/ecoin](https://www.gitlab.com/ecorp-org/ecoin) | 
|[5] **Ada**, Charles Hoskinson et al. [https://coinmarketcap.com/currencies/cardano](https://coinmarketcap.com/currencies/cardano) | -- | [5] **Ecoin**, Jon Ide et al. [https://www.gitlab.com/ecorp-org/ecoin](https://www.gitlab.com/ecorp-org/ecoin) | -- | -- | 
|[6] **Ecoin**, Jon Ide et al. [https://www.gitlab.com/ecorp-org/ecoin](https://www.gitlab.com/ecorp-org/ecoin) | -- | -- | -- | -- | 

### Acknowledgements

##### Software Dependencies
**Thank you** to everyone who works on the following projects. The present project, Ecoin, relies on the following projects to work. **Thank you so much**. Also, thank you to the **Open Source Community** for all the light and love ! ✨ ❤️

| Project Compilation | Programming Languages | Website Libraries and Frameworks | User Interface / User Experience JavaScript Libraries | Database Provider | Text Search Provider | Authentication Provider | Computer Resource Provider |
| - | - | - | - | - | - | - | - |
| [1] **Webpack**, [Tobias Koppers](https://github.com/sokra), [timse](https://github.com/timse), [Florent Cailhol](https://github.com/ooflorent), [Sean Larkin](https://github.com/TheLarkInn) et al. [https://webpack.js.org](https://webpack.js.org) | [1] **TypeScript**, Microsoft et al. [https://www.typescriptlang.org](https://www.typescriptlang.org) **JavaScript**, [Brendan Eich](https://en.wikipedia.org/wiki/Brendan_Eich) et al. [https://en.wikipedia.org/wiki/JavaScript](https://en.wikipedia.org/wiki/JavaScript)| [1] **Vue.js**, [Evan You](https://github.com/yyx990803) et al. [https://vuejs.org](https://vuejs.org) | [1] **Vuetify** [John Leider](https://github.com/johnleider), [Heather Leider](https://github.com/heatherleider) et al. [https://vuetifyjs.com](https://vuetifyjs.com) (Material Design Language Framework) | [1] **Firebase Realtime Database**, [Andrew Lee](https://github.com/startupandrew), [James Tamplin](https://github.com/jamestamplin) et al. [https://firebase.google.com/docs/database](https://firebase.google.com/docs/database) (for storing data on remote computers) | [1] **Algolia**, Nicolas Dessaigne, Julien Lemoine et al. [https://www.algolia.com](https://www.algolia.com) | [1] **Firebase Anonymous, Email and Password and Google Authentication**, Google et al. [https://firebase.google.com/docs/auth](https://firebase.google.com/docs/auth) (for specifying unique user access to creating and accessing data while preventing updates and reads from non-permissioned users for that data) | [1] **Heroku**, James Lindenbaum, Adam Wiggins, Orion Henry et al. [https://www.heroku.com](https://www.heroku.com) (for publishing the administrator daemon) |
| [2] **Rollup**, [Rich Harris](https://github.com/Rich-Harris), [Lukas Taegert-Atkinson](https://github.com/lukastaegert), et al. [https://rollupjs.org](https://rollupjs.org) | [2] **SCSS**, [Hampton Catlin](), [Natalie Weizenbaum](), [Chris Eppstein]() [https://sass-lang.com](https://sass-lang.com), **CSS**, [World Wide Web Consortium](https://en.wikipedia.org/wiki/World_Wide_Web_Consortium) et al. [https://en.wikipedia.org/wiki/CSS](https://en.wikipedia.org/wiki/CSS) | [2] **Vuex**, [Evan You](https://github.com/yyx990803), [Katashin](https://github.com/ktsn), [Kia King Ishii](https://github.com/kiaking) et al. [https://vuex.vuejs.org](https://vuex.vuejs.org) | [2] **ECharts**, Deqing Li, Honghui Mei, Yi Shen, Shuang Su et al. [https://github.com/apache/incubator-echarts](https://github.com/apache/incubator-echarts) (for displaying statistics charts such as line charts, bar charts and more) | [2] **Localforage**, [Matthew Riley MacPherson](https://github.com/tofumatt), [Thodoris Greasidis](https://github.com/thgreasi) et al. [https://localforage.github.io/localForage](https://localforage.github.io/localForage) (for storing data locally in the web browser) | [2] **Open Internet Communication Protocol**, [Jon Ide](https://gitlab.com/PracticalOptimism) et al. [https://gitlab.com/PracticalOptimism/oicp](https://gitlab.com/PracticalOptimism/oicp) (planned but not yet implemented) | [2] **Open Internet Communication Protocol**, [Jon Ide](https://gitlab.com/PracticalOptimism) et al. [https://gitlab.com/PracticalOptimism/oicp](https://gitlab.com/PracticalOptimism/oicp) (planned but not yet implemented) | [2] **Firebase Cloud Storage**, Google et al. [https://firebase.google.com/docs/storage](https://firebase.google.com/docs/storage) (for storing user uploaded file content) |
| -- | [3] **HTML**, [Tim-Berners Lee](), [Ted Nelson](https://en.wikipedia.org/wiki/Ted_Nelson) et al. [https://en.wikipedia.org/wiki/HTML](https://en.wikipedia.org/wiki/HTML) | [3] **Vue Router**, [Evan You](https://github.com/yyx990803), [Eduardo San Martin Morote](https://github.com/posva), [宋铄运 (Alan Song)](https://github.com/fnlctrl), [Alexander Sokolov](https://github.com/Alex-Sokolov) et al. [https://router.vuejs.org](https://router.vuejs.org) | -- | [3] **Open Internet Communication Protocol**, [Jon Ide](https://gitlab.com/PracticalOptimism) et al. [https://gitlab.com/PracticalOptimism/oicp](https://gitlab.com/PracticalOptimism/oicp) (planned but not yet implemented) | -- | -- | [3] **Firebase Hosting**, Google et al. [https://firebase.google.com/docs/hosting](https://firebase.google.com/docs/hosting) (for storing the static website files) |
| -- | -- | -- | -- | -- | -- | -- | [4] **Open Internet Communication Protocol**, [Jon Ide](https://gitlab.com/PracticalOptimism) et al. [https://gitlab.com/PracticalOptimism/oicp](https://gitlab.com/PracticalOptimism/oicp) (planned but not yet implemented) |


##### Philosophy

**John Perry Barlow**
<sup>1</sup> There are historical documents that agree with this stance of global citizenship for each individual regardless of their physical whereabouts such as "[A Declaration of the Independence of Cyberspace](https://gitlab.com/ecorp-org/ecoin/-/blob/master/resources/a-declaration-of-the-independence-of-cyberspace.md)" by [John Perry Barlow](https://en.wikipedia.org/wiki/John_Perry_Barlow)

### License
MIT






