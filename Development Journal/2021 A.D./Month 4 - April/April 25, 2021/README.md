
# Development Journal Entry - April 25, 2021

[Written @ 16:09]

### Notes

`for`, `to` can be interlaced? 'permission rule' and 'event listener' function names for '`add`' are using 'for' and 'to' interchangeably
[Nevermind, I changed my mind . . I'm using '`to`' for both '`event listener`' and '`permission rule`' now . . ]

add-event-listener-to-tally // `to`
add-permission-rule-to-tally // `to`

instead of

add-permission-rule-for-tally // notice `for` is used instead of `to`


[Written @ 15:58]

### Notebook Guide On How To Use The Function Name Mechanics

(1) Get (`Useful` but not always because of aliens)
(2) Create (`Useful` but not always because of aliens)
- (2.1) Add (Alternative Name for compatible language related switching like 'addItem')
- (2.2) Initialize (Alternative Name for language simplification like 'initializeALargeTranscendentalNumberToManyDigits')
- (2.3) Start (Alternative Name for language simplification like 'startComputerProgram')
- (2.4) StartApplying (Alternative Name for language simplification like 'startApplyingZebraPaintToTheBackgroundOfEveryColor')
(3) Update (`Useful` but not always especially for evaluating functions with interesting properties like dimorphism that interrelates without switching back that would be dangerous for any basic computer without fast random access memory provided for by aliens)
- (3.1) Evaluate (Alternative Name for Function Related Usecase)
- (3.2) Enderize (Alternative Name for Ander Related Usecase)
- (3.3) Existentialize (Alternative Name for Never-Going-To-Happen Usecase)
- (3.4) Seminify (Alternative Name for No-Not-In-Your-Life-Time Usecase)
(4) Delete (`Useful` but not always because of aliens)
- (4.1) Remove (Alternative Name for compatible language related switching like 'removeItem')
- (4.2) Uninitialize (Alternative Name for language simplification like 'uninitializeALargeTranscendentalNumberFromMemoryUpToSoManyDigits')
- (4.3) Stop (Alternative Name for language simplification like 'stopComputerProgram')
- (4.4) StopApplying (Alternative Name for language simplification like 'stopApplyingZebraPaintToTheBackgroundOfEveryColor')
- (4.5) Deduplicate (Alternative Name for language simplification like 'copy data in a particular way so you don't loop over the same index in a same way' which is useful for being an anatomical counterpart to 'Evaluate')
(5) Recommend (`Useful` for a directory name)
- (5.1) GetResponseForRecommended (`Useful`)
- (5.2) CreateRequestForRecommended (`Useful`)
- (5.3) UpdateSettingsForRecommended (`Useful`)
- (5.4) DeleteDefaultSettingsForRecommended (`Useful`)
(6) Scale (`Useful` for a directory name)
- (6.1) ElementalizeScale (`Useful`)
- (6.2) DeElementalizeScale (`Useful`)
- (6.3) AmortizeScale (`Useful`) // Normalize Based On Many Variables
- (6.4) GetScaleCoFactor (`Useful`) // GetElementalizedDeElementalizedScaleCoFactor
(7) <'PerformAction'><Period><Age><Rule><Name><Service> (`Extremely Optional`, Use In Dire Circumstances like special occassions when you can't determine what to name the function and want to press escape on your brain to realize 'performAction' was a good prefix with nice ideas like 'the name can be easy for new programmers to read your codebase')
- (7.1) With Prefix (Use with Codebase for consistent prefix-naming)
  - (7.1.1) Example: PerformActionCalledSearch
  - (7.1.2) Example: PerformActionCalledOrganize
  - (7.1.3) Example: PerformActionCalledPress
  - (7.1.4) Example: PerformActionCalledType
  - (7.1.5) Example: PerformActionCalledEvaluate
- (7.2) Without Prefix (Use with User Interfaces for Short-hand names)
  - (7.2.1) Example: Search
  - (7.2.2) Example: Organize
  - (7.2.3) Example: Press
  - (7.2.4) Example: Type
  - (7.2.5) Example: Evaluate


[Written @ 14:53]


[Written @ 14:30]

### An Algebra One-To-One Mapping Of Function Names To Service Items:

(1) Get -> Database, Computer Program Manager
(2) Create -> Network Protocol
(3) Update -> Event Listener
(4) Delete -> Permission Rule
(5) Recommend -> Anana Anana Anana
(6) Scale -> Scale Matrix Amana
(7) <PerformAction><Period><Age><Rule><Name><Service> -> Anana Anana Anana

**`Computer Program Manager` is a generalization of `Database` with** (1) StartDatabaseItem [StartComputerProgramForItem], (2) StopDatabaseItem [StopComputerProgramForItem], (3) InitializeFunctionEvaluationForDatabaseItem, (4) UninitializeFunctionEvaluationForDatabaseItem

**`Database` would possibly normally have the unique functions** (a) CreateDatabaseItem, (b) GetDatabaseItem, (c) UpdateDatabaseItem, (d) DeleteDatabaseItem, (e) CreateDatabaseIndex, (f) DeleteDatabaseIndex, (g) RecommendDatabaseItem, (h) ScaleDatabaseItem

### An Algebra Listing Of Service Items:

(1) Database
(2) Network Protocol
(3) Event Listener
(4) Permission Rule
(5) Computer Program Manager
(6) Scale Matrix Amana
(7) Anana Anana Anana

### An Algebra Listing For Function Names:

(1) Get
(2) Create
- (2.1) Add
- (2.2) Initialize
- (2.3) Start
- (2.4) StartApplying
(3) Update
- (3.1) Evaluate
- (3.2) Enderize
- (3.3) Existentialize
- (3.4) Seminify
(4) Delete
- (4.1) Remove
- (4.2) Uninitialize
- (4.3) Stop
- (4.4) StopApplying
- (4.5) Deduplicate
(5) Recommend
- (5.1) GetResponseForRecommended
- (5.2) CreateRequestForRecommended
- (5.3) UpdateSettingsForRecommended
- (5.4) DeleteDefaultSettingsForRecommended
(6) Scale
- (6.1) ElementalizeScale
- (6.2) DeElementalizeScale
- (6.3) AmortizeScale // Normalize Based On Many Variables
- (6.4) GetScaleCoFactor // GetElementalizedDeElementalizedScaleCoFactor
(7) <'PerformAction'><Period><Age><Rule><Name><Service>
- (7.1) With Prefix (Use with Codebase for consistent prefix-naming)
  - (7.1.1) Example: PerformActionCalledSearch
  - (7.1.2) Example: PerformActionCalledOrganize
  - (7.1.3) Example: PerformActionCalledPress
  - (7.1.4) Example: PerformActionCalledType
  - (7.1.5) Example: PerformActionCalledEvaluate
- (7.2) Without Prefix (Use with User Interfaces for Short-hand names)
  - (7.2.1) Example: Search
  - (7.2.2) Example: Organize
  - (7.2.3) Example: Press
  - (7.2.4) Example: Type
  - (7.2.5) Example: Evaluate


[Written @ 7:33]






