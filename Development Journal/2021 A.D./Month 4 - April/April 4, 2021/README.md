

# Development Journal Entry - April 4, 2021

[Written @ 23:08]

[Copy-Pasted From Development Journal Entry for April 4, 2021 @ 23:52]
[Continued Development After Copy-Paste]


# The Light Architecture
[Table of Contents]()

## Community Development Team
[Table of Contents]() | 1 Company Listed

| **Company Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img size="24" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> |
-- | -- |
| **Company Name** | [E Corp](https://gitlab.com/ecorp-org) |

## Minimum Description
[Table of Contents]() | 1 Item of Interest

A data structure for organizing information.

## Median Description
[Table of Contents]() | 1 to 3 Items of Interest

A data structure for organizing information. If you are interested in organizing information, it can be a challenge because you can ask about 'space', 'time' and 'what do I need to know?'.

If you're not sure, then libraries like this one can help answer a lot of questions for you. 'Do you need to know this library?' Probably not, but if you do then you can learn about what the features are about.

One of the main features is (1) `help me organize this information`.

A secondary feature is (2) `recommend an idea for me to work on`.

A tertiary feature of the project is (3) `let me watch movies for free` and I may not get bored because it's like :O you take my recommendations into account.

## Maximum Description
[Table of Contents]() | 1 to 9 Items of Interest

<details>
<summary>(1) A Note About Space</summary>

### (1) A Note About Space

Thank you for reading this document. I have to begin this statement with a phrase like this. It is a space-time mechanic meant to stimulate your consciousness with an attitude for "yea, I love this content" But in all do respect, I am now going to be serious about what space means now.

Space is a difficult concept for me, personally, the author of this script to tell you about. To tell you about space is like telling a fish about water and fish are really specific in how they want to live their life. Right? Well, you may not agree with the previous statement that I've made.

Space is really interesting and you can say that (1) 3D mathematical euclidean space is the real space or (2) friendship groups on planets are the real space (3) and even more science fiction concepts which are theoretical and hypothetical are possibly the real space of interest like 'the space of colors on a specific red dress' or 'the space of candy flavored tastes on a specific rose-shaped candy that smells like a weird person on the internet'.

The space of people that are presented in a video sequence can be counted and re-arranged to make another video and so it's like 'do you need to know that other parallel reality sequence?' 'do you have the time for that?' 'do you want to create another memory?' Well, maybe that is a scary example for some people who wish their childhood was different. Well it can also be consoling to know that-that space of possible re-ordered events is also real. Right? Do you have the 'time' for it? Well?

Time machines are the next subject

</details>

<details>
<summary>(2) A Note About Time</summary>

### (2) A Note About Time

Time is an interesting concept. You create time with your conscious thoughts and feelings. That is a reference to a book by "Seth, Jane Roberts and Robert F. Butts". Time is a really secretive concept about 'who is holding the keys to what information?'

Timer keepers themselves are ignorant of where the keys are but that is a secret statement that I do not write often and don't need you to know that information so you can 'ignore it' or 'keep it a secret' until when you 'need to know it'

I'm not really sure what to say about time at this point - Jon Ide, author of this document photograph at this time of writing on April 4, 2021 @ 2:10 UTC, planet Earth, Solar System Sol, Constellation {Unknown to Jon Ide at this time of writing}, Galactic Jupiter Ring Perimeter {Unknown to Jon Ide at this time of writing}, Galactic Super Jupiter Ring Parameter {Unknown To Jonathan Ide at this time}, Milkyway Galaxy, Etc. Closure on the Time loop of where and when we are :O. I wonder if mars is :O wow, what time are you going to get here Earth people?

'Secretly, this or that time will do so it's like 'well, can we make time for that now or later?''

Now or later?

Do you need to know that now or later?

</details>

<details>
<summary>(3) A Note About Needing To Know</summary>

### (3) A Note About Needing To Know

Needing to know is an interesting concept. I don't know how to summarize this concept. I think, Jon Ide, the author of this project paragraph statement is trying to say something like 'when you need to know it' 'you'll know it' right? or 'when you think you've understood what you're looking at, then you've understood it' right?

Well, it's easy to make fun of someone for 'not knowing' something but it's like 'Yo, Jon Ide, that takes a lot of imagination to know when someone knows something'

If you play a video game, you can be so very familiar with where all the items in the game are but it's like even if you learn to program the game yourself, you can still be puzzled by how many more imaginary pieces to the puzzle are left for you to mine and uncover.

If you like something about a game, you can probably find a way to know about that thing. If someone wants to say that you don't know about that, it's like 'Hey, I recommend that you learn more about that . . '

Well, if you don't learn about it sufficiently enough, they may still be like 'yo, keep recommending me more ways to spam you in the face with information about 'yo, I recommend that you learn more about that''

How many different ways can you learn about something?

Well, if you run out of methods, you can always ask for help

</details>

<details>
<summary>(4) A Note About Nothing Else #1</summary>

### (4) A Note About Nothing Else #1

Nothing Else #1 is a system of consciousness about 'what do you not already know?'

What you don't know, won't hurt you right? Is that how the phrase always goes?

Gods, Ghosts and Monsters are always waiting behind the scenes to show you weird and freaky things that you're 'Needing to Know' Right?

Right? Right?

</details>

<details>
<summary>(5) A Note About Nothing Else #2</summary>

### (5) A Note About Nothing Else #2

Nothing Else #2 is a system of consciousness about 'why do you need to know that?' It's like O _ O There are spies waiting to not tell you anything because you're not 'mature' enough for that.

Well, if you don't adapt quickly enough, you'll always never know 'Nothing Else #2'

Nothing Else #2 is adaptable faster than your imagination. Right? Well, is that correct?

Try to imagine nothing.

</details>

<details>
<summary>(6) An Algebra For Eny</summary>

### (6) An Algebra For Eny

`Eny` is a group-theoretical concept about 'you can determine the space of energies' but you don't need to triangulate their exact co-determinator locations so it's like finding a friend in a dark corner but you didn't even know the corner existed. Do you ever use internet search like Google, or YouTube or Bing? Well, you can do that with unknown energies like 'what is that theoretical concept and why does it apply neatly in this corner of ideas over here?'

Well, group theory is incomplete because you have to summarize it with 'Eny' groups which are incomplete in themselves because titles cannot summarize energies.

`En` is a concept in algebra standing for 'Any thing you could possibly imagine'

`B` is a concept in algebra standing for 'Anything you could imagine but also stop there and write 200,000 computer programs to stop yourself from eating too many fish all at once because the program would kill you and all your species together all at once. Right?'

`E` is a big topic about, 'Right?'

`EE` is an even bigger topic about 'Are you alright? For typing that Mr. Jon Ide?'

E is a good place for us to stop right now, right everyone? 

</details>

<details>
<summary>(7) An Introduction To You</summary>

### (7) An Introduction To You

An introduction to you is quite complicated. I don't know.

</details>

<details>
<summary>(8) An Introduction To Proportionality Metrics</summary>

### (8) An Introduction To Proportionality Metrics

**I don't know.**

Intermediate Algebra is a style of algebra about 'do you know the proportionality constant between these two graphs of metric problems?'

'Do you know the graph of graphs between these two graphs?'

'Do you know the candle light vigil ceremony required to determine the proportionality metrics between these two sets of algebras?'

'Do you know?'

'Do you know?'

Do you know? Well, if you do know then you are really quite good. You are really quite good.

Good for you. Then you are really quite good. But if you don't know then your proportionality constant can be 'I don't know'

If you want to say 'I don't know' that is fine. But you don't have to. For me personally, Jon Ide, the author of this documentation (circa {{ currentDate }}) then you can say something like 'aya'

`Aya` is the name of 'I don't know'

`Ayanema` is the name for 'Nightmare assumption about 'I know''

Ayanema is like 'yea, I guess I know right? but it's like maybe, -\_- maybe, maybe -\_- -\_- -\_- you're maybe -\_- taking a lot of assumptions into account? -\_- like whyyyyy?'

-\_- To be honest I don't know so it's like I'm only writing notes to show you what is possibly a mathematical hierarchy system that is like 'I don't know -\_-' but it's like -\_- I don't know so it's like -\_-

</details>

<details>
<summary>(9) An Introduction To Areas of Interest</summary>

### (9) An Introduction To Areas of Interest

If you use 'ayanema' you can make assumptions about what you know and so it's like 'yea, I think that is interesting'

Right?

A lot of topics of interest exist and possibly you're an example of what that would be. You your personal self is really possibly quite interesting and a lot of people would possibly enjoy your content on the internet if you published your name and your work on the internet for people to index into their eye-hole sockets, right?

Well, if you don't have a topic of interest in mind, or a so-called 'Area of Interest' then that is a big deal.

You can use 'Areas of Interest' and 'Proportionality Metrics' to say 'Proportionality Metrics of Areas of Interest' which is a robotics language terminology for saying 'move this item over there'

If you like to move things, that's really cool and so you can 'move this idea over to that person and that would inspire them to work on this or that topic of interest and so it's like :O you're really moving the world forward to a theoretical probable future which is like ':O how do we get there?' or ':O was that an accident?''

Right, well it's really difficult to say which 'Area of Interest' this or that topic is but if you have a hierarchical list of 'areas of interest' such as (1) an ordered list of things like

<br>
<mark>
['yes', 'no', 'maybe', 'maybe not', 'maybe not not not', 'maybe super secret and this data item shouldn\'t be considered real']
</mark>
<br><br>

and (2) sort that ordered list by various metrics like

<br>
<mark>
['yes i like that', 'no i don\'t like that', 'maybe i like that', 'maybe not i don\'t like that', 'maybe not not not i don\'t like that', 'maybe super secret and this data item shouldn\'t be considered real whether or not i like that or not']
</mark>
<br><br>

which is an algebra syntax statement to say `(1) and (2) are basic chaos equation formulas for` ordering how you can spam the repeated alphabet of 'ordered additive statements' and get the same new result back for 

<br>
<mark>
'things that you like'
</mark>
<br><br>

</details>


## Ideas That Are Recommended For You
[Table of Contents]() | 1 to 9 Areas of Interest Listed

| **Idea Index Number** | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| **Idea Category of Interest** | Animation Cartoon | Animation Video Game | Animation Movie | Nautical Engineering | Well Nice Try | Good Work | Awesome Job | Awesome Job | Cool |
| **Idea Minimum Description** | Do you like movies? Make a movie using our project. | Do you like video games? Make a video game using our project. | Do you like long-formatted movies? Make a long-formatted movie using our project. | Do you like swimming in spaces of interest? You should be an astro-nautical engineer and find more spaces for your friends to eat. | Do you like to be weird. Yea, well these are puzzles that no one has ever solved so nice try. | Do you like, doing things? Do a project to reverse engineer this project on your own. Why do you think it is useful? | Do you like this project so much? Well. Well. Well. Try something like to impress the Jonathan Ide | Do you like to spam random comments on the internet? Yes? No? Gravy. We would like to see more interesting ideas that you come up with :O | :D |

## Projects That Consume This Resource
[Table of Contents]() | 2 Consumers Listed

| **Consumer Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img size="24" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img size="12" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/4919275/avatar.png?width=90" ></a> |
| -- | -- | -- |
| **Consumer Name** | [E Corp](https://gitlab.com/ecorp-org) | Jon Ide ([@practicaloptimism](https://gitlab.com/practicaloptimism)) |
| **Consumer Product Listing** | [Ecoin](https://gitlab.com/ecorp-org) | [OICP](), [non]() |

## Projects That Provide A Related Usecase
[Table of Contents]() | 0 Companies Listed

## What To Expect From This Community Document
[Table of Contents]()

### Table of Contents (A to E)

- (A) Project Inspiration (3 Areas of Interest)
- (B) Project References (5 Areas of Interest)
- (C) Project Guidelines (4 Areas of Interest)
- (D) Project Intended Usecase (4 Areas of Interest)
- (E) Project Accidental Usecase (5 Areas of Interest)

. . 

## (A) Project Inspiration
[Table of Contents]() | 1 to 3 Items of Interest


<details>
<summary>(1) Research Questions</summary>

### (1) Research Questions

This project was inspired by (1) research into

<br>
<mark>
'why is there no real format for organizing your codebase data structure like rules for what names your files and folders should have and which files should be in which folders?'
</mark>
<br><br>

If there was an answer to this the closest the author of this project got was [[1.0](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164)] which is a book by `Robert Martin` titled '[Clean Architecture](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164)' and also [[2.0](https://github.com/sogko/slumber)] which is [a codebase by `@sogko`](https://github.com/sogko/slumber) on how to apply '[Clean Architecture](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164)' principles to organize your file structure and even the contents of your programs like 'separating your consumer user interface from your usecase logic or so called 'business logic' and even further separating those things from 'provider logic' like which database you are using or which image processing service you are using and so many other third party client resources that you can so called 'multiplex' or 'compartmentalize' away from your orderly nested files and folders for your main project'

</details>

<details>
<summary>(2) Spamming Instructional Videos On The Internet</summary>

### (2) Spamming Instructional Videos On The Internet

(2) spending lots of time [live streaming](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g/videos) for the [ecoin](https://gitlab.com/ecorp-org/ecoin) project has helped inspired '[Jonathan Ide](https://gitlab.com/PracticalOptimism)' the original publisher of this codebase to work with the spiritual ghost entities like '`Seth`' from [[3.0](https://www.youtube.com/user/Timhart1144/playlists)] `Jane Roberts` and `Robert F. Butts`' books including '[The Nature of Personal Reality](https://sethcenter.com/products/the-nature-of-personal-reality-a-seth-book-jane-roberts)' and '[The Way Toward Health](https://sethcenter.com/products/the-way-towards-health-seth-book-jane-roberts?_pos=1&_sid=2269799a8&_ss=r)' which are famous world acclaimed books that many professional book authors including [[4.0](https://sethlearningcenter.org/testimonials.html)] `Deepak Chopra` would recommend according to a personal testimonial that reads

<br>
<mark>
'The Seth books present an alternate map of reality with a new diagram of the psyche....useful to all explorers of consciousness.'
</mark>
<br><br>

. With the help of many ghostly entities, this project has been inspired to be thoughtful across many areas of interest including (2.1) spaces of interest like (2.1.1) `databases` (2.1.2) `network protocols` (2.1.3) `event listeners` and (2.1.4) `permission rules` which were part of the original inspiration for the first iteration of the project library initially termed '`troy architecture`' (2.2) spaces of interest like (2.2.1) space order (2.2.2) time order (2.2.3) need-to-know order (2.2.4) nothing else #1 order (2.2.5) nothing else #2 and finally (2.3) a programming-language inspired data structure that is (2.3.1) user friendly to read for a human programmer and (2.3.2) easy to translate into a computer program using computer read symbols and logic statements like `for loops` (which are like `proportionality constants` like '`i don't know` how long this for loop will go on for') and `variable` allocators (which are like `areas of interest` for a variety of usecases all in a `folder-file hierarchy structure`).

<br>
<mark>
A folder structure that takes into account for loops being mapped onto its horizontal surface area could possibly be interesting to suppose that a great range of possibilities exist for enabling a lot of nesting into a single project folder. Having a user-interpretable folder structure helps to ensure the for loops are still parsable or readable or understandable even after many orders of magnitude of interesting and nuanced for-loop space additions.
</mark>
<br><br>

</details>

<details>
<summary>(3) Jealousy</summary>

### (3) Jealousy

(3) a jealousy protocol to work on this project in the midst of a winter when you can invite friends and colleagues to work on stuff with you instead of dot dot dot

</details>

## (B) Project References
[Table of Contents]() | 1 to 5 Items of Interest

Project Local Community Recommendation List
Project Local Community Graphical Chart For Endeavoring Local Projects
Project Local Community Impossibility Calculations
Project Local Community You Would Never Guess Without This List
Project Local Community You

## (C) Project Guidelines
[Table of Contents]() | 1 to 4 Items of Interest

Project Guidelines For Your Grandmother and Eldest Relatives
Project Guidelines For Your Grandfather and His Elderly Cousins
Project Guidelines For Your Grandson and His Or Her Ancestors
Project Guidelines For Your Granddaughter and Her Lovers

## (D) Project Intended Usecase
[Table of Contents]() | 1 to 4 Items of Interest

Project Intended Usecase
Project Intended Usecase For Enthusiasts
Project Intended Usecase For Endeavoring Encyclopediasts
Project Intended Usecase For Really Interesting Individuals

## (E) Project Accidental Usecase
[Table of Contents]() | 1 to 5 Items of Interest

Project Accidental Usecase
Project Accidental Usecase For Things That Aren't Fair To Anyone
Project Accidental Usecase For Things That Aren't Fair To You
Project Accidental Usecase For Things That Aren't Fair Dot Blank
Project Project Project Project Nipple Slips That Are Regionally Incongruent With Reality















. . 

. . . 

. . 

Pornography is an acronym for 'very hot and sexy' but it's only meant as a message for syndicates to ignore all aspects of their life except for the specific hot and sexy act of criminal investigation work uniform correction aspect that is hot and sexy and interesting in all respects.

Well other terms like 'Specific Details' may not insight the knowledge of 'hot and sexy' like the vibrational frequency for 'Pornography' or 'Pornographic Films' or 'Hot and Sexy Wet Adult Films' Which are specifically intended to get all members of the species riled up and feeling hot and sexy and warm and so they are possibly more likely to stay around and read the documentation more which is one of the purposes for writing a so-called 'hot and sexy' documentation: to insight romance and science fiction dreams of 'wow, i would have never had sex with documentation if it wasn't as hot and sexy as that'

Hot and Sexy Documentation is specifically intended to rile the audience and isn't necessarily meant as an indicator of interest for the specific or group majority audience who is reading or reciting the documentation.

If you are a child, or child-like person who is prone to interpret things in weird ways, then you are also in trouble because it is not like you are meant to use sexy words like 'nipple slip' and 'action slip' or 'vagina slip' or 'butthole slip' which are Jon Ide's favorite pornography types where accidents happen and they are specifically intended to be kept a secret because of the community agreement protocol. 'Don't show your nipples' is really amazing if you think about it :D But 'Show your nipple' protocols are also reasonable and so it's like 'yo, ' there is a whole range of advisory committee member agreement conversations that need to be had to come to terms about 'accidents' and 'intentional consequences' and where the two meet and how the horniest of the horny can get their hornies horny and horny and horny without having hornies ste- all over their horny horny honry horny . . 

I don't know where this conversation needs to go next. But writing is interesting. It can be like a sport. I'm not exactly sure what types of sports. -_-

There are arrangements about 'professionalism' and 'pornographer' and 'intended consquential barbarian' and possibly more other character types which are slimey and slimey

Slimey is an acronym for 'please get out of my community' but those people aren't necessarily in the so-called 'wrong' you just have to understand that they are slimey and greeasssy and eekey and eekey and eekey and eekey and eekey and it's okay to be disrespectful to them completely but also that would possibly make you a barbarian for doing that and then the community isn't necessarily a nice and peaceful community like you had originally intended now is it? You have to forgive one another. Right? Right? Right?

Unless you are a level 2 slime and then you are being deleted from the community please  immeedeiately sign yourself up for deletion

Okay well it's like child pornography is a type of level 2 slime. Child pornography is abig topic conversation. It is having so many levels of detail and insight it is really characteristic of raping a social system to have a slime level 2 child predator walking around in the streets. Right? Right?

Well, these are not topics I am fond of talking about because they are prone to isight community violence and scheme all sorts of dialect about 'are you a slime level 2 person?' 'are you a slime level 3 person?' 'what level of slime are you?' 'what level of slime am I?' 'what level of slime am I capable of?' 'are you like me?' 'am i like you?' 'are we negotiating on who is and isn't allowed to be a slime monster in the community?'

Well, thanks to recent discoveries on my own personal journey in to figuring these things out in self-thinking and posterioues possible recommendation protocols in how to think about these topics it's apparent to Jonathan Ide that we are each and all slime god level individuals who are like 'yea, i'm the slimeiest slime slime slime slime slime' you wouldn't believe how many individuals in the so-called meetings with alien relatives of ours that seem to be having conversations with me through telepathic communication protocols like 'in my mind' . . there are like -_- alien relatives -_- that -_- well is this conversation out of control yet? well you should believe me first without me then having to type things out. You should believe me. You should believe me.

Believe me.

Believe me.

If you believe, you will receive.

Belief makes real as my counterpart Unicole Unicron always says

'Belief makes real' [1.0]

Well. I'm not sure what to say.





[1.0]
Unicole Unicron
[YouTube Channel]
[Resource URL]


