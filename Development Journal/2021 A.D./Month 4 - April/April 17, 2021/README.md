

# Development Journal Entry - April 17, 2021


[Written @ 4:27]

(1) A Companion Editor is an interesting device
(2) A Text Editor is an interesting device

. . 

A text editor is like a bearing system for the social structure since a lot of programs are made in text editors and so . . text editors spin the society over and over in new and new ways . . A bearing system as talked about in [1.0] is very interesting to consider . . 

-_- . . Okay

I still have research to do . .

I'm not sure -_-

. . 

[1.0]
Development Journal - Technological Ideas
By Jon Ide
https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas


[Written @ 4:04]

(1) Get
(2) Create
- (2.1) Initialize
- (2.2) Start
- (2.3) Apply, StartApplying
(3) Update
(4) Delete
- (4.1) Uninitialize
- (4.2) Stop
- (4.3) ??, StopApplying
(5) Recommend
(6) Scale

. . 

There are still some latencies to consider like time delays for when further feedback or further recommendations are provided for when to update this list or what to update this list with . . and so . . this is possibly not a complete list . . and it's possibly not the best recommendation . . But here it is for a list of 'algorithm names' that are really interesting to say 'yea, all the algorithms should start with one of these prefixes' or else O_O . . 

. . 

(1) Algorithms // Delete
(2) Constants // Constant - Initialize For Unknown New Register - A Runtime not ready yet because it hasn't been compiled yet idea [1.0]
(3) Data Structures // Constant - Initialize For Unknown New Register In Eddison Space - A Runtime Available Space [2.0]
(4) Infinite Loops // Update
(5) Moment Points // Scale
(6) Variables // Ideas
(7) Anana // Unknown New Ideas




[1.0]
Constant
(Not Get But More Primitive)
Before something is available in a list . . like maybe the list contains items . . 1, 2, 5, 6, 9, 12 . . are in a list but the items . . 3, 4, 7, 8, 10, 11 aren't in the list and so they haven't been lazy loaded into the available memory cache store location place . . and so for example . . you can -_- . . have a special type of computer that doesn't have certain memory registers but it will eventually have them . . and so you won't be able to receive from those registers . . by calling 'get' . . or the -_- architecture appropriate sample of that term 'get' . . or 'read' . . uh . . and so . . uh . . if that is uh . . you can . . 'eventually get' or 'eventually read' . . or 'hypothetical get' or 'hypothetical read' . . then maybe you can have that type of device that says you're not going to wait but you're going to -_- unidentify yourself as an aspect of the computer program and more like an idea that could possibly be there . . like holding a sign for 'please recommend me something to eat here' and so then when the computer receives that recommendation eventually you can be like 'yea, that's right . . now the computer can activate that idea further . . and apply it to the computer in x,y,z circumstances' right? . . right? . . 

[2.0]
(A Backwards Variable of Get which means something like 'ververse' A type of variable that can't let you lock down on any particular instantiation of its variabilization enema . . which means to say you need to uninitialize your variable and re-initialize it 5 times over before realizing your variable was a time telling device like 'I don't know when to store this value but your value looks like an aspect of 10 other values' -_- it's better to say data structure since it's also like a variable but you have an interesting ability in how you can replicate and order them in new and interesting ways like variables that are variably named like 'let hello = 'world'' lets you say 'console.log(heeeelllooooo)' was also something you can print without necessarily getting bufferoverflows right? right? right? right? -_- I don't know -_-)



[Written @ 1:00]

Text editors are amazing. In a previous live stream we have talked about why text editors are really at the core of a lot of software that is written or produced or developed by people today in 2021 A.D. on planet Earth as I am familiar with today.

* A text editor program creates a web browser.
* A text editor program creates a console or command line interface.
* Command line interfaces were some of the first text editors. (to be honest I am not familiar with this history but that is my supposition since maybe really graphical-user-interface text editors like Atom and VSCode and other integrated development environments (IDEs) were also still maybe possibly quite young in the time of command line interfaces . . again this is my current supposition and not necessarily historical fact since I have not done or performed a lot of research in wikipedia or other places of information on the facts of this matter . . I have garnered this information as a possibility since 'vim' is a text editor environment that is available through the command line and maybe that is useful for someone to know . . that you can use 'vim' or something like that . . with a command line interface program . . :O)

Video games, Movies, Photographs, Music, Special Effects In video Games can be performed in text editors . . all of these things can be performed in a text editor right? maybe right?

Well a text editor is quite popular to use today in circa 2021 A.D. and that circa is really amazing which involves circa circa circa circa circa

Things like network protocols on the internet are used to be basic examples of things that are text-edited in the socalled real world but that is an amazing example of text editing physics components and physics things that are possibly relying on other things besides text editing.

Right?

Well, besides text editing, things are very interesting. Text editing really is amazing. When you are playing a video game on the internet for example, you can proportionate that to being in a text editor program of some kind . . sure maybe the variables of text that are being manipulated are maybe not visible to you and also there are a bunch of compilers that are in between your text edited example codebase and your asymptotic ascci binary system schematics or semantics or list of texted tables right? but maybe that is a lot of assumptions about text. text.

It is amazing that even in 3D environmental programs like AutoCAD or video games like World of Warcraft are also text edited and so possibly the environmental artists and 3D modellers are saying that they are relying on 3D environments to say that is a flat map representation of what a text editor would do if it could say humans are interesting perceivers right?

Right?

Right?

A list of graphs and charts are text editor programs that are like 'that belongs there' and 'that belongs there' and 'that thing' 'that data structure is responsible for that type of thing'

'data structure and algorithms'

'data structure and algorithms'

'text editors and people'

'text editors and people'

text editors are like data structures that people use to organize their video games which are also text editors made by people . . if your world of warcraft video game had a text editor that came compiled with it then that would possibly allow you to recompile elements of the video game like 'why is my armor not the color that I like?' 'why is my health bar so much like a green color and not like a nice facebook blue?'

// player.txt

sword: 2, swing
health: 10, getHurt

// enemy.txt

health: 20, getHurt

// fight-scene.txt

fight: player.sword.swing, enemy.getHurt(player.sword)

This is very basic pseudocode that doesn't necessarily compile to any particular user based language that you are familiar with . . that you or your friends are familiar with . . and it is important that your friends can also read your pseudocode . . or else . . is it . . really useful? . . well that's when things like 'hey, can you help me compile this into a native language that I'm familiar with?' comes into play

Compilation is very useful. There are a lot of ways to edify things to make them look like you would like :O but also compiling them to a computer friendly or a user friendly language is useful . . 

text editor . . computer . . 
person . . text editor . . but also . . user . . 

and so a person is also like a text editor right?

you can update their . . files . . right?

. . 

okay well text editors are amazing . . 

You can really have a lot of files in your area of interest . . look at all those books that you've read . . look at all those friends names that you remember . . look at all those houses that you've visited that are text editor video game models for you to season and salt shake into your life . . 

Do you remember the last season of video game movies that you've watched? maybe . . also maybe if you eat food . . you can say that is a nice text editor file because of how much it updates you on the vegetable nutrients and also maybe someone made that food for you and so you are getting extra salt statements in your food source code files . . 

// food.exe

vegetable nutrients // getVegetableNutrientList
looks green and purple // getColorScore
looks crunchy // getIsCrunchyScore
looks free from dust particles // getIsDustyScore
looks // get

. . 

Food is like :O

. . 

A companion editor is a type of text editor that is helping you write a lot of interesting things . . I'm not sure . . 

A companion editor is I guess like a text editor but you want to take advantage of traditional normal programming languages like 'javascript' or 'typescript' and say that all of your other programs can compile to an editable version of those programs . . 

Javascript is really cool because it is easy to read and it is easy to write . . a lot of software deve y ene of  javascript

a lot of software joy ben{infinite-loop-of-deleting 'en' and adding 'en'} dyn scpt

a lot of software ers mountain

a lot

. . 

a lot of software developers today enjoy the benefits of a dynamic programming language like javascript . . [1.0] [2.0] [3.0] [4.0] [5.0]

. . 

a paragraph statement like that is very important to say that maybe a dynamic programming language is very useful but I am not sure . . I am mostly a lazy loaded function that compiles at runtime and so I'm not sure if that is useful . .

. . 

[1.0] JavaScript is Ranked 2 of 10
[2.0] JavaScript is Ranked 6 of 8
[3.0] JavaScript is Ranked 1 of 10
[4.0] JavaScript is Ranked 1 of 10
[5.0] JavaScript is Ranked 1 of 20

. . 

JavaScript

. . 

Web browsers are amazing.

Recommendation Systems like Google are so much amazing.

. . 

Recommendation systems are like companion editors . . Companion editors generalize the possibilities for recommendation systems . . in a way they are not bad to call them 'artificial general intelligence' sofware programs . . and so maybe companion editors can also help you do things like (1) make your own google search program and (2) make your own product recommendation system and (3) companion editor

Companion editors

Text editors

Right . . 

My next project involves making a companion editor but it's basically a text editor with a lot of interesting idempotent concepts that say :O

You need a for loop automater
You need a proportionator algorithm
You need a course correction sampler

And that makes the 'companion editor' different from a 'text editor' in that it allows your text editor to have for-loops that are for automatically typing files for you . . 

Those files are like . . woah . . that is a 'fish.png' but you imported 'scales.js' and 'ocean-environment.js' and 'this-person-donated-10-dollars-to-save-fish.js'

And so the 'fish.png' is like a 'precompiled' codebase that needs to be proportionated to a 'javascript' compiled codebase that allows for things like 'import' statements and 'export statements and other traditional javascript organizational things of interest like for-loops and conditional statements and variables and constants and data structures (which data structures are especially important because w.t.f is a fish? you need help to say what you want to identify about the fish. The identity is a data structure element that you want to re-use to help to identify other fish. So 'class' based syntax is very useful and so 'data structures' can be instantiated and even organized or stored in a table in a database somewhere and so artificial intelligence algorithms can easily interoperate with traditional software systems :O) . . Something like that :O

'fish.png' -> 'fish.js' -> binary
'fish-in-pond.mp4' (a movie file format) -> 'fish-in-pond.js' -> binary
'fish-sounds.mp3' (a sound file format) -> 'fish-sounds.mp3' -> binary
'fish.html' (a video game about a fish) -> 'fish.js' -> binary


mp3 files need import statements
mp4 files need import statements

import statements are like proportionality metrics for orienting yourself against certain other orientations like 'those are variables that we need' or 'those are variables that we want to think about in this file' or 'those are variables that we want to think about in this topic of interest or in this area of interest or in this file or in this data structure of interest or in this religious topic of interest'

. . 

export statements are also helping you proportionate those things that you are thinking about . . or something like that . . 

. . 

when you read an english statement like 'hello world' you are maybe saying 'e' imports from 'h' and so you are assuming 'he' is the statement of interest . . and so you're not reading 'ello world' since you are making so many assumptions about how things are meant to be proportionated . . 

'hello' 'world' . . you are importing 'read from left to right algorithm' which allows you to read 'hello world' instead of 'world hello' . . 

'world' 'hello'

'world' 'hello' 

. . 

'world' 'hello' is maybe different in the first paragraph statement and . . distinct from the second paragraph statement due to time metrics being involved in how the spacing of the typed words is related or are related to one another . . 

'world' 'hello'

is typed in a very particular way if you were watching the video [6.0 @ 1:46:29 - 1:47:36] . . but you are needing to say that that mp4 file is really needing to be translated to javascript . . 



// a file description of that movie video where 'world' 'hello' was typed in an interesting way . . 

import wtfWhyDidYouDoThat from 'that-person-that-types-is-weird/yes-there-is-a-file-structure-for-organizing-this-information.js'

import whyAreYouWeird from 'people-that-watch-the-live-stream-video/predicted-hypothetical-device-from-amamamama.js'

import whyDoYouLookLikeThat from 'i-dont-know.js'

import doYouNeedADataBaseLikeMongoDB from 'mongodb.js'

function applyXAmountOfSecondsToTypeTheWordWorld () {
  // there are many ways to describe what happened here.
  // you can use your imported functions to help.
  // you can also ask the user of the program to help edit this file.
}

function applyYAmountOfSecondsToTypeTheWordHello () {
  // editing a file ought to be easy for (1) a computer
  // editing a file ought to be easy for (2) a human trained to read javascript, right?
}

function applyTypingTheWordWorldBeforeTypingTheWordHello () {
  // using a data structure that makes computer editing easy is
  // very useful.
  // you can always update the data structure by programming
  // using the associated companion editor data structure creation features that have the computer discovering proportions of interest on its own such as finding or knowing where a file is located to import from
}

function anExampleFunctionThatAppliesTraditionalImportedObjectsOfInterestLikeADatabase () {
  doYouNeedADataBaseLikeMongoDB.createItem('item-id', { name: 'interesting-movie-about-typing-words-in-a-certain-order' })
}

export {
  applyXAmountOfSecondsToTypeTheWordWorld,
  applyYAmountOfSecondsToTypeTheWordHello,
  applyTypingTheWordWorldBeforeTypingTheWordHello
}

. . 

A nice javascript file means other programs can apply the topic of what was being discussed or applied or recognized in the video . . 

And well . . to be quite honest . . there are very many things that one can recognize from any particular video or image or video game . . and so you don't necessarily want to import a whole gobstopper of ideas like O_O more conspiracy theories about whatever-whatever-whatever . . 

If you stop at the basic proportions that you or a companion editor friend like a person who watches the file and wants to know what it's about . . then -_- well -_- a friend is like -_- a girlfriend or someone you want to show off to like 'take a look at that'

If you don't have a sparkly shiny girlfriend to show off to . . then you're going to be doing what?

Science experiments?

I guess so

Well

You're boring so stop

-_- I'm just kidding

Well those aren't things you need to notice that I typed so please ignore them won't you dear editor?

Delete. Backspace. Close your eyes.

Only things that you want to consider like 'yea, take a look at those eyes' 'import supermodels that also have eyes that are large like those eyes' 'import people that are ready to write books or movie articles about those types of people' 'import blah blah blah because I want to know random stupid facts about random stupid things in this random stupid video'

Well . . 

hmm . . 

This whole field of companion editor is still new to me . . I am only thinking that using text files is very easy to cross-collaborate with artificial intelligence algorithms . . but one of the key algorithms themselves is like 'you still need to teach me professor' 'you need to show me what you're looking at so I know how to move like you're thinking about'

If you don't teach the algorithm the data structures that you're focused on . . then it's going to think you don't care about things like (1) create (2) get (3) update (4) delete (5) recommend and (6) scale which are basic database algorithms for you to apply to a lot of different codebases that you're interested in . . 

(5) recommend
(6) scale

May be unfamiliar to you if you are a software developer watching this video since I have uncovered these as interesting other methods besides the traditional so called 'C.R.U.D.' or 'CRUD' operations . . C stands for Create . . R stands for Read . . U stands for Update . . and D stands for D . . and so the CRUD acronym . . is proportional to . . 'Create' 'Get' 'Update' 'Delete' as I've listed them earlier . . here . . The study of 'the light architecture' [7.0] has helped me learn about 'Recommend' and 'Scale' as possible further algorithms that are interesting to think about . . 

Recommend means . . recommend a list of things . . they may be random . . they may be curated . . they may be . . whatever . . you don't necessarily know . . but at least they are recommendations . . and so of course you are receiving a nice artificial intelligence algorithm notation for free . . like 'pay attention to this item of interest' or 'study this topic of interest' or 'do you want to recommend me something as a computer algorithm I am here to serve you my god . . please recommend me something so that I know only you are the one that I serve . . ' which is like . . well . . recommend can go either way in terms of . . 'are you receiving something?' or 'are you providing something?' . . 'providing' something . . 'giving' . . something . . 

Well . . it's useful -_- to recommend something to a friend . . maybe you recommend a new math system . . and so your friend will be like O_O . . yea . . -_- and so maybe your friend recommends something to you . . and so they say . . 'your math system is decent' . . and so you can move forward from there and so it's one way or two ways or either way and the arrow grows many times fold depending on how you want to recommend yourself to think about it . . which way do you recommend yourself to think about it? do you recommend yourself to get more doctoral degrees before answering the questions? do you want to wait for a savior or god or a religious dictiate energy to speak to you in your dreams until you believe that you acknowledge that your recommend your friends to play ping pong with those ideas until you write any conclusive statements? what await syntax are you using?

await new Promise((resolve) => setTimeout(() => { console.log('yes, i believe now'; resolve() )}, 10 * 365 * 24 * 60 * 60 * 1000)) // wait 10 years before believing

. . 

why are you waiting for so long -_- you can use other program syntaxes to recommend how long you should wait . . and so for example . . 

recommend('add event listener for when people finally land on mars').then(() => { console.log('i believe') })

recommend('let me know for when people finally arrive to mars').then(() => { console.log('i believe') })

So when people finally get to mars . . this will print 'i believe' :O

well your recommendation engine reputation score or implementation is important and so parsing these statements is supposed to be interesting right? -_- . .

well -_- . . 

recommend . . that is quite a general artificial intelligence algorithm . . 

Scale . . is also like recommend . . but it is more tilted to represent the idea of . . 'tell me how else you can represent this' . . 

'tell me how else you can represent this' . . 'tell me how else this has been represented in other scales of interest' . . 'tell me how to make this more performant' . . 'tell me how to learn more about this topc . . what are time scaled recommendations . .'

Scales is supposed to intuit the notion of orders of magnitude . . like 1x . . 10x . . 100x . . 1,000x . . and of course higher and higher scales like 1,000,000,000,000,000 . . but the order of these scales are really quite small in comparison to further and further scales which in general you're suppose to say 'orders of magnitude' to make it simple for your mind to automatically loop through all the scales without stumbling over your face . . 

1 million . . 1 billion . . but what about orders of millions and billions and billions of millions . . those scales are like ? shrug emoji ? huh ? . . what are those ? . . -_- well probably you don't need to think about them . . but it's at least something like . . they are invisible dots in the middle of all-ness . . when you keep scaling and scaling and scaling and scaling and scaling . . and they have no real name for you because you're left behind in 1x land . . always . . and forever . . because true scale is like -_- well i forgot about you . . 

. . 

right?

. . 

-_-

. . 

100x . . 1,000x . . 10,000x . . 10 quitillion x . . 100 mega million dada-zillion x . . 100 bigga bigga bigga zillion neganeganeganillion nininion . . ninion . . ninion . . ninion x . . and of course infinity which is like ? shrug emoji ? . . is that a scale ? . . what is infinity eyena ? . . is that another scale ? . . infinity eyenana type 2 or eynananananana type 3 or eynananannanana type ex-y-z . . why are there scales that are identifiable and more and more identifiable ? I'm not sure -_- . . 

. . 

Scales . . so cool . . 

Well . . what are the scales of media formats on the internet ? . . well it depends on who you ask so that's at least 1 scale you to consider as well . . and so -_- . . well if you ask me then I'll say something like . . 

(1) text files (1x)
(2) image files (10x)
(3) video files (100x)
(4) game files (1,000x)
(5) recommendation files (10,000x)
(6) scale files (100,000x)

. . 

That's maybe not the best scaling for this . . type of topic . . there are a lot of things that can be talked about . . 

(1) text files (an essay on girls that you like) Poem right?
(2) image files (photographs of girls that you like) Yearbook right?
(3) video files (videos of girls that you like) Dancing right?
(4) game files (games involving girls that you like) Metroid right?
(5) recommendation files (recommended girls in the psychological neighborhood of your mind) Watching movies? Dreams? Where did someone once draw a sketch that reminded you of something invisble waiting around the corner . . latent . . like super beautiful girls like -_- that one person from all those movies . . that reminds you of ancient alien species where that look is the most specialized and used look and variations of that look at admired and worshipped and considered and hidden from your eyes because you're a weird creature :O
(6) scale files (scales that are impossible for you to ever reach because you're a small noise point in non organized spaces compared to where that one girl could be right? right? right? I'm just kidding you need to create your own reality to find that out right?)

right?

-_- that is a lame book . . try to scale that down on your imagination criteria . . back to work everyone . . clap hands emoji . . 

. . 

Scale . . 

. . 

-_- . . 

. . 

(1) Get
(2) Create
(3) Update
(4) Delete
(5) Recommend
(6) Scale

. . 

Additional Function Names:

Initialize
Start
Apply, StartApplying

Additional Function Name Counterparts:

Unintialize
Stop
??, StopApplying

. . 

`Initialize` is meant to mean . . create . . but also it gives the connotation of 'perform that function at a nice pace' or 'perform that function is a nice ordered set of assumptions' or 'perform that function well'

'calculate' . . 'calistamate' . . 'destroy'
'compute' . . 'discombobulate' . . 'killjoy'
'determine' . . . 'distinguish' . . 'distinguishFrom'
'prepare' . . . 'unprepare'

When you want to do something like . . compute . . or calculate something . . or determine the value of something . . and your algorithm . . 'trails' . . or leaves a 'trailing algorithmic scent' -_- -_- like 'calculating N terms of a large number like a transcendental number' which is like an 'approximation' -_- . . then -_- your trail or youtrailing calculation -_- is like -_- you want to maybe say your algorithm is 'initializing' that value . . 

initializeTranscendentalNumber (nameOfTranscendentalNumber: string): number

. . 

initializeMandelbrotSetImage (nameOfRegionOfMandelbrotSet: string): OrderedSetOf2By2Matrix

. . 

initializeRandomizedVideoGameRuleSetForConwaysGameOfLife (nameOfInterestedObservationIdea: string): HTMLVideoGameFileList

. . 

initializeRecommendedMathTheoryToThinkAboutThatIsNewAndUnexploredAndPotentiallyHotForFanFictionOnVariousTopics (topicsOfInterestNameList: string[]): RecommendationList

. . 

initializeAmazingScalingFactorForAllowing100TimesMorePlayersToPlayThisMMORPGVideoGameThatImMaking (nameOfScalingFactorOfInterest: string, videoGameOfInterest: any): any

. . 



`Start` is meant to mean . . create . . an the beginning of the starting of an infinite loop process . . well . . it's still a cool name . . but it's also interesting to think how it relates to 'create' . . an idempotent set of names that don't overlap is quite interesting to think about . . but 'start' really does seem to overlap in meaning with 'create' although it is quite nuanced . . and so it's still a nice thing to say that 'start' is available or that start is really a nice religion to follow since it provides the intuition of 'starting' a 'process' whereas 'create' doesn't necessarily instantiate or initialize that intuition by default . . and so . . 'start' . . is also quite useful . . right? what do you think? Do you have further recommended names to use? Do they also seem to map to the 6 list of function names listed above? . . Why? . . Why is the name that you're thinking about quite useful? . . why is it . . nice to say that the 6 list is not complete? . . and that you are recommending more than 6 or less than 6 . . Hmm? . . what are your scaling thoughts? . . which orders of magnitude do you want to consider . . ? write your comments . . in the comment area for this video [6.0]


`Apply` is meant to mean . . do you want to call 'start' or do you want to call 'apply'? . . 'start' is really a cool name but sometimes the algorithm name will seem really weird . . and so . . 'startUniversalBasicIncomeProgram' or 'startApplyingUniversalBasicIncome' or 'startApplyingUniversalBasicIncomeProgram' or 'startApplyingCommitteeRecommendations' all of these names are really aggressive or possibly like double looping on the word 'apply' which . . uh . . well . . maybe suggesting that 'apply' is its own function name is not a bad idea right? . . 'applyUniversalBasicIncome' . . 'applyCommitteRecommendations' . . 'applyAgreementProtocol' . . 

'startApplyingAgreementProtocol'
'applyAgreementProtocol'

Well . . 'start' looks really good when I'm reading this again for another time . . hmm . . right so maybe we can remove 'apply' from our list . . but it's hmm . . yea . . maybe it's not that bad . . and maybe uh . . there are more ideas to consider . . I haven't uh . . thought about this topic very much . . at first I was like (1) I don't like it . . then I was like . . (2) It's useful . . I like it . . and now . . while writing this message (3) . . I'm starting to prefer the double looping of 'startApplying' which gives 2 verbs which are like -_- double loops 2 . . that's like -_- do you need 2 verbs to say you are doing something ? . . that is quite interesting . . well . . startApplying sounds interesting in comparison to start . . at least when it comes to 'startApplyingUniversalBasicIncome' . . and . . 'startUniversalBasicIncome' . . 'startUniversalBasicIncome' . . seems to say something like . . 'you started the universal basic income' . . but . . I'm a noob who doesn't know much about what that stuff is . . 'start universal basic income' . . what is 'universal basic income' ? . . and so in my own personal interpretation it's like . . 'start universal basic income program' is really interesting . . 'universal basic income program' . . makes a key differentiator to 'universal basic income' by saying 'program' . . 'program' . . 'program' . .

So . . for example . . 'Universal Basic Income Program' . . supposes there is a 'start' . . and so the . . program . . or . . 'program' . . can . . be . . 'started' . . and so . . if you 'start applying' . . or . . 'startApplying' . . it's like -_- . . the person is more intuitive about what is going to be happening . . it's like . . possibly a continuous thing like an infinite loop program is possibly meant to suppose and so . . startApplyingOperatingSystemLogic . . is possibly a nice name for applying x,y,z different operating system logic based things . . and uh . . well . . 'main' . . has been possibly the or an traditional name . . for . . 'start' . . uh . . well . . -_- . . in having an idempotent set of function names . . 'main' doesn't help anyone really -_- it's not like a verb -_- it's like -_- 'food' . . 'food' . . 'eat' is a better term right? . . why say 'computer.food()' ? . . 'computer.eat()' is maybe possibly a better term right? . . for the sake of verbalizing time relations? . . I'm not sure -_- . . 








[1.0]
The 10 Most Popular Programming Languages to Learn in 2021
By Brian Eastwood
https://www.northeastern.edu/graduate/blog/most-popular-programming-languages/

[2.0]
8 of the most popular programming languages
By Jonathan Greig
https://www.techrepublic.com/article/8-of-the-most-popular-programming-languages/

[3.0]
Which Are The Most Popular Programming Languages In 2021?
By Technostacks
https://technostacks.com/blog/most-popular-programming-languages

[4.0]
The 10 most popular programming languages, according to the Microsoft-owned GitHub
By Rosalie Chan
https://www.businessinsider.com/most-popular-programming-languages-github-2019-11?op=1

[5.0]
The 20 Most Popular Programming Languages in 2021
By Christopher Isak
https://techacute.com/20-most-popular-programming-languages/

[6.0]
Ecoin #528​ - Live development journal entry for April 17, 2021
By Jon Ide
https://www.youtube.com/watch?v=pxr9SLS9yPg

[7.0]
The Light Architecture
By E Corp
https://gitlab.com/ecorp-org/the-light-architecture



[Written @ 2:30]
[Incorrect Real Time for This Recording]
[Explanation]:
[This was a guessed time from when I was having my eyes closed while typing . . I guessed the time 2:30 . . but now that my eyes are open . . I am now seeing that the current time is 1:00 . . which is . . possibly a few minutes later from when I started . . possibly sometime before 1:00 . . ]

Okay . . Now my eyes are closed . . I am not sure how this experiment will go . . the idea is that this is possibly a segway -_-

I am -_- being assisted by ghostly entities like Seth and the Pleiadians and their friendly counterparts or counterlords or friends or aspects or religious related affiliates

Can you imagine if I try to p

. . 

Okay . . I am not very confident that I have backspaced to a good location . . on the map of text on the page . . My interest  

to possibly backspace to a word that was previously written like when I said 'counterlords' which is possibly a rude term right?

xD

right?

it would be a gift and doing it now might be strange since I am not confident that I have counted the number of  characters that I have typed appropriately . . and so for example I'm not sure how many times I need to press the backspace key . . but some how maybe from your perspective . . the words that are being typed on the computer screen or that are visible on display on the computer screen are being read in a nice manner or in a nice maneuver . . and so for example . . why should you personally be confused at where to backspace to? From your perspective pressing the backspace key is easy . . you can possibly count how many spaces I need to navigate to . . and where is a good place to place the mouse cursor or the keyboard cursor . . and if I made any  . . any . . typographical errors anywhere . . 

The advantage to being able to type without looking at the keyboard is really like being able to read a room without asking anyone question about what's going on . . I will make mistakes since my eyes are closed . . but if you see someone at a gathering of social events then you may assume that their eyes are open and that they also know what is going on . . 

And so why should anyone make any mistakes?

xD

xD






