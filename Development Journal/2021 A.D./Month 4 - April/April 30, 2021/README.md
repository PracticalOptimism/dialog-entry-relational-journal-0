


# Development Journal Entry - April 30, 2021

[Written @ 15:42]

Where are you?

- File System
- Network Protocol

. . 

We need to create web servers that provide 'the-light-architecture' features over the internet . . 

We need that to avoid local file system related things like 'read' that file from the local file system when in fact it could be 'read' through so-called 'any' file system so long as it's 'http' compliant to avoid negligently assuming that 'file system' is where things are always being done . . 'http' is a pretty reliable standard for cross-network resource relations and so it is convenient to start here and of course any theoretician could tell you that 'http' could be neglected for a further theoretical construct like 'glob' which is like a type of idea which means 'don't worry about it, you have a place to stay' . . 

. . 

Okay . . 'http' is a nice general enough place to start for our usecases for practical reasons since 'file system' doesn't necessarily have all the resources we need and anywhere else those resources would be available is such that 'http' is a possible way to access those resources . . 

. . 

Our javascript source code assumes 'javascript' runtime environment like 'V8'

. . 

If our program is to run appropriately, the so called 'network protocol' of interest is 'node.js' and 'web browser that supports V8-like javascript runtime features' . . otherwise O _ O we are -_- maybe not in the best position right? . . -_- Deno . . I don't know -_- I suppose that could also be a javascript environment of interest . . but to be honest . . I don't know . . 

. . 

JavaScript is a nice 'network protocol' language

(1) web browser

. . 

Because the web browser is so easily accessible to a lot of people in society . . it is easy to achieve the 'nice' property since we are people who want to share our source code and so 'nice' may be a property that applies here . . 

. . 

WebAssembly is a close runner up for 'nice' 

(1) fast

. . 

Because web assembly is so fast, everyone can benefit from the benefits of having fast running software

. . 

Because 'Rust' is a nice language to use for compiling to 'WebAssembly' we may be able to get away with writing our codebase in Rust as well as JavaScript . . 

Well . . 

. . 

I am not opposed to learning Rust . . I may have to do that later . . but for now . . I will opt to stay with JavaScript . . 

. . 

Okay . . 

. . 

Typing . . this is a nice network protocol for 'getting our work done' right? . . right! :O

. . 

Okay . . 

. . 

That should possibly be enough network protocols right ? . . I'm not sure . . We just need to say . . 'connect to a lot of people' and so 'everyone can benefit from yama yama yama' . . right ? . . -_- right? . . -_- . . yea . . right . . 

. . 

`Network Protocol Connection` <br>
`Network Protocol Member` <br>
`Network Protocol Message` <br>

. . 

A reminder for you if you like that theoretical foundational idea to a lot of topics :O . . 

. . 

Read more at https://gitlab.com/ecorp-org/the-light-architecture

. . 

In brief, those 3 topics will help you `triangulate` a lot of other topics . . so create `connections` between any `member` . . and those members send `messages` between one another . . and those messages themselves can be considered members . . and so . . uh . . you can form connections between those `message-members` . . and those can further have the ability to send messages to other message-members . . and continue this loop for as long as you like . . to get really amazing triangulation properties . . like '`connect anything you want`' and always know what you are trying to question: '`who sent what to whom and when?`' . . '`who sent blah blah blah to xyz and when?`' . . 

. . 

. . 

. . 

Okay . . JavaScript is nice . . uh . . but uh . . it's not necessarily like 'http' since . . uh . . one JavaScript runtime is not necessarily equivalent to another JavaScript runtime . . uh . . and so for example . . uh . . the program running uh -_- I guess uh . . on one computer . . uh . . maybe the web browser . . uh . . the codebase ought to run in both a web browser -_- -_- well if it doesn't work in the web browser -_- that's kind of like /slash-shrug . . why not ? . . well there are a lot of reasons for that (1) file system isn't available . . and so . . 'pseudo-file-system' needs to be created . . and 'pseudo-file-system' may rely on 'http' . . which means -_- . . being able to create computer resources at runtime even from the web browser and as an anonymous user is really useful . . 

. . 

so . . 

(1) HTTP Network Protocol
(2) Computer Resource Availability

. . 

Computer Resource allows us to have (1) a node.js resource or (2) a web browser resource through something like uh . . 'headless' or 'graphical-user-interface-less' web browser resources

. . 

Well that's the best deal to have (1) http availability and (2) computer resource availability . . 

. . 

That means we can have our programs always be able to run . . regardless of where we are . . unless we're specifically not on the internet . . and simultaneously don't have the 'the-light-architecture' installed for running a local web server . . 

A local web server would provide computer resources through 'http' only through localhost . . which is the network protocol that would possibly bypass CORS issues or something like that . . uh . . something like that . . I'm not sure . . 

. . 

Well . . 

. . 

Computer Resources are interesting . . 

. . 

(1) Ngrok http web server traffic
(2) Free http web server hosting on the internet
(3) Localhost
(4) Pay-to-play web server hosting on the internet
(5) Local Area Network
(6) ??

. . 

I'm not exactly sure how to do this . . but this is a list of ideas . .

. . 

I think ngrok is nice since it's a 'tunnel' from our 'localhost' environment to the 'public' resource web where other users can access those same 'localhost' resources from our computer through one of these tunneling services . . like 'ngrok' . .

. . 

. . 

Okay well . . all of these things look like they have their own application programmable interfaces (APIs) to learn . . or to look at . . which is like . . okay . . we need to do research . . and also . . we need to say . . hmm . . which project should all of these computer resources be available in ? . . -_- well . . my previous answer from previous development log episodes . . was OICP (Open Internet Communication Protocol)

. . 

-_- . . 

. . 

Computer Resources . . and HTTP . . 

. . 

Great . . 

. . 

We just need computer resources now . . 

. . 

So . . 

. . 

hmm . . 

. . 

const result = await oicp.addComputerProgram(`console.log('hello world')`, startDate)

console.log(result.computerCommandLineInterface.latestResult)

. . 

That is an idea of what we would like to achieve . . 

. . 

To run a 'hello world' program on another computer . . and access the results on our own computer . . 

. . 

This is a useful idea since (1) maybe your 'hello world' program really takes 2 days to return a result because it requires a super fast computer . . and your super slow computer would take 2 months . . if that . . because maybe it would take 'cross-your-fingers'-time since your computer is an old model right? . . but at least you get your results in 2 days right which was like 'can you calculate that really large number?' . . and then as long as your old computer model can access that number in memory . . for example . . your memory is enough to not overload your computer . . then . . uh . . yea . . that makes sense . . something like that . . 

. . 

'Can you find that number for me?' is a possible question you could ask a super computer . . 

and so your old computer receives the number . . 

. . 

But really maybe 'I have a problem and the answer is a special peculiar number . . can you find that for me?' . . yea . . so then . . if your computer is slow at going through the measurements . . then you would want another computer to triangulate those metrics and return those results for you . . 

. . 

Well . . 

. . 

Okay . . 

. . 

Now then it is really useful to have computer resources . . 

. . 

OICP is where we would love to have our computer resources congregate . . the OICP network is versatile . . and can be turned on and off . . 

. . 

It is quite useful . . 

. . 

But for some reason . . It is another project -_- . . and so . . we should possibly consider . . -_- . . 

-_- . . 

-_- . . 

. . 

I think the OICP project 

(1) Helps us have computer resources
(2) 

. . 

So maybe for now . . we may need to tell ourselves to not work on '`the-love-algorithm`' and '`the-light-architecture`' and '`ecoin`' since those are all depending on 'computer resources' . . right? . . I'm not sure . . it sounds REASONABLE

. . 


Always having our programs accessible is very useful . . that's what the whole 'decentralized' technology movement is about right? . . you don't want 'banks' and 'governments' turning off your program right? . . something like that . . 

. . 

Alright . . well . . I'd love to go through the checklist of things we have from 15:35 listed here in this development journal entry . . but for some reason . . we would really like to have computer resources . . 

. . 

Computer resources make accessing memory resources . . easy . . We don't need to ask so many questions of whether or not we know where something is . . or if it's accessible or not . . or even where the item is . . as long as you have a computer resource manager . . you can possibly not have to think about where your program is running . . or where your data is stored . . well . . programs are possibly a generalization of 'data' at least in the way that we mean since 'json' is like a data format but json parser is possibly the program that parses the json and makes it meaningful to read right? . . I'm not sure -_- anyway . . a json program is really useful to be able to run whether or not you have the json program installed locally on your computer environment . . 

. . 

Well . . 

. . 

Yea . . I guess it's like a 'try-catch' block for all of our programs right? . . I'm not sure . . 

. . 

(1) OICP having computer resources
(2) ??

. . 

(2.1) Publish 'the-light-architecture' to 'OICP?'
(2.2)

. . 

Weird . . 

. . 

I guess we could say 'hey OICP, install that program from npm and start running it' . . which is like 'runtime access to already publically available programs' and so 'oicp.addComputerProgram('https://npmjs.org/package/hello-world', startDateOrMillisecondTimeAfterInstall [string|number], 'start', [inputItem1, inputItem2])' . . and so that is possibly a way to start the computer program at some time after it has been installed . . 

```javascript
const computerProgram = oicp.addComputerProgram('https://npmjs.org/package/hello-world')

const result = computerProgram.initializeFunctionEvaluation('start', [inputItem1, inputItem2])
```

. . 

well maybe that is an okay example . . 

. . 

. . 

I'm not sure . . 

. . 

Uhm . . 

. . 

(1) Create Computer Resource
(2) Use Existing Computer Resource Locations
(3) Publish Items To Existing Computer Resource Locations
(4) Create Email Accounts
(5) Sign Up For New Computer Resource Accounts On The Internet
(6) Verify Email Accounts
(7) 

. . 

Creating Computer Resources . . is really cool . . but it's also like -_- we're pretty much like a 'bot' farm . . -_- I don't really want to say that this isn't an automatic task . . computer resources are something you can possibly create by typing but why not have your computer program do some of those typing things for you . . right ? . . -_- just create email accounts . . and uh . . -_- . . keep track of the passwords I guess . . and somehow also -_- . . uh . . make sure the network partitions the traffic appropriately . . and at runtime if needed . . right? . . and automatically create new computer resources as needed . . -_- but also -_- -_- . . -_- maybe request an administrator of the network like Jon Jon Ide Ide to see if you are not spamming too many all at once O _ O . . 

. . 

Hmm . . an event listener system for telling us . . if there were recently new created accounts . . or recently new created resources . . and how many . . and how frequently . . that would be a nice list of things to receive at a regular interval . . lets say . . every day . . and so a dashboard for that is quite useful . . and of course the dashboard should be available from 'the-light-architecture' or else -_- . . why isn't it available? . . -_- . . right? . . well . . maybe . . it's just an idea right now . . and also . . it's weird how these projects are depending on one another . . circular dependencies right? . . amazing . . 

. . 

Not quite circular dependencies since there will be different versions installed at various times . . and so it's okay . . for example . . project X will have project Y installed . . but a copy of project Y will be installed and not project Y itself :O . . which itself means it's just a copy of a local version which is familiar for us for when development . . is happening . . so they have multiple similar versions installed to avoid circular dependencies . . something like that . . 

. . 

something like that . . 

. . 

Well yea . . to avoid making the internet mad . . having a dashboard for . . making OICP . . uh . . manageable . . to see when and where there are bans . . or if there are bans . . or if there are too many resources created and we should manually delete them . . and even the possibility of sending a 'gratuity email' or a 'gratitude email' to the internet resource providers to say 'thank you' for their service since we are roadhogs who are publishing servers for free and saying that we are having fun . . and that is what the network is about -_- . . 

. . 

Okay . . with all of that said . . including all of the gratuity . . and gratitude . . and thankfulness . . that should possibly align us with a nice looking project that says . . yea . . we can know when and where we make mistakes . . and correct our mistakes as we need to . . and if we're alerted that there is a possible mistake upcoming . . well that's useful for just backtracking how many servers we have on the network . . and saying that there ought to be fewer servers on the network . . or there ought to be fewer programs on those servers or something weird like that . . really if we can avoid some of those things that would be great because we'd like to have an open network where you can publish your program and you don't have to worry about the program being removed because of 'resource constraints' . . or 'resource constraints from administrators not liking the resource hogging ability of that computer program'

. . 

O_O

. . 

Computer resource . . 

. . 

(1) Install program from OICP
(2) Install program from NPM

. . 

(1) Install OICP
(2) oicp install program-name
(3) 

. . 

(1) Install OICP Desktop Application
(2) Start OICP Desktop Application

. . 

(1) Install OICP
(2) Start OICP Local Environment Daemon

. . 

(1) Publish to OICP
(2) Start Program From Remote Environment Daemon

. . 

(1) Your Local Computer Has Permission Rule Keys
(2) Your Local Computer Can Share Permission Rule Keys
(3)

. . 

(1) Access a dashboard of the performance of your computer program
(2) Access a dashboard for the statistics of your computer program
(3) OICP is a dashboard for the computer programs you publish
(4) 

. . 

Warning Comments: Try publishing only to local instances of OICP to avoid . . headaches for other internet resource providers . . receiving bills to pay because free users are doing blah blah blah . . 

. . 


[Written @ 15:35]

We need to :

(1) `Install` the light architecture
(2) Create a `tally` using the light architecture
(3) Create a `README.md` file using the light architecture
(4) Update the `README.md` file using the light architecture
(5) Update the `tally` using the light architecture
(6) Start a `development server` to update our project using the light architecture
(7) Start an `experimental release server` to release our project to N-ary various places of interest using the light architecture
(8) `Release` the project we're working on to N-ary various places of interest using the light architecture
(9) `Fix` any bugs that we made using the light architecture
(10) Use the love algorithm for other things of interest




[Written @ 12:38]

Interactive Graph . . 

Interactive Graph Types . . 

An interactive graph type is very useful . . probably you should identify some and then . . maybe use that for something interesting . . 

Physical Probabilistic Physical Graph Readers
Physical Policy Reader
Digital Interactive Command Line Reader
Digital Interactive Command Line Reader With Visual Layout Format
Digital Information Reader
Digital Simple Idea Reader

. . 

These are a few typed names from 'Seth' and other probable counterparts . . they have helped me type this list to showcase the possibilities of what could be useful for names of 'types'

. . 

These could be useful -_- . . It does place -_- things into interesting topic areas . . like tallys -_- being interactible with -_- /sigh . . more things to think about . . 

. . 

consumer . . is a type of information format for having interactivity . . and so if you use interactive graph . . you are potentially supposing that that functionality is possibly placed into -_- other places -_- . . like usecase/algorithms/interactive-graph/**/* etc. . . which is really well -_- I guess you could do that if you wanted to . . but I will try to avoid that . . maybe . . I'm not sure -_- we'll see where this goes . . 

well these are the basic types so it's like -_- that takes care of a lot of things of interest . . and so for example . . 

A javascript library is a 'digital interactive command line reader' type right? . . since it's like the command line except for javascript runtime environments? -_- right? . . and 'http' is also a 'digital interactive command line reader' since it's also like the command line except for 'web browsers'? right ? web browsers or web servers . . right? -_- web browsers or networking through physical resources of the world right ? . . -_- like bandwidth highways underground or under the ocean? -_- right? . . I'm not sure . . 

. . 

well -_- . . we have a few types here . . but we don't have to use them . . remember they are only interactive graph types -_- . . 

. . 





[Written @ 12:34]

Minimum Description List: `Interactive Graph`, `Comments`

I'm not really sure -_- interactive graph is a good name . . maybe my previous comments are satire . . satirical . . 

Maybe . . 

Maybe . . 

Interactive Graph is like 'Artificial Intelligence' or 'Artificial General Intelligence' so it's not really going to tell you anything about what it does specifically . . 

'Mow the lawn' <br>
'Eat vegetables' <br>

. . 

These are all lame things that people do . . right? . . what happens when you're an ant creature and want some Artificial General Intelligence to tell you how to build ant hills?

-_-

-_-

whatever . . Interactive Graph is a good name but it's not necessarily going to tell you what type of interactive graph you're trying to build . . 

[Written @ 12:30]

Minimum Description List: `Anana anana anana`, `Interactive Graph`, `Anana anana anana is better`, `Interactive Graph is stupid`

Anana anana anana is really quite a good name

anana anana anana . . doesn't necessarily let you assume anything . . 

anana anana anana . . anana anana anana . . is a name for 'anderson' related topics . . 

Interactive Graph . . on the otherhand is '-_-' you get the impression that possibly you should do something with that -_- but it's really lame when you do something with -_- a lame idea . . 

A theory about -_- anything would always possibly be a 'interactive-graph' -_- and so 'non-interactive graphs' are possibly the limits for all theory . . -_- . . 

'interactive-graph' makes you think there is something there -_- but it's like -_- probably you shouldn't focus on that since you're just trying to build a dumb website . . so stop being an idiot and building a universe before you start blah blah blah -_- right?

'interactive-graph' is dumb.

'anana-anana-anana' is better.



[Written @ 11:54]

### An Algebra Listing For Function Names:

(1) Get
- (1.1) GetIsItemBeing
- (1.2) GetIsItemDoing
(2) Create
- (2.1) Add
- (2.2) Initialize
- (2.3) Start
- (2.4) StartApplying
(3) Update
- (3.1) Evaluate
- (3.2) Enderize
- (3.3) Existentialize
- (3.4) Seminify
(4) Delete
- (4.1) Remove
- (4.2) Uninitialize
- (4.3) Stop
- (4.4) StopApplying
- (4.5) Deduplicate
(5) Recommend
- (5.1) GetResponseForRecommended
- (5.2) CreateRequestForRecommended
- (5.3) UpdateSettingsForRecommended
- (5.4) DeleteDefaultSettingsForRecommended
(6) Scale
- (6.1) ElementalizeScale
- (6.2) DeElementalizeScale
- (6.3) AmortizeScale // Normalize Based On Many Variables
- (6.4) GetScaleCoFactor // GetElementalizedDeElementalizedScaleCoFactor
(7) <'PerformAction'><Period><Age><Rule><Name><Service>
- (7.1) With Prefix (Use with Codebase for consistent prefix-naming)
  - (7.1.1) Example: PerformActionCalledSearch
  - (7.1.2) Example: PerformActionCalledOrganize
  - (7.1.3) Example: PerformActionCalledPress
  - (7.1.4) Example: PerformActionCalledType
  - (7.1.5) Example: PerformActionCalledEvaluate
- (7.2) Without Prefix (Use with User Interfaces for Short-hand names)
  - (7.2.1) Example: Search
  - (7.2.2) Example: Organize
  - (7.2.3) Example: Press
  - (7.2.4) Example: Type
  - (7.2.5) Example: Evaluate


[Written @ 8:46]

- /interactive-graph
  - /database
    - /graph-item-1
    - /graph-item-2
    - /graph-item-3
  - /event-listener
    - /graph-item-1
      - /algorithm-1
      - /algorithm-2
    - /graph-item-2
    - /graph-item-3
  - 

. . 

- /interactive-graph
  - /@service
  - /database
    - /graph-item-1
    - /graph-item-2
    - /graph-item-3
  - /event-listener
    - /graph-item-1
      - _service.ts
      - algorithms.ts
      - data-structures.ts
      - constants.ts
      - variables.ts
      - infinite-loops.ts
    - /graph-item-2



[Written @ 8:39]

Minimum Description List: `Interactive Graph`

Here is an updated list of the service items as I've come to identify them

. . 

Service Item List:

- /database
- /network-protocol
- /event-listener
- /permission-rule
- /interactive-graph
  - /database
  - /network-protocol
  - /event-listener
  - /permission-rule
  - /scale-matrix-amana
  - /computer-program-manager
- /scale-matrix-amana
- /computer-program-manager

. . 

Service Item List . . 

Notice how the 'interactive-graph' item is not containing an 'interactive-graph' directory within its folder item . .

This is uh . . possibly intentional . . to possibly uh . . avoid . . uh . . overhead looping like uh providing another interactive graph item which would possibly be better uh . . assumed to be provided for by another source and not necessarily directed in that uh . . directory . . well . . if you really need to have . . uh . . an interactive graph folder in the base . . or root . . or top level . . interactive graph . . directory folder . . then you can do that . . but also don't forget to possibly terminate the looping since interactive graph looping could possibly not be something you're trying to support at a particular time depending on your usecase I suppose . . I'm not sure . . I haven't thought about this topic very much and these are only my opinions at this time . . 

- /database
- /network-protocol
- /event-listener
- /permission-rule
- /interactive-graph // first item listing of 'interactive-graph'
  - /database
  - /network-protocol
  - /event-listener
  - /permission-rule
  - /interactive-graph // second item listing of 'interactive-graph'
    - /database
    - /network-protocol
    - /event-listener
    - /permission-rule
    - // hypothesized next item listing of 'interactive-graph'
    - /scale-matrix-amana
    - /computer-program-manager
  - /scale-matrix-amana
  - /computer-program-manager
- /scale-matrix-amana
- /computer-program-manager



[Written @ 8:24]

Minimum Description List: `Interactive Graph`, `Non-Interactive Graph`

Interactive Graph is a notion to relate to the topic of 'anana-anana-anana'

Interactive Graph originated by the usecase of trying to find a name for what type of information service provider was the type of service provider that provides information about (1) graphical user interface charts like (1.1) Echarts and (1.2) D3.js and other types of interactive graphical interface html items or javascript and html items . . since a lot of the graphs are interactive with javascript involved . . but they can be displayed on the web page with a nice uh . . user interface application programmable interface like 'add that chart to the page' or 'chart.getHtmlElement()' and 'document.appendChild(htmlElement)' which are user friendly interface application programmable interface code functions that make adding information to a web page easy and usable . . 

But . . Well . . uh . . Interactive Graph is a nice name for . . 'charts' . . like 'bar graphs' and 'line graphs' and 'pie charts' and other types of interactive charts that show statistics of interest . . well . . today . . I was reviewing my notes in my physical pencil and paper notebook and realized that possibly . . 'interactive graph' was a nice replacement term for 'anana anana anana' since even those things are supposed to be expected to be 'interactive' . . and in some sense they are possibly also . . 'graphs' of some kind . . and so . . for example . . an 'interactive graph' could really be possibly described as one of the following items in terms of how it's presented in a directory codebase:

- /interactive-graph
  - /database
  - /network-protocol
  - /event-listener
  - /permission-rule
  - /scale-matrix-amana
  - /computer-program-manager

. . 

An Interactive Graph . . is possibly a replacement term for 'Anana anana anana' . . Anana anana anana was previously recovered by understanding or trying to understand the topic of 'Anderson' . . 'Anderson' is a related topic being taught to me by Seth and other counterpart entities as it relates to the topic of 'And' and 'And' . . and so for example . . 'And . . and . . and . . and . . ' and so if you have a topic of interest like a cat . . then you could say ' and . . it has red fur . . and it has a blue hat . . and it has a red scarf . . and it has black sunglasses . . and . . and . . and . . and . . and . . and . . and . . and . . and . . ' and so on . . and you can continue this process to press forward the idea that 'time is really a subjective event' and so for example 'and' really supposes the idea that you aren't necessarily perceiving things in the same way that any other individual is perceiving things . . 'and . . and . . and . . and . . ' supposes that there are 'anderson' relations that are really developmental . . developmental is a technical term used here to mean . . 'no real ending' . . 'enderly' or 'enderson' or 'enderlyness' is the topic of 'anderson' with an ending . . and so . . (1) and (2) and is an enderly anderson . . enderly anderson is stopping the anderson . . there is an end . . and so if you want to point out only so many topics of interest for a topic . . you can say . . enderly is the topic of interest and say that it stops being supposed any further beyond those 2 or 3 four five six seven eight nine ten etc. endersons . . 

Anderson by itself doesn't suppose an ending to the topic of interest and so for example it can even 'so-called loop over itself' . . 'looping over itself' means that endersons are hidden by further loops of interest and so even between the ands that you are supposing you can suppose there are furhter ands that are even hidden and meaningful for being re-discovered but even re-discovering them supposes that you could possibly have an end . . or an enderly nascency . . or an enderly neighbor . . but really you're not necessarily supposed to assume the enderliness of an anderson without further clarification . . and so all those things of various topical interest and all the possibilities of how they could be mouthwashed rinse and run dried forever and throughout all of time and for any given moment of period of time history event . . or period of time history event . . there are andersons of andersons of andersons of andersons . . 

Ander is another topic that relates to 'anding' but it isn't like it is really structured and so for example it is 'unidentifiable' and 'non-interactive graph' could possibly be the name for this topic since it really is 'non-interactive' in all the senses of interactivity that you could possibly imagine . . such as (1) seeing with your eyes (2) hearing with your ears (3) tasting with your tongue (4) hypothesizing with your mind . . and so on and so forth . . and now even the term 'interactive graph' becomes useful here because it helps us suppose further ideas of 'non-interactive graph' which are already 2 terms that are easy to consider and easy to parse for a human mind such as myself who is at least familiar with where and when these words are used such as (1) popular culture and (2) academic circles . . and so it is useful to have a nested loop in each of these two areas for at least relating to both people of interest who would find those topics interesting . . something like that -_- . . 

. . 

-_- . . 

-_- . . 

-_- . . 

-_- . . 




