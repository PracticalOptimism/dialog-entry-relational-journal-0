

# Development Journal Entry - April 26, 2021

⚠️ Unavailable Media Format (.pdf, .jpg, etc.) Please visit [resource url](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Websites/YouTube/YouTube%20Live%20Streams/Jon%20Ide/2021%20A.D./%40%20Reminder)

. . 


[Written @ 2:28]

A `playback repeatable list of items of interest` is also a nice playbook for when you want to have a list of things of interest like a 'news feed' or a 'new bulletin board' but you're not sure how else to organize the information to be specific to enumerated efficiencies-_- I'm sorry for the previous remark :( . . 

Example Photograph #1
![playback-repeatable-list-1](./Photographs/playback-repeatable-list-1.png)

Example Photograph #2
![playback-repeatable-list-2](./Photographs/playback-repeatable-list-2.png)


[Written @ 2:00]

`Expansion Panel Grid`

By the time of [Ecoin](https://gitlab.com/ecorp-org/ecoin)'s development the project went through many phases of '`redesigning the user interface`' . . and the hail hitler approach of using 'expansion panel grids' was finally achieved . . and that can be available to you if you visit this commit message: https://gitlab.com/ecorp-org/ecoin/-/commit/eb942fbd54ab0a4474af0549823c97ef1dac43bb . . 



. . 

Example Photograph #1
![expansion-panel-grid-1](./Photographs/expansion-panel-grid-1.png)


Example Photograph #2
![expansion-panel-grid-2](./Photographs/expansion-panel-grid-2.png)


Example Photograph #3
![expansion-panel-grid-3](./Photographs/expansion-panel-grid-3.png)


Example Photograph #4
![expansion-panel-grid-4](./Photographs/expansion-panel-grid-4.png)


Example Photograph #5
![expansion-panel-grid-5](./Photographs/expansion-panel-grid-5.png)


Example Photograph #6
![expansion-panel-grid-6](./Photographs/expansion-panel-grid-6.png)


Example Photograph #7
![expansion-panel-grid-7](./Photographs/expansion-panel-grid-7.png)


Example Photograph #8
![expansion-panel-grid-8](./Photographs/expansion-panel-grid-8.png)


[Written @ 1:52]

`Expansion Panel Grid` is a generalized data structure for enumerating items of interest without having to . . uh . . expand everything by default . . 

. . 

Expand

. . 

There are a lot of trial and effort names that I'll go through to see what it was like to see a good looking name that could be COMPARTMENTALIZABLE IN MY HITLER REGIMEN TO EAT GOOD LOOKING NAMES

. . 

Expand <br>
Expansion <br>
Interest <br>
Idea <br>

Contract <br>
Condence <br>
Deduce <br>
Deliminate <br>
Eliminate <br>

Trial And Effort <br>
Development <br>
Development Routine <br>
Eager <br>

. . 

These are really sketchy names but it was a trial and effort and maybe I will enumerate that more and more as I write notes . . a lot of the previous ones were trial and effort and also simultaneous help from ghostly entities -_- which I had originally thought was my own personal effort but was automatic typing events that happened without my where with all -_- huh 🤷 shrug-emoji who would have known? . .

. . 

-_-

. . 

But Personal Responsibility is Good To Take On One's Own So I try to imagine I am mostly responsible for my own work and there is a possible ghost ready to help me develop -_- which is kind of lame because I thought I was developing on my own without ghosts and so I was cool without having to say ghosts are the cool ones O_O

. . 

Responsibility <br>
Response <br>

Request <br>
Consider <br>
Corner <br>

CornerCase <br>
EdgeCase <br>

Edgy <br>
Dicey <br>
Icey <br>

Spicy <br>
Wow <br>

. . 



[Written @ 1:35]

**Minimum Description List**: `Itemization Pipe`, `Itemization Start`, `Strict Pipe Notation`, `Relaxed Pipe Notation`


`For item lists that are really easy to read . . `

. . 

| Item Name | Item Description | Item Date Created | Item Date Updated | Item Date Specification Requirement |

. . 

has alternative counterparts . . you can write a list without starting with "|" (pronounced "pipe" in language circles that I'm familiar with) . . (you could also pronounce it as "start" since I just made that up 😏)

| Item Name | Item Description | Item Date Created |

. . 

You can have so many lists of things that are easy to read O_O

. . 

And within those lists can be further lists

| Item Name, Item Communication Name, Item Pseudoname, Item Nickname | Item Minimum Description, Item Median Description | Item Specification Requirement |

. . 

But to be a real differentiator you could possibly ask the question 'why is this useful to look at for a human being?' 'is this really easy to read?' 'i'm not sure about that, it reminds me of HAIL HITLER' 'hail hitler syntax reminds you to stand up straight and determine if you are always standing up straight and going in proportional order as where you were supposed to go'

| . . | . . | . . | . . | pipe syntax can be a hail hitler regime if you keep using it very often and very often and very often and very often

But it's not necessarily the worst system . . tables like spreadsheets or data entry items can have things like this where you can have hail hitler routines that are interesting and usable for that type of maneuvering recourse relations . . by recourse relations we mean . . 'where a uniform to work' and 'please behave'

. . 

But behaving is O_O why can't I be a baby?

. . 

-_-

. . 

pipe syntax without the starting pipe and the ending pipe are also useful to say that you have relaxed . . your notation . . 

. . | item #1 | item #2 | . . 

And that is a usable item list without the interesting idea that my uniform needs to have a tie that is tied all the way to the top . . it can be a loose tie and I can look cool while wearing it . .

. . 

`Itemization Pipe` . . `Itemization Start` . . I'm not sure . . either name could possibly work . . 

. . 

`Strict Piping Syntax` . . `Strict Starting Syntax` . . <br>

| item #1 | item #2 | item #3 | item #4 | item #5 |

. . 

`Relaxed Piping Syntax` . . `Relaxed Starting Syntax` . . <br>

item #1 | item #2 | item #3 | item #4 | item #5

. . 


[Written @ 1:23]

Hitler

Hail Hitler

| Item Name | Item Description | Item Date Created | Item Date Updated | Item Date Specification Requirement |

Hail

These things are very instrumental in saying 'hail'

'Table of Contents' is a fine name -_- . . but it's like . . 'Enumerated Table Listing' is like hmm 'is that going to kill everyone's spirit? . . is that the best way to contribute? haha well that is really a dumb question and so opinionated I'm sorry for asking . . of course you're welcome to contribute . . hop right in . . haha [pull-shorts-up-and-dance-emoji] . . 

well . . enumated table listing is a hail hitler term and possibly hatred and anger and outrage can emerge from it . . 

If you enjoy enumerating things your user interface will look like a mathematician's brain . .

. . 

If your user interface is a soft body of water . . then everyone will appreciate it including the mathematicians

. . 

'Table of Contents' <br>
'Enumerated Table Listing'

. . 

It's not necessarily a bible to say no to either of these items but it's like up to you I guess

. . 

Vocabulary can help you find new verticals to distinguish yourself

. . 

O_O

[Written @ 1:18]

Minimum Description <br>
Median Description <br>
Maximum Description <br>

Table of Contents <br>

. . 

Table of Contents hasn't really been thought through that much by myself . . I suppose the word 'Contents' is interesting . . 'Table' is also interesting . . 

Enumerated Table Listing <br>

Enumerated Items Of Interest <br>

. . 

Areas is an interesting term 'Areas of Interest' has been used in places of interest . . 'areas is like a generalized 'wow, I'll never understand that'' and so it's meant to be a capture flag without specifying anything in general . . 

-_- mathematics is topical 'areas of interest are like' 'areas of interest are like' 'areas of interest are like'

'areas of interest[without-the[before-you[close-sentence{remind{{new-notation}}}]]]

And you may never finish what you were saying

. . 



[Written @ 1:05]

**Minimum Description List**: `Arrangement`, `Infinite Loop`, `Hashmap`, `Function`, `Object`, `Item`

`Arrange` or `Arrangement` . . is meant to be a generalized mathematical term for 'infinite loop' that has 'usecases of interest'

Usecases of interest can involve . . 'throwing dice at random band wagons that roll by'

\-\_\-

In the original usecase of the term there was . . -_- no 'random dice throwing like what is written above -_-'

-_- I'm sorry I think that was a message from 'Seth' who is helping me write these notes

-_-

Arrangement is 'function + data structure instance'

. . 

data structure instance by itself can have names like 'map' 'hashmap' 'data structure' 'data structure tree' 'data structure tree index' 'variable' 'variable tree index' 'variable constant' 'constant' 'constant tree index' 'constant index' 'index' 'item' 'item of interest' 'itemization item' 'itemization item index' 'itemization item index index index' 'alphanumerical itemization index' 'itemization index index index super-supreme-omega-index-finder-range-value' 'super-supreme-omega-item-index-range-finder-value'

All of those are quite interesting and filled with nuances . . all you need to do is deduce them to a set like 'hashmap' and you will be fine, right? right?

hashmap has various names like 'data structure' and 'data structure instance' and 'object notation data structure item' or 'object for short' or 'object' or 'object'

dictionary . . table . . book . .

Okay . . 

So an arrangement is if you have a 'function' that operates on the book . . and so for example . . typing in the book . . or pressing your pencil onto the book surface to write . . 

uh . . well . . 

originally . . the term arrangement relied on 'infinite loop processes' like 'always have someone there to write in the book' or 'always operate on the book' or 'always do that thing in the book' or 'keep typing' or 'keep fast forwarding since we need that for things like web applications and computer graphics systems that repeatedly render a new page or something like that'

'always keep functioning, on the book'

Arrangement . . to arrange also suggests the possibility of 'ordering' things . . since 'ordering' is an idea . . you may think to yourself . . well . . arranging . . ordering . . well . . those are related right? Well don't get yourself mixed up . . 'arrangement' is specifically for 'orders that are continual' 'continual' . . 

'Orders' are 'specific to 'finite, nowness' in which the process is complete and done and simplified like an ordered list of numbers like [1, 2, 3] or an ordered list of fruit ['apple', 'banana', 'orange'] or an ordered list of students in a room that are in a certain grade or belong to a certain social caste . . 

Orders are supposed to be 'finite arrangeable items of interest such as if you took a timelapse snapshot of an arrangement'

A timeline annotation

. . 



[Written @ 0:58]

**Minimum Description List**: `Obstacle`, `Obstacle Course`

`Obstacle` is a generalized syntax name for 'what if you only knew 1 thing'

'Everything else would be an obstacle' is the type of idea that it was relating to . . 

'Obstacle notation allows you to answer questions like'

'what was that about?'

'how can you relate it to that one thing?'

'when did you know about that?'

'how did you learn about that?'

'not sure'

'not sure'

well all of those items are 'obstacles' and so you can say you have an 'infinite loop' obstacle or an 'infinite loop' program and so that those are abilities for you to traverse a list of other obstacles . . 

One obstacle doesn't have to collide with another obstacle . . those are only like ideas or possible alterior motive interpretations of what the term 'obstacle' is meant . . 

An obstacle is like . . if you can go through an 'Obstacle Course' You will have a really clear understanding of a certain topic . . 

An `Obstacle Course` _like riding go-karts_ is like 'woah, that was quite a ride' . .

And so all the obstacle courses can be . . various sequences of random ideas or focused ideas or clarified ideas and you are able to organize the ideas in whatever sequence you like

. . 

(1) do this thing first <br>
(2) focus on this thing next <br>
(3) do this other thing <br>

. . 





[Written @ 0:54]

[Copy-Pasted From January 3, 2021 Development Journal Entry]
[`Added Break points` <br> . . to help distinguish each line in the rendered markdown file . . for platforms like gitlab and github and npm . . as examples of things where the rendering could possibly be 'defunct' or not rendering line by line as indicated in this text editor reading in 'visual studio code' at the time of this writing . . ]

. . 

There's just a cacophony of possible manipulations of this data structure.

There's just a cacophony of possible mutations of this data structure.

There's a cocaphony of possibly mutations of this data structure.

There's a cocophony of possible monipulations of this data structure.

There's a cococophony of possible 

. . 

Alternative Names For Loop

Cap
[Inspired by a re-arrangement of my visual system when I was reading the . . notes noted at . . 1:22 . . on this same page . . "Item Simultaneous Negative Loop" looked like the text reading . . "Item Simultaneous Cap" . . Cap . . seems to possibly relate to the term . . "Capacity" . . which is possibly a term used in another alternative reality of this Eearth . . which means . . "Potential" . . or . . "Ability to Become But Not Actualized Yet" . . "Cap" . . or . . "Capacity" . . is a possible alternative word for "Loop" . . or . . "Repeated Observation Of Item Index Value" or something like that . . "I'm not sure" is a Cap for representing the attention to detail of not knowing :O . . :O . . :O right? :O . . ]

. . 

The Light Architecture's 8 Algorithms

(01) Get <br>
(02) Create <br>
(03) Update <br>
(04) Delete <br>
(05) Initialize <br>
(06) Uninitialize <br>
(07) Start <br>
(08) Stop <br>
(09)  <br>
(10)  <br>

The Light Architecture's 10 Constants

(01) Item <br>
(02) Item List <br>
(03) Item Store <br>
(04) Item Arrangement <br>
(05) Item Simultaneous Negative Loop <br>
(06)  <br>
(07)  <br>
(08)  <br>
(09)  <br>
(10)  <br>

(01) Item Length <br>
(02) Item Size <br>
(03) Item Order <br>
(04) Item Shape <br>
(05) Item Ordering <br>
(06) Item Organization <br>
(07) Item Itemization <br>
(08) Item Interesting <br>
(09) Item Indecency <br>
(10) Item Indicator Pool <br>
(11) Item Integer <br>


(01) Service <br>
(02) Service Account <br>
(03)  <br>

(01) Network Protocol Connection <br>
(02) Network Protocol Member <br>
(03) Network Protocol Message <br>
(04) <br>
(05) <br>
(06) <br>
(07) <br>
(08) <br>
(09) <br>
(10) <br>

(01) Event Listener (Get Access To Variable When Updated) <br>
(02) Permission Rule (Stop Access To Variable When Updated) <br>
(03) Access Coordinator (Update Access To Variable When Updated) <br>
(04) Item Indicator (Create Access To Variable When Updated) <br>
(05)  <br>

The Light Architecture's 6 Data Structures

(01) Algorithms <br>
(02) Constants <br>
(03) Data Structures <br>
(04) Infinite Loops <br>
(05) Moment Point Algorithms <br>
(06) Variables <br>
(07)  <br>
(08)  <br>
(09)  <br>
(10)  <br>

The Light Architecture's 2 Infinite Loops

(01) Anticipation <br>
(02) Intention <br>
(03)  <br>
(04)  <br>
(05)  <br>
(06)  <br>
(07)  <br>
(08)  <br>
(09)  <br>
(10) <br>

The Light Architecture's 8 Algebras

Sort, Order, Arrange <br>
Cross Multiplication <br>
Diagonalization <br>
Hexagonal Cross Loops <br>
Actrometrin Dice Rolls <br>
Mentripediculican Pensil Spins <br>
Mentripedicularian Pandumentrianc Paddleboard Rice <br>
Andustrian Mendiscutrilustriein Mendicularienstreimestrium <br>
Manstripulatinarqriugs <br>
Andrstripiuslianstrian <br>

Matrix Comparison Operation <br>
Membrane Equality Variable Indicator Mestrian <br>
Ancispecien Spreystrian Membsterian Anstipiuhus <br>

. . 

Order Development Provider
- Quicksort
- Mergesort

Order Development Algorithm List
- Order by number indexed list
- Order by time based list
- Order by nucance ignorance
- Ordered list by X-dimensional trie ordered matrix

. . 

Simultaneous <br>
Sequential <br>

Immediate <br>
Latent <br>

Active <br>
Delayed <br>

Provider <br>
Consumer <br>
Usecase <br>

Development <br>
Acceptance <br>
Completion <br>
Satisfaction <br>
Development Activity <br>

Variant Force (Development as a state of completion)

Frequency <br>
Intensity <br>
Density <br>

. . 

Arrival <br>
In-Transit <br>
Deliverance <br>


Product Resource Composition Map:

Obstacle <br>
Organize <br>
Arrange <br>
Arrival <br>

. . 

A Product Resource Deliverance: <br>

Deliverance, Delivery <br>

. . 

Arrangement <br>
Entity <br>
Shape <br>
Concept <br>

. . 

Turn <br>
Compose <br>
Remember <br>
Commitment <br>

. . 

Reflect <br>
Return <br>

. . 

Reflect <br>
Rotate <br>
Translate <br>

. . 

Enumeration Score <br>
Encasing <br>

. . 




The Light Architecture New Item Ideas That Are Uncategorized So Far . . 

Item Shot <br>





Christopher Alexander's 15 Properties of Life

(01) Alternating Repetition <br>
(02) Good Shape <br>
(03) The Void <br>
(04) Boundaries <br>
(05)  <br>
(06)  <br>
(07)  <br>
(08)  <br>
(09)  <br>
(10)  <br>
(11)  <br>
(12)  <br>
(13)  <br>
(14)  <br>
(15)  <br>

Thoughts on Artificial Intelligence So Far . . 2 Ideas

(01) The Love Algorithm <br>
(02) The Light Architecture <br>
(03) <br>
(04) <br>
(05) <br>


Thoughts on The Love Algorithm So Far . . 11 Ideas

(01) Obstacles, Obstacle Courses, Obstacle Streams <br>
(02) Rectangle-Based Obstacle Observation Algorithm <br>
(03) Obstacle-Based Algorithms as a Generalization to Functions <br>
(04) Pain and Pleasure Architecture ?? <br>
(05) Finite Space and Finite Time Constraint Obstacles <br>
(06) Computer Advancement Theory And Practice <br>
(07) Computer Relations <br>
(08) Orgasmic Experiences / Natural Orgasmic Experiences <br>
(09) You Are I <br>
(10) You cannot create. You only observe <br>
(11) You create your own reality. You are an illusion. <br>
(12) <br>

Thoughts on An Initial First Application To Showcase The Applicability of The Love Algorithm . . 2 Ideas

(01) Speed Drawing <br>
(02) Speed Painting <br>

Questions In Artificial Intelligence So Far . . 2 Questions

(01) How many Obstacles do you need for the program to be satisfied for an initial first implementation? <br>

(02) What are these core obstacles and <br>


. . 


Notes @ Newly Created Words

Qhat
[Written @ January 3, 2020 @ approximately 1:44]
[I accidentally typed the word "Qhat" instead of typing the word "What" . . I'm sorry . . I'm not sure what this word . . "Qhat" . . should mean . . at this time . . Although . . "Qhat" . . does remind me of the map environment and the creature landscape environment of the idea of . . those majestic creatures in "Ahn'Quraj" . . which are like giant . . beetles . . that are like . . guards . . and . . rulers . . of . . this . . uh . . I guess it could be considered like a majestic place . . uh . . or something ilke that . . I'm not sure . . it's really quite cool]

janit
[Written @ January 3, 2021 @ approximately 1:44]
[I wrote the word . . "Jiant" . . and . . re-typed the word . . to spell the word . . "giant" . . and then . . alternated typing the word . . "jiant" and . . "giant" . . and . . accidentally typed the word . . "janit" . . ]




. . 


Itention Descirption <br>

Itention Description <br>

Title Description <br>

Description <br>

Benefits <br>

Features Description <br>

Limitations Description <br>

Miscellaneous Description <br>

Contact Information <br>
News Updates <br>
Community Restrictions & Guidelines <br>

Codebase <br>
Provider Resource <br>
Consumer Resource <br>
Usecase Resource <br>

Community Members & Volunteers <br>

Usecase Provided By Rleated Video Series <br>

Settings <br>

Consume News Updates <br>

Provide News Updates <br>

Authentication Description <br>


. . 

Intention Description <br>

Anticipation Description <br>

Title <br>

Description <br>

Benefits <br>

Features <br>

Limitations <br>

Additional Notes <br>

Contact Information <br>
Community Restrictions & Guidelines <br>
News Updates <br>

Codebase <br>
Provider Resource <br>
Consumer Resource <br>
Usecase Resource <br>

Community Members & Volunteers <br>

Usecase Provided By Related Video Series <br>


. . 

Intention Description <br>

Anticipation Description <br>

Title <br>

Description <br>

Benefits <br>

Features <br>

Limitations <br>

Community Restrictions & Guidelines <br>

. . 

Profile Resources <br>

Profile Provider Resources <br>

Profile Profile Resources <br>

Profile Restrouration Resources <br>

Profile Profile Restriction Resources <br>

Profile Profile Profile Resources <br>

Profile Profile Profile Restouration Resources <br>

Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile <br>

. . 

Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile Profile <br>

. . 

Profile Profile Profile Profile Profile Profile Profile  <br>

. . 

Member . .  <br>

. . 

Member Member Member Member Member <br>

Participant Participant Participant Participant Participant Participant Participant Participant Participant Participant Participant Participant <br>

. . 

Participant Participant Participant Participant Participant Participant Participant Participant Participant Participant Participant Participant <br>

. . 

Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member Member <br>

. . 

Janculator Janculator Janculator Janculator Janculator Janculator <br>

. . 

Janculatorium Janculatorium Janculatorium Janculatorium Janculatorium Janculatorium Janculatorium Janculatorium Janculatorium Janculatorium <br>

. . 

Janculatorium Janky Janky Janky Janky Janky Janky Janky Janky Janky Janky Janky Janky Janky Janky Janjuy Jankuy Jankuy Jankuy Jankuy Jankuyhiu Jankuyhiu Jankuyhiu Jankuyhiu Jankuyhiu Jankuyhi Jankuyhi Jankuyhi Jankuyhi Jankuyhi Jankuyhi Jankuyhi Jankuyhi Jankuyhi Jankuyhi <br>

. . 

Contact Information <br>
Community Restrictions & Guidelines <br>
News Updates <br>

Codebase <br>
Provider Resources <br>
Consumer Resources <br>
Usecase Resources <br>

Community Members & Volunteers <br>

Usecase Provided By Related Video Series <br>

. . 


. . 

Keep going <br>


[Post-Appended-Content]
[Post-Appended-On-April-27-2021]

Statistics <br>
Settings <br>





