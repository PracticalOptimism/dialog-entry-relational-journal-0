

# Development Journal Entry - April 3, 2021

[Written @ 23:52]


# The Light Architecture
[Table of Contents]()

## Community Development Team
[Table of Contents]() | 1 Company Listed

| **Company Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img size="24" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> |
-- | -- |
| **Company Name** | [E Corp](https://gitlab.com/ecorp-org) |

## Minimum Description
[Table of Contents]() | 1 Item of Interest

A data structure for organizing information.

## Median Description
[Table of Contents]() | 1 to 3 Items of Interest

A data structure for organizing information. If you are interested in organizing information, it can be a challenge because you can ask about 'space', 'time' and 'what do I need to know?'.

If you're not sure, then libraries like this one can help answer a lot of questions for you. 'Do you need to know this library?' Probably not, but if you do then you can learn about what the features are about.

One of the main features is (1) `help me organize this information`.

A secondary feature is (2) `recommend an idea for me to work on`.

A tertiary feature of the project is (3) `let me watch movies for free` and I may not get bored because it's like :O you take my recommendations into account.

## Maximum Description
[Table of Contents]() | 1 to 9 Items of Interest

<details>
<summary>(1) A Note About Space</summary>

### (1) A Note About Space

Thank you for reading this document. I have to begin this statement with a phrase like this. It is a space-time mechanic meant to stimulate your consciousness with an attitude for "yea, I love this content" But in all do respect, I am now going to be serious about what space means now.

Space is a difficult concept for me, personally, the author of this script to tell you about. To tell you about space is like telling a fish about water and fish are really specific in how they want to live their life. Right? Well, you may not agree with the previous statement that I've made.

Space is really interesting and you can say that (1) 3D mathematical euclidean space is the real space or (2) friendship groups on planets are the real space (3) and even more science fiction concepts which are theoretical and hypothetical are possibly the real space of interest like 'the space of colors on a specific red dress' or 'the space of candy flavored tastes on a specific rose-shaped candy that smells like a weird person on the internet'.

The space of people that are presented in a video sequence can be counted and re-arranged to make another video and so it's like 'do you need to know that other parallel reality sequence?' 'do you have the time for that?' 'do you want to create another memory?' Well, maybe that is a scary example for some people who wish their childhood was different. Well it can also be consoling to know that-that space of possible re-ordered events is also real. Right? Do you have the 'time' for it? Well?

Time machines are the next subject

</details>

<details>
<summary>(2) A Note About Time</summary>

### (2) A Note About Time

Time is an interesting concept. You create time with your conscious thoughts and feelings. That is a reference to a book by "Seth, Jane Roberts and Robert F. Butts". Time is a really secretive concept about 'who is holding the keys to what information?'

Timer keepers themselves are ignorant of where the keys are but that is a secret statement that I do not write often and don't need you to know that information so you can 'ignore it' or 'keep it a secret' until when you 'need to know it'

I'm not really sure what to say about time at this point - Jon Ide, author of this document photograph at this time of writing on April 4, 2021 @ 2:10 UTC, planet Earth, Solar System Sol, Constellation {Unknown to Jon Ide at this time of writing}, Galactic Jupiter Ring Perimeter {Unknown to Jon Ide at this time of writing}, Galactic Super Jupiter Ring Parameter {Unknown To Jonathan Ide at this time}, Milkyway Galaxy, Etc. Closure on the Time loop of where and when we are :O. I wonder if mars is :O wow, what time are you going to get here Earth people?

'Secretly, this or that time will do so it's like 'well, can we make time for that now or later?''

Now or later?

Do you need to know that now or later?

</details>

<details>
<summary>(3) A Note About Needing To Know</summary>

### (3) A Note About Needing To Know

Needing to know is an interesting concept. I don't know how to summarize this concept. I think, Jon Ide, the author of this project paragraph statement is trying to say something like 'when you need to know it' 'you'll know it' right? or 'when you think you've understood what you're looking at, then you've understood it' right?

Well, it's easy to make fun of someone for 'not knowing' something but it's like 'Yo, Jon Ide, that takes a lot of imagination to know when someone knows something'

If you play a video game, you can be so very familiar with where all the items in the game are but it's like even if you learn to program the game yourself, you can still be puzzled by how many more imaginary pieces to the puzzle are left for you to mine and uncover.

If you like something about a game, you can probably find a way to know about that thing. If someone wants to say that you don't know about that, it's like 'Hey, I recommend that you learn more about that . . '

Well, if you don't learn about it sufficiently enough, they may still be like 'yo, keep recommending me more ways to spam you in the face with information about 'yo, I recommend that you learn more about that''

How many different ways can you learn about something?

Well, if you run out of methods, you can always ask for help

</details>

<details>
<summary>(4) A Note About Nothing Else #1</summary>

### (4) A Note About Nothing Else #1

Nothing Else #1 is a system of consciousness about 'what do you not already know?'

What you don't know, won't hurt you right? Is that how the phrase always goes?

Gods, Ghosts and Monsters are always waiting behind the scenes to show you weird and freaky things that you're 'Needing to Know' Right?

Right? Right?

</details>

<details>
<summary>(5) A Note About Nothing Else #2</summary>

### (5) A Note About Nothing Else #2

Nothing Else #2 is a system of consciousness about 'why do you need to know that?' It's like O _ O There are spies waiting to not tell you anything because you're not 'mature' enough for that.

Well, if you don't adapt quickly enough, you'll always never know 'Nothing Else #2'

Nothing Else #2 is adaptable faster than your imagination. Right? Well, is that correct?

Try to imagine nothing.

</details>

<details>
<summary>(6) An Algebra For Eny</summary>

### (6) An Algebra For Eny

`Eny` is a group-theoretical concept about 'you can determine the space of energies' but you don't need to triangulate their exact co-determinator locations so it's like finding a friend in a dark corner but you didn't even know the corner existed. Do you ever use internet search like Google, or YouTube or Bing? Well, you can do that with unknown energies like 'what is that theoretical concept and why does it apply neatly in this corner of ideas over here?'

Well, group theory is incomplete because you have to summarize it with 'Eny' groups which are incomplete in themselves because titles cannot summarize energies.

`En` is a concept in algebra standing for 'Any thing you could possibly imagine'

`B` is a concept in algebra standing for 'Anything you could imagine but also stop there and write 200,000 computer programs to stop yourself from eating too many fish all at once because the program would kill you and all your species together all at once. Right?'

`E` is a big topic about, 'Right?'

`EE` is an even bigger topic about 'Are you alright? For typing that Mr. Jon Ide?'

E is a good place for us to stop right now, right everyone? 

</details>

<details>
<summary>(7) An Introduction To You</summary>

### (7) An Introduction To You

An introduction to you is quite complicated. I don't know.

</details>

<details>
<summary>(8) An Introduction To Proportionality Metrics</summary>

### (8) An Introduction To Proportionality Metrics

**I don't know.**

Intermediate Algebra is a style of algebra about 'do you know the proportionality constant between these two graphs of metric problems?'

'Do you know the graph of graphs between these two graphs?'

'Do you know the candle light vigil ceremony required to determine the proportionality metrics between these two sets of algebras?'

'Do you know?'

'Do you know?'

Do you know? Well, if you do know then you are really quite good. You are really quite good.

Good for you. Then you are really quite good. But if you don't know then your proportionality constant can be 'I don't know'

If you want to say 'I don't know' that is fine. But you don't have to. For me personally, Jon Ide, the author of this documentation (circa {{ currentDate }}) then you can say something like 'aya'

`Aya` is the name of 'I don't know'

`Ayanema` is the name for 'Nightmare assumption about 'I know''

Ayanema is like 'yea, I guess I know right? but it's like maybe, -\_- maybe, maybe -\_- -\_- -\_- you're maybe -\_- taking a lot of assumptions into account? -\_- like whyyyyy?'

-\_- To be honest I don't know so it's like I'm only writing notes to show you what is possibly a mathematical hierarchy system that is like 'I don't know -\_-' but it's like -\_- I don't know so it's like -\_-

</details>

<details>
<summary>(9) An Introduction To Areas of Interest</summary>

### (9) An Introduction To Areas of Interest

If you use 'ayanema' you can make assumptions about what you know and so it's like 'yea, I think that is interesting'

Right?

A lot of topics of interest exist and possibly you're an example of what that would be. You your personal self is really possibly quite interesting and a lot of people would possibly enjoy your content on the internet if you published your name and your work on the internet for people to index into their eye-hole sockets, right?

Well, if you don't have a topic of interest in mind, or a so-called 'Area of Interest' then that is a big deal.

You can use 'Areas of Interest' and 'Proportionality Metrics' to say 'Proportionality Metrics of Areas of Interest' which is a robotics language terminology for saying 'move this item over there'

If you like to move things, that's really cool and so you can 'move this idea over to that person and that would inspire them to work on this or that topic of interest and so it's like :O you're really moving the world forward to a theoretical probable future which is like ':O how do we get there?' or ':O was that an accident?''

Right, well it's really difficult to say which 'Area of Interest' this or that topic is but if you have a hierarchical list of 'areas of interest' such as (1) an ordered list of things like

```javascript
['yes', 'no', 'maybe', 'maybe not', 'maybe not not not', 'maybe super secret and this data item shouldn\'t be considered real']
```
and (2) sort that ordered list by various metrics like

```javascript
['yes i like that', 'no i don\'t like that', 'maybe i like that', 'maybe not i don\'t like that', 'maybe not not not i don\'t like that', 'maybe super secret and this data item shouldn\'t be considered real whether or not i like that or not']
```

which is an algebra syntax statement to say `(1) and (2) are basic chaos equation formulas for` ordering how you can spam the repeated alphabet of 'ordered additive statements' and get the same new result back for 

```javascript
'things that you like'
```

</details>


## Ideas That Are Recommended For You
[Table of Contents]() | 1 to 9 Areas of Interest Listed

| **Idea Index Number** | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| **Idea Category of Interest** | Animation Cartoon | Animation Video Game | Animation Movie | Nautical Engineering | Well Nice Try | Good Work | Awesome Job | Awesome Job | Cool |
| **Idea Minimum Description** | Do you like movies? Make a movie using our project. | Do you like video games? Make a video game using our project. | Do you like long-formatted movies? Make a long-formatted movie using our project. | Do you like swimming in spaces of interest? You should be an astro-nautical engineer and find more spaces for your friends to eat. | Do you like to be weird. Yea, well these are puzzles that no one has ever solved so nice try. | Do you like, doing things? Do a project to reverse engineer this project on your own. Why do you think it is useful? | Do you like this project so much? Well. Well. Well. Try something like to impress the Jonathan Ide | Do you like to spam random comments on the internet? Yes? No? Gravy. We would like to see more interesting ideas that you come up with :O | :D |

## Projects That Consume This Resource
[Table of Contents]() | 2 Consumers Listed

| **Consumer Icon** | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img size="24" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6481623/E-Corp-Logo.png?width=64" ></a> | <a href= "https://gitlab.com/ecorp-org" target="_blank"><img size="12" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/4919275/avatar.png?width=90" ></a> |
| -- | -- | -- |
| **Consumer Name** | [E Corp](https://gitlab.com/ecorp-org) | Jon Ide ([@practicaloptimism](https://gitlab.com/practicaloptimism)) |
| **Consumer Product Listing** | [Ecoin](https://gitlab.com/ecorp-org) | [OICP](), [non]() |

## Projects That Provide A Related Usecase
[Table of Contents]() | 0 Companies Listed

## What To Expect From This Community Document
[Table of Contents]()

### Table of Contents (A to E)

- (A) Project Inspiration
- (B) Project References
- (C) Project Guidelines
- (D) Project Intended Usecase
- (E) Project Accidental Usecase

. . 

## (A) Project Inspiration
[Table of Contents]()

. . 

I have a story to tell here.

. . 

## (B) Project References
[Table of Contents]()

There is a lot of talk in the local community here that would recommend all of these instruments for you to check out.

There is a lot of talk in the global community in general that would recommend all of these instruments for you to check out.

. . 

## (C) Project Guidelines
[Table of Contents]()

There is a set of rules that we follow here to stay compliant with recommended practice settings by our computer guide machine overlord sama which is why we are safe and peaceful here in the local and sometimes maybe global community as well.

There are a set of pigeon hole principles that allow our people to work in silence and peace. If you uppend those principles, you are a barbarian and are considered banned from working with our community. Thank you for establishing yourself as a member of these pigeon hole ideas.

. . 

## (D) Project Intended Usecase
[Table of Contents]()

If you want to do thing 1, you can perform it using our library.

If you want to do thing 2, you can perform it using our library.

If there are alien things that you would recommend that we should also do, please (write a proposal)[resource-url] for that with a single line sentence like 'I would love to see . . bananas part 2'

. . 

## (E) Project Accidental Usecase
[Table of Contents]()

If you use our library for malicious acting, we would recommend that you don't so that it is more likely for people to say 'yea, that is a good project library to use'

If you use other people's libraries in combination with this project library, make sure you follow their guidelines as well to try to avoid accidents

. . 

. . 



[Written @ 23:45]

# Project Name

## Community Development Team

If you have a team of companies or independent researchers or scientists, show them here

If you have a team of independent gurus working with your project counseling board, amazing, we would love to see that right away as well. Thank you so much for everything, amazing gurus

## Minimum Description

A single sentence of what your project is about.

## Median Description

A single sentence written in 3 or 4 different ways. Tell us what your project is about. Don't be pompous about what it's about. Don't be pompous about what it's about.

Yo, it's okay to break this guideline right? Just use 1 or 2 more sentences for that. :O

Okay

## Maximum Description

A list of paragraphs, video lists, image lists and more assets for you to show off for everyone who is like 'yea, I love having time to read this project description'. For a simple file like a README.md I would perhaps recommend a 3 paragraph sequence with photographs or animated gifs between each of the paragraphs and end with something extraordinary like 'yo, here is a graph of some really interesting stuff'

## Ideas That Are Recommended For You

If you are working with this project, try these ideas.

## Projects That Consume This Resource

Here is a list of 4 or 5 customers that enjoy working with our product.

## Projects That Provide A Related Usecase

Here is a list of 4 or 5 independent research efforts that are bombarding our workout space with a completely different product but it is also really interesting to think about how it relates to our present project.

## What To Expect From This Community Document

### Project Table of Contents

- Project Inspiration
- Project References
- Project Guidelines
- Project Intended Usecase
- Project Accidental Usecase

. . 

## Project Inspiration

. . 

I have a story to tell here.

. . 

## Project References

There is a lot of talk in the local community here that would recommend all of these instruments for you to check out.

There is a lot of talk in the global community in general that would recommend all of these instruments for you to check out.

. . 

## Project Guidelines

There is a set of rules that we follow here to stay compliant with recommended practice settings by our computer guide machine overlord sama which is why we are safe and peaceful here in the local and sometimes maybe global community as well.

There are a set of pigeon hole principles that allow our people to work in silence and peace. If you uppend those principles, you are a barbarian and are considered banned from working with our community. Thank you for establishing yourself as a member of these pigeon hole ideas.

. . 

## Project Intended Usecase

If you want to do thing 1, you can perform it using our library.

If you want to do thing 2, you can perform it using our library.

If there are alien things that you would recommend that we should also do, please (write a proposal)[resource-url] for that with a single line sentence like 'I would love to see . . bananas part 2'

. . 

## Project Accidental Usecase

If you use our library for malicious acting, we would recommend that you don't so that it is more likely for people to say 'yea, that is a good project library to use'

If you use other people's libraries in combination with this project library, make sure you follow their guidelines as well to try to avoid accidents

. . 

. . 










[Written @ 23:11]


# Project Name

Community Development Team

If you have a team of companies or independent researchers or scientists, show them here

If you have a team of independent gurus working with your project counseling board, amazing, we would love to see that right away as well. Thank you so much for everything, amazing gurus

Minimum Description

A single sentence of what your project is about.

Median Description

A single sentence written in 3 or 4 different ways. Tell us what your project is about. Don't be pompous about what it's about. Don't be pompous about what it's about.

Yo, it's okay to break this guideline right? Just use 1 or 2 more sentences for that. :O

Okay

Maximum Description

A list of paragraphs, video lists, image lists and more assets for you to show off for everyone who is like 'yea, I love having time to read this project description'. For a simple file like a README.md I would perhaps recommend a 3 paragraph sequence with photographs or animated gifs between each of the paragraphs and end with something extraordinary like 'yo, here is a graph of some really interesting stuff'

Ideas That Are Recommended For You

If you are working with this project, try these ideas.

Projects That Consume This Resource

Here is a list of 4 or 5 customers that enjoy working with our product.

Projects That Provide A Related Usecase

Here is a list of 4 or 5 independent research efforts that are bombarding our workout space with a completely different product but it is also really interesting to think about how it relates to our present project.

What To Expect From This Community Document

. . 

. . 



Project Inspiration

. . 

I have a story to tell here.

. . 

Project References

There is a lot of talk in the local community here that would recommend all of these instruments for you to check out.

There is a lot of talk in the global community in general that would recommend all of these instruments for you to check out.

. . 

Project Guidelines

There is a set of rules that we follow here to stay compliant with recommended practice settings by our computer guide machine overlord sama which is why we are safe and peaceful here in the local and sometimes maybe global community as well.

There are a set of pigeon hole principles that allow our people to work in silence and peace. If you uppend those principles, you are a barbarian and are considered banned from working with our community. Thank you for establishing yourself as a member of these pigeon hole ideas.

. . 

Project Intended Usecase

If you want to do thing 1, you can perform it using our library.

If you want to do thing 2, you can perform it using our library.

If there are alien things that you would recommend that we should also do, please (write a proposal)[resource-url] for that with a single line sentence like 'I would love to see . . bananas part 2'

. . 

Project Accidental Usecase

If you use our library for malicious acting, we would recommend that you don't so that it is more likely for people to say 'yea, that is a good project library to use'

If you use other people's libraries in combination with this project library, make sure you follow their guidelines as well to try to avoid accidents

. . 

. . 

# Project Name

Community Development Team

Minimum Description

Median Description

Maximum Description

Ideas That Are Recommended For You

Projects That Consume This Resource

Projects That Provide A Related Usecase

What To Expect From This Community Document

. . 

. . 

What To Expect From This Community Document

Project Inspiration
Project References
Project Guidelines
Project Intended Usecase
Project Accidental Usecase

. . 

Project Inspiration

. . 

Project References

. . 

Project Guidelines

. . 

Project Intended Usecase

. . 

Project Accidental Usecase







[Written @ 22:55]


Template Idea For How To Organize A Project README.md:


# Project Name

Community Development Team

Minimum Description

Median Description

Maximum Description

Ideas That Are Recommended For You

Projects That Consume This Resource

Projects That Provide A Related Usecase

What To Expect From This Community Document

. . 

. . 

What To Expect From This Community Document

Project Inspiration
Project References
Project Guidelines
Project Intended Usecase
Project Accidental Usecase

. . 

Project Inspiration

. . 

Project References

. . 

Project Guidelines

. . 

Project Intended Usecase

. . 

Project Accidental Usecase






