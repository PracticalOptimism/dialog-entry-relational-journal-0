

# Development Journal Entry - April 27, 2021

[Written @ 13:35]




[Written @ 13:26]

`Dot Dot Dot` is another notation for Blah Blah Blah . . 

`Blah Blah Blah` sounds sexier since it doesn't assume anyone is really interested in whatever comes next . . blah blah blah is possibly not supposed to be used everywhere since it really does seem to demean energies and relax them to feel less than and less than and so it's like one of those 2 year old people who walks around whispering blah blah blah for many days . . 

. . 

Dot Dot Dot is maybe nice to say but it also doesn't necessarily help you say 'and then some more' and '`and then no more than all that was`'

'`no more than all that was`' is a space-time notation for '`you couldn't even begin to possibly imagine`'

. . 

Notes @ Newly Created Words

`emagine`
[Written @ April 27, 2021 @ 13 hours:31 minutes:31 seconds]
[I typed the word 'emagine' instead of 'imagine' to get a point across while typing the phrase 'you couldn't even begin to possibly emagine' and the point was that 'emagine' is so new and so unfamiliar to possible people of interest that it was like an 'emigrated' term of the 'imagination' from one 'imagination' place to another and so you can travel between imaginations and see 'thanks for sharing that' 'emagine' is like an nice emigration portal for new imaged ideas but those are so new and numerous you don't really have a space-time goggles for you to present an image in your face to see those things . . space-time goggles are like 'nice bi-focal lenses' but the realm of the mind and the realm of the imagination can help you focus less and less on space time imagineations . . O _ O ]



[Written @ 13:20]

`Blah Blah Blah` . . Data Structure Notation . .

. . 

`For-Loop` Notation . . 

. . 

Blah Blah Blah . . Can Help You Flat-Map . . For-Loops . . To Space Order Notation just as you would like for . . O _ O . . 

. . 

. . 

Blah Blah Blah is an acronym for 'are you kidding me?' 'who needs to know the name?' which is kind of rude and disrespectful towards a person because names are useful for people and also there are many beautiful names to learn about and you don't want to say that you don't want to learn that person's name since it can be so unique and precious and lovely . . and so it's uh . . only what mathematicians with gigantomastia intention say when they aren't focused on being a kind person who treasts people like good human beings and not generalized mathematical ideas . . 

. . 

. . 



[Written @ 13:03]

Tall Order

[Written @ 12:56]

[This was channeled by a ghost]:

`Tall Order`

- Same height in the directory tree
- Same height in the directory tree listing
- Same height in the directory tree listing but plus or minus an order of magnitude like 1, 2 or 3
- Same meaning
- Similar meaning
- Similar meaning with some edge cases
- Similar meaning with some edge cases that can be tall ordered proportional turned on and off by the reader of the document
- Similar order to interesting ideas that are the tallest of all tall mountains which are like amazing space bearing civilizations
- Hypothetical tall, no real measure
- No real measure of the height
- No real measure of the influence
- No real measure of how far
- No real measure of how long
- No real measure of recommended parameters
- No real measure of influential ideas
- No real measure of O _ O ghost specific ideas that require ghosts to interpret
- No real measure of individuality
- No real measure of communality which is like individuality but commutable to individual ideas O _ O but is spooky
- No real measure of spookiness
- No real measure
- No
- No
- No hypothetical
- Order of Magnitude Totality
- Order of Magnitude Completeness
- Order of Magnitude Enderson
- Order of Magnitude Endera
- Order of Magnitude Enderela
- Order of Magnitude Nonsense Schema
- Order of Magnitude Wow
- Order of Magnitude Well
- Order of Magnitude When
- Order of Magnitude Okay


[Written @ 12:56]

[Copy-Pasted from the YouTube Livestream Chat for Ecoin #539]

Well we should say that this list of things inside of each tall order folder is really useful . . well . . we may not use it . . but it may be useful . . and so it's like /slash-shrug why not copy whatever precompiled is doing?. . . . . I like how we've organized 'precompiled' so far . . so it's like /slash-shrug . . do your counterparts need to look so different ? . . I'm not sure . . and counterparts here means tall order counterparts which are like 'sibling-order' counter parts or individuals with the same 'height' 'tall order' 'height order' 'height order' . . something like that . . 'same height' in the directory tree . . 'tall order' so . . 'tall order' is quite 'tall' in how 'meaningful' it is 'tall' O _ O . . 'tall' . . 'tally'

[Written @ 12:25]

I kind of what to go back to whatever else I was doing . . I will answer these questions at another time O _ O but wow . . amazing . . 

[Written @ 12:23]

Minimum Description List: `2 Items of Interest`, `2 Questions`, `Questions`, `Mystery`, `Hypothesis List`, `Problem Solving Ideas`


What is that scaling related or scaling-anticipated idea for 'names of interest'?

. . 

O _ O 

. . 

What is that continued 3 or 4 item list related to `function names`, `service items` and `names of interest` . . ? . . O _ O 

[Written @ 12:05]

We have a list of `function names` and we have a list of `service items` . . and for some reason the `function names` and `service items` seem to make a mapping of some kind where we can associate the function names and service items while making sense of what they do independently from one another . . which is quite magical by the way . . I didn't realize it until some many few weeks ago . . O _ O . . O _ O . . maybe 2 or 3 weeks ago but that is also proportional to 1 month or 2 months . . 

O _ O . . 

Well . . uh . . there is an algebra for that one-to-one mapping that is shown here:

- (1) Get             -> Database
- (2) Create          -> Network Protocol
- (3) Update          -> Event Listener
- (4) Delete          -> Permission Rule
- (5) Recommend       -> Anana Anana Anana
- (6) Scale           -> Scale Matrix Amana
- (7) PerformAction<Period><Age><Name><Rule><Service>                                               -> Computer Program Manager

. . 

And now we are working with . . `names of interest` which is like a way to ask the question of 'if we are constructing a name . . how do you think that name should be constructed?' . . what 'variables' should we take into account? . . 

Racism matters so we are really amortizing all of these names into something that is 'species specific' or 'race specific' for 'human beings' and not necessarily 'other alien beings that don't interpret the world the same way that we do'

And so . . O _ O . . well . . Those names are listed there . .

. . 

- (1) Get             -> Database                 -> Gender
- (2) Create          -> Network Protocol         -> Name
- (3) Update          -> Event Listener           -> Age
- (4) Delete          -> Permission Rule          -> Period
- (5) Recommend       -> Anana Anana Anana        -> Service
- (6) Scale           -> Scale Matrix Amana       -> ???
- (7) PerformAction<Period><Age><Name><Rule><Service>                                               -> Computer Program Manager -> Rule

. . 

Well now our mapping is even more magical and mysterious to have a 3-way binding O _ O of Functions, Services, and Names O _ O and you know what else is even more mysterious about this list of `Functions`, `Service`, and `Names` ? . . there is a theory about O _ O . . that seems to suppose that our list of 3 items here isn't even complete . . we should have . . 3 or 4 more items on the list to ake it O _ O . . sppoooookkkyyyyy . . and accessssiiibllleeeee forr ennggeerrrgggyyyyyy O _ O . . do you see what I'm talking about ? . . O _ O 

`Function Names`, `Service Items`, `Names of Interest` . . This is a list of 3 items . . but all our other lists . . for each of those items specifically shows . . 6 or 7 items O _ O . . that means . . our list here is incomplete right ? . . or at least we can suppose that there is a hypothetical continuation of that list of things that we have here O _ O . . which is like O _ O . . oh my gosh . . what a weird and spooky coincidence . . spooky is kind of like a 'period' term since many people could possibly get offended by that word . . and so O _ O . . spooooooooooooooooooooooookkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkyyyyyyyyyy . . and when it comes to 'being offended' that is like O _ O permission rule O _ O . . I don't like you . . you are banned from the server . . you don't have permission to be here . . and also . . Delete . . delete . . delete . . O _ O . . spooookkkkyyyyyyyyyyyyyyyyyyy how offensive . . and spooooooookkkkkkkkkkkkkkkkkkyyyyyyyyyyyyyyyyyyyyyyyyyyyyy

I'm sorry for showing you that word sppokkkkkkyyyyyyyy several times but please forgive me as I try to figure out why these things are being so coincidentally delivered from spooooookkkkkkkkkkkyyyy ville . . 

O _ O . . 

O _ O . . 

It's like discovering a whole new treasure trove where those treasures were only hypothetical and supposed before but now your map is like O _ O . . I was only a part of an even greater map O _ O . . Hunter X Hunter style O _ O . . Spooky .

O _ O . . 



[Written @ 11:58]

Function Names Of Interest:

- (1) Get
- (2) Create
- (3) Update
- (4) Delete
- (5) Recommend
- (6) Scale
- (7) PerformAction<Period><Age><Name><Rule><Service>

Service Items Of Interest:

- (1) Database
- (2) Network Protocol
- (3) Event Listener
- (4) Permission Rule
- (5) Anana Anana Anana
- (6) Scale Matrix Amana
- (7) Computer Program Manager

Names Of Interest:

- (1) Gender
- (2) Name
- (3) Age
- (4) Period
- (5) Service
- (6) ???
- (7) Rule



[Written @ 11:34]

Minimum Description List: `recommendComputerProgram`, `scaleComputerProgram`, `Computer Program Manager`, `Service Item`

[Copy-Pasted from the YouTube Livestream Chat for Ecoin #539]

(1) create (2) delete (3) get (4) update (5) recommend (6) scale . . . recommendComputerProgramForTally . . hmm . . this is really an interesting one . . A computer operating system with this function is really possibly interesting . . it's like . . google search . . uh . . meets computer programming and so uh . . all uh . . the world's information could help provide recommendations for uh . . what else this computer program should do . . or something like that . . and uh . . possibly uh . . those could be uh . . functions that can then be automatically evaluated . . or something like that 😑 hmm . . it's kind of not a clear thought in my head right now . . energy meter is also an interesting terminology . . I'm not completely aligned with it but it's still interesting to consider . . Now I am going to copy-paste what was from 'recommend-tally' from the 'database' service but also try to map onto the 'computer program manager' in a nice way that seems to make sense . . or something like that . . Now . . I'm going to copy-paste what is in 'scale-tally' and approportionate it to the the computer program manager usecase . . 'computer program manager' service item . . 

[Written @ 11:23]

Notes @ Newly Learned Words Which I thought Were Original But They Look Already Defined In The Dictionary

Approportionate
[Written @ April 27, 2021 @ 11:29]
[Appropriately, Proportionate, a new term . . right? . . maybe not . . maybe not . . probably not . . probably not . . almost possibly not . . right?]



[Written @ 11:15]

Settings <br>
Statistics <br>

Maybe these are interesting related terms . . 

Reading . . Statistics . .  <br>
Writing . . Settings . .  <br>

. . 

Well Settings are Read and Written . . but uh . . Statistics seem to be mostly Read by the Program User and not necessarily Written . . I'm not really sure if this analogy goes further that what I've written here -_- . . but also these are lame ideas . . 

. . 

Statistics . .  <br>
Settings . .  <br>

-_- . . 

. . 

Well . . It's nice to at least write these ideas . . since uh . . there is a long list of 'what terms are useful for what usecase?' . . and so if you read the [Development Journal Entry - April 26, 2021] . . you will see an even longer list that 'Statistics' and 'Settings' . . uh . . well something something . . 

[Written @ 11:09]

Minimum Description List: `Describe inspiration for updateComputerProgramEnergyMeter`, `Computer Program Manager`, `Service Item`

[Copy-Pasted from the YouTube Livestream Chat for Ecoin #539]

(1) create (2) delete (3) get (4) update??? (5) recommend??? (6) scale??? . . . update . . well . . as with the function 'get' . . we don't want to say 'update database item property' or something like that . . the equivalent . . computer program manager item would be what? . . I haven't figured that out yet so that's not a rhetorical question right? . . I'm working through this with you the audience reading so you can be like 😲 oh I see 😲 . . updateComputerProgramForTally hmm . . this name isn't bad but what do you think it does 😲 😑 it does weird things 😑 . . 😲 you need to tell yourself you're not updating the statistics of the computer program either since that's read-only information provided by the computer program manager right? . . seems to make sense if I try to proportionate that to computer operating system terminology that I'm familiar with . . alright . . updateComputerProgramForTally . . `updateComputerProgramEnergyMeterForTally` . . EnergyMeter could possibly be like a measurement for what resources are being provided for here and there for this program as it operates . . and so for example (1) evaluating a new function could be an energy meter for this . . uh . . function to carry through . . and so 😲 that gives me an idea that we generalized the 'evaluateFunctionForTally' idea from earlier . . 😲 . . 


[Written @ 10:57]

Minimum Description List: `Describe inspiration for getComputerProgramStatistics`, `Computer Program Manager`, `Service Item`

[Copy-Pasted from the YouTube Livestream Chat for Ecoin #539]

(1) create (2) delete (3) get (4) update??? (5) recommend??? (6) scale??? . . my natural intuition at this time is telling me that 'get' does not make sense . . because we are not trying to say 'get this item from the database' . . we are trying to say something specific about 'computer program manager' ideas . . 'computer program manager' ideas seem to relate to 'starting' and 'stopping' computer programs in my own humble opinion at this time . . and so . . 'get' really needs not to look like the database equivalent of 'get that item from the database' it could possibly be transformed to . . 'getAccessInformation' uh . . which is possibly a clarification on statistics about how the computer program is being uh . . developed inside of the . . uh . . runtime engine or something like that . . so . . getComputerProgramStatisticalInferenceMachineInstructions is possibly a mapping idea . . uh . . `getComputerProgramStatisticsForTally` . . getComputerProgramStatistics . . statistics as a term itself hasn't really been clarified by myself 😑 it is maybe like a 'connection' list or a 'whereabout' list where you can find 'information about where you think you are with respect to this program's runtime' and so 'normal statistics information like date started and other things are possibly there' . . 

### Additional Notes:

'getComputerProgramStatistics' seems to be possibly equivalent to the database terminology for 'getDatabaseItemStatistics' or 'getDatabaseItemMetadata' or 'getFileMetadata' or something like that . . and so . . uh . . well . . something like that . . it seems like 'metadata' is a nice name to remember in this case since files have pre-populated fields of information provided for by the 'computer operating system' or 'computer program manager' . . 

'`metadata`' . . it's kind of a strange term possibly . . since 'how meta is it?' really is a question that is like O_O do I need to think a lot about what is happening?

'statistics' is possibly like another alternating term . . since it is also possibly 'meta' in those terms of 'O_O how much do I need to think about it?' but it's it's like 'now I have a tangible name object and put in a neckhold because it's so easy'

'statistics' relates to observable phenomenon and so possibly it's like O_O . . 'statistics about ghosts are interesting' O_O 'ghosts' . . 

'meta' seems like a clairvoyant terminology for O_O . . 'you can't really touch it' which is like O_O programming would be interesting if you couldn't really O_O program right? O_O

. . 

'statistics' at least lets us feel as though we think we are going to program and there will be a list of statistics about how well we are programming . . and how our heartbeat is while we're programming . . and so it's like O_O . . cool let's share that information with other programmers . . let's share that information . . 

'meta' seems like you can't really share that information O_O it's too . . 'quote-on-quote' 'deep' . . 'deep' . . 'deep' . . 'meta' . . meta fabrics are like O_O . . ~@~!~@! . . ~#!~!~@ . . 


[Written @ 10:17]


