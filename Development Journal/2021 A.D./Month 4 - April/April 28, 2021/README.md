

# Development Journal Entry - April 28, 2021

[Written @ 19:24]

I have a headache. I'm going offline for a few moments . . see you later

[Written @ 18:06]

project-service-website
- /precompiled
  - /usecase
    - /inherit-digital-currency-course-ware
      - /algorithms
      - /constants
      - /data-structures
        - /anana-anana-anana
          - /home-page
            - /digital-currency-account-introduction
            - /digital-currency-account-suspended-belief
            - /digital-currency-account-product-listing-panels
          - /network-status-page
            - /network-status-introduction
            - /network-status-product-listing
            - /network-status-digital-currency-product-listing-panel-list
            - /network-status-product-resource-product-listing-panel-list
            - /network-status-network-recycling-center-product-listing-panel-list
            - /network-status-digital-currency-exchange-product-listing-panel-list
            - /network-status-job-employment-product-listing-panel-list
          - /settings-page
            - /settings-introduction
            - /settings-product-listing-panels
            - /
          - /about-page
            - /about-introduction
            - /about-readme-description
            - /about-product-feature-list
            - /about-product-question-and-answer-description
            - /
        - /database
          - /digital-currency-items-of-interest
            - /product-listing-panel
            - /digital-currency-promotional-item
              - /digital-currency-symbol
              - /digital-currency-price-listing
              - /digital-currency-payment-button
              - /digital-currency-user-interest-item
            - /digital-currency-account-promotional-item
              - /minimum-promotional-item
            - /digital-currency-transaction-promotional-item
              - /minimum-promotional-item
            - /digital-currency-transaction-panel-body-content
              - /create-content
              - /show-content
              - /update-content
                - /account-settings
                - /memorandum-settings
                - /payment-amount-settings
                - /product-resource-settings
                - /network-recycling-center-settings
            - /digital-currency-account-panel-body-content
              - /create-content
              - /show-content
          - /product-resource-items-of-interest
            - /product-listing-panel
            - /product-resource-promotional-item
              - /product-resource-shop-here-button
              - /product-resource-product-support-listing
              - /product-resource-product-promotional-item-search-listing
              - /product-resource-product-promotional-item-website-listing
              - /product-resource-product-promotional-agreement-list
            - /product-resource-panel-body-content
              - /create-content
              - /show-content
              - /update-content
            - /
          - /network-recycling-center-items-of-interest
          - /digital-currency-exchange-items-of-interest
          - /job-employment-items-of-interest
        - /network-protocol
          - /network-protocol-connection
          - /network-protocol-member
            - /website-header-item
            - /website-footer-item
            - /website-promotional-item
            - /website-panel-item
            - /website-overlay-panel-item
            - /website-panel-header-item
            - /website-panel-closing-item
            - /
          - /network-protocol-message
        - /event-listener
        - /permission-rule
        - /scale-matrix-amana
        - /computer-program-manager
      - /infinite-loops
      - /variables
  - /consumer
  - /provider



Notes @ Newly Created Words

diaply
[Written @ April 28, 2021 @ 18:08]
[I accidentally typed the word "diaply" instead of "display" . . I'm sorry . . I'm not sure what this word "diaply" should mean at this time . .]

[Written @ 17:48]

The Light Architecture Previous Description:

A finite size computer document data structure for high orders of information representation

A data structure for information representation. A tally is the technical name for this data structure type.

[Written @ 10:21]

Cognitive Summation Game

Benefits + Limitations = Choice

. . 

Benefits = Type1 * Type2
Limitations = TypeA * TypeB

. . 




### Benefits

(1) Use The Love Algorithm Name
- Because it looks cool! Love and Light :D
- 

(2) Use The Light Architecture Name
- Because it is easy!
- 

### Limitations

(1) Use The Love Algorithm Name
- Change the README.md for `the light architecture` to exclude creating and recommending features which makes the user sad :(
  - Fix with: 'the-love-algorithm' proxy naming (ie. `Uses the-love-algorithm`)
- Are we missing out on -_-

(2) Use The Light Architecture Name
- 


[Written @ 9:39]

Minimum Description List: `Notes on Service Items in Data Structures Folder`, `Service Items`, `Data Structures`, `Algorithms`, `I was confused`, `But now I think I understand`, `Use service items in data-structures as well as algorithms`, `service items as direct children folders of the computer program indicators makes sense`, `something like that`, `may need to do more research`, `not sure`

[Copy-Pasted from YouTube Live stream chat window]

(1) usecase/service/<usecase-name> . . and so <usecase-name>/data-structures/database/ . . the database folder should contain items that are specific for having direct relation to the 'usecase-name' and so for example . . if your usecase is 'digital-currency' . . it makes sense to have your `data-structures/database` contain 'digital-currency' and 'digital-currency-account' and other digital currency related data structures . . and so in our case with using . . 'tally'. . . we are having 'tally' specific data structures in /data-structures/database . . whereas . . (2) . . if we have a usecase-name that we aren't applying DIRECTLY . . and are instead not really using the 'usecase-name' as with 'consumer-ready-user-interface-item' at this time of writing . . since a general consumer-ready-user-interface-item is possibly not the purpose of this project . . and so having 'consumer-ready-user-interface-item' data structures is not going to be applied and so /data-structures/database will be an empty directory . . etc . etc . etc . . so because we're not having a general . . account for answering the question of 'what is the data structure name? and how does it relate to the usecase name?' because we are answering the question with 'readme user interface' which is not a general user interface . . we will say that it belongs to 'anana-anana-anana' meaing 'unspecified' or 'unknown' how this type of data structure relates to the usecase name titled 'consumer-ready-user-interface-item' . . these particular data structures outlined here with the mouse cursor . . 'consumer-ready-user-interface-item' 'consumer-ready-user-interface-item-other-data-structure' and 'consumer-ready-user-interface-item-other-data-structure-2' are not going to be implemented because we don't have a specific usecase for them . . what I mean by a specific usecase is that . . personally I haven't been able to think that . . 'yea' . . 'yea' . . 'yea' . . 'yea, blah blah blah' for example . . my initial idea was to say that 'readme.md' the README.md file needs to be a user interface that we provide for the consumer of this project and so . . for example we should be able to create a README.md file . . and so . . uh . . something like that is not being specifically related to 'consumer-ready-user-interface-item' . . it's only tangentially related . . and we don't even know how that relation is uh . . tangential to the 'consumer-ready-user-interface-item' so we will specify that it is in 'anana-anana-anana' since that is a catch all relational area for us to realize that we don't really know . . 


[Written @ 7:26]




[Written @ 7:23]

A User Interface is really quite a strange device . . compartmentalizing a user interface is weird . . it seems to be anana anana anana complete meaning it's hard to really distinguish these items from one another . . without further rapidity which could possibly make archiving and reading as a human user very difficult . . and so . . 

. . 

miscellaneous really does mean something . . and dialogue really does mean something since it's like on going and repeditive and repeditive even within any given moment point right? I'm not sure . . 

I'm not sure . . 

I'm not sure . . 

[Written @ 7:07]

```javascript
class TallyImportantMessagePreviousVersion {
  service = null
  serviceDescription = null // new
  serviceAccount = null

  databaseItem = null
  databaseItemList = null
  databaseItemArrangement = null // new
  databaseItemComforter = null // new
  databaseItemIgnoramus = null // new
  databaseItemIdea = null // new

  networkProtocolConnection = null
  networkProtocolMember = null
  networkProtocolMessage = null

  permissionRuleWarning = null

  computerProgramAction = null // new

  eventListenerAnnouncement = null // new
  thankYouessageForGratitudeMeditation = null // new
  imSorryMessageForAdditionalComfortAndSupport = null // new
}
```

. . 

Okay . . the previous message described here is listed with more items of interest that were recommended to me by the spiritual Ghost Entity Energy Seth . . Well . . this list was further refined to start from 'scratch' with a new list . . that is possibly better for a lot of things . . a lot of important messages could not possibly be explained or described with further rapidity if you have only the listed property attributes here . . and so for example . . 'networkProtocolConnection' is possibly the best example of a 'catch all' approach but it is still useless if no one knows when and when not to use it or else otherwise you would possibly have a lot or maybe most of the things needing to be described as 'networkProtocolConnection' which can be confusing and non-informant for a new beginner to the software . . or a new user to the software or a new beginner to the project . . This listing should be more important to describe common ideas for all important messages . . and no need to enumerate them any further . . 


[Written @ 6:24]



