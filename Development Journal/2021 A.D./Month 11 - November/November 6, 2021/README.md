

# Development Journal Entry - November 6, 2021

### [Written @ 9:49]

I'm not exactly sure how to synchronize the time across many devices such as web browser tabs and daemons running locally on my local computer as well as possibly daemons running somewhere in the public cloud environment.

I think the solution that I found so far has to do with using UTC. 

.......................................................................

You can convert a date from local time to UTC time. And I tested how the browser website looks on multiple devices so far. And the UTC date looks the same across all of these multiple devices.

That experiment of looking at different web browser results for the website https://ecoin369.web.app has shown that the UTC time didn't vary across the devices even though the local time sometimes varied.

I still need to experiment with the results of what a daemon outputs. If the daemon that runs on a Glitch (https://glitch.com) or Heroku (https://heroku.com) server can show that the UTC time isn't varying a lot even though the time zone is different from possibly where my own local computer shows a local time zone difference of 240 between the local time and the utc time. . . Those results will be promising that UTC is a really possibly good way to synchronize the time across these devices.

.......................................................................

Being able to convert from UTC to local time is also possibly important.

.......................................................................





