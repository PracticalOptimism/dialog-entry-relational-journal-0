

# Development Journal Entry - May 1, 2021


[Written @ 17:38]

Tally Size

. . 

Tally Size: 1 has 1 file<br>
Tally Size: 2 has . . a list of 2 or more files . . <br>
Tally Size: 3 has . . a directory tree of 2 or more files . . <br>
Tally Size: 4 has . . a directory tree of 2 or more files . . <br>
Tally Size: 5 has . . a directory tree of 2 or more files . . <br>

. . 

service.js

. . 

compiled.js<br>
compiler.js<br>
precompiled.ts<br>
dialogue.ts<br>

. . 

algorithm.js

. . 

item.js

. . 

item-with-cat-in-photograph.png<br>
item-with-dog-in-photograph.png<br>
item-with-zebra-in-photograph.png<br>

. . 

item-with-cat-1-in-video.mp4<br>
item-with-cat-2-in-video.mp4<br>
item-with-cat-1-in-video-2.mp4<br>

. . 

item-with-service-account-photograph.png

. . 

service-account-photograph.png<br>
service-account-banner-image.png<br>
service-account-introductory-video.mp4<br>
service-account-3d-model.json<br>

. . 

/service-for-storing-cat-photographs/
  - /cat-photographs-set-1
  - /cat-photographs-set-2
  - /cat-photographs-set-3

. . 

data structures inspire the algorithm . . Algorithms perform actions . . actions can involve reading and writing or writing information to a file system . . or writing information to a network protocol like http . . 

. . 

other actions include . . writing information to http network protocols . . writing information to file system network protocols . . writing information to robotics drivers network protocols . . and writing information to other network protocols of interest . . 

. . 

Import a network protocol service provider 

. . 

'where are you trying to update that?'

'where are you trying to receive that?'

'where are you trying to get that from?'

'where are you trying to get that recommendation from?'

'where are you trying to scale that?'

. . 

OICP is both (1) a file system reader and writer program as well as an (2) http network protocol service reader and writer program . . 

. . 

OICP provides 1 interface for both types of network protocols . . and so any web service with OICP-standard http-protocol query methods such as path and query parameters is able to support a standard 'file system' for updating or requesting or making certain network requests on the http network just as they would for file system network requests . . as long as they have permission rule keys for making those requests satisfied . . then those items of requests could possibly go through . . or . . be satisfied . . or . . be approved . . or something like that . . 

. . 

A file system reader is useful . . but another type of file system reader is a 'file-concatenation-system' reader which is possibly like the type of program that 'webpack' and 'rollup' are . . but this is a hypothesis and not necessarily the best name for this type of program . . 'file-concatenation-system' which is like a type of 'network protocol' since it connects files together . . uh . . something like that . . and so import statements work properly . . and export statements work properly . . something like that . . 

. . 

A file-concatenation-system network protocol like 'webpack' and 'rollup' . . should possibly work in the web browser for arbitrary string files . . and so . . if they do not . . that is kind of interesting . . 

. . 

OICP needs to also be a 'file-concatenation-system' network protocol . . and so that would help it be a network protocol for that type of information system as well . . and so that's really cool . . well . . it's cool because you could possibly do other things with that type of network protocol . . but uh . . I'm not sure . . mostly I'd like to have file concatenation working . . uh . . inside of a web browser . . and not only on a file system uh . . idea . . or file system traditional database / data store / data structure location idea . . or something like that . . 

. . 

Network Protocol Connection <br>
Network Protocol Member <br>
Network Protocol Message <br>

. . 

. . 

Connect the files . . Transpile the files . . And the result is the Compiled.js . . 

. . 

When the files are connected . . that's very useful . . 

. . 

(1) file system
(2) file listing (string list)
(3) http
(4) space-efficient transpiling
(5) time-efficient transpiling
(6) need-to-know-efficient transpiling
(7) nothing-else-efficient transpiling

. . 

Network Protocol Connection
- Do you have permission to create a connection between 2 members?
Network Protocol Member
- Do you have permission to send a message from one member to another?
Network Protocol Message
- Do you have permission to evaluate the message without encryption?

. . 

(1) file://
(2) file://
(3) http://, https://
(4) firebase, google drive, etc.

. . 

dat://

ipfs://

. . 

youtube://

. . 

webpack://

. . 

rollup://

. . 


. . 

(1) File System: 'file:///Users/xevavex/Desktop/unicole-unicron.jpg'
(2) File Listing: '../../../../data-structures/database/tally/item'
(3) HTTP: 'https://youtube.com/api/video/abc-123'
(4) IPFS: 'ipfs://ipfs.io/abc-123-hash/something'
(5) DAT: 'dat://abc-123-public-key/something'
(6) Git: 'git://abc-123-hash'

. . 

Absolute paths . . 

. . 

Relative paths . . 

. . 

Absolute Paths . . are really useful . . if you have an absolute path . . that is really useful . . but for some reason . . a lot of relative paths are available in software versions like TypeScript . . where a relative path is available for a user to have convenient access to programs in a relative directory tree . . possibly another syntax format needs to be available for dedicating ourselves to an absolute path standard . . 

. . 

Absolute Path . . Absolute . . path . . is useful . . for example if you have runtime updates for where a file is . . you may have a path that is still useful for saying that you can have a relative location of where to search for the expected file item . . 

. . 

import { Tally } by 'oicp://hahaha-hahaha-hahaha/item'

import { Tally } from 'oicp://hahaha-hahaha-hahaha/item'

. . 

. . 

Relative paths are still quite useful . . 

. . 

Absolute path imports could possibly eliminate the use of 'node' or 'node_module' imports . . or 'node_modules' relative imports . . since those could possibly be imported . . uh . . as needed . . when they are used by the program . . and possibly cached . . uh . . as needed . . 

. . 

Uhm . . I suppose . . 'compiled' could also be a nice folder directory to uh . . possibly relate to the 'node_modules' folder uh . . and so . . it's like you can place all the runtime archived files from the internet into . . the 'compiled' folder . . with something like 'compiled/provider/service/service-name/constants/@service/compiled/item.js' or something like that . . 

. . 

And so it's really interesting to imagine . . a compiled folder directory could be available . . at runtime when needed . . and possibly . . oicp can start a computer program with absolute paths . . with oicp start compiled.js or something like that . . 

. . 


precompiled.ts

. . 

oicp compile precompiled.ts // -> provides compiled.js

oicp start compiled/usecase/service/compiled.js

. . 

- precompiled.ts
- /compiled
  - /provider
    - /service
      - /service-name
        - /constants
          - /@service
            - /compiled
              - item.js // imported at runtime after 'oicp start compiled/usecase/service/compiled.js'
  - /usecase
    - /service
      - compiled.js

. . 

Importing . . at runtime . . is very cool . . 

. . 




[Written @ 16:13]

import { Tally } from '../../../../data-structures/database/tally/item'

function createTallyAlgorithm () {
  // 
}

class CreateTallyAlgorithmService {
  createTallyAlgorithm
}

const _service = {
  defaultServiceClass: CreateTallyAlgorithmService,
  defaultServiceInstance: new CreateTallyAlgorithmService()
}

export {
  _service
}

[Written @ 15:54]

permissionRuleWarning

warning <br>
answer <br>
action <br>
collon <br>

-_-

. . 

action

. . 

answer

. . 

warning

. . 

collon

. . 

TallyInsignificantErrorWarningIndicator

. . 

warning . . 



[Written @ 14:26]

permissionRule

permissionRuleWarning

expectationDefinition

anticipationDescription

intentionDescription

. . 

Intention Description is like a computer program 

Anticipation Description is like a unit test or expectation definition for how a computer program is expected to behave

. . 

Anticipation Description is maybe not a better name than 'Expectation Definition' for some community circles possibly such as 'academic circles' that can trademark the language around in their office corners and not only for mathematics circles for example which tend to be general and superfluous to necessary applications . . but also for individuals in biology or physics or chemistry or something like that . . I'm not sure . . 

. . 

Expectation Definition . . 

. . 

Anticipation Description . . 

. . 

Expectation Definition . . 

. . 

Expectation Definition . . 

. . 

Expectation Definitions . . 

. . 

Permission Rule . . 

. . 

Permitting the program to apply within certain expectation criteria . . such as measuring its effect within a few variable translations . . and so for example . . updating variables is a useful application to measure after operating or evaluating a function of interest . . or an application usecase of interest . . 

. . 

Permission Rule . . Permission Type . . Permission . .

. . 

Permission Type possibly needs to be 'expectation-definition' type . . which could possibly allow for 'make sure that this operation will indeed effect the program within these measurement constraints or else . . respond with a warning . . that the program didn't achieve that . . ability . . so in effect this could also possibly provide users of a computer program with runtime access to 'ensuring the program is indeed operating within the measurement constraints . . uh . . as per the user operator's expectation definitions . . or something like that . . It's maybe a 3rd case as opposed to the first 2 cases which are regional and related to (1) updates of data structures and (2) permitting the user to run a function of interest . . and so . . this 3rd and final usecase could possibly be related to 'do you know what you know?' which is possibly quite a special usecase since I haven't run upon this idea before until now as Seth has been typing that related idea . . 

. . 

(1) space order -> permission to update,get,create,delete data structure
(2) time order -> permission to perform operation or evaluate and unevaluate or uninitialize function evaluation
(3) need-to-know order -> permission to expect that the operation will perform what was expected of the operation
(4) nothing else #1 -> permission to be recommended other observation criteria for how to measure the accessibility of that usecase to that particular user at that particular time and so on and so forth (such as, maybe the news says there are more individuals taking over accounts on the internet and possibly our security system reacts to that information)
(5) nothing else #2


. . 

expect is like a different type of function name . . it seems that . . expect is like something possibly unique from 'get', 'create', 'update', 'delete', 'recommend', 'scale' and 'performAction' which have been the 7 previous function names that we've identified . . 

. . 

expect . . ensure . . declare . . define . . statisfy . .

. . 

These -_- . . hmm . . seem quite primitive . . from my natural intuition it seems like they are really 'assumptions' about the world and so it's like . . kind of like 'get' except you don't 

. . 

getExpectationDefinition

. . 

ensureExpectationDefinition

. . 

defineExpectationDefinition

. . 

declareExpectationDefinition

. . 

createExpectationDefinition

. . 

createExpectationDefinition

. . 

Hmm . . 

. . 

create

. . 

define

. . 

create and define seem somewhat different . . 'define' maybe suggests some sort of arbitrary -_- . . rule that you have to follow . . like a sort of . . uh . . permission rule . . or even a type of function rule that you need to perform for when you do something . . 

. . 

defineExpectation

. . 

createExpectation

. . 

createExpectation could possibly be okay to use . . 

. . 

create-expectation-definition.ts

. . 

item.ts

. . 

and then we may need so other service items 

. . 

item.ts
/service/
  - create-expectation-definition-for-item.ts
  - expectation-definition-for-item.ts
  - 

. . 

I'm really not sure what to name things anymore . . 

. . 

'expectation-definition-for-item.ts' is possibly one example of a type of function . . that we would want as a service to associate with our item.ts . . 

. . 

but the name is . . it doesn't seem formal I suppose . . 

. . 

create-permission-rule-for-evaluating-item-function.ts

. . 

permission-rule-for-evaluating-item-function.ts

. . 

Okay . . this name seems way more formal . . I like it because it follows our (1) service items . . and also I like it since . . it . . looks like . . it can scale to other types of permission rules . . and so for example . . you can have several of these file types for different types of permission rules that you want to declare or define or clarify . . and so for example . . maybe you have . . (1) unit tests (ie. expectation definition) (2) integration tests (3) linting tests (4) author update tests (ie. only certain users can update the item) (5) formal methods (6) and any other types of tests that you could possibly consider for the future . . of this type of program where (1) you are interested in adding 'permission rules' for what can and cannot happen with that particular item of interest . . 

. . 

permission-rule-for-evaluating-item-function/
- /formal-methods-permission-rule-1
- /formal-methods-permission-rule-2
- /expectation-definition-permission-rule-1
- /expectation-definition-permission-rule-2
- /


. . 

expectation definition

unit test

. . 

these two names are used interchangeably by myself . . I had once thought that 'expectation definition' was possibly a nice replacement for the term 'unit test' . . I am really not sure . . 

. . 

'expectation definition'

. . 

expectation definition . . I'm not exactly sure where this type of naming scheme fits into the rest of the ideas that we've gathered so far . .

. . 

Anticipation Description . . Expectation Definition . . 

. . 

Intention
Anticipation
. . 

. . 

### (1) space order . . Intention
### (2) time order . . Anticipation
### (3) need to know order . . Acceptance
### (4) nothing else #1 . . Acclimation
### (5) nothing else #2 . . Acclimation

. . 

Anticipation . . Expectation . . Action . . Action At A Distance . . 

. . 

### (1) space order . . Anticipation
### (2) time order . . Expectation
### (3) need to know order . . Acclimation
### (4) nothing else #1 . . Acting
### (5) nothing else #2 . . Acting

. . 

. . 

Action at a distance . . Action at a distance

. . 

Expectation . . Expectation Definition . . 

. . 

Definition . . 

. . 

- item.ts
- /service
  - permission-rule-for-evaluating-item.ts
  - permission-rule-for-evaluating-item-2.ts
  - permission-rule-for-evaluating-item-3.ts

. . 

And then it's possibly not necessary to name the type of permission rule since for example . . you don't necessarily care if the permission rule is a 'formal method' or a 'unit test' . . right? . . maybe you care so maybe . . 

permission-rule-for-evaluating-item-using-formal-method.ts

permission-rule-for-evaluating-item-using-unit-test.ts

. . 

'using' is like a possible 'formal' counterpart of 'for' . . whereas with 'for' possibly we are going to use a noun or name that is pronounced like a formal specified item . . as per what is provided by your program . . to consider or iterate through . . and yet for 'using' it could possibly denote an 'informal item' or an 'informal' item in that there are possibly many variants that aren't known formally in the construction engine logic system or something like that . . and so . . it's possibly considerable as an 'any' type informally as a name typing mechanism or something like that . . I'm not sure . . I'm sorry . . 

. . 

permission-rule-for-xyz-defined-item-using-informal-undefined-item.ts

. . 

for . . using . . 

(1) space order . . is
(2) time order . . 
(3) need to know order
(4) nothing else #1 . . using
(5) nothing else #2

order by . . 

called . . 

for . . 

is . . 

while also . . (hold in place)


create-item-for-item-2
update-item-using-item-3
delete-item-using-item-3-while-also-updating-item-4

get-item-while-also-creating-item-3-while-also-updating-item-4

. . 

get-item-

. . 


### for is like a data structure reference

### while also is like a 'hold in place' anderson reference

### then is like a 'next item' reference

### using is like a provider reference // from, 

### providedBy is like a consumer reference

### withAll is possibly a 'catch-all' statement script

### from is possibly like a 'formal' reference to an item of interest


. . 

from, after, or, etc.

. . 


get-item-with-all-items-of-interest-held-in-place

get-item-using-database-4-while-also-removing-item-2-with-all-capital-case-letters-deleted

get-item-using-database-2-then-update-item-2-using-database-3-then-delete-item-4-using-database-5

create-item-then-create-item-2-then-create-item-3-while-also-create-item-that-deletes-previous-two-or-three-items-after-two-or-three-seconds


update-item-provided-by-user-interface-1-while-also-deleting-item-from-user-interface-2

. . 


permission-rule-for-evaluating-item.ts

. . 

permission-rule-for-evaluating-item-using-graph-metrics.ts

. . 

### permission-rule-for-getting-item.ts
### permission-rule-for-creating-item.ts
### permission-rule-for-updating-item.ts
### permission-rule-for-deleting-item.ts
### permission-rule-for-recommending-item.ts
### permission-rule-for-scaling-item.ts
### permission-rule-for-performing-actiong-for-item.ts

. . 

permission-rule-for-evaluating

permission-rule-for-measuring

permission-rule-for-achieving

. . 

createFunction

evaluateFunction

completeFunctionEvaluation

receiveErrorWhileFunctionEvaluation

. . 

permission-rule-for-catching-errors-while-function-evaluation.ts <br>
// seems equivalent to -> permission-rule-for-updating-item.ts

. . 

permission-rule-for-completing-function-evaluation.ts <br>
// seems equivalent to -> permission-rule-for-updating-item.ts

. . 

permission-rule-for-evaluating-function.ts <br>
// seems equivalent to -> permission-rule-for-updating-item.ts

. . 

permission-rule-for-creating-function.ts <br>
// seems equivalent to -> permission-rule-for-creating-item.ts

. . 

. . 

. . 

permission-rule-for-evaluating-item.ts

. . 




