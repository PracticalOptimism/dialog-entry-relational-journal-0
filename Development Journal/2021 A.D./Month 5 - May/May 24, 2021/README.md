

# Development Journal Entry - May 24, 2021


[Written @ 17:54]

The Listless Architecture.

`Add an item to the list`
- (1) `number.number.number...`

`Remove an item from the list`
- (1) `number.number.number...x-removed-type`

`Update the list`
- (1) `flat map to number` (no number.number.number... and no 'removed-type' syntax)

`Replace an existing item on the list`
- (1) `change the name from 'removed-type' to something else` (remove 'removed-type' and don't change the 'x' or change the number ? ? right ? . . )

`Add a large amount of things to the list`
- (1) `{index-of-starting}.r{random-number}.{index-of-item}`


. . 
. . 

Create an infnite loop pool program -_- . . an infinite loop pool . . -_- I don't like the term 'pool' but okay . . Pool is like 'set' and it can possibly be related to 'orders of magnitude' since -_- . . 

. . 

A large set of items can be added to the list . . which is possibly different from 'add item' and so it's like 'add infinite loop amount' or add 'super large amount' . . 

. . 

'add super large amount'

. . 

'add super large amount'

. . 

So if you start at infnity . . and walk backwards to 1 . . that is the idea . . you need to have to add items to the list from the reverse interesting direction . . and so for example . . you don't really necessarily (theoretically speaking) know where you are starting from . . but it's really practically a large number . . large enough that then you can traverse the list of items from the large number to the . . 1st item in the list . . like you're trying to fill the plot holes of a complicated story by first starting at the end point and walking backward to fill in all the items of the story . . in reverse order from Infinity to Infinity - 1 to Infinity - 2 and . . so on . . and so forth . . 

. . 

Until you have 3 . . 2 . . 1 . . 

. . 

3 . . 2 . . 1 . . 

. . 

A way of naming these items is possibly . . 

. . 

`{index-of-starting}.r{random-number}.{index-of-item}`

. . 

For example . . 

If you have 'Anderson' computers . . they could have 'simultaneous' 'negative loops' that play a role meaning they are computer looping structures that loop inside of an item . . or are started by being in some depth of an item and are very quick to walk out of that structure to give the impression that they are occuring simulaneously and not sequentially . . like an infinite loop that is caught in a single moment . . or a small delta period of simple time reliance . . or something like that . . 

Maybe there is an electron equivalent to these time measurements . . like Planck time or something i'm not sure what Planck is but it's like there are units of measurement or something like that . . which are possibly related to practical constraints or something like that . . [For more information see Dan Winter's work on Planck Length . . [Available on YouTube and also a variety of personal websites . . ]]

. . 

I'm not sure . . 

Something about . . 

. . 

Okay well if you have something to create a lot of files . . all at once then . . indexing them could be awkward . . and so we use a 'random number' to possibly 'negotiate' that this item was started at sometime . . and use the 'r' character to denote that it's a random number . . or something like that . . 

. . 


{index-of-starting}.r{random-number}.{index-of-item}

5.r17364738293.1
5.r17364738293.2
5.r17364738293.3
. . 
5.r17364738293.10000
. .
5.r17364738293.100000
. .
5.r17364738293.100000000

. . 

Something like this . . 

The index-of-item is for that particular 'negative loop' and so it can be a really large number . . 

. . 

I don't really know what these item lists could be used for but they could be useful

. . 

Computer sensor data could be useful . . 

. . 

Other types of information could be useful

. . 










[Written @ 13:34]



