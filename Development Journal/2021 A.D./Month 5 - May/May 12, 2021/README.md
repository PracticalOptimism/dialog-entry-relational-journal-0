


# Development Journal Entry - May 12, 2021

[Written @ 20:01 - 21:16]

'x.x.x' could possibly be an indicator value for 'specifying' that the 'version for that type is available'

. . although . . that may not be useful since . . uh . . the latest method of using 'arrays' instead of 'string-based' listings of 'type-with-version-id' such as 'type-id-x.x.x' is possibly uh . . suggesting that . . using another approach such as

'type-id': [ // `0` // `not index` since the item `is an array`. version id indicator.
  [ // `0.0` // `not index` since the item `is an array`. version id indicator.
    [ // `0.0.0` // `not index` since the item `is an array` version id indicator.
      0 // `0.0.0.0` // `index` value since the item is `not an array`
    ]
  ]
],

'type-id-2': [
  [
    [
      1, // 0.0.0.0 // index value
      2 // 0.0.0.1 // index value
    ]
  ]
]


1 // one index value
[] // no index value
[[]] // no index value
[[], [], []] // no index value
[[], [], [[], []], [[], [], []]] // no index value

```js
//           0    1   2           3                4
//            0        0    1      0    1    2     -
//            -         0    0      0    0    0    
//                      -    -      -    -    -
let array = [[0], [], [[2], [1]], [[1], [3], [2]], 1] // 7 index values
```

index value #1: { versionId: `0.0`, indexValue: `0` }
index value #2: { versionId: `2.0.0`, indexValue: `2` }
index value #3: { versionId: `1.0`, indexValue: `1` }
index value #4: { versionId: `3.0.0`, indexValue: `1` }
index value #5: { versionId: `1.0`, indexValue: `3` }
index value #6: { versionId: `2.0`, indexValue: `2` }
index value #7: { versionId: `4`, indexValue: `1` }

An index value listing like this can go deeper and deeper into more and more arrays of `values` or `arrays of values` or `values of arrays` however you would like to say that . . 

. . 

For an MMORPG video game that tracks a player's movements, those items may be interesting to version as 'error correction' releases or 'patch releases' of a player's behavior . . uh . . which is uh . . possibly the '3rd' version id indicator value in the 'semantic version control software' version 2.0.0 that I am familiar with at this time . . and so . . the final 0 in the version id '2.0.0' is known as the 'patch release' and I would personally possibly call it the 'error correction release' since I like the term 'error correction at this time of writing' but possibly the term may not stay since I'm not sure and haven't included this term in the 'finite list algebra for 'the-light-architecture'' . . 

. . 

Semantic Versioning 2.0.0
Summary
Given a version number MAJOR.MINOR.PATCH, increment the:

MAJOR version when you make incompatible API changes,
MINOR version when you add functionality in a backwards compatible manner, and
PATCH version when you make backwards compatible bug fixes.
Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

https://semver.org/

. . 




[Written @ 5:18]

```js

const pamper = new Pamper({
  databaseStorageLocationList: [{
    //
  }],
  administratorStorageLocationList: [{
    //
  }],
  reverseIndexAlgorithmVersionIdStorageLocationList: [{
    //
  }]
})

// Use semantic versioning semantics by default if you would like:
// 'reverse-index-algorithm-version-id': [0, 0, 0]
// https://semver.org/
// item-type://major-version.minor-version.patch-version
// item-type://0.0.0 , item-type://1.0.0
// item-type://2.0.0 , etc.


pamper.getReverseIndex('item-type-id', 'reverse-index-algorithm-version-id'?)

pamper.addReverseIndex('item-type-id', 'indexNumber', 'reverse-index-algorithm-version-id'?)

pamper.deleteReverseIndex('item-type-id', itemReverseIndexAlgorithmVersionIdList|matchPattern|function, completeDeleteWithoutDelay)

pamper.updateReverseIndex('delete-reverse-index-periodically', { 
  itemTypeId: 'item-type-id',
  itemReverseIndexAlgorithmVersionIdMatchingPattern: itemReverseIndexAlgorithmVersionIdList|matchPattern|function
})

pamper.getLatestReverseIndexAlgorithmVersionId('item-type-id', 10)
pamper.getEarliestReverseIndexAlgorithmVersionId('item-type-id', 10)

pamper.getReverseIndexAlgorithmVersionIdSequence('item-type-id', 'earliest-version-id', 'latest-version-id', excludeVersionIdList, excludeMatchPattern, excludeFunctionPattern)

pamper.createReverseIndexAlgorithmVersionId(10, 0, 0) // use semantic versioning by default (behaves like a web address for locating different types ie. https://0.0.0 is a web address. And related project web addresses can change their 'minor' version) . . 'item-type://0.0.0' can be the reference for an item of interest.

// The same items can be accessed in different 'databases'
// The same items can be administered by different 'administers'
// The same items can be referenced by version id by different 'reverse-index-algorithm-version-id-storage-locations'

pamper.addReverseIndex('database-storage-location')
pamper.addReverseIndex('administrator-storage-location')
pamper.addReverseIndex('reverse-index-algorithm-version-id-storage-location')
// pamper.addReverseIndex('')

// 




```




