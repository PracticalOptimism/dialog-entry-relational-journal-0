

# Development Journal Entry - May 2, 2021

[Written @ 23:21]

Item Changer

Item Reverse Changer Indicator

Item Pinch Factor Co-Relator

. . 

```javascript

class ItemChanger<ItemChangerResponse> {
  itemChangerId: string = ''

  itemChangerType: string = '' // concatenate next 3 properties with a separator like '|:|:~|' to avoid possible conflicts with propertyIdList

  itemChangerFunctionName: string = 'all' // example: 'update'
  itemChangerDataStructureName: string = 'all' // example: 'file'
  itemChangerPropertyIdList: string[] = ['all'] // example: ['line-100']

  isBeingRestricted: boolean = false
  restrictionLimitType: string = ''
  restrictionLimitDescription: string = ''

  // example output #1: 'that particular file with 'itemId' does not allow that operation to occur at this time'
  // example output #2: 'operation successful'

  changerFunction: (itemId: string, itemData?: any) => Promise<ItemChangerResponse> = (_itemId: string, _itemData?: any) => Promise.resolve(new DataStructure())
}
```

An `item changer` is a function that is called by another program . . That function has a general application programmable interface like an 'event listener' . . The 'item changer' application programmable interface . . is specific for (1) `calling functions` that aren't necessarily available to that computer program without necessarily hacking or trying to traverse directory trees and force one's way into permission-restricted areas or something like this . . (2) and `performing actions that would possibly otherwise be restricted` by (a) the computer user or (b) a default computer program that is tasked with managing the computer software of that particular item of interest . . or something like that . . In general . . Artificial Intelligence Programs would possibly use '`item changer`' programs that (1) provide permissioned function operations and (2) warn the function caller of mistakes that are made such as the 'update' type being an 'illegal' operation because of 'too many filtered comments' or 'too many foul language word usecases' or something like that . . 

. . 

item changer co-locator map

. . 

An `item changer co-locator map` is a directory tree mapping structure that relates to (1) what event would you like to respond to? (2) what permissions do you need?

. . 

```javascript
class ItemChangerCoLocatorMap {
  itemChangerCoLocatorMapId: string = ''

  itemChangerCoLocatorMap: {
    [eventType: string]: {
      
      [itemChangerType: string]: (itemChanger: ItemChanger, error?: Error) => any
    }
  } = {}
}

```

```javascript

// Manually remove the co-locator map item changer reference

// eventType = 'none' // no event in particular
// functionName = 'delete'
// dataStructureName = 'CoLocatorMapResponseFunctionListing'
// propertyIdList = ['myCoLocatorMapId', 'eventType', 'itemChangerType']

/*

Automatically delete the itemChanger when

(1) the event type occurs and
(2) the program isn't responding
(3) the user requests to remove the item changer
(4) the user restricts item changers that require those permissions
(5) the computer program restricts item changers that require those permissions
(6) stop programs that are preventing other programs from having a nice memory profile for the overall computer
(7) 

Warn the user that there are a lot of item changers

(1) remove a program that uses item changers
(2) keep a list of programs that use item changers the most
(3) 

*/
```


[Written @ 9:15]


Install '`the-light-architecture`' abbreviated 'tla' from 'OICP'

. . 

'`the-light-architecture`' website allows to

(1) use a text editor
(2)

. . 

'non' is a text editor

. . 

'`the-light-architecture`' builds your project (create) <br>
'`oicp`' provides resources for your project (get) <br>
'`non`' provides a resource environment in the web browser (update) <br>
'`pamper`' provides a resource for answering questions about performance and bottlenecks and responding to those automatically for any standard computer program that would use a resource like that . . <br>

. . 

(1) README.md
(2) Consumer Resource: Website Resource, JavaScript Library, Command Line Resource
(3) Update Cycles (publish on the internet)
(4) Get Cycles (download from the internet)
(5) Program Maintainability Statistics (health status)
(6) Action Buttons (create updates, warnings and 'thanks' alerts)
(7) Sleep

. . 

Consuming Resource needs to know 'where do you belong?' 'how do I use you without asking the customer?'

. . 

'file type A' 'file type B' are places . . 

. . 

'linear scan the file type' and 'map to a range of references' where one of the references is the item presented at 'provider' resource . . Or 'provider'

. . 

'provider' then says 'add this to the file' 'remove this from the file' 'update this in the file' 'get this from the file' 'recommend this in the file' 'scale this in the file' 'perform this action in the file' and so on . .

. . 

'event listener' type could useful to know when to apply an idea . . for the 'provider' of interest . . 

. . 

reverseCopyEditing . . recommendedConsumptionUsecase ? . . 

. . 

recommendedConsumerUsecase is possibly a type of consumer that will automatically perform the abilities that are being requested of this item . . using a standard . . application programmable interface . . with expected items of interest being available like 'is the file changing?' 'where did the file change?' 'what line of text?' 'what character sequence is being updated?' 'what line of code?' 'what column?' 'what are the general actions being performed?' 'do I need to cache these results?' 'how long should I cache these results?' 'is there a time delay limit for when to update this item?'

. . 

. . 

recommendedConsumerUsecase . . 

. . 

recommendedComputerProgramToStart . . 

. . 

'event-type' event listener . . map to . . computer program manager algorithm . . or something like that . . 

. . 

/anana-anana-anana
- /event-listener-to-computer-program-manager-map
  - /recommended-event-response-algorithms
    - /@service
      - item.ts
    - /update-file-item
    - /update-folder-item
    - /update-network-item


// /@service/item.ts

const service = {
  'event-type': {
    'response-type-1': {
      name: 'response-type-1',
      expectedInputList: {
        event: EventType,
        itemChangerTypeList: ['update-file', 'get-file', 'recommend-file', 'scale-file']
      },
      responseFunction: function (...expectedInputList) {
        updateFileItem(...expectedInputList)
      }
    }
  }

  'event-type': function (event, ) { updateFileItem() },
  'event-type-2': function (event) { updateFolderItem() },
  'event-type-3': function (event) { updateNetworkItem() }
}



. . 

itemChanger

. . 

eventListener

. . 

itemChanger is specifically ready to receive your request to update an item . . or to perform an action on an item . . 

. . 

itemChangerList is a list of itemChangers that provide you with permission to perform an action or a particular type with particular types of parameters available to you . . or something like that . . 

. . 

database <br>
network protocol <br>
event listener <br>
permission rule <br>
item changer <br>
anana anana anana <br>
scale matrix amana <br>
computer program manager <br>

. . 

item changer . . has the permission to perform an action . . an item changer gives you permission to perform an action . . if you have that item changer . . and so it's like a private key but it is generalized to a particular algorithm . . and not only a private key or secret key or something like that . .

. . 






