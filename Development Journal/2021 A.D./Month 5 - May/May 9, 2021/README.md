

# Development Journal Entry - May 9, 2021

[Written @ 18:43 - 18:49]

(1) Keep an index of where the program is going <br>
(2) Have a reverse index of where a program would go if you knew its id <br>
(3) Keep an index of total number of places that a program can go <br>
(4) Update the reverse index function for each version of the place total counter <br>


(1)
```js
// (1) Keep an index of where the program is going

let indexList = ['https://database.com/database-store-1', 'https://database.com/database-store-2', 'https://database.com/database-store-3'] // array indexing is useful; [0, 1, 2] is the array index list for this array; index the 'places of interest'
```

(2)
```js
// (2) Have a reverse index of where a program would go if you knew its id

let reverseIndexMap = { 'item-id-type-1': 0, 'item-id-type-2': 1, 'item-id-type-3': 2, 'item-id-type-4': 0, 'item-id-type-5': 2, 'item-id-type-6': 2, 'item-id-type-7': 2, 'item-id-type-8': 1 } // a map for representing 'item-type-to-item-place-of-interest'

let totalNumberOfPlaces = indexList.length


function getItemType (itemId: string): string {
  return `item-id-type-${getNumberFromString(itemId) % totalNumberOfPlaces}`
}

function getNumberFromString (stringValue: string): number {
  return stringValue.split('').map((character) => character.charCodeAt(0)).reduce((number1, number2) => number1 + number2) 
}
```

(3)
```js
// (3) Keep an index of total number of places that a program can go

// total number of items of interest [1.0]
let totalNumberOfItems = indexList.length
```

(4)
```js
// (4) Update the reverse index function for each version of the place total counter

// integrate the 'reverseIndexMapFunctionVersionIndex'
function getItemType (itemId: string, reverseIndexMapFunctionVersionIndex: number): string {
  return `item-id-type-${getNumberFromString(itemId + reverseIndexMapFunctionVersionIndex) % totalNumberOfPlaces}`
}

```

[1.0] items of interest . . places of interest . . places are interesting places for 'items' to be and so 'items' can be in a 'place' if you are a nice 'visual' person like a 'human being' 'a place' can say 'I am an item' and so if you have strange and interesting usecases where 'place' and 'item' cannot be 'distinguished' maybe you can have 'interesting usecases' where that is applied to use 'item of interest' as a generalized form of 'place of interest' and so 'item of interest' is used in this example although in other parts of the codebase 'place of interest' is used . . :O


[Written @ 18:38]

`(1)` Keep an index of where the program is going <br>
`(2)` Have a reverse index of where a program would go if you knew its id <br>
`(3)` Keep an index of total number of places that a program can go <br>
`(4)` Update the reverse index function for each version of the place total counter <br>


`(1)`
```js
let indexList = ['https://database.com/database-store-1', 'https://database.com/database-store-2', 'https://database.com/database-store-3'] // array indexing is useful; [0, 1, 2] is the array index list for this array; index the 'places of interest'
```

`(2)`
```js
let reverseIndexMap = { 'item-id-type-1': 0, 'item-id-type-2': 1, 'item-id-type-3': 2, 'item-id-type-4': 0, 'item-id-type-5': 2, 'item-id-type-6': 2, 'item-id-type-7': 2, 'item-id-type-8': 1 } // a map for representing 'item-type-to-item-place-of-interest'

let totalNumberOfPlaces = indexList.length


function getItemType (itemId: string): string {
  return `item-id-type-${getNumberFromString(itemId) % totalNumberOfPlaces}`
}

function getNumberFromString (stringValue: string): number {
  return stringValue.split('').map((character) => character.charCodeAt(0)).reduce((number1, number2) => number1 + number2) 
}
```

`(3)` 
```js
// total number of items of interest / places of interest in particular usecases . . 
let totalNumberOfPlaces = indexList.length
```

`(4)`
```js
// integrate the 'reverseIndexMapFunctionVersionIndex'
function getItemType (itemId: string, reverseIndexMapFunctionVersionIndex: number): string {
  return `item-id-type-${getNumberFromString(itemId + reverseIndexMapFunctionVersionIndex) % totalNumberOfPlaces}`
}

```

[Written @ 18:36 - 18:37]

Okay . . I'm not sure what to do next . . I suppose I would possibly be able to end the live stream . . 

I am also getting a little bit hungry . . and so something like some food to cook and eat would possibly be okay . . 

Have a good rest of your day

[Written @ 18:27]


[Written @ 17:45]

This is an interesting list

(1) :)
(2) now you can say O _ O 'i have things that I know and what I know needs to flat map onto what is being talked about'
(3) 10 . . I flat mapped 10 things that I can do with what you typed sir. 
(4) interesting . . re-map

. . 

Okay that is a list that I wrote I'm not exactly sure if it maps onto the structure of an interesting idea . . like 'look that program is like 'an artificial intelligence' program' . . for hmm 'interesting items of interest right?' -_- well maybe it's useful -_- maybe it's not . . but I'm not sure . . I'm sorry . . 

. . 

Okay in summary

(1) You have a list of something like 'words' and you don't know what to do with those
(2) You have types of things that you like like 'eating bananas' and 'walking outside' and so you say that 'what you read is really something that is related to the things that you like and so if you see the word 'banana' and the word phrase 'walking outside' then your adrenaline levels will go up to learn that 'woah', 'this information said that I can get massive gains if I keep a map to where I found this information in my mind so then I can be like 'yea, I learned that walking with my hands is an interesting topic especially at a park with friends'' . . and 'so' . . -_- [Rosie Burr](https://www.youtube.com/c/RosieBurr/search?query=hand%20stand) can get all the credit for that idea . . 
(3) You may not walk on your hands at any park but if you have a list of 'ideas' like 'yea, that makes sense to apply walk-on-hands-technique here' then you will be like 'my maximum index list for ideas for 'walking on hands' is like 'well '`superstitious`' or '`absurd`' or '`ingneious`' or '`just right for me`' or '`no way I should have always been doing that`' or '`yes, I have already done that`' or '`yes, I am on the way to knowing that I am a regular hand-walker`' or '`yes, I am already a good hand walker`' or '`no way, of course handwalking is the religion of my town`' or '`no way, of course handwalking is the religion of my country`' or '`no way, of course handwalking is the religion of my species`' or '`no way, I don't know a universe or galaxy or extraterrestrial space station with humans that don't handwalk`' or '`O _ O what do you mean?`''
(4) Interesting re mapping . . wow . . what an idea . . I wonder if I can use that for anything . . wow . . -_- you can say for example . . 'you want to ensure that the way you know where things are is useful for preserving where you already know where things are like 'family emotions' . . if you are a person that really loves your family then you may want to keep those feelings in tact even if you get new weird family members that really annoy you and freak you out and make you wish really weird things would happen to them so they don't destroy your idea of family [crying emoji]' . . but that is just an example and an example is an example of an example and so you need to exemplify this example by examining your samples of examples and sampling new examples to samplify an example tree that doesn't keep this examples as an ordinary example but just as an exemplary example that is examplifying a possible example of a possible example of a possible example of a possible example of a possible example of a possible example of a possible example of a possible example of a possible example of a possible example . . and each example is exemplary of examples of examples of examples of examples of examples and so each example is possibly an example of an example of an example of an example . . and if you forget that you met that person by coincidence because you happen to examplify yourself on a possible example planet or a possible example version of a planet or an example of a place in a place of a place that you think you're in which is exemplary of you could have watched this video in a different place but for some reason you think you're watching the video is a place that is a possible place that was an example of a place that you could have possibly watched the video in an example place of an example place and you met example friends along the way who showed you example ideas of what real good friends could be like but those were examples like `Spongebob` and other cartoons can also exemplify true frienships from deep in the deep dark sea of example places deep in your heart



[Written @ 17:25]

Just Kidding Everyone [throws confetti in the air] [Shaiapouf emoji smile :)]

[Written @ 16:01]

(1) Keep an index of where the program is going <br>
(2) Have a reverse index of where a program would go if you knew its id <br>
(3) Keep an index of total number of places that a program can go <br>
(4) Update the reverse index function for each version of the place total counter <br>

. . 

Okay if this is a good list of the things for our program . . it's not a bad list . . uh . . in terms of -_- I don't know . . -_- it seems to have -_- something about its abilities that can be measured and completed in terms of how to achieve it . . 

But really you must also ask yourself '`-_- I'm reading these random words on this notebook`' -_- 'what the heck is a translation of this random word list to a computer program?' . . 

. . 

-_- it's so awkward -_-

. . 

What do you mean I have to push buttons to press the computer program into existence ? . . isn't that a weird concept ? . . -_- -_- -_-

. . 

(1)
```js
let indexList = ['https://database.com/database-store-1', 'https://database.com/database-store-2', 'https://database.com/database-store-3'] // array indexing is useful; [0, 1, 2] is the array index list for this array
```

(2)
```js
let reverseIndexMap = { 'item-id-type-1': 0, 'item-id-type-2': 1, 'item-id-type-3': 2, 'item-id-type-4': 0, 'item-id-type-5': 2, 'item-id-type-6': 2, 'item-id-type-7': 2, 'item-id-type-8': 1 } // array indexing starts from 0. Reverse index each item in the 'indexList'. 'reverse' refers to the 'index number value' which we do not initially care about when we are 'adding' items to our 'index list' and so we have to 'reverse' our 'mindset' to learn that 'keep that in mind' so that 'we can access the value from that index' . . 'reversing' can be a general notation for 'I didn't initially care about that and so now it's like O _ O . . well it's useful if you want to access certain variable information cases' or something like that . . 

let totalNumberOfPlaces = indexList.length

// a simple 'get item type' function which is not necessarily recommended but is exemplary of the idea . . of how the item id is mapped to a item type . . which is then further mapped to an item location . . -_- one example for why this is exemplary -_- and it is only an example . . -_- . . is because (1) . . consider you have more than 3 types . . and so you need all of those types to map to at least 1 location . . so that means you will have duplicate location maps for any particular location . . and so for example . . type 1 and type 2 can map to the same location and type 1 and type 2 may not be equal to one another . . FOR EXAMPLE . . do you see how since we only have 3 places we cannot actually have a map to 3 and 4 . . since our index list only contains up to 2 ? . . we have to 'wrap' around or . . 'subdivide our existing list of indices into pairwise adjacent sets' right? is that a good terminology for this ? . . notation ? . . -_-
function getItemType (itemId: string): string {
  return `item-id-type-${getNumberFromString(itemId) % totalNumberOfPlaces}`
}

function getNumberFromString (stringValue: string): number {
  return stringValue.split('').map((character) => character.charCodeAt(0)).reduce((number1, number2) => number1 + number2) 
}
```

(3) 
```js
// having the 'maximum' number of something is really useful in a lot of computer programs and so 'the length of a file' is a useful characteristic that allows many network requests to be 'finite' and 'calculatable' for modern day computers (circa 2021) . . and so 'maximum' is a nice benchmark for measuring 'accolades' or 'capabilities' of a civilization and so 'maximum' O _ O . . 
// 'total' might be an 'okay' replacement for 'maximum' since you may not 'know' if 'some of them left or not' during 'runtime' and so it might be more or less than what you thought since you may not have the processing power to continually re-sample the 'maximum' pool and so a 'total' count could be like a 'I think this is the approximate amount since I last checked' . . something like that . . 
let totalNumberOfPlaces = indexList.length
```

(4)
```js
// I don't really know how to think about this one right now . . My idea is that (1) you need to involve the 'version index' or 'version id' of the 'reverseIndexMapFunction' or 'reverseIndexMapAlgorithm' and so you can 'back track' or 'reverse appropriate' an 'idea' of 'where you think you are' for each 'type-to-index' location mapping id that you have created . . and so . . for example . . this is especially useful when your 'index list' grows in size or even if it shrinks in size . . you can catch up to yourself and be able to 'reverse back track' your way to and from the 'new' 'possible' location of where your data is . . and of course . . this goes along with an important thing which is (2) . . you need to have a 'neighborhood' calculation method of 'go-forward' and 'go-backward' and 'know' where your 'neighbors' are in case you don't 'precisely land where you thought your data would have been . . '
function getItemType (itemId: string, reverseIndexMapFunctionVersionIndex: number): string {
  return `item-id-type-${getNumberFromString(itemId + reverseIndexMapFunctionVersionIndex) % totalNumberOfPlaces}`
}

```


[Written @ 15:10]

I uh . . don't have a lot of thoughts prepared for this live stream

A lot of review was done in yesterday's live stream . . Ecoin #551 [https://www.youtube.com/watch?v=cOlMPSL9cKY&feature=youtu.be]

. . 

. . . 

. . . 

. . 

Flat map . . 

. . 

[1, 2, 3]

. . 

A list of computers is available on a computer network

. . 

You want to provide each of them with a responsibility like 'store this data' or 'run this program' . . 

. . 

The list of computers is 'growing' and 'shrinking' . . 

. . 

[1, 2, 3] was the initial set of the computer network

[1, 2, 3, 4] was a second possible set of the computer network for when another computer was 'in this case' 'added' to the 'network'

[1, 2] is a second possible set of the computer network for when a computer is 'removed' from the 'computer network'

. . 

Documents that were once available on computer '3' are lost if the computer network loses computer '3' . . as is indicated in [1, 2] as a set of computer networks after [1, 2, 3] is updated in a later time step . . 

. . 

. . . 

. . 

(1) Keep an index of where the program is going <br>
(2) Have a reverse index of where a program would go if you knew its id <br>
(3) Keep an index of total number of places that a program can go <br>
(4) Update the reverse index function for each version of the place total counter <br>

. . 

If you grow your number of places 'name listing' from 'left to right' and not 'going back and filling in the places in between' or 'going back and inserting a place between two already existing places' then possibly this method of searching for 'where an item was placed in a distributed network' could possibly be useful for things.

. . 

. . . 

. . . 




[Written @ 14:58]

[Jon Ide]: I was planning on going on the live stream to talk about . . software development . . and maybe "Seth" can help introduce the next topic of interest since I myself am motivated to ask for the help as it relates to "pamper" which is a software project meant to help ease safety relations of certain computer programs or something like that . . I guess -_- I'm not really sure how to go about this . . 

Seth . . do you have anything to say?

[Seth]: Yes, I do but not now since now is not a good hour.

[Jon Ide]: Okay.

. . 

Okay . . 





