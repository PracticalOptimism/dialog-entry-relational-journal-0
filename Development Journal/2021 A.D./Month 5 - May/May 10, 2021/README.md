

# Development Journal Entry - May 10, 2021

[Written @ 21:27]

Cool cartoon animations are so cool in my opinion . . The one cartoon animation in 'Gantz' surprised me when I first saw it . . it was really interesting how it aligned with sound effects and also the question arose in my mind 'how the heck did they think to do that?' 'what a weird animation' 'rotating green squares' 'rotating green squares in a cascade?' 'how does an animator decide that is a useful one' 'maybe there were candidates but it's like O _ O that reverses my interest in random mathematics topics' 'what if that random math can be applied to an animation to make it punk rock so cool?' O _ O right? -_- you won't know unless you have that random math topic in your mind or an alien tells you about it right ? because aliens can flip switches in your mind to make you answer your own question before it even arises in your mind right ? -_-

. . 

A cool book of nice random math shapes . . 

. . 

If you can make a book like that you're really cool . . because it's like O _ O all the math can be 'reverse indexed' or 'reverse flipped' over its head like a wrestler reverses things of interest and makes them flat mapped onto other topics of interest like 'woah, that video game has 'WHAT inside of it?'' O _ O . . 'you reverse flipped WHAT?!' inside of that video game ? . . O _ O imagine you are watching a movie and you see people talking . . but then you want to say those people (1) are in a video game that you are making and need to perform like they are performing in the movie and you don't want to have yourself draw the characters or animate them . . O _ O well you can if you want to but I'm just saying (2) if you have a 'reverse' flip math algorithm . . O _ O then you can copy cat all the moves and not only apply them to your game like in the movie but you can do 'reverse flips' that are like (3) okay play their conversation backwards so that their lips are out of sync with theit words and (4) play their hands so they aren't crossed across their chest but up in the air clapping throughout the conversation . . (5) and you can keep 'reverse flipping' for various random things of interest like 'take the freckles on the face of the person on the left and make the environment map of where they are reflect that freckle pattern of how the buildings are layed out . . (6) give the buildings the color pattern that is (6.1) realistic (6.2) reflects the hair color of the person on the left . . and so 'blondish' colored buildings that look realistic is possibly how they could look which is just another example of 'reverse flipping' where you take indices that you know about like 'hair color' and you flip them for other usecases like 'slam them onto the building color' . . or 'make them be the color of everyone's shirt who walks by' or 'do something weird and wacky with the color that would possibly be surprising if you didn't already know about it' . . 

'Reverse flipping' . . it's so cool . . you can make fun and interesting video games . . 

Keep flipping an idea around in your head with 'horsoginal' (octagonal + horizontal) variations of a drawing until you get it just right . . it can be like how a painter on a canvas can 'reverse' flip the image repeatedly in their mind as they scribble and scrabble repeatedly to get the matching paints to match that one idea they had or the correct angle of that idea thing that's in a reference image . . and they keep reverse flipping repeatedly to match their idea of that item . . 'reverse flip' 'reverse flip' 'reverse flip' . . 

. . 

Reverse flipping could possibly be a gearbox item for 'strengthening the character of an item of interest' and so for example if you really try to paint a lot but the idea isn't coming across you may consider (1) starting from a place far away from where you've been starting . . this can involve (1.1) taking a tutorial in how to draw for beginners or (1.2) even take new and refreshing tutorials on how to draw in general even if you're way past beginner stages . . you can gain fresh new perspectives from artists that are already only starting their career just because they've never seen things and so they'll teach you based on what they know . . it's a reverse flip operation that's really like O _ O wow I really am a newb, how come I can't reflect that idea like that artist who is only just starting out ? . . -_- but it's of course subjective since you're only training to try to "see" where it is you need to start to get back to that one idea . . (2) starting from one place is nice . . if you're drawing a city landscape it may be cool to learn what that is . . 'a planet data structure of interest' could be a topic . . and so starting 'far away could mean 'forgetting' that you're on a planet' or 'forgetting that you're drawing something that could be considered to be on a planet' just like if you're drawing a person in a photograph you may not think to first draw their 'parents' and their 'grandparents' . . instead you'll assume that 'genetic information is possibly irrelevant' or 'ancestral information' is irrelevant or 'neighborhood relational information is irrelevant' . . to some degree you may need to come back to each of those interesting 'aspect reality variants to try to capture the idea precisely as you are looking to' but possibly you don't need to 'gearbox lock yourself so tightly in that aspect when you're just starting out' . . and so well eventually you'll tighten into closer and closer neighborhoods of thoughts and ideas and possibly you'll be happy with how far you get without needing to tighten any closer to any particular idea or sound effect or visual hallucination pattern that you're trying to create or whatever else it may be -_- I'm sorry to flat map my hallucinatory idea of what flat map ideas are possibly flat mappable to O _ O . . 

. . 




[Written @ 20:33]

Alright . . (1) I'm not sure where to go with the research and development now . . (2) Pamper looks interesting since yesterday's live stream Ecoin #552 which we made interesting progress with ideas of (2.1) index list (2.2) reverse index list (2.3) total item count (2.4) tracking the reverse index list algorithm version id . . (3) I am lazy now and not sure . . (4) lazy (5) 

. . 

Some of the things that I'm thinking about (1) video games

-_- well it's like a weird topic anyway since 'reverse index list' is the theoretical topic that 'you're not really necessarily going to compartmentalize all of your program to fit nicely into everyone's favorite genre of organizing the codebase' and so it's like . . ehh . . what does research mean if I don't reverse index my way into all those nooks and crannies of underwear from all those game development teams that look like they develop things in a particular way . . look at all those possible organization structures that they use that I have no idea about . . don't you imagine it's like a really big list of possible ideas ? . . and with just 'yea, that had to be there file types'? . . yea like 'package.json' possibly 'has to be' in the base area of the file directory structure or in the 'top level' of the file directory tree listing . . 

'README.md' 'package.json' . . these things are 'reverse indexable' to be said 'yea, that has to be there' . . and of course if you're me and you haven't read the documentation . . then you'll possibly skip over the fact that possibly that's not always true for all the projects or for all the possible configurations of the projects with this or that other tool or etc. etc. 

. . 

(1) video games

. . 

Fun pointed structures . . 

. . 

And making a game example with 'the-light-architecture' should possibly help us determine if we're on the right track . . certainly 'reverse operating' our way into the ways of the warrior of each of these idea categories of 'wow, what about that usecase' or 'wow, what about that usecase' and so those are useful ideas . . 

. . 

reverse indexing is . . in my mind a fun operation to think about . . as I playfully consider using the topic . . I imagine in my mind . . a wrestler . . like a high school wrestler person on the mat with another wrestler person . . and so you can 'reverse' an 'idea' by saying 'woah, look at that wrestler' 'twist' and 'turn' their way around the idea and try to press ito onto the floor or place it flat on the map . . right ? . . is that a good flat map of this idea of 'reversing?' . . 




[Written @ 20:17]

Loadings . . Loadings . . (A list of things to get)

Settings . . Settings . . (A list of things to update)

Developments . . Developments . . (A list of things to create)

Deletings . . Deletings . . (A list of things to delete)

. . 

-_- alright these next ones look really weird and complicated . . I have no idea how to translate them into a nice list . . 

. . 

You know what's weird . . -_- even though the terms aren't necessarily popular . . maybe we can use them anyway . . 'ya know what I mean?' . . because they are so cool you can be like 'yea, they exist now' . . -_- what a lame way to exist to not be able to flat map your nice new words onto the words of existing words for your nice homely custoers to buy and purchase into their lexicon . . but it's like -_- I don't know . . it's kind of interesting . . because of generalization concerns but also -_- well we have practical user customer interests like 'sticking with game development traditions' or 'whatever seems familiar to me at this time --__---' -_-

. . 

Gettings . . Gettings (A list of things to get)

Settings . . Settings (A list of things to update)

Creatings . . Creatings (A list of things to create)

Deletings . . Deletings (A list of things to delete)

. . 

O _ O 

`Gettings` . . this can have -_- O _ O things that are -_- O _ O things that can be retrieved right ? . . like 'extensions' or even lists of things that are in the process of being retrieved right? like information requests right ? . . and even a user can get used to the name and be like 'gettings' yea -_- . . 'loading' 'load' . . 'loading' is popular in video game terminology and so I'm leaning towards using the term as well -_- but it's -_- -_- characterstic . . and -_- well hypergains in theory are lacking if you use -_- lame loser ideas like 'loading' but it is also useful for things like 'loads' when you talk about 'bearing' systems and so 'load' or 'load' is like O _ O 'weight' 'electric pressure' 'aeroneumatic pressure' 'social pressure' 'ecological pressure' -_- . . -_- . . loads . . 'homework pressure' 'due date pressure' 'successful homework grade score pressure' 'receive an A+ or get kicked off the planet pressure' 'relax eat potato chips pressure' 'relax spend dream time all day in your sleep without anyone waking you up to give you assignments pressure' 'dream-art-science student pressure' 'lazy suicidal person pressure' 'planet on the brink of collapsing pressure' 'everyone is okay pressure' 'tia exists'

. . 

I have to go this is getting boring





// Recommend

[Get-Response-For-Recommended] -> . . -> . . -> . . 

[Update-Settings-For-Recommended] -> . . -> . . -> . . 

[Create-Request-For-Recommended] -> . . -> . . -> . . 

[Delete-Default-Settings-For-Recommended] -> . . -> . . -> . . 

// Scale

[Get-Scale-Co-Factor] -> . . -> . . -> . . 

[Amortize-Scale] -> . . -> . . -> . . 

[Elementalize-Scale] -> . . -> . . -> . . 

[De-Elementalize-Scale] -> . . -> . . -> . . 


[Written @ 19:55]

```js
class TallyInsignificantOrderIndicator {
  unordered = null
  inProcessOfBeingOrdered = null
  ordered = null
  blockedBy = null
}
```

Which was inspired by . . 

```js
class TallyInsignificantItemEndpointIndicator {
  unmeasured = null
  measured = null
  complete = null
  annoyedBy = null
}
```

. . 

Loadings . . Loadings . . (A list of things to get)

Settings . . Settings . . (A list of things to update)

Developments . . Developments . . (A list of things to create)

Deletings . . Deletings . . (A list of things to delete)

. . 

Buildings . . Developments . . 'buildings' has been referred to like 'compiling' in computer programs that I am familiar with in the past -_- well 'Developments' is possibly like 'in the process of being created' but it is more like 'building precompiled' in one of the usecases that I'm having in mind . . 

'`Developments`' could possibly be used or could possibly 'have the usecase' of 'keep a list of the things that this particular service account has created' . . and so that could be things like 'files' and 'folders' and things like that and so if you're in a computer network where you don't necessarily know who's file is whose or which file belongs to whom . . like in an anonymous file publication service maybe like IPFS . . I'm not sure . . then . . you may say that it is still useful to have 'service account' addressing which means 'keep track of the id of the service account that created that file' and make sure that '`service account`' can have a list of all the files that they've particularly created and not just a list of general files that a lot of other users are publishing or that a lot of other users have created . .

. . 

'`Building`' . . well in the way that software programmers have used the term in my limited understanding is that 'compiling' is the flat mapped relation to be made . . and so for example 'building . . . ' means 'work in progress as this program compiles this file or this set of files into this other program' . . and so well . . that could possibly even further be 'flat mapped' or 'flat mapped' onto the idea of 'loading' . . which is like . . '`get that compiled file result`' . . 

. . 

`Loading Compiled Result . . `

. . 

'`Building` . . ' is uh well -_- yea I think I've already belabored this point -_- but (1) it sounds nice to use 'building' as a term for 'in the process of being created' and (2) 'get that compiled result' but . . -_- it sounds dumb overall compared to 'developments' which are -_- a nice flat map idea for saying 'stop using dumb words like 'building' since it can be confusing since in this case it can apply for (1) 'building' => '`in the process of being created`' and (2) 'building' => '`in the process of getting`' . . which are possibly 2 different uh . . function names and so 'create' and 'get' are being hogged by one word . . or are being 'name spammed' by one term which is possibly a useful idea but I don't want to fantasize about that right now -_- . . also I'm a bad person for writing stuff like this since it's so biased so stop reading :O -_- it's biased to 'time-impatience' routines like -_- I don't want to think about this -_-

. . 

. . 

I'm sorry

. . 

I'm sorry . . 

. . 

I'm sorry . . 






[Written @ 19:45]

Okay well if you must not use real world names then you can use real world ideas like hmm . . things that excite people and get them out of bed to do real world things like idea creation and development of projects or things that are interesting but not necessarily interesting . . 

. . 

well . . 

. . 

It's interesting . . 

. . 

I'm sure you can imagine things on your own . . a tutorial like this one is meant to inspire a few '`catch me up with what that was`' ideas and so you'll be like '`yea, yea . . yea `' . . 

. . 

I don't claim to be a moral arbiter on 'what that was' but -_- you can also research and learn more . . right ? . . 

. . 

flat mapping . . it's so useful . . 

. . 

flat mapping . . 

. . 

Do you want to learn how to draw ? . . 

Do you want to learn how to paint ? . . 

Do you want to learn how to build an airplane ? . . 

Do you want to learn how to betray information systems ? . .

Do you want to learn how to O _ O do regretful things ? . . 

Do you want to learn how to be a nice person ? . . 

Follow this idea . . 

Flat map . . 

(1) have a list of friends

(2) have those friends tell you the things they like

(3) Pump up your energy enough to meet the demands of all those friends

(4) Pump up your energy to meet the demands of all those friends again because you're probably tired

(5) Pump up your energy and say 'yea, I guess friendship is really interesting' . . 'friends are like 'physics components' and 'people' and 'physics instruments' and 'drawing utensils' and 'paint brushes' and 'shoes' and all sorts of things that you can imagine'

(6) Friendly ghosts are also real so just be like O _ O yo ghost . . can you help me handwrite in secret spelling keys that are not known to human people ? . . 

-_- . . well -_- it's a myth you can do whatever and a lot of ghosts can 'autowrite' for you with only one meeting and so just let your hand do whatever . . 

. . 

Flattering comment

[Written @ 18:23]

unmeasured -> measured -> complete -> annoyedBy <br>

Get -> Loading [Loads] -> Loading Completed -> Inaccessible Information <br>
Update -> Setting [Settings] -> Update Completed -> Invalid Operation <br>
Create -> Developing [Developments] -> Development Completed -> Development Build Error <br>
Delete -> Deleting [] -> ?? -> ?? <br>

Recommend -> ?? -> ?? -> ?? <br>
Scale -> ?? -> ?? -> ?? <br>

. . 

Create -> Creating . . 

'Creating' 'Things That Have Been Created' 'Items Created By Your Account' 'Items Created By Your Service Account' 'Your Creations' 'Your Account Creations' 'Your Creations'

'In the process of being 'created'' . . 'Creation' . . 'Creatling' . . 'Cooking' 'Making' 'Building' . . '`Building`' . . '`Developing`' . . '

. . 

Delete -> Deleting . . 

'Deleting' 'In the process of being deleted' . . 'Erasing' . . 'Clearing Memory' . . 'Removing' . . 'Deleting' 'Clearing' . . 'Deleting' . . 

'In the process of being deleted' . . 


. . 

Get -> Loading [Get-Accessible-Items-Of-Interest] -> Loading Completed -> Inaccessible Information <br>
Update -> Setting [Update-Accessible-Items-Of-Interest] -> Update Completed -> Invalid Operation <br>
Create -> Developing [Create-Accessible-Items-Of-Interest] -> Development Completed -> Development Build Error <br>
Delete -> Deleting [Delete-Accessible-Items-Of-Interest] -> ?? -> ?? <br>

. . 

Get -> Loading [Loads] -> Loading Completed -> Inaccessible Information <br>
Update -> Setting [Settings] -> Update Completed -> Invalid Operation <br>
Create -> Developing [Developments] -> Development Completed -> Development Build Error <br>
Delete -> Deleting [] -> ?? -> ?? <br>

. . 

'Get' Could possibly contain an 'application directory list of services and other things to 'add' or 'get' for a project to 'install' or something like that' and so for example this could be an 'extension' listing . . or 'modifications' listing if 'game mods' are the idea of interest . . but more specifically . . if you're not necessarily requiring these 'extension' capabilities . . then you could possibly have a . . list of . . things of interest about your application that could be able to be loaded . . and so . . 'get' would list those items . . and so . . for example maybe there is a list of things that are currently being loaded 'in the background' and so those things could be listed in this area or section or region or map directory region of the program . . and so that could be useful for something . . I'm sorry . . I'm really not sure where I'm going with this . . but mostly doing research and experimenting to see what would possibly work with this listing of ideas . . in terms of especially saying that there is a list that comes to a good idea . . from what we had learned from a previous live stream about 'loading' . . [Ecoin #551](https://www.youtube.com/watch?v=cOlMPSL9cKY) . . 

. . 

[Get] -> [In-The-Process-Of-Being-Retrieved] -> [Get-Completed] -> [Bad-Get-Request]

[Update] -> [In-The-Process-Of-Being-Updated] -> [Update-Completed] -> [Bad-Update-Request]

[Create] -> [In-The-Process-Of-Being-Created] -> [Create-Completed] -> [Bad-Create-Request]

[Delete] -> [In-The-Process-Of-Being-Deleted] -> [Delete-Completed] -> [Bad-Delete-Request]

// Recommend

[Get-Response-For-Recommended] -> . . -> . . -> . . 

[Update-Settings-For-Recommended] -> . . -> . . -> . . 

[Create-Request-For-Recommended] -> . . -> . . -> . . 

[Delete-Default-Settings-For-Recommended] -> . . -> . . -> . . 

// Scale

[Get-Scale-Co-Factor] -> . . -> . . -> . . 

[Amortize-Scale] -> . . -> . . -> . . 

[Elementalize-Scale] -> . . -> . . -> . . 

[De-Elementalize-Scale] -> . . -> . . -> . . 

. . 

[In-Process-Of-Being-Create] . . is possibly an okay name . . uh . . in earlier examples I've been trying to 'elementalize' a name for those that would possibly be related to 'simple word' or 'simple word pattern' that is possibly 'amortizable' or 'co-factor related' to the term 'settings' which is like the equivalent 'update' related terminology that was discussed or discovered as being 'elemental' in the Ecoin #551 . . but that is only a hypothesis that this is useful anyway . . but it is still interesting . . I uh . . don't have a big problem with the word 'settings' . . it seems 'cute' and 'interesting' and especially since it is popular with development teams today who apply it in various applications of interest such as 'YouTube' 'Facebook' and other hypothesized places where I think it is currently being applied . . 

. . 

But the equivalent words for 'settings' . . according to our 'hypothetical map ordering matrix' that we've developed here . . doesn't seem to tell me a lot about what the other words would or could be that would fill the gap to allow other items of interest to also have their own . . uh . . idea construct related to those things . . for example . . 'Settings' or 'settings' is really related to . . the idea of . . 'update in progress' . . in other terms well that is really a hypothesis based on how we've flat mapped the term 'settings' into our so called 'insignificant order indicator' . . and so for example 'update' is a term that can be considered 'unordered' and so then . . it is like . . in order to 'order' the term 'update' we could say that . . it needs to be in the process of being ordered which means that . . that idea of 'in the process of being updated' or 'in the process of being ordered for the particular term of update' is possibly related to the term 'settings' which is possibly like 'hey, you may not have set an item yet, but it is waiting for you here and when you do so . . it is even still then waiting for you for ever and ever and ever as long as you like since 'settings' is like 'yea, i'll always wait for you to do something' or 'yea, i'll always be in the process of being ordered but I don't necessarily ever need to be in the 'ordered' state' and so 'ordered' is for example when you 'complete an update' but 'settings' are never truly completed since you can always change the 'color' of an item or something like that and so that is uh possibly an example of how you can 'update something' but you don't ever really need to make up your mind you can always come back to it . . and so . . 'settings' is like flat mapped onto 'inProcessOfBeingOrdered' . . uh . . we explored this map in a previous Ecoin Episode (Ecoin #551) so I won't talk about it a lot more than that . . 


```js
class TallyInsignificantOrderIndicator {
  unordered = null
  inProcessOfBeingOrdered = null
  ordered = null
  blockedBy = null
}
```

Which was inspired by . . 

```js
class TallyInsignificantItemEndpointIndicator {
  unmeasured = null
  measured = null
  complete = null
  annoyedBy = null
}
```


. . 

?? = null
?? = null
neighborhood = null
?? = null

. . 

the word 'neighborhood' appeared in my peripheral vision over the word 'ordered' just a few seconds ago . . and now I am wondering if that is a hint from aliens as to what to possibly think about the possibility of 'neighborhood' having its own 'neighborhood' of terms like the ones we've listed here . . and of course this 'neighborhod' listing is about 'flat mapping' onto this 'particular neighborhood chart' which is mostly inspired by 'insignificant item endpoint indicator' from my own personal perspective and for you . . you could possibly have other things that you would want to flat map onto . . 

An easy example of things you would possibly want to flat map on to is relationships that you are already familiar with . . 

Consider the following relationship . . 

```js
class RelationshipMap {
  mother = null
  grandMother = null
  father = null
  grandFather = null
}
```

. . 

Okay if you have a 'SIMPLE' relationship map like the one listed here with the so called name 'Relationship Map' or 'Relationship Map' . . you may be interested in 'well that is a simple relationship map' 'but what does that have to do with things of interest like physics or blah blah blah' . . -_- . . well that is up to you . . and that is possibly a point that I am trying to get across at this time -_- . . you are supposed to say that (1) you have a relationship map of interest like a made up relationship map that you can understand for yourself . . this one is provided for you as a simple one for you to read since you possibly have a clue as to what relationships are being built here . . but of course . . you want to apply your own other ideas to further apply to this relationship map since you like this relationship map so much . . 

mother is a person of interest . . this person can be 'someone you admire'

grandMother is a person of interest . . this person can be 'someone you really admire and read their history to learn more about them'

father is a person of interest . . this person can be 'someone you admire and secretly want to be careful to not make them upset'

grandFather is a person of interest . . this person can be 'someone you admire and secretly you are not sure what their ideas are about'

. . 

Okay . . with that sort of 'clued in flat map idea construction' . . 'does that match with your own natural intuition?' . . well if you are confused well I am sorry for you then this example will be useless and not nice . . but imagine they are fruits and that one fruit is good and the other is possibly not your favorite fruit but you want to say they are a fruit and they exist in the list of fruits that you know about . . 'mother' is a good fruit like a banana or a strawberry or a blueberry or a fruit that you like or something like that or a kiwi or a blah blah blah or a raspberry or a blah blah blah . . and grandmother is the same way . . and 'father' is a weird triangle that doesn't look like a fruit and so you are confused . . 'grandFather' is the same way . . 

. . 

Well . . already we have a flat map of ideas that we can say . . 

```js
class RelationshipMapInspiredByRelationshipMap {
  bananaFruit = null
  bananaFruit = null
  triangleConfusingFruit = null
  triangleConfusingFruit = null
}

```

I'm not really sure . . maybe that is a good 'flat mapping' of 'fruits' to 'the relationship map' we've created . . but maybe it is not . . 'flat maps' are like you want to 'align' each of the items in the 'relationship map' with your own understanding of things . . so if we start by aligning the idea of 'fruits' then we better make the rest of the map for that particular flat map example look like it has something to do with 'fruit' . . and or else we don't think we have succeeded or something like that . . I'm not sure . . the general idea is that you should try to 'align' and 'alignment' can mean a lot of different things depending on the flat map and where the ideas are trying to go . . or rather on the relationship map and where the ideas are trying to go . . the relationship . . the flat map . . those are used interchangeably in how this diction is being 'layed out' at this time : / . . layed out . . layed out . . like a carpet on the floor . . a flat map . . a nice flat . . map . . a large carpet has a lot of ideas and you want to lay other ideas on top of that carpet such that you form a 'solving' of a puzzle piece in your mind or something like that . . 

. . 

Okay that is a long winded explanation of what we are trying to do here . . we have a relationship map . . called 'insignificant endpoint item indicator' . . which is a made up name by the way . . but it is useful to use names so we love to use names and we are often recommended names by 'Seth' the 'spiritual personality ghost entity who is helping us write this computer program' . . 

. . 

```js
class RelationshipMap {
  mother = null
  grandMother = null
  father = null
  grandFather = null
}
```

```js
class RelationshipMapInspiredByRelationshipMap {
  bossAtWork = null
  bossPreviousEmployer = null
  oldHandicapCompanyNextDoor = null
  anOldCompetitorOfTheOldHandicapCompanyNextDoor = null
}
```

```js
class RelationshipMapInspiredByRelationshipMap {
  cartoonNetwork = null
  nickelodeon = null
  nickAtNite = null
  adultSwim = null
}
```

```js
class RelationshipMapInspiredByRelationshipMap {
  hairyMustache = null
  oldHairyMustache = null
  handleBarMustache = null
  oldHandleBarMustache = null
}
```

Flat maps look like '`artificial intelligence`' systems when you look at it . . it means you can say '`my friend reminds me of that thing`' . . and all the '`neighborhoods`' of '`that thing`' are '`available`' by the '`flat map categorizer`' . . and so 

'president A looks like president B' <br>
while <br>
'president B looks like president C' with respect to the previous flat map being observed <br>
while <br>
'president C looks like president D' with respect to the previous flat map list being observed <br>
'president D looks like president E' with respect to the previous flat map list being observed <br>

. . 

and so on and so forth and you don't have to use 'celebrities' of 'people' but you could possibly use 'celebrities' of way more interesting things like 'woah, look at those probable human beings that could have existed or even possibly exist . . isn't that so interesting that we have a population like those people . . aren't they so polite . . or what if IN FACT . . we had . . people like that . . they are so specific . . they are so . . eager . . so immediate . . so available . . and look at those flat map highlands that keep tem cornered in that aspect of existence' O _ O . . well well well . . 

. . 

Well you have to judge whether or not the flat map makes sense to you . . for example . . the flat map relationships made earlier could possibly not make sense or 'work' for your understanding of the world . . and so you're always welcome to create your own flat maps and help the 'artificial intelligence' system learn those flat maps . . 

. . 

Flat mapping makes life interesting in all sorts of ways (1) think of your friends now and all the interesting things they've done in their life . . (2) now flat map that onto celebrities in the 'technology' industry . . just for fun . . 

(1) Jeff Bezos <br>
(2) Elon Musk <br>
(3) Bill Gates <br>
(4) Larry Page <br>

. . 

If that was your relationship map and you had to map your friends to each of these . . and all your friends have to be included . . how many 'Elon Musks' would you have in your close friend list? . . 10 Elon Musks ? . . 11 Elon Musks? . . 15 Elon Musks? . . Go ahead . . now count your 'Bill Gates' . . 10 Bill Gates? . . 11 Bill Gates? . . How many Bill Gates can you muster up ? . . 

. . 

Whether or not your friends map onto these people directly is not really important . . you have to use your (1) imagination and (2) creativity to force yourself to place each of your friends in one of the 4 boxes provided . . or if you have more boxes then of course those are available . . but for the sake of an example like this one . . there are 4 items that are 'mandatory' . . and so . . you are going to have to say 0 for each if you really don't believe any of your friends map onto this structure which is a shame right since you're not being creative enough to say at least 1 of your friends could possibly map onto this list of 4 . . if at least 1 friend of yours can map onto this structure then that's great since then possibly the next 'neighborhood' for 'that friend' like things like 'oh yea I saw them at a movie theatre once' or 'oh yea they do this or that sport as a hobby' then possibly . . possibly possibly possibly you could also order that next 2nd or 3rd friend with respect to the 1 that you have already flat mapped onto the 4 available items . . 

. . 

If you need more angles to aspect these individuals in . . than . . just their names . . You have to do research like 'playing a video game to discover interesting statistics about their lives' . . 

(1) Jeff Bezos <br>
(2) Elon Musk <br>
(3) Bill Gates <br>
(4) Larry Page <br>

. . 

(1) who has accumulated the most wealth in terms of US Dollars <br>
(2) Who has lead the most companies as a Chief Executive Officer (C.E.O.)? <br>
(3) Who plays the most amount of video games with their children in their free time? <br>
(4) Who eats more potato chips? <br>

. . 

Alright . . 

I think Elon Musk has played the most amount of video games with their children . . and so . . it's possibly useful to say 'they are the most playful :D' . . 

. . 

So that's what an A.I. system could 'assume' or 'say' or 'pronounce' and so . . well . . it's 'only a hypothesis' but you can 'go a long way with even a small hypothesis' . . 

. . 

'which of your friends is the most playful?' . . map them onto 'Elon Musk' . . right ? . . 

. . 

-_- . . and of course that's not ever enough information . . 'eating potato chips could be the friendliest thing you can ever do' -_- . . and so if you don't have statistics on that information -_- . . -_-

. . 

-_- . . 

-_- . . 

. . 

-_- . . 

. . 

-_- . . 

-_- . . 

. . 

-_- . . 

. . 

. . 


-_- I'm sorry -_- . . I made a mistake by writing specific names of individuals in the real world . . I'm sorry . . 

. . 


