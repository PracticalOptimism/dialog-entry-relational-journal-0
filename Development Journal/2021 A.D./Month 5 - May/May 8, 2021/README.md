
# Development Journal Entry - May 8, 2021

[Written @ 13:13]




[Written @ 13:00]

Minimum Description List: `Service Administrator Account`, `law enforcement`, `privacy protection`, `neighborhood reasonable behaviors`, `safety`

Some algorithms . . are for 'administrators' only . . 

Service Administrator Accounts . . Service Administrator Account . . is responsible . . for . . being an 'administrator' of a service and so . . this can mean . . 'overwrite' privileges for all other rules that are being realized by the 'service community' . . or 'service account community' or something like that or 'network protocol member community' or something like that . . 

'`Service Administrator Account`' can be thought of like a 'private web server' where the owner or the authoritative writer of the server can provide functions or algorithms that run without having to feel afraid that . . a service account that is using those functions or having those functions 'applied' for them . . is going to possibly mishandle other . . things that the service would care about having available for itself . . 

Service Administrator Accounts . . for a web server . . means you can (1) have your own server rack or server set and be sure that you're going to treat your physical equipment properly . . (2) believe that god is not going to slam down on the physics that you assumed would hold true for all of time immemorial . . and so the god-alien-physicists are administrators for the physical racks you rely on . . so pray and ask them what is and isn't allowed . . 

(3) can I lazer beam my neighbors with lights and sounds that would disorient them? . . O _ O hmm . . that would be an interesting usecase . . let's see O _ O . . 

. . 

Okay . . well . . a service administrator account is a cool idea right? . . 

. . 

service account . . is cool as well . . 

. . 

You may need to clarify in your program . . which aspect is meant for 'service account' (a general user) and a 'service administrator account' (a personal private protective user) . . or . . something . . like . . that . . right? . . right? . . 

. . 

```js
// _service.js

/*

Table of Contents:
- (1) Import

// Precompiled Usecase #1

- (2) Service Account Related Items
  - (2.1) Constants
  - (2.2) Variables
  - (2.3) Data Structures
  - (2.4) Algorithms
  - (2.5) Infinite Loops

- (3) Service Administrator Account Related Items
  - (3.1) Constants
  - (3.2) Variables
  - (3.3) Data Structures
  - (3.4) Algorithms
  - (3.5) Infinite Loops

- (4) Export

*/

```


[Written @ 12:45]

Minimum Description List: `table of contents ordering`

[2.1] A general pattern for organizing the contents of your file can be . . grouping . . the items of interest like 'algorithms' and 'data structures' into groups that you can use . . or that are relevant for your program . . if for example . . certain items of interest must be 'imported' or 'available' in the file listing before other items are available . . such as (1) variables need to be defined above . . or before . . or in the listing order of 'before the line of code' where the algorithm or other item of interest references that item . . well then . . depending on cases like this where you may need to have an appropriate ordered . . file listing . . then you may need to re-arrange the order of your indexed items of interest . . and so you can . . reference how that indexing looks in a 'table of contents' listing above . . inside of the file . . or something like that . . 

```js
// _service.js file

/*
Possible Table Of Contents Ordering

Table of Contents:
- (1) Import

- (2) Algorithms
- (3) Data Structures

- (4) Algorithms
- (5) Data Structures

- (6) Algorithms
- (7) Data Structures
*/

/*
Possible Table Of Contents Ordering Type #2

Table of Contents:
- (1) Import

// Store For Item #1
- (2) Constants
- (3) Data Structures

// Store For Item #2
- (4) Constants
- (5) Data Structures

// Program For Item
- (6) Algorithms

// Creator For Item
- (7) Constants

// Dialog For Item
- (8) Constants

// Compiler Usecase #1
- (9) Constants
- (10) Variables
- (11) Data Structures
- (12) Algorithms
- (13) Infinite Loops

// Precompiled Usecase #1
- (14) Constants
- (15) Variables
- (16) Data Structures
- (17) Algorithms
- (18) Infinite Loops

// Precompiled Usecase #2
- (19) Constants
- (20) Variables
- (21) Data Structures
- (22) Algorithms
- (23) Infinite Loops

- (24) Export

*/

```

[Written @ 12:30]

Minimum Description List: `Item Of Interest Exporting`

```js
// _service.js file


/*

// not necessarily the order that you want your personal program to look but using these item index names is possibly of relevance to you . . see [2.1]
Table of Contents:
- (1) Import
- (2) Algorithms
- (3) Data Structures
- (4) Constants
- (5) Variables
- (6) Infinite Loops
- (7) Export

*/


// (1) import various items of interest 'photographs', 'videos', 'javascript files', 'computer programs', etc.

import { itemOfInterest, itemOfInterestType2, itemOfInterestType3 } from 'place-of-interest/item-of-interest.file'
import { itemOfInterest2 } from 'place-of-interest/item-of-interest-2.file'
import { itemOfInterest3 } from 'place-of-interest/item-of-interest-3.file'

// (2) algorithms
function getItem () {}
function createItem () {}
function updateItem () {}
function deleteItem () {}
function performActionForItem () {}

// algorithms for 'recommend'

function getResponseForRecommendedItem () {}
function createRequestForRecommendedItem () {}
function updateSettingsForRecommendedItem () {}
function deleteDefaultSettingsForRecommendedItem () {}

// algorithms for 'scale'

function getScaleCoFactorForItem () {}
function elementalizeScaleForItem () {}
function amortizeScaleForItem () {}
function deElementalizeScaleForItem () {}

// (3) data structures
class Item {}
class ItemThingForItem {}
class ItemThingForItemPart2 {}
class ItemThingForItemPart3 {}
class ItemThingForItemPart4 {}

class ServiceAccount {}
class ServiceAccountRegistration {} // register to log in to an account; this is like possibly a token or json web token or secret password authorization item for verifying that you're logged in as a particular service account . . 'registration' is a nicer name than 'session' or -_- other variants that I could think of at this time

class ServiceAdministratorAccount {}
class ServiceAdministratorAccountRegistration {}

// (4) constants
const serviceName = 'yea'
const serviceDescription = 'okay'
const serviceDateCreated = 'yes'
const serviceDateLastUpdated = 'yes'
const serviceAdministratorName = 'yea'
const serviceAdministratorDescription = 'okay'
const serviceAdministratorContactInformation = 'yes'
const serviceEmergencyAction = 'yes'

// (5) variables

let serviceDatabase = {}
let serviceEventListenerDatabase = {}
let serviceNetworkProtocolDatabase = {}
let servicePermissionRuleDatabase = {}
let serviceComputerProgramManagerDatabase = {}
let serviceScaleMatrixAmanaDatabase = {}
let serviceItemChangerDatabase = {}
let serviceItemReverseChangerIndicatorDatabase = {}
let serviceItemPinchFactorCoRelatorDatabase = {}
let serviceAnanaAnanaAnanaDatabase = {}

// (6) infinite loops

function startApplyingInfiniteLoop () {}
function stopApplyingInfiniteLoop () {}
function getInfiniteLoopStatistics () {}
function updateInfiniteLoop () {}

// infinite loops for 'recommend'

function getResponseForRecommendedInfiniteLoop () {}
function createRequestForRecommendedInfiniteLoop () {}
function updateSettingsForRecommendedInfiniteLoop () {}
function deleteDefaultSettingsForRecommendedInfiniteLoop () {}

// infinite loops for 'scale'

function getScaleCoFactorForInfiniteLoop () {}
function elementalizeScaleForInfiniteLoop () {}
function amortizeScaleForInfiniteLoop () {}
function deElementalizeScaleForInfiniteLoop () {}

// applied infinite loop algorithms

function applyThing1ForItem () {}
function applyThing2ForItem () {}
function applyThing3ForItem () {}
function applyThing4ForItem () {}


// 'ItemOfInterest'-export for 'data structure replicability'
class ItemOfInterest {
  // algorithms
  getItem,
  createItem,
  updateItem,
  deleteItem,
  performActionForItem,

  getResponseForRecommendedItem,
  createRequestForRecommendedItem,
  updateSettingsForRecommendedItem,
  deleteDefaultSettingsForRecommendedItem,

  getScaleCoFactorForItem,
  elementalizeScaleForItem,
  amortizeScaleForItem,
  deElementalizeScaleForItem,

  // data structures
  Item,
  ItemThingForItem,
  ItemThingForItemPart2,
  ItemThingForItemPart3,
  ItemThingForItemPart4,

  ServiceAccount,
  ServiceAccountRegistration,

  ServiceAdministratorAccount,
  ServiceAdministratorAccountRegistration,

  // constants
  serviceName,
  serviceDescription,
  serviceDateCreated,
  serviceDateLastUpdated,
  serviceAdministratorName,
  serviceAdministratorDescription,
  serviceAdministratorContactInformation,
  serviceEmergencyAction,

  // variables
  serviceDatabase,
  serviceEventListenerDatabase,
  serviceNetworkProtocolDatabase,
  servicePermissionRuleDatabase,
  serviceComputerProgramManagerDatabase,
  serviceScaleMatrixAmanaDatabase,
  serviceItemChangerDatabase,
  serviceItemReverseChangerIndicatorDatabase,
  serviceItemPinchFactorCoRelatorDatabase,
  serviceAnanaAnanaAnanaDatabase,

  // infinite loops
  startApplyingInfiniteLoop,
  stopApplyingInfiniteLoop,
  getInfiniteLoopStatistics,
  updateInfiniteLoop,
  
  getResponseForRecommendedInfiniteLoop,
  createRequestForRecommendedInfiniteLoop,
  updateSettingsForRecommendedInfiniteLoop,
  deleteDefaultSettingsForRecommendedInfiniteLoop,

  getScaleCoFactorForInfiniteLoop,
  elementalizeScaleForInfiniteLoop,
  amortizeScaleForInfiniteLoop,
  deElementalizeScaleForInfiniteLoop,
  
  applyThing1ForItem,
  applyThing2ForItem,
  applyThing3ForItem,
  applyThing4ForItem
}

// Exporting a 'table' or 'dictionary' or 'object' or 'map' data structure is useful for 'pass by reference' in 'javascript' which lets you (1) 'share' the 'default' item of interest and (2) add more items of interest that are area-of-interest proportional to what was exported here

const anotherItemOfInterest = new ItemOfInterest()

anotherItemOfInterest.getItem = function () {
  console.log('a new implementation of this function at runtime')
}

const experimentalVersionOfItemOfInterestInstance = new ItemOfInterest()

experimentalVersionOfItemOfInterestInstance.createItem = function () {
  console.log('this new version has a more efficient create item function which is rendered at runtime . . thanks for using it . . ')
}

const itemOfInterestTable = {
  defaultItemOfInterestClass: ItemOfInterest,
  defaultItemOfInterestInstance: new ItemOfInterest() // this is the default recommended item of interest but others may be available for 'auto-correcting' mistakes made by this default recommended . . users can provide an updated version of a function that would possibly fix an issue . . at runtime without re-compiling the program . . which is useful for artificial intelligence programs . . for speed . . or something like that . . possibly -_- I don't know . . it's just an experiment . . 
  anotherItemOfInterestInstance: anotherItemOfInterest,
  experimentalVersionOfItemOfInterestInstance: experimentalVersionOfItemOfInterestInstance
}

// (7) export

export {
  itemOfInterestTable
}
```


[Written @ 11:28]

There are many usecases for 'store' 'program' 'creator' 'dialog'

. . 

Types . . are a way to organize the stored items in each of those . . directories . . 

However . . you don't need to organize the directory with 'types' . . if you want . . you can cancel that operation . . or terminate . . that operation . . and say . . 

flat map . . 

Flat mapping lets you get to work very quickly . . 

Have the file you need without further 'type' segmentation . . types are useful for a lot of organizational purposes . . but eventually you may want to 'flat map' . . 

Flat map agreements . . take the thing you like to store . . and just make the file right away . . 

. . 

- /store-for-vegetarian-dish-photographs
  - vegetarian-dish-1.png
  - vegetarian-dish-2.png
  - vegetarian-dish-3.png
  - vegetarian-dish-4.png
  - vegetarian-dish-5.png

. . 


That's all you need . . 

Now . . not flat mapping this list . . will possibly give you more 'orders' of 'information categorization' to consider . . 

- /store-for-vegetarian-dish-photographs
  - /vegetarian-dish-type-1-soup
    - soup-dish-1.png
    - soup-dish-2.png
    - soup-dish-3.png
    - soup-dish-4.png
  - /vegetarian-dish-type-2-lasagna
    - lasagna-dish-1.png
    - lasagna-dish-2.png
    - lasagna-dish-3.png
  - /vegetarian-dish-type-3-whole-grain
    - whole-grain-dish-1.png
    - whole-grain-dish-2.png
    - whole-grain-dish-3.png
  - /vegetarian-dish-type-4-lactose-free
  - /vegetarian-dish-type-5-egg-free
  - /vegetarian-dish-type-6-tofu-based
  - /vegetarian-dish-type-7-idea-type-1

. . 

Although we have types listed in the example above . . it is still useful to say that . . we have 'flat mapped' even though the 'flat map' is respective to idea categories like 'soup' 'lasagna' and more . . 

Flat map hierarchies can be . . 2 . . 3 . . 4. . 5. . etc . . if you don't have types . . you can say . . 'flat map level 1' . . or something like that . . 'flat map without orders of hierarchies' or something like that . . 

Also don't forget . . that you can make . . mistakes . . if you make mistakes . . that's okay . . you always have the same idea of 'store-for-xyz' available for you . . and so . . if you want you can re-index everything at runtime on your own . . and you don't have to arrange or remix or rematch everything that you have already listed or something like that . . 

. . 

- /store-for-vegetarian-dish-photographs
  - /vegetarian-dish-type-1-soup
    - soup-dish-1.png
    - soup-dish-2.png
    - soup-dish-3.png
    - soup-dish-4.png
  - /vegetarian-dish-type-2-lasagna
    - lasagna-dish-1.png
    - lasagna-dish-2.png
    - lasagna-dish-3.png
  - /vegetarian-dish-type-3-whole-grain
    - whole-grain-dish-1.png
    - whole-grain-dish-2.png
    - whole-grain-dish-3.png
  - /vegetarian-dish-type-4-lactose-free
  - /vegetarian-dish-type-5-egg-free
  - /vegetarian-dish-type-6-tofu-based
  - /vegetarian-dish-type-7-idea-type-1
  - /store-for-vegetarian-dish-photographs-but-this-time-lets-order-them-differently-because-i-may-have-messed-up-somewhere-where-i-didnt-want-to
    - /vegetarian-dish-type-1-contains-eggs-and-flour
    - /vegetarian-dish-type-2-contains-fruits-and-nuts
    - /vegetarian-dish-type-3-contains-pesticide-free
    - /vegetarian-dish-type-4-contains-sugar-or-honey
      - _service.js [1.0]
  - /store-for-vegetarian-dish-photographs-but-again-i-just-to-have-fun-and-see-if-i-can-order-the-things-a-little-differently
  - /store-for-vegetarian-dish-photographs-just-for-fun-ordering
  - /store-for-vegetarian-dish-photographs-cool-idea
  - /store-for-vegetarian-dish-photographs-cool-idea-thanks-for-the-recommendation-person-of-interest


_service.js [1.0]

```js
// This is a special file for allowing you to do things like 'import' from other files . . 

// Example dishes that contain 'sugar or honey'

import soupDish2 from '../../vegetarian-dish-type-1-soup/soup-dish-2.png'
import soupDish3 from '../../vegetarian-dish-type-1-soup/soup-dish-3.png'
import soupDish4 from '../../vegetarian-dish-type-1-soup/soup-dish-4.png'

export {
  soupDish2,
  soupDish3,
  soupDish4
}

```

Doing things this way . . by importing . . items into a file . . allows you to . . 'reference' that file . . and you don't need to . . uh . . for example . . move . . the existing file . . from its already . . existing location . . That can save a lot of time ! . . 

Exporting . . the images helps you . . re-locate those imports in other files that you need . . and so it is very . . resource . . effective . . or resource efficient . . :D . . right?! do you agree . . well . . let us know . . 

. . 

Using the name '_service.js' is useful since it is possibly a name that we aren't already going to use in the directory . . 

'`the-light-architecture`' uses '`@service`' (the folder type) and '`_service`' (the file type) to delineat the idea of a 'service' item . . a 'service' item is able to . . :O . . do anything :O . . you sitting there or standing there or . . O _ O doing something . . watching a video like this one . . or reading these notes . . or having these notes . . being read to you . . O _ O . . that means that is a service that you are providing O _ O . . and you don't even know it O _ O . . you are serving so well . . and all the things of interest can serve if you want to identify that character and so a '_service.js' file is nice to be a general case for all the things of interest . . but overusing it can be a pain if you use it everywhere -_- people may not like that . . even breathing is a service for plants . . and pooping a service for your body to feel less achy with poop pains . . and so . . if you have a service like scratching your body or trimming your fingernails O _ O . . -_- O _ O - _ - O _ O . . then that service is . . identifiable as a service . . wouldn't you agree? . . 

Well services are especially nice for 'referencing' and so you can say 'yes, that is something that this service depends on' . . 'scratching my bottom with a bottom scratcher requires (1) a bottom scratcher and a (2) hand for using the bottom scratcher' and if you use your hand for a bottom scratcher O _ O . . then you don't need O _ O 

. . 

O _ O 

. . 

A service O _ O . . is very useful O _ O . . 

. . 

Don't forget . . that _service.js is an idea . . and so for example . . now you don't have to say . . 'don't add anything else to the folder' . . you can say . . 'add this or that to the folder' and that should be great . . and you don't necessarily need to import it to the service . . but the service is nice . . uh . . for a general exporter . . 

I haven't really thought about it personally until now but possibly you could also have more than one . . @service or _service for each folder . . 

- /@service-1
- /@service-2

. . 

- /folder-name
  - _service-1.js
  - _service-2.js

. . 

I really don't know . . it's possible I guess . . I haven't personally had a usecase for this yet . . but the theory is there right?! :O . . 

. . 

`@service` and `_service.file-extension` are meant to be used interchageably . . if your @service folder contains a lot of folders . . then you can O _ O yea . . use a folder instead of using 1 file to contain that list of things of interest . . 1 file is useful if you forget O _ O . . 

// `_service.js`

```js
// _service.js file

// import various items of interest 'photographs', 'videos', 'javascript files', 'computer programs', etc.

import { itemOfInterest, itemOfInterestType2, itemOfInterestType3 } from 'place-of-interest/item-of-interest.file'
import { itemOfInterest2 } from 'place-of-interest/item-of-interest-2.file'
import { itemOfInterest3 } from 'place-of-interest/item-of-interest-3.file'


// algorithms
function getItem () {}
function createItem () {}
function updateItem () {}
function deleteItem () {}
function performActionForItem () {}

// algorithms for 'recommend'

function getResponseForRecommendedItem () {}
function createRequestForRecommendedItem () {}
function updateSettingsForRecommendedItem () {}
function deleteDefaultSettingsForRecommendedItem () {}

// algorithms for 'scale'

function getScaleCoFactorForItem () {}
function elementalizeScaleForItem () {}
function amortizeScaleForItem () {}
function deElementalizeScaleForItem () {}

// data structures
class Item {}
class ItemThingForItem {}
class ItemThingForItemPart2 {}
class ItemThingForItemPart3 {}
class ItemThingForItemPart4 {}

// constants
const serviceName = 'yea'
const serviceDescription = 'okay'
const serviceDateCreated = 'yes'
const serviceDateLastUpdated = 'yes'
const serviceAdministratorName = 'yea'
const serviceAdministratorDescription = 'okay'
const serviceAdministratorContactInformation = 'yes'
const serviceEmergencyAction = 'yes'

// variables

let serviceDatabase = {}
let serviceEventListenerDatabase = {}
let serviceNetworkProtocolDatabase = {}
let servicePermissionRuleDatabase = {}
let serviceComputerProgramManagerDatabase = {}
let serviceScaleMatrixAmanaDatabase = {}
let serviceItemChangerDatabase = {}
let serviceItemReverseChangerIndicatorDatabase = {}
let serviceItemPinchFactorCoRelatorDatabase = {}
let serviceAnanaAnanaAnanaDatabase = {}

// infinite loops

function startApplyingInfiniteLoop () {}
function stopApplyingInfiniteLoop () {}
function getInfiniteLoopStatistics () {}
function updateInfiniteLoop () {}

// infinite loops for 'recommend'

function getResponseForRecommendedInfiniteLoop () {}
function createRequestForRecommendedInfiniteLoop () {}
function updateSettingsForRecommendedInfiniteLoop () {}
function deleteDefaultSettingsForRecommendedInfiniteLoop () {}

// infinite loops for 'scale'

function getScaleCoFactorForInfiniteLoop () {}
function elementalizeScaleForInfiniteLoop () {}
function amortizeScaleForInfiniteLoop () {}
function deElementalizeScaleForInfiniteLoop () {}

// applied infinite loop algorithms

function applyThing1ForItem () {}
function applyThing2ForItem () {}
function applyThing3ForItem () {}
function applyThing4ForItem () {}

// export

export {
  // algorithms
  getItem,
  createItem,
  updateItem,
  deleteItem,
  performActionForItem,

  getResponseForRecommendedItem,
  createRequestForRecommendedItem,
  updateSettingsForRecommendedItem,
  deleteDefaultSettingsForRecommendedItem,

  getScaleCoFactorForItem,
  elementalizeScaleForItem,
  amortizeScaleForItem,
  deElementalizeScaleForItem,

  // data structures
  Item,
  ItemThingForItem,
  ItemThingForItemPart2,
  ItemThingForItemPart3,
  ItemThingForItemPart4,

  // constants
  serviceName,
  serviceDescription,
  serviceDateCreated,
  serviceDateLastUpdated,
  serviceAdministratorName,
  serviceAdministratorDescription,
  serviceAdministratorContactInformation,
  serviceEmergencyAction,

  // variables
  serviceDatabase,
  serviceEventListenerDatabase,
  serviceNetworkProtocolDatabase,
  servicePermissionRuleDatabase,
  serviceComputerProgramManagerDatabase,
  serviceScaleMatrixAmanaDatabase,
  serviceItemChangerDatabase,
  serviceItemReverseChangerIndicatorDatabase,
  serviceItemPinchFactorCoRelatorDatabase,
  serviceAnanaAnanaAnanaDatabase,

  // infinite loops
  startApplyingInfiniteLoop,
  stopApplyingInfiniteLoop,
  getInfiniteLoopStatistics,
  updateInfiniteLoop,
  
  getResponseForRecommendedInfiniteLoop,
  createRequestForRecommendedInfiniteLoop,
  updateSettingsForRecommendedInfiniteLoop,
  deleteDefaultSettingsForRecommendedInfiniteLoop,

  getScaleCoFactorForInfiniteLoop,
  elementalizeScaleForInfiniteLoop,
  amortizeScaleForInfiniteLoop,
  deElementalizeScaleForInfiniteLoop,
  
  applyThing1ForItem,
  applyThing2ForItem,
  applyThing3ForItem,
  applyThing4ForItem
}
```



[Written @ 11:11]

Here are a list of . . 4 directories that you can add to any directory in the '`the-light-architecture`' tally data structure . . something like that . . 

- /store-for-item
- /program-for-item
- /creator-for-item
- /dialog-for-item

. . 

Example Usecase #1

- /store-for-cat-photographs
- /store-for-dog-photographs
- /store-for-penguin-photographs
- /store-for-flamingo-photographs
- /store-for-youtube-videos
- /store-for-personal-music-albums
- /store-for-my-friends-music-albums
- /store-for-the-presidents-music-albums
- /store-for-alien-research
- /program-for-catapulting-rocks-into-the-air
- /program-for-catapulting-scissors-into-the-air
- /program-for-catapulting-papers-into-the-air
- /program-for-creating-magnets
- /program-for-creating-rubber-bands
- /program-for-creating-gear-motors
- /program-for-insectalizing-my-homework
- /program-for-walking-on-my-hands
- /program-for-walking-backwards
- /program-for-cooking-pancakes
- /program-for-having-orders-of-magnitudes-of-computer-resources
- /program-for-garbage-collecting-resources
- /program-for-evaporating-planets
- /program-for-insectalizing-planets
- /program-for-beating-a-banana-over-someones-head
- /program-for-eating-bananas
- /program-for-eating-cranberries
- /program-for-looking-with-open-eyes
- /program-for-studio-ghibli-film-automatic-creation
- /program-for-sleeping-in-and-not-answering-your-questions
- /program-for-crying
- /program-for-lying-to-you
- /creator-for-doing-laundry
- /creator-for-doing-the-chores-for-the-house
- /creator-for-walking-the-dog
- /creator-for-transporting-sunlight-into-my-house-for-free
- /creator-for-transporting-sunlight-into-my-house-for-chores-like-watch-a-good-animated-television-show
- /creator-for-transporting-sunlight-into-my-house-without-enemies-looking-at-where-i-live
- /creator-for-helping-me-walk
- /creator-for-helping-me-speak
- /creator-for-answering-questions-without-my-conscious-awareness
- /creator-for-blah-blah-blah-go-away
- /creator-for-blah
- /creator-for-forgetting-real-life
- /dialog-for-why-are-you-a-banana
- /dialog-for-did-you-forget-to-do-your-chores
- /dialog-for-did-you-plan-to-make-that-video-before-the-year-is-over
- /dialog-for-did-you-want-to-give-someone-a-high-five
- /dialog-for-did-you-want-to-eat-a-vegetable
- /dialog-for-did-you-plan-to-eat-a-vegetable
- /dialog-for-what-type-of-movie-would-achieve-those-results
- /dialog-for-what-type-of-movie-would-stop-that-idea-from-taking-place
- /dialog-for-did-hitler-really-specify-racism-as-a-cure-for-humanity
- /dialog-for-did-that-really-have-a-good-message-that-we-should-integrate-in-our-web-application
- /dialog-for-is-this-a-serious-company
- /dialog-for-is-our-company-going-to-get-new-toilets
- /dialog-for-is-our-company-full-of-people-that-enjoy-pooping-for-fun
- /dialog-for-is-our-company-tired-of-good-poop-music-not-existing
- /dialog-for-is-there-a-peanut-butter-available-that-produces-good-poop
- /dialog-for-is-there-a-good-music-that-will-help-sewers-smell-like-gardens
- /dialog-for-is-there-a-furnace-to-place-poop-in-to-evaporate-its-existence-for-free
- /dialog-for-is-there-a-nice-catapult-for-equalizing-poop-with-nice-tea-and-coffee-material
- /dialog-for-do-you-like-to-eat-poop
- /dialog-for-answering-questions
- /dialog-for-looking-and-why-humans-have-an-idea



[Written @ 10:44]

You can loop over each of these 4 items (1) `store-for-item` (2) `program-for-item` (3) `creator-for-item` and (4) `dialog-for-item`

Each of these 4 items are able to be . . repeated in any directory of interest . . 

A benefit for using these 4 items is to (1) differentiate the program from only being a 'for usecase' type of device which is linearated towards 'applying a usecase' but it can then also be considered to be a 'store' for general items . . and a 'program for general items' and a 'creator for general items' and a 'dialog for general items'

. . 

A `store` means (1) You can treat `the light architecture data structure as a database` . . a database lets you access resources . . not necessarily for the sake of the existing program . . but possibly for arbitrary usecases . . like 'I just wanted to add something to this directory structure' or 'I just wanted to add a photograph of this or that item' or 'I just wanted to add a book pdf reference to this or that folder of interest' or something like that . . `image.png` . . is a nice item to store in a database . . 

A `program` means (2) You can treat `the light architecture data structure as a data structure that contains . . or . . stores . . programs` . . programs that aren't necessarily meant to be interpretted as 'face-value' items of 'that reads a name that you aren't necessarily supposed to parse and re-interpret' . . and so . . if a 'store' contains 'items' that have 'text' that isn't necessarily meant to be reverse searched or indexed through searching other program hierarchies . . then . . those items will possibly not need an 'interpretter' . . an . . 'interpretter' . . is a computer program . . a computer program can parse text . . and . . do things like . . manipulate the order of items of text . . and that can be a way to 'relevantize' the 'text' or 'make the text more relevant' depending on the purpose of the computer program . . or something like that . . and so . . uh . . possibly . . these program . . files . . uh . . can be expected to be re-interpretted or something like that and so . . '`program.js`' is a fine program to have in these . . 'program stores' . . uh . . which they are stores but you don't need to write that since . . uh . . stores will be assumed uh . . for a lot of things that you place into the data structure . . 

A `creator` means (3) you can 'treat `the light architecture as a data structure that provides . . a creator` . . ' . . a creator . . is possibly like a computer program . . but the computer program is possibly already going to be assumed to be . . '`start by default`' . . and so for example . . if you have a '`creator.js`' . . then that creator file will possibly already contain programs that . . a computer program that is reading the data structure is supposed to `start automatically` . . and `possibly even assume that that program has already been started` . . otherwise it is possibly a 'simple program' with 'functions' . . 'functions' that don't need to be called . . or 'functions' that are 'helper functions' that don't need to . . be evaluated . . 

A `dialog` means (4) you can 'treat `the light architecture as a data structure that providers . . dialog abilities` . . dialog . . abilities are things like 'what else do we need to talk about?' . . If a team of developers is working on a codebase for example . . there may be questions of (1) what to do next (2) where to get inspiration (3) research references that are informal (4) ideas (5) suggestions from the community . . and so on and so forth . . There are many categories of ideas that you can have and you may not know how to index them . . for dialogs . . you don't need to know how to index them . . you can store random numbers in a list if you really wanted to . . and so dialog allows you to do that without necessarily havocing your other aspects of the codebase . . '`a-random-number-list.txt`'

. . 



. . 


Item Name is . . <item name> which means that . . in the way that it's being used if you see 'item-name' here . . you can say that it is 'any particular item name so for example 'dog' or 'cat' or 'bicycle' will work and so on and so forth'

. . 

Item Type . . is . . a . . prefix character for each of the types of things that are there . . and so for example . . 

. . 

- /store-for-dog-photographs
  - /item-type-1-for-dogs-with-ears
  - /item-type-2-for-dogs-without-ears
  - /item-type-3-for-dogs-with-collars
  - /item-type-4-for-dogs-without-collars
  - /store-for-blue-dogs
    - /item-type-1-for-dogs-with-a-particular-fur-pattern
    - /item-type-2-for-dogs-with-a-guest-animal-in-the-photograph
    - /item-type-3-for-dogs-with-an-owner-in-the-photograph
  - /store-for-dog-photographs
    - /item-type-1-for-dogs-that-wear-scarves
    - /item-type-2-for-dogs-that-wear-mittens
    - /item-type-3-to-represent-that-the-previous-hierarchical-directory-did-not-necessarily-describe-the-item-type-category-listing-that-would-have-otherwise-been-preferred-for-another-general-usecase
    - /item-type-5-for-another-representative-group-that-is-not-necessarily-tied-to-the-hierarchical-grouping-of-the-containing-folder-or-something-like-that

. . 

- /store-for-item
  - /item-type-1-for-item-name
  - /item-type-2-for-item-name
  - /store-for-item
    - /item-type-1-for-item-name
    - /item-type-2-for-item-name
    - /item-type-N-for-item-name
    - /store-for-item
      - /item-type-1-for-item-name
      - /item-type-2-for-item-name
    - /item-type-N-plus-1-for-item-name
    - /item-type-N-plus-2-for-item-name
  - /store-for-item-2
  - /store-for-item-type-3-item-name
  - /store-for-item-type-4-item-name
    - /item-type-1-for-item-name
    - /item-type-2-for-item-name
    - /item-type-3-for-item-name


. . 

- /store-for-item
- /program-for-item
- /creator-for-item
- /dialog-for-item

. . 

- /store-for-item
  - /item-type-1-item-name
  - /item-type-2-item-name
  - /item-type-3-item-name
  - /item-type-4-item-name
- /program-for-item
  - /item-type-1-item-name
  - /item-type-2-item-name
  - /item-type-3-item-name
- /creator-for-item
  - /item-type-1-item-name
  - /item-type-2-item-name
- /dialog-for-item
  - /item-type-1-item-name


[Written @ 10:41]

Notes @ Thoughts and Ideas On Permission Rules For Computer Programs

- permission-rule-for-`catching-errors-while-function-evaluation`.ts
- permission-rule-for-`completing-function-evaluation`.ts
- permission-rule-for-`evaluating-function`.ts
- permission-rule-for-`creating-function`.ts

. . 

- permission-rule-for-`catching-errors-while-function-evaluation`.ts // nothing else #1 order
- permission-rule-for-`completing-function-evaluation`.ts // need to know order
- permission-rule-for-`evaluating-function`.ts // time order
- permission-rule-for-`creating-function`.ts // space oreder

. . 

- permission-rule-for-catching-errors-while-function-evaluation.ts (`runtime exception handling linter`)
- permission-rule-for-completing-function-evaluation.ts (`runtime completion linter`)
- permission-rule-for-evaluating-function.ts (`runtime linter`)
- permission-rule-for-creating-function.ts (`compile time linter`)

permission-rule-for-catching-errors-while-function-evaluation.ts <br>
// seems equivalent to -> permission-rule-for-updating-item.ts

. . 

permission-rule-for-completing-function-evaluation.ts <br>
// seems equivalent to -> permission-rule-for-updating-item.ts

. . 

permission-rule-for-evaluating-function.ts <br>
// seems equivalent to -> permission-rule-for-updating-item.ts

. . 

permission-rule-for-creating-function.ts <br>
// seems equivalent to -> permission-rule-for-creating-item.ts




[Written @ 10:30]

Item Changer

Item Reverse Changer Indicator

Item Pinch Factor Co-Relator

. . 



Notes @ Newly Discovered Ideas

Service Test

. . 

`Expectation Definitions` and `Unit Tests` have been previous names for 'testing your program to ensure that it meets expected . . performance . . or results . . or something like that . . '

. . 

User Interface Testing is also another item of interest . . 

. . 

Service Test . . 

Service Validation . . 

getIsItemBeingValidService

getIsItemBeingValid

. . 

permissionRule

. . 

service-account

service-administrator-account

. . 

getIsServiceBeingValid

. . 

Resolving to true . . is a nice way to then . . ensure that our service . . for example . . a function . . is being . . evaluated as the expected result . . 

. . 

`Linting` seems to often relate to . . compile-time . . service validation . . to ensure the linting rules of the program are being followed . . 

`Runtime-Linting` or Unit testing at runtime could possibly relate to . . service validataion . . to ensure the linting rulles are being followed . . 

And so . . service requests would return errors or a computer program would create a log . . and those items would be assumed to be the desired behavior . . and any undesired behavior . . even at runtime . . could be . . caught . . and discovered . . very easily if it was tested for . . or if it was accounted for . . or something like that . . 

. . 



[Written @ 10:15]

Item Changer


