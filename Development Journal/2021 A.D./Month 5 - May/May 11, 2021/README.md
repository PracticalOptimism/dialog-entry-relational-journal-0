

# Development Journal Entry - May 11, 2021

[Written @ 18:22]

```js

const pamper = new Pamper()

pamper.addItem({
  itemName: 'computer-resource',
  itemResourceLocationId: 'https://computer.com/program',
  itemResourceReverseIndicatorDescriptionList: ['cool program', 'program with cool features', 'cool program with cool cool cool features', 'cool']
})

pamper.getItem('cool program', 1) // [{ itemIndexNumber: 0 }]




```


[Written @ 17:58]

```js

const pamper = new Pamper()

pamper.addItem('https://computer.com/program')
pamper.addItem('https://computer-2.com/program')
pamper.addItem('https://computer-3.com/program')

// what should this do??
pamper.changeItem('evaluate-function', 'program-id', ['hello', 'world'])

// assumptions
/*

(1) 3 different computers with the ability to run arbitrary programs

(2) Load balance the programs?

(3) All 3 computers should run the program?

*/


```

```js

const pamper = new Pamper()

pamper.addItem('https://computer.com/program')
pamper.addItem('https://computer-2.com/program')
pamper.addItem('https://computer-3.com/program')

pamper.changeItem()

/*

One idea that I have in mind is being able to have multiple computers in a network together and for some reason they are able to find inforamation . . back and forth and to and from one another . . something like that . . That is one usecase that I have in mind . . but now I need to think (1) how will Pamper help with that ? . . 

. . 

Computer One is connected to Two, Three

Computer Two is connected to Three, Four

Computer Three is connected to Five

. . 

In this non-bidirectional . . switching mechanism computer network where a connection could possibly mean . . information is going from the left most part to the right most party . . 

what are some reasonable things that we can say about

(1) how fast can each user access the information they are looking for
(2) will some information not be available?
(3) will some information not be available but them come back to being available one day ?
(4) 

. . 

Reverse indexing lets us point to resources that are responsible for 'this or that feature' . . except it is kind of interesting to think that reverse indexing can be specific . . 

. . 

We may want a 'reverse indexer' for 'network availability' and 'data store reliability' and 'computer performance speed indication' and 'computer availability network metrics' and even other algebras of interest like 'social reliability score like is this computer not misbehaving in the network?' 'is this computer behaving poorly in the network?' 'is this computer not always saying gratitude prayers and meditations to its neighbors?'

. . 

In case of a 'poor behaving computer' we may need to contact an authority figure . . that can resolve problems by things like (1) administering more social credit points (2) administering more computer network privileges like bandwidth usage and availability and (3) turning on and off various aspects of interest depending on how well the administrator serves the community network since they themselves could be on watch as being judged by variety other administrators or community members . . 

. . 

Item Changers can help us with (1) privilege abilities

Item Reverse Changer Indicator could help us with (2) notification alerts

Item Pinch Co-Relator Factors could help us organize each (3) data entry point in terms of relevance for a particular task of interest

. . 



*/


```


[Written @ 15:56]


[Written @ 15:14]

This type of program design pattern is called a 'multiplexer' or 'mux' as some users on the internet would short-circuit the term -_- to make it 'x-or' friendly since 'xor' pronounced 'Ex Or' is short for 'Exclusive Or' which means something like 'only Or and not the other type of Or which would allow you to support either one of the items both being on at the same time' . . 'hello' 'world' . . Xor or XOR or Exclusive Or would mean . . you can only have 'One or the other' and 'not both' . . and so . . 'hello' satisfies 'XOR' . . and 'world' satisfies 'XOR' but 'hello' 'world' does not satisfy 'XOR' since it is Exclusive . . or exclusively for one or the other and not both simultaneously . . 

OR would suppose . . either . . and so . . 'hello' 'world' is satisfied by 'OR' . . and so the distinction between 'OR' and 'XOR' is in the 'exclusivety' . . or something like that . . 

. . 

XOR . . MUX . . MUX is short for 'Multiplexer' and maybe in some circles you will read 'Multiplexor' I'm not sure -_- I don't really know about this latter case . . or later case however you spell that word . . 

. . 

-_- . . later . . latter . . 

. . 

-_- . . 

. . 

Multiplexer . . uh . . well . . okay . . so we can use . . 'pamper' as a multiplexer which is really cool . . 

. . 

It means we can have one input with several output types . . 

. . 

This is particularly useful for things like if you have one or more database service providers and you're not necessarily sure on which one you would like to use . . and so you can 'choose all of them' or 'choose a subset of a few of them' and so that is a useful way to say . . you didn't really have to make a choice . . since you are just wanting the abilities that a 'database' would provide you and not necessarily caring on what the names or the service descriptions of the databases are -_- which is useful for saying that if you use 2 or 3 or more different databases . . then . . you can just have one interface to all of those databases . . and so that's really cool . . 

. . 

'databases' 'computer program managers' (like cloud service providers) . . 'asymmetric cryptography providers' 'hash function providers' . . 'artificial intelligence technologies'

. . 

Pamper is a cool multiplexor library . . but it can do other things as well . . One thing about trying to figure out what pamper is capable of . . is . . uh . . something like 'writing out a list of commands like the example we have done below which would help us see how we want the end product user to react when they are using our project library' 'how would we want the user to react? isn't that an interesting question . . it's like we are acting like . . those one people that would possibly be impressed with our computer program . . like we are trying to flat map onto their expectation tree of how to communicate with programs' or something like that . . 

`Item Changer` is a relatively new innovation in my book . . I haven't really implemented this type of thing before . . It was inspired by teachings from 'Seth' and 'The Pleiadians' who are communicating with me through sub-language like 'silent sounds that I can pick up and detect and listen into and there is also loud vocal sounds for when I don't find their words annoying enough to want to tune into and so then I will vocalize what they are saying by letting my mouth and lips and vocal chords move and tone to the expression that is coming in' or something like that . .

`Item Changer` from my own personal perspective very much reminds me of 'Event Listener' which is like a personal revalation that I made many months ago during the beginning ages of this project last year in 2020 A.D. or circa that time -_- . . well 'Event Listener' . . was -_- really well . . the library interface for that type of thing in the web browser normally goes something like 'addEventListener(itemId, eventType, eventResponseFunction)' and 'eventResponseFunction' is what is sometimes known as a 'callback function' or a 'function' that you 'can expect to be evaluated or called or called back for a later time during the execution of the evaluating function that is listening to your codebase call 'addEventListener'' or something like that . . and so for example if you are using a web browser you can possibly add an event to listen to mouse-click events . . from the user of the computer . . or keyboard-press events . . from the keyboard of the user . . or something like that . . and so that could look something like . . addEventListener('onclick', function (event) { console.log('a click happened', event) }) . . and so that is a way of 'detecting a click event and responding to it as a computer programmer' . . or as a 'computer program' . . 

'`Item Changer`' is possibly an 'inverse character' for 'Event Listener' . . 'If something is wanting to be done by the user . . they can express that they are the only ones with the permission to do that thing . . and so they are the only ones with the real codebase . . secret or private keys to do that thing . . but at the same time . . maybe . . they have an assistant . . and that assistant . . whether it's another computer program or another person on another computer . . they need access to that 'secret key' or that 'private key' . . well . . instead of providing them a 'secret key' or 'private key' . . you can provide them with a 'item changer' 'item changer' is specific to not show the 'secret key' or 'private key' to the individual but . . the function can perform things of interest . . uh . . and so if your friend receives that function then they only need to call it . . and then of course in the function if you need to say that that function cannot be called more than 10 times or cannot be called after midnight or . . maybe there are some other arbitrary constraints . . to why this function cannot operate . . during specific ages of its existence . . then . . you are able to program that into the function callback or the function argument . . or the function . . callback item . . of the 'item changer' . . and so the 'item changer' is uh . . like a function . . but it has default permission rules that won't need to be traded with another user . . but they are assumed to be . . implicit in the function . . 

. . 



. . 

(1) Computer #1
```js

const pamper = new Pamper()

const itemIndex1 = pamper.addItem('https://database.com/store-1/')
const itemIndex2 = pamper.addItem('https://database.com/store-2/')
const itemIndex3 = pamper.addItem('https://database.com/store-3/')

// itemIndex1: add item changer for 'update-item' type
pamper.addItemChanger(itemIndex1, 'update-item', function (itemId: string, itemUpdateValue: any) {
  const createdItem = database.createItem('store-1', itemId, itemUpdateValue)
  console.log('store-1 item changer called!', itemId)
  return createdItem
})

// itemIndex2: add item changer for 'update-item' type
pamper.addItemChanger(itemIndex2, 'update-item', function (itemId: string, itemUpdateValue: any) {
  const createdItem = database.createItem('store-2', itemId, itemUpdateValue)
  console.log('store-2 item changer called!', itemId)
  return createdItem
})

// itemIndex3: add item changer for 'update-item' type
pamper.addItemChanger(itemIndex3, 'update-item', function (itemId: string, itemUpdateValue: any) {
  const createdItem = database.createItem('store-3', itemId, itemUpdateValue)
  console.log('store-3 item changer called!', itemId)
  return createdItem
})

// Call #1: create item changer event for 'update-item'
pamper.changeItem('update-item', 'item-id-1', 'hello-world')

// Call #2: create item changer event for 'update-item'
pamper.changeItem('update-item', 'item-id-2', 'hello-world')

/*

Result Description: all 3 add item changers will be evaluated for this particular item changer type called 'create-item' . . since the change item algorithm is evaluated 2 times . . 6 evaluations will result in total . . 

Result:

store-1 item changer called! item-id-1
store-2 item changer called! item-id-1
store-3 item changer called! item-id-1

store-1 item changer called! item-id-2
store-2 item changer called! item-id-2
store-3 item changer called! item-id-2

*/

```

(2) Computer #2
```js

const pamper = new Pamper()

pamper.addItem('https://database.com/store-1/')
// remove store-2
pamper.addItem('https://database.com/store-3/')
pamper.addItem('https://database.com/store-4/') // add store-4



```


[Written @ 12:57]


