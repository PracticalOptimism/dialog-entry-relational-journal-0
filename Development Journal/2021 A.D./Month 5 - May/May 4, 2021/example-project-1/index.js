





stripe.createToken(cardElement).then(function(result) {
  // Handle result.error or result.token
});

stripe.createSource(ibanElement, {
  type: 'sepa_debit' // ,
  // currency: 'eur',
  // owner: {
  //   name: 'Jenny Rosen',
  // },
})
.then(function(result) {
  // Handle result.error or result.source
});


/*

(1) Create Source // 
(2) Create Charge // https://stripe.com/docs/api/charges

*/

/*

curl https://api.stripe.com/v1/charges \
  -u sk_test_4eC39HqLyjWDarjtT1zdp7dc: \
  -d amount=2000 \
  -d currency=usd \
  -d source=tok_amex \
  -d description="My First Test Charge (created for API docs)"

*/