

# Development Journal Entry - May 3, 2021

[Written @ 12:55]

```bash
# Initialize project
light initialize-project 'account-name' 'project-name' 'usecase-1' 'usecase-2' 'usecase-3'

# Questions:
- Do you need service accounts? yes, no
- Project Website Type:
  - (1) Interactive Game / Interactive Graph
  - (2) Informational Service / Service Application
  - (3) Realworld Physical Robotics
  - (4) Realworld Actuated Devices

# Here is your Administrator Account Temporary Password
Hello-World-123

# Instructions:
# (1) 📐 Beginner: Update the `precompiled/@service/constants` with new default settings
# (2) 📚 Advanced: Update the `usecase data structures`
# (3) 📚 Advanced: Update the `usecase algorithms`
# (4) 📘 Intermediate: Import the `provider services` into your files of interest

# Reminder: Learn more about how 'the-light-architecture' works at https://gitlab.com/ecorp-org/the-light-architecture

```

```bash

light start-development-environment

# Development Environment Started!

# Project Id:                             project-id
# Project Name:                           Project Id
# Project Version Id:                     1.0.0
# Project Minimum Description:            No minimum description provided yet.
# Project Introduction Video:             https://website.com/introduction-video.mp4
# Project Introduction Website:           https://website.com/about

# Administrator Temporary Password:       Hello-World-123

# 14 of 14 Items of Interest:

# Service Providers

# (1) Website Address:                        https://website.com/
# (2) Localhost Website Address:              https://localhost:8080
# (3) Project Static Asset Location (images, videos, etc.):                                             https://website.com/project-codebase
# (4) Project API Reference:                  https://gitlab.com/account-id/project-id#table-of-contents
# (5) Project Codebase Dialogue / Code Review: https://gitlab.com/account-id/project-id/dialogue/usecase/ideas-and-inspiration/README.md

# JavaScript Import Using NPM

# (5) NPM Package Install:                    npm install --save 'project-id'
# (6) JavaScript Import:                      import * as projectId from 'project-id'

# HTML Import

# (7) HTML Script Import:                     <script src="https://cdnjs.com/package/project-id">

# JavaScript Import Without NPM

# (8) OICP Import:                            const projectId = await oicp.importComputerProgram('https://cdnjs.com/package/project-id')

# (9) JavaScript-HTTP-Eval Import:            const projectId = await fetch('https://website.com/project-codebase/compiled/usecase/service/javascript-library/constants/project-id/item.js').then(response => { const projectIdAndVersionId = '{project-id}-{version-id}' let globalObject = (global || window || self || this); if (globalObject.javaScriptHttpEvalImportTable[projectIdAndVersionId]) { return globalObject.javaScriptHttpEvalImportTable[projectIdAndVersionId] } let projectEvaluatedResponse = eval(response.text()); globalObject.javaScriptHttpEvalImportTable[projectIdAndVersionId] = projectEvaluatedResponse; return projectEvaluatedResponse })

# JavaScript Inline-HTTP API Reference

# (10) HTTP Function Call:                     fetch('https://website.com/project-http-server/evaluate-function/<propertyId1>/<propertyId2>?inputItem1=hello&inputItem2=hello&inputItem3=hello&currentEnvironmentId=environmentId')

# (11) Reset Environment Identity:              fetch('https://website.com/project-http-server/delete-current-environment/<propertyId1>/<propertyId2>?inputItem1=hello&inputItem2=hello&inputItem3=hello&currentEnvironmentId=environmentId')

# 3rd Party Service Providers

# (12) Gitlab Project Location:                 https://gitlab.com/account-id/project-id
# (13) Github Project Location:                 https://github.com/account-id/project-id
# (14) NPM Project Location:                    https://npmjs.com/package/account-id/project-id
# (15) Google Chrome Extension:                 https://googlechromeextensions.com/project-id
# (16) Firefox Extension:                       https://firefox-extensions.com/project-id
# (17) VSCode Extension:                        https://vscode-extension.com/project-id
# (18) Desktop Application (MacOS, Linux):      https://website.com/project-codebase/compiled/usecase/desktop-application/constants/mac-os-and-linux-operating-system/project-id.app
# (19) Desktop Application (Windows):           https://website.com/project-codebase/compiled/usecase/desktop-application/constants/windows-operating-system/project-id.exe
# (20) Android Application:                     https://googleplaystore.com/project-id
# (21) iOS Application:                         https://apple.com/project-id


# Reminder: Visit /precompiled/@service/constants to update project settings

```

```bash
light start-experimental-release-environment

```

```bash
light help
```

```bash
light sign-in 'service-id'
```

```bash
light get-consumer 'consumer-id'
```

```bash
light get-compiler 'compiler-id'
```

```bash
light get-provider 'provider-id' --get-codebase
```




// In case you need to (1) delete old items after (2) receiving an update from the project settings of a project . . 

/dialogue
  - /provider
    - /service
      - /the-light-architecture
        - /constants
          - /project
            - /project-id-{date-created}
              - /precompiled
                - /@service
                  - /constants
                    - /project-settings // keep a copy for update usecases

    - /service-group
    - /service-type




[Written @ 2:39]

Alright . . well . . 

[Written @ 2:22]

The Useful thing about this protocol . . "Website Identity System" . . noted below this comment . . 

(1) Have websites publish metadata properties with unique identity information across the whole internet <br>
(2) Metadata properties can be scrapped or retrieved or received or gotten or requested for at runtime of any 'http' reading program <br>

. . 

http domain locations are a useful source for information . . 

websites can be file systems for other websites . . and can provide metadata resources for websites to read from and request write permissions to . .

. . 

A standard metadata protocol I suppose is the aim of that idea that I had . . uh . . well . . uh . . those seem to exist already such as 'open graph' or 'open graph protocol' . . well that was a mistake . . I thought I was trying to develop uh . . something original or something different -_- . . well . . -_- research I guess -_- . . 

Well the hashcode of the website address is still possibly a useful item to have a metadata property of : / . -_- well I didn't know whether I uh . . -_- well the idea was to have uh . . metadata verification of the website address xD . . and so possibly you don't even need the hashcode since you can just have a metadata property that reads

```html`
<meta data-id="website-id" data-value="website.com">
```

. . 

To verify that the website id is correct . . (1) `read the website address` and (2) `read the website content` . . (2.1) `read the metadata with 'website-id' data id` . . (3) `check that they 'website-id' data value is equal to the 'website-address'` . . or something like that . . http or https agnostic could be okay . . 

```javascript
function getIsValidWebsiteId (websiteAddress: string) {
  const websiteAddressContent = oicp.networkProtocol.getMessage(websiteAddress, ['content'])
  const websiteId = oicp.networkProtocol.getMessage(websiteAddressContent, ['meta', 'data-id', 'website-id']).dataValue
  const isValidWebsiteId = (websiteId === websiteAddress)
  return isValidWebsiteId
}
```

'Website Ownership' is possibly the term for this . . and so uh . . right . . I need to think more about the usecase that I was thinking about originally . . I think I forgot . . I'll be back later to see if I remember it . . 

. . 

Website Identity System

- websiteAddress: '`https://website.com`'
- websiteAddressPathList: ['username', 'item', 'item-2'] // resolves to '`https://website.com/username/item/item-2`'
- websiteAddressPathListHashCode: '``'
- websiteAddressHashCode: `sha256://${websiteAddress}.websiteAddress`
- websiteId: '`${websiteAddressHashCode}`'

- websiteMetadata:
```html
<meta data-id="websiteId" data-value="${websiteId}" >
<meta data-id="websiteAddressHashCode" data-value="${websiteAddressHashCode}">
<meta data-id="websiteAddressPathListHashCode" data-value="${websiteAddressPathNameHashCode}">
```

To Verify That The Website Id is valid . . 

```typescript

function getIsValidWebsiteId (websiteId: string, websiteAddress: string, websiteAddressHashCode: string): boolean {

  // Check if the cryptographic hash code is valid
  const cryptographicHashCode = cryptographicHashFunction.getHashCode(`sha256://${websiteAddress}.websiteAddress`)

  const isValidCryptographicHash = websiteAddressHashCode === cryptographicHashCode

  // Check if the websiteId is valid
  const isValidWebsiteId = isValidCryptographicHash

  return isValidWebsiteId
}

```



[Written @ 1:48]

- get
  - getIsItemBeing<Idea Structure>
  - getIsItemDoing<Process Name>

. . 

- get
  - getIs<Idea Structure> // valid, received, sent
  - getIs<Process Name> // processing, updating

[Written @ 0:40]

Programs That Were Previously Being Thought About:

(1) Open Internet Communication Protocol (OICP) <br>
(2) Program Manager <br>
(3) Password Manager <br>
(4) Uniplexer <br>
(5) Hashgraph <br>
(6) Program Operator Variables <br>

. . 


(1) Open Internet Communication Protocol (OICP)
  - Publish Information On The Internet
  - Publish Computer Programs On The Internet
  - 1 API for JavaScript in both browser and node.js
(2) Program Manager
  - Operating System for JavaScript Programs
  - Install Programs at runtime for browser and node.js
(3) Password Manager
  - Cryptography library for computers
  - People don't manage passwords anymore, computers manage them
  - People receive non-administrator passwords (unless administrator is really super important otherwise, only simple permission rules -_-)
  - Computers share passwords without telling people -_-
(4) Uniplexer
  - Multiplexer library for 3rd party service providers
  - It's like variables for 3rd party service providers
  - Change the service provider at runtime
(5) Hashgraph
  - Distributed consensus based on the gossip protocol and virtual voting
  - Replacement for 'raft' 'paxos' and 'leader-based' consensus mechanisms
(6) Program Operator Variables
  - Programs Look Like they have operators like 'update' that variable
  - Operator variables let you 'look at the update' history for the 'variable'
  - 'variabilize your variables' 'get statistics on how your variables are operating' -_-
  - -_- possibly this lets you see that you can have variables that for-example are 'pass-by-reference' across different files -_- but -_- that -_- was -_- one of the original inspirations for this idea -_- . . 'pass-by-reference' in this case means 'have an object data structure in memory and have your variable be a property in that object' . . and so for example 'you can change the property in the object and that change will be reflected in other parts of the program for that same computer or something like that' whereas a normal or traditional variable that isn't a part of an object like let helloWorld = 'hello' is not necessarily passed by reference and so the value of the variable . . when updated . . will not necessarily be reflected . . in other parts of the program . . and so . . there is a . . 'consensus disagreement' or a 'lack of consensus' or a 'lack of agreement' -_- for . . -_- -_- what the updated value of the variable is -_- . . 

  . . 


. . 

(1) Open Internet Communication Protocol (OICP) <br>
  - `OICP` is still a project I'm working on
(2) Program Manager <br>
  - `OICP` is a 'computer program manager'
(3) Password Manager <br>
  - -_- -_- I'm not sure
  - This is possibly '`pamper`' + `oicp` + `cryptography`
(4) Uniplexer <br>
  - `OICP` will use '`pamper`' with the resources relevant here
(5) Hashgraph <br>
  - '`pamper`' could possibly replace this project for the time being
(6) Program Operator Variables <br>
  - '`pamper`' could possibly replace this project

. . 

I don't remember giving this project a name . . 

. . 

Website Identity System

- websiteAddress: '`https://website.com`'
- websiteIndexHTML: `<!DOCTYPE html><html><head><title>Hello World</title>${websiteMetadata}</head><body><h1>Hello World</h1></body></html>`
- websitePrivateKey: '`ecc25519://secret-private-key-123`'
- websitePublicKey: '`ecc25519://public-key-123`'
- websiteHashCode: `sha256://${websiteAddress}.websiteAddress/${websiteIndexHTML}.websiteIndexHTML`
- websiteId: '`ecc25519-signature://${websiteHashCode}`'

- websiteMetadata:
```html
<meta data-id="websiteId" data-value="${websiteId}" >
<meta data-id="websitePublicKey" data-value="${websitePublicKey}">
<meta data-id="websiteHashCode" data-value="${websiteHashCode}">
```

To Verify That The Website Id is valid . . 

```typescript

function getIsValidWebsiteId (websiteId: string, websitePublicKey: string, websiteAddress: string, websiteIndexHTML: string, websiteHashCode: string): boolean {

  // Check if the cryptographic signature is valid
  let cryptographicSignature = websiteId
  const isValidCryptographicSignature = cryptography.isValid(cryptographicSignature, websitePublicKey)

  // Check if the cryptographic hash code is valid
  const cryptographicHashCode = cryptographicHashFunction.getHashCode(`sha256://${websiteAddress}.websiteAddress/${websiteIndexHTML}.websiteIndexHTML`)

  const isValidCryptographicHash = websiteHashCode === cryptographicHashCode

  // Check if the websiteId is valid
  const isValidWebsiteId = isValidCryptographicSignature && isValidCryptographicHash

  return isValidWebsiteId
}

```

. . 

So long as . . the secret private key is kept a 'secret' and so for example . . the key is not given to non-authorized users or something like that . . uh . . then . . uh . . this could possibly be a way to uh . . register your website . . to have a valid website id . . uh . . uh . . by (1) adding metadata properties to your website . . that follow those specified rules . . 

. . 

Well one of my inspirations for this method of 'website-authentication' was to say something like . . each website address can possibly have an 'administrator' uh . . resource or 'administrator' public-private-key resource that is like 'your website is unique' and so 'other websites won't necessarily or won't probabilistically have the same -_- websiteId' -_- I'm not really sure how well the program would work in practice but it's possible to say that because each website has a unique public website id that can be 'scrapped' or 'retrieved' from the website . . at runtime of your program . . uh . . that's possibly a useful uh . . thing to then say . . uh . . users that use that website . . uh . . have the possibility to also have a unique uh . . website related id . . and so it's like a stype of . . file system identity management system for websites . . that is like 'permanent' and 'static' or something like that and so even if your website changes or something like that . . it's possibly guaranteed to be -_- uh -_- something -_- I don't know xD . . also changes aren't necessarily the best idea -_- so -_- -_- -_- uh . . since you want the 'permanence' 'hash codes' possibly make things -_- weird since uh you're possibly updating your website index.html a lot and so your hash code changes and so the website id changes . . and so possibly you want to take the hashcode for a sub-set of the index.html which is expected to be uh . . static and the same throughout all the change cycles . . and so . . uh . . I guess that can be like a random -_- something I don't know . .

. . 

proportionality metrics are like . . hmm . . well your website is unique but it keeps changing . . do you really want a unique id that doesn't change as well ? . . that's really weird and possibly difficult since -_- . . I don't know -_- what kind of changes are you making?? . . don't you need your id to maintain an identity that is proportional to your data structure? -_- But that's uh -_- . . yea . . if it's not proportional to your data structure then anyone with a website can 'copy-paste' your website id . . and say . . 'yea, that's me as well' . . -_- so I guess a hashcode of your website address is possibly -_- the only proportion you need ? . . xD . . -_- . . since for example . . -_- . . -_- . . 

[More thoughts in notes after this timeline area . . for 0:40]


Notes @ Newly Created Words

proportina
[Written @ May 3, 2021 @ 2:14]
[I accidentally typed the word 'proportina' instead of 'proportional' . . I'm sorry . . I'm not sure what this word 'proportina' should mean at this time . . ]


. . 

Authentic, Registered, Valid . . RegisteredWebsiteId . . ValidWebsiteId . . ExpectedWebsiteId . . ExpectedDefinedWebsiteId . . 

I'm not really sure . . 

createServiceAccountSession <br>
createServiceAccountRegistration <br>
createServiceAccountRegistrationToken <br>

. . 

registration

created

. . 

approved

. . 

getIsItemBeingApprovedWebsiteId

. . 

Approved

. . 

Completed . . Approved . . Applied . . Resourceful . . Correct . . 

. . 

getIsItemBeingCorrectWebsiteId

. . 

Correct . . Incorrect . . Correct That Mistake . . Update That Mistake . . Create a Correction . . Create a Connection?? . . Correct . . Correct . .

Allowed . . Allowance . . Award . . Awarded . . Award . . 

. . 

incorrect <br>
beingCorrected <br>
correct <br>
impossibleCorrection <br>

. . 

correct <br>
correction <br>
corrected <br>

. . 

valid <br>
processOfBeingValidApproved <br>
validated <br>

getIsItemBeingCorrected <br>
getIsItemBeingApproved <br>
getIsItemBeingValidated <br>
getIsItemBeingApprovedForValidation <br>
getIsItemBeingApprovedForCorrection <br>

. . 

valid looks interesting . . 'correct' looks -_- weird kind of -_- xD . . 

. . 

-_- valid . . sounds okay . . I'm not sure why . .

. . 



