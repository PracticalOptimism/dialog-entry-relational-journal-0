

# Development Journal Entry - May 13, 2021

[Written @ 13:39]




[Written @ 10:39]

'`service-for-item`' <br>
'`store-for-item`' <br>
'`program-for-item`' <br>
'`creator-for-item`' <br>
'`dialog-for-item`' <br>

. . 

/update-item
- /update-item
- /update-item-by-property-id
  - /add-item-to-property-id
  - /remove-item-from-property-id
  - /update-item-from-property-id
- /update-item-by-property-id
  - /add-item-to-property-id
  - /remove-item-from-property-id
  - /update-item-from-property-id
- /update-item-by-property-id
  - item.ts

. . 

/service-for-typescript
  - /update-item
  - /update-item-by-property-id

/service-for-rust
  - /

/service-for-item
/service-for-item-2
/service-for-item-3
/service-for-item-4

. . 

/service-for-programming-language
  - /programming-language-type-1-typescript
  - /programming-language-type-2-rust
/service-for-programming-language-environment
  - /programming-language-environment-type-1-web-browser
    - /web-browser-language-type-1-typescript-without-nodejs-features
    - /web-browser-language-type-2-javascript-without-nodejs-features
    - /web-browser-language-type-3-webassembly-without-nodejs-features
  - /programming-language-environment-type-2-nodejs
    - /nodejs-language-type-1-typescript-without-web-browser-features
    - /nodejs-language-type-2-javascript-without-web-browser-features
  - /programming-language-environment-type-3-deno
    - /deno-language-type-1-typescript-without-web-browser-features
    - /deno-language-type-2-typescript-without-nodejs-features

. . 

/update-item
  - /service-for-item
    - /item-type-1-typescript-implementation
      - /typescript-implementation
        - typescript-implementation-1.ts
        - /program-for-permission-rule-reverse-index-verification
          - /permission-rule-reverse-index-verification-type-1-expectation-definition
            - expectation-definition-1.ts
          - /permission-rule-reverse-index-verification-type-2-formal-method
            - formal-method-1.ts
          - /permission-rule-reverse-index-verification-type-3-idempotency-claim-verification
            - idempotency-claim-verification-1.ts
      - /
  - /service-for-item-property-id
    - /item-property-id-type-1-item-property-id-name
    - /item-property-id-type-2-item-property-id-name
    - /item-property-id-type-3-item-property-id-name
    - /item-property-id-type-4-item-property-id-name

. . 



Expectation Definitions . . Unit Tests . . User Interface Test Verification . . Formal Methods . . 

Things that involve 'make sure that item is there like we have supposed that item should be there' . . can possibly be called

'reverse index verification' . . 'reverse index' is a term talking about . . 'would that item be referenced by another item?' . . 

'reverse index verification' allows programs to 'ensure that any item that would refer to this item of interest' 'is actually being present' . . and so 'verification' helps us suppose that 'that item is there as we would like to it be there' or 'we are verifying that the item of interest exists' 'the item of interest in this case is that item that is the so called 'reverse index'' . . 'the reverse index will map to our particular index of interest in some way' . . 

For example . . say you have a list of magazines that you are reading . . 

Magazine #1 . . Magazine #2 . . Magazine #3 . . 

These comic book magazines may have different 'genres' of information . . and so you may be interested in asked very strange questions like 'who else would like this magazine?' . . This question is strange because now you need to do a 'reverse index' search for individuals or ideas or patents of concepts that would otherwise possibly not have an interest in that item of interest . . and instead you are hypothesizing that they could possibly have an interest . . and to verify your hypothesis you may need to ask those individuals yourself but verifying uh . . well . . that's even a difficult science . . verifying could be as good as 'yea I know them well enough . . so possibly they probably would agree with what I've written here' . . and this could possibly be akin to the type of verification that we would be interested in in software development with 'ahh yes, I do believe this file is in the file system and so possibly any person who comes by and reads the screen of my computer or maybe on their computer as well then possibly the file should be on their computer as well . . and so I have verified that this file exists on the possibility of my hypothesis . . idea . . ' . . 

Well . . a reverse index could possibly look like

. . 

Person #1 likes Magazine #1 but doesn't like Magazine #2

Person #2 likes Magazine #3 but doesn't like Magazine #1

Person #3 likes Magazine #1 and doesn't have opinions on Magazine #2 and Magazine #3

. . 

This reverse index gives you the clue that 'well possibly this is true for these individuals' but it's not necessarily true . . and so anyway . . you move on from there . . and if you really wanted to be sure of whether this is true or not . . you need to do a reverse index on the reverse index that you've collected . . and so for example . . 

Person A verifies that Person #1 really does like that reverse index gathering

Person B verifies that Person #2 does not agree that they don't like Magain #1

Person C verifies that Person #3 is agnostic to Magazine #2 and Magazine #3 during the time period of XYZ A.D. and XYZ + 10 years A.D.

. . 

If you want to verify that those verifications from Person A and Person B and Person C are valid . . then you need to do another reverse index . . with further other criteria and variables of interest . . more and more authority figures or technical devices can be sampled and sub-sampled at various rates and that could give you further criteria on whether or not your ideas are still ideas that you're trying to ideate as ideas relating to ideas . . or something like that . . 

. . 

Back to our previous example . . we have created the reverse index involving Person #1 . . Person #2 . . Person #3 . . these are very useful . . for things like . . well . . possibly we know that we don't need to make Person #1 angry . . or mad or upset . . by showing them Magazine #2 . . or by recommending them Magazine #2 . . 

. . 

Well . . again our reverse index may be not correct and so we may need to reverse index to collect further sub-samples of interest . . like words from the person that says 'Yes, it is true, I do not like that' . . or 'Yes, it is true, I do not like that in the time period of 10 years ago until now in 2021 and possibly I won't like it in 10 years from now . . '

. . 

Well . . whatever happens . . it's still useful . . to have reverse index listings . . they are quite assumptive but those assumptions can be useful for social cohesion around biased ideas like '`lets do that`' and '`lets do this`' and '`lets agree on that`' and '`lets agree on this`' . . 

. . 

There are so many types of reverse indices or reverse index metrics of interest . . you don't have to worry about not knowing one because probably that's already the case that you can't reverse index something because you may not have the imaginative attitude to do so . . and then when you do . . you can write it down and say that it's a reverse index parameter . . 

. . 

'make sure blue cows would agree with this file type'

'make sure people that cannot see well can read this file'

'make sure people that walk on 2 feet are able to walk in this direction'

. . 

-_- . . 

. . 

These are possible ideas we may have . . and so our reverse index would allow us to say something like . . 'yea, that would be the case . . if I agree that I see enough individuals doing that or participating in that' . . 

. . 




