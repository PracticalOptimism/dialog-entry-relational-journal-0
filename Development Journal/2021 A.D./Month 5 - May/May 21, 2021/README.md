

# Development Journal Entry - May 21, 2021

[Written @ 23:21]




[Written @ 22:52]

Perospective waiting

. . 

Waiting for an answer from the future . . 

. . 

Retrospective waiting

. . 

Waiting for an answer from the past . . 

. . 

The future will come back and respond to this area of text that reads 'perospective waiting'

. . 

Like an author's note . . 'this will be updated with new information' . . 

. . 

. . . . 

. . . . 

Waiting for an answer from the past . . 

Waiting for an answer from the poet . . 

. . . . 

'the past' is possibly thoughtful to say it is a 'poetic' 'device' since possibly interpretation and misinterpretation play a role in what is being seen and not seen about the past . . 

. . . 

. . . 




[Written @ 22:49]

<!-- There could be corrections made here . . at the beginning of the list -->

example-type-1.?-something

<!-- There could be corrections made here -->

example-type-2.?-something-else

<!-- There could be corrections made here -->

example-type-3-something-else-2

<!-- No correction necessary at the end of the list right? . . just keep adding to the list -->

. . 


[Written @ 22:47]

example-type-1-something

example-type-2.1-something-else

example-type-2.2-something-else-2

example-type-2.3-something-else-3

example-type-2.4-something-else-4

example-type-3.1.1-something-else-5

example-type-3.1.2-something-else-6

example-type-3.2-something-else-7

example-type-4-something-else-8


. . 

Since we are using 'decimal' points . . we can possibly replace our initial first ideas on what . . uh . . the uh . . number series should look like . . 

. . 

the Decimal point is possibly 'distinguishing' enough . . and so why use another number system ? . . right ? . . 

. . 




[Written @ 22:31]

You may make a mistake . . but even your mistakes could have been mistakes . . so you need to fix your mistakes . . with more types . . 

. . right?

. . 

example-type-1-something

example-type-2-something-else

example-type-3-something-else-5

example-type-4-something-else-8

. . 

Assume we are starting with a list of types like the one listed above . . 

. . 

Okay . . 

. . 

example-type-1-something

example-type-2-something-else

example-type-3-a-something-else-5

example-type-3-b-something-else-7

example-type-4-something-else-8

. . 

Here . . is a possible . . correction . . listed above . . 

. . 

. . 

example-type-1-something

example-type-2-something-else

example-type-3-a-something-else-5

example-type-3-b-something-else-7

example-type-4-something-else-8

. . 

But because we could have made a mistake . . that we didn't know about . . except in 'hind-sight' or . . 'looking back' and 'seeing that oh . . we could have made a mistake here . . ' . . well . . that can be updated . . 

. . 

But you don't necessarily want to update this . . by . . renaming . . 'b' to 'c' . . since the list of updates could be longer . . and you again don't want to rename a lot of things . . instead we are possibly opting to say 'just add those things that are 'fixes'' and it seems like the cost of doing that is also updating the one right before the 'fixes' are added with a . . 'initial' 'first' . . 'index' 'number' to 'commemorate' or 'resignate' or 'symbolize' that it is 'the first part' or 'where the mistake was first corrected' or 'inserted' . . 

. . 

example-type-1-something

example-type-2-something-else

example-type-3-a-1-something-else-5

example-type-3-a-2-something-else-6

example-type-3-b-something-else-7

example-type-4-something-else-8

. . 

Upon discovering our . . 2nd mistake . . we need to use the same method since . . uh . . by now it may not be a good time to say 'change 'b' to 'c'' . . because you have to imagine the possibility that in another type of project where we realize that we didn't completely fix the listing . . then . . this is possibly . . a possible new solution to help us . . by saying we can 'continue' this 'concatenation' series . . 

. . 

Okay . . I'm not exactly sure if this naming style is really cool or really useful for 'updating' each of these names . . but certainly the idea is that possibly this listing style could be confusing . . although . . from my perspective it's possibly not all that confusing after getting over the fact that we made some mistakes . . and tried to fix them . . 

. . 

Alright . . well . . going back and forth between two types of number systems . . separated by 'hyphenations' or '-' . . is possibly weird to look at since in general a 'hyphen' possibly is meant like a 'space' character which doesn't really 'symbolize' anything or isn't possibly meant to 'symbolize' anything . . but so we might even consider using a new sort of symbol between each of our so-called 'item versions' uh . . or 'item versionations' or 'something like that' . . 

. . 

example-type-1-something

example-type-2-something-else

example-type-3.a.1-something-else-5

example-type-3.a.2-something-else-6

example-type-3.b-something-else-7

example-type-4-something-else-8

. . 

. . . 

. . 



example-type-1-something

example-type-2.a-something-else

example-type-2.b-something-else-2

example-type-2.c-something-else-3

example-type-2.d-something-else-4

example-type-3.a.1-something-else-5

example-type-3.a.2-something-else-6

example-type-3.b-something-else-7

example-type-4-something-else-8

. . 

This could be a possible correction of our . . type listing . . which respects existing types . . 

. . 

So we don't have to 'rename' each type to match the original 'number' mapping . . 

. . 





[Written @ 21:55]

1, a, 1, a, 1, a, 1, a, 1, a . . this correctional pattern can be used to 'correct' a 'typing' mistake . . for when . . a 'type' needs to be added 'between' 'two' types . . 

. . 

If you make a typing mistake . . and list one type before another . . 

That can look like this . . 


example-type-1-something

example-type-2-something-else

example-type-3-something-else-5

. . 

And if your mistake was that you forgot to add 'something-else-2' 'something-else-3' and 'something-else-4' before 'something-else-5' then . . You can . . correct your mistake . . by having . . a list . . 

. . 

example-type-2-b-something-else-2

example-type-2-c-something-else-3

example-type-2-d-something-else-4

. . 

These 3 items can be added after 'example-type-2-something-else' . . but also . . 'example-type-2-something-else' should be renamed to 'example-type-2-a-something-else' . . 

. . 

The final list will look like this . . 

example-type-1-something

example-type-2-a-something-else // update name to add 'a'

example-type-2-b-something-else-2 // add

example-type-2-c-something-else-3 // add

example-type-2-d-something-else-4 // add

example-type-3-something-else-5

. . 

This listing strategy helps you avoid 'possibly' updating the names for all the other types just to 'insert' a 'type' between another type . . 

. . 

By using another 'number' coordinate system . . you can avoid . . possible confusion . . which could arise when you use the same number system repeatedly . . 

A, B, C, D . . etc . . is a type of number system . . 

I, II, III, IV, V . . etc . . is a type of number system . . 

1, 2, 3, 4, 5 . . etc . . is a type of number system . . 

Alpha, Beta, Gamma, Delta, Epsilon . . etc . . is a type of number system . . 

If you have at least 2 number systems . . you can 'recycle' them in this . . type of listing method . . so that you can alternate between those 2 number systems . . to promote readability . . and avoid possible confusion . . 

. . 

A, B, C, D, . . is possibly not a 'super strong' number system like . . uh -_- . . the other number system . . 1 , 2 , 3 , 4 , 5 . . but it really possibly depends on what you mean by 'super strong' . . my own personal initial intuition belongs to the group of ideas that it's kind of like -_- lameeee . . I have to think about -_- lamee . . 

Anyway -_- . . 

-_- . . 

It's possibly not that big of a deal . . 

a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z . . 

But then how do you continue this number series ? . . 

. . 

aa, ab, ac, ad, ae, af, ag, ah, ai, aj, ak, al, am, an, ao, ap, aq, ar, as, at, au, av, aw, ax, ay, az . . 

. . 

ba, bb, bc, bd, be, bf, bg, bh, bi, bj, bk, bl, bm, bn, bo, bp, bq, br, bs, bt, bu, bv, bw, bx, by, bz . . 

. . 

etc . . 

za, zb, zc, zd, ze, zf, zg, zh, zi, zj, zk, zl, zm, zn, zo, zp, zq, zr, zs, zt, zu, zv, zw, zx, zy, zz . . 

. . 

aaa, aab, aac, aad, aae, aaf, aag, aah, aai, aaj, aak, aal, aam, aan, aao, aap, aaq, aar, aas, aat, aau, aav, aaw, aax, aay, aaz . . 

. . 

etc . . 

And so on and so forth . . 

. . 

I'm sorry if I made mistakes in those listings . . 

. . 

Well . . 

. . 

Our number system here isn't so bad . . it allows us to possibly have . . an arbitrarily sized number series . . a so called 'infinite' possibility . . series set . . right ? . . 

. . 

well . . 

. . 

awesome . . 

. . 

Okay . . 

Here is a possible alternative . . 

. . 

a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z . . 

. . 

z1a, z1b, z1c, z1d, z1e, z1f, z1g, z1h, z1i, z1j, z1k, z1l, z1m, z1n, z1o, z1p, z1q, z1r, z1s, z1t, z1u, z1v, z1w, z1x, z1y, z1z . . 

. . 

z2a, z2b, z2c, z2d, z2e, z2f, z2g, z2h, z2i, z2j, z2k, z2l, z2m, z2n, z2o, z2p, z2q, z2r, z2s, z2t, z2u, z2v, z2w, z2x, z2y, z2z . . 

. . 

z3a, z3b, z3c, z3d, z3e, z3f, z3g, z3h, z3i, z3j, z3k, z3l, z3m, z3n, z3o, z3p, z3q, z3r, z3s, z3t, z3u, z3v, z3w, z3x, z3y, z3z . . 

. . 

etc . . etc . . 

The overall pattern is possibly something like

. . 

z{number-of-times-cycled}{letter}

. . 

And if the number of times cycled . . is 0 . . then . . just have . . 

{letter}

. . 

Uh . . I don't know what name this type of symboled listing pattern is going by in the mathematics community . . but we can call it . . z-started list . . 

. . 

z-started list . . 

. . 

A `Z-Started List` . . starts with the 'end' character and keeps an algebraic numeral representation between the end character and the alphabet character that follows . . 

. . 

`z{number-of-times-cycled}{letter}`




