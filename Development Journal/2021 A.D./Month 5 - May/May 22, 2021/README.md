

# Development Journal Entry - May 22, 2021

[Written @ 22:19]




[Written @ 22:05]

Possibly this idea of . . having . . a version id . . that is like a series of numbers . . separated by a 'period' or a symbol of interest . . is possibly related to 

(1) in database technologies you don't necessarily want to archive or keep around the history of . . updates to the database . . and so for example any particular item . . could be updated . . in the database . . but for example . . maybe your database system uh . . possibly would . . be weary . . to hold on to all the . . transactions . . or all the . . update cycles . . or all the . . information for how the database was updated . . 

If you remove the history . . and flat map each item to a new listing . . possibly without the versioning id's . . then that's possibly an okay strategy to (1) ensure your data is still available and (2) try to have readable names . . 

. . 

I could possibly imagine a case in robotics . . 

. . 

where millions of these folders are created at runtime . . 

. . 

which means of course . . that the robotics software is possibly deleting . . and adding . . and updating . . a lot . . 

. . 

the delete and add cycle should possibly occur more frequently than the update cycle . . 

. . 

the remove and add cycle . . or delete and add cycle . . however you would like to call it . . 

. . 

the delete and add cycle . . is . . possibly . . uh . . something like . . (1) add this microcontroller sensor data (2) remove this outdated microcontroller sensor data and repeat this cycle . . for many times . . 

. . 

Because of the fact that new sensors could possibly be created at runtime . . for any particular robotics software application . . like for example . . 'identifying new limbs' by 'reverse engineering processes' . . like 'inverse kinematics' to 'solve new problems' . . well . . those are useful for example if you want to apply 'inverse kinematics' . . you don't necessarily know what those constraints are for the robot to traverse . . and so you have to say that it's okay to not know . . and if you have a goal . . you can say that some parameters need to be . . 'inversely' 'relatable' or 'inversely' 'discoverable' in weird ideas . . 

An example is something like . . 

Say you want to build a computer out of certain physical equipment in your garage . . 

You are awkward because the way you want to create the computer is (1) it needs to have an admirable structure like your own current understanding of computers . . with logic gates and things like that . . 

Well the logic gate idea can be awkward depending on how you define it . . for example . . you might be used to logic gates . . that are defined using silicon chips or something simple and familiar like metal alloys or something like that . . 

But when you want to say . . well you know what . . this pile of hot magma is my computer . . then . . wow . . this pile of hot magma is like . . 'well . . what really is in this stuff?' . . 'do you think there is a circuit that is equivalent to a logic gate in there?' . . 

Well . . inverse kinematics can be like . . play around with the magma by applying various forces of interest . . like electrical forces or acoustic forces . . or aerodynamic forces . . or water forces whatever type of force you're possibly interested in . . 

And for some reason . . you're trying to have the computer mix and match these ideas to result in a type of logic gate that would possibly perform the things that your own logic gate would do possibly in different terms that are possibly uh . . new to discover for the computer . . since for example . . it's possible that you ahve to do weird flips and flops . . that you possibly wouldn't have come up with manually without knowing how magma works or something like that . . 

It's kind of a weird example . . I'm sorry . . I'm not exactly sure if that example came across correctly . . the idea is that you don't necessarily know the environment where you want to apply familiar ideas . . 

Bananas are useful . . for example you could possibly use them as computers . . 

Build a computer from bananas . . 

Build a computer from ants walking around in the neighborhood . . 

. . 




[Written @ 20:25]

To add to list : `number.number.number...`

To remove from list : `number.number.number...x-removed-type`

To update list : `flat map to number` (no number.number.number... and no 'removed-type' syntax)

To add a replacement for a 'removed' type : `change the name from 'removed-type' to something else` (remove 'removed-type' and don't change the 'x' or change the number ? ? right ? . . )

. . 

1.1.1

1.1.2

1.1.x.1

1.1.x.2

1.1.x.3

. . 

- (1) update 10 items to the list (`completed`)
- (2) add 1 item after 3rd item (`completed`)
- (3) add 2 items after 5th item (`completed`)
- (4) add 3 items after 7th item (`completed`)
- (5) remove 2nd item after 3rd item (`completed`)
- (6) remove 3rd item after 3rd item (`completed`)
- (7) add 2 items after 4th item (`completed`)
- (8) add 1 item after 3rd item (`completed`)
- (9) remove 3 items after 3rd item (`completed`)
- (10) remove 2 items after 4th item (`completed`)
- (11) add 3 items after 2nd item (`completed`)
- (12) add 2 items after 3rd item (`completed`)
- (13) add 2 items after the 9th item (`completed`)
- (14) replace removed type at 12th position (`completed`)
- (15) add 1 item after the 13th item (`completed`)
- (16) remove 1 item after the 10th item (`completed`)
- (17) remove 2 items after the 11th item (`completed`)
- (18) add 1 item after the 10th item (`completed`)
- (19) add 1 item after the 13th item (`completed`)
- (20) add 1 item after the 15th item (`completed`)
- (21) remove 3 items after the 11th item (`completed`)
- (22) remove 1 item after the 12th item (`completed`)
- (23) add 3 items after the 13th item (`completed`)
- (24) update list (`completed`)

. . 

After step (1)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3-hello-world-3 <br>
service-for-item-type-4-hello-world-4 <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (2)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1-hello-world-3  <br>
service-for-item-type-3.2-hello-world-xyz <br>
service-for-item-type-4-hello-world-4 <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (3)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1-hello-world-3  <br>
service-for-item-type-3.2-hello-world-xyz <br>
service-for-item-type-4.1-hello-world-4 <br>
service-for-item-type-4.2-hello-world-xyz <br>
service-for-item-type-4.3-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (4)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1-hello-world-3  <br>
service-for-item-type-3.2-hello-world-xyz <br>
service-for-item-type-4.1-hello-world-4 <br>
service-for-item-type-4.2-hello-world-xyz <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (5)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1-hello-world-3  <br>
service-for-item-type-3.2-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2-hello-world-xyz <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (6)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1-hello-world-3  <br>
service-for-item-type-3.2-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (7)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1-hello-world-3  <br>
service-for-item-type-3.2.1-hello-world-xyz <br>
service-for-item-type-3.2.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (8)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2-hello-world-xyz <br>
service-for-item-type-3.2.1-hello-world-xyz <br>
service-for-item-type-3.2.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (9)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x-removed-type <br>
service-for-item-type-3.2.1.x-removed-type <br>
service-for-item-type-3.2.2.x-removed-type <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (10)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x-removed-type <br>
service-for-item-type-3.2.1.x-removed-type // already removed <br>
service-for-item-type-3.2.2.x-removed-type // already removed <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (11)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x-removed-type <br>
service-for-item-type-3.2.1.x-removed-type <br>
service-for-item-type-3.2.2.x-removed-type <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (12)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x-removed-type <br>
service-for-item-type-3.2.1.x-removed-type <br>
service-for-item-type-3.2.2.x-removed-type <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (13)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3-hello-world-xyz <br>
service-for-item-type-3.2.1.x-removed-type <br>
service-for-item-type-3.2.2.x-removed-type <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (14)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3-hello-world-xyz <br>
service-for-item-type-3.2.1.x-hello-world-xyz <br> // strange case ? (`x` or `x.1` ? . . )
service-for-item-type-3.2.2.x-removed-type <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (15)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3-hello-world-xyz <br>
service-for-item-type-3.2.1.x-hello-world-xyz <br>
service-for-item-type-3.2.2.x.1-removed-type <br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (16)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x-hello-world-xyz <br>
service-for-item-type-3.2.2.x.1-removed-type <br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (17)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x-removed-type // strange case?? (`x` or `x.x` ? . . )<br>
service-for-item-type-3.2.2.x.1-removed-type // already removed<br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (18)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2.1-hello-world-xyz // 10th - updated<br>
service-for-item-type-3.1.2.x.2.2-hello-world-xyz // 11th - added<br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x-removed-type <br>
service-for-item-type-3.2.2.x.1-removed-type <br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>


. . 

After step (19)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2.1-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x.1-removed-type // 13th - updated<br>
service-for-item-type-3.2.1.x.2-hello-world-xyz // 14th - added<br>
service-for-item-type-3.2.2.x.1-removed-type <br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (20)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2.1-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x.1-removed-type <br>
service-for-item-type-3.2.1.x.2-hello-world-xyz <br>
service-for-item-type-3.2.2.x.1.1-removed-type // 15th - updated<br>
service-for-item-type-3.2.2.x.1.2-hello-world-xyz // 16th - added<br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (21)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2.1-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2.1-hello-world-xyz // 11th - updated<br>
service-for-item-type-3.1.2.x.2.2.2-hello-world-xyz // 12th - added<br>
service-for-item-type-3.1.2.x.2.2.3-hello-world-xyz // 13th - added<br>
service-for-item-type-3.1.2.x.2.2.4-hello-world-xyz // 14th - added<br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x.1-removed-type <br>
service-for-item-type-3.2.1.x.2-hello-world-xyz <br>
service-for-item-type-3.2.2.x.1.1-removed-type <br>
service-for-item-type-3.2.2.x.1.2-hello-world-xyz <br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (22)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2.1-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2.1-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2.3.x-removed-type // 13th - removed<br>
service-for-item-type-3.1.2.x.2.2.4-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x.1-removed-type <br>
service-for-item-type-3.2.1.x.2-hello-world-xyz <br>
service-for-item-type-3.2.2.x.1.1-removed-type <br>
service-for-item-type-3.2.2.x.1.2-hello-world-xyz <br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (23)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2.1-hello-world-2 <br>
service-for-item-type-2.2.1-hello-world-xyz <br>
service-for-item-type-2.2.2-hello-world-xyz <br>
service-for-item-type-2.2.3-hello-world-xyz <br>
service-for-item-type-2.3-hello-world-xyz <br>
service-for-item-type-2.4-hello-world-xyz <br>
service-for-item-type-3.1.1-hello-world-3  <br>
service-for-item-type-3.1.2.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2.1-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2.1-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2.2-hello-world-xyz <br>
service-for-item-type-3.1.2.x.2.2.3.x.1-removed-type <br>
service-for-item-type-3.1.2.x.2.2.3.x.2-hello-world-xyz // 14th - added<br>
service-for-item-type-3.1.2.x.2.2.3.x.3-hello-world-xyz // 15th - added<br>
service-for-item-type-3.1.2.x.2.2.3.x.4-hello-world-xyz // 16th - added <br>
service-for-item-type-3.1.2.x.2.2.4-hello-world-xyz <br>
service-for-item-type-3.1.2.x.3.x-removed-type <br>
service-for-item-type-3.2.1.x.1-removed-type <br>
service-for-item-type-3.2.1.x.2-hello-world-xyz <br>
service-for-item-type-3.2.2.x.1.1-removed-type <br>
service-for-item-type-3.2.2.x.1.2-hello-world-xyz <br>
service-for-item-type-3.2.2.x.2-hello-world-xyz <br>
service-for-item-type-3.2.3-hello-world-xyz <br>
service-for-item-type-4.1.x-removed-type <br>
service-for-item-type-4.2.x-removed-type <br>
service-for-item-type-4.3.1-hello-world-xyz <br>
service-for-item-type-4.3.2-hello-world-xyz <br>
service-for-item-type-4.3.3-hello-world-xyz <br>
service-for-item-type-4.3.4-hello-world-xyz <br>
service-for-item-type-5-hello-world-5 <br>
service-for-item-type-6-hello-world-6 <br>
service-for-item-type-7-hello-world-7 <br>
service-for-item-type-8-hello-world-8 <br>
service-for-item-type-9-hello-world-9 <br>
service-for-item-type-10-hello-world-10 <br>

. . 

After step (24)

service-for-item-type-1-hello-world-1 <br>
service-for-item-type-2-hello-world-2 // updated <br> 
service-for-item-type-3-hello-world-xyz // updated <br>
service-for-item-type-4-hello-world-xyz // updated <br>
service-for-item-type-5-hello-world-xyz // updated <br>
service-for-item-type-6-hello-world-xyz // updated <br>
service-for-item-type-7-hello-world-xyz // updated <br>
service-for-item-type-8-hello-world-3  // updated <br>
service-for-item-type-9-hello-world-xyz // updated <br>
service-for-item-type-10-hello-world-xyz // updated <br>
service-for-item-type-11-hello-world-xyz // updated <br>
service-for-item-type-12-hello-world-xyz // updated <br>
service-for-item-type-13-hello-world-xyz // updated <br>
service-for-item-type-14-hello-world-xyz // updated <br>
service-for-item-type-15-hello-world-xyz // updated <br>
service-for-item-type-16-hello-world-xyz // updated <br>
service-for-item-type-17-hello-world-xyz // updated <br>
service-for-item-type-18-hello-world-xyz // updated <br>
service-for-item-type-19-hello-world-xyz // updated <br>
service-for-item-type-20-hello-world-xyz // updated <br>
service-for-item-type-23-hello-world-xyz // updated <br>
service-for-item-type-21-hello-world-xyz // updated <br>
service-for-item-type-22-hello-world-xyz // updated <br>
service-for-item-type-24-hello-world-5 // updated <br>
service-for-item-type-25-hello-world-6 // updated <br>
service-for-item-type-26-hello-world-7 // updated <br>
service-for-item-type-27-hello-world-8 // updated <br>
service-for-item-type-28-hello-world-9 // updated <br>
service-for-item-type-29-hello-world-10 // updated <br>

(Command + Option + Shift + Up Arrow) - Multicursor Select (Going Up) [1.0]

(Command + Option + Shift + Down Arrow) - Multicursor Select (Going Down) [1.0]


References:

[1.0]
Multi-Select in Visual Studio Code
By Ryan Carter
https://dev.to/rncrtr/multi-select-in-visual-studio-code-19k2


[Written @ 19:48]

In the case of an emergency . . 

And one group is concatenated with another . . 

Or one type . . is combined with another . . 

How do you correct . . the error of leaving those 'type' 'spaces' 'empty'?

For example . . 

hello-world-type-1-hello-world

hello-world-type-2-hello-world-2

hello-world-type-N-hello-world-3

hello-world-type-4-hello-world-4

hello-world-type-N-plus-1-hello-world-5

hello-world-type-N-plus-2-hello-world-6

hello-world-type-7-hello-world-7

. . 

. . 

And now a possible correction by joining

N, and N plus 1 and N plus 2 with . . the contents in type 1 for example . . 

. . 

We can see the following result . . 

. . 

hello-world-type-1-hello-world
- /hello-world-type-N-hello-world-3
- /hello-world-type-N-plus-1-hello-world-5
- /hello-world-type-N-plus-2-hello-world-6

hello-world-type-2-hello-world-2 // this number is 2 . . which follows 1 . . so everything is okay . . 

hello-world-type-4-hello-world-4 // this number is 4 . . which does not immediately follow 2 . . so everything is not okay . . we are expecting to see 3 . . here . . 

hello-world-type-7-hello-world-7 // this number is 7 . . which does not immediately follow 4 . . so everything is not okay . . we are expecting to see 5 . . here . . but actually since the previous one is not aligned and it is expected to be a 3 . . then . . this one should possibly be expected to be a 4 . . 

. . 

A possible correction for the 'incongrueties' . . here . . are possibly:

. . 

hello-world-type-1-hello-world

hello-world-type-2-hello-world-2

hello-world-type-3-hello-world-4 // this file name was changed . . from 'type-4' to 'type-3' . . 

hello-world-type-4-hello-world-7 // this file name was changed . . from 'type-7' to 'type-4' . . 

. . 

The numbers 2, 4, 7 that are shown in this example list of folder or file names . . are only examples and aren't necessarily important . . we have used numbers but we could have used . . chemical names . . and uh . . that would have also made it clear that those are particular types of items . . ordered by a particular uh . . typing system like chemical types but possibly we discovered that some chemicals really belong to another chemical type listing . . and so we could possibly say they belongs to type-1 or even type-2 or whichever type we discovered they belonged to later . . suggesting that we possibly made a mistake in our initial development of the file system . . 

. . 

Well . . as long as we have . . our 'type-1' 'type-2' 'type-3' suggesting the ordered list of types . . without any 'missing holes' or 'gaps' . . between numbers . . we can possibly be more confident that our 'type' 'system' is not bad . . 

. . 

But so . . 

. . 

If we discover that we made mistakes . . it's kind of weird that we have to update the names of the types that are now having the . . uh . . non-mistake . . uh . . result . . and so for example . . changing the name from 'type-4' to 'type-3' . . and from 'type-7' to 'type-4' could possibly not only have us do that for 2 . . folders or files . . but possibly if our list is long enough . . we could possibly have to change the names of multiple files . . or folders . . and so if that is millions of files or folders . . that is kind of laborious . . 

. . 

Now the question is . . do we really want to uh . . enable this feature to go forward . . or should we have some sort of 'recovery' method like the 'dot' uh . . 'matrix' listing . . idea from when we make mistakes by 'forgetting' to 'add' something . . 

. . 

1.1

1.2

1.3

. . 

1.1.1

1.1.2

1.1.3

. . 

1.1.1.1

1.1.1.2

1.1.1.3

. . 

etc . . 

1.2.1

1.2.2

1.2.3

. . 

10.1.1

10.1.2

10.1.3

. . 

5.5.1

5.5.2

5.5.3

. . 

5.4.2.3.4.5.1

5.4.2.3.4.5.2

5.4.2.3.4.5.3

. . 

etc.

Those are possible examples of 'fixing' 'forgetting' to 'add' . . to a list of items . . 

. . 

Which we talked about in the previous [Ecoin Episode #564](https://www.youtube.com/watch?v=tzeH7CUrlhw)

. . 


hello-world-type-1-hello-world

hello-world-type-2-hello-world-2

hello-world-type-3.x-removed-type

hello-world-type-4-hello-world-4

hello-world-type-5.x-removed-type

hello-world-type-6.x-removed-type

hello-world-type-7-hello-world-7

. . 

'x' could be an insignia that an item was 'removed' ?? . .

. . 

Although it's kind of confusing since 'x' could possibly be interpretted as an N-ary number assignment like a variable casing for other types of numbers like 3.1 or 3.2 or 3.3 . . and so on and so forth . . and 3.10 and 3.12 and 3.15 . . and so on and so forth . . 

. . 

It looks kind of interesting . . 

. . 

Having to see 'removed-type' . . 

. . 

It's possibly not the worst thing to happen in the world . . 

. . 





[Written @ 18:14]


