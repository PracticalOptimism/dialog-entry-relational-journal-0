
# Development Journal Entry - October 2, 2021



### [Written @ 13:02]

Development Checklist For Beginners

(1) `npm run install:packages`
- `npm install` for the root directory
- `npm install` for the project-service-website directory

(2) `npm run start:firebase:emulator`
- Start the firebase emulator before running expectation
  definition measurements

(3) `npm run start:http-server:provider-service-emulator`
- Start the service emulator for different provider services

(4) `npm run start:service-website:development`
- Start the development server for the service website

(5) `npm run measure:definitions`
- Run measurements on the "expectation definitions"
- "expectation definitions" is a new term for "unit testing"

(6)






### [Written @ 11:40 - 11:41]

Wow, that was so much research that I did and I hope that I won't need to do as much research any more . . 

I will try to keep things simple and readable for myself but the architecture itself may have documentation later in the future

.......................................................................

### [Written @ 11:26 - 11:40]

(1) I would like to greatly apologize for my mistakes.

(2) I know I have made very many mistakes throughout this project.

(3) (1) I did research when it was possibly easier to not do research and continue with the project development without research effort.

(4) (2) I have now discovered that (1) `Firebase` (2) `Heroku` (3) `Algoliasearch` are the 3 primary resources we'll need to have a successful project

.......................................................................

Without `Firebase`, `Heroku` or `Algoliasearch` our project might have a different focus.

.......................................................................

Fortunately, the resources provided by `Firebase`, `Heroku` and `Algoliasearch` are easy to use

.......................................................................

Because `Firebase`, `Heroku` and `Algoliasearch` are so easy to use

I am expecting that the entire planning that I did around the 'distributed' nature of the project ought to be cancelled now so I can further make efficient progress without further postponing the project due to 'research'

.......................................................................

There are too many architectural problems that I have

.......................................................................

Because I am not the best programmer in the world,

.......................................................................

Because I did not have a blueprint for the best architecture

.......................................................................

Firebase, Heroku and Algoliasearch

.......................................................................

I hope that others that fork this project will find those resources easy to use

.......................................................................

MongoDB, SQL, Postgres and other resources were runners up for 'firebase' alternatives

Glitch, Redhat OpenShift and Microsoft Azure and Google Cloud and others were runners up for 'heroku' alternatives

solr, lunr, elastic search and others were runners up for alternatives to 'algoliasearch'

.......................................................................

And now finally, because I've realized

(1) Firebase is free for a long time

(2) Heroku is free for a long time

(3) Algolia Search is free for a long time

.......................................................................

I think it is better to prepare this project with these three primary resources

.......................................................................

IPFS, WebDB, OrbitDB

.......................................................................

There are even more resources that I had prepared to use

.......................................................................

But that was very much for research

.......................................................................

It was a full proof plan

.......................................................................

For the most effectiveness

.......................................................................

I am a smart engineer and I can still make it work out

.......................................................................

But right now it better to not focus on the research aspect

.......................................................................

One research goal that you might figure that I have discovered is using

url names for a lot of things so for example

'firebase-authentication://google'

'firebase-authentication://github'

'firebase-authentication://facebook'

.......................................................................

'mongodb://???/???/??? ?query=something&query2=something

.......................................................................

'firebase://???/???/??? ?query=something&query2=something

.......................................................................

having protocol names for 

'firebase'

'mongodb'

and all the other myriads of resources all use a similar protocol convention as http and https so that they can be as easy to use as URLs is such a cool discovery that I hope you or others take advantage of

.......................................................................

That codebase is hinted at in the Ecoin 2.0 codebase

.......................................................................

Ecoin 1.0 used a sort of 'the-light-architecture' approach

.......................................................................

Ecoin 2.0 used a 'listless-architecture' approach

.......................................................................

Next I would like to have Ecoin 3.0 which is starting to use 'the-light-architecture' approach again

.......................................................................

this time I would possibly like to get away from making the project so distributed

.......................................................................

Holy gosh

.......................................................................

`Distributed architectures need more research`

.......................................................................

Go be a researcher for distributed architectures

.......................................................................

(1) I am embarrassed to say that I didn't always make the right choices

.......................................................................

But my choices were legitimate

.......................................................................

I will now try to focus my effort

.......................................................................

.......................................................................




### [Written @ 9:32 - 10:53]

(1) I forgot what some of this code was going to do.

(2) I had an intuition of what it could possibly do . . but I don't think I remember how it connects with other code or how it works

..

I miss the light architecture the way the codebase was written many months ago

..

The codebase now is really difficult to read and possibly even difficult to write because I rely on updating old code from intuitions that I had several months ago when originally writing that code

..

I lost that intuition now and should possibly go back to using the light architecture

..


### [Written @ 9:26 - 9:31]


(1) I seem to be quite stuck.

(2) I am relying on Seth to type for me some programs sometimes.

(3) That has left me poor and questioning where and when to type programs on my own.

(4) I think using ghosts to help you evaluate programs can leave you poor.

........................................................................

(1) The light architecture

(2) Evolved into the listless architecture

........................................................................

And now both architectures aren't very good

........................................................................

(1) The light architecture was good

(2) The listless architecture was also okay

........................................................................

........................................................................

Seth do you have anything to say?

........................................................................

[Seth]: Hello I am Seth. I speak for Jon Ide. I write notes for him sometimes and he casually pleases himself with some of the programs that I write.

........................................................................

........................................................................




