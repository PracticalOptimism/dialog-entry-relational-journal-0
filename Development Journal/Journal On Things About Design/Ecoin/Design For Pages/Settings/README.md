
# Settings

### Settings Introduction

```html

Settings

Settings helps you find (1) Information about `Your Account` (2) Information about Network Privileges You Allow (2) Information about Application Privileges You Allow and finally (3) Automatic Updates To This Ecoin Software By Program Developers

```

### Settings Resources


```html

⚙️ Account Settings
Resources about how to update your account

👩 Personal Information Settings

.......................................................................
👩 Account Information Settings                                   🔽
.......................................................................
🚶‍♀️ Transaction Information Settings                               🔽
.......................................................................
🛍️ Product Resource Information Settings                          🔽
.......................................................................
💼 Job Employment Resource Information Settings                   🔽
.......................................................................
🚶‍♀️ Currency Exchange Information Settings                         🔽
.......................................................................
💰 Ecoin Alternative Settings                                     🔽
.......................................................................

⭐ Network Related Settings

.......................................................................
🔐 Network Privileges Settings                                    🔽
.......................................................................

⚙️ Application User Experience Settings

.......................................................................
📱 Application Settings                                           🔽
.......................................................................

ℹ️ Developer Team Contact Settings

.......................................................................
🔁 Software Updates Settings                                      🔽
.......................................................................




```


```html

Account Settings

Account Information Settings
Transaction Information Settings
Product Resource Information Settings
Job Employment Information Settings
Currency Exchange Information Settings
Ecoin Alternative Settings

Network Privileges Settings

Application Settings

Software Updates Settings

```

### Digital Currency Account List

.......................................................................
[photograph] | Account Name | Currency Account Balance | [Set Active]
.......................................................................

.......................................................................

### Transaction Information Settings

```html

Turn on and off notifications from Universal Basic Income Transactions

```

### Product Resource Information Settings

```html

```

### Job Employment Information Settings

```html

```

### Currency Exchange Information Settings

```html

Paypal.me Account URL - Using this information, everyday paypal users can send your account money or resources

Stripe API Key - Using this information, developers can send your stripe account money or resources

[Other Payment Methods] Add more methods that currency exchange users can use to contact you send you your money when initiating currency exchange trades

```

### Ecoin Alternative Settings

```html

```

.......................................................................

### Network Privileges Settings

```html

Blocked Account List

```


### Application Settings

```html

Turn Off Notifications


```


### Software Updates Settings

```html

```





