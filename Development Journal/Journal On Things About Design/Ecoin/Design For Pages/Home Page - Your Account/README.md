
# Home Page - Your Account

### Legend Map

### Application Specific Map Items

🔵 - Project Icon

### General Map Items

▶️ - Paper Airplane Icon (Send)<br>
🔍 - Search Icon<br>
👩 - Person Icon<br>
💰 - Money Icon<br>





### Header

```html
.......................................................................
🔵 100 Ecoin * 99+ products pending          [▶️ Send Payment]  🔍  👩
   Customer Name
.......................................................................

```

### Header Support

```html
.......................................................................
     [👩 Your Account] [⭐ Network Status] [⚙️ Settings] [ℹ️ About]
.......................................................................
```

### Your Account Introduction

```html
                                  O
                             O         O
                                _______
                           O   | o   o |  O
                                | --- |
                             O         O
                                  O
                            Customer Name

                      📍 Los Angeles, California
                      🌐 https://website.com

........................................................................
100 Ecoin                                                           🔵
Account Balance
........................................................................
https://ecoin369.web.app/customer-username                          👩
Account Username
........................................................................
                      [▶️ Send Payment][👩 Account]
........................................................................
1 Ecoin                                                             💰
Amount of Universal Basic Income Distributed Per 1 minute. For free, forever.
........................................................................
54 seconds                                                          🕑
Time Until Universal Basic Income Is Distributed To Your Account
........................................................................
OOOOOOOOO..................10% Complete.................................
........................................................................

```


### Resources Introduction



```html

                           👩 Account Resources

.......................................................................
👩 Account Description                                             🔽
.......................................................................
📊 Account Statistics                                              🔽
.......................................................................
📊 Transaction Statistics                                          🔽
.......................................................................
⭐ Your Groups                                                     🔽
.......................................................................

                           🛒 Product Resources

.......................................................................
🛒 Your Shop                                                       🔽
.......................................................................
🛒 Find Places To Shop                                             🔽
.......................................................................
🛒 Find Customers To Sell To                                       🔽
.......................................................................
📊 Your Shopping Statistics                                        🔽
.......................................................................

                        💼 Job Employment Resources

.......................................................................
💼 Your Job Listings                                               🔽
.......................................................................
💼 Find Places To Work                                             🔽
.......................................................................
💼 Find Employees To Hire                                          🔽
.......................................................................
📊 Your Work Statistics                                            🔽
.......................................................................

                      ↔️ Currency Exchange Resources

.......................................................................
↔️ Your Currency Exchanges                                          🔽
.......................................................................
↔️ Find Currency Exchanges                                          🔽
.......................................................................
↔️ Find Customers Searching For A Currency Exchange                 🔽
.......................................................................
📊 Your Currency Exchange Statistics                               🔽
.......................................................................

                      🌎 Ecoin Alternative Resources

.......................................................................
🌎 Your Ecoin Alternatives                                         🔽
.......................................................................
🌎 Find Ecoin Alternatives                                         🔽
.......................................................................
🌎 Find Customers Searching For Ecoin Alternatives                 🔽
.......................................................................
📊 Your Ecoin Alternatives Statistics                              🔽
.......................................................................



```



[Deleted on September 27, 2021]
[This Model doesn't look as nice and convenient for the user as I had initially envisioned that it would be at least nice for the feature of not having to click into the account description or statistics to read further. Well. I just don't like how this part looks in the codebase / user interface experience. I'm sorry.]

```html

                           Account Resources
.......................................................................
100 Ecoin per Month        | 10 Ecoin per Month         |
Incoming Funds (per Month) | Outgoing Funds (per Month) |
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
10 Transactions                  |
Recently Created (last 48 hours) |
.......................................................................
👩 Account Description                                             🔽
.......................................................................
📊 Account Statistics                                              🔽
.......................................................................
📊 Transaction Statistics                                          🔽
.......................................................................
⭐ Your Groups                                                     🔽
.......................................................................

                           Product Resources
.......................................................................
0 Products                         | 0 Products
Recently Published (last 48 hours) | Recently Purchased (last 48 hours)
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
0 Customer Requests                |
Recently Requested (last 48 hours) |
.......................................................................
🛒 Your Shop                                                       🔽
.......................................................................
🛒 Find Places To Shop                                             🔽
.......................................................................
🛒 Find Customers To Sell To                                       🔽
.......................................................................
📊 Your Shopping Statistics                                        🔽
.......................................................................

                        Job Employment Resources
.......................................................................
0 Employees                   | 0 Jobs
Recently Hired (last 2 weeks) | Recently Published (last 2 weeks)
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
0 Jobs                             |
Recently Applied To (last 2 weeks) |
.......................................................................
💼 Your Job Listings                                               🔽
.......................................................................
💼 Find Places To Work                                             🔽
.......................................................................
💼 Find Employees To Hire                                          🔽
.......................................................................
📊 Your Work Statistics                                            🔽
.......................................................................

                      Currency Exchange Resources
.......................................................................
0 Trades                        | 0 Requests To Trade
Recently Traded (last 48 hours) | Recently Requested (last 48 hours)
.......................................................................
↔️ Your Currency Exchanges                                          🔽
.......................................................................
↔️ Find Currency Exchanges                                          🔽
.......................................................................
↔️ Find Customers Searching For A Currency Exchange                 🔽
.......................................................................
📊 Your Currency Exchange Statistics                               🔽
.......................................................................

                      Ecoin Alternative Resources
.......................................................................
0 Alternatives                     | 0 Requests To Use
Recently Published (last 2 months) | Recently Requested (last 2 months)
.......................................................................
🌎 Your Ecoin Alternatives                                         🔽
.......................................................................
🌎 Find Ecoin Alternatives                                         🔽
.......................................................................
🌎 Find Customers Searching For Ecoin Alternatives                 🔽
.......................................................................
📊 Your Ecoin Alternatives Statistics                              🔽
.......................................................................


```

```html

<!-- This code was responsible for adding this feature for a 'quick' description above the resource items -->

<!-- Account Resources Short Summary -->
    <v-divider></v-divider>
    <div class="account-resources-short-summary">
      <div style="display: flex; align-items: center;">
        <v-chip class="ma-2" color="red" text-color="white">4</v-chip>
        <HorizontalList style="border-left: 1px solid #BBBBBB; border-right: 1px solid #BBBBBB;">
          <v-list-item class="horizontal-list-item" v-for="index in 10" :key="index">
            <v-list-item-icon>
              <v-icon>person</v-icon>
            </v-list-item-icon>
            <v-list-item-content>
              <v-list-item-title>Hello World</v-list-item-title>
              <v-list-item-subtitle>Account Balance</v-list-item-subtitle>
              <v-list-item-subtitle>{{ index }} of 10</v-list-item-subtitle>
            </v-list-item-content>

            <v-list-item-icon>
              <v-btn icon large><v-icon>menu</v-icon></v-btn>
            </v-list-item-icon>
          </v-list-item>
        </HorizontalList>
        <v-btn icon large>
          <v-icon>keyboard_arrow_right</v-icon>
        </v-btn>
      </div>
    </div>
    <v-divider></v-divider>

```

### Website Footer Material


```html


                                    O
                              O         O
                                  
                            O      🔵      O
                                  
                              O         O
                                    O
                      A new currency for a new era

                        [🎥 Watch A Video Tutorial]

                        Learn more about Ecoin

        [👩 Your Account] [⭐ Network Status] [⚙️ Settings] [ℹ️ About] 

                        Join the global community
                  Let people know your participation


                            ...............
                            .             .
                            .      🔵     .
                            .    E Coin   .
                            .             .
                            ...............

                            Stay connected
                          [▶️YouTube][🦊Gitlab]



                               THANK YOU
                            FOR USING ECOIN.
    Thank you for particpating in our Universal Basic Income Program.
    We’re continuing to work on new ways to make Ecoin better for you.
                             Stay tuned.


```

.......................................................................

.......................................................................

# Account Resources - Account Description

```html
.......................................................................
⬅️

                    ..............................
                    .  🔍 Search For Description . ⚙️
                    ..............................

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Basic Description          Transaction Resource      Product Resource
                           Center Description        Center Description

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Job Employment             Currency Exchange         Ecoin Alternative
Center Description         Center Description        Center Description

                        < | 1 | | 2 | | 3 | | 4 | >

.......................................................................
```

```html
.......................................................................

Account Name
Account Username
Account Id

Account Description Text

Account Phone Number
Account Website
Account Email Address

Account Physical Address City
Account Physical Address State
Account Physical Address Country

Account Physical Mailing Address

Account Social Media Account Id #1
Account Social Media Account Id #2

Account Recovery Account #1

Description Title #1
Description Title #2

```



# Account Resources - Account Statistics

```html
.......................................................................
⬅️

                    ..............................
                    .  🔍 Search For Statistics  .
                    ..............................

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Basic Statistics👁️         Boolean         👁️         Inquiry      👁️
                           Statistics                 Statistics

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Line Graph      👁️          Pie Graph      👁️
Statistics                 Statistics


                        < | 1 | | 2 | | 3 | | 4 | >

.......................................................................
```

### Basic Statistics

Number of Accounts Transacted With

Number of Accounts Receiving Income From

Number of Accounts Sending Income To

Number of Product Resources Published

Number of Product Resources Sold

Number of Product Resources Purchased

Number of Active Product Resources

Number of Published Job Employment Jobs

Number of Accepted Job Employment Applications

Number of Rejected Job Employment Applications

Number of Accounts Sent A Job Application Promotion To

Number of Accounts Received A Job Application Promotion From

Number of Currency Exchange Transactions

Number of Currency Exchange Promotions Sent

Number of Currency Exchange Promotions Received

Number of Ecoin Alternatives Published

.......................................................................

### Boolean Statistics

Is Looking For Product Resources: True

Is Looking For Job Employment: True

Is Looking For Currency Exchange: True

Is Looking For Ecoin Alternative: True

.......................................................................

### Inquiry Statistics

Product Resources Being Searched For: something, something, something

Job Employment Being Searched For: something, something, something

Currency Exchange Being Searched For: something, something, something

Ecoin Alternative Being Searched For: something, something, something

.......................................................................

.......................................................................





# Account Resources - Transaction Statistics

.......................................................................

.......................................................................




.......................................................................

.......................................................................

### Content Listing

```html
.......................................................................

⬅️
                   .............................. _____
                   .     🔍 Search For Item     . | \/ |
                   ..............................  ----

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Item #1         👁️          Item #2        👁️         Item #3      👁️

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Item #4         👁️          Item #5        👁️         Item #6      👁️

                        < | 1 | | 2 | | 3 | | 4 | >

.......................................................................
```


### Content Presentation

```html
.......................................................................
⬅️
..................
.  [photograph]  . Content Title
.                . Content Description By Minimum Length
..................

[👩] Content Publisher Name (@Username)
     Published By

       [Action Button (open popup)] [Action Button (open popup)]

Features
.......................................................................
Feature #1    | Feature #2    | Feature #3    | Feature #4    |    >
Description   | Description   | Description   | Description   |
.......................................................................
Videos and Images
..................         ..................        ..............
.                .         .                .        .             >
.                .         .                .        .             
..................         ..................        ..............
Content Item #1            Content Item #2           Content Item #

Content Description By Maximum Length


                              [Show More]

.......................................................................
Your Statistic #1   | Your Statistic #2   | Your Statistic #3   |  >
Statistic Name      | Statistic Name      | Statistic Name      |
.......................................................................
Audience Statistic #1   | Audience Statistic #2   | Audience Sta   >
Statistic Name          | Statistic Name          | Statistic Name
.......................................................................

.......................................................................
[Audience Action Button #1] [Audience Action Button #2]            >
.......................................................................
Drop Down Area For More Information
.......................................................................

Messages From The Audience

Message #1
By Person Number A * [Statistic #1] * [Statistic #2]
Message Description
[Action Button #1] [Action Button #2]

Message #2
By Person Number B * [Statistic #1] * [Statistic #2]
Message Description
[Action Button #1] [Action Button #2]

                        < | 1 | | 2 | | 3 | | 4 | >

.......................................................................

More Like This

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Content Item #1            Content Item #2           Content Item #3

                        < | 1 | | 2 | | 3 | | 4 | >
.......................................................................

🔴 Others Are Looking At Now

..................         ..................        ..................
.                .         .                .        .                .
.                .         .                .        .                .
..................         ..................        ..................
Content Item #1            Content Item #2           Content Item #3

                        < | 1 | | 2 | | 3 | | 4 | >
.......................................................................



```


Example Feature:
- 200 Ecoin (Payment Price), 200 Ecoin per Month (Payment Price)
- Free (Payment Price)

.......................................................................

Example Actions:
- Purchase Product, Buy Now, Subscribe, Pay Subscription, Watch Video, Play Game, Play Now, Order Now
- Open Document, View Document
- Report

- View Network Delivery Route

- Add To Group, Remove From Group

[Buy Now][Open Document][Show Routing][Add To Group]
(Show Open Document for 'Repeated Purchases')

[Buy Now][Show Routing][Add To Group]

[Open Document][Show Routing][Add To Group]
.......................................................................

Example Audience Actions:
- Write Comment, Write Review, Like, Dislike
- Show More Comments, Filter Comments By (Popularity, Latest, By The Publisher)
.......................................................................
Example Message Actions:
- Write Comment, Show Comments, Report, Like, Dislike

.......................................................................

1 Time Purchase Then Free Forever
1 Time Purchase Then Free Until End Date
1 Time Purchase Then Free Until Missed Recurring Transaction
Repeated Purchases

.......................................................................




.......................................................................

.......................................................................


```html
   ____
  | \/ | Filter Icon
   ----_____________
  | < Back          |
  |_________________|
  |                 |
  | Popularity    > |
  |_________________|
  |                 |
  | Location      > |
  |_________________|
  |                 |
  |  More Options   |
  |  < 1 | 2 | 3 >  |
  |_________________|
```

.......................................................................



