
# Popup Item

### Legend Map

### Application Specific Map Items

🔵 - Project Icon

### General Map Items

🗖 - Maximize Icon
👩 - Person Icon<br>



### Transaction

"Vuetify Dialog"

```html

.......................................................................
⬅️ Create Transaction                                          👩   X
.......................................................................

0 Ecoin * 10 Products                                              🔵
Every 7 Days (No Cancellation Date) [Until 9/10/2022]
Transfer Currency Amount

0 Ecoin                                                            👩
Remaining Account Balance

.......................................................................
ℹ️ Fill in the transaction form below
.......................................................................

Your Account
Recipient Account

Payment Amount
Notes

Transaction Type
- General, Gift, Donation, Product Resource Purchase, Product Resource Promotion, Currency Exchange Trade Purchase, Currency Exchange Trade Promotion, Job Employment Application Fee, Job Employment Application Promotion, Job Employment Salary, Coupon / Discount Promotion, Investment, Investment Return Payment, Refund Payment, Universal Basic Income

[?] Send Transaction Today
   \/ Transaction Date

[?] Send Transaction Now (Current Time)
   \/ Transaction Time

[?] Subscription Payment
   One payment every X Period
   \/ Recurrence Time Period
   \/ Time Scale Unit
   [?] Unlimited Number of Subscription Payments
      \/ Subscription Payment Cancellation Date

[?] This Transaction Is On Behalf Of Another Account
   \/ Beneficiary Account

....Product Resource Information....

Product Resource

Number Of Products

[?] Product Delivery Method
   [text][show information][eye-icon (show or hide details)]
   \/ Method Type (In-Person Pickup, Physical Address Delivery, Email Address Delivery, Phone Number Delivery, Online Delivery, No Delivery)
   \/ For Provided Account (Select User Account)

[?] Sender's Anticipated Time To Delivery

[?] Recipient's Estimated Time To Delivery

....Advanced Settings....

[?] Sender's Programmable Note

[?] Recipient's Programmable Note

.......................................................................

```

Is Product Received<br>
Is Product Sent





### News Updates

"Vuetify Snackbar"

`Congratulations, Your Account Has Been Created.`

`Congratulations, Universal Basic Income Has Been Delivered To Your Account.`

`You have created 1 Transaction Recently` (transaction)

`You have purchased 1 Item Recently` (product resource purchase)

`You have traded 1 Item Recently` (currency exchange trade)

`You have created 1 Item Recently` (product resource, currency exchange, etc.)



### Content Presentation Viewport


```html
.......................................................................
Content Title                                                🗖     X
.......................................................................




















.......................................................................
```



....Media Format Reader....

(1) Video, Image
(2) Game, Web Application
(3) Music

(4) Book, PDF, Markdown, Text File

(5) Website URL Content

....Download Files....

(6) Multiple Files To Download
(7) Courseware

....Navigate Folder Hierarchy....

(8) File System Grouping

(9) 

### Search Settings


```html
.......................................................................
⚙️ Search Settings                                                 X
.......................................................................




.......................................................................
```


.......................................................................


### Account


```html
.......................................................................
👩 Account                                                         X
.......................................................................

                        .......................
                        .                     .
                        .                     .
                        .          QR         .
                        .         Code        .
                        .                     .
                        .                     .
                        .......................

.......................................................................
Account QR Code
.......................................................................
????
Account Id
.......................................................................
????
Account Username
.......................................................................

.......................................................................
```

### QR Code Scanner

```html
.......................................................................
👾 QR Code Scanner                                                 X
.......................................................................

               ........................................
               .                                      .
               .                                      .
               .                                      .
               .                                      .
               .            Camera Video              .
               .               Stream                 .
               .                                      .
               .                                      .
               .                                      .
               .                                      .
               .                                      .
               ........................................

.......................................................................
????                                                  [Open Account]
Account Result [(Product Creator)]
.......................................................................
????                                                  [Open Product]
Product Result
.......................................................................


.......................................................................
```

.......................................................................

(1) Open another dialog box with the content of interest

.......................................................................





