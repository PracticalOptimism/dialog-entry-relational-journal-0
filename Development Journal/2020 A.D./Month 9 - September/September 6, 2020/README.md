
# Development Journal Entry - September 6, 2020









[Written @ 13:46]
[copy-pasted from Day Journal @ September 6, 2020]
[Written @ 12:53 - 13:20]

I would like to mention what I'm working on now . . 

. . The expectation definitions . . for the ecoin project . . are taking advantage of the . . "Firebase Realtime Database" Emulator Suite . . Which is a program that starts a database server at localhost:9000 . . 

The expectation definitions . . or unit tests . . are ways to measure that the source code is working as we expect . . And so . . we can run the measurements . . as many times as we'd like . . [unit tests was a previous word that was used . . uhm . . and I . . uhm . . out of personal preference as this time . . am using the word or word phrase . . or term . . "expectation definitions" . . which seems to be a useful way to extend to . . uhm . . other . . uhm . . domains of interest . . and not only . . computer science related topics . . uhm . . like software engineering . . . . and so . . biology . . and . . chemistry . . and physics . . can also have expectation definitions . . of how they expect their . . theory . . or theoretical . . code programs . . to work on in their uhm . . discipline of interest . . uhm . . well . . theory can be great . . uhm . . well . . and you can also expect the theory . . which uhm . . in a maybe kind of theoretical way . . you can look at the theory of . . uhm . . . biological theory . . or physics  . . . uhm . . theory . . to be a description of expectations . . or you can ascribe or set expectation definitions . . on . . uhm . . . the world . . based on those descriptions . . or something like that . . or something like this . . uhm . . well . . . so . . maybe you want to run measurements . . on if those expectations are being met . . and so . . . that's essentially uhm . . well . . that's what "running a unit test" . . is all about . . "running a unit test" . . uhm . . translates to . . "taking a measurement on whether the expectation definitions are met" . . or something like that . . "expectation definition measurement" . . or something like that . . ]

Uhm . . the expectation definition measurements can be taken . . . uhm . . as soon as we . . push the code to . . a code repository . . like . . Gitlab . . in our case . . 

And so . . it's already common practice today . . to run . . your unit tests . . in a continuous integration / continuous deployment process . . . uhm . . so . . yea . . it's quite effective practice I suppose . . I'm really still a beginning student in this area of interest . . I'm trying to uhm . . . apply it here on this project . . or for this project . . Uhm . . it's effective to ensure that the codebase . . is meeting the defined expectations . . or that the unit tests are passing . . for when the codebase . . . is . . worked on by several . . contributors . . or something like this . . and so for example . . if the expectations are being met . . the we can be confident . . that our software is working how we expect it to work . . and that logical bugs . . or errors in our logic . . or thinking . . uhm . . maybe not be . . present . . around the topic . . of . . how we defined that expectation . . or something like that . . well . . . . . . uhm . . I'm not sure else to say on this topic . . I'm still learning . . and so . . there is a lot of . . . . uhm . . hand-wavy . . uhm . . uncertainty . . well . . Uhm . . if you are also beginning . . I would encourage you to do more research in the . . continuous integration / continuous deployment processes used today . . by a lot of source code repository engines . . like Gitlab . . Github . . and Bitbucket . . 


Well . . I'm having trouble . . running the expectation definitions . . for the current project . . I would like to have the "Firebase Realtime Database" Emulator . . . . running . . in the same . . Gitlab . . container . . or . . something like that . . the same Gitlab . . process . . or pipeline process . . Uhm . . I'm still new to the terminology . . "Container" . . seems like . . maybe the term that is used . . uhm . . to talk about . . where the . . "pipeline" process . . or "gitlab job" . . . is operated . . uhm . . in a "containerized" . . environment . . which . . from what I've learned so far in the last 1 day . . uhm . . which was mostly yesterday . . uhm . . in Yesterday's live stream . . well . . something like that . . uhm . . in yesterday's live stream . . we set up . . Docker . . and I read the documentation . . they have . . on the . . . "docker101tutorial" . . . application . . . . well . . I'm still only . . just . . getting started . . I'm still a novice . . in this topic . . and not so much . . an expert . . or advanced . . or proficient user of this technology . . 

. . . . I would like to install . . both . . node.js . . and java . . on the same . . container . . or . . instance . . container . . I'm not sure how to do that . . 

Node.js . . is required for running . . uhm . . well . . node.js . . comes with . . npm . . or node package manager . . . . uhm . . . well . . npm is useful for running . . . npm scripts . . 

Java is required for running . . the . . firebase emulator . . 

Well . . without the firebase emulator running . . I don't know . . uhm . . our expectation definitions . . rely . . on the firebase emulator to be running . . 

And so . . what I'm working on now . . is . . . a way . . to install . . java . . on the gitlab container . . along with . . having node.js . . installed as well . . 





