

# Development Journal Entry - September 24, 2020



[Written @ 21:02]

Learning about how to structure the Gitlab Issue . . 

Here are a few notes I've taken so far . . 

Inspired by . . 
[1]
Docs post-merge review: Document GitLab.com puma request timeout
By Thong Kuah
https://gitlab.com/gitlab-org/gitlab/-/issues/255879


[2]
As a user I do not want to see hubble related alerts from Operations -> Alerts page
By Zamir Martins Filho
https://gitlab.com/gitlab-org/gitlab/-/issues/255751


[3]
Spike: Cilium and Hubble integration
By Thiago Figueiró
https://gitlab.com/gitlab-org/gitlab/-/issues/238142


[4]
By GAO Bohan
https://gitlab.com/gitlab-org/gitlab/-/issues/255885

# Gitlab Issue

-- development related issues
* Author's Checklist [1]
* Related Issues [1]
* References [1]
* Implementation Plan [2]
* Linked Issues [2]
* Research Goals [3]

-- community related issues [4]
* Summary
* Steps to reproduce
* Example Project
* What is the current bug behavior?
* What is the expected correct behavior?
* Relevant logs and/or screenshots
* Output of checks
* Results of [Project Name] environment info
* Results of [Project Name] application Check
* Possible fixes

-- done automatically ? 
* Is Blocked By [2]
* Relates to [2]
* Blocks [2, 3]

--





[Written @ 19:02]


Update Algorithm: Apply Universal Basic Income
* Firebase Realtime Database Security Rules
* Expectation Definitions

Update Algorithm: Apply Transaction List
* Firebase Realtime Database Security Rules
* Expectation Definitions

Update Algorithm: Update Transaction
* Firebase Realtime Database Security Rules
* Expectation Definitions

Update Algorithm: Update Account
* Update Account Username
* Firebase Realtime Database Security Rules
* Expectation Definitions

Update Algorithm: Search For Account
* Update Algoliasearch trial account

Update Algorithm: Delete Transaction
* Firebase Realtime Database Security Rules
* Expectation Definitions

Update Algorithm: Delete Account
* Firebase Realtime Database Security Rules
* Expectation Definitions

Update Algorithm: Get Transaction List
* Firebase Realtime Database Security Rules
* Expectation Definitions

Update Algorithm: Get Account List
* Firebase Realtime Database Security Rules
* Expectation Definitions

Initialize Algorithm: Delete Transaction List
* Firebase Realtime Database Security Rules
* Expectation Definitions


Create Continuous Integration / Continuous Deployment (CI / CD) Processes
* Publish Website
* Publish Administrator Daemon

Initialize Pay with Digital Currency HTML Component
* Create HTML Component
* Delete HTML Component
* Expectation Definitions

Initialize Digital Currency Price Label HTML Component
* Create HTML Component
* Delete HTML Component
* Expectation Definitions

Initialize Digital Currency Symbol HTML Component
* Create HTML Component
* Delete HTML Component
* Expectation Definitions

Initialize the Network Status Page
* Newly Created Transactions List
* Number of Transactions Created Over Time
* Number of Accounts Created Over Time

Update Settings Page
* Add a method to sign in with Google account
* Add a method to sign in with email and password
* Update Account Properties
* Delete Account
* 


--

Digital Currency Account

isAnonymous?: boolean = true





--


Digital Currency Transaction Pending Data Structure


is transaction list processed
number transactions
number of failed transactions
number of succeeded transactions


median transaction digital currency amount
maximum transaction digital currency amount
minimum transaction digital currency amount

--

Digital Currency Transaction Data Structure


number of times the transaction has been applied
number of times applied

number of failed process attempts
hasFailedToProcess
reasonForFailedProcess





