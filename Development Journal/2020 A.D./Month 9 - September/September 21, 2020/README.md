
# Development Journal Entry - September 21, 2020

[]

[Written @ 23:09]

'.validate': 'newData.isNumber() && newData.val() === 0'

The firebase realtime database security rule ".validate" allows us to validate the organization of the entries of each of the variable location paths or variable route paths of the database entries . . 

'newData' allows us to understand what are the new data that are being entered into the database for that particular query to the firebase database . . 

and so . . for example . . if . . the firebase browser client is updating the database with { name: 'alice' } then newData will be equal to { name: 'alice' } . . 

if the user is updating . . or inserting . . 

{ name: 'bob' }

. . into the database . . 

then . . 

{ name: 'bob' }

is the newData . . 

newData.val() allows you to access . . { name: 'bob' } . . which is the newly inserted . . or newly updated . . data . . and . . newData.val().name allows you to access . . 'bob' . . 

so if you want to validate to ensure that the database entry has a name that is set to 'bob' or 'alice' . . you could write the following '.validate' rule:

".validate": "newData.val().name == 'alice' || newData.val().name == 'bob'"

That will ensure that if a new data item is being updated to the database then the "name" property field is required to be be . . "alice" or "bob" . . 

. .


Okay . In what we would like to do . . 

We would like to . . Ensure that the transaction id being inserted into our transactionIdVariableTree has a corresponding transaction in the digital-currency-transaction-variable-tree location and that the firebase auth uid that belongs to the account id of the creator of that transaction is the auth.uid that is making the firebase database update


(1) ensure the transaction id is present in the digital-currency-transaction-variable-tree

(2) ensure the firebase auth uid that belongs to the sender account of the transaction is the auth.uid that is making the firebase database update





[Written @ 23:02]

Okay . . so . . my task now . . is to write the firebase realtime database rules for the . . digital-currency-account-to-transaction-map . . 

From there . . I'll have more confidence on the security of the application at that . . database entry point for users to create transactions in regard to this methodology .

[Written @ 22:49]

. . 

Okay . . 

One of the key pieces I was looking for . . was . . how to retrieve . . the . . transactions . . that an account . . is associated with . . 

And . . I've planned this to be . . digital-currency-account-to-transaction-map . . 

And well . . uh . . I don't think I've looked at this part of the codebase for a while . . and so . . maybe felt nervous . . to . . uh . . learn more about that . . topic . . uh . . also . . the firebase rules haven't been written for how . . clients . . uh . . browser clients . . uh . . firebase browser clients will . . update to the . . to . . this part or this section of the database . . uh . so . yea . I . . uh . . am . . needing to look at this further . . uh . one of the ideas . uh . uh . well . I was afraid at one point in uh . updating uh . another user's entry field or another user's variable route uh if you are for example going to update some information not related to the transaction topic . . to a user's database entry field . .

for example . . 

/digital-currency-account-to-transaction-map/my-acocunt-id/transaction-id: {
  helloWorld: 'something here' // hey . . wait a minute . . this isn't supported right ? . . this should be information relating to the transaction or something like that uh for example the transaction id or -__ - well I haven't thought about it too much yet . . uh . . the timestamp of when the transaction was created or processed or something like this . . but uh . an acocunt id is fine . . to prevent redundancy of the data or something like that . . and so . . the transaction id can also be used to retrieve that information relating to the transaction from the digital-currency-transaction-variable-tree/transaction-id . . 
}

. . 

Well . uh . I'm thinking more . . and thinking that it is possible to prevent a case like this where the user is able to add arbitrary information to my-account-id variable path . . so it's uh . maybe less likely to be information that is not related to the topic that we would prefer . . which is . . the transaction id . . something like that . . 

Right . . well . . I'm thinking through these things as well and uh . well . I'm sorry . . uh . that's why maybe my explanations are also not very well written . . or . . uh . are a work-in-progress to explain . . uh . so I'm sorry for the communication uh . if it seems like I'm really not thinking uh . . or explaining the topic well . . uh . 

-_--- - - --__---- -_ - - - __- - - --__-__--_______-- _ _ - - _ _ _ - _ - _ - -_ - - - - _ - __________- - _-__--__-----------------------__-----------






[Written @ 22:32]

I think one of the problems I'm facing right now is . . 

. . . 

createTransaction() // firebase version
* create transaction in digital-currency-transaction-variable-tree
* create transaction in digital-currency-transaction-pending-variable-tree
* create transaction in digital-currency-account-to-transaction-map for sender id
* create transaction in digital-currency-account-to-transaction-map for recipient id


intiailizeAccountListWithDigitalCurrencyAmount()
* checks digital-currency-account-pending-create-variable-tree
* creates a transaction for each of the accounts in this variable tree
* deletes the accounts from the variable tree


applyTransactionList() // the current version (already implemented)
* checks the 

applyTransactionList() // the updated version (yet to be implemented)
* checks digital-currency-transaction-pending-variable-tree for the present day, hour and minute
* creates a transactionId entry in digital-currency-account-id-to-transaction-id-
* updates the transaction.isProcessed to true



. . . uhm . . for educational purposes . . I'll say that . . while . . I'm uh . . thinking . . uh . -_-- ------___-----___-----___---_--- _- - . 




[Written @ 22:18 - 22:32]


Okay . . with the . . intiailize account list with digital currency amount function . . I'm not exactly sure . . what to expect out of this function . . it seems like a lot of the expectations . . are also being conditioned around other . . parts of the codebase . . 

For example . . this initializeAccountListWithDigitalCurrencyAmount function . . is . . well . . it says . . uh . . that . . it's intending to initialize the digital currency amount with an account . . and uh . . well . . that's going to be applied for all of the accounts that are currently waiting for that to happen . . uh . . for example . . all newly created accounts will be . . pending . . or waiting . . for their account to be initialized with a default amount of digital currency . . Well . . this function . . is . . uh . . the way . . well . . it is not really . . applying the transactions . . it is only . . creating the transactions . . uh . . applying the transactions . . is another process . . and so . . for example . . this initializeAccountListWithDigitalCurrencyAmount is dependent on . . the applyTransactionList function or algorithm . . to apply the transactions . . uh . . so . . well . . (1) maybe this function should have a different name . . so . . initializeAccountListWithDigitalCurrencyAmount could be named to something else . . uh . . that is more about . . creating the transactions . . that will initialize the accounts with a digital currency amount . . uh . . but also not trying to suggest that those accounts will have . . any currency . . since . . that won't happen . . until the applyTransactionList is called in a separate algorithm . . uh . . 


. . . 

Uh . . well . . the initializeAccountListWithDigitalCurrencyAmount . . -_- . . uh . . wow . . expectation definitions . . how uh . . I'm not sure . . they are . . well . . uh . . I don't know . . I guess . . dependent on . . other ideas . . uh . . so for example . . how this project is . . uh -_- . . is architected in terms of . . what are the ways the software interacts with itself ? . . uh . in any case . . This is a work-in-progress . . 

-_-

-_-




