
# Development Journal Entry - September 22, 2020

[Written @ 21:59]



Digital Currency Transaction Pending Data Structure


is transaction list processed
number transactions
number of failed transactions
number of succeeded transactions


median transaction digital currency amount
maximum transaction digital currency amount
minimum transaction digital currency amount

--

Digital Currency Transaction Data Structure


number of times the transaction has been applied
number of times applied

number of failed process attempts
hasFailedToProcess
reasonForFailedProcess









[Written @ 21:34]

When a transaction is being checked to see whether or not it can be applied . . 

If the sender account . . currently doesn't have enough digital currency in their account . . the transaction will fail to be applied . . the transaction won't be applied . . because the rules specify that you have to have at least as much digital currency amount in your account as the amount that you're sending in a digital currency transaction . . 

However . . should we try to process the transaction again later ? . . 

For example . . the next time the applyTransactionList function is called . . . 

Well . . One usecase for trying to process the transaction again . . at a later time . . for example . . 2 minutes later . . or . . 2 hours later . . is to . . help ensure that the transaction can at least have a chance to go through . . when the digital currency account may have funds available in the future . . Uh . . it may be the case that the digital currency account doesn't have enough digital currency amount . . to satisfy the condition of the amount they are sending being less than or equal to the amount they have in their account . . in which case . . we may need to consider . . trying the operation . . of applying the transaction . . at a later time . . 

When should we apply the transaction ? . . How many attempts . . should we allow before finally deleting the transaction from the pending variable tree ? . . 

Trying again . . in 2 minutes doesn't seem so bad . . 
For each failure, up to a maximum of 5 failures

Try again in the next:

1st failure: 2 minutes
2nd failure: 20 minutes
3rd failure: 1 hour
4th failure: 24 hours
5th failure: 2 days


2 minutes, 20 minutes, 1 hour, ??, 2 days

Unless the transaction is cancelled









[Written @ 20:46]

1 * 60 * 1000 milliseconds is equal to 60 seconds or 1 minute


1000 milliseconds is equal to 1 second


N * 60 * 1000 milliseconds is N minutes


for N Minutes Until the previous transaction application list process
  // if all the previous transactions in the previous minute were applied, return to indicate that perhaps the previous minutes from here have also been applied






