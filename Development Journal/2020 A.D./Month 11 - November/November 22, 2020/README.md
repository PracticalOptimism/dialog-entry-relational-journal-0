
# Development Journal Entry - November 22, 2020


[Written @ 23:17]



[Written @ 22:56]

[random nonesense was pasted . . from here . . to the day journal entry - November 22, 2020 . . Written @ 23:14]


[Written @ 22:29]

Thoughts on README.md Organization Structure
- 

Thoughts on Database Organization Structure
- 

Thoughts on User Interface Organization Structure
- 

. . . 

Something about the user interface organization structure . . needs to support . . something like . . publishing . . information . . uh . . in a nested format . . or something like this . . 

publishing a data item . . on the website . . uh . . can be a nice feature . . uh . . but . . there's something about . . publishing . . an item . . uh . . and having . . that item being related to another item of interest . . within . . maybe . . a nested . . hierarchy . . or something like that . .


accountStore:
  - accountId1:
    - accountItem: AccountDataStructureInstance
    - accountDescriptionStore: AccountDescriptionStoreDataStructureInstance
    - 

. . . 

- /compiler
- /compiled
- /precompiled
  - /consumer
    - /cli
    - /javascript-library
    - /go-lang-library
    - /rust-lang-library
    - /java-library
    - /c-plus-plus-library
    - /c-library
    - /web-browser
    - /http-server
    - /websocket-server
    - /hyper-protocol
    - /ipfs-protocol
    - /ios-application
    - /android-application
    - /desktop-application
    - /windows-phone-application
    - /unity
    - /unreal-engine
    - /godot
    - /playcanvas
    - etc.
  - /provider
    - /service
      - /firebase
      - /localforage
      - /orbit-db
      - /ipfs // inter-planetary file system
      - /oicp // open internet communication protocol
      - /braid-protocol
      - /hashgraph
      - /ethereum
      - /bitcoin
      - /dat-archive
      - /hyper-drive
      - etc.
    - /service-group
      - /service-group-for-environment-type-1-firebase-and-algolia
      - /service-group-for-environment-type-2-oicp
      - /service-group-for-environment-type-3-orbit-db-and-lunr
      - /service-group-for-environment-type-4-undecided-precursor-services
      - /service-group-for-environment-type-5-undecided-precursor-services
      - /service-group-for-environment-type-6-undecided-precursor-services
      - etc.
    - /service-type
  - /usecase
    - /digital-currency
    - /product-resource-center
    - /shopping-mall // deprecated
    - /currency-exchange // deprecated
    - /messaging-service // undesired at this time
    - /
  - /miscellaneous





[Written @ 22:24]

Currency Exchange
Shopping Mall
Job Employment

. . . 

Currency Exchange Trader
Shopping Mall Shopper
Job Employment Seeker

. . . 






[Written @ 22:09]

Project README.md Organization Structure
Project Database Organization Structure
Project User Interface Organization Structure

. . . 


Product Resource Center
Product Resource Union
Product Resource Management Group
Product Resource Organization Unit
Product Resource Organization Agenda Reunion
Product Resource Organization Meeting Headline
Product Resource Organization Headquarters

Center For Resource Management
Center for Resource Management
Center for Resource Management
Center for Resource Management

. . . 

Product Resource Provisioning Center
Product Resource Distribution Center
Product Resource Management Center
Product Resource Committee Service
Product Resource Center
Community Resource Center
Community Service Center
Community Service Reenactment Group
Community Service Re-enactmenet Service Center
Community Service For Entity Relational COmponent Systems
Community Re-enactment
Community Service Center Relaxation Party

. . . 

Service Resource Center

. . .

Product Resource Center

Product Resource Center
Good Resource Center
Goods Resource Center
Goods And Services Resource Center

. . . 

Managerial Item Management Dashboard

. . . 

Shopping Mall
Place To Shop
Shopping Facility

. . . 

Product Resource Center

. . . 

Product Resource Center
Product Resource Center
Product Resource Center


[Written @ 7:33]


Notes @ Project README.md Anticipation Description

. . . 


* (1) Project Intention Description
* (2) Project Anticipation Description
* (3) Project Title
* (4) Project Description
* (5) Project Benefits
* (6) Project Features
* (7) Project Limitations
* (8) Project Additional Notes
* (9) Project Contact Information
* (10) Project News Updates
* <details><summary>(11) Project Community Restrictions & Guidelines</summary>
  <ul>
    <li>(11.1) Project License Agreement - MIT License</li>
  </ul>
</details>

* <details><summary>(12) Project Codebase</summary>
	<ul>
		<li>(12.1) Introduction</li>
		<li>(12.2) Codebase Installation</li>
		<li>(12.3) How To Rebrand The Project</li>
		<li><details><summary>(12.4) Codebase Development Process</summary>
		<ul>
			<li>(12.4.1) How To Find Files</li>
			<li>(12.4.2) How To Create Files</li>
			<li>(12.4.3) How To Update Files</li>
			<li>(12.4.4) How To Delete Files</li>
			<li>(12.4.5) How to Find Directories</li>
			<li>(12.4.6) How to Create Directories</li>
			<li>(12.4.7) How to Update Directories</li>
			<li>(12.4.8) How to Delete Directories</li>
			<li>(12.4.9) How To Start The Development Environment</li>
			<li>(12.4.10) How To Start The Production Environment</li>
			<li>(12.4.11) How To Stop The Development Environment</li>
			<li>(12.4.12) How To Stop The Production Environment</li>
		</ul>
		</details></li>
	</ul>
</details>


* <details><summary>(13) Project Consumer Resource</summary>
	<ul>
		<li>(13.1) Introduction</li>
		<li>(13.2) Project Command Line Interface Tool</li>
		<li><details><summary>(13.3) Project JavaScript Library</summary>
			<ul>
				<li>(13.3.1) JavaScript Library Installation</li>
				<li>(13.3.2) JavaScript API Reference</li>
				<li>(13.3.3) Supported JavaScript Runtime Environments</li>
			</ul>
		</details></li>
		<li><details><summary>(13.4) Project Service Website</summary>
			<ul>
				<li>(13.4.1) Project Instance Website</li>
				<li><details><summary>(13.4.2) Feature Visualization</summary>
				<ul>
					<li>(13.4.2.1) Digital Currency Features</li>
					<li>(13.4.2.2) Digital Currency Account Features</li>
					<li>(13.4.2.3) Digital Currency Transaction Features</li>
				</ul>
				</details></li>
				<li><details><summary>(13.4.3) Accessibility and Performance Measurements</summary>
				<ul>
					<li>(13.4.3.1) PageSpeed Insights</li>
					<li>(13.4.3.2) WebPageTest</li>
					<li>(13.4.3.3) Language Support Measurements</li>
					<li>(13.4.3.4) Internationalization Support Measurements</li>
					<li>(13.4.3.5) Disability Assistance Technology Support Measurements</li>
				</ul>
				</details></li>
				<li><details><summary>(13.4.4) Active User Usage Measurements</summary>
					<ul>
						<li>(13.4.4.1) Google Analytics measurements</li>
						<li>(13.4.4.2) Hotjar measurements</li>
					</ul>
				</details></li>
			</ul>
		</details></li>
		<li>(13.5) Project Android App</li>
		<li>(13.6) Project iOS App</li>
		<li>(13.7) Project HTTP Server</li>
		<li>(13.8) Project Websocket Server</li>
	</ul>
</details>


* <details><summary>(14) Project Provider Resource</summary>
	<ul>
		<li><details><summary>(14.1) Introduction</summary></details></li>
		<li><details><summary>(14.2) Artificial Intelligence Provider</summary></details></li>
		<li><details><summary>(14.3) Asymmetric Cryptography Provider</summary></details></li>
		<li><details><summary>(14.4) Authentication Provider</summary></details></li>
		<li><details><summary>(14.5) Computer Resource Provider</summary></details></li>
		<li><details><summary>(14.6) Cryptographic Hash Function Provider</summary></details></li>
		<li><details><summary>(14.7) Database Provider</summary></details></li>
		<li><details><summary>(14.8) Programming Language</summary></details></li>
		<li><details><summary>(14.9) Project Compiler</summary></details></li>
		<li><details><summary>(14.10) Text Search Provider</summary></details></li>
		<li><details><summary>(14.11) User Interface / User Experience Framework / Library</summary></details></li>
		<li><details><summary>(14.12) Web Browser Website Framework / Library</summary></details></li>
	</ul>
</details>


* <details><summary>(15) Project Usecase Resource</summary>
	<ul>
		<li><details><summary>(15.1) Introduction</summary></details></li>
		<li><details><summary>(15.2) Digital Currency</summary></details></li>
		<li><details><summary>(15.3) Product Resource Center</summary></details></li>
	</ul>
</details>


* <details><summary>(16) Project Community & Volunteers</summary>
	<ul>
		<li>(16.1) Present Volunteers and Community Members</li>
		<li>(16.2) How To Communicate To Project Volunteers</li>
		<li>(16.3) How To Be a Volunteer or Community Member</li>
		<li><details><summary>(16.4) Community Feedback Measurements Members</summary>
			<ul>
				<li>(16.4.1) Frequently Asked Questions</li>
				<li>(16.4.2) Community Comments that initially inspired the project development</li>
				<li>(16.4.3) Community Comments that continue to inspire the project development</li>
				<li>(16.4.4) Recent Feedback relating to the active development process</li>
				<li>(16.4.5) Recent Feedback on the Quality of the Codebase</li>
				<li>(16.4.6) Recent Feedback on the Quality of the Instance Website</li>
				<li>(16.4.7) Recent Feedback on the Quality of the Instance Android App</li>
				<li>(16.4.8) Recent Feedback on the Quality of the Instance iOS App</li>
			</ul>
		</details></li>
	</ul>
</details>
* (17) Project Usecase Provided By Related Community Projects


. . . 



[Written @ 4:43]


* (1) Project Intention Description
* (2) Project Anticipation Description

* (3) Project Title
* (4) Project Description
* (5) Project Benefits
* (6) Project Features
* (7) Project Limitations
* (8) Project Additional Notes

* (9) Project Contact Information
* (10) Project News Updates
* (11) Project Community Restrictions & Guidelines

* (12) Project Codebase
* (13) Project Consumer Resource
* (14) Project Provider Resource
* (15) Project Usecase Resource

* (16) Project Community & Volunteers

* (17) Project Usecase Provided By Related Community Projects


. . . 

Editable Copy:

* (1) Project Intention Description
* (2) Project Anticipation Description

* (3) Project Title
* (4) Project Description
* (5) Project Benefits
* (6) Project Features
* (7) Project Limitations
* (8) Project Additional Notes

* (9) Project Contact Information
* (10) Project News Updates
* (11) Project Community Restrictions & Guidelines

* (12) Project Codebase
* (13) Project Consumer Resource
  * (13.1) Introduction
  * (13.2) Project Command Line Interface Tool
  * (13.3) Project JavaScript Library
  * (13.4) Project Service Website
  * (13.5) Project Android App
  * (13.6) Project iOS App
  * (13.7) Project HTTP Server
  * (13.8) Project Websocket Server
* (14) Project Provider Resource
* (15) Project Usecase Resource
  * (15.1) Introduction
  * (15.2) Digital Currency

* (16) Project Community & Volunteers
  * (16.1) Introduction
  * (16.2) Digital Currency

* (17) Project Usecase Provided By Related Community Projects





. . . 




* (1) Project Intention Description
* (2) Project Anticipation Description

* (3) Project Title
* (4) Project Description
* (5) Project Benefits
* (6) Project Features
* (7) Project Limitations
* (8) Project Additional Notes

* (9) Project Contact Information
* (10) Project News Updates
* (11) Project Community Restrictions & Guidelines

* (12) Project Codebase
  * (12.1) Introduction [Get]
  * (12.2) Codebase Installation [Get]
  * (12.3) How To Rebrand The Project [Create]
  * (12.4) Codebase Development Process [Initialize]
    * (12.4.1) How To Find Files [Get]
    * (12.4.2) How To Create Files [Create]
    * (12.4.3) How To Update Files [Update]
    * (12.4.4) How To Delete Files [Delete]
    * (12.4.5) How to Find Directories [Get]
    * (12.4.6) How to Create Directories [Create]
    * (12.4.7) How to Update Directories [Update]
    * (12.4.8) How to Delete Directories [Delete]
    * (12.4.9) How To Start The Development Environment [Initialize]
    * (12.4.10) How To Start The Production Environment [Initialize]
    * (12.4.11) How To Stop The Development Environment [Uninitialize]
    * (12.4.12) How To Stop The Production Environment [Uninitialize]
* (13) Project Consumer Resource
  * (13.1) Introduction
  * (13.2) Project Command Line Interface Tool
  * (13.3) Project JavaScript Library
    * (13.3.1) JavaScript Library Installation [Get]
    * (13.3.2) JavaScript API Reference [Create]
    * (13.3.3) Supported JavaScript Runtime Environments [Get]
  * (13.4) Project Service Website
    * (13.4.1) Project Instance Website [Update]
    * (13.4.2) Feature Visualization [Get]
      * (13.4.2.1) Digital Currency Features [Get]
      * (13.4.2.2) Digital Currency Account Features [Get]
      * (13.4.2.3) Digital Currency Transaction Features [Get]
    * (13.4.3) Accessibility and Performance Measurements [Get]
      * (13.4.3.1) PageSpeed Insights [Get]
      * (13.4.3.2) WebPageTest [Get]
      * (13.4.3.3) Language Support Measurements [Get]
      * (13.4.3.4) Internationalization Support Measurements [Get]
      * (13.4.3.5) Disability Assistance Technology Support Measurements [Get]
    * (13.4.4) Active User Usage Measurements [Get]
      * (13.4.4.1) Google Analytics measurements [Get]
      * (13.4.4.2) Hotjar measurements [Get]
  * (13.5) Project Android App
  * (13.6) Project iOS App
  * (13.7) Project HTTP Server
  * (13.8) Project Websocket Server
* (14) Project Provider Resource
  * (14.1) Introduction
  * (14.2) Artificial Intelligence Provider
  * (14.3) Asymmetric Cryptography Provider
  * (14.4) Authentication Provider
  * (14.5) Computer Resource Provider
  * (14.6) Cryptographic Hash Function Provider
  * (14.7) Database Provider
  * (14.8) Programming Language
  * (14.9) Project Compiler
  * (14.10) Text Search Provider
  * (14.11) User Interface / User Experience Framework / Library
  * (14.12) Web Browser Website Framework / Library
* (15) Project Usecase Resource
  * (15.1) Introduction
  * (15.2) Digital Currency

* (16) Project Community & Volunteers
  * (16.1) Present Volunteers and Community Members [Get]
    * (16.1.1) Project Codebase Volunteers
    * (16.1.2) Project Communication Volunteers
    * (16.1.3) Project Service Volunteers
  * (16.2) How To Communicate To Project Volunteers [Update]
  * (16.3) How To Be a Volunteer or Community Member [Get]
  * (16.4) Community Feedback Measurements [Get]
    * (16.4.1) Frequently Asked Questions [Get]
    * (16.4.2) Community Comments that initially inspired the project development [Get]
    * (16.4.3) Community Comments that continue to inspire the project development []
      * (16.4.3.1) Today's Commentary, Circa 2020 +/- 30 years [Get]
        * Jacque Fresco
        * John Perry Barlow
      * (13.3.2) Today's Commentary, Circa 2020 +/- 2 years [Get]
        * Paget Kagy
        * Unicole Unicron
    * (16.4.4) Recent Feedback relating to the active development process
    * (16.4.5) Recent Feedback on the Quality of the Codebase
    * (16.4.6) Recent Feedback on the Quality of the Instance Website
    * (16.4.7) Recent Feedback on the Quality of the Instance Android App
    * (16.4.8) Recent Feedback on the Quality of the Instance iOS App

* (17) Project Usecase Provided By Related Community Projects



. . . 





* (1) Ecoin [Get]

* (2) Project Description [Get]

* (3) Project Benefits [Get]

* (4) Project Features [Get]

* (5) Project Limitations [Get]

* (6) Project Progress Report [Get] [Add Event Listener, Development Process]
  * (6.1) Completed Tasks [Get]
  * (6.2) In-Active-Development Tasks [Get]
  * (6.3) Planned Tasks / Not yet completed [Get]

* (7) Project Codebase [Get]
  * Introduction
  * (7.1) Codebase Installation [Get]
  * (7.2) Codebase Development Process [Initialize]
    * (7.2.1) How To Find Files [Get]
    * (7.2.2) How To Create Files [Create]
    * (7.2.3) How To Update Files [Update]
    * (7.2.4) How To Delete Files [Delete]
    * How to Find Directories [Get]
    * How to Create Directories [Create]
    * How to Update Directories [Update]
    * How to Delete Directories [Delete]
    * (7.2.5) How To Rebrand The Project [Create]
    * (7.2.6) How To Start The Development Environment (Locally and remotely) [Initialize] [Initialize, Development Process, Development Environment]
    * (7.2.7) How To Start The Production Environment (Locally and remotely) [Initialize, Development Process, Production Environment] [Initialize]

* (8) Project Consumer Resource
  * Introduction
  * (8.1) Project Command Line Interface Tool
  * (8.2) Project JavaScript Library [Get]
    * (8.2.1) JavaScript Library Installation [Get]
    * (8.2.2) JavaScript API Reference [Create]
    * (8.2.3) Supported JavaScript Runtime Environments [Get]
  * (8.3) Project Service Website [Get]
    * (8.3.1) Project Instance Website [Update]
    * (8.3.2) Feature Visualization [Get]
      * (8.3.2.1) Digital Currency Features [Get]
      * (8.3.2.2) Digital Currency Account Features [Get]
      * (8.3.2.3) Digital Currency Transaction Features [Get]
    * (8.3.3) Accessibility and Performance Measurements [Get]
      * (8.3.3.1) PageSpeed Insights [Get]
      * (8.3.3.2) WebPageTest [Get]
      * (8.3.3.3) Language Support Measurements [Get]
      * (8.3.3.4) Internationalization Support Measurements [Get]
      * (8.3.3.5) Disability Assistance Technology Support Measurements [Get]
    * (8.3.4) Active User Usage Measurements [Get]
      * (8.3.4.1) Google Analytics measurements [Get]
      * (8.3.4.2) Hotjar measurements [Get]
  * (8.4) Project Android App
  * (8.5) Project iOS App
  * (8.6) Project HTTP Server
  * (8.7) Project Websocket Server



* (10) Community & Volunteers [Update]
  * Present Volunteers and Community Members [Get]
    * Project Codebase Volunteers
    * Project Communication Volunteers
    * Project Service Volunteers
  * How To Communicate To Project Volunteers [Update]
  * How To Be a Volunteer or Community Member [Get]
  * Community Feedback Measurements [Get]
    * (13.1) Frequently Asked Questions [Get]
    * (13.2) Community Comments that initially inspired the project development [Get]
    * (13.3) Community Comments that continue to inspire the project development []
      * (13.3.1) Today's Commentary, Circa 2020 +/- 30 years [Get]
        * Jacque Fresco
        * John Perry Barlow
      * (13.3.2) Today's Commentary, Circa 2020 +/- 2 years [Get]
        * Paget Kagy
        * Unicole Unicron
    * (13.4) Recent Feedback relating to the active development process
    * (13.5) Recent Feedback on the Quality of the Codebase
    * (13.6) Recent Feedback on the Quality of the Instance Website
    * (13.7) Recent Feedback on the Quality of the Instance Android App
    * (13.8) Recent Feedback on the Quality of the Instance iOS App

* (11) Project Resource Providers and Codebase Dependencies

* (12) Alternative Projects

* (13) Project License Agreement - MIT License










[Written @ 2:53]

. . . 

(1) functions can have a standard way of writing them . . so that . . for example . . all the functions can have . . the same . . argument . . list . . and for example . . those should allow for some many capabilities . . for any individual function . . 

function functionName (functionConsumedItem1, functionConsumedItem2, functionConsumedItem3, etc.): functionProvidedItem {}

function functionName (functionConsumedItemStore: {
  
}): functionProvidedItemStore {
  // Usecase // ?? not sure what to name this at this time . .
}


. . . 

functions . . uh . . input . . processing . . output . . uh . . right . . uh . . hmm . . input . . sequential item list initializing . . output . . something like that . . 

uh . . from my understanding . . sequential item list initializing is something along the process that is performed . . by . . functions . . uh . . from what I'm familiar with today . . and so for example . . sequential is a key term . . where programs operations . . or uh . . program algorithms . . are initialized sequentially . . initialized can mean "invocation" . . or "invoked" . . or uh . . well . . uh . . for example . . a variable creation can be indicated as a a type of . . -_- . . hmm . . the terminology . . I am trying to map it to the . . way the terminology is written in . . the documentation that has been developed by the . . uh . . Project Data Structure, Project Algorithms, and Project Constants & Variables . . uh . . well . . creating variables is uh . . maybe similar to creating an item in the database . . and . . getting the value of a variable . . or uh . . referencing the name of the variable in the codebase can be considered like a get request . . from the database . . uh . . and uh . . updating the value of the variable can be considered like . . a uh . . variable assignment statement . . uh . . something like that . . well . . uh . . uh . . well . . uh . . removing a database item . . using the delete algorithm can be considered like removing uh . . the pointer reference from the variable uh . . or recycling the variable uh . . pointer reference . . uh . . in memory . . or something like that -_- . . I'm not really sure how the process works and so maybe the ways I'm writing things now . . is . . off scale from the particular terminology that would have otherwise been applicable if I were more uh . . in the topic of describing memory access . . events . . or memory recycling events . . -_- . . . . . . . . 





[Written @ 2:20]


[Written @ 1:17]

Item Identifier . . I think this can be anything . . but it's maybe usually something like . . a string . . or a number . . or an image such as a profile photograph . . or maybe a banner image . . or maybe a video . . or something that serves as an identifier signal for a particular item . . 

Item . . I think this can be anything . . but it's maybe usually something like . . a javascript object . . for example . . a javascript object with property fields . . but really it's not restricted to this . . an Item can also be . . a function for example . . it can also be . . uh . . a file . . or something else . . in terms of programming languages today . . an item can be any numerical-represented value . . and so anything that can be represented with numbers but this is not the only constraint for the meaning of this term . . but in practical terms that are applicable today . . in the digital computer system . . an item can be represented by numerical values . . 

Item List . . I think this can be a list of items . . I think this is focused on the idea of an array where values are accessed by numerical indices . . but maybe this idea of a list can also be more general but in any case . . the inspiration is from an array . . where items in the array are indexed by numbers . . 

Item Store . . I think this can be a collection of items . . like a store . . uh . . but really uh . . a store is more focused on the idea of a hashmap . . or a hashtable data structure where values can be indexed with string values and not only numerical values like with a list . . 

Item Simultaneous Negative Loop . . I think this is a theoretical idea . . that is maybe uh . . suggested or implied by looking at the idea . . (1) Item, (2) Item List, (3) Item Store, (4) Item Infinite Loop . . an Item Infinite Loop is the idea that supposes that there are news created items that are accessible in the same way that an item in the . . "Item Store" is accessible . . there is a "constant time" runtime performance guarantee or something like this . . according to the hardware of the computer . . and so for example . . even with an "item" represented in memory . . if the item is available in a variable . . that item can be accessed from the variable at . . uh . . very rapidly . . or maybe really as rapid as the computer processing ability is able to handle that ability . . random access memory is involved in these things of interest and yet I'm not really validated by how those ideas work since I don't have a lot of tremendous work experience in working with the hardware aspects of computer technologies . . . . an Item Simultaneous Negative Loop . . is the idea that supposes that you could have an infinite loop . . or uh . . an infinite loop that is available within a simultaneous degree of uh a calculation or something like that . . and so there are an indefinite amount of values that are accessible . . within the instant that the loop is accessed . . uh . . well I'm only hypothesizing uh from the relation of how maybe from a list . . data structure you could then consider . . uh . . a store . . which is a multidimensional list . . uh . . but if you had . . uh . . a multidimensional list . . that expanded in uh . . size for example . . expanded in size . . at any given moment . . indefinitely . . expanded in size . . indefinitely . . at any given moment . . then . . uh . . you would have the idea of a list . . that grows like . . calling .push() . . uh . . for an indefinite period . . for any given period of time . . uh . . well . . uh . . .push() uh . . is how you add items to a list . . and so the list grows continually . . but its not across uh . . the time that is observed from sequential processing . . step by step processing like in computers today . . or in . . computer hardware as I've come to understand how it works in my Macbook Pro for example . . well I'm not really sure . . but that is the idea . . that you can have access to an ever expanding list of items within a given instance of time perception or time observation . . and well . . uh . . that would give you the idea of a . . uh . . Item Simultaneous Negative Loop . . this is still a work-in-progress in terms of the naming . . for this item . . uh . . for example . . "Simultaneous" means . . occuring at once . . uh . . but that can be relative uh . . well . . I'm not exactly sure how this can be applied in practice . . I think . . that maybe you can have a computer that can at least find new directions of time or space directions to traverse . . as long as new directions are accessible then that is a way to have a . . uh . . way to have more and more uh . . item availability portals or item available places . . like indices of a list . . or the memory location position of a list . . but I'm not really sure for example uh well . . you could always find new directions . . uh . . but you have to find new directions uh . . for example . . using a device that . . uh . . postulates maybe I'm not really sure . . my thinking in this area isn't very advanced . . but I've taken notes on this topic before in a previous development journal entry . . [November 16, 2020]

Service . . I think a service can be anything . . I'm not really sure . . for example . . a service was inspired by the idea of using a firebase . . service such as the realtime database service . . well . . uh . . services . . uh . . also come with the idea of a service account . . suggesting that the uh . . service . . and the account are relying on one another . . hmm . . I'm not very uh . . advanced in thinking about this item . . for example . . uh . . service, service account . . maybe these are like . . network protocol, network protocol connection . . maybe for example . . network protocol could be the item that is used as the name and not . . service . . and so for example . . the connection is uh . . an account that is -_- . . well . . I'm not really sure to be honest . . I uh . . I uh . . like the name . . service . . and I also like the name . . service account . . these names seem to be helpful when thinking about what it means to have an account . . what it means to belong to a group of uh . . activity monitoring abilities or activity monitoring devices or something like that . . for example . . really a person could be considered a service . . and a dog can be considered a service . . in that they provide carbon dioxide to the atmosphere in exchange for the oxygen and the food and the water and other things of interest or something like that . . a service account for a human can be a friend that comes by to visit . . and so if the friend would like to have so many units of carbon dioxide emitted into their local environment by their human friend service . . they can stand by the person and receive their regular dose of carbon dioxide . . and of course there are more things that people can do besides breathing . . there are many more abilities that can be identified as services . . being could be considered a service . . and so a service account could be another being . . and so those service and service account can be quite general structures and the naming isn't necessarily restrictive but meant to provide a guiding post for how to maybe relate one service to an account that benefits from that service . . 

Service Account . . I think a service account is relating to the idea that a service is something that can be accounted for by someone or something . . and so for example . . a person who is developing a codebase . . can be considered . . a service account that provides the service . . for . . the development of that codebase . . and so . . the service . . is considered . . the development of that codebase . . and . . the service account is considered . . the person or individual . . or maybe even computer related equipment that works alongside the person . . to . . uh . . develop . . that codebase . . and so for example . . a computer that is artificially intelligent can have uh . . certain privileges to the codebase . . that maybe . . uh . . maybe a community member who isn't involved in the codebase development process . . uh . . well . . service accounts are a way for allowing . . permission rules to be provided to each of the service accounts . . also a service account in the way it's . . named . . "service account" . . the word "account" . . really supposes a sort of . . individual nature . . and so . . even though . . a developer . . or a community member could also be considered "services" . . because they are providing services for the service of interest . . well . . "service account" . . gives the idea that they belong to the idea of possibilities where they are also in possession of certain abilities with respect to the service of interest . . and so for example . . maybe the developer of a service can access the password for publishing the codebase to a certain website or something like that . . or maybe . . the service account for the customer . . has access to certain measurements that the develop doesn't have access to . . for example . . the could have access to information relating to their account that is encrypted . . and the . . service . . nor the developer or other service accounts they are not allowed to access that information . . only the . . uh . . service account that has those permissions . . is allowed to access that information

Event Listener . . I think an event listener listens for when an event . . is . . achieved . . and so for example . . the . . keyboard event listener . . listens for when . . a keyboard event is . . achieved . . and so . . for example . . pressing the keys on a keyboard . . can be a way of achieving . . a . . "keyboard key pressed" . . event . . and . . pressing down a key . . on the keyboard . . for a long . . period of time . . can . . achieve the event . . "keyboard key is pressed down" . . well . . other events can also be of interest . . for example . . if there is a computer program that is listening for events such as . . a . . person . . walking . . across . . the road . . in a camera . . video . . footage document . . then . . the computer algorithm . . can . . say the event that "a person has walked across the road" . . as been achieved . . and . . by . . saying this has happened . . or by triggering the event . . the computer can notify . . event listeners . . who are waiting to perform an action . . when a person is caught . . walking across the road . . uh . . and so for example . . maybe . . it is interesting for there to be a record . . of how many people . . have walked across the road . . and so . . the event listener callback function . . or event listener . . event callback handler response function . . can increment . . the value of how many people . . have been . . viewed . . by the computer . . as . . having walked across the road . . 

Permission Rule . . I think a permission rule can be a program that specifies what things are performed . . and so for example . . this can be a general program . . but more specifically . . this idea of a permission rule was inspired by the idea of providing permission to access certain database elements . . or for being able to update certain database elements . . or for being able to delete or create . . certain . . database . . elements . . 


. . . 





. . . 


Item [Inspired-by-work-with-database-providers]
Item Id / Item Identifier [Inspired-by-work-with-database-providers]

Item Introduction [Inspired-by-intention-and-anticipation-on-this-document-page]
Item Description [Inspired-by-working-on-project-readme.md]
Item Benefits [Inspired-by-working-on-project-readme.md] [Additional Notes @ 3.0]
Item Features [Inspired-by-working-on-project-readme.md]
Item Limitations [Inspired-by-working-on-project-readme.md] [Additional Notes @ 2.0]
Item Additional Notes [Inspired-by-working-on-project-readme.md]

Item List [Inspired-by-work-with-database-providers]
Item Store [Inspired-by-work-with-database-providers]
Item Simultaneous Negative Loop [Inspired-by-extrapolation-of-(1)-item-(2)-item-list-and-(3)-item-store-possibilities] [Inspired-by-theoretical-hypothesis-for-the-existence-of-item-infinite-loop-accessible-at-runtime-in-any-given-instant-but-only-theory-for-now-a-computer-needs-the-hardware-to-support-this-and-I-dont-know-what-hardware-would-help-to-make-this-real]

Service [Inspired-by-work-with-database-providers]
Service Account [Inspired-by-work-with-database-providers]

Event Listener [Inspired-by-work-with-database-providers]
Permission Rule [Inspired-by-work-with-database-providers]

Network Protocol Connection [Inspired-by-work-with-network-protocol-providers]
Network Protocol Member [Inspired-by-work-with-network-protocol-providers]
Network Protocol Message [Inspired-by-work-with-network-protocol-providers]

??? [Inspired-by-work-with-computer-resource-providers] [Additional Notes @ 1.0]
??? [Inspired-by-work-with-computer-resource-providers] [Additional Notes @ 1.0]


Additional Notes:

[1.0]
I don't have a lot of experience working with computer resource providers . . Maybe there are . . more things of interest to discover . . and so . . for example . . those things could be quite useful to add to this list . . 

[2.0]
I don't know . . uh . . Limitations . . are uh . . maybe . . uh . . Thoughts . . uh . . on . . uh . . what are the restrictions of the codebase . . and so for example . . if the codebase is using . . uh . . a certain algorithm that doesn't allow for certain things to be reached . . uh . . by theoretical constraints . . maybe the algorithm . . can perform as . . a . . uh . . Big-O of . . N algorithm but is known to not be able to achieve any more performance than this . . and so . . well . . uh . . those things are things that could be stated as limitations . . and yet . . on the other hand . . uh . . do you think that maybe . . the "limitations" . . could be considered "permission rule" ? . . for example . . the codebase is only permitted to perform as well as . . uh . . a certain amount of speed or something like that or maybe achieve a certain amount of memory consumption uh . . within some given constraints ? hmm . . those are kind of long to describe maybe . . for now the term "limitations" . . doesn't feel so bad . . on my initial thoughts and feelings . . well for example the term . . "limitations" . . uh . . well . . uh . . I suppose maybe it's related to the "permission rule" . . and the . . uh . . rule is permitted by theoretical constraints that are measured by the development team or the people involved in the project . . and so . . I suppose . . this could be considered . . permisison rules for the codebase . . and not necessarily permission rule . . for the . . community . . and so maybe . . this could be specified . . in another format . . that takes this message into account . . 

[3.0]
Benefits . . benefits . . seem like . . "beliefs" . . uh . . what are the beliefs of the community or the project development team or the volunteers or the service accounts in terms of what they believe to be the benefit of this project . . uh . . 

Belief . . is not really specified in terms uh . . that I'm familiar with in terms of the project . . constants & variables specification that I've been looking at so far . . 

Beliefs . . thoughts . . ideas . . motivations . . inspirations . . ideals . . constant bafflings . . hoorays . . achievements . . commendments . . surprises . . 

surprises . . spontaneous . . limitations ? . . limiting beliefs ? . . empowering beliefs ? . . empowering beliefs . . beliefs that uplift the project community . . beliefs that uplift the project community . . hmm . . high spirits . . encouragement . . creativity . . creativity . . sparking . . creativity . . encouraging . . 

Anticipation . . Creative . . Anticipation . . Creative Anticipation . . Anticipation to create . . Anticipation to . . create

hmm . . Anticipation to benefit . . anticipation to have ease of use . . anticipation to have convenience . . anticipation to have less restrictive permission rules . . anticipation to have . . less restrictive permission rules . . anticipation to have less restrictive permission rules on the ability to create . . 

well . . maybe that's one way of interpretting "beliefs" . . or . . "benefits" . . but that's highly judgmenetal for example . . those are maybe my own personal beliefs . . 

. . . 

"create" "anticipation" "delete" "permission rule"

. . . 

and so for example . . when you see that the program has benefit (1) . . benefit (2) . . benefit (3) . . benefit (4) . . and so on . . then you are able to anticipate . . taht . . "woah . . this may be a way to have access to those things that I may or may not have been able to do before . ." . . something like that . . I'm not sure . . uh . . a belief . . uh . . permission rule . . deleting permission rules . . 

"delete" "permission rule" 

. . . 

that's maybe one way to say a belief . . since if you are permitting yourself to believe something . . maybe it is a way of removing constraining rules . . that would otherwise . . prevent you from doing . . or being a part of that group of things . . or thinking in that sort of way or uh . . performing some kind of activity . . and so if you believe in yourself . . maybe that is like removing . . uh . . permission rules that would maybe otherwise have you think that you're not able to do something . . uhm . . well . . that's maybe one way of interpretting this topic . . another way of interpretting the way in which this should be added to this project . . is maybe finding uh . . other uh . . concepts . . for example . . maybe . . the concept uh . . "belief" . . should be added to the . . uh . . project constants & variables . . but I'm not really sure . . it's not a big deal for me at this time . . I'm not really sure but it does matter I think . . it does matter I think . . for example . . now that we have . . 

"delete" "permission rule" . . 

we can hypothesize . . other concepts like . . 

"create" "permission rule" . . 

"update" "permission rule" . . 

"get" "permission rule" . . 

. . 

well . . which uh . . which . . sorts of things does this suggest for our document ? . . 

[Delete-Permission-Rule] What was the blockade before that you were facing? that this project now removes from your shoulders?
[Create-Permission-Rule] What would you like to block now that you have this project?
[Update-Permission-Rule] What updates would you like to have to the permission rules you currently have, now that you have this project?
[Get-Permission-Rule] What do you see is blocked because of this project?



[Delete-Permission-Rule] Project Benefits: Why Use This Project To Solve Your Problem?
[Create-Permission-Rule] Project Benefits: Why Use This Project To Create New Problems?
[Update-Permission-Rule] Project Benefits: Why Use This Project To Update Progress on Existing Problems?
[Get-Permission-Rule] Project Benefits: Why Use This Project To Learn About Existing Problems?

. . . 

[Codebase-Permission-Rule] Project Limitations:




. . . 

(1) Text Search Provider
(2) Database Provider
(3) Asymmetric Cryptography Provider
(4) Network Protocol Provider
(5) Cryptographic Hash Function Provider
(6) Computer Resource Provider
(7) Artificial Intelligence Provider

. . . 





[Written @ 0:46]

### Project Algorithms

Get
Create
Update
Delete
DoesExist // ??? Should this be there, doesn't "Get" satisfy this possibility?
Initialize / Start
Uninitialize / Stop

### Project Constants & Variables

Item
Item Id / Item Identifier

Item List
Item Store
Item Simultaneous Negative Loop

Service
Service Account

Event Listener
Permission Rule

Network Protocol Connection
Network Protocol Member
Network Protocol Message

. . . 

### Project Data Structures
[previously called "topic sites"]
[now called "project data structures"]

Algorithms
Constants
Data Structures
Infinite Loops
Moment-Point Algorithms
Variables
etc.

### Project Infinite Loops

Simultaneous
Sequential

### Project Moment-Point Algorithms

// ??? - I haven't thought of these very much

### Uncategorized Thoughts

Density
Intensity

Immediate
Latent

Spontaneous
Delayed

Intention
Anticipation

. . . 

Consumer // Get, Create, Update, Delete | Area of Interface
Provider // Event Listener | Area of Influence
Usecase // Permission Rule | Area of Exchange

. . . 

Compiler
Precompiled
Compiled
Miscellaneous
Codebase
Project

. . . 

Emotions
Feelings
Thoughts
Desires
Beliefs
Benefits
Description // Item Id
Title // Item Id
Limitations
Features

. . . 

[] Project Introduction // [2.0]
[] Project Intention Description // [1.0]
[] Project Anticipation Description // [3.0]

[] Project Title
[] Project Description
[] Project Benefits
[] Project Features
[] Project Limitation
[] Project Additional Notes

[] Project Contact Information
[] Project News Updates
[] Project Community Restrictions & Guidelines

[] Project Codebase
[] Project Consumer Resource
[] Project Provider Resource
[] Project Usecase Resource

[] Project Community & Volunteers

[] Project Usecase Provided By Related Community Projects

. . . 

[Intention] Project Intention Description
[Anticipation] Project Anticipation Description

[Identifier] Project Title
[Identifier] Project Description
[Service-Account-Permission-Rule] Project Benefits // 
[Network-Protocol-Connection] Project Features // connect to new possibilities
[Service-Permission-Rule] Project Limitations // acknowledge the present service limitations
[Network-Protocol-Message-List] Project Additional Notes // additional messages to the community

[Network-Protocol-Connection] Project Contact Information
[Event-Listener] Project News Updates
[Network-Protocol-Member-List-Permission-Rule] Project Community Restrictions & Guidelines

[Service] Project Codebase
[Service-List] Project Consumer Resource
[Service-List] Project Provider Resource
[Service-List] Project Usecase Resource

[Network-Protocol-Member] Project Community & Volunteers

[Netowrk-Protocol-Connection] Project Usecase Provided By Related Community Projects



. . . 


[Intention] Project Intention Description
[Anticipation] Project Anticipation Description

[Item-Identifier] Project Title
[Item-Identifier] Project Description
[???] Project Benefits // not referenced from the project constants, variables and other thoughts that are uncategorized or categorized so far
[???] Project Features // not referenced from the project constants, variables and other thoughts that are uncategorized or categorized so far
[???] Project Limitation // not referenced from the project constants, variables and other thoughts that are uncategorized or categorized so far
[???] Project Additional Notes // not referenced from the project constants, variables and other thoughts that are uncategorized or categorized so far

[Network-Protocol-Connection] Project Contact Information
[Event-Listener] Project News Updates
[Permission-Rule] Project Community Restrictions & Guidelines

[Service] Project Codebase
[???] Project Consumer Resource
[???] Project Provider Resource
[???] Project Usecase Resource

[Network-Protocol-Member] Project Community & Volunteers

[Netowrk-Protocol-Connection] Project Usecase Provided By Related Community Projects


. . . 

(2) Secondary thoughts:

[Intention] Project Intention Description
[Anticipation] Project Anticipation Description

[Item-Identifier] Project Title
[Item-Identifier] Project Description
[???] Project Benefits
[???] Project Features
[???] Project Limitation
[???] Project Additional Notes

[Network-Protocol-Connection] Project Contact Information
[Event-Listener] Project News Updates
[Permission-Rule] Project Community Restrictions & Guidelines

[Service] Project Codebase
[???] Project Consumer Resource
[???] Project Provider Resource
[???] Project Usecase Resource

[Network-Protocol-Member] Project Community & Volunteers

[Netowrk-Protocol-Connection] Project Usecase Provided By Related Community Projects


. . . 

(1) Initial thoughts:

[Intention] Project Intention Description
[Anticipation] Project Anticipation Description

[???] Project Title
[???] Project Description
[???] Project Benefits
[???] Project Features
[???] Project Limitation
[???] Project Additional Notes

[Network-Protocol-Connection] Project Contact Information
[Event-Listener] Project News Updates
[Permission-Rule] Project Community Restrictions & Guidelines

[Service] Project Codebase
[???] Project Consumer Resource
[???] Project Provider Resource
[???] Project Usecase Resource

[Network-Protocol-Member] Project Community & Volunteers

[Netowrk-Protocol-Connection] Project Usecase Provided By Related Community Projects


. . . 

- (1) Project Anticipation Description
- (2) Project Intention Description

- (3) Project Title
- (4) Project Description
- (5) Project Benefits
- (6) Project Features
- (7) Project Limitations
- (8) Project Additional Notes

- (9) Project Contact Information
- (10) Project News Updates
- (11) Project Community Restrictions & Guidelines

- (12) Project Codebase
- (13) Project Consumer Resource
- (14) Project Provider Resource
- (15) Project Usecase Resource

- (16) Project Community & Volunteers
- (17) Project Usecase Provided By Related Projects


. . . 


[1.0]
 this contains information on the intention of the document . . to educate on the project . . is a common . . intention . . I imagine . . to educate . . on the possibility of . . a solution . . to certain dilemmas . . or certain problem statements . . or cognitive inquiries . . or something like that . . 

[2.0]
this could maybe replace the intention, anticipation section . . 

[3.0]
this contains the table of contents . . that may be anticipated by the reader of the document


. . . 


Previous Naming Idea: Topic Data Structure Instance List

* Title
* Description
* Features
* Benefits
* Limitations
* Codebase
* Community
* etc.


[Written @ 0:39]


I had a few thoughts today . . 

(1) a framework for consuming (1.1) data structures and (1.2) providers and producing algorithms for getting, creating, updating, deleting, initializing / starting and uninitializing / stopping . . those data structures

* consume data structures
* consume the organization of those data structures and how they relate to one another
* consume providers such as database providers



(2) 




a user interface 


