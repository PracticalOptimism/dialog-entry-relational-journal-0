


# Development Journal Entry - November 16, 2020

[Written @ 19:16]

Codebase Architecture / Codebase Directory and File Tree Structure
Movies
Video Games
Books
Paintings
Biological Studies
Experimentation
Spirituality Related Topics
Crystal Technology
Design
User Interface / User Experience Technology
Material Sciences
Biological Engineering
Physics
Chemistry
Engineering
Behavior Analysis
Social
Society Studies
Philanthropy
Sexual Related Topics
Pornography
Podcast Organization Structure
Conversation Organization
Personal Thinking Directory Matrix Structure
Membrane Organization
Planetary Colonization Studies
Extraterrestrial Insectography
Glass Making Sciences
Mathematics
Gigantomastia Studies
Nipple Slip and Areola Slip Studies
Vagina Slip and Butthole Slip Studies
Macromastia Studies
Female Humans Studies
Non-Binary Humans Studies
Male Human Studies
Hetersexual Humans Studies
Homosexual Humans Studies
Human Breast Cancer Studies
Human Cytographic Scan Techniques Studies
Human Social Development Studies

. . . 

and more 


[Written @ 16:23]

### Item Simultaneous Negative Loop 

// Item Infinite Loop?? I'm not sure what this hypothetical device should be called . . I'm not sure . . sorry  .. -_- . . well the name that's there now is still unreasonable and I'm not really sure what the topic is about . . it has something to do with availability but is also an infite loop in the direction of now and not really in the direction of time step observations or something like that -_- -_- . . huh . . it's not necessarily an infinite loop . . it only needs to have a direction that's "negative" in the sense that . . it's uh not uh . . uh . . like uh . . a time-stop observable thing or something like that . . and so it can maybe be a list with uh . . the negative uh uh direction for a variety of negative directions . . that uh . . I don't know -_- . . haha . . but it doesn' tneed to be an infinite loop it seems . . uh . . Negative Loop uh . . maybe means that you at least need to be able to find new . . non-positive uh . . time directions . . for example . . if you can find the direction of time in space video games . . you can maybe have a way to make new directions have a way to have a list or at least 1 item or something is maybe enough . . but you need at least to find new directions that are non-positive or something like that . . simultaneous means they are existing at the same time which makes it so uh . . it is like uh . . very fast . . to retrieve information but that could also be uh . . a parameter . . that can vary to have a practical uh . . device or something . . since maybe the non-negative time direction measurement device is something that isn't uh . . very fast at directing between the simultaneous directions or something like that . . the simultaneous negative time directions . . one example that I think of . . uh . . is if you have one person in uh . . China for example . . and another person . . in . . the United Kingdom . . then they can experience time independently of one another . . and simultaneously . . uh . . but you also need to be able to look at more than these two individuals to represent your information . . and so you could maybe for example . . look at the other people of the world . . one at a time . . or maybe for any particular individual . . you could . . uh . . maybe look at that individual's uh . . body . . uh . . body regions . . for example . . the threads of the clothes they wear . . and for any particular thread you may want to look at the other things you can infer from those things . . maybe the thread color is inferable from uh . . your uh . . spontaneous negative time recurser program system to look at those new directions of time which are related to uh simultaneous existences being available . . uh . . for uh . . well uh . . being available for example for information recording purposes-_- . . recording information uh . . or relating information for providing a recording system or something like that . . -_- . . -_- . . hmm . . I'm not really sure how to think about this at this time . . I think I'm having a tough time relating uh . . simulaneous events . . like one thread being green and one thread being blue . . uh . . well . . and especially relating them uh . . since maybe here are so many possible combinations of uh . . how these threads are oriented . . uh . . and for any given shirt . . uh . . and uh . . maybe even these things change right ? . . -_- . . strange I'm really not sure . . changing in the positive time direction seems like the problem since I am normally used to thinking of positive time . . but in a color space maybe the time is measureable . . in other ways . . I'm really not sure what that means . . uh . . well for example . . uh . . space and time are positive direction from my perspective . . and yet . . in the perspective of a probable version of myself . . maybe . . uh . . -_- . . it seems like those could be positive versions as well . . and yet . . their direction for example . . a version of myself with . . I'm not sure . . are those directions negative for my observation ? . . hmm . . well -_- . . I'm taken notes about this topic in a way that seems uh . . well . . hmm . . I wrote notes uh . . hmm . . well . . uh . . hmm . . -_- . . hmm . . hmm . . I'll be back another time to maybe continue this topic conversation . 


[Written @ 15:59]

Item Simultaneous Negative Loop

Is an idea that relates to having an item representation where the item is like an instaneous finstantous loop that uh someone maybe provides a memory storage system that is space-time recurively available or something like that but at the same time you need to not have a constraint on the values -_____________________


[Written @ 14:11]

. . . 

hmm . . maybe . . one way of describing . . the data structure . . of the . . readme.md . . file so . . far . . is to say that . . there are . . types . . or . . data types . . or data structure . . types for each of the sections . . of the project readme . . and so . . for example . . each of the sections . . serves a particular purpose . . or something like that . . 

well . . uh . . my thinking . . uh . . process has been related to how to think about . . uh . . well . . uh . . hmm . . (1), (2), (3), (4) . . as being . . a . . uh . . set of . . things . . that you can use . . to uh . . find out what sort of information that you can use . . to . . uh . . be . . comprehensive . . or complete in describing . . the nature of the document . . in some respect . . or something like that . . 

for example . . uh . . in previous . . readme.md . . files . . project title . . is quite uh . . a . . natural . . thing to have . . and so . . normally . . uh . . if you look around in the community . . of . . uh . . open source software for example . . you will a lot of the time . . see . . a title . . uh . . for the project . . and you will also maybe often see a description of the project . . 



[Written @ 14:05]


Readme.md

Project Title
Project Description
Project Benefits
Project Features
Project Limitations
Project Additional Notes

Project Contact Information
Project News Updates
Project Community Restrictions & Guidelines

Project Codebase
Project Consumer Resource
Project Provider Resource
Project Usecase Resource


Project Community & Volunteers
Project Community Related to the Project Usecase

Project License Agreement

. . . 

https://opensource.org/licenses/MIT


### Uncategorized thoughts (I haven't thought of a name for these topics yet . . )

* Intention
* Immediate
* Anticipation
* Latent
* Simultaneous
* Sequential

### (1) Topic Data Structure Instance List

* Title
* Description
* Features
* Benefits
* Limitations
* Codebase
* Community
* etc.

### (2) Topic Algorithms

* Get
* Create
* Update
* Delete
* Initialize
* Uninitialize
* Start
* Stop

### (3) Topic Data Structures

* Service 
* Service Account
* Item
* Item List
* Item Store
* Item Simultaneous Negative Loop 
* Event Listener
* Permission Rule
* Network Protocol Connection
* Network Protocol Member
* Network Protocol Message

### (4) Topic

* Algorithms
* Constants
* Data Structures
* Infinite Loops
* Moment Point Algorithms
* Variables
* etc.


[Written @ 13:58]

I'm not really sure if the . . service . . and . . . . topic . . are the same thing . . for example . . in the troy architecture . . documentation that I've written so far . . it seems that . . a topic . . is uh . . like a data structure that contains . . uh . . "algorithms", "data structures", "constants", "variables", "infinite loops", "moment point algorithms" and so on . . uh . . as those other constructs are discovered or created or something like that . . 

well . . I'm not really sure . . if . . a . . "service" . . is also . . a . . "topic" . . and really I haven't thought too much about the conditions that a . . "service" . . has . . or holds . . uh . . in general . . I think . . for now in any case . . that the concept of a service . . really fits anything . . like . . the concept . . of an . . "item" . . can fit . . anything . . for example . . 

uh . . when you have . . uh . . well . . I suppose maybe . . a . . "service" . . and an . . "item" . . can maybe be considered to be distinguishable . . uh . . since with a service . . there is . . a . . uh . . topic . . called . . "service account" . . well . . uh . . "service account" . . uh . . well . . uh . . something like that . . uh . . I'v really treated . . uh . . them . . kind of . . separately . . for now . . for example . . uh . . I really haven't thought of a service account requiring a service . . to be associated with -_- . . but I suppose that makes sense . . uh . . . uh . . hmm

. . 

right . . so I'm really not sure if a . . "topic" . . uh . . is always a . . "service" . . or . . if a "service" . . is . . always . . a . . "topic" . . I haven't uh  . . considered this topic too much :O . . and so that's why there is a question mark . . uh . . because I am indicating in my notes that I am uncertain about this topic . . 


[Written @ 13:54]

Here are notes that were . . not included . . in the previous . . 2 transcripts . . which are  . . . additional notes that were taken . . on the page . .


  _________
/          \  Same
|  Service |   thing ?
|  Topic   |
 \________/

ALgorithms
Data Structures
Constants
Variables
Infinite Loops

Get
Create
Update
Delete
Initialize / start
Uninitialize / stop
-
Service
Service Account

Item
Item List
Item Store

Event Listener
Permission Rule

Network Protocol
      Connection
Network Protocol
      Member
Network Protocol
      Message


[Written @ 13:47]

Here are notes that I have written in my physical paper journal notebook . . that I'm transcribing here . . to share my thoughts on how to organize the project readme.md file . . so far . . these are experimental thoughts and maybe things can be improved or updated or something like that . . I'm sorry


Readme.md

Project Title
Project Description
Project Benefits
Project Features
Project Limitations
Project Additional Notes
-
Project News Updates
Project Community Restrictions & Guidelines



Project Codebase
Project Consumer Resource
Project Provider Resource
Project Usecase Resource


Project Community & Volunteers

Project Contact Information

Project Community Related to Digital Currency Usecase

Project License Agreement

. . . 

The notes above are . . a transcription of what I've written in my . . notebook . . without the margins . . and additional . . comments . . written on the . . uh . . side of the . . uh . . page . . or on the side of the list . . for the readme . . or something like that . . uh . . well . . maybe . . here is another . . transcript . . that is . . more . . related to how the page appears . . including . . the notes on the left side of the vertical line to the far left of the page . . 

Readme.md

[network protocol message]        Project Title
[network protocol message]        Project Description
[network protocol message list]   Project Benefits
[network protocol message list]   Project Features
[network protocol message list]   Project Limitations
[network protocol message list]   Project Additional Notes
                                  -
[event listener list]             Project News Updates
[permission rule list]            Project Community Restrictions & Guidelines



[service]                         Project Codebase
[service list]                    Project Consumer Resource
[service list]                    Project Provider Resource
[service list]                    Project Usecase Resource


[service account list]            Project Community & Volunteers

[network protocol connection]     Project Contact Information

[network protocol member list]    Project Community Related to Digital Currency Usecase

[network protocol message]        Project License Agreement








