


# Development Journal Entry - November 24, 2020

[Written @ 23:15]

Thoughts on the project web browser service website user interface / user experience



hmm . . over the past few days I've been thinking about the topic of the user interface as it relates to the project document structure that I've been working on for the past few days . . the project document structure has been applied to trying to organize the project readme.md file . . based on . . some of the things that I've been learning . . and am still continuing to learn more about . . as I play theoretical thought experiment games in my mind and sometimes on paper notebook to see what would be really a good way to organize the project readme.md file . . 


. . . 


The uh . . well . . let's see . . hmm . . one of the items in the . . project document structure comes to mind . . quite strongly . . or I think very highly of it . . in terms of . . having uh . . a really impactful experience on how I think . . uh . . about the organization structure document . . 

. . 

News Updates

. . 

News Updates . . or Project News Updates as it's written in a lot of the documentation that . . I've . . written so far . . on this topic . . uh . . well . . News Updates . . are . . uh . . 

. . 

News Updates . . uh . . well . . you can have . . news . . updates . . about . . (1) the project service . . or the project item of interest . . or something related to the project in general . . and . . (2) . . the project community . . the network of uh . . people . . or uh . . volunteers . . or . . uh . . community members in general . . that are involved in the project in some regard . .

. . 

Well . . hmm . . I'm still having to think about this topic . . of . . News Updates . . in terms of . . really what uh . . -_- .  hmm . . I'm not really sure . . how to wrod my sentences here . . so maybe i'll try another approach . . 

. . 

hmmm . . on the one hand . . I'm trying to write my sentences to sort of . . uh . . avoid . . theoretical generalizations of which I'm not really aware of the . . uh . . uh . . "News Updates" . . idea being applied . . to . . and on the other hand . . I'd like uh . . to uh . . maybe uh . . also hint at those general ideas . . while also being maybe down to earth or more practical with my presentation sentences or something like that . . 

. . 

uh . . hmm . . okay . . 

. . 

Yesterday, on November 23, 2020 . . I took some notes . . uh . . random notes . . on an idea that came to mind that day . . uh . . well . . uh . . the idea . . uh . . well . . I uh . . guess it wasn't completely random since for example . . I had . . been thinking about the topic of . . "Anticipation" . . uh . . as uh . . part of the . . project document structure . . uh . . "Project Anticipation Descirption" . . is a part of the document structure that I've been thinking about for the past few days . . uh . . well . . uh . . I have also been thinking about . . a . . "User Interface" . . uh . . for the web browser project service website . . uh . . and so for example . . uh . . how could you apply . . the . . project document data structure . . to the . . uh . . interface . . of the . . web browser website . . 



. . . 

Project Document Data Structure
[Copy-Pasted from November 22, 2020 @ 4:43]


* (1) Project Intention Description
* (2) Project Anticipation Description

* (3) Project Title
* (4) Project Description
* (5) Project Benefits
* (6) Project Features
* (7) Project Limitations
* (8) Project Additional Notes

* (9) Project Contact Information
* (10) Project News Updates
* (11) Project Community Restrictions & Guidelines

* (12) Project Codebase
* (13) Project Consumer Resource
* (14) Project Provider Resource
* (15) Project Usecase Resource

* (16) Project Community & Volunteers

* (17) Project Usecase Provided By Related Community Projects

. . . 

hmm . . 


. . . 

An Anticipation Dialog Book For User Interfaces / User Experiences . . 

. . 

My thinking in writing this . . uh . . idea . . or having thought about . . this topic . . was . . to . . suppose that . . I had information . . on . . what . . the user was anticipating . . to see . . when they visit a uh . . website . . 

And so for example . . if someone . . visits the . . ecoin project service website . . it would be very cool to be able to satisfy a lot of the anticipations . . of how . . that program . . ought . . or . . should . . behave . . uh . . to some degree . . and so for example . . the experience . . of using the website . . ought not to feel so frustrating . . uh . . and the experience should be . . within the anticipation . . of the user . . and so for example . . uh . . well . . if you had . . information one what people are anticipating . . like maybe . . this button should be placed here . . or that text field ought to be larger to read the text or to click on the text field area . . or maybe . . this . . text item . . ought to be in bold face . . so that information stands out more . . or maybe . . this or that . . type of information . . whether it's contact information . . or . . maybe it's . . uh . . additional . . resource . . information . . like . . scientific references . . or . . uh . . document references to the . . uh . . talked about . . resources . . or the . . uh . . providers . . or . . provider resources . . for that . . particular . . uh . . group of text . . or for that . . particular item that's in the document page . . or that's on the document page . . uh . . 

well . . users can expect a lot of things from a website . . and . . it may not be clear what those expectations are . . if there was a book on that topic . . that would be very useful . . I myself . . have not personally done any research in this area of . . a book . . that provides a complete . . guide on what users expect . . uh . . from . . a news source . . like a website . . or uh . . maybe even a uh . . hmm . . I guess . . hmm . . I'm not sure . . it's not like I haven't done any research at all really . . but the idea that it would be really cool to have a resource that provides a guide for all the information that a user could want to expect . . or could come to expect . . uh . . maybe that is quite a cool . . uh . . book . . or quite a cool resource to look into . . but . . uh . . I guess . . uh . . I've uh . . done a lot of . . peripheral research in terms of . . uh . . well . . having personal experience in . . uh . . reading uh . . documents like . . uh . . web pages . . uh . . and uh . . reading uh . . pages like . . readme.md files for software projects on github . . or on gitlab . . uh . . reading . . uh . . scientific papers . . reading . . books . . reading . . magazines . . articles . . reading . . movies . . uh . . movies . . uh . . reading movies . . maybe is like readina a book in some sense as well . . uh . . for example . . uh . . there . . are . . often times . . titles . . and descriptions . . for the movie . . just as there are often . . titles . . and descriptions . . for books . . 

. . 

. . 

Well . . uh . . hmm . . hmm . . 

. . 

Okay . . well . . the idea uh . . (10) Project News Updates . . in the project document data structure . . really . . stands out to me . . 

. . 

(10) Project News Updates . . is a reflection of . . Event Listeners . . from the . . Project Constants & Variables . . uh . . section . . of the . . uh . . project . . uh . . uh . . I guess . . uh . . Project . . Document Structure . . hmm . . I'm not really sure what to call this . . I uh . . maybe uh . . well . . I'm not sure if I should use the term . . "Troy Architecture" . . so much . . anymore . . since . . uh . . maybe the . . uh . . principles and ideas . . are . . uh . . really uh . . quite . . uh . . more general . . than . . the scope . . in which I was originally thinking about . . the . . meaning . . of . . the . . "Troy Architecture" . . the . . "Troy Architecture" . . was initially thought about in terms of how to organize software programs . . uh . . in terms of a file-based-text-based programming codebase that's similar to the file-directory-hierarchy ordering that we have today . . uh . . that's uh . . widespread in the computer programming world . . uh . . 

hmm . . well . . the Troy Architecture . . really uh . . was maybe a precursor . . uh . . to the thoughts that I started to develop . . later . . uh . . well . . uh . . uh . . the "Topics" and "Topic Sites" . . uh . . are now considered . . "Project Data Structures" . . and I think that's where . . the troy architecture started . . "Project Data Structures" such as . . Algorithms, Constants, Data Structures, Infinite Loops, Moment-Point Algorithms, Variables . . those . . are . . ideas that were originated from having been working on a program codebase data structure that I would then come to term . . "Troy Architecture" . . but . . very . . recently . . I've been looking at other ideas . . that I've now . . come . . to think of as . . "Project Constants & Variables" . . "Project Algorithms" . . and . . "Project Infinite Loops" . . 

. . 

hmm . . right . . so . . the . . "Troy Architecture" . . isn't necessarily the name . . of the . . uh . . idea . . uh . . uh . . that . . uh . . I'm talking about now . . uh . . well . . uh . . maybe . . uh . . to re-iterate . . since now my thoughts are more in order . . after having written the above . . precursor . . paragraph . . that has helped me think about this topic . . more . . 

uh . . well . . the . . "Troy Architecture" . . really began . . with . . the ideas . . of using . . "algorithms", "data-structures", "constants", "infinite-loops", "variables" and "moment-point-algorithms" . . as names for the folders . . for each of the respective items of the codebase . . and so for example . . each of the . . algorithms . . for the project codebase . . for . . that particular . . "topic" . . a . . "topic" . . being something like . . "digital-currency" topic . . or . . "shopping-mall" topic . . or . . "currency-exchange" topic . . so . . the . . topics . . are . . uh . . directories . . that . . have . . these . . uh . . previously . . named . . folders . . and an example of that would look as follows

- /digital-currency
  - /algorithms
  - /constants
  - /data-structures
  - /infinite-loops
  - /moment-point-algorithms
  - /variables

. . . 

the . . "digital-currency" . . directory . . is considered . . a . . "topic" . . because of the nature of the immediately . . available . . subdirectories . . or child directories . . or children directories . . that are within that folder . . uh . . and each of those . . specifically named . . children directories . . for . . a . . "topic" . . is called . . a . . "topic site" . . and so . . "algorithms" . . is a . . "topic site" . . and "constants" . . is a . . "topic site" . . and . . so on . . and so . . forth . . uh . . well . . uh . . each of these . . topic sites . . is uh . . where the codebase for that particular . . uh . . item . . type would go . . 

. . 

- /digital-currency
  - /algorithms
  - /constants
  - /data-structures
  - /infinite-loops
  - /moment-point-algorithms
  - /variables

. . . 


- /digital-currency
  - /algorithms
    - /digital-currency
      - /get-digital-currency-description
        - index.ts
        - another-file.ts
      - /get-digital-currency-community-list
        - index.ts
        - another-file.ts
    - /digital-currency-account
      - /get-digital-currency-account-description
        - index.ts
        - another-file.ts
      - /get-digital-currency-account-community-list
        - index.ts
        - another-file.ts
    - /digital-currency-transaction
      - /get-digital-currency-transaction-description
        - index.ts
        - another-file.ts
      - /get-digital-currency-transaction-community-list
        - index.ts
        - another-file.ts


. . . 

That was one of the initial structures for . . a . . "Troy Architecture" based . . project . . 

. . . 

hmm . . well . . uh . . I was doing more research . . and started to think . . about . . other things . . like for example . . the idea was in my mind that . . (1) you should be able to create as many directories nested one inside of the other . . A tree depth . . of . . N . . for an arbitrary number . . N . . should be able to be reached . . (which is useful for clarifying more and more details about the programs capabilities by introducing more and more ideas . . that can be nested and built one on top of the other . . ) . . and (2) if you're able to nest . . the data structure as many layers or . . as many levels . . as . . you would like . . to reach . . more and more clarity . . on more and more . . parts of the codebase . . by defining those program files . . that . . should (2.1) . . be clear . . on to find . . those files that are . . 100 levels in depth . . or 100 levels . . in height . . of the tree data structure of the program directory tree . . structure . . or maybe . . 1,000 levels . . in depth or height [1] . . or the tree data structure


. . . 


(2.1) Read As Many Levels Within The Tree Surface While Being Able To Easily Navigate Through The Files and Directories for the File You're Looking For

be clear to read as many levels of the tree directory data structure of the project codebase

(2.2) Add More Files and Directories To The Tree While Maintaining The Ease Of Use For Finding Those Directories And Files

be able to always add more levels without sacrificing the clarity of the project directory to be read . . or traversed to discover files . . 

. . . 

Well those were some ideas . . while working with the troy architecture . . uh . . well . . one thing that seemed to fail . . in terms of what I had develoed so far . . is that . . uh . . you could uh . . have files within directories . . but it seemed like there would be . . a natural stopping point . . like for example . . if you have an implementation . . for an algorithm . . written in . . index.ts . . uh . . what would happen . . if . . 2 or 3 . . months from now . . or . . uh . . sometimes in the future . . you may . . want to . . extend . . the capabilities . . of this algorithm . . or provide . . for more . . things to be done . . well . . index.ts . . is uh . . well . . one thing you could do . . is . . maybe . . create . . a . . directory . . with the files . . that you would like to . . add . . to extend . . the functionality . . of . . the algorithm . . implemented . . in . . index.ts . . and so for example . . that could look like . . 

Before:

- /digital-currency
  - /algorithms
    - /digital-currency
      - /get-digital-currency-description
        - index.ts // original algorithm implementation
      - /get-digital-currency-community-list
    - /digital-currency-account
    - /digital-currency-transaction


After:

- /digital-currency
  - /algorithms
    - /digital-currency
      - /get-digital-currency-description
        - /index
          - index.ts // original algorithm implementation
          - algorithm-extension-1.ts
          - algorithm-extension-2.ts
          - etc.
      - /get-digital-currency-community-list
    - /digital-currency-account
    - /digital-currency-transaction

. . . 

uh . . well . . what if then . . you wanted to . . extend . . the algorithm extension . . that you had . . written . . one way of extending an algorithm . . is maybe . . (1) writing unit tests for that algorithm . . I've started to uh . . use the term . . "expectation definitions" . . uh . . well . . you can have all sorts of things that you . . "expect" . . from the algorithm . . (2) the performance . . capabilities of the algorithm . . if you want to measure how quickly an algorithm performs at a task . . of variable manipulation or . . looping over content . . or doing . . uh . . http . . uh . . calls . . or something like that . . maybe you want to have a file for that performance measurement capability . . and so for example . . that file could . . live alongside . . or be a sibling . . to the original . . algorithm file . . in the same directory . . uh . . as the original file . . uh . . well . . and not only is there one type of performance measurement that you can make . . but there could be new ways . . new ideas that you have . . maybe for example . . you want to measure the performance . . based on . . a particular . . runtime environment being assumed . . and so . . having particular algorithms . . for each of those runtime measurements could be particularly useful . . for your project . . (3) . . accessibility related things like . . documentation . . are really important . . and so there is yet another example . . of . . why you may want to extend . . your existing algorithm . . by providing . . additional . . "information" . . or . . additional . . "specifications" . . or . . "identification" metrics . . about that algorithm . . and you could go on and on . . and . . on and . . on . . and on . . with ideas of how to . . make an algorithm more . . uh . . believable . . in accordance to the requirements of the software by the . . community . . (4) . . one believability metric could be . . how the algorithm stands up to theoretical constraints . . and this is where the topic of . . "formal verification" . . or . . "formal methods" . . comes into play . . or uh . . can come into play . . personally I'm not very familiar with using these types of . . uh . . measurement systems for measuring . . uh . . a codebase . . ability to achieve a certain uh . . observed capability . . constraint . . or something like that . . uh . . [3.0] is a great resource . . for . . uh . . anyone who is interested in learning more about . . uh . . "theorem proving computer algorithms" . . uh . . which . . uh . . I think is related to the topic of . . "formal verification" . . where you're interested in having a computer . . verify . . that certain theoretical . . uh . . definitions . . are satisfied . . by . . computer algorithms . . uh . . or maybe uh . . for example . . being able . . to . . provide . . a . . computer-checked proof . . and so for example . . traditional . . pencil-holding mathematicians . . aren't so necessarily involved . . in checking if a mathematical proof is . . correct . 





Additional Notes:

[1]
[Tree Data Structure Height, Depth, Level]
The terminology . . "Tree height" . . and "Tree depth" . . are . . being used . . interchangeably here . . for example . . so far as I'm familiar . . I've seen . . people talking about the . . "depth" . . of a . . "tree" . . and other people . . use the term . . the . . "height" of a . . "tree" . . and yet the concept is uh . . meant to resemble to same idea . . sometimes . . I'll also use the term . . "level" . . and all 3 of these items are meant to represent the same . . sort of topic . . 

- /level-1-item-1
  - /level-2-item-1
    - /level-3-item-9
      - /level-4-item-6
        - /level-5-item-2
          - /level-6-item-1
  - /level-2-item-2
    - /level-3-item-4
    - /level-3-item-5
    - /level-3-item-6
  - /level-2-item-3
    - /level-3-item-7
      - /level-4-item-4
      - /level-4-item-5
        - /level-5-item-1
    - /level-3-item-8
      - /level-4-item-1
      - /level-4-item-2
      - /level-4-item-3
  - /level-2-item-4
    - /level-3-item-1
    - /level-3-item-2
    - /level-3-item-3

. . . 

in the example tree data structure represented above . . 

the height of the tree . . or maybe . . the maximum . . height of the tree . . is considered . . 6 . . and it is also equally as valid to consider . . uh . . the . . depth . . of the tree . . or the maximum . . depth . . of the . . tree . . to be . . 6 . . 

. . 


[2]
[Tree Data Structure, Hash Map, Dictionary, Hash Table, Table]
These terms are also interchangeable . . I've . . had the experience of having these terms used . . uh . . maybe . . interchangeably . . 

Hash Tables => JavaScript Community
Dictionary => Object C Community (iOS App Development)
Hash Map => JavaScript Community
Tree Data Structure => Mathematics Learning



