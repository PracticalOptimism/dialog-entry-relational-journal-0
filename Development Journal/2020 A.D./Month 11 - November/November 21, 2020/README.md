
# Development Journal Entry - November 21, 2020



[Written @ 23:24]


### Project Algorithms

Get
Create
Update
Delete
Initialize / Start
Uninitialize / Stop

### Project Constants & Variables

Item
Item Id
Item List
Item Store
Item Simultaneous Negative Loop

Service
Service Account

Event Listener
Permission Rule

Network Protocol Connection
Network Protocol Member
Network Protocol Message

. . . 

### Project Data Structures

Algorithms
Constants
Data Structures
Infinite Loops
Moment-Point Algorithms
Variables
etc.

### Project Infinite Loops

Simultaneous
Sequential

### Project Moment-Point Algorithms

// ??? - I haven't thought of these very much

### Uncategorized Thoughts

Immediate
Latent

Intention
Anticipation

. . . 

Consumer
Provider
Usecase

. . . 

Compiler
Precompiled
Compiled
Miscellaneous

. . . 

Emotions
Feelings
Thoughts
Desires
Beliefs
Benefits
Description
Title
Limitations
Features

. . . 

[] Project Intention Description
[] Project Anticipation Description

[] Project Title
[] Project Description
[] Project Benefits
[] Project Features
[] Project Limitation
[] Project Additional Notes

[] Project Contact Information
[] Project News Updates
[] Project Community Restrictions & Guidelines

[] Project Codebase
[] Project Consumer Resource
[] Project Provider Resource
[] Project Usecase Resource

[] Project Community & Volunteers

[] Project Usecase Provided By Related Community Projects



. . . 

### Thoughts on: (1) Location Id and (2) Network Protocol Connection Id

- These are similar concepts and since "Network Protocol Connection" is an "Item" . . "Item Id" is sufficient to place on the list
- Connection Id is suggestive of the possibility of "contact" for example such that you can "contact" a "location" or establish a "connection" to a location" and so "network protocol connection id" is sufficient from needing to write "Location Id"
- Idempotency is the idea of keeping the structure similar without having redundancy within a certain respect . . 
- Linear Independence is another topic that maybe is useful to ascribe to the relation of each of the items in the lists . . for example . . the items in the list are meant to be regarded as independent from one another to some respect . . and still be able to combine with one another . . those are the thoughts I've been having while thinking about a way to organize these thoughts . . 
- Color Theory has unique colors that can be combined with one another . . and maybe for example . . each of the items in the list are meant to be . . colors . . that can be combined with one another . . and so for example . . you can have . . an item store list . . or a service account event listener . . or maybe you can have a service account event listener list . . or maybe you can have . . uh . . a service id . . event listener . . network protocol connection . . uh . . well . . hmm . . a "service id event listener network protocol connection" seems to uh . . hmm  . . being able to connect to the codebase that is responsible for creating or updating or deleting or getting event listeners that listen specifically to the service id property . . or something like that . . hmm . . well . . to be honest . . I haven't actually thought about more than . . 2 combinations . . for example . . a permission rule list . . is a possibility . . but that possibility really has 2 parts that I've considered . . "permisison rule" is number 1 . . and "item list" . . is number 2 . . (1) permission rule . . (2) item list . . which together allows you to for example hav e. . "permission rule list" . . well . . what if you had . . 3 or more items that are combined ? . . (1) . . (2) . . (3) . . (4) . . and so on ? . . what would that look like . . 

item list . . item list . . item list . . item list . . 

This is a list of item lists . . of item lists . . of item lists . . 


item list #1:
[]

item list #2:
[
  []
]

item list #3:
[
  [
    []
  ]
]

item list #4:
[
  [
    [
      []
    ]
  ]
]

. . . 

hmm . . let's see . . well . . uh . . 

item list:
  - let list = []
item list list:
  - let list = [[]]
item list list list:
  - let list = [[[]]]
item list list list list:
  - let list = [[[[]]]]

. . . 

item store:
  - let store = {}
item store store:
  - let store = { store: {} }
item store store store:
  - let store = { store: { store: {} } }
item store store store store:
  - let store = { store: { store: { store: {} } } }

. . . 

item store:
  - let store = {}
item store list:
  - let list = [{}]
item store list store:
  - let store = { list: [{}] }
item store list store list:
  - let list = [{ list: [{}]}]

. . . 

item store:
  - let store = {}
item store store:
  - let store = { store: {} }
item store store id:
  - let storeId = { store: {} }
item store store id id:
  - let storeId = { store: {} }
  - let storeIdId = 'storeId'
item store store id id id:
  - let storeId = { store: {} }
  - let storeIdId = 'storeId'
  - let storeIdIdId = 'storeIdId'

. . . 

item event listener:
  - const eventListenerId = createEventListener(itemId, eventType, function (event) {
      console.log(event)
    })

item event listener, event listener:
  - const eventListenerId = createEventListener(itemId, eventType, function (event) {
      console.log(event)
    })
  - const eventListenerId2 = createEventListener(eventListenerId, eventType, function (event) {
      console.log(event)
    })


item event listener, event listener, event listener:
  - const eventListenerId = createEventListener(itemId, eventType, function (event) {
      console.log(event)
    })
  - const eventListenerId2 = createEventListener(eventListenerId, eventType, function (event) {
      console.log(event)
    })
  - const eventListenerId3 = createEventListener(eventListenerId2, eventType, function (event) {
      console.log(event)
    })

item event listener, event listener, event listener, event listener:
  - const eventListenerId = createEventListener(itemId, eventType, function (event) {
      console.log(event)
    })
  - const eventListenerId2 = createEventListener(eventListenerId, eventType, function (event) {
      console.log(event)
    })
  - const eventListenerId3 = createEventListener(eventListenerId2, eventType, function (event) {
      console.log(event)
    })
  - const eventListenerId4 = createEventListener(eventListenerId3, eventType, function (event) {
      console.log(event)
    })

. . . 


item store: {}
item store, event listener:
  - const eventListenerId = createEventListener(itemStoreId, eventType, function (event) {
    console.log(event)
  })

item store, event listener, id:
  - const eventListenerId = createEventListener(itemStoreId, eventType, function (event) {
    console.log(event)
  })

item store, event listener, id, id:
  - const eventListenerId = createEventListener(itemStoreId, eventType, function (event) {
    console.log(event)
  })
  - const eventListenerIdId = 'eventListenerId'

item store, event listener, id, id, id:
  - const eventListenerId = createEventListener(itemStoreId, eventType, function (event) {
    console.log(event)
  })
  - const eventListenerIdId = 'eventListenerId'
  - const eventListenerIdIdId = 'eventListenerIdId'

. . . 

item id:
  - const itemId = 'hello-world'
item id, event listener:
  - const itemId = 'hello-world'
  - const eventListenerId = createEventListener('itemId', eventType, function (event) {
    console.log(event)
  })
item id, event listener, permission rule:
  - const itemId = 'hello-world'
  - const eventListenerId = createEventListener('itemId', eventType, function (event) {
    console.log(event)
  })
  - const permissionRuleId = createPermissionRule(eventListenerId, permissionRuleType, function (permissionRuleEvaluationEvent) {
    console.log(permissionRuleEvaluationEvent)
  })

. . . 

these names are a work-in-progress (I'm not sure which one to use yet . . I haven't thought about this topic very much yet . . and yet . . "permissionRuleEvaluationEvent" seems like an okay option for now . . ):

permissionRuleEvaluationEvent
permissionRuleEvaluationPoint
permissionRuleEvaluationRequest
permissionRuleRequest
permissionRuleEvent

. . . 

one of my metrics for determining a good name is if my hands uh . . feel like backpeddling . . on the keyboard . . and so for example . . if I type a word . . then uh . . maybe I don't feel very confident in typing this word . . maybe the rule of this word doesn't provide the general intention or the general feeling that I would like to capture . . and normally the feelings is possibly trying to be positive . . or neutral in my use of language . . and so for example I'm not always interested in using negative terminology or something like that . . something that doesn't make me feel very good as a person . . and also so that way it is easier for people to assimilate for the use of their programs . . uh . . maybe assimilate it's not the best term to use in this scenario for example since it's very close sounding to the term "assassinate" . . and yet . . for example it has the proper meaning that I would like to relay . . but that is an example . . for example of why using the word "assimilate" wasn't the proper or best choice for me in any case . . and yet it's still a good word to use if I want to mean "understand" or "comprehend" or "grasp" or "grok" or . . "acquire meaning from" . . or something like that . . and yet it is still a good word but it's not necessarily the word that I wouldn't have not already have pressed the backspace key . . so as to reset the rhythm and rhyme of the sentence . . the feeling of how I feel to the words that are written play an important role for me at least . . and for example . . it's maybe possibly like a video game where you can try many different styles of character spellings to see what is the proper fit for the general culture and community that could possibly also be involved in thinking about the same notation for similar applications or for similar problems they are facing and so if you apply your emotions to track you along the way for the proper meaning and uh . something then maybe the games can be fun and meaningful for the team

-_|

. . . 


