

# Development Journal Entry - November 10, 2020


[Written @ 14:57]

. . 

. . 

. . 




[Written @ 14:51]

[Written @ 13:58]


. . the development of the readme file . . could be very interesting . . I'm thinking of . . books . . and research journals . . and movies . . and news articles . . and websites . . 

uh . . well . . video games could also be a thing . . 

a README.md . . file . . is very useful . . for uh . . well . . uh . . 

well . . for example . . adding numbers . . to the table of contents . . seems to really help in being able to quickly navigate . . through the . . website README.md . . file . . and so . . you can have an intuition about where you are in the page . . without needing to scroll up to the table of contents . . or uh . . well . . you get an order of the contents . . uh . . with your mental reasoning patterns about numbers already . . something like that . . 

for example . . 1 . . 2. . . 3. . . 4. . . 5. . 6. . . well . . without having to think about it . . you can probably . . guess which number could come next . . or would come next . . this idea . . that the familiarity with numbers . . is uh . . well . . really cool because for example . . even for a new person . . who visits the website . . a new person who visits the README.md . . file . . or the README.md page . . for the first time . . they can be more familiar with your program . . already because they have an intuition about numbers . . and numerical organization schemes . . like the . . uh . . addition . . and subtraction . . and ordering of numbers and things like this . . 

well . . it would be a reasonable thing to expect this familiarity anyway . . it seems like at least . . if there isn't a familiarity . . one could learn . . uh . . very rapidly . . how these organizations of . . uh . . ideas . . or symbols . . go . . or something like that . . 

uh . . 

well . . so . . 1. . 2. . 3. . 4. . . well . . uh . . because someone is already familiar with these ideas . . the . . presentation page . . or the README.md . . file . . is really . . losing its jarring nature . . of being . . consumed up front . . fro example . . maybe someone could be nervous about the nature . . of the content . . in terms of . . uh . . well . . what is the content about . . how do I . . read this . . content . . uh . . well . . aren't I here for only a little bit . . don't I have other things to do ? . . well . . why should I read this content ? . . well . . having a numbered list . . can quickly introduce someone to the nature of the content . . and then . . if any of the content items look appealing or look directed toward the interest of the individual . . then . . that person . . or that individual . . or that computer program . . or uh . . diagram of reflexes and actions of otherworldly dimensions and syntax . . can be able to read through the project introduction for only a few moments . . without needing to think about the details and already be familiar . . with the content . . 

. . well uh . . if you are familiar with numbers . . operating through the list of numbers . . you can operate through with your focus . . in your own particular intered order and not necessarily in . . uh . numerical . . natural number ordering schemes or something like that . . 

and so . . naturally the eye can evaluate from . . . . . . uh . . . . . . whatever . . t owhatever . . and uh . . well . . if you ever need to do a basic scan . . of the material . . where you use . . the . . uh . . naturally . . uh . . defined order . . or the order of . . uh . . 1. . 2. . 3. 4. . . . . uh . . . if that's a natural order . . well . . you can always . . rely on that . . if you want to quickly . . be . . an expert . . in the topic of interest . . but not be required to read the specific . . uh . . individual content items or something like that :O . . something like that . . I'm not sure if that's the idea that I would uh . . well . . you can uh . . well . . uh . . for example . . somehting like that is along the lines that I am thinking about this README.md file at this time . . it can be used uh . . well . . maybe it's like a computer prgram . . and so . . other uh . . computer programs can read it . . and uh . . other people . . can . . read it . . and uh . . well . . it can be . . in such a way . . to be . . completed . . or to be . . compilable into an understanding for either . . computer . . or . . person individual . . or something like that . . and so . . the human being can compile the . . readme.md . . file . . to something that . . is useful for them to use . . uh . . by . . reading . . or skimming . . the . . table of contents or something like that . . 

uh . . well . . the table of contents is another topic . . I'm not very familiar in the origin . . or the literature in how they were started but they are quite interesting tools . . for example . . a table of contents . . uh . . well . . in programs today . . we are writing table of content folder-file systems . . or folder-directory structures . . when writing programs . . and so . . . uh . . well . . that's an interesting observation . . this topic of hierarchy is space-time organization . . and so . . you can evaluate . . space . . at many . . dimensions . . or at many . . levels . . of hierarhcy . . or structure . . you can be . . in one . . codebase . . and you can . . open the door . . of a folder or something like that . . and the codebase in that folder . . can be structured . . uh . . well . . uh . . well . . it can be . . uh . . written in another language . . for example . . if you are opening one directory . . maybe it's written in . . javascript . . and if you open another directory . . maybe it's written in . . golang . . or rust . . or python . . or the myriad of other programming languages available today . . 

Well uh . . it's interesting that still by using a table structure . . or a table of contents structure . . of having . . a name associated . . to the reference of some content . . for example . . the name of a directory . . being a reference to a list of files . . and the . . file . . being a reference . . or rather . . uh . . the file name . . and the . . uh . . file . . extension type or something like that . . uh . . well . . a file name like . . hello-world . . and a file extension type like . . txt . . or dot text . . or . . ".txt" . . is . . uh . . . producing . . a . . table of contents . . item like . . . "hello-world.txt" . . uh . . is a lot of the text editor environments . . uh . . that are here today . . uh . . well . . uh . . I'm not sure . . 

Uh . . well . . at least . . uh . . uh . . so . . the file . . is a reference . . to further more files . . uh . . I'm sorry . . rather . . the file name . . uh . w. when you click on it . . can . . uh . . . be a reference . . to file content . . uh . . in another . . "dimension" of space . . or . . another . . quote-on-quote . . dimension . . of . . space time observation abilities or something like that . . -_- . . I'm not sure . . uh . . well . . uh . . th eidea . . is like . . you can  . . click on the file name . . uh . . in a directory tree of a text editor . . uh . . or . . you could for example . . hover over it with your mouse . . and that can be another scheme for opening . . the . . file in some way . . or maybe imagine that the file could open in another way . . and then . . that possibility could also be programmed . . well . . uh . . maybe for example . . the text program . . uh . . or rather. . . the file contents . . are represented by changing the colors of the screen on the computer . . to be . . uh . . like . . . random . . cubes . . or uh . . random blocks of colors . . that . . uh . . flash on and off according to some scheme of patterns that are meant to be interpretted by weird people . . [I'm sorry . . :O ]

. . 

. . 

. . 

Okay . . I'm not really sure . . where to go with this . . uh . . well . . I'm working on the project README.md . . file . . for . . the https://gitlab.com/ecorp-org/ecoin project and the codebase is inspiring me to think more uh considerately about the structure of the READEME.md file


* The table of contents is like a directory tree

* A directory tree for covering the project introduction
* A directory tree for introducing the project
* A directory tree

* How to
* What to do
* Where to go
* 

* what is this about
* what is it for
* what would I want to do with this in this theoretical scenario
* ideas for other projects


* ecoin related ideas
* ecoin related topics
* ecoin related things to consider
* 

. . . 


medical journals . . research publications . . theoretical sciences books . . applied sciences books

mathematics

journalism

art

aesthetics

culture


human ease of use
* images, videos, colors, sights, symbols, sounds, objects, visualizations, writings, ???, hello worlds, sexual related topics, ideas, thoughts, inspirations, creativity, leave me alone, being alone, being together, 

. . . 


one thing that I've noticed . . uh . . well . . I guess . . uh . . most recently . . uh . . especially the last . . uh . . few uh . . weeks . . uh . . well weeks . . uh . . I'm not sure . . 

. . well . . I've been thinking about the topic of . . 

uh . . feelings . . and how they relate to the material world of expression . . 

uh . . well . . I've also been reading a lot of Seth books . . or rather . . listening to a lot . . books . . uh . . audiobooks . . uh . . by . . Seth . . Jane Roberts . . and Robert Butts . . 

. . well . . uh . . feels are interesting for example . . if I'm nervous . . maybe I won't necessarily . . uh . . well . . for example . . in the README.md . . in the past . . I've felt that I'm not always extremely happy . . uh . . but it's not really a feeling that is out in the open for me to see and so . . it's not apparent to me . . 

well . . for example . . the size of the text . . is very small . . . . when you use . . ##### . . that is . . 5 . . hashtag signs . . or hashtag characters . . i wasn't uh . . realy happy about that . . uh . . wel  . . maybe the feeling was there . . uh . . well . . I just didn't know what to do about it . . hmm . . well . . I feel a lot more happy now . . but . . it's not really something that I expected to be able to solve this problem . . on the one hand . . I didn't want to . . say . . ### . . those . . are . . 3 .. . hashtag signs . . uh . . which makes the . . text a lot larger . . uh . . but large . . text . . emphasizes . . uh . . the uh . . well . . it emphasizes . . a sort of significance relation . . well uh . . it's something to do with . . sections of a document . . and how . . maybe the chapter header . . is a lot larger than the . . uh . . section header . . which is quite uh . . common for me to see . . and so . . if the section is . . uh . . well . . uh . . it's a section . . and so for example . . 

### chapter title
##### section title

### another chapter title
##### another section title

. . 

well . . uh . . the size for the text for . . . . . . the section titles . . is preferred to be small . . but . . at the same time . . uh . . I don't think the aestheics match what I would prefer . . 

well . . 

uh . . my recent discovery xD . . if I can really call it that . . it really seems like an accident . . and also at the same time it seems like . . uh . . well . . it was already something that was there . . in books uh . . for example . . textbooks or uh . . traditional books with chapters . . and numbers and so on . . 

well . . uh . . it was a personal discovery I should say . . maybe not a phenomenological discovery that is new to anyone else . . 

well . . uh . . uh . . for example . . if you want to still have . . uh . . maybe . . the "sectional" . . relationship . . with the . . chapter and the "section" . . of the . . page . . 

### (1) chapter title
### (1.1) section title

### (2) chapter title
### (2.1) section title

. . 

having . . a number system like that . . where you can . . add . . uh . . numbers . . is a way to suggest to the reader . . that . . 2.1 . . is a section of . . 2. . . . . . and so . . well . . that is really quite cool . . and you can use the ### . . 3 hashtag signs . . or 3 pound signs . . or 3 pragma script signs . . or 3 number signs . . uh . . or 3 . . hash . . signs . . or . . 3 . . sharp signs . . or . . 3 . . hex signs . . or . . 3 . . pounds . . or . . 3 . . Octothorps . . octothorpes . . octathorps . . octatherps . . or . . 3 . . squares . . or . . 3 . . crosshatches . . or . . 3 . . crunches . . or . . 3 . . fences . . or . . 3 . . flashes . . or . . 3 . . garden fences . . or 3 . . garden gates . . or . . 3 . . gates . . or 3 . . grids . . or . . 3 . . haks . . or . . 3 . . meshes . . or . . . . . 3 . . . . . oofs . . or . . . . . 3 . . . . pig-pens . . or 3 . . punch marks . . or . . 3 . . rakes . . or . . 3 . . scratches . . or . . 3 . . scratch markes . . or . . 3 . . tic-tac-toes . . or . . 3 . . unequals . . or 3 . . Capital 3s . . or . . . . 3 . . comments . . or . . 3 . . corridors . . or . . 3 . . hash marks . . or 3 . . waffles . . or . . 3 . . tallies [Names inspired by . . [1.0]]


. . 


[1.0]
Number sign
From Wikipedia, the free encyclopedia
https://en.wikipedia.org/wiki/Number_sign





