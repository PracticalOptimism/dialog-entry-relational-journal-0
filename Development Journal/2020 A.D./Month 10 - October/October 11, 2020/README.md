


# Development Journal Entry - October 11, 2020


[Written @ 17:43]

DigitalCurrencyAccountToTransaction

[Written @ 17:33]

Notes @ Newly Created Words

Busidness
[Written @ October 11, 2020 @ 17:33]
[I accidentally typed the word "busidness" instead of typing the word "business" . . I'm sorry . . I'm not sure what this word "busidness" should mean at this time]


[Written @ 16:34]

Aliexpress Product Category Listing

- Women's Clothing
- Men's Clothing
- Cellphones & Telecommunications
- Computer & Office
- Consumer Electronics
- Jewelry & Accessories
- Home & Garden
- Luggage & Bags
- Shoes
- Mother & Kids
- Sports & Entertainment
- Beauty & Health
- Watches
- Toys & Hobbies
- Weddings & Events
- Novelty & Special Use
- Automobiles & Motorcycles
- Lights & Lighting
- Furniture
- Electronic Components & Supplies
- Education & Office Supplies
- Home Appliances
- Home Improvement
- Food
- Security & Protection
- Tools
- Hair Extensions & Wigs
- Apparel Accessories
- Underwear & Sleepweras
- Virtual Goods
- In All Categories


[Written @ 16:25]

Notes @ Newly Created Words

Amazone
[Written @ October 11, 2020 @ 16:25]
[I accidentally typed the word "amazone" . . instead of "amazon" . . I'm not sure . . what this word . . "amazone" . . should mean at this time]


[Written @ 16:16]

Amazon Product Category Listing

- [Audible Books & Originals]
- [Alexa Skills]
- [Amazon Devices]
- [Amazone Warehouse]
- Appliances
- Apps & Games
- Arts, Crafts & Sewing
- Automotive Parts & Accessories
- Baby
- Beauty & Personal Care
- Books
- CDs & Vinyl
- Cell Phones & Accessories
- Clothing, Shoes & Jewelry
  - Women
  - Men
  - Girls
  - Boys
  - Baby
- Under $10
- [Amazon Explore]
- [Amazon Pantry]
- Collectibles & Fine Art
- Computers
- Courses
- Credit and Payment Cards
- Digital Educational Resources
- Digital Music
- Electronics
- Garden & Outdoor
- Gift Cards
- Grocery & Gourmet Food
- Handmade
- Health, Household & Baby Care
- Home & Business Services
- Home & Kitchen
- Industrial & Scientific
- [Just for Prime]
- [Kindle Store]
- Luggage & Travel Gear
- Magazine Subscriptions
- Movies & TV
- Musical Instruments
- Office Products
- Pet Supplies
- Premium Beauty
- [Prime Video]
- Smart Home
- Software
- Sports & Outdoors
- Subscription Boxes
- Tools & Home Improvement
- Toys & Games
- Vehicles
- Video Games


[Written @ 15:40]

Product Categories

- Video Games
- Movies
- All-Purpose Shopping
- Housing
- Apartments
- Warehouses
- Land & Property Titles
- Seasonal
- Legal Services
- Finances
- 


Job Categories

- Customer Service
- Nursing Homes
- Medicine
- Software Engineering
- Electrical Engineering
- Mechanical Engineering
- Beauty & Fashion
- Sports
- Health & Fitness
- Management
- Dietary Science
- Accounting
- Lifestyle Coaching
- 


[Written @ 15:01]

Account Page Usecase
  - search for a business to shop with
    - what products do they sell (product category)
    - where are they located (physical location address)
    - where are they located online (website url)
  - search for an employer to work with
    - what skills do they need (job title)
    - job application form (job application url)
    - where are they located (physical location address)
    - where are they located online (website url)
    - how to contact them (send chat message)
    - 
  - search for a currency exchange to trade with
    - what are the currencies available (currency list)
    - 

Transaction Usecase
  - send a list of contact information
    - send physical location address (read permission)
    - send private message (private message)

Network Status Page
  - number of created accounts
    - number of newly created accounts timeline graph
  - number of created transactions
    - number of newly created transactions timeline graph (amount per day for last 30 days)
    - median transaction amount timeline graph (amount per day for last 30 days)
    - number of liked transactions
    - number of disliked transactions
  - number of created digital currencies
    - number of newly created digital currency units timeline graph (amount per day for last 30 days)
    - 
  - newly created accounts list
  - newly created transactions list
    - same as account page
  - newly created digital currencies list
    - for newly created accounts (reason for distribution)
    - for universal basic income (reason for distribution)



[Written @ 14:55]

DigitalCurrencyAccount {
  isDigitalCurrencyExchange: boolean = false

  isCurrentlySellingProductsOrServices: boolean = false
  isCurrentlyEmployingWorkers: boolean = false
}

DigitalCurrencyAccountDescription {
  contactVariableTree: { [contactId: string]: DigitalCurrencyAccountContact } = {}
}

DigitalCurrencyAccountContact {
  contactName: string = ''
  contactNameDescription: string = ''
  readPermissionId: string = ''
}

DigitalCurrencyAccountBusinessDescription {
  productWebsiteUrl: string = ''

  productCategoryVariableTree: { [productCategoryId: string]: boolean } = {}
}

DigitalCurrencyAccountEmployerDescription {
  jobWebsiteUrl: string = ''

  jobTitleVariableTree: { [jobTitleId: string]: boolean } = {}
}

DigitalCurrencyAccountCurrencyExchangeDescription {
  currencyExchangeWebsiteUrl: string = ''

  currencyVariableTree: { [currencyId: string]: boolean } = {}
}

[Written @ 14:01]

goals for today:
- [x] get a list of country names
  - [x] gather a list of resources available
  - [] have a working prototype of getting the country list
- [cancelled] get a list of town / city names
  - [x] gather a list of resources available
  - [cancelled] have a working prototype of getting the town list for a country
    - will have the user type in their city / town name instead
- [] prepare data structures
  - [x] digital currency account data structures
  - [] digital currency transaction data structures
- [x] get a list of product categories
  - used amazon and aliexpress listing above (amazin')






## Resources:

##### Finding a list of countries, towns and cities

GeoDB
http://geodb-cities-api.wirefreethought.com/


wft-geodb-js-client
https://www.npmjs.com/package/wft-geodb-js-client


Place Search
https://developers.google.com/places/web-service/search


World Cities Database Free Edition
http://www.geodatasource.com/world-cities-database/free


US Cities and Towns
https://www.citytowninfo.com/places


Countries, Languages & Continents data
https://www.npmjs.com/package/countries-list


A World of Countries
https://github.com/lucagrandicelli/a-world-of-countries
https://www.npmjs.com/package/a-world-of-countries


country-list
https://www.npmjs.com/package/country-list


Show district list as per country and state select in dropdown select box Javascript
https://www.studentstutorial.com/javascript/country-state-city-select.php



https://www.sitepoint.com/community/t/country-state-city-dropdown-list/2438/2

##### Product Categories


##### Fiding a list of 


Notes @ Newly Created Words

ajuyunkool
ajuyuncool
ajuyunkul
[Written @ October 11, 2020 @ 14:48]
[This word came to mind to me a few seconds ago . . the sound . . could be written in these 3 ways . . the pronounciation is like this . . a-ju-yun-kul]




[Written @ 11:47]

DigitalCurrencyEmployerAccount


DigitalCurrencyBusinessAccount


[Written @ 11:06]

## Variables

digitalCurrencyAccountVariableTree: { [digitalCurrencyAccountId: string]: DigitalCurrencyAccount } = {}

digitalCurrencyAccountToTransactionMap: { [digitalCurrencyAccountId: string]: DigitalCurrencyAccountToTransactionEntry } = {}

digitalCurrencyAccountToChatChannelMap: { [digitalCurrencyAccountId: string]: DigitalCurrencyAccountToChatChannelEntry } = {}

digitalCurrencyAccountToEmployerAccountMap: { [digitalCurrencyAccountId: string]: DigitalCurrencyAccountToEmployerAccountEntry } = {}

digitalCurrencyAccountToBusinessAccountMap: { [digitalCurrencyAccountId: string]: DigitalCurrencyAccountToBusinessAccountEntry } = {}



## Data Structures


DigitalCurrencyAccount

DigitalCurrencyAccountToTransaction

DigitalCurrencyAccountToAccountDescriptionEntry
DigitalCurrencyAccountToBusinessDescriptionEntry
DigitalCurrencyAccountToEmployerDescriptionEntry
DigitalCurrencyAccountToCurrencyExchangeDescriptionEntry


DigitalCurrencyAccountNewlyCreatedEntry
DigitalCurrencyAccountNewlyUpdatedEntry
DigitalCurrencyAccountNewlyDeletedEntry




DigitalCurrencyTransaction

DigitalCurrencyTransactionToAccountEntry
DigitalCurrencyTransactionToCurrencyEntry
DigitalCurrencyTransactionToChatChannelEntry


DigitalCurrencyTransactionNewlyCreatedEntry
DigitalCurrencyTransactionNewlyUpdatedEntry
DigitalCurrencyTransactionNewlyDeletedEntry


. . . 

DigitalCurrencyAccountToChatChannel
DigitalCurrencyAccountToShoppingMallAccount
DigitalCurrencyAccountToCurrencyExchangeAccount
DigitalCurrencyAccountToEmployerAccount
DigitalCurrencyAccountToBusinessAccount

. . . 

DigitalCurrencyAccountTimeline

. . . 


DigitalCurrencyTransactionTimeline
DigitalCurrencyTransactionWaitingToBeProcessed

. . .


NewlyUpdated
UpdatedTimeline

. . . 



DigitalCurrencyTransactionNewlyUpdated {
  analyticsDescription: any

  dateCreatedLikeEventVariableTree: {}
  dateCreatedDislikeEventVariableTree: {}
}

DigitalCurrencyTransactionNewlyUpdatedLikeEntry {

}



. . . 


class DigitalCurrencyAccountToTransactionMap {

  analyticsDescription: DigitalCurrencyAccountToTransactionAnalyticsDescription = new DigitalCurrencyAccountToTransactionAnalyticsDescription()

  transactionIdVariableTree: { [transactionId: string]: boolean } = {}

  recurringTransactionIdVariableTree: {
    [recurringTransactionId: string]: { [transactionId: string]: boolean }
  } = {}

  processedTransactionVariableTree: {
    [transactionId: string]: DigitalCurrencyAccountProcessedTransactionVariableTreeItem
  } = {}
}

class DigitalCurrencyAccountProcessedTransactionVariableTreeItem {
  transactionOrderIndex: number = 0
  digitalCurrencyAmountBeforeTransaction: number = 0
  isLikedBySenderAccount: boolean = false
  isLikedByRecipientAccount: boolean = false
}


class DigitalCurrencyAccountToTransactionAnalyticsDescription {
  latestProcessedTransactionId: string = ''
  numberOfProcessedTransactions: number = 0
  isAdminCurrentlyProcessingTransaction: boolean = false
}

. . . 



DigitalCurrencyTransactionNewlyCreated {
  analyticsDescription: DigitalCurrencyTransactionNewlyCreatedAnalyticsDescription

  ateCreatedVariableTree: {
    [millisecondDay: number]: {
      [millisecondHour: number]: {
        [millisecondMinute: number]: {
          [transactionId: string]: DigitalCurrencyTransactionNewlyCreatedEntry
        }
      }
    }
  } = {}
}

DigitalCurrencyTransactionNewlyCreatedAnalyticsDescription {
  numberOfCreatedTransactions: number = 0
}

DigitalCurrencyTransactionNewlyCreatedEntry {

}


. . . 


DigitalCurrencyNewlyCreatedMap {
  analyticsDescription: DigitalCurrencyNewlyCreatedAnalyticsDescription = new DigitalCurrencyNewlyCreatedAnalyticsDescription()

  currencyDateCreatedVariableTree: {
    [millisecondDay: number]: {
      [millisecondHour: number]: {
        [millisecondMinute: number]: DigitalCurrencyNewlyCreatedEntry
      }
    }
  } = {}
}

DigitalCurrencyNewlyCreatedAnalyticsDescription {
  numberOfCreatedCurrencyUnits: number = 0
}

DigitalCurrencyNewlyCreatedEntry {
  numberOfNewlyCreatedDigitalCurrencyUnits: number = 0
  numberOfDigitalCurrencyAccountsReceivingNewCurrencyUnits: number = 0

  reasonForDistribution: string = ''
  dateDistributed: number = (new Date()).getTime()
}



. . . 

DigitalCurrencyTransaction {
  isLikedBySenderAccount: boolean = false
  isLikedByRecipientAccount: boolean = false

  privateMessage: string = ''
  senderPrivateEmailAddress: string = ''
  senderPrivatePhysicalMailingAddress: string = ''
}

DigitalCurrencyTransactionToChatMessageProtocol {
  
}

. . . 


ChatMessageProtocol
ChatChannel
ChatMessage
ChatMember
ChatProtocol

ChatProtocol
ChatCommunicationProtocol
ChatCommunicationMessageProtocol
ChatMessageProtocol

ChatMessageProtocolChannel
ChatMessageProtocolMessage
ChatMessageProtocolMember

. . . 

ChatMessageProtocol





. . . 

Notes @ Newly Created Words

Chanem
[Written @ October 11, 2020 @ 17:47]
[I accidentally typed the word "Chanem" . . instead of . . "Chat Message Protocol" . . I'm sorry . . I'm not sure what this word "Chanem" should mean at this time]
