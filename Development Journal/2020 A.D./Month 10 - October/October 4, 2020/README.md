

# Development Journal Entry - October 4, 2020


[Written @ 16:57]



[Written @ 16:57 - 16:57]

"the theme continues" . . 

this thought came to me . . while working on the . . codebase . . for today . . 

[Written @ 16:44]

. . . 

uh . . I'm trying to reach my goal of publishing the demo website using the continuous integration / deployment process . . to the development version of firebase . . for this project . . 

and so . . am not trying to talk so much . . but in any case . . this may be an intereting topic . . even if I am not really uh . . well I haven't really explored the topic all that uh . . much . . the idea only came to me . . uh . . 1 or 2 or 3 . . days ago . . if I remember correctly . . uh . . certainly . . within the last week . . 

Uh . . the idea . . is to ask . . a computer 1 . . question . . "have you seen this before?" . . . . and the computer . . responds . . with "yes" . . or . . "no" . . and so . . in this way . . you can . . train a computer . . uh . . to . . tell you the -_- . . like I said I am not really that professional in this topic right now . . but it is new to me . . and maybe my words aren't the best to talk about things . . like the idea of "learning" . . is a loaded term maybe for some people . . and so . . I'm sorry for using this term . . 

if you ask the question . . "have you seen this before?" . . you can show a picture of a dog . . and then . . more pictures . . of dogs . . and if the computer says yes . . then . . the . . idea of dog is maybe . . something that can be said to be . . uh . . "understood" . . 

uh . . and then . . if you show the picture of a cat . . maybe . . that can be uh . . not understood . . if the cat has not been seen before . . uh . . in the familiar context of having already uh . . seen maybe . . 400 dogs or . . 400 pictures of dogs . . 

uh . . well . . 

maybe . . uh . . well . . you can do this method . . of uh . . well . . you can . . uh . . label . . uh . . well . . I think this could be useful for uh . . well something about neural networks  uh i'm really not experienced all that much in the topic but there could be an avoidance of having to retrain the model on different categories of items if the question of the newness of a perception has been seen before in the uh . . . something . . 


Notes @ Newly Learned Words

neut
[Written @ October 4, 2020 @ approximately 16:51]
[I was going to type the words "neural network" . . but I accidentaly typed the word "neut" . .]

Notes @ Newly Created Words

neutwork
[Written @ October 4, 2020 @ approximately 16:54]
[I accidentally typed the word . . "neut" . . while . . trying to type the word . . "neural network" . . a possible continuation of this typographical error is to have the word . . "neutwork" . . uh . . "neutwork" . . uh . . a word . . ]

materix
[Written @ October 4, 2020 @ 16:55]
[I accidentally typed the word . . "mater" . . while . . preparing to type the word . . "matrix" . . and so maybe a possible way to combine the two words . . "mater" and "matrix" is to have the word "materix" . . ]

wrods
[Written @ October 4, 2020 @ 16:56]
[I accidentally typed the word . . "wrods" . . instead of . . "words" . . ]


[Written @ 16:41]

"
The theme continues . . 
"

This is a thought that I had in my mind . . just now . . a few . . seconds ago . . 

I am inspired to write it down . . for . . uh . . well . . I thought of an interesting question to ask . . a few days ago . . 

I wrote 2 . . questions that really are interesting and I don't remember having asked them before . . 

. . . 

Have you seen this thing before ? 
  - Yes, I have seen this before
  - No, this is new to me
. . . 

Ahh . . uh . . I think this was the question . . and then . . you have . . 2 answers to give . . "Yes, I have seen this before" . . or . . "No . . this is new to me" . . 


. . . 

have you seen this cat ear before ? [picture 1]
have you seen this cat ear before ? [picture 2]
. . . 

the concept of an ear could maybe be . . uh . . applied or . . requested . . by associating more photographs of ears with the language terminology 

. . . 

have you seen this dog ear before ? [picture of dog ear 1]
have you seen this dog ear before ? [picture of dog ear 2]

. . . 

have you seen this math problem before ? [math problem 1]
have you seen this math problem before ? [math problem 2]

. . . 

can you show me a math problem that is new ? [generate math problem that the computer hasn't seen before . . according to what its uh . . been trained to be familiar with . . so far . . something like that . . ]

can you show me a math problem that you have seen before ? [generate math problem that the computer has seen before]

. . . 


can you show me an episode of Teletubbies that has each character having cat ears ?

can you show me a picture of a cat with dog ears ? 

. . . 

have you seen a picture of a cat with dog ears ? 

have you seen a picture of a dog with cat ears ? 

. . . 

can you show me a new way to fight ? 

have you seen this fighting style before ? 

. . . 

. . . 

. . . 

right . . so . . one question is to ask . . "have you seen this before ? " . . 


. . . 

have you seen this weather pattern before ? 
have you seen this air balloon before ? 
have you seen this series of events before ? 
have you seen this person before ? 
have you seen this technical diagram before ? 
have you seen this comedy routine before ? 
have you heard this music piece before ? 
have you watched this movie before ? 
have you seen this piano recital before ? 

. . . 

the response can be "yes" or "no" . . 

. . . 

show me a weather pattern that you've seen before . . 
show me a picture of an air balloon that you've seen before
show me a video of a series of events that you've seen before
show me a picture of a person you've seen before
show me a picture of a technical diagram you've seen before
show me a demonstration of a comedy routine that you've seen
play me a music piece that you've heard before
play me a movie that you've seen before
play some music like a piano recital you've attended before

. . . 

show me a weather pattern that you haven't seen before . . 
show me a picture of an air balloon that you haven't seen before . . 
show me a video of a series of events that you haven't seen before . . 
show me a picture of a person that you haven't seen before
show me a picture of a technical diagram that you've never seen before . . 
show me a comedy routine that you've never seen before
play me a music piece that you've never heard before
play me a movie that you've never seen before
play me a music from a piano recital that you've never attended before

. . . 


. . . 


Is this something new ? 
Is this something old ? 

. . . 

okay . . those are the two questions . . relatively . . but uh . . 


[Written @ 16:17]

the current time the server is processing the transaction list

current date . . 


the transaction list is available at . . minute . . intervals . . from the current date . . 

. . . 

the dates . . before . . the current date . . are the ones that will be processed . . up to . . the last . . 15 minutes . . unless the transaction list has not been processed for another reason [??? not sure yet . . ] . . 

. . . 

. . . 


for some reason . . the . . apply transaction list . . algorithm . . is currently . . adding the transaction list statistics . . to . . the time . . 1 minute . . or 60 * 1000 milliseconds . . before . . the . . minute interval . . of the . . transaction list currently being processed . . 

. . . 

for example . . the 540000 . . millisecond minute . . is 1 minute . . or . . 60 * 1000 milliseconds or . . 60000 milliseconds . . before . . 1140000 . . 

. . . 

uh . . currently . . I am expecting . . to see . . that the . . transaction list . . statistics . . currently in 540000 . . are being . . placed . . inside . . the . . millisecond minute . . entry variable path . . for the 1140000 . . minute . . path . . 

. . . 

I'm not sure . . why this is happening . . that . . the statistics . . are being . . placed . . inside of . . 540000 . . and not . . 1140000 . . 

. . . 


I am trying to look at the code . . for . . apply transaction list . . but am not . . finding . . anything . . that . . really . . highlights to me . . that something is wrong . . in my thinking . . of how the code is written . . uh . . but . . uh . . if . . these results . . of being in 540000 or . . being in a minute entry path 1 minute prior to where the transaction id variable tree is actually presented . . 1140000 in this case . . well . . I'm thinking that . . there really is something . . happening . . that I am not expecting . . and so . .


I am going to spend more time . . to look at this codebase and see what are the things that are possibly missing in my thinking abilities . . 


. . . 

the date the transaction is created . . 
  - 

the date the transaction is processed . .
  - appearing 1 minute before the transaction is processed . . 


? ? ? 

shouldn't the transaction be processed . . after it is created ? . . 

hmm . . well . . let me think . . there is something happening here . . maybe . . related to . . the . . uh . . hmm . . the time ? ? . . . one moment . . I'm really not clear on this idea yet . . and so let me think things through by moving my eyes through the codebase . . 




. . . 


