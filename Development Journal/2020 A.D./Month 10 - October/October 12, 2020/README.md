


# Development Journal Entry - October 12, 2020


[Written @ 19:30]

Notes @ Newly Created Words

ENVRIO
envrio
[Written @ October 12, 2020 @ 19:30]
[I accidentally typed the word "ENVRIO" instead of "ENVIRONMENT" . . I'm sorry . . I'm not sure what this word "envrio" should mean at this time]


[Written @ 19:16]

@project

. . . 

project-configuration
project-environment-variable
project-environmnet-variable

project-environment-variable


environment-variables
environment-variables

configuration

configuration-object
project-configuration-object
project-configuration-object-notation
project-configuration-object-specification
project-configuration-object-


speignifier
speignifier
speignifier


project-configuration-object-speignifier

configuration-object-notation-specifier
configuration-object-notation-object


conditionalizer

program-condition-specifier
program-condition-initializer-metric

. . . 

project-instantiation-service-originator
project-program-instantiation-specializer-value

project-helper-value

. . . 

NODE_ENV
FIREBASE_CONFIGURATION_OBJECT
DATABASE_SERVICE_CONFIGURATION_OBJECT
TEXT_SEARCH_SERVICE_CONFIGURATION_OBJECT

. . . 

DEVELOPMENT_ENVIRONMENT_BUILD
PRODUCTION_ENVIRONMNET_BUILD
TEST_ENVIRONMENT_BUILD

ENVIRONMENT_SPECIFIER_ID


. . . 


. . . 

// web browser website project environment specifier
NODE_ENV=production


SERVICE_DEVELOPMENT_ENVIRONMENT_SPECIFIER_ID



service-group-1
service-group-1-emulator


FIREABSE_CONFIGURATION
ALGOLIA_CONFIGURATION


FIREBASE_SERVICE_ID=development
FIREBASE_SERVICE_ID=production


FIREABSE_ENVIRONMENT_SPECIFIER_ID=emulator
FIREBASE_ENVIRONMENT_SPECIFIER_ID=development
FIREBASE_ENVIRONMENT_SPECIFIER_ID=production

ALGOLIA_ENVIRONMENT_SPECIFIER_ID=emulator
ALGOLIA_ENVIRONMENT_SPECIFIER_ID=development
ALGOLIA_ENVIRONMENT_SPECIFIER_ID=production

. . . 

FIREBASE_ENVIRONMENT_SERVICE_ID
FIREBASE_ENVIRONMENT_SERVICE_ID


. . . 

VUEJS_ENVIRONMENT_SERVICE_ID

ENVIRONMENT_SERVICE_ID

. . . 

NODEJS_ENVIRONMENT_SERVICE_ID

. . . 

WEB_BROWSER_WEBSITE_ENVIRONMENT_SERVICE_ID

. . . 


. . . 

project environment service

. . . 

/project-environment-service
  - /basic-environment-service
  - /

. . . 



[Written @ 17:40]

// import * as firebase from 'firebase'

// import * as firebase from '@firebase/rules-unit-testing'

// import * as firebase from '@firebase/testing'

// console.log(firebase)

. . . 

firebase testing . . firebase . . rules unit testing . . these . . don't . . seem . . to work . . in . . the web browser . . and so . . uh . . running . . the . . firebase emulator . . uh . . maybe . . uh . . is . . meant . . to work . . for . . uh  . . the nodejs . . environment . . only . . uh . . I'm not sure . 

. . . 

uh . . . 

I suppose . . one uh . . possible . . thing . . to do . . if . . a web browser . . client . . would like to . . request resources from a . . firebase emulator . . is to have an http server . . uh . . that the . . web browser . . can communicate with . . uh . . and the http server can also . . communicate . . with the . . firebase emulator . . using the . . firebase testing (or now firebase rules-unit-testing) . . library . . or something like that

. . 



[Written @ 17:27]

Notes @ Newly Discovered Project


targaryen
By goldibex
https://www.npmjs.com/package/targaryen
Completely and thoroughly test your Firebase security rules without connecting to Firebase.



[Written @ 17:04]

. . . 

a service emulator . . 
a service . . 

maybe making the distinction between these is useful at runtime . . and not . . a compile time . . decision . . or something like that . . and so for example . . 

toggle between a service . . and a service emulator . . at runtime . . 

. . . 


/initialize-service-group-1
  - /~item-service
  - /~item-service-emulator

. . . 

/initialize-service-group-1
  - /-based-on-service
  - /-based-on-service-emulator

. . . 


. . . 

hmm . . I'm not sure . . it seems like maybe . . that is already uh . . something that can be done . . uh . . . . uh . . 


// call ? . . 
// with firebase testing as the service emulator library for firebase, we would need to check to ensure that firebase testing works in the web browser . . uh . . which . . uh . . I'm uh . . not really sure about at this time . . 


initializeServiceGroup1.itemServiceEmulator.algorithm.function()




[Written @ 16:24]



/service-group
  - /service-group-1
    - /algorithms
      - /delete-service-group-1
        - /expectation-definitions
          - /expectation-definitions
            - expectation-definitions.ts
          - /delete-service-group-1
            - index.ts
        - index.ts
      - /initialize-service-group-1
        - /expectation-definitions
          - /expectation-definitions
            - expectation-definitions.ts
          - /initialize-service-group-1
            - index.ts
        - index.ts

. . . 

the expectation definitions . . are maybe having their own . . algorithms . . that are performing things . . and those algorithms can each have their own expectation definitions . . and this architecture . . was trying to represent . . something along those lines . . 

However . . 

. . 

. . . 


/initialize-service-group-1
  - /~item-service
    - /permission-rule
      - initialize-algorithm-expectation-definition-measurement.ts
    - /item
      - algorithm.ts
  - /~item-service-emulator
    - /permission-rule
      - initialize-algorithm-expectation-definition-measurement.ts
    - /item
      - algorithm.ts
  - /~item-service-stand-in-replacement
    - /permission-rule
      - initialize-algorithm-expectation-definition-measurement.ts
    - /item
      - algorithm.ts

. . . 


initializing a service . . can involve . . creating the service . . uh . . for example . . a . . database service . . can be initialized . . and those . . computer resources can be prepared to be used . . uhm . . well . . 

a mock service . . a service that is meant to emulate or stand in place of a "real" service . . 

hmm . . 


initialize-mock-service-group-1

. . . 

hmm . . 


. . . 


index.ts
item.ts
algorithm.ts

. . . 






