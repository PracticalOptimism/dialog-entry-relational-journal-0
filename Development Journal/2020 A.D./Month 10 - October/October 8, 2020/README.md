

# Development Journal Entry - October 8, 2020


[Written @ 19:47]

. . . 



[Written @ 18:26]

Notes @ Newly Created Words

architecutre
[Written @ October 8, 2020 @ 18:27]
[I accidentally typed the word "architecutre" . . instead of the word "architecture" . . I'm sorry . . I'm not sure . . what this word "architecutre" . . should mean . . at this time .]

architecu
[Written @ October 8, 2020 @ 18:45]
[I accidentally typed the word "architecu" . . instead of the word "architecture" . . I'm sorry . . I'm not sure . . what this word . . "architecu" . . should mean at this time]

[Written @ 17:44]


/create-item
  - /create-item-by-item-id

. . . 

/create-item
  - /-by-item-id
    - /~service

. . . 

/create-item
  - /-by-item-id
    - /~item-service

. . . 

in general: 

/create-item
  - /-by-item-id
    - /~item-service
      - /service
      - /service-account
      - /item
      - /item-list
      - /item-store
      - /event-listener
      - /permission-rule
      - /network-protocol-connection // network-protocol-agreement ??
      - /network-protocol-member
      - /network-protocol-message

. . . 

to delineate . . 

/create-item
  - /-by-item-id
    - /~item-service
      - service.file-type
      - service-account.file-type
      - item.file-type
      - item-list.file-type
      - item-store.file-type
      - event-listener.file-type
      - permission-rule.file-type
      - network-protocol-connection.file-type
      - network-protocol-member.file-type
      - network-protocol-message.file-type

. . . 

a `tilda tree` (/~item-service tree) is a way to create indices of elements . . that can elongate . . into further specified tree specified features . . 



. . . 

/algorithms
  - /digital-currency-account
    - /create-account
    - /get-account
    - /update-account
    - /delete-account

. . . 

/algorithms
  - /digital-currency-account
    - /account
      - /create-account
      - /get-account
      - /update-account
      - /delete-account
    - /account-list
      - /create-account-list
      - /get-account-list
        - /-based-on-firebase-cloud-firestore
          - /-by-account-id-list
          - /-by-text-search-query
            - /-based-on-algolia
              - /~item-service
                - item.ts
                - permission-rule
                  - get-is-allowed-to-update-algorithm-list.json
                  - get-is-valid-algorithm-input.ts
                  - get-is-valid-algorithm-output.ts
                  - get-is-valid-algorithm-performance-measure.ts
                  - /initialize-expectation-definition-measure
                    - /-based-on-this-web-browser-environment
                      - -for-performance.ts
                      - -for-algorithm-input-and-output.ts
                    - /-based-on-this-other-web-browser-environment
                      - -for-performance.ts
                      - -for-algorithm-input-and-output.ts
                    - /-based-on-this-javascript-runtime-environment
                      - -for-performance.ts
                      - -for-algorithm-input-and-output.ts
                    - 
                  - initialize-performance-measure.ts
              - /~item-service-2
              - /~item-service-3
              . . .
            - /-based-on-elasticsearch
          - /-by-sort-and-filter-query
        - /-based-on-firebase-realtime-database
      - /update-account-list
      - /delete-account-list
    - /account-store
    . . . 
  - /digital-currency-transaction
    - /transaction
    - /transaction-list
    - /transaction-store
    . . .


. . . 

troy architecture . . topic . . 

/topic-name
  - /algorithms
  - /constants
  - /data-structures
  - /infinite-loops
  - /moment-point-algorithms
  - /variables
  . . .

. . . 

troy architecture . . item service . .

/~item-service
  - /service
  - /service-account
  - /item
  - /item-list
  - /item-store
  - /event-listener
  - /permission-rule
  - /network-protocol-connection
  - /network-protocol-member
  - /network-protocol-message

. . 

@ . . 

. . 

/@topic-name
  - /algorithms
  - /constants
  - /data-structures
  - /infinite-loops
  - /moment-point-algorithms
  - /variables
  . . .

. . should the topic name have the @ [at symbol] symbol in front of the name . . uh . . like the ~ [tilda symbol] symbol . . is in front of the item-service name ? . . 

. . . 

well . . earlier . . we are using . . @ for . . uh . . "shared" uh . . uh . . mostly for @project and @architecutre :P . . hmm . . it was originally uh . . a way to use . . uh . . uh . . to improt the contents of a . . directory . . outside of that . . uh . . project . . directory . . uh . . so for example . . @project . . can be imported to other . . uh . . directories . . like . . uh . . the usecase / digital-currency directory . . hmm . uhm . well . by having the @ designation . . we could for example . imagine that it is uh . maybe going to save us from . uh . circular imports . :O . uh . where for example . uh . the @project isn't expected to import from other . uh . parts of the codebase uh . for example @project uh . isn't expected to import from usecase / digital-currency . . 

uhm . . erll .  . . uh . . and now I am uh . not uh . sure what the topic is about . uh . for example . it uh . is an interesting concept . uh . to have algorithms, constants, etc. uh . well . now I'm not sure how this concept relates to the topic of an item service . . and so . well . . I'd like to work around this topic . to understand more about how they interact . uh . in my initial thoughts at the moment . . uh . . a topic . . and an item service are quite . complementary . uh . and they . . are able to work teogther uh . quite well . . or quite nicely . but I'm not sure . that is maybe uh . an hypothesis . 


uh . . 

well . . I like the idea of uh . not having . . the @ prespecifier . or . uh . . the @ uh . . prefix on the name of the topic . . 

uh . . well . uh . I'm also needing to think more about the topic relating to . . @architecture and @project . . uh . for example . . I uh . could imagine . . uhm . . hmm .  

@ is uh . well . . hmm . . 

/@architecture
  - /network-protocol-connection
    - /-based-on-file-import
      - /-based-on-es6-syntax
        - /-based-on-firebase-admin
        - /-based-on-firebase
        - /

. . . 


@architecture
  - /network-protocol-message
    - /-based-on-file-import
      - 

@architecture
  - /network-protocol-connection
    - /-based-on-file-based-importing

@architecture
  - /network-protocol-connection
    - /-based-on-file-import-and-export-connection
    - /-based-on-internet-send-and-receive-connection
    - /-based-on-local-area-network-send-and-receive-connection
    - /-based-on-information-about-movies-yes-or-no-question-connection

. . . 

@architecture
  - /network-protocol-connection
    - /-based-on-file-import-and-export-connection
    - /-based-on-internet-send-and-receive-connection
    - /-based-on-local-area-network-send-and-receive-connection
    - /-based-on-information-about-movies-yes-or-no-question-connection

. . . 


uh . well . uh . in thinking about @architecture . uh . hmm . the name is interesting . uh . well . hmm . uh . I'm not really sure . I'll uh . well . I haven't really thought about it that much . what an architecture means . or . uh . what the . @architecture . uh . directory . uh . should relate to . besides . uh . connecting . uh . files . uh . by . uh . providing . uh . the . uh . uh . radius of curvature . uh . of . uh . the uh . underlying . uh . statistical uh . method . of . uh . approximating . uh . relations . 

xD . -_- . well . uh . what that means is uh . 

uh . uh . uh . uh . 

uh . uh . uh . uh . well . uh . hmm . -_- . xD . uh . hmm . let's see . uh . hmm . the architecture of a project is an interesting topic . uh . we can talk about the architecture in terms of a file based directory tree or uh hash map structure uh or maybe dictionary tree structure uh well uh depending on the name you prefer or something like that . uh . well uh we could also have a uh nested list of arrays uh which is uh maybe more uh ways to talk about the uh hash map or something like that . uh . well . uh . the architecture is also possibly a thing that relates to infinite loops of uh newly created lists or something like that . uh well if there is a uh program uh that uh creates uh more uh files and directories uh at runtime or something uh as part of the project organization structure uh . well uh uh well uh maybe in some case this can be viewed as a computer programmer uh person or uh automated uh computer programmer uh that operates the new names and files and directories inside of the program tree data structure organization structure or something like that . . . 

uh . . hmm . . well . . in general . uh . uh . well -_- . I'm really not sure . the idea of architecture is uh maybe uh something more understandable in uh terms uh that I'm not uh necessarily uh thinking about uh . at the moment . uh well . hmm . it's an interesting topic . uh hmm . 

the architecture hmm . I still have to think about if this directory is needed . 

for example . maybe it can be replaced by a service . . uh . . directory or something like that . . 

architecture is quite a general term uh . well . . and we can also consider the idea of trying to re-use what uh . . the . . uh . . item service enumerated properties are . . uh . . service . . service account . . item . . item list . . and so on . . uh . . if we can re-use those thoughts uh . . well . that could be considered a way to uh  . . . uh . . well uh maybe keep a certain uh need for uh having uh boundaries uh that are extendable uh in a systematic way uh without uh necessarily needing to have uh something like new file or directory names or something uh . something like that . 

hmm . . 




[Written @ 17:32]


Notes @ Newly Created Words

requipre
[Written @ October 8, 2020 @ 17:32]
[I accidentally typed the word "requipre" . . instead of . . "requirements" . . I'm sorry . . I'm not sure . . what this word . . "requipre" . . should mean . . at this time]


Notes @ Newly Learned Words

delian
[Written @ October 8, 2020 @ 17:48]
[I accidentally typed the word "delian" . . instead of the word "delineate" . . I'm sorry . . I'm not sure . . what this word . . "delian" . . should mean at this time . . uhm . . well . . after doing some research . . it seems that this word . . "delian" . . is already . . defined :O]



[Written @ 17:28]


/create-item
  - /create-item-by-item-id
    - /-based-on-these-requirements
      /
    - /-based-on-the-administrator-daemon-processing-requests
    - /-based-on-the-client-processing-requests
    - /-
  - /create-item-by-item-store-anonymity
  - 

. . . 

[Written @ 17:00 - 17:28]

. . . 

/service
/service-account
/item
/item-list
/item-store
/event-listener
/permission-rule
/network-protocol-connection
/network-protocol-member
/network-protocol-message

. . . 



/service
/service-account
/item
  /create-item
  /get-item
  /update-item
  /delete-item
/item-list
/item-store
/event-listener
/permission-rule
/network-protocol-connection
/network-protocol-member
/network-protocol-message

. . . 


/service
/service-account
/item
  /create-item
    /???
  /get-item
  /update-item
  /delete-item
/item-list
/item-store
/event-listener
/permission-rule
/network-protocol-connection
/network-protocol-member
/network-protocol-message


. . . 

/???

. . . 

possibilities:

index.ts
expectation-definitions.ts

. . . 

possibilities:

/service
  - theoretical possible usecase or useful option that may or may not be used
/service-account
  - theoretical possible usecase or useful option that may or may not be used
/item
  - index.ts is in here
  - . . . 
/item-list
  - theoretical possible usecase or useful option that may or may not be used
/item-store
  - theoretical possible usecase or useful option that may or may not be used
/event-listener
  - theoretical possible usecase or useful option that may or may not be used
/permission-rule
  - expectation-definitions.ts are in here
  - validation rules are in here
  - access control rules are in here
/network-protocol-connection
  - file connection
    - an import statement is like a network protocol connection between files
    - an export statement is like a network protocol connection between files
  - internet connection
  - ??? theoretical possible usecase or useful option that may or may not be used
/network-protocol-member
  - file member
    - a computer system can be uh . . maybe seen as uh . . a source for . . file messages . . uh . . and connecting to that member can be . . uh . . uh . . maybe a way to connect to uh . . certain files and directories of the computer . . or something like that . . uh . but this is experimental and I'm not really sure . . I'm mostly playing around with ideas . . uh . . and seeing what could be a useful software architecture to consider uh . . . to consider
/network-protocol-message
  - file message
    - the contents of a file can be seen as a network protocol message
. . . 

. . . 

. . . 

. . . 

create-item
  - index.ts
  - expectation-definitions.ts

advantages:
  - easy to write
  - easy to read


. . . 

create-item
  - /item
    - index.ts
  - /permission-rules
    - expectation-definitions.ts
    - 

thoughts: . . . uh . . I'm not sure yet . . I'm mostly experimenting at this time . . uh . . well . . one thing that  . . uh . . I'm really not sure about . . uh . hmm-_- . hmm  .. .. . . . . . 
hmm

hmmmmmmmmmmmmmmmmmmmm



. . . 

permission rules . . what are they about ? . . well . . ideally . . this topic relates to information about what is or is not allowed to uh . . be performed uh . . uh . . for example . . in a . . uh . . uh . . what is . . allowed to be created, updated, retrieved or deleted . . 

and so . . if you have a permission rule for . . a create item request . . or for a create item . . algorithm . . you may prefer the data to be composed . . or formatted . . in a certain way . . uh . . and so for example . . 

DigitalCurrencyAccount {
  accountId: string = '' 
  anotherPropertyId: string = ''
}

. . . uh . . well . . this is . . a data structure . . that represents an account . . uh . . so . . when you create item . . and place this item into the item store . . uh . . for uh . . where those . . uh . . where those items are placed . . 

you can have a permission rule that . . specifies . . 

. . . 

// create a permission rule for the digital currency account item store

const storeId = 'digital-currency-account'
const itemId = 'account-id'

createPermissionRule(storeId, permissionRule)

. . . 

// create a permission rule for the digital currency account with a specific account id

const storeId = 'digital-currency-account'
const itemId = 'account-id'

const permissionRuleId1 = createPermissionRule(storeId, itemId, permissionRule)
const permissionRuleId2 = createPermissionRule(storeId, itemId, permissionRule2)

. . . 

function permissionRule (digitalCurrencyAccount: DigitalCurrencyAccount): boolean {
  const isPermittedOperation = false

  // your rules here

  return isPermittedOperation
}

. . . 

function permissionRule2 (digitalCurrencyAccount: DigitalCurrencyAccount): boolean {
  const isPermittedOperation = false

  // your rules for other conditions you'd like to satisfy

  return isPermittedOperation
}

. . . 


// delete a permission rule for the digital currency account with a specific account id

// delete the second permission rules . . 

deletePermissionRule(storeId, itemId, permissionRuleId2)


. . . 




