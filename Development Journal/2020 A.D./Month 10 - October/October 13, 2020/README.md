


# Development Journal Entry - October 13, 2020


[Written @ 17:40]

Notes @ Development Issues

The vuetify snackbar component . . is . . not always showing at the bottom of the page . . sometimes . . the component . . isn't visible . . where it would otherwise . . be expected to be . . at the bottom . . of the page . . 

For example . . on . . a mobile . . display . . scrolling . . up and down . . results . . in . . the . . snackbar . . component . . being . . hidden . . when . . you scroll up . . uh . . and . . well . . sometimes . . the snackbar . . component . . is visible . . at the bottom of the page . . as is . . expected . . but . . when you scroll up . . some time later . . then . . maybe . . only half of the component . . (the top half) . . is visible . . or maybe . . none of the component . . is visible . . and so . . it's like . . scrolling up . . leaves the component . . at the bottom of the page . . 


https://dev.vuetifyjs.com/en/components/snackbars/ 

[Written @ 17:24]

Notes @ Development Delays
Notes @ Development Issues


<v-snackbar bottom right v-model="showUniversalBasicIncomeNotification" :timeout="-1" class="universal-basic-income-notification">
        <DigitalCurrencySymbol />
        <span>A Universal Basic Income Has Been Distributed.</span>
        <template v-slot:action="{ attrs }">
          <v-btn icon v-bind="attrs" @click="showUniversalBasicIncomeNotification = false">
            <v-icon>close</v-icon>
          </v-btn>
        </template>
      </v-snackbar>


Add an v-slot:action="{ attrs }" for the close button . . seems to . . not always . . show the . . snackbar . . component . . at . . an appropriate . . width . . on mobile displays . . 

The width . . of the . . snackbar . . component . . overflows . . . .




