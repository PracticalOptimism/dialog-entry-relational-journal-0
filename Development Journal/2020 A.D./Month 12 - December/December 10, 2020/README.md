

# Development Journal Entry - December 10, 2020

[Written @ 23:51 - December 11, 2020 @ 0:23]

I wrote . . in my physical material paper notebook . . a few days ago . . 

The thoughts were relating to . . how I've been feeling recently about the work I'm doing arounnd building . . or constructing . . or developing . . the project . . service . . website . . 

(1) I am familiar with developing the project website by using . . examples of other websites . . I have taken . . inspiration from websites . . like . . YouTube . . uh . . Facebook . . Twitter . . uh . . Discord . . Slack . . Fox News . . Twitch . . Amazon . . Alibaba . . There are so many websites on the internet . . and I'm always open to learn more about what was . . the inspiration for this and that feature . . and why . . does this or that 

-__- hmm I think maybe writing some of this message is quite strange -_- . . I think I felt different when I was doing these things . . on my own . . without supervision of my present . . future . . self . . consciousness who is wanting to maybe glorify the effects . . by supposing something like . . effect being applied -_- . . mostly when I was looking at websites . . I was using them as well . . and I . . didn't always have a conscious attention to the description of the website . . it was . . navigating using my feelings to see why I was there . . 

With YouTube . . I've always used the website for a lot of things . . -_- . . well mostly watching videos -_- . . but uh . . -_- . . I guess maybe I could say instead . . that . . I learned a lot of things . . and those all felt like different adventures . .

A lot of adventures . . 

(2) . . after all those adventures . . now . . -_- . . I've been . . inspired . . by the work . . of spiritual teachers . . like . . Seth . . and Jane Roberts . . and Robert F. Butts . . and the Pleiadians . . uh . . The mind creates reality . . or you create reality with your conscious thoughts and feelings . . something like that . . 

Creation . . is an interesting . . program . . 

There seems to be . . endless abilities . . 

I worked on the troy architecture . . and . . was inspired . . to think about . . -_- . . -_- . . -_- . . -__- information organization patterns . . or something like that . . how to organize a project codebase . . something like that . . 

A few programmatic principles . . emerged from that . . effort . . in terms of . . naming conventions . . for things . . things . . uh . . and ideas around how to organize the program . . for . . a . . text-based . . codebase . . or something like that . . for a hierarchical file-system . . based . . program representation . . something like that . . in terms of . . how do you . . organize the directory tree . . and what names do you provide . . the . . directory . . tree . . items . . ? . . the files . . the folders . . or directories . . uh . . 

A structure . . that can be applied across . . any project . . codebase . . would . . be . . ideal . . for allowing . . development teams . . to . . migrate . . quickly from one . . codebase . . to another . . without loss of efficiency . . in terms of their familiarity . . with the codebase . . structure . . and so even if the usecase . . for the project codebase . . changes . . uh . . across . . various . . different project codebases . . if uh . . for example . . one codebase . . does this or that thing . . or provides this or that feature . . uh . . another codebase . . uh . . could provide unique . . and different . . features . . and . . the program could still be represented . . in a way . . that is uh . . apparent to new . . development . . team . . members . . and so for example . . new development team members . . can look at the codebase . . and be familiar with the . . ideas that are trying to be . . expressed . . uh . . because the format of how the ideas are trying to be expressed are similar to the format of other ideas from other codebases that were considered . . by the development team member . . and so for example . . file names . . appear in a common naming convention . . or a common naming scheme . . and . . directory structures . . are expected to follow a . . certain . . development pipeline . . and don't sqweue . . to the left or to the right . . by some arbitrary measure . . that uh . . hasn't been accounted for . . by the development program . . or the development guideline for how the project codebase is . . expected to develop . . under various circumstances . . and so . . there are no surprising . . uh . . ignorances . . that . . one needs to take into account . . 

-__-_-_-_-_-_-_-_-_-__--_-----_-__-_-_-_

The development process of a program can be predicatable . . uh . . and so even in . . circumstances . . that would have otherwise seemed . . difficult to organize . . like maybe a new usecase is considered that wasn't taken into account at the origin of the development of the project architecture . .

well . . the architecture can yet be updated . . in a . . conventional way . . -_- . . maybe -_- . . I don't really know . . I'm making this part up  . . as I write this . . maybe for exmaple . . this is a theoretical group of possibilities . . theoretical to me in my timeline of events and observations and yet practical to those that would meet those thoughts and practical those ideals

. . 

-_- . . 

Okay . . well . . 

There are advantages to having a theory . . 

-_- . . On the one hand . . if you have a list of examples . . to . . try to mimic . . or try to . . copy . . that is quite useful . . 

On the other hand . . if you have a theoretical framework . . for the development of a program . . that . . is also . . very useful . . 

. . 

-_- . . 

. . . 




