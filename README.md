# Dialog Entry Relational Journal 0

## About

A **Dialog Entry Relational Journal** talks about the needs and topics to discuss . 

The journal can be for personal purposes or for social dialog . 

## YouTube Live Stream

Jon Ide YouTube

https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g/videos

## Realtime Day Journal Updates
[Day Journal](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal)

## Realtime Development Journal Updates
[Development Journal](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal)

## Realtime Dream Journal Updates
[Dream Journal](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Dream%20Journal)

## Inventions and Ideas

Folder Name: /[Development Journal - Technological Ideas](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas)

https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Development%20Journal%20-%20Technological%20Ideas

## Book Library

You can access the books from `Book Library` directory at:

https://drive.google.com/drive/folders/1qjYXtkvfnPomhfBOKuQc_A_H_O1ickJt?usp=sharing


## Resources

* [Live Stream Checklist](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Websites/YouTube/YouTube%20Live%20Streams/Jon%20Ide/2020%20A.D./Month%209%20-%20September/%40%20Live%20Stream%20Information/Live%20Stream%20Checklist)

* [Notes, Images, Videos, Books and More](https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE?usp=sharing) 👈 🔄 For realtime notes updates, visit here


## Missing YouTube Live Stream Episodes

* **Ecoin #400** - December 13, 2020
  - 🎬 Video Summary Through ["Ecoin #402 - December 15, 2020"](https://www.youtube.com/watch?v=Eb8_8gP3v0k)
  - 🚫 Blocked by Copyright Claim
  - 💭 May Republish with trimmed video

* **Ecoin #354** - October 31, 2020 (Happy Halloween! 🎃)
  - 🎬 [Video Resource](https://drive.google.com/drive/folders/1VXQEOyV5s7YmtCwZaHct0z6Qx6qXJ4jA)
  - 🚫 Blocked by Copyright Claim
  - 💭 May Republish with trimmed video

* **Ecoin #1** to **Ecoin #283** . . . I need to review these episodes to ensure my `physical address location` isn't available . . to preserve . . my . . privacy :O I'm sorry . . [Ecoin #284](https://www.youtube.com/watch?v=fa_2y4mRZXA) addresses this topic

## Contributors

Jon Ide

## References

## License
MIT

