
# Gitlab Issue Format

-- development related issues
* Author's Checklist [1]
* Related Issues [1]
* References [1]
* Implementation Plan [2]
* Linked Issues [2]
* Research Goals [3]

-- community related issues [4]
* Summary
* Steps to reproduce
* Example Project
* What is the current bug behavior?
* What is the expected correct behavior?
* Relevant logs and/or screenshots
* Output of checks
* Results of [Project Name] environment info
* Results of [Project Name] application Check
* Possible fixes

-- done automatically ? 
* Is Blocked By [2]
* Relates to [2]
* Blocks [2, 3]

--




-- Resources
[Development Journal @ September 24, 2020]



[1]
Docs post-merge review: Document GitLab.com puma request timeout
By Thong Kuah
https://gitlab.com/gitlab-org/gitlab/-/issues/255879


[2]
As a user I do not want to see hubble related alerts from Operations -> Alerts page
By Zamir Martins Filho
https://gitlab.com/gitlab-org/gitlab/-/issues/255751


[3]
Spike: Cilium and Hubble integration
By Thiago Figueiró
https://gitlab.com/gitlab-org/gitlab/-/issues/238142


[4]
By GAO Bohan
https://gitlab.com/gitlab-org/gitlab/-/issues/255885


