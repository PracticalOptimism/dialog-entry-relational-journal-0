#!/bin/bash

# start command (copy-paste to command line):
# sh ./item.sh

cd ../../../../

number = 1

while true
do
  sleep 3 # every 3 minutes
  echo $number
  number=$(( number+1 ))

  git add . 
  git commit -m "update dialog relation entry journal"
  git push origin master
done
exit 0


