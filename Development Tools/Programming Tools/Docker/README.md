


Docker Commands

# run a docker image
docker run -d -p 80:80 docker/getting-started

# push a new image to the docker repository
docker tag local-image:tagname new-repo:tagname
docker push new-repo:tagname




Docker Resources

# docker playground

https://labs.play-with-docker.com/





