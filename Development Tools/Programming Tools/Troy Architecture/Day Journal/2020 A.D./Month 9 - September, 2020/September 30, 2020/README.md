

# Day Journal Entry - September 30, 2020

[Written @ 23:46]

/blog-post
  - /-as-created-by-user-123
    - /-as-created-on-date
      - /time
        - /-for-year 
          - /-for-month
            - /-for-day

hmm . . well . . I'm still experimenting . . with this idea . . and so . . uh . . well . . uh . . I'm not sure . . 

uh . . 

yea . . I'll have to come back and think about this . . uh . . some other time . . 

uh . . well . . I'll also . . mention . . that . . uh . . I'm still not sure . . how to uh . . specify . . the leaf nodes . . or the files . . in each of these directories . . 

uh . . should the file be . . index.file-type ? . . uh . . for example . . uh . . with the ecoin . . project . . I'm working on . . uh . . well . . 

index.ts . . . is a common file type . . 
expectation-definitions.ts . . is another common file type . . 

. . uh . . file type . . uh . . being . . uh . . maybe . . like . . a uh . . common naming . . convention . . uh . . for the files . . uh . . well . . I really haven't . . uh . . figured out . . what those uh . . common names . . or standards should be . . these are what I've been working with so far . . and have been . . quite okay with them . . uh . . 

well . . in any case . . I think . . I've also been playing . . around with . . uh . . this . . sort of . . file naming system . . but am not really sure . . how that would work out . . in practice . . 


- service
- service account

- item
- item list
- item infinite loop
- item store

- permission rule
- event listener

- network protocol member
- network protocol message
- network protocol connection


. . . 



[Written @ 23:31]

. . . 

well . . those have been my thoughts . . so far . . on uh . . a lot . . of . . uh . . this topic . . around . . how to name . . directories . . uh . . well . . the idea . . is that . . maybe . . you want to name directories . . uh . . in a way . . that makes it uh . . possible . . to . . uh . . uh . . uh . . speak . . uh . . the name . . uh . . like . . maybe . . uh . . reading a book . . 

. . uh . . even if the . . number of directories . . extends . . uh . . for example . . to the length of the words . . in a book . . or something . . uh . . a very long book . . uh . . well . . if it's easy to . . uh . . continue . . reading . . from where . . you left off . . that is . . quite a . . convenient . . ability . . uh . . so them . . uh . . as . . long . . as you have . . uh . . uh . . 

if you want to keep writing . . you can do that . . and it doesn't necessarily need to be the case . . that you need to re-write . . your program . . to satisfy . . uh . . this . . or that . . uh . . condition . . uh . . if . . uh . . you . . want to extend . . your uh . . uh . . book . . program . . uh . . uh . . hmm but . . -__ . . uh . . right . . well . . uh . . right . . if the directory . . tree . . is like . . uh . . maybe . . reading a book . . uh . . in terms of . . uh . . relating to the information . . uh . . specified  . . uh . . in the program . . uh . . directory . . or . . uh . . the project directory . . well . . maybe that's quite useful . . 

uh . . well . . I'm not really sure if it's possible . . but . . theoretically . . maybe it's possible to have an architecture . . where all the programs . . on the internet . . are presented . . uh . . in a single . . data type . . or . . a single . . uh . . data structure . . and so . . no matter . . how many new . . programming languages . . are . . created . . or . . uh . . the . . formates . . of files . . that are developed . . and so . . uh . . the directory . . tree . . uh . . or the project . . codebase . . tree . . or project codebase . . directory . . could be updated in realtime . . from . . uh . . multiple . . sources . . and . . uh . . be able to satisfy . . a lot of . . uh . . nested . . uh . . information . representation . . uh . . without . . necessarily . . overlapping one another . . uh . . well . . that's the theory . . uh . . well . . hmm . . dates . . for example . . are a really . . popular way . . to segment information . . so for example . . uh . . I suppose . . one could have . . part of the directory tree . . look like this . . 

/blog-post
  - /-as-created-by-user-123
    - /-as-created-on-date
      - /time
        - /-for-year // ? ? ? hmm . . I haven't figured out . . uh what files should go in these directories . . uh . . one moment . . 
          - /-for-month // ? ? ? 
            - /-for-day // ? ? ? 


      - /year-specification // ???? i'm really not sure uh . . 
      - /month-specification // these names are uh maybe uh . . 
      - /day-specification // not standard or something . . /shrug


Notes @ Newly Created Words

edevelop
[Written @ September 30, 2020 @ approximately 23:38]
[I accidentally typed the word . . "edevelop" . . instead of the word . . "develop" . . I'm not sure . . what this word . . "edevelop" . . should mean at this time . . I'm sorry . . ]

[Written @ 23:06]


Well . . uh . . 


getAccount.byAccountId.fromFirebase.basedOnJavaScript()

getAccount.byAccountId.fromFirebase.basedOnJava()

. . having . . a directory . . uh . . structure . . that allows . . for . . an . . arbitrary . . number . . of . . uh . . modifiers . . uh . . for any . . uh . . given . . entry . . or . . uh . . I suppose . . uh . . uh . . file . . or . . uh . . file directory . . or . . file directory . . entry . . or . . something uh . . like that . . uh . . 

well . . for example . . 

uh . . uh . . you could . . keep . . all of the . . variations . . of a function . . implemented in several . . programming languages . . uh . . in the same directory . . or in the same . . project . . directory . . hierarchy . . and . . be able to keep track . . or reason . . about . . where things are . . or how things are related to one another . . in this large . . uh . . . infinitely . . expandable . . directory . . structure . . 

uh . . for example . . 


/precompiled
  - /usecase
    - /digital-currency
      - /algorithms
        - /digital-currency-account
          - /get-account
            - /-by-account-id
            - /-by-account-username
            - /-by-text-search
              - /-based-on-database-provider
                - /mongodb
                - /my-sql
                - /firebase
                  - /-based-on-programming-language
                    - /javascript
                      - /-for-web-browser-environment
                        - /-for-green-browsers
                        - /-for-ie-version-8
                          - /-as-specified-by-javascript-developer-123
                            - /-in-agreement-with-the-proposal-123
                              - /-in-consideration-of-this-instruction-set
                              - /-in-consideration-of-this-group-of-parameters
                              - /-in-consideration-of-this-group-of-people
                              - /
                            - /-in-agreement-with-the-identity-matrix-structure-586
                            - /-in-assumption-of-condition-X-being-satisfied
                            - /-in-assumption-of-condition-Y-being-satisfied
                            - /-in-assumption-of-condition-Z-being-satisfied
                          - /-as-specified-by-javascript-developer-234
                          - /-as-specified-by-javascript-developer-345
                      - /-for-nodejs-environment
                        - /-for-node-version-8-and-prior
                        - /-for-node-verison-12-and-prior
                      - /-for-deno-environment
                      - /-for-electron-application-environemnt
                      - /-for-environment-A
                      - /-for-environment-B
                    - /java
                    - /go
                    - /rust
                    - /haskel


. . . 


get-account
-by-text-search
-based-on-database-provider
firebase
-based-on-programming-language
javascript
-for-web-browser-environment
-for-ie-version-8
-as-specified-by-javascript-developer-123
-in-agreement-with-the-proposal-123
-in-consideration-of-this-group-of-people

. . . 

getAccount.byTextSearch.basedOnDatabaseProvider.firebase.basedOnProgrammingLanguage.javascript.forWebBrowserEnvironment.forIEVersion8.asSpecifiedByJavascriptDeveloper123.inAgreementWithTheProposal123.inConsiderationOfThisGroupOfPeople()

. . . 

const getAccountAlgorithm = getAccount.byTextSearch.basedOnDatabaseProvider.firebase.basedOnProgrammingLanguage.javascript.forWebBrowserEnvironment.forIEVersion8.asSpecifiedByJavascriptDeveloper123.inAgreementWithTheProposal123.inConsiderationOfThisGroupOfPeople

getAccountAlgorithm()

. . . 

. . . 


. . 

here are some . . uh . . modifiers . . to help us . . 

- -while-also-using
- -as-structured-by
- -as-specified-by

. . . 


. . .

modifiers

- -using
- -by
- -from
- -for
- -based-on
- -as
- -while


[Written @ 22:25 - 23:06]

[Written @ 22:09]

> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > . . . // and so on . . 


This directory structure representation . . uh . . could map . . to . . something like . . 

/precompiled
  - /usecase
    - /digital-currency
      - /algorithms
        - /digital-currency-account
          - /get-account
            - /-by-account-id
              - /-from-firebase
                - /-based-on-javascript
                - /-based-on-java
                  - . . . // and so on . . for how every many layers we prefer . . or however . . many layers . . our codebase . . uh . . requires us . . uh . . to . . specify . . information . . or something like that . . 

getAccount.byAccountId.fromFirebase.basedOnJavaScript()

getAccount.byAccountId.fromFirebase.basedOnJava()

getAccount.byAccountId.fromFirebase.basedOnJava.< and more . . >.< and more . . >.< and more . . >.< and more . . > . . . and more . . and more . . 

. . . 

getAccount.byAccountId.fromFirebase.basedOnJava.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier.anotherModifier. . . and so on . . . . . .theLastModifier() // this is a function call

. . . 

. . . "for as long as we can speak . . " . . is a message . . that . . I heard . . in my mind today . . when . . thinking . . about . . this topic . . of . . uh . . uh . . how . . uh . . can . . we . . represent . . or . . really . . uh . . uh . . when . . we're typing . . or . . having . . the names . . of a . . file . . in . . a directory . . uh . . what names should we give those files ? . . is there a standard way . . we can name . . each of the files . . in the directory . . uh . . theoretically . . even if you have a file . . uh . . in the directory today . . tomorrow . . that file . . uh . . may . . get have . . uh . . more content . . and . . uh . . well . . then . . converting that . . file . . into . . a directory . . is a very . . useful . . uh . . thing . . to be able to do . . uh . . well . . if . . uh . . there are . . thoughts . . around this topic . . maybe . . that can be the case . . so that . . files . . can easily . . become . . directories . . or . . directories . . can become . . files . . uh . . if . . uh . . for example . . we want to . . expand . . on . . the . . content . . of a particular . . file . . or . . if we want to . . uh . . provide . . a reference . . to other content . . or another directory . . by . . conveting . . the directory . . into . . a . . file . . . . uh . . a file . . data strcut

. . . well . . I haven't really answered the question . . of . . how can we . . name the files . . or . . the . . leaf nodes . . of a . . directory structure . . uh . . in this . . architectural . . uh . . format . . that we've been talking about here . . so far . . with the . . troy architecture . . 

Uh . . mostly . . I'm still experimenting . . 

One idea . . is to name the files . . . 

- index.file-type // ie. index.ts, index.png, index.mp4, index.genre
- expectation-definitions.file-type // ie. expectation-definitions.ts, expectation-definitions.cpp, expectation-definitions.ls

. . . 

uh . . well . . I'm not sure . . uh . . I've been . . uh . . thinking . . about . . other . . file names . . uh . . well . . for example . . having . . the files . . uh . . reflect . . the . . topics . . discussed by the database . . uh . . data structures . . uh . . well . . those data structures are . . 


- service
- service account

- item
- item list
- item infinite loop
- item store

- permission rule
- event listener

- network protocol member
- network protocol message
- network protocol connection

. . . 

Well . . uh . . these are the uh . . main . . uh . . or . . data structures . . that . . I've been thinking about . . when I think about . . database . . operations . . uh . . database operations . . with respect to uh . . database . . data structures . . uh . . well . . uh . . 

so . . if . . uh . . these things . . uh . . well . . let's see . . here's a list of . . things . . that I wrote . . down . . a few . . uh . . months ago . . I have them written here . . in my . . physical paper material notebook journal . . the . . date . . that I have logged . . is . . "Development Journal @ April 30, 2020" . . and so . . well . . uh . . here are those ideas . . 

- documentation.ts
- expectation-definitions.ts
- index.ts
- access-control.ts
- validator.ts

. . . 

/algorithms
  - /get-something
    - /get-something
    - /expectation-definitions
    - /validators
    - /access-control
    - /documentation
    - /run-algorithm
    - /performance

. . . 

- /unitary-expectation-definitions // like . . unit tests
- /formal-method-expectation-definitions // formal methods
- /integration-expectation-definitions // integration tests
- /customer-feedback-expectation-definitions // customp

. . . 

Those are the notes . . uh . . . related . . to . . how to name files . . uh . . or . . uh . . right . . those are the notes . . but this is probable . . a more . . clear . . example . . uh . . I'll modify . . the notes . . here . . and so . . it's . . uh . . more like uh . . well . . uh . . in the notes . . I wrote . . directories . . for one of the examples . . here . . I'll write the directories . . as files . . 

/algorithms
  - /get-something
    - /get-something
    - expectation-definitions.ts
    - validators.ts
    - access-control.ts
    - documentation.ts
    - run-algorithm.ts
    - performance.ts

. . . and so . . for example . . 

`expectation-definitions` . . have to do with . . specifying . . what we expect . . of the . . index.ts . . uh . . for example . . how do we expect the program to behave . . and so . . these expectations . . uh . . can be . . uh . . programs . . that can be . . run . . uh . . to measure . . that . . uh . . our expectations are being met . . which . . is uh . . I suppose . . uh . . the same idea . . uh . . as . . uh . . unit tests . . uh . . I was inspired by unit tests . . uh . . but . . wanted to think of . . another name . . to call uh . . the idea . . that unit tests are . . uh . . supposing to suggest in . . uh . . maybe . . a more . . general . . uh . . notion . . or a more general concept . . uh . . well . . I'm uh . . happy with the result of the word . . "expectation definitions" . . uh . . as it seems to be . . a . . uh . . word . . or . . uh . . word phrase . . that . . can . . uh . . be very widely . . applicable . . even . . across . . disciplines . . like . . biology . . and physics . . uh . . or even mathematics . . when . . we expect . . uh . . structures . . to behave in a certain way . . uh . . we can . . measure those expectations . . uh . . repeatedly . . and . . uh . . well . . check to see if they are in fact . . uh . . still being . . uh . . validated . . by . . uh . . our uh . . perspectives . . or . . uh . . our observation . . of the world . . or something like that . . uh . . I'm not sure . . but . . uh . . yea . . well . . it's uh . . an idea . . and . . uh . . I'm happy to uh . . think more about this topic . . uh . . if things aren't really as . . uh . . maybe . . thoughtful . . or applicable . . as I had originally thought . . or something like that . . something something . . [something something . . uh . . that's a word . . in the notes . . uh . . please see the definition . . to learn more . . it's uh . . like . . "i'm not sure" . . or . . uh . . "it's a work in progress" . . or uh . . "yea . . I've said a lot of things here . . but don't know the details . . and so . . I'm uh . . going to say . . uh . . something something . . " . .]

. . . 



Notes @ Newly Created Words

writtei
[Written @ September 30, 2020 @ approximately 22:43]
[I accidentally typed . . the word . . "writtei" . . instead of the word . . "written" . . I'm sorry . . I'm not sure . . what this word . . "writtei" . . should mean . . at this time . . ]

customp
[Written @ September 30, 2020 @ approximately 22:43]
[I accidentally typed the word . . "customp" . . instead of . . "customer" . . as in the . . word . . phrase . . "customer . . feedback" . . uh . . well . . I'm not sure what this word . . "customp" . . should mean at this time . . I'm sorry . . ]

defintioin

Notes @ Newly Created Words

filt
[Written @ September 30, 2020 @ 22:31]
[I accidentally typed the word . . "filt" . . uh . . instead of the word . . "file" . . I'm not sure what this word . . "filt" . . should mean . . at this time . . ]



Notes @ Newly Created Words


strcut
[Written @ September 30, 2020 @ 22:17]
[I accidentally spelled the word . . "structure" . . as in . . "data structure" . . as . . "strcut" . . I'm not sure . . what this word . . "strcut" . . should mean . . at this time . . I'm sorry . .]
[While writing the words . . "Newly Created Words" . . I accidentally typed . . "Newly Created Fi" . . as in . . it seemed like I was preparing to type . . "Newly Created Files" . . uh . . well . . strcut . . could be a type of file I suppose . . uh . . I'm not . . sure . . on what the file type . . should do . . uh . . but it seems interesting . . uh . . well . . uh . . this typographical error . . of almost . . uh . . written . . "Files" . . in . . "Newly Created Files" . . gave me an idea . . on what this . . "strcut" . . could mean . . uh . . maybe it has something to do with . . "cutting" ? . . . . it's quite an interesting name . .]


[Written @ 21:31]

for as long as you can speak . . 

- /digital-currency
  - /algorithms
    - /get-account (1)
      - /-by-account-id (2)
        - /-from-firebase (3)
          - /-based-on-javascript (4)
          - /-based-on-java

. . . 

get-account-by-account-id-from-firebase-based-on-javascript

. . . 

(1) . . . this is a verb . . 'get-account'
(2) this is . . a . . modifier . . '-by-account-id'
(3) this is . . a . . modifier . . '-from-firebase'
(4) this . . is . . a modifier . . '-based-on-javascript'


(1) // verb
(2) // modifier
(3) // modifier
(4) // modifier

. . . 

Imagine . . . a directory tree . . representation . . format . . as follows . . 

> > > 

. . . 

which . . represents . . 3 . . directories . . 

One . . directory . . inside of another . . in that order . . and so . . . in practice . . this . . directory . . can look as follows . . 


. . . 

/directory-1
  - /directory-2
    - /directory-3

. . .

. . each directory . . is . . nested . . inside . . the previous directory . . uh . . well . . this . . is only an experimental . . directory representation format . . for example . . I haven't played around with . . how . . to represent . . multiple . . directories . . in the same . . directory . . location . . or . . uh . . how to represent . . the . . branching . . of . . directories . . into . . further . . . . uh . . child . . directories . . or something like that . . uh . . or . . directed graph . . entry point nodes . . or something like that . . 


. . . uh . . well . . 

> > > > > > > > 

is a directory representation . . that . . maps to . . 8 . . directories . . each . . one . . nesting . . inside . . the previous . . directory . . uh . . 

/directory-1
  - /directory-2
    - /directory-3
      - /directory-4
        - /directory-5
          - /directory-6
            - /directory-7
              - /directory-8

. . . 

. . . well . . 


we can continue by  . . . offering . . a way to view . . directories . . of . . very large amounts . . because . . this representation format . . is quite . . easy to work with . . uh . . and to type out . . each of the . . directories . . only takes . . uh . . not necessarily . . as much . . effort . . as typing . . out the . . tree . . uh . . formatting . . uh . . for the directories . . with . . each of their names . . uh . . so . . 


> > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > 

/directory-1
  - /directory-2
    - /directory-3
      - /directory-4
        - /directory-5
          - /directory-6
            - /directory-7
              - /directory-8
                - /directory-9
                  - /directory-10
                    - /directory-11
                      - /directory-12
                        - etc. etc. 


. . . this is uh . . quite a deep . . tree you could call it . . or . . the . . uh . . directory . . tree . . is . . quite . . tall . . uh . . sometimes . . the . . uh . . depth . . of the . . uh . . tree-like data-structure . . uh . . of . . a directory . . uh . . or . . uh . . file system . . hierarchy . . system . . is . . uh . . talked about . . in terms of . . the "height" . . of the . . "node" . . in the . . "tree" . . or . . the . . "depth" . . of the . . "leaf" . . or . . "leaf node" . . or . . "leaf element" . . in the "tree" . . uh . . . well . . in this case . . it may not be important for us to know . . what that number . . is . . or . . what that . . uh . . value . . is . . 

Uh . . well . . in thinking . . about the topic . . of . . data structure . . or . . uh . . project . . codebase . . data structure . . or project . . codebase . . architecture . . we . . uh . . well . . the topic . . of . . how deeply . . we can nest things . . in a directory . . tree like format . . and still have the program be . . readable . . or be . . uh . . understandable . . just as . . for example . . the . . more shallow . . or . . less . . tall . . uh . . nodes . . of the tree . . can be . . uh . . easy for people to . . uh . . read . . and work with . . since maybe those things . . are . . uh . . more on top of people's minds . . and they are familiar . . with . . working with those . . uh . . levels . . of the tree . . uh . . well . . it's . . uh . . maybe . . also . . a thoughtful thing to do . . to see if the . . uh . . architecture . . or . . uh . . data structure . . uh . . in this case . . we're . . talking a lot about . . nested tree-like data structures . . which are uh . . how . . a lot of . . project . . codebases . . or project codebase . . data structures . . are . . represented . . 


. . . 



. . . 



Notes @ Newly Created Words

faim
[Written @ September 30, 2020 @ 21:56]
[I accidentally typed the word . . "faim" . . instead of the word . . "familiar" . . I'm not sure what this word . . "faim" . . should mean at this time . . I'm sorry . . ]


fami
[Written @ September 30, 2020 @ 21:56]
[I accidentally typed the word . . "fami" . . instead of the word . . "faim" . . I'm not sure what this word . . "fami" . . should mean . . at this time . . I'm sorry . . ]


Notes @ Newly Created Words


fortuerh
[Written @ September 30, 2020 @ 21:45]
[I accidentally typed the word . . "further" . . as . . "fortuerh" . . I'm not sure what this word . . "fortuerh" . . should mean . . at this time . . I'm sorry . . ]

Notes @ Newly Created Words

furhter
[Written @ September 30, 2020 @ 21:45]
[I accidentally typed the word . . "further" . . as . . "furhter" . . I'm not sure what this word . . "furhter" . . should mean . . at this time . . I'm sorry . . ]


. . . 

- -while-also-using
- -as-structured-by
- -as-specified-by

. . . 


. . .

modifiers

- -using
- -by
- -from
- -for
- -based-on
- -as
- -while

. . . 

- nouns (identifier)
- modifier
- verbbs (action)

parts of speech:
- English
- Many Languages (Chinese, Japanese, French, etc.)



[Written @ 17:06 - 17:48]

notes from my physical paper material notebook journal

/algorithms
  - /get-account
    - /-by-account-id
      - /-from-firebase // (1)
        - /-based-on-java
          - /-based-on-java-version-9
          - /-based-on-java-version-10


(1) still thinking about this one . . from or based-on . . '-based-on-firebase' is my preference, yet still this '-from-firebase' is a possibility . . 

// many different modifications or verions of get account for this usecase [are possible]


// how to create topics for `get-account`

* metrics / statistics on `get-account`
* create/get/update versions of `get-account`
* 
* 


* get-account // this topic has its own history . . [like an account . . would have its own history . . an . . algorithm . . for . . operating . . on . . an . . account . . would . . also . . have . . its . . own . . history . . ]
  * modifiers
  * statistics


properties:
[properties for `get-account`]:

* code
* parameter
* code environment
* output
* number of calls [number of function calls]
* number of errors [number of errors resulting in function call returns]

. . . 

* get-get-account-parameter-history-list
* get-get-account-output-history-list
* create-get-account-algorithm
* update-get-account-algorithm

. . . 

* get-get-account-algorithm
  * get-get-account-algorithm-date-created
  * get-get-account-algorithm-parameter-history-list
  * get-get-account-algorithm-output-history-list
* create-get-account-algorithm
* update-get-account-algorithm
* delete-get-account-algorithm


// the get account algorithm . . is defined . . as a class

. . . 

class GetAccountAlgorithm {
  getAccountAlgorithmId: string = ''
  dateCreated: Date = new Date()


  // the source code of the algorithm . . can be a variable . . in this . . class . . data structure . . so the source code can be . . updated . . at runtime . . . . and . . different . . versions . . of the algorithm . . can be instantiated . . as . . needed . .

  sourceCode: string = ``

  // or . . 

  sourceCodePart1: string = ''
  sourceCodePart2: string = ''
  sourceCodePart3: string = ''

  // or . . 

  sourceCodeList: string[] = []

  // or . . 

  sourceCodeVariableTree: { [sourceCodeId: string]: string } = {}
}


. . . 

class Algorithm {

  sourceCodeList: string[] = [
    // import statements . . are like . . network protocol . . objects . . where . . you're interested . . in . . accessing . . a message . . from a certain . . network . . protocol . . connection . . and . . if you're on your local . . computer . . the network protocol connection . . can be . . the local . . file system . . and so . . the . . network . . messages . . are files . . that you want to . . read and write from . . 
    // if you're using the internet . . the . . network protocol . . can be . . tcp/ip . . based . . network . . protocols . . where you want to read and write messages from . . the internet . . uh . . web servers . . or . . uh . . other . . internet . . participants . . uh . . through . . a peer to peer network . . or something like that . . 
    // network protocols . . can also be between . . react . . or . . vuejs . . or . . angular components . . in . . a javascript library . . or . . a javascript . . framework . . or . . something . . like that . . and so . . uh . . two-way data binding . . means . . something like . . both . . network protocol members .  . can . . communicate . . back and forth . . to one another . . whereas . . one-way . . data binding . . can mean . . that . . only . . one member can send messages to the other . . while . . the . . receiving . . member . . uh . . isn't . . necessarily . . permissioned . . or . . allowed . . to . . send messages back . . but . . can . . only . . listen . . 
    // network protocols . . are also . . uh . . part of programming languages . . like . . javascript . . where . . the message . . let helloWorld = 'hello world' . . sends . . a message . . to the computer . . to . . store . . the . . value . . of the string . . 'hello world' . . in the variable . . helloWorld . . . . and . . to . . receive messages from the computer . . uh . . you can . . uh . . get the value . . of a certain . . uh . . "location" . . or . . "memory address" . . of the computer . . uh . . memory address . . being a location . . uh . . maybe . . in analog to . . uh . . "website addrees" . . or . . "file address" . . or . . "file directory address" . . uh . . or . . "vuejs component id" . . uh . . and so . . uh . . these ids . . or addresses . . can be . . ways for . . retrieving . . information . . from those . . locations . . uh . . and . . uh . . for example . . it could be possible to listen to information . . at a memory address . . uh . . maybe . . the . . javascript ES6 . . syntax . . await . . is an example . . of . . uh . . listening . . for a message to resolve . . uh . . and . . the javascript function . . addEventListener . . can be like . . uh . . a network . . event listener . . like . . addMessageEventListener . . if we're listening . . to message events . . coming through from the internet . . or something like that . . uh . . well . . then . . maybe . . it's theoretically possible to have . . special file types . . on our computers . . that are . . uh . . like . . able . . to communicate with each other . . uh . . like . . uh . . addMessageEventListener(chromeBrowserFileId, messageTypeId, onMessageEventCallbackFunction) . . and so . . a function call like this . . can represent something like . . a file . . on the computer . . listening . . to wait . . or . . watch . . for an event . . or . . to listen to an event . . from the google chrome browser . . the google chrome web browser . . and so . . uh . . when the google chrome web browser . . publishes a message . . uh . . of a certain type . . then . . uh . . the file . . that's on our computer . . uh . . maybe . . something like . . file.hello-world . . the . . hello-world . . file type . . can . . uh . . respond . . by doing something . . uh . . maybe . . letting the user know . . that the chrome web browser . . did something . . uh . . something . . uh . . something like that . . maybe a console.log statement . . uh . . there are all sorts of possibilities . . 
  
    // (0) source code list item 0
    `import * as library from 'https://website.com/library'
    import * as library2 from 'https://website.com/library-2'
    import * as library3 from 'network-protocol://website.com/library-3'
    import * as library4 from 'dat://website.com/library-4'
    import * as library5 from 'local://website.com/library-5'
    import * as library6 from 'file://location/library-6.js'
    `,

    // (1) source code list item 1
    `
      // your source code goes here . . in this string . . 
    `,

    // (2) source code list item 2
    `
    export {
      item1FromTheSourceCode,
      item2FromTheSourceCode,
      item3FromTheSourceCode,
      item4FromTheSourceCode
    }
    `
  ]
}


Notes @ Newly Created Words

possion
[Written @ September 30, 2020 @ 17:10]
[I accidentally typed the word . . "possion" . . instead of . . "possible" . . I'm not sure what this word . . "possion" . . should mean . . at this time . . I'm sorry . . ]

[Written @ 15:12 - 15:25]


// Using functions a variable parameters . . . 

function helloWorld (callbackFunction: Function): any {}


// Using network protocol as a . . function . . type . . 

networked helloWorld


send message

receive message

. . . 

uh . . traditional . . javascript . . functions . . can use . . a . . . one way . . or . . uni-directional . . execution . . context flow matrix . . 

What if you had . . a type . . of function . . that . . has . . more than one . . execution . . context flow matrix . . or in other words . . the . . uh . . network . . uh . . messaging . . and . . receiving . . is . . aligned . . uh . . in several . . uh . . execution . . uh . . flows . . or something like that . . 

Uh . . so for example . . 

function helloWorld () {
  // line 1
  // line 2
  // line 3
  // line 4
  // line 5
  // etc. etc.
}

In this function . . the program . . is read . . one . . line . . at a . . time . . and . . so . . line 1, . . then . . line 2 . . then . . line 3 . . then . . line 4 . . then . . line 5 . . are read in that order . . uh 

In some sense . . each of these lines . . of the function . . can be . . perceived as . . a . . network protocol . . send message call . . 

And so for example . . 

function helloWorld () {
  // send the computer a message 1
  // send the computer a message 2
  // send the computer a message 3
  // send the computer a message 4
  // send the computer a message 5
  // etc. etc.
}

And so . . uh . . well . . the computer . . . Uh . . the physical . . hardware . . device . . uh . . with . . the . . already-installed program . . instructions . . that can . . interpret the message . . and do something with the message . . uh . . well . . the computer . . is receiving messages . . from the function . . and yet . . uh . . hmm . . 

what does it mean . . for . . uh . . the computer . . to return . . a message . . to the function . . [when I think about it now . . the computer . . sends messages back to the function . . and so . . these are . . for example . . the reutnr . . values of . . the functions . . or instructions . . that are . . uh . . specified . . by the computer program . . ]

hmm . . wait a minute . . I had a thought earlier . . and now . . I'm not sure . . if I wrote it down correctly . . uh . . well . . 

The idea is that . . the single . . flow of instructions for a program . . is . . uh . . well . . it's . . the single . . flow . . of instructions . . one line after another . . uh . . 

that could maybe be seen like . . a . . send message . . hmm . . . . . . hmm . . I'm not really sure . . where I was going with this thought . . I'll have to pick up . . from where I've written here . . I was mostly playing around to see what the possibilities could be . . uh . . hmm . . but now I'm not really sure . . what I was . . trying to achieve . . I'll come back to this . . idea . . or this topic later . . 

[Written @ 13:20]

"service" // 
"service account"

"item" // index.file-type
"item list" // optimized-version-of-index-file-type-list.optimized-file-type (1)
"item store" // index-store.file-type
"item infinite loop" // index-infinite-loop.file-type (2)
"permission rule"
"event listener"

"network protocol member"
"network protocol message"
"network protocol connection"






[Written @ 12:57 - 13:20]


"service" // 
"service account"

"item" // index.file-type
"item list" // optimized-version-of-index-file-type-list.optimized-file-type (1)
"item store" // index-store.file-type
"item infinite loop" // index-infinite-loop.file-type (2)
"permission rule"
"event listener"

"network protocol member"
"network protocol message"
"network protocol connection"



(1) like batch processing can improve performance of operations on large groups of items . . or large . . lists of items . . a special . . file type . . for a list of items . . can be . . specified . . for each item . . batch data storage . . or . . batch . . processing . . related . . operations . . 

(2) . . a data item . . a data item list . . a data item . . infinite . . loop . . is . . uh . . well . . maybe . . a . . natural progression . . and so . . a . . special . . uh . . file . . for . . creating . . an item of a certain kind . . indefinitely . . is . . an interesting . . concept . . I'm not sure . . how it would . . or . . could work . . in practice . . at this time . . although . . it . . seems . . like . . this could be a possibility . . I'm not sure . . if the hardware . . or . . uh . . physical computing . . devices we're using today . . will . . uh . . make this easy to do . . I suppose . . if we had . . other . . types of . . computing devices . . that . . made . . computing . . with . . infinite . . loops . . like . . moment point algorithms . . that . . uh . . well . . "moment point" is a concept . . talked about . . in the books by . . Seth . . and Jane Roberts . . and . . Robert Butts . . and . . well . . the idea relates . . so far . . as I've understood this topic . . "moment point" . . relates to . . uh . . time perception . . really being . . uh . . well . . there's only the . . now . . moment . . the . . "now" . . moment . . point . . or the . . "now" . . moment . . uh . . and because of . . human . . biological . . perspective . . uh . . our perception . . is . . uh . . translating . . the . . "now" . . moment . . point . . in . . uh . . infinite . . variety of ways . . uh . . well . . and that's what we experience . . as . . uh . . a . . linear . . sequence . . uh . . of a life or something like that . . life events . . and . . well . . I suppose . . according to Seth . . what they talk about in the book . . is . . the now . . moment . . uh . . holding all the events . . of . . physical . . experience . . and . . all the events . . of reality . . and . . dreams . . and . . related . . thoughts . . and . . constructs . . mental . . or physical . . constructs . . or . . spiritual constructs . . uh . . that's . . in a way how I've understood this topic . . uh . . well . . in the now moment . . uh . . uh . . well . . there exists . . a . . truly . . infinite . . variety . . of . . possibilities . . or . . uh . . possible . . events . . and its . . uh . . well . for example . . you can maybe . . have a computer . . that takes advantage of the . . fact that maybe . . in a single . . uh . . perceived moment in time . . the plant . . is making . . an infinite variety of . . uh . . decisions . . that may not be perceptible . . by . . uh . . people . . uh . . or . . experienced . . uh . . well . . there's . . the practical . . identifiable . . things . . like . . maybe . . the color of the plant . . if its green . . those qualities . . could perhaps . . be . . related to . . in a computational task . . and yet . . there are also . . things that are theoretical . . and which . . uh . . are theorized . . or . . hypothetical . . circumstances . . or hypothetical . . realities . . of the plant . . and so . . these could uh . . maybe be things that . . the plant . . uh . . for example is . . quote-on-quote . . "thinking about" . . in their own way of thinking . . however that means for the plant to "think" . . maybe isn't environmentally . . or easily . . translatable . . uh . . without the proper tools I suppose . . well . . in any case . . the thoughts that people . . individuals . . uh . . and groups . . have . . about the future . . where they would like things to go . . uh . . those may be . . theoretical . . constructs . . uh . . and yet . . they are also . . important . . for determining . . uh . . certain things . . about . . for example . . what you can expect . . a person to do . . or . . uh . . things of uh . . related interest . . uh . . something like that . . uh . . so . . maybe .  in  other terms . . the . . "personality" . . of a plant . . could be an interesting . . uh . . thing to measure . . and . . calculate . . to get more information . . on uh . . not only the practical . . observed things . . uh . . like . . the things . . that may be are immediately . . apparent . . when . . looking at a plant . . with . . uh . . human . . perception physical eyes . . but also . . if you are . . watching the personality characteristics . . the . . infinite . . diversity . . of the decisions . . that a plant made . . can also be taken into account when making computations . . or something like that . . uh . . and so . . moment point algorithms could be . . algorithms . . uh . . that . . rely on . . the infinite . . probabilities . . of events . . that . . have . . a practical reality . . in themselves . . uh . . and . . uh . . yea . . I'm not really sure what this means in practice . . for building a computer that takes advantage of these . . uh . . thoughts . . . . well . . uh . . maybe . . studying . . theoretical physics . . like . . quantum mechanics . . could help . . uh . . I'm still in the very . . early stages of . . uh . . studying this topic . . of . . moment . . point . . algorithms . . or the topic of a moment point . . in general . . 

I'm sorry . . I would . . suggest . . reading . . Seth's . . books . . to learn more . . 


[Written @ 12:53]

what if you could have . . these . . be . . the base . . uh . . leaf nodes . . or . . files . . in the . . base . . of . . a . . directory tree . . structure ? . . 

"service"
"service account"

"item"
"item list"
"item store"
"item infinite loop"
"permission rule"
"event listener"

"network protocol member"
"network protocol message"
"network protocol connection"



[Written @ 12:26]

terminology:

topic

topic site

topic site entry

topic site entry modifier

identity file


. . 

topic : a directory that contains . . "algorithms", "constants", "data-structures", "infinite-loops", "moment-point-algorithms", "variables" and other conceptual archetypes of a program

topic file : a file that is like a topic and contains the archetypes of a program such as "algorithms", "constants", "data-structures", "infinite-loops", "moment-point-algorithms", "variables" and more

. . 

topic site : a directory that is specified to be one of the conceptual program archetype for example . . "algorithms" . . is . . a topic site . . and . . "constants" . . is also a topic site . . and yet . . the list containing . . "algorithms" . . and "constants" . . is called a topic . . 

topic site file : a file containing . . the related topic site . . entry data . . for example . . algorithms.ts containing . . an algorithm . . or a set of algorithms . . 

. . 

identity file : a leaf node of a directory tree that is titled "index", "permission-rule", "expectation-definitions", "documentation"

ideas : . . 

"service"
"service account"

"item"
"item list"
"item store"
"item infinite loop" ? ? - I'm not sure what this means yet . . 
"permission rule"
"event listener"

"network protocol member"
"network protocol message"
"network protocol connection"


. . 


topic example

/movie // topic
  - /algorithms // topic site
  - /constants // topic site
  - /data-structures // topic site
  - /infinite-loops // topic site
  - /moment-point-algorithms // topic site
  - /variables // topic site


topic site example

/algorithms // topic site
  - /get-movie // topic site entry
  - /create-movie // topic site entry
  - /update-movie // topic site entry
  - /delete-movie // topic site entry
  - /get-movie-event-listener // topic site entry
  - /add-movie-event-listener // topic site entry
  - /update-movie-event-listener // topic site entry
  - /remove-movie-event-listener // topic site entry


topic site entry example

/get-movie // topic site entry
  - /-based-on-javascript // topic site entry modifier
  - /-based-on-go-lang // topic site entry modifier
  - /-based-on-java // topic site entry modifier
  - /-based-on-typescript // topic site entry modifier
  - /-based-on-rust // topic site entry modifier



. . . 

previous thoughts on leaf nodes . . for directories . . 

index.ts
permission-rule.ts // previously validator.ts or . . validation-rule.ts . . or . . access-controls.ts . . 
expectation-definitions.ts
documentation.ts

. . . 

. . .

expectation-definitions.ts . . seem like they could be considered . . as . . permission-rules . . and so . . for example . . the permission . . to create . . an . . account . . could be specified . . by the rules . . in the . . expectation definitions . . uh . . and then . . if you want to run . . an . . expectation-definitions . . measurement . . you could . . uh . . maybe . . have . . a . . run-algorithm.ts . . file inside . . of the . . permission . . rule . . directory . . but . . run-algorithm.ts . . is . . really another convention . . that I'm not really . . uh . . sure about yet . . the . . name . . is maybe . . uh . . maybe . . not . . uniformly . . accessible . . across . . the entire . . codebase . . 


. .  

. . 




[Written @ 12:21]


/update-account // directory
  - /update-account-username // directory
    - /-by-condition-1 // directory
      - index.ts // file
    - /-by-condition-2
      - index.ts
    - /-for-account-type-1
    - /-for-account-type-2
  - /update-account-name
  - /update-account-profile-picture
    - /-based-on-firebase
    - /-based-on-firebase-admin

/group
  - /group
    - /group
      - 




/update-account
  - /account

