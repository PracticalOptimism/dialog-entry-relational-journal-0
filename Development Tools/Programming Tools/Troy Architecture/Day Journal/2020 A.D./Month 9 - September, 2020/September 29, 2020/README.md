
# Troy Architecture

## Day Journal Entry - September 29, 2020


[Written @ 23:09]

# Algorithms

- ?? // retrieving an infinite loop . . get initialization snapshot
- ?? // updating an infinite loop 
- initialize item // or "start item"
- uninitialize item // or "stop item"

- get item
- update item
- create item
- delete item
- does exist item

- add item event listener
- remove item event listener

-- 

# Data Structures

- service
- service account

- item
- item list
- item store
- item event listener
- item permission rule

- network protocol member
- network protocol message
- network protocol connection





[Written @ 22:18]

sign up
sign in

register

create account

logout

. . . 

user related functions . . . 

. . . 

identity . . 

account . . 

. . . 

accountPermissionToken . . . 

. . . 


getAccountPermissionRule . . . 

. . . permission rule type
. . . permission rule id
. . . permission rule callback function 

. . . 

const permissionRule = createAccountPermissionRule(permissionRuleType, permissionRuleCallbackFunction)

const permissionRule = getAccountPermissionRule(permissionRuleId, permissionRuleType?: string)

. . . 

. . . to allow . . only having . . the permission rule id . . this could be a useful convention . . 


permissionRuleType:permissionRuleId

for example . . 

create:aIoKelwp83lfneiUYelriyf8

. . . for a permission rule . . on the rules for creating . . an item . . in the database . . 

a permission token . . for when you sign into an account for example . . uh . . 

well . . uh . . in general . . when you sign in to an account . . a lot of things can happen . . and maybe we don't want to assume . . so much . . on what those things are . . 

and yet . . uh . . having a name for . . when . . you . . want to call . . a . . . sign in function . . uh . . if we are working with the previously . . defined . . or previously talked about . . database algorithm names . . 

. . then . . we'd like . . this set . . of naming conventions . . to uh . . also . . encompass . . the . . ability . . to . . uh . . sign in . . uh . . to an account . . or something like that . . 






calculate // get or initialize or start or create

process // get or initialize or start or create

order // get or initialize or start or create

sort // get or initialize or start or create

. . .

getList.bySortedOperation
getListedItem.bySortedOperationIndex

getList.bySortedOrder(sortedOrderCallbackFunction)

getList.byOrderedIndexList()
getList.byOrderedAlphabeticalPropertyNameList(propertyNameLocationIdList)


getList.byOrderedAlphabeticalPropertyNameList(['planet', 'earth', 'california-united-states', 'housingAddress', 'streetName'])


. . . 

initialize item

. . . 

calculate-pi-to-N-decimal-places . . . 

can become . . . 

initialize-pi-to-N-decimal-places . . . 

or . . 

get-pi-to-N-decimal-places . . . 

. . . 

initialize . . . doesn't necessarily return the value of pi . . or . . the function with "initialize" . . doesn't necessarily . . suggest to the reader . . that . . the . . function . . or algorithm . . will return anything . . uh . . well . . that is only maybe the case for some readers . . and I'm not sure . . it is only an idea . . 

get . . . suggests . . that something is being returned . . 

get-pi-to-N-decimal-places . . suggests that the value of . . pi . . will be returned . . uh . . well . . this is also . . an idea . . and maybe isn't the case for all readers . . for example . . readers of . . uh . . non-english . . background uh . . or non-english . . language . . familiarity . . or something like that . . 


. . . 

start-server
initialize-server
start-application
initialize-application
initialize-parameter-list
initialize-permission-rule-set
initialize-permission-rule-list


initialize-network-protocol-connection
initialize-network-protocol-connection-list

initialize-account-get-permission-rule
install-account-product-listing-id
install-backup-camera-to-the-garage
install-list-of-uekele-events-at-the-local-pickup-truck-event



. . . 

install . . start . . start . . installation . . 

start-installation
stop-installation

start-download
stop-download

start-synchronization-protocol
stop-synchronization-protocol

. . . . 

. . . . 


. . . . 

initialize account get permission rule
uninitialize account get permission rule

. . . 

. . . 

a type of account . . a signed in account . . 

account . . service account . . 

service account . . 

get service account . . 

get service account . . 

initialize service account . . // sign in . . 

uninitialize service account . . // sign out . . 


[Written @ 22:01]

get item
create item
update item
delete item
does exist item

. . . 

get item list
create item list
update item list
delete item list
does exist item list

. . . 

get item store
create item store
update item store
delete item store
doex exist item store

. . . 

get item store list
create item store list
update item store list
delete item store list
does exist item store list

. . . 

/get-item
  - /-by-item-id
  - /-by-item-name
  - /-by-text-search
  - /-by-item-list
    - /-for-firebase
    - /-for-firebase-admin
      - /-for-firebase-admin
        - index.ts
      - /-by-certain-property-1
      - /-for-certain-property-A
      - /-for-certain-property-B
      - /-based-on-suggestion-by-A
        - /-for-the-purpose-of-doing-thing-1
        - /-for-the-purpose-of-doing-thing-2
          - index.ts


function call:

getItem.byItemList.forFirebaseAdmin()


As specified in the directory tree:
`/get-item/-by-item-list/-for-firebase-admin/-for-firebase-admin/index.ts`

. . . 

getItem.byItemList.forFirebaseAdmin.basedOnSuggestionByA.forThePurposeOfDoingThing2()


As specified in the directory tree:
`/get-item/-by-item-list/-for-firebase-admin/-based-on-suggestion-by-A/-for-the-purpose-of-doing-thing-2/index.ts`

. . . 




[Written @ 20:27]

validate-item

validateItem (item)

get create item permission rule // validate-item . . . 
get update item permission rule
get delete item permission rule


// validate item . .

getCreateItemPermissionRule (item)

. . . 

getCreateTransactionPermissionRule (transaction)

. . . 

getCreateTransactionPermissionRule (permissionRuleId)

. . . 

. . . 

/get-transaction-permission-rule
  - /-for-create-transaction
    - index.ts
  - /-for-get-transaction
  - /-for-delete-transaction
  - /-for-update-transaction


const createTransactionPermissionRule = getTransactionPermissionRule.forCreateTransaction(permissionRuleId, permissionRuleType)

createTransactionPermissionRule.permissionRuleCallbackFunction({
  isSenderAdministratorAccount: true,
  senderAccount,
  recipientAccount,
  digitalCurrencyAmount
})



let digitalCurrencyTransactionPermissionRuleVariableTree: {
  [permissionRuleId: PERMISSION_RULE_TYPE_CREATE_TRANSACTION]: PermissionRule < Item >,
  // [permissionRuleId: PERMISSION_RULE_TYPE_GET_TRANSACTION]: PermissionRule <>,
  // [permissionRuleId: PERMISSION_RULE_TYPE_UPDATE_TRANSACTION]: PermissionRule <>,
  // [permissionRuleId: PERMISSION_RULE_TYPE_DELETE_TRANSACTION]: PermissionRule <>,
} = {}

let digitalCurrencyTransactionPermissionRuleTypeToIdMap: {
  [permissionRuleType: string]: { [permissionRuleId: string]: boolean }
} = {}



[Written @ 20:03]


getItem (itemLocationId: string, itemIdList: string): Item
getItem (itemLocationIdList: string): Item


getItem (itemLocationIdList: string): ItemResponse


createItem (itemLocationIdList: string): ItemResponse





getItem (itemLocationIdList: string, levelsOfDepth?: number): ItemResponse


operationDiagnosticIndicatorOptions = {
  millisecondNetworkLatencyPreferenceMetric: number = 100
  operationDiagonalMatrixMultiplier: string = ''

}


class ItemResponse {
  item: Item
  additionalInformation: { [additionalInformationId: string]: any } = {}
}



[Written @ 19:46]

/get-account
  - /-by-account-id
    - /-based-on-firebase
    - /-based-on-firebase-admin
    - /-based-on-oicp
  - /-by-account-username
    - /-based-on-firebase
    - /-based-on-firebase-admin
    - /-based-on-oicp
  - /-by-email-address
    - /-based-on-oicp
  - /-by-text-search
    - /-based-on-firebase


getAccount.byTextSearch.basedOnFirebase



[Written @ 19:33]



/topic-site-entry-name
  - /-based-on-firebase
    - index.ts
  - /-based-on-firebase-admin
    - index.ts
  - topic-site-entry-name


for example

/create-account
  - /-based-on-firebase
    - index.ts
  - /-based-on-firebase-admin
    - index.ts
  - create-account

. . . 

/create-account
  - /algorithms
    - /create-create-acocunt-algorithm
      - index.ts
    - /get-create-account-algorithm
      - index.ts
  - /variables
    - /create-account-algorithm
      - index.ts
  - /data-structures
    - /create-account-algorithm
      - index.ts

// in the create-account/algorithms/create-create-account-algorithm/index.ts file . . 
  - function signature: createCreateAccountAlgorithm (basedOnDatabaseProvider, inputParameterObjectDefinition: ): CreateAccountAlgorithm


// in the create-account/data-structures/create-account/index.ts

class CreateAccountAlgorithm {
  sourceCode: string = ``
}

class InputParameterObjectDefinition {

}


// in the create-account-algorithm/variables/create-account/index.ts

let createAccountAlgorithmVariableTree: {
  [createAccountAlgorithmId: string]: CreateAccountAlgorithm
} = {}

. . . 


. . . 


# Database Algorithm Names:

## Database Service

- start service // or "initialize service"
- stop service // or "uninitialize service"
- get service
- update service

## Database Service Account

- initialize service account // or "start service account"
- uninitialize service account // or "stop service account"

- get service account
- create service account
- update service account
- delete service account

## Item Store

- initialize item store // or "start item store"
- uninitialize item store // or "stop item store"

- get item store
- create item store
- update item store
- delete item store
- does exist item store
- create item store id

- get item store list
- create item store list
- update item store list
- delete item store list
- does exist item store list
- create item store id list

## Item

- initialize item // or "start item"
- uninitialize item // or "stop item"

- get item
- create item
- update item
- delete item
- does exist item
- create item id

- add item event listener
- remove item event listener

- get item permission rule
- add item permission rule
- update item permission rule
- remove item permission rule

## Item List

- initialize item list // or "start item list"
- uninitialize item list // or "stop item list"

- get item list
- create item list
- update item list
- delete item list
- does exist item list
- create item id list

## Database Network Protocol Member

- initialize network protocol member // or "start network protocol member"
- uninitialize network member // or "stop network protocol member"

- get network protocol member
- add network protocol member
- update network protocol member
- remove network protocol member

## Database Network Protocol Message

- start network protocol message auto sync
- stop network protocol message auto sync
- update millisecond delay between network protocol message


# Network Protocol Algorithm Names

## Network Protocol Service

- start service // or "initialize service"
- stop service // or "uninitialize service"
- get service
- update service

## Network Protocol Service Account

- initialize service account // or "start service account"
- uninitialize service account // or "stop service account"

- get service account
- create service account
- update service account
- delete service account

## Network Protocol Connection

- initialize network protocol connection // or "start network protocol"
- uninitialize network protocol connection // or "stop network protocol"

- get network protocol connection
- create network protocol connection
- update network protocol connection
- delete network protocol connection

- add network protocol connection event listener
- remove network protocol connection event listener

- get network protocol connection permission rule
- add network protocol connection permission rule
- update network protocol connection permission rule
- remove network protocol connection permission rule

## Network Protocol Connection List

- initialize network protocol connection list // or "start network protocol connection list"
- uninitialize network protocol connection list // or "stop network protocol connection list"

- get network protocol connection list
- create network protocol connection list
- update network protocol connection list
- delete network protocol connection list

## Network Message

- send message

- add message event listener
- remove message event listener






# Algorithm Modifiers:
- by-input-parameter-id
- by-input-parameter-constant-tree
- based-on-provider-service
- on-event
- for-item-purpose


- by-< wild-card-specifier-type >
- by-< name of property or item of interet >
- based-on-< name of property or item of interest >
- on-event-< name of property or item of interest >
- for-< item purpose name >

. . . 

// more modifiers . . 
- by-output-type
- by-function-signature-type
- by-listed-group

. . . 

for example . . 

/get-item-permission-rule
  - /-for-create-item-algorithm
    - index.ts
  - /-for-get-item-algorithm
    - index.ts
  - /-for-add-item-event-listener-algorithm
    - index.ts
  - /-for-remove-item-event-listener-algorithm
    - index.ts


. . . 

event modiefies

event modifiers

. . . 

examples:

function names: 

get-item-on-event
create-item-on-event
add-item-event-listener-on-event

create-item-based-on-firebase
update-item-based-on-firebase
add-item-event-listener-based-on-firebase


directory tree:


/get-item
  - /-on-event

/create-item
  - /-on-event

/add-item-event-listener
  - /-on-event



/create-item
  - /-based-on-firebase

/update-item
  - /-based-on-firebase

/add-item-event-listener
  - /-based-on-firebase



. . . 

examples of function calls . . 


getItemPermissionRule.forCreateItemAlgorithm(): PermissionRule < Item >
getItemPermissionRule.forGetItemAlgorithm(): PermissionRule < Item >
getItemPermissionRule.forAddItemEventListenerAlgorithm(): PermissionRule < Item >
getItemPermissionRule.forRemoveItemEventListener(): PermissionRule < Item >

getItem.onEvent(eventId, eventType, algorithmInputParameterObject): OnEventResponse< Item >

createItem.onEvent(eventId, eventType, algorithmInputParameterObject): OnEventResponse< Item >


createItem.basedOnFirebase(algorithmInputParameterObject): Item
updateItem.basedOnFirebase(algorithmInputParameterObject): Item
addItemEventListener.basedOnFirebase(algorithmInputParameterObject): Item




class OnEventResponse < Item > {
  
  eventId: string = ''
  eventType: string = '' // create, get, update, delete, etc.

  onEventResponseResolvedValue?: Item

  dateEventResponseWasRequested?: Date
  dateEventResponseResolved?: Date
}

class PermissionRule < Item > {

  permissionRuleId: string = ''
  permissionRuleType: string = '' // create, get, update, delete, etc.

  permissionRuleCallbackFunction: (inputParameter: Item) => Promise< boolean > = () => { return true }
}


. . . 

you can have more than . . one permission rule . . for example . . 



/get-transaction-permission-rule
  - /-for-create-transaction-algorithm
    - /-for-when-the-customer-asks-for-transaction-type-1
    - /-for-when-the-customer-asks-for-transaction-type-2
    - /-for-when-the-customer-asks-for-transaction-type-3

/get-transaction-permission-rule
  - /-for-create-transaction-algorithm
    - /transaction-type-1
      - /-for-when-the-customer-asks-for-a-certain-item
      - /-for-when-the-customer-asks-for-a-certain-restraint
      - /-for-when-the-customer-takes-too-long-to-respond
      - /
    - /transaction-type-2
    - /transaction-type-3


/get-transaction-permission-rule
  - /-for-create-transaction-algorithm
    - /-for-when-the-customer-asks-for-transaction-type-1
      - /-for-when-the-customer-asks-for-a-certain-item
      - /-for-when-the-customer-asks-for-a-certain-restraint
      - /-for-when-the-customer-takes-too-long-to-respond
      - /
    - /-for-when-the-customer-asks-for-transaction-type-2
    - /-for-when-the-customer-asks-for-transaction-type-3





