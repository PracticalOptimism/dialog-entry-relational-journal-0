
# Troy Architecture 

Thoughts and Notes on the Troy Architecture so far . . 


This is an experimental architecture for structuring . . or developing . . or designing . . or compositing . . or segmenting . . or . . organizing . . a . .  codebase . . 

The idea is to have a format for getting, creating, updating and deleting files in a file directory hierarchy as is organized . . 

Organizing the files of a program . . could have standards . . or . . uh . . a standard document format . . for the documentation of the entire codebase . . uh . . that is agnostic to uh . . maybe the types of files that are created . . the file extensions could uh . . be . . uh . . alright . . or important . . only for the specific details of the program . . and yet the architecture could uh . . be easy for people to learn . . and so . . people working on different projects could come to a new project . . and be familiar with how it's organized . . uh . . since the directory structure is familiar . . uh . . This architecture is experimental . . or uh . . not really uh . . complete . . it seems like a work-in-progress . . uh . . there are some conveniences for using this data structure . . uh . . and these conventions . . for naming files . . and naming folders . . and structuring the organization of the files and folders . . and yet . . there could also be inconveniences . . for example . . uh . . using the architecture so far . . seems to . . uh . . well . . in a certain sense . . for example . . if you export the . . javascript library . . to be . . a reflection of the codebase . . the javascript library . . could have really long names for all the functions . . and that could be . . uh . . not very user friendly for a developer who wants to use . . 1 function . . and . . referencing . . all the . . uh . . directory entries . . to access that single function could be an inconvenience . . uh . . on the other hand . . it's . . a really powerful tool for example . . if you want to have computer programs . . that . . also . . program alongside . . people programmers . . computer programs . . that update the source code . . alongside . . the work of the people programmers . . could know the architecture format . . uh . . in terms of where things are expected to be . . placed . . and so . . the program . . knows which directories to place a file . . and which file names to provide . . according to the standard . . that's defined . . or suggested . . by . . an architecture . . like the troy architecture . . uh . . well 


. . . 

Example of Inconvenience:

// To call an algorithm in the ecoin library . . so far . . requires something like this . . 

// directory structure
/usecase
  - /digital-currency
    - /algorithms
      - /get-account
        - index.ts

// library function call structure

libraryName.usecase.digitalCurrency.algorithms.getAccountAlgorithm.algorithm.function()

// if the directory tree goes . . further . . the . . access to the function could be further extended . . 


// directory structure
/usecase
  - /digital-currency
    - /algorithms
      - /get-account
        - /algorithms
          - /get-get-account-algorithm
          - /create-get-account-algorithm
          - /update-get-account-algorithm
          - /delete-get-account-algorithm
        - /data-structures
          - /get-account-algorithm
            - /index.ts
        - /variables
          - /get-account-algorithm
            - /index.ts


// library function call structure

libraryName.usecase.digitalCurrency.algorithms.getAccountAlgorithm.algorithms.getGetAccountAlgorithm.algorithm.function 


// from what I've explored so far . . having an algorithm . . be a topic . . so for example . . "get-account" . . could be a topic . . with . . "algorithms" . . "constants" . . "data-structures" . . and other topic sites . . specified . . and . . well . . uh . . this allows for the possibility of . . having . . a way . . for example . . to . . have your algorithms . . be created at runtime . . and you can retrieve . . different versions of your . . "get " . . algorithm . . uh . . that could look something like this . . 

// in data-strctures/get-account/index.ts


class GetAccountAlgorithm {
  algorithmPart1: string = `some code here`
  algorithmPart2: string = `some code here`
  algorithmPart3: string = `some code here`
}


// in variables/get-account/index.ts

let getAccountAlgorithmVariableTree: { [getAccountAlgorithmId: string]: GetAccountAlgorithm } = {}



Well . . you can create different variations . . for the . . "GetAccounAlgorithm" . . uh . . a topic . . really seems to revolve around the data structure that is being discussed . . and so . . whereas . . uh . . I had originally . . started thinking . . about . . for example . . "digital-currency" . . usecase . . as a topic . . uh . . well . . the digital-currency . . data structure . . uh . . is sort of the implicit . . structure that the topic . . is about . . uh . . so . . well . . uh . . I suppose . . uh . . in extending this idea . . to other . . directories . . uh . . uh . . for example . . if we wanted to extend the . . topic . . and topic site architecture . . to further layers . . of the . . architecture . . of the codebase . . uh . . well . . uh . . it seems like the . . uh . . ideal thing to do . . to allow . . more . . breathing in terms of . . allowing the developer to add further detail or clarification . . on the . . uh . . directory of concern . . uh . . so for example . . if the algorithm . . that's being developed . . uh . . requires more than . . a single file . . to write . . uh . . in a convenient way for example . . and . . uh . . that algorithm . . really . . only relates to the . . topic being discussed . . or . . uh . . hmm . . well . . uh . . so if the algorithm has several parts . . uh . . those parts could be . . uh . . well . . if there are enough parts of the algorithm . . maybe let's say . . 10 or 20 . . uh . . other . . maybe . . 1000 . . lines of code length functions or something . . uh . . maybe . . 10,000 lines of code . . I'm not sure . . uh . . I haven't really thought of that usecase . . but . . uh . . it seems . . theoretically possible to have . . uh . . a topic . . for that single algorithm . . uh . . and so . . for example . . the constants . . variables and data structures and infinite loops and so on . . for that single algorithm . . can be . . partitioned . . or separated . . into . . uh . . the . . uh . . topic . . uh . . of that algorithm . . uh . . uh . . and uh . . that gives you . . the further convenience to be able to uh . . . uh . . for example . . provide . . 

- documentation
- access-control
- validator
- expectation-definitions
- or even more topics ? ? . . 

and uh . . well . . for example . . what if you wanted to make an entire video game . . to document . . the use for a function ? . . or maybe a constant ? . . well . . maybe . . uh . . a whole . . uh . . simulation . . specific for that function needs to be . . organized in a topic . . uh . . directory . . . 

/topic
- /constants
  - /constant-group-1
    - /algorithms
      - /
    - /constants
    - /data-structures
      - /website
        - /precompiled
          - /usecase
            - /
    - /infinite-loops
    - /moment-point-algorithms
    - /variables
  - /constant-group-2



Example of a constant that could need uh . . or use . . further . . discussion . . uh . . and so . . further presentations are defined . . in those respective directories . . for example . . a web browser website . . [uh . . but really you could uh have other presentation styles as well . . uh . . ]



/mathematics
- /constants
  - /transcendental-numbers
    - /algorithms
      - /add-numbers
      - /subtract-numbers
      - /
    - /constants
    - /data-structures
      - /transcendental-numbers
        - /eulers-number
        - /tao-number
      - /website
        - /precompiled
          - /consumer
            - /web-browser
          - /usecase
            - /history-of-numbers
            - /relationship-map-of-numbers
            - /web-browser-website
              - /pages
                - /home
                  - index.html
              - /components
                - /
    - /infinite-loops
      - /calculate-number
      - /
    - /moment-point-algorithms
    - /variables
  - /rational-numbers





Notes So Far 
["Notes Application" @ April 30, 2020]




@project
@documentation


/<topic>
	/<topic> - OR -
	/<site>
		/<topic> - OR -
		/<site-operation>
			index.filename
			index.documentation.filename
			index.expectation-definitions.filename



Example

/digital-currency
	/algorithms
		/create-account
			index.ts
			index.documentation.ts
			index.expectation-definitions.ts
			
	/data-structures
	/variables
	/constants
	/infinite-loops





[Sketch Application Notes]

The Troy Architecture
(also known as the C.P.U. Architecture or Consumer, Provider, Usecase Architecture)


Data Structures
- class file data structures
- image file data structures
- video file data structures
- static asset data structures


Topic Site
- algorithms
- data-structures
- variables
- constants
- infinite-loops



Site Modifiers
- documentation
- access-control
- validator
- expectation-definitions
- 

------


The Troy Architecture
(also known as the C.P.U. Architecture or Consumer, Provider, Usecase Architecture)



3 Main Folders within the /precomiled folder:

/consumer
	/<topic-or-folder-for-topics-1>
	/<topic-or-folder-for-topics-2>
	/ and more . . . 
/producer
	/<another-topic-or-folder-for-topics-1>
	/<another-topic-or-folder-for-topics-2>
	/ and even more . . .
/usecase
	/<more-topic-or-folder-for-topics-1>
	/<more-topic-or-folder-for-topics-2>
	/ and then some . . . 

--

/<project-folder>
	/precompiled
		/consumer
		/provider
		/usecase
	/compiled
	/expectation-definitions
		/algorithm-expectations
	/demos
	/documentation
	/


-- 

Spheres: A topic that is used by
other topics. The directory name
is prefixed with “@“ symbol to
help indicate the purpose of the
topic as not only self-referenced
but also meant to be referenced by
other topics. A common sphere is
the utility sphere as follows:

/@utility
	/<topic-or-foloder-for-topics>
	/ and so on . . .

-- 

Topics: A directory named after a specific interest or topic of concern

The Topic architecture is as follows:

/<topic-name>
	/algorithms
	/data-structures
	/constants
	/infinite-loops
	/variables


--

Every directory in the project under the root directory titled “precompiled”
is either a topic or a directory containing topics. Where your topic would
otherwise use a space character to separate the words, a hyphen is used instead

- use “-“ (pronounced hyphen) instead of “ “ (pronounced space or space character)
- use alphanumeric characters without punctuation such as , . ;
- use lowercase letters


For example, “I love butterflies and computers.” is replaced by
	“i-love-butterflies-and-computers”


-- 

Ideas on how . . files are imported from one file to another . . in these various topic sites . . [while trying to avoid circular imports]


- Algorithms are imported from algorithms

- Constants are imported from algorithms
- Constants are imported from constants
- Constants are imported from infinite loops
- Constants are imported from variables

- Data structures are imported from algorithms
- Data structures are imported from constants
- Data structures are imported from inifinite loops
- Data structures are imported from variables

- Variables are imported from algorithms
- Variables are imported from infinite loops
- Variables are imported from variables




