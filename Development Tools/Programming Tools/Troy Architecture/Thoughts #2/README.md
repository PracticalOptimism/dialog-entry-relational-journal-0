

It would be cool to see . . structures like . . cities . . and buses . . or houses . . or . . architecture . . for buildings . . or . . blueprints . . for . . inventions . . or . . structures . . for organizing information . . uh . . relating to . . uh . . society uh . . or . . uh . . I'm not sure . . 

Well . . one thing that uh . . is really cool . . or . . that I think would be . . or could be practical . . for an architecture . . uh . . like the . . troy architecture . . uh . . for file-hierarchy organization . . formats . . is the usecase for . . organizing . . blueprints . . for how . . a building works . . and so for example . . the various aspects of the . . development . . of a building . . can be . . organized . . in a codebase . . uh . . that . . can . . support . . all . . sorts . . of file types . . uh . . and . . uh . . various teams . . could work . . using . . those various file types . . such as . . CAD . . or . . Computer-Aided Design model file types . . if that's a form of information that is being used in the project . . 

uh . . I especially like the idea of having infinite loop file types . . or infinite loop programs that can be a part of the design or development . . of a city . . or a town hall building . . or a school . . uh . . so it's not only . . uh . . maybe . . seen as data structures with . . properties . . uh . . like . . a javascript class . . or . . uh . . typescript class object . . -_- . . uh . . well . . uh . . that's maybe perhaps one useful way to view things . . but having an architecture that let's you . . switch between various . . uh . . interpretations . . of the . . uh . . structure . . of interest . . and so . . for example . . uh . . constant conditions . . like . . the . . uh . . amount of . . sun . . or sun exposure that an area gets uh . . could be . . considered a constant value . . if the measurement is taken once . . uh . . well . . then . . uh . . the codebase . . could support . . or make it easy to conceive to change the constant to . . an infinite loop where you're constantly taking measurements of how much sun is available in a plot of land . . for example . . which might be or could be useful for . . things like determining the solar panel units to apply in that area of interest . . or something like that . . 


uh . . a common codebase . . uh . . project . . structure . . for . . not only . . uh . . websites . . or . . uh . . javascript library . . . . uh . . code bases . . uh . . but maybe also . . there could be other consumers . . for the project . . uh . . maybe uh . . those other consumers of the codebase could be defined uh . . or uh . . discussed or . . considered . . or something like that . . 

for organizing plans for biological instruments . . for organizing the experiment . . in a chemistry lab . . for organizing the architecture of a city . . or a small town . . or a bus or vehicle or car or plane or truck . . those could be interesting . . uh . . keeping the blueprint format . . in a way . . common for various types of projects . . uh . . and so . . maybe people from one industry . . can come to another industry and be familiar . . with how information is organized . . or something like that . . 





