
Command Line Interface


# list currently active processes relating to node
ps aux | grep node   [1]


# stop node process from running
kill -9 $(ps aux | grep '\snode\s' | awk '{print $2}')


# stop command running from port address
kill $(lsof -t -i :9000)


[1]
How to Kill a Process from the Command Line
By Linux.com Editorial Staff
https://www.linux.com/training-tutorials/how-kill-process-command-line/

[2]
How do I kill the process currently using a port on localhost in Windows?
Asked By KavinduWije
Answered By Fellow Stranger
https://stackoverflow.com/questions/39632667/how-do-i-kill-the-process-currently-using-a-port-on-localhost-in-windows

