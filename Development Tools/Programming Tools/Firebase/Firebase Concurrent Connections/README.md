

## Firebase Concurrent Connections

Clarify the Firebase connections
https://stackoverflow.com/questions/21070095/clarify-the-firebase-connections


##### Kato's Answer

In addition to Mike P's excellent answer, here are a few other discussions on the same topic which may prove insightful.

From the Firebase pricing page:

What is a Connection?

A connection is an open network connection to our servers. It's a measure of the number of users that are using your app or site simultaneously. This isn't the same as (and is usually a lot lower than) the total number of visitors to your site or the total number of users of your app. In our experience, 1 concurrent corresponds to roughly 1,400 monthly visits.

Our Development Firebase has a hard limit on the number of connections allowed. All of the paid Firebases, however, are “burstable”, which means usage is not capped and instead you are billed for any overages. We measure connections for paid plans based on the 95th percentile of usage during the month.

From this mailing list discussion, by Andrew Lee (Firebase founder):

I strongly recommend you not worry about it unless you're actually bumping up against our limits...most developers vastly overestimate the number of concurrent users they will have. A good rule of thumb is 1 concurrent = 1000 monthly visits for the typical website. For mobile, the ratio between installs and concurrents is sometimes even higher (though it varies considerably depending on your use case). Our plans are quite generous when it comes to concurrent users. As a data point -- our own website could operate comfortably on the "free" Firebase plan most days. In fact, more than 99.5% of all of Firebases never hit the 50 concurrent limit.

So, long story short, if you're working on a hobby project, you will almost certainly not hit our free tier 50-concurrent limit. If you're a business or a larger app, I hope you will find our $49 / month plan more cost-effective than spending engineering time to figure out when to goOnline / goOffline to minimize that number.

At the very high end (huge Enterprise apps with 10k+ concurrents) we do offer custom pricing that has a lower per-concurrent rate.

A user benchmarking and testing of connections here on SO: How the Connection is calculated in Firebase

Another similar question here on SO: How are concurrent connections calculated





