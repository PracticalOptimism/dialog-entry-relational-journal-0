





--


## Basic Security Rules

https://firebase.google.com/docs/rules/basics


## Insecure Rules

https://firebase.google.com/docs/rules/insecure-rules


## Data Validation

https://firebase.google.com/docs/rules/data-validation





--


## Firebase Security Rules Conditions
https://firebase.google.com/docs/database/security/rules-conditions


## Using $ Variables to Capture Path Segments

".write": "$room_id.contains('public')"

## Authentication

".write": "$user_id === auth.uid"


".read": "auth != null && auth.uid == $uid"


## Working with Authentication Custom Claims

{
  "rules": {
    "frood": {
      // A towel is about the most massively useful thing an interstellar
      // hitchhiker can have
      ".read": "auth.token.hasEmergencyTowel === true"
    }
  }
}


## Existing Data vs. New Data

".write": "!data.exists() || !newData.exists()"


## Referencing Data in other Paths

".write": "root.child('allow_writes').val() === true &&
          !data.parent().child('readOnly').exists() &&
          newData.child('foo').exists()"

## Validating Data

".validate": "newData.isString() &&
              newData.val().matches(/^(19|20)[0-9][0-9][-\\/. ](0[1-9]|1[012])[-\\/. ](0[1-9]|[12][0-9]|3[01])$/)"


".write": "newData.hasChildren(['color', 'size'])",


".write": "newData.isNumber() && newData.val() >= 0 && newData.val() <= 99"

".read": "auth.uid != null &&
          query.orderByChild == 'owner' &&
          query.equalTo == auth.uid" // restrict basket access to owner of basket


".read": "query.orderByKey &&
          query.limitToFirst <= 1000"
