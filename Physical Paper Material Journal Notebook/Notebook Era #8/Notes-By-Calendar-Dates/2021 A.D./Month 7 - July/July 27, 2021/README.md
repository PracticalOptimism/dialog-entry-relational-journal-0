

# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 27 July, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 27 July, 2021
- Time Page Transcription Started: 16:09
- Time Page Transcription Stopped: xx:xx

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- The daughter said so many sweet things about "Mr. Sweet Nipple"


## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

## Part II - Page #1

A young daughter visits her parents' bedrooms late at night because she is thirsty for milk.

She loves the convenience of her mother's breast laying at the side of the bed with one large tremendous bulge that lays out from under the bed covers.

The bulge is a large long nipple

The nipple is normally soft but gets hard when the daughter sucks on it for milk.

The long soft nipple for milk is free.

The daughter feels loved

The daughter feels precious

Her large breasted gigantomastia mother with long nipples is precious

The daughter is short in height

She stands up tall and stands next to the bed

She grabs the nipple and sucks on it for milk

One day

On a secret day that was not known to the daughter

The daughter grabs the penis

## Part II - Page #2

of the husband

The husband has a penis that is confused for a large nipple

The nipple

The penis

These aren't so different

They look so similar

Especially a large long nipple

Especially a large long nipple from mother

Mother is like grandmother, this story has so many other subcategories of ideas.

The free milk from mother is great.

The wife pretends to suck on the babies penis [kinky softcore words]
[The daughter sucks on the husband's penis][kinky hardcore words]
[The husband is a baby ;) winky face][kinky explanation][1]
[pretend is word since actually it was the daughter][kinky explanation][2]
[pretend since now the daughter is the real wife][kinky explanation][3]
[the real wife is the one that does what the daughter is doing in the hardcore words sense][kinky explanation][4]
[the real wife is supposed to give a blowjob][kinky explanation][5]
[in a weird way, life is like a penis because of how hard and erect life feels. so then. if a baby has a penis. that means they have a life. a woman also in fact has a real penis called a clitoris right? and a man as a real penis called a penis. well. so then. for a mother to suck on the penis of her own children suggests a sort of poetic idea like "I'm going to make your life" or "I'll give you head" or "I'll make you feel good" "don't be alive in that one world over there which is theoretical and unknown to man kind" "come over here" "Let me give you a real blowjob" "give you a hard fact reality to think about" "hard facts" so then with those theories in mind. A wife. well. A mother isn't necessarily a wife. A mother has children. But a wife is married. And so . If the mother is also a wife. well. Wifes are uh. Then thought to be . "married" to their children right? . . So then it's like . . "the wife pretends to suck on the babies penis" is like -_- wow. so many weird twists and turns you can make from this one hallucination -_- . . -_- it's so weird. you're my wife? -_- but you're pretending? -_- L.O.L. -_- so how far does the pretend go? Do you -_- pretend that you're pretending that you're pretending? Do you pretend that you pretend that you pretend? Are you pretending that you're really pretending? -_- how many fake juke blowjobs are you jukin' me out on right now . . jukes are like weird cum drop sounding emblems -_- drip drop drip drop . . juke . . juke . . juke . . so many cum drops . . so then I really did cum right? or did you -_- . . juke . . ][kinky explanation][6]
[how many kinky explanations do you need to make a "macromastia" comic book? Can you count to 1 million? That would be hilarious. I would love to read that book.][kinky results]
[this comic book series or illustrative idea description gives 6 kinky explanations for this one line][notes]

"wife. What are you doing. as the eyes are close." "here. Keep sucking"

"Here. Let me help you use this hole as well."

And the husband sleeps with the thin bodied daughter

What an accident

The husband was sleepy.

The creampie

The creampie

The creampie milk is being swallowed for free.

Same milk.

Different person.

Yummy.

I'm going back to sleep

Goodnight

Goodnight, kissy kissy to Mr. Sweet Nipple.

Mr. Sweet Nipple. So sweet.


## Part III - Transcript Left Vertical Area Margin Of The Page

Write Notes Here . . 

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



