
# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: [no date on page] [assumed to be February 1, 2021]
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 3 February, 2021
- Time Page Transcription Started: 05:25
- Time Page Transcription Stopped: xx:xx

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0] This chart is in a lot of notes that in my physical paper material notebook . .
  - An accent of this is to represent time sequence diagrams
  - An accent of this is to represent a pattern sequence language like trying to access questions from your memory structure but you're not allowed to use [choose, antonym] characters that are abled by your language like english and so for example the sequence looks like a sound wave that you have to memorize by your nasal hold your breath idea of what you're trying to say and hopefully you carry the energy capacity along with you in other energy conversation activities . . and so for example | | | | . . is "I like your work" . . you are accenting the idea of its meaning because you allow yourself to . . but basically it doesn't mean anything because you are not accepting it . . you have to ask a question if you read . . | |  | | . . which means something like . . "uhh . . what does this mean again?" . . okay but it doesn't mean anything . . you have to ask a question . . "does this mean . . this . . does this mean . . that . . " . . but that is an accent to the original question . . "does this mean 'I like your work'?" . . which is answered by myself but cannot be answered by you don't need that accent to be materialized . . you can ask a question about its nascency . . "what if it could mean this?" . . "what if it could mean that?" . . "should you describe more characters?" . . "are you sure this is a basic sublanguage of human kind?" . . Basically . . | | | | . . means . . "you work hard" . . and so now your anderson clock meter is like . . "woah woah . . did you change the meaning of this clock concept?" . . basically it is an anderson and so its not that it has any meaning but rather you have to ask a question . . | | | | . . okay now you are accepting me to say that this means . . "you like wearing blue shoes" . . basically now you are and . . and . . and . . and . . and . . and if you continue like this . . you can clock all the haracters of the meaning of this concept of . . | | | | . . anderson . . so you can characterize this to mean ask me a question . . which is what . . "hello how are you?" means in english . . 
  - An accent like this is useful for measuring time sequence thoughts very rapidly and quickly and so the order of the arrangements of the line sequences can be really quick and representative of the idea you want to correct or sketch but you don't need to spoil yourself with nascent details like typographical error correct sentences and so if you have a poem that is very long to write and you have to ask a lot of nascent ideas like "wow" . . "woah" . . "oh my god" . . "woah" . . "wow" . . "wow" . . "wow" . . and "superlative" . . antient
  - 
- [2.0] "random" is a qord that is collecting in my memory loop when I accidentally look at the word "anderson" at this time of . . February 3, 2021 @ 5:43 . . I'm not really sure . . I haven't had this happen a lot to me before . . in a highly noticeable way like now . . 
  - [Seth]: A hap coyne accents itself in hyperloops

## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

Aspects you want that Anderson are andersons [2.0] without your nascent adjacent memory Anderson

|   |   ||||||| |   |   | [1.0]

Andering needs 2 time sequences
the (1) is for tracking your nascency
by laking your beds
Andering needs samples

(1) Andering needs pierccings like memory
  pool lake beds       but those
  are anders that leak     not Andersons
      And so you have to collect the
  leakage with an anderson leak
  collector called a second trace

(2) Anderson number 2 is not nannn
its a data structure called a
hap coyne

## Part III - Transcript Left Vertical Area Margin Of The Page

Write Notes Here . . 

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



