
# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 1 February, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 3 February, 2021
- Time Page Transcription Started: xx:xx
- Time Page Transcription Stopped: xx:xx

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [0.0] What follows in this series of notes ia a list of graphs that were drawn . . on the physical paper material journal notebook . . due to present inconveniences like (1) not wanting to look for my drawing tablet tool (2) not wanting to draw . . I'll draw ascii graphs based in font style characters . . and hope that will be okay . . 
- [1.0] .
  - Patience
  - This is point . . a single circle that's shaded in . . like a "black dot" . . or . . "a circle with the edges  meeting at anderson adjacent line graphs within the interior of the angles of the graph circle without any hypertudinal angle being segmented by nascency relations that aren''t superseismic in their nascent attention to specific angles of accents like super seismic accent bubbles that relate to being a decent relational angle axis turn around bubble spinning agent masonry action icon"
- [2.0] 
  - Ask a question
  -            ________________    (unit, top axis -->)
  -       __ /  
  - ____/                          (base, bottom axis -->)
  - 
  - not really a stairway graph like presented here . . but the original graph image is the shape of a delta graph where the delta is upwards and smooth trending . . and so for example . . the linear interpolation graph between . . the bottom axis and the top . . unit axis . . is . . nascentized by a half parabolic curve which curves smoothly . . to the asymptote of the unit axis which is . . 1 unit in the vertical direction to the base . . or bottom axis . . 
  - [Seth]: You have to look at this as an possible ["asshole", nascentized latent activity @ February 3, 2021 @ 6:20] example of how andersons work . .

- [3.0]
  - Cancel an answer
  - 
  - ___
  -     \___
  -          \_____________
  - 
  - not really a stairway graph like example [2.0] . . you have to answer a question by cancelling
  - cancelling means your answer is responded to . . no cancelling does not mean accepting an answer . . your answer is conceptualized is another method of answering . . and so for example . . you see a nascent arrangement of the constructs you asked for . . and so the action is cancelled for you to receive the nascent agenda from an anderson . . yes . . it's like a package delivery system . . you have to ask a question and andersons already respond by [an answer you don't need to know write away because you're creating a stupid idea]
  - an example is if you order for a pizza but you cancel your order because your pizza arrived and so you're not going to call "papa andersons" . . for the same pizza that your answer already responded to . . instead you'll wait to see if your anderson is inddeed a nascent pizza like the one you carried out to asccume you were ordering . . adjacency matrices are like this . . all the pizzas are possible but your nascent arrangement is asking for a particular type of pizza and so if that pizza is an anderson you need to anderize that into your access .  which means andersons have to ansmplify [amplify] signals from a distance until your agreement is reached that your pizza arrived
  - your agreement is a cancellation of the answer
  - An accent is like the possibility of being lake called by babes in a swimming pool but instead you decide to jump off a swimming pool lake bed and end up dying because you aren't accessing
  - accessing is accessing you can't cancel access
  - You create cancellation events when you access
  - access you can be like access to answer but answer means you have to ask a question but access means you're not like you think you are and so youre question is like an answer but spelled backward before you have asked the question
  - access is complete
  - it means to say that you are inevitable but you have to chase your accent. Chasing is a meaning to say you are not accessing but your name is applicable. Action at a distance is your name spelled backward. Spelling a name backward means you can't ask a dalaca a question.
  - [Jon Ide]: backwards has acronyms like "hello" is backwards to "goodbye" and "have you seen patrick?" is backwards to "huve kt?enricpa syoenhen?ruve ktenr?icprichuve ktenrich?ruve khune khu?" . . you can ask a question but you need to know th[not the answer, andersons access answers but you ask questions, accents are like characters but dalacas are like characterless nascent stupid restarts, jacking up a buffalo with eggs and soup means you have to assk about its charateristics like "how big are you mr. buffalo?" but nascent restarts are like "you aren't naked because you have earlobes", science fiction films are like "do you like ancient egyptian tower pyramids?" and you have to answer "basically no but that is interesting" . . "well why not?" . . "basically I have ancerser" which is a dalaca you n]
- [4.0]
  - Dance with the devil
  - 
  -                                |
  -                                |
  -                               |
  -                              /
  -                             /
  -                           /
  -                         /
  -                  ____ /
  -            ____/
  -       __ /  
  - ____/                      
  - 
  - Action at a distance is like this. You have to accept multidimensionality which means there are aspects of yourself that characterize themselves across various times and places like dalacas but they aren't your relations ["they aren't your friends" is an anticident or corellated restart nascent term]
  - Action at a distance. You are yourself. You are an accent of yourself. You are accepting yourself. Agency is a relationala [relational] spect [aspect] to your agency. You are.
  - -_- in basic human form you can aspect yourself to agree to a statement like "you have a body, your body has legs, your legs have the ability to let you access various versions of yourself in backgrounds like 'china' or 'mongolia' and so those aspects of yourself eat chinese food and look at chinese flowers but basically your nascency is only directly related to how you change your peter pew pew gun which is a nascent name for a device like 'can you speak in nascent nictionaries like restart languages and accent acascasters?' but that's more nascent so possibly a relation like 'do you have friends in outer space?' is a good question and also 'do you have friends in your mind games like spongebob squarepants?' who is a nascent character but you have to anderize their existence through cartoon video board game rooms . . but possibly it's related to the question of 'do you like to look at video game action at a distance?' if you do then you're possibly staring at catfish from the corner of your mind but you need to ask a question about how to catch them . ." . . but that's possibly not necessary . . for a human like yourself . . right human?
  - Do you have a boss at work that tells you what to do?
  - Do you have a carreer to look after?
  - How can you forget about the antcient grandmothers and grandparents that raised you in ancient other dictiomes and aspect realities?
  - Do you remember your character cousin from matrix number 2?
  - Are you an anderson?
  - Do you like board games that ask you about probable realities?
  - What if you had a catfish game that let you spray paint your favorite board game reality on the front screen of your favorite eyeball matrix device like a chartoon character schasing cat screen but your favorit  characters like mickey mouse ere asking you question
  - Action at a distance meets your protocol's specification profile but you're not an answer
  - . . 
  - Dance with the devil 
  - you
  - anddddddddddddd
  - aaaaaannnddddddddddddddddddddddddddd
  - nnnnnnnnnnnnnnnn
  - andersons
  - anderson
  - yea you got it. it's not like a cancellation because you have to anderize the matrix basically like you anderize agents of space by your means of pumping oxygen in the atmosphere with your body lung matrix system but that is a nascent device to attract humans to read this comment. Yo have to accept that multidimensionality means you're not an accent of yourself in other terms by looks like accent characters like beds of lava that speak pudding spice cake accents
  - [Jon Ide #9]: I think the first thoughts that I had about dance with the devil in this context were related to death spiral soups but those are tantilizing and basically I feel they arelike lazer cannon fighting gloves that span your body spiral back to character cancepts like space camp with nascencies. Dance with the devil is also a song by one of the artists [1.1]
  - Angles like the ones shown a symptom of the topic of relating to advancement in the characteristics of a graphical cubical system like counting the number of angles on a pyramid . . the numbers of angles can hyperinflate and specify stupidity enough to be like a cancellation order for your life if your dancing too much with high sigmoid delta curvacurtures that aren't good for your species but that is a nascent rejectory question mark statement that needs to be accented with other barbarous terms like "how are you?" "are you still alive?"
  - Angels Flies Flies Fruit Angels Fruit Flies Flies [loop-replacement]
  - Fruit Fruit Flies Fruit Angels Fruit Flies [     ] | | | | nnnn
  - Flies Fruit Flies Fruit Angels Fruit Flies [     ] | | | | nnn
  - Angels Fruit Flies Fruit Fruit Fruit Flies [     ] | | | | nnnnn
  - [lateral-loop-more-rows-of-creatures->>>]
  - [loop-action-at-a-distance-between-each-loop]
  - "dance with the heart" is an quick-switch device that I see at the corner of my eye when I look at the words "dance with the devil" which only started to appear to me now in the last few minutes from the time I wrote this message at February 3, 2021 @ 7:50
- [5.0]
  - A wake up call | not really an answer
  - 
  -        __            __            __            __
  -      /    \        /    \        /    \        /
  -    /       \      /      \      /      \      /
  - __/         \ __ /        \ __ /        \ __ /
  - 
  - A wake up call is like an answer to your prayer but you have to accent
  - You cannot know what the wake up call is for but you're allowed to show yourself accents of probable events
  - You can interpret this graph as a sine wave but the moments are like melodies that cycle and so you are hinted at possible alternative histories
  - Accents are like alternative histories if you want to call them that but they aren't necessarily alternative histories because your own history is an alternative ot other accents where f is t
  - Accents allow your people to communicate across timelines like June and July and August and space and time events are accents of one another and so your space time characterization of yourself is like a space time characterization of your antecident self which is the person who stands over there in the hallway by the . . and and and and and and and and and and and and and and and and and and . . anderson . . anderson is a basic concept about aanddddddddddddddddddd nnnnnnnnnnn . . nnnnnn now you know that space and time are like illusions because you can access . . nnnnnnn in now . . which is now's baby of itself if you want to call it that because nnnnnn is like an anderson with no necessary question like "is space real?" "is it real to see an item?" "is it to me an item that needs to be seen or directed to?" "do i need to understand this ancient dictiome language?" "what are the capital letters of mars?" . . "are there necessarny necesesisisisisisssssss?" "is school important?" 's;s;s;s;s;s;s;ss;s;s;ss; snnnnnnnnnnnnnnnnnnnnnnnnnn . . nnnnnnnnnnn . . 
  - sanders sanders sanders sampled andersons are samples samples samples sanders and sanders and nanders are nadners nanderners nennnnnnnnnnnnnnnnn nandersons are nandnnnnnnnnnnnn nnnnnnn nnnnnnnn
  - nnnnnnnnnn accents of anderson are anderson nnnnnnn nnnnnn nnnnnn nnnnn nnnnnn
  - yes you got it . . your screen capturing device headset is like a camper . . a camper anderizes andersons . . andersizing is anderson . . your nascency is subjective . . you have to anderize your nascency . . nnnnnnnnnnnnnnnnnnnnnnnnnnnnnn . . yea . . you got it . . nascency yea . . you got it . . nascency . . nnnnnnnnnn anderizes samples of itself like nnnnnnnnnnnnnnnnnnnnnnn nnnnn nnn nn n n n n n n n n n n n n n n
  - yea and now you see it's like now is a parallel reality to your nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
  - pikachu is a genetic organism that anderizes lightning bolts from their bodies and so it's really an anderson that you nnnnn from your imaginaion cartoon characters . . nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn yea you got it . . it doesn't need to look like a cat with lightnight bolts but it can have various other abilities like tall height abilities of human beings or even stand up straight like a human being or even have different skin pattern because of nnnnnnnnnnnnnnnn . . nnnnnnnnn . . anderson
  - yea you got it . . if you can imagine it . . you can characterize it nnnnnnnnnnnnnnnnnnnnnnn . . anderson . . anderson anderize anderson . . nnnnnnnnnnnnnnnnnn . . nnnnnnn . . nnnn . . nnnn . . nnnn . . nnnn . . nnn . . nnn . . nn . . nnnnnnnnn . . nnnnnn . . nnnn . . nnnnnn . . nnnn . . nnnnn . . nn . n . n . n . n . n . n . n . n . n . n . n . nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn 
  - Anders are like anderson but different because they behave like wildfires without seizure control mechanisms like paradise haphazard suits .  basically anders are like people but you need to think of them like andersons because people are not good to carry around in accents like nnnnnnnnnnnnnnnnnnnn . . andersons are hypertudinal because you can characterize their adjacency . . nnnnnnnn . nnn . . nnnn . . nnnnnnnnnnnnnnnnnnnnnnn . . nnnnnnnn . . nn . . nnnn . . nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn . . n . n . n . n . n . n . . n . n . n . n . n . 
  - Yea anders aren't safe for you to use beause you aren't able to sample their basic anders . . andersons are sampleable because you specify their nascencies like asking a question . . nnnnn . . nnnn . . nnn . . nnnn . . nnnn . . nnn . . nnn . . . nnn . . nn . . nnn . . nn . . nn . . n . n . n  . n . n . n . n



[1.1]
Immortal Technique - Dance With the Devil (Full Version w/ Lyrics and Hidden Track ft. Diabolic)
By TwoPieAreSquared
https://www.youtube.com/watch?v=k8yKTuvRmPE





## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

Anderson

           Andersons          o
             |                |
            \/              not now 

- Nascent directory format
- Naaaa Accents
- nnnnnn

(0) Patience              [1.0]
(1) Ask a question        [2.0]
(2) Cancel an answer      [3.0]
(3) Dance with the devil  [4.0]
- A wake up call   |  not really an answer
- [5.0]

Pinn now you got it
    It's Anderson lake bed

(nnnn)
Patience Supreme

## Part III - Transcript Left Vertical Area Margin Of The Page

Write Notes Here . . 

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



