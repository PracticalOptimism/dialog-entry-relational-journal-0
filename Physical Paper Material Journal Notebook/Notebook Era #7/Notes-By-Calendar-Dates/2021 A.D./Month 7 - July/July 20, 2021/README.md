
# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 20 July, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 20 July, 2021
- Time Page Transcription Started: 10:08
- Time Page Transcription Stopped: 10:12

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- I was writing on a notebook page today in only a few minutes ago since 10:12 (on July 20, 2021) when these those are written.
- Those notes are now thinking to myself "it's incredible" that "lead" from the "mechanical pencil" that I use is like "metal" "ball" "bearings" and so those metal ball bearings are like "getting" "stuck" in "grease" which is the "greasy sort of chemical concoction that is "physical material paper".
- Even though physical material paper doesn't seem sticky or greasey like the beeswax of an African American Man. Or like the olive oil from a kitchen. Or like the types of grease used to make the angles of a nice bearing or joint system in cars or robotics or mechanics more easy to use, it is interesting.
- Grease can be in many types of shapes and forms and yet the idea is "sticky" "fluidity" "properties"
- "In theory. You could have a metal road"
- "The metal road"
- "could be lathered"
- "bathed"
- "With grease"
- "And no one would complain on how they get to and from work if they are all slipping and sliding around all over the place.
- "Slipping and sliding is an interesting idea"
- "Now that I think about it. Probably I can take some notes on that topic." [July 20, 2021 @ 10:16]
- I wrote notes about these ideas of "slipping" . . "here" [Notes are available here](https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0/-/tree/master/Day%20Journal/2021%20A.D./Month%207%20-%20July/July%2020%2C%202021) (Day Journal - July 20, 2021)
- If you have a metal road that is taped with grease on the very top then if that grease is slippery . . you can possibly have other people or creatures or boats or vehicles slide onto the surface of the grease and travel across large distances in that way
- Remember our analogy about strings?
- Greasey strings are really useful for travelling
- Greasey strings are like wires that you can hold on to to just travel up and down any creek you would like
- I mean think about it . . you don't even need the metal . . if you have hard-metal-like hands like Jon Ide right . . who is assuming you have hands to work with . . then you can also be able to slide by holding on to a greasey cotton fabric rope right?
- I am not sure.

. . 

- Think about how awesome that is for electrons to travel
- Copper metal is maybe not considered "grease" for electricians
- But then O _ O
- Is metal also like grease?
- O _ O
- Metal is Grease?
- O _ O

. . 

- Light is grease?
- O _ O

. . 

- Think about it then
- What type of things travel using light?
- If they had to hold on to light waves like using nice hands
- What are 
- Those things?
- New Theoretical Particles?
- Barbarians?
- Big Apes?
- Ideas?
- Invisible Ideas?

. . 

- Well all of this theoretical science is really interesting. It's fun to play around with ideas.

. . 

. . . . . . . 

- That's so weird to think that possibly more and more new inventions can possibly be "thanks" to "greasey" things.
- Nice grease. Even normal grease.


## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

So the notebook that I am writing on
now is even made of a
metal And Grease System ?

Isn't that weird that

"Cars" "Trucks" "Airplanes" and
a lot of other transportation
vehicles possibly even including
boats also use

Metal and Grease Bearings?

Man and Woman Could even
possibly be considered a 
metal and Grease System.

Penis shaft is soft pliant
flexible metal like a bendy
straw [or] a sheet of aluminum
foil that becomes hard and
stiff when the blood rushes
in like a hot ball of grease
to make the stiff boner.

A hot ball of grease is a weird
tactile analogy.

Grease as a vagina is interesting
to think about.

## Part III - Transcript Left Vertical Area Margin Of The Page

Write Notes Here . . 

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



