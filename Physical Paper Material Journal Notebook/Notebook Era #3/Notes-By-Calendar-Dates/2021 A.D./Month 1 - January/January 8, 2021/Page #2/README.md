

# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: Unknown Date, Hypothesized to be 8 January, 2021 due to memory closeness to related page, but a physical date isn't written on the page to suggest its date of origin creation
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 10 January, 2021
- Time Page Transcription Started: 19:00
- Time Page Transcription Stopped: 19:06

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0] There is a drawing of vertical lines in a grid of horizontal rows of vertical lines . . some of those vertical lines from various rows . . have the top of their line edge or the top line end point . . having a line drawn to the bottom end point of another line at another row of the horizontal line matrix . . The image reminds me of electrical poles with electrical wire lines connecting the poles in a neighborhood street map or something like that . . This image is drawn above the text that reads "Diagonals" . . I'm not really sure how to interpret the message at this time since it felt like an automatic drawing image that I drew in uh . . some sort of hand holding suggestive way where a ghost spirit was possibly writing or drawing for me . . or something like that . . 


## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

## (Part I) Codebase - Transcript Central Body Margin

Dedication
 |
\/
Dictiomes

Dictiomes allow you to travel
Back in time to collect New
Memories and Events

Directions allow you to see four steps forward
in time to see where you are going and ["and" is circled]
Explanation is open to
innenencation

[1.0]
Diagonals

Diagonals are need to know basis events that
are based on new meaning and interpretation

Based on this understanding

## (Part II) Codebase - Transcript Left Body Margin

Decision -> Dictiomes
Destinctions -> Directions

## (Part III) Codebase - Transcript Right Body Margin

[to the right of "and"]
we concept
is infinite

innenancation
doesn't have a
definition or
identity and is
an invisible word
meant to be ignored when
you see it

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels





