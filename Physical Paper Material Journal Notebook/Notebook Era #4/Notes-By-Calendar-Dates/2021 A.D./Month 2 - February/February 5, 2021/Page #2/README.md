
# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: [5 February, 2021][Hypothesized date since the notes page didn't show the date in pencil markings]
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 6 February, 2021
- Time Page Transcription Started: 04:54
- Time Page Transcription Stopped: xx:xx

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0]
  - (1) a image is drawn here . . that looks like
  - ![Dascency Matrix Chart](./Photographs/dascency-matrix-chart-2.png)
- [2.0]
  - (1) a smaller copy of [1.0] is drawn here . . without the text labels . . similar to . . but without the smallest of the circles being drawn or represented . . 
  - ![Dascency Matrix Chart](./Photographs/dascency-matrix-chart.png)
- [3.0]
  - (1) My thinking in drawing this image at . . [1.0] . . that . . inspired this comment at [3.0] . . was to . . (1) look at how my image is portrayed in front of me . . like looking at my laptop and my hands typing on th elaptop . . (2) look at how the image fades from my vision when I draw my attention away . . like looking at another scene . . like if I close my eyes . . or look to a nearby . . uh . . table . . or something like that . . uh . . my table has food on it . . and a water bottle . . and other items of interest like notebooks . . uh . . (3) if you want to draw a map to correlate these images . . like you would co-relate images in a directory tree by sampling names like "vacation photographs" or "family photographs" . . you don't need to have uh . . I guess . . separate directory trees to represent the "family photographs" of one group of people or of one dinvidiaul or another . . you could have . . one uh . . directory tree . . which is uh . . like . . represented in a "flat map" type of way for example . . you see the adjacent events and scenes to the perspective that are being captured in this . . "star tree data structure" representation . . well . . I'm not really sure . . uh . . it seems like . . for example . . you could . . champion your perspective to be all sorts of these original perspectives . . across the field . . which for example could be a representation of your life . . and so your life is represented in this graphical way . . which for example is "star tree data structure" represented as this was uh . . possibly a related thought to what I was working on earlier . . which was the "star tree data structure" . . uh
  - (2) hyperbolic geometry is a topic of interest when it comes to visualizations like [1.0] which is like a tiling of space . . in a "non-euclidean" way . . or something like that . . Escher tiling . . and Escher's artwork in general is possibly a good resource to also consider . . in relating to this topic . . 
- [4.0]
  - This next set of notes is from the next page . . right-side adjacent to the initial page . . (it's the back side of the page in this case) . . The notes are fairly short and so possibly it's okay to place them on this page as well . . 
  - [4.1]
    - I drew an image of a tree-like data structure . . 
    - ![Dascency List of Accent Stars](./Photographs/dascency-list-of-accent-stars.png)
    - Dascency is a term meant to mean "latent data connection type" and so for example . . the connection is not necessarily established but is for example, shared in a listing setting where adjacency of the structure is assumed or something like that . . uhm . . adjacency I suppose is a theoretical topic relating to . . action at a distance . . and at a great enough adjacency distance you are asking about dascency topics . . or topics that don't have shape or form and are horizontally flat in their accessibiliity . . or something like that . . but access in hierarchies are possible approaches to accessing action at a distance as well . . and so for example . . using space ship data structures in the real world to grapple the grips of space time horizon turnways to travel to a planet like the Mars planet that is horizontally adjacent in other ways besides space time hierarchy travelling vectors . . and so for example you could possibly visit mars in its various life cycle turns through various eras through things like (1) your dreams [as I have done before I think] . . (2) portals in the water that allow you to teleport from one planet to another very quickly . . [as is what happened in a dream of mine once when I traveled to Mars] . . (3) Spiritual guide forces . . and possibly other paranormal activities . . but also you can have Mars simulations that help you ask questions about what Mars is really like because Mars itself could vary depending on the time period . . and the activities that are taking place . . and so you can peel back various versions of your own planet and study those environments to indirectly or directly study Mars related topics such as the furniture that you would be accustomed with there like "an earth like gravity" or "an earth like planetary atmosphere" or "an earth like solar rotation cycle" or "an earth like mobile transportation network" . . and so these things are directly hospitable even now on our sturdy surface planet
- [5.0]
  - This next set of notes is right-side adjacent to [4.0] and so . . this page is 1 page to the right of the initial page transcribed before [4.0] . . The notes are fairly short and so possibly it's okay to place those notes on this page . . instead of creating a new page . . 
  - [5.1]
    - I drew an image of a tree-like data structure . . similar to . . [4.1] . . the idea was to visualize what a . . "star tree data structure" could could look like . . and so for example . . 
    - I suppose . . a . . "dascency list" is the term that I've used to describe this . . data structure . . uh . . visualization pattern I guess . . I'm not sure . . dascency is a term that is relatively new to me . . I don't recall using this word uh . . so much . . but it does remind me of . . "nascency" . . or . . "nascent" . . a . . "nascent" data structure is supposed to relate to something observed but possibly latent . . and so for example . . a "toy in your imagination is an observed event but the toy itself may not be in your immediately observed environment through your eye lense vision or something like that . . " . . "the toy is part of the environment becuase you're observing it through your imagination environment . . and so it is nascent . . and yet . . ebcause it's not sitting in fornt of you with your eye vision . . you can call it 'latent' . . and not real or not visible or not existing . . uh . ." . . 
    - A dascency list . . lists are like data structures relating to . . neighborhoods . . but neighboors are kind of "invisible" . . you can say the neighborhood of . . "hello" exists the word "hi" . . and "hi there" . . but because you don't speak another language . . you're not able to identify the characters "hahahahaha" . . as a . . way to say . . "hello" . . 
    - I haven't really worked on this "star tree data structure" that much to know the full details at this time . . but it seems that . . uh . . "stars" could be . . uh . . those uh . . nodes of the triangle . . "data list" . . "network protocol connection item" . . "update map of area" . . uh . . each of those items . . are . . stars . . uh
    - One thing that is really cool about the "star tree data structure" is its ability to expand quite cool
    - cool means you can be quite cool
    - because I'm only on my second day now of knowing what this data structure is all about . . uh . . uh . . well . . uh . . I am stil experimenting and not really sure what else to say about it . . my impression so far is that you could possibly use it for graphing a great deal of things . . 

- [6.0]
  - (1) Time is visualized as moving from one circle to another on a dascency matrix chart . . and so for example . . you scroll up and down through various time horizonts of various perspectives of yourself . . 
  - (2) . . It's supposed to be like "a fish eye lense made of fish eye lenses" uh but possibly that's not the best expression since I just made that up right now . . uh . . let me think of another analogy . . well . . my original thinking in drawing the dascency matrix chart was something along the lines of . . "your peripheral vision seems kind of blury" . . but . . "your peripheral vision with respect to what?" . . "you can look around the corner of the room by traveling across the floor" . . "you can look around the corner of your neighbor''s thoughts by asking them questions" . . "you can look around the corner of a digital computer technology that allows people to see through one another's eyes" . . "you can see through the corner of timed historical events" . . well . . these are so many perspectives to consider . . they all sit at a peripheral vision . . your peripheral vision could also be considered the current life you're living which is like a corner of the mind's eye . . but it looks so much in the focus of the experience . . so it's taken for granted as the one true reality and one true perspective . . and yet it's already supposed that you can turn your perspective to consider other ideas . . and so possibly if you can map out those various perspectives . . one way to do that could possibly be . . a dascency matrix chart . . which places various peripheral perspectives of interest in the center of the graph . . those other perspectives of interest are placed in the peripheral edge of the graph . . so to speak . . and yet . . uh . . well . . in their own order of being . . they are like focused on uh . . like they are the center of focus or something like that . . 
  - (3) I've been reading about Seth's books a lot . . I'm not reminded of something they have talked about . . "I know my audience exists" . . "and I would now like for my audience to grant me the same privilege" . . which means to say something like . . "I am a ghost . . and not a lot of people necessarily believe in ghosts in your time . . and so please allow me to express that I am as real as you are . . although I don't have a physical body with which to write these words on my own" . . or something like that . . which is a message from . . "Seth Speaks" . . by Jane Roberts and Robert F. Butts . . uh . . well . . uh . . the thought that I have now been recalling is the perspective of . . social creatures that live under the sea . . like . . spongebob square pants and patrick star . . which are possibly in the peripheral vision of members . . of our species in our time . . and yet . . if . . Stephen Hillenburg hadn't have developed the animated story telling show "Spongebob Squarepants" . . possibly our peripheral vision would not be as expanded in this area . . as other krabby patty lovers . . in this universe . . 
  - (4)


## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

[1.0]

Dascency Matrix Chart
- A Dascency Matrix chart Asks
  About the plans of an individual
    like "How are you today?" and
      those responses are charted on their
        dascency matrix graph which shows
          only the peripheral interfrace of
            what is "immediately" available
              today

                "Your peripherals have
                  peripherals   and that
 [2.0]              is what you call time
                      events and feeling
                        events and agency
                          events and
                            alien events  "


Aliens are in a
  direct relation
if your map looks flat like this one [3.0]

[4.0]

Address
Reverse Address
- List all other Address Mains with this Address

[4.1]

Music is anderson

[5.0]

A dascency list of
accent stars
[5.1]

- A neighbor visiting dascency star
  allows access to the perceived
    dascency lists that were ealier
      considered

## Part III - Transcript Left Vertical Area Margin Of The Page

hypergeometric
grid to showcase
peripheral
significance
relations

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



