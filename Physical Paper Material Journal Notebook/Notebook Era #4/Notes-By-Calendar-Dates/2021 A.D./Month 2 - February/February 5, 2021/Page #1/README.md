
# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 5 February, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 6 February, 2021
- Time Page Transcription Started: 03:57
- Time Page Transcription Stopped: 04:53

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0] ALthough the notes say "February 4, 2021" . . the day these notes were created was "yesterday" if my memory serves me correctly. I am starting to believe that I have made a typographical error in my physical paper material journal notebook where I thought "yesterday" was "February 4, 2021" when in fact, the day "today" is February 6, 2021 and so that makes "yesterday" February 5, 2021 . . Although these typographical errors are possibly in the journal notebook several times . . for February 4, 2021 . . when in fact the day was February 5, 2021 . . and I just forget to change the pages to read that . . since I am just now . . catching this typograhical error . . from the original time the notes were taken which was sometime in the morning . . of February 5, 2021 . . in the morning is what I say but I'm not really sure . . it could have been . . early evening or evening as well . . I remember the sunlight was still outside . . and not too . . early in the morning that the sky was blue . . or something like that . . dark blue . . or something like that . . a pale blue moonlight or something like that . . 

- [2.0] A graph with 3 nodes is drawn here . . 
  - (1) 3 circles are drawn to look like they are arranged in a equilateral triangle
  - (2) The original intention of this idea was live streamed to me in my consciousness perspective from "alien name here" who is from the "Speace" . . "I let them type this message and this is the thing they have written here and so please take it as a casual joke but also I was not necessarily feeling that all the information could have been streamed from myself but possibly there are guides who help me think or something like that" - Jon Ide
  - (3) My thinking was relating to the idea of the "data hierarchy management system" that we use in our world today . . for example . . "file systems" . . are a good example of "parent-child" relationship trees . . and those trees are . . not necessarily the most general way to structure information . . and so for example, you can come to "re-arrangements" of the data that you're looking at . . and so . . "re-arrangements" can be something interesting to consider . . I'm not exactly sure . . I don't know exactly . . what would be a replacement for the "creativity structure of the file system" which is for example like a chartoon character in a video game where you write computer code programs but you're using an old outdated computer program codebase to represent how your code programs look which is possibly an ilustration of my influence in looking at this area of the computer program . . but it isn't necessarily the case that the hierarchy of "parent-child" relations is outdated or that it's not useful . . uh . . well . . my thinking was relating to . . if you had another representation of code programs written in text libraries like the ones we use today . . what would that representation look like ? . . and so . . that's when these notes were . . starting to be replicated from my mental images of what was possibly a alternate replication of a data structure that could represent coding language programs . . well . . partially I feel like I was guided to think about triangles . . and so that is where I started . . 
  - (4) with a triangle . . (4.1) one of the nodes can be "where you are" . . "where you are represents" . . a list of data elements like . . "file.txt" . . "folder" . . "folder-2" . . "file-2.txt" . . etc . . (4.2) a second node can represent . . what are the files that this file is connected to ? . . and so for example . . "file.txt" is connected to other files by . . for example . . "a parent-child relationship" . . or . . "a sibling-sibling relationship" . . but . . those aren't the only relationships you can have . . and so you can create more as yo uwould like . . and so for example . . already this arrangement gives you the ability to have . . (4.2.1) "a file that is a child of a folder can also be the child of another folder which is the sibling of the original folder" . . and . . (4.2.2) "a file that is the child of a folder can be the parent of a folder that is the sibling of the original folder"
    - (4.2.1)
      - /folder
        - file.txt [file.txt-reference]
      - /folder-2
        - [file.txt-reference]
      - Additional Notes: symlinks are possibly like this but I don't know how they work to be honest . . I need to do more research . . 
    - (4.2.2)
      - /folder
        - file.txt
          - [folder-2-reference]
      - /folder-2 [folder-2-reference]

  - (4 cont'd) (4.3) a third node . . is a reference to the connection . . and . . was originally thought about to be . . "if you have a connection between elements in the original data list" . . "there are latent connections waiting to be referred to" . . "what are those latent connections?" . . and so for example . . latency means something like "invisible" until you are actually specifying those ideas . . and so for example . . if there are things to consider, those things are possibly considerable and yet you haven't necessarily considered them . . and so . . you let this area possibly be . . an . . so called . . "update map of area" . . or "update area map" . . "map of area" was the original name . . and then I supposed that you could call it an "update map of area" since . . you are possibly updating this map of area or this map of influence . . or this . . influence range of ideas . . hmmmmmmmmmmmmmm . . another anology that I was thinking about when trying to draw this . . triagular map . . is . . the idea that . . (1) if you have yourself . . you are that first node . . (2) if you have a friend that you know . . they are that second node . . (3) . . but you are not really familiar with how you and your friend know each other . . and so that is represented by the third node . . and so for example . . if you have eaten dinner together . . that's possibly a way that you know each other . . and if you have . . walked to university campuses together . . that is possibly another way that you have gotten to know each other . . and so you and your friend can learn how you are knowing each other through this third medium carrier which is like invisble but you are like . . making it up as you go along . . it is meant to be . . like a carrier medium capacity which has no realy limit to how you can know your neighboor . . 
  - 
- [3.0] The next set of notes . . were not specified on this physical paper material journal notebook page . . but were specified on the next page . . I have confidential material that is on that page . . and so I am concatenating that page . . here with only the non-confidential things . . 
- [4.0] Variably Enderson means "anderson" . . Enderson means . . "you can keep going but not as far as you think" . . "you can keep going but not as far as you think is like a finite list of numbers or something" . . "Enderly" . . "Ender" . . "Ending" . . "An End" . . "Enderson" . . is . . a topic that I'm still learning about . . and so . . I'm sorry if these notes aren't really helpful . . 
  - Variably Enderson . . this is a new term for me [Jon Ide] . . I haven't used this term before in my notes . . and . . uh . . well . . it was a surprise for me to think that . . "the star tree data structure" could be . . "Anderson" . . simply by being . . "Variably Enderson" . . uh . . "Variably Enderson" . . seems like uh . . the . . enderson grows and shrinks . . variably . . uh . . I'm not sure . . that's uh . . well . . I'm reminded of . . mathematics . . journaling related things . . I'm not uh . . really defining my terms well because I'm still learning about these topics . . but . . it seems like . . "Theorem: Show that if you are variably enderson then you are anderson" . . is a related mathematical theorem or something like that . . but because . . 
- [5.0] Link Casting . . this is a thought that came to mind . . uh . . "casting" . . is a word that suggests . . uh . . like throwing . . or . . uh . . casting a fishing rod . . uh . . and uh . . links can be like . . uh . . basic . . enderson . . uh . . windings . . and . . uh . . casting is like . . a variably relationship with all those enderson cast windings . . and so for example . . it's like a young person is throwing a casting rod . . repeatedly . . and the bait can change . . after each cast . . and if you can imagine that this is happening very quickly like . . "super microsecond . . bait and switch technique casting" . . you are then possibly imagining a great idea for how to envision "variably enderson" . . uh . . liks are also interesting because links . . are . . not necessarily like . . "fish food" . . if you are imagining . . a traditional . . casting sessions . . the . . uh . . "fish food" or the "bait" . . doesn't need to be . . "worms" or "egg shells" or other critters of interest for fish to be attracted to . . but can possibly be . . more . . casting rods . . and so your casting rods . . are like . . uh . . uh . . the bait . . but those casting rods themselves can have casting rods  . . which makes your fishing pole variably enderson to more variable hierarchies . . and so . . those casting rods . . that you're baiting can also have casting rods . . and so on . . and so forth . . with a gigantic . . casting rod fish net that has variably changeable casting rod bait items . . and so if you repeatedly cast your giant fishing rod . . in a pond . . 

## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

Star Tree
Visualization
Map

* Created Date Address: February 4, 2021 [1.0]
* Created Author Address: [My Real Name Here]
* Created Location Address: Place on Earth
* Created Data Access Address: Now
* Created Content Address: . . . 
* Created Content Address Hash: . . . 
* Created File Type Address: . . . 
* Created File Name Address: . . . 

* Update Date Address: February 4, 2021
* Update Time Address: . . . 


         O
Update Map of Area
    .         .
  .             .
O    .   .   .    O
Data List         Network Protocol
                  Connection
                  List

[2.0]

Star Tree characters
- Alphabet-name ->
- Balphabet-name
- Calphabet-name
- Dalphabet-name
- Ealphabet-name
- Falphabet-name
- Galphabet-name
- Halphabet-name
- Ialphabet-name
- Jalphabet-name
- Kalphabet-name
+ Create New Data Item

Network
- Network Connection Item-1
- Network Connection Item-2
- Network Connection Item-3
- Network Connection Item-4
- Network Connection Item-5
+ Create New Network Connection

Star Type - File Type
- Network Connection Address Item 1 ->
- Network Connection Address Item 2 ->
- Network Connection Address Item 3 ->
- Network Connection Address Item 4 ->
+ Create New Network Address

--

- Search Index
- Order Index (Date updated, alphabetical order, createdAuthorAddress)

[3.0]

- Star Tree Data Structure

- Exact Space Partition Program
- Anderson Specific Relational Map Construct Relation
- This is Variably Enderson [4.0]
- Anderson

Program management Becomes
Efficiency Management

Ander not nascent not acceptable approach
to sandle Ander nanannnnn


## Part III - Transcript Left Vertical Area Margin Of The Page

Write Notes Here . . 

## Part IV - Transcript Right Vertical Area Margin Of The Page

[3.0]

Link
Casting
(Fish
Net) [5.0]

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



