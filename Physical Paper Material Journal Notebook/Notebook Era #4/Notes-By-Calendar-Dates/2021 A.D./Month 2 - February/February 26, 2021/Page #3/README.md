
# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 26 February, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 28 February, 2021
- Time Page Transcription Started: 15:30
- Time Page Transcription Stopped: 15:45

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0]
  - One circle is around the word "let"
  - A second circle is around aya . . "no text here" which is like . . well . . I didn't know what to say . . I think I was just doodling . . but uh . . 
  - In computer programming today . . there's like a notion of a "variable" which is like . . "well . . I like to know stuff . . " . . "aya is like . . well . . I can't tell you that . . stuff aya . . " . . well . . [smirk face emoji] . . well . . "you're like the worst . . why can't you let me know what I need to know ? ! . . I wanna know stuff . . aya . . aya . . aya . . let me know . . "
  - "well well well . . "
  - "well well well . . "
  - "well . . well . . well . . "
  - "well . . well . . what have we got here . . someone who wants to know stuff right?"
  - "yea, they keep askin' questions like where's the food at . . what do you think of that robert?"
  - "well . . well . . well . . that's kind of a big deal . . like what they hell are they tryin' to look at ? . . "
  - "they lookin' for numbers and stuff . . you got a deal for that? . . "
  - "hey, do you think that's illegal to tail yourself in all multiple directions at once?"
  - "well, if you do that it's like . . "
  - "hmm . . well then . . "
  - [close anology here]
  - hmm
  - [Seth]:
  - unfortunately disclosure of the specifications of these events is illegal to express without further development of the product resource provided here by ecoin . . or something like that . . it's like you have to keep asking questions but there's not an answer . . the answer abides by you in your aya aya aya discriminatory aya-proportionality aya discriminatory aya-proportionality [sanderize this to tell your aya to not look like a cloud of dust] which is like "we are all one" but you really aren't allowed to know what that one is . . it's like a rule to let it be itself without necessary hypertudinal coordinate agreements like "hey, that's like what this is and that's what that is but it's like yea . . it's like yea . . that's like . . aya" . . 
  - [Jon Ide]: well well well . . this is like a big dealio . . I haven't looked at this answer-question skema until now . . that's a big deal . . aya . . aya . . aya . . 
  - [Seth]: yea, it's like your proportionality agreements have to be kept inside your mind or your ladder equipment gear like "what type of characters do I want to cast on the screen of my world of warcraft player anatomy spanner board?" "do they need to span spanderson?" "do they need to andel laderson?" "what types of flags do they need to spandel?" "ayewa" "ayewa" "ayewa" . . ayewa is a term now that means "a way of being" "a way of applying" "a way of speaking" "a way of music" "a way" "ayewa" . . "ayewa" is aya's neighborly cousin like . . "well . . I don't know what you're saying but at least I know what I like about what I like about stuff . . and so it's like . . well . . I guess I'll look like you're saying like you look like you look like you look like you . . uh . . yea . . okay . . uh . . ayera . . " . . ayera is like a cap tol (pronounced "lock") on the spelling of the word "aya" which is like . . "well, we can say . . ayera . . " and that would be like saying . . "and that is all" . . and so it's like if we have . . "aya aya ya . . ayera" . . it's like saying . . "aye] which doesn't have an ending but rather hangs there in mid air waiting for you to spell out an anderson related aya . . like for example . . "what birthday was jeorge washing" . . and then it's like . . "well . . there are variable ayas that are able to be capricorned here which are like 'what birthday was george washington agreeing on their birthday to be?' 'what birthday was george washington agreeing that they should span yellow underwear for that day?' 'what birthday does georgggie washington from parallel reality version aeta-o agree that there are average sitizens does exctrastris in their beidesroms like weye wye weye [which is "aya aya aya" in the alternative version reality they are from . . and so it's like pronounced "aya aya aya" for us but it also has a possible alternative pronounciation for their weira (pronounced "waya") . . ] aya . . 
  - 
- [2.0]
  - Nowwwww
  - the "w"s in the "Nowwwww" . . really come together like hmm . .
  - 

- [3.0]
  - hmm . . well . . it's like . . okay . . I had to write uh . . well -_- . . i'm not sure . . I wrote a few data structures (1) list (2) tree (3) arrangement (4) aya . . which are the the names of the data structures in memory for the computer systems that I'm more familiar with today . . in aya-1956 . . and so it's like . . well . . if you say . . hmm . . it's 
  - oh yea . . by the way . . it's kind of interesting to think about times 
  - oh yea . . by the way . . it's kind of interesting to think about proportionalities . . 
  - someone could say . . hey look here's a graph of a lot of points on the map and arrows for where those points are going to migrate to next in the next time step . . well anyway . . it's like . . if you look at the dots . . that's kind of a big deal . . but if you look at the proportionalities of the dots its like "wow . . that's really interesting" . . so for example . . I could tell you my name is "zack" and . . I am from . . "Florida " . . and already that is a lot of information for you to work with if you are familiar with area of interest proportionalities like "earth space time events" or something like that . . and so for example . . if I'm so-called "lying to you" then that could be . . "intentional" or "accidental" and so for example maybe I wrote my name wrong and spelled "zach" instead of "zack" with a "K" and so it's like . . well . . you are quite familiar with aya proportionalities so you know that you can start building a directory list of all the mistakes that I could have made . . and so its like . . "well 'so-called zack' . . it looks like . . " . . and then for example you could make a big mistake by assuming I'm from California because you're like "well, the proportionalities are aya-effective because its like "Florida and California" are in the same so-called country and so you're kind of being an aya-wizard" . . well . . okay . . you can do a lot with proportionalities like this . . well you have "zack" which is cool . . and so you probably also have . . "aya-zack" proportionalities in your list . . and so for example you can possibly back sample your data pool of "aya-zack" listings to look like you know about your population size that comes in for the next time step so for example "steven" starts to sign up for your website and they say "I'm from Mexico" and so its like . . well well well . . now there's more data . . what are those new aya proportionalities and how are they relating to the previous aya proportionalities and in which aya relations are we talking about their neighborliness? . . well . . okay fine . . that's okay . . so its like if you have a demographic chart of numbers about population sizes of towns . . maybe you can hypothesize 1 or 2 neighbors didn't sign the population poll listings and so its like . . "well . . possibly this measurement is like aya-tastic and needs to be pool sized aya proportional times like . . 'ask questions', 'ask questions' 'ask questions' 'ask questions' aya-wise magic times and then that's possibly a stronger confidence ratio of raining down what the hell those ayawas are doing in your eyea . . it's like well . . I probably think this tire measurement is 12 inches but if I look closer at the gauge pressure device, possibly there needs to be more specifc aya relaxation machines that don't need to ask questions about aya but look at proportional statistics like what the hell style calculations that are like 'what the hell type of groove is that. . how come that groove effects my wheel size more in this style of winter weather? . . sicko style aya wheel compartmentalization machine . . aeya . . aya . . aya . . " . . those are some nice things to say but they're like . . well . . okay . . fine . . aya . . 
  - well . . okay . . I haven't known . . uh . . -_- . . well . . uh . (1) computers tomorrow could look like they have aya-related operations . . and so . . uh . . for loops are like aya . . and so its like hmm interesting . . I'm not sure . . maybe . . maze-aya . . is like a construction you could have that's like . . 'build me a maze that's like no one has a problem-answer solution to that aya . . ' . . which is like . . "well . . if manderizer the manderizer telled the species to spanel (pronounced "spandel") the spanderizer, where would the andels stell the staim eny?" . . which is like "well . . don't we need to define these aya proportional terms or are there aya related aya-incies to relate to this aya-nanderizer?" . . well . . so if you have aya related aya wise aya . . then it's like . . wow . . okay . . hmm . . interesting . . its like . . well . . hmm . . interesting . . wow . . okay . . hmm . . interesting . . which is like "what the hell?!" to any new programmer for your computer system because its like "what the hell is an Nonsense-loop?!" is that supposed to be like cannot answer but realize your nana . . ?! . . anananananaa . . I had a dream the other night . . several nights ago now . . from February 28, 2021 when this is being written . . but it's like if you look at psychedlic dreams like when snakes turn into cacti . . and the swamp is full of these lizard-like snake cacti creatures and they're like morphing back and forth in various aya . . well . . it's like hmm . . interesting . . an artist could do a lot with that type of imagery machine . . it's like . . yea well . . the colors are also variably aya . . and so its like you can do so called uh "photopainting with aya related paints" or something like that . . painting with music is also similar and so for example, your music could really be related to unknown new beat drum music sets that are like "well . . you're like an idiot or something for writing such an awesome soundtrack like this . . aya . . now I'm gonna go home and cry . . you're the worst . . [Drake & Josh Sounding Emoji using Josh's Voice]: "The Worst!!" . . well well well . . okay . . (2) . . for loops in modern computers are really quite cool . . hmm . . I guess uh . . well . . an arrangement is a for loop with . . uh . . updates to variables . . and so for example . . uh . . if you're sorting a list [1.0] repeatedly in new orders of the sorting to happen and so (2.1) "numerical order", and then (2.2) "order by date created", and then (2.3) "order by date last updated", and then (2.4) "order by x-y-z event being operated on this item for that event's date created", and then . . (2.aya) aya-related ordering sequences . . 


[1.0]
"sorting a loop" that was a typographical error but wow, that's an interesting concept . . I meant to type "sorting a list"
Let's take a look at what "sorting a loop" would look like . . 

// list is denoted by "[]"
let list = [{ item: 1, dateCreated: '12/12/2012' }, { item: 2, dateCreated: '12/13/2012' }]

// loop is denoted by "aya"
let loop = aya-wow-start,aya-wow-start,aya-wow-start

. . 

what does that mean to "order" a list ? . . order is related to . . "order of magntitude" . . which is like . . alright cool . . we can say things like order or eya . . which is like aya directional arrows but without directional aeya capacity limit locks like "you can (pronounced "have") to look this way to rpuns (pronounced "pronounce") this aeya" . . okay . . and so for example . . "how tall are you?" . . "how tall are your siblings?" . . "how tall are your grandparents?" . . are related to an order of magnitude directional vector . . which is itself ordered in a relational order aera . . like that of the answer for . . "what is your middle name?" . . "what is the middle name of your siblings?" . . "what is the middle name(s) for your grandparents?" . . 

well ordering a list by alphabetical or numerical order on "height" or "middle" name are aeya proportional by the name of . . "well . . alphabetical number system is aeiya (pronounced to appear like "aieya" or "ayea" with other multitudinal front line horizons of superimposed images of text spring spare free ideas of aya relations . . ) . . proportional to . . "now number system like 1, 2, 3 etc" . . and so for example . . A = 1, B = 2, C = 3 . . etc . . or in other proportional diagnostics terms A ~ 1, B ~ 2, C ~ 3 . . etc . . and so the appropriation characteristic of aya (aeya, appeared aeya) proportionalities is like . . well (woll, proportionality appearance (arrearance, proportionality aeya)) . . well . . it's like hmm . . well . . a height list like [4, 3, 6, 5] is like okay . . fine we can sort that in numerical order . . or aeya appropriate alphabetical order . . alphanumerical order aeya . . and . . ['laramy', 'wow', 'hey', 'yo'] . . and so its like . . wait a minute . . it's like . . okay . . let's use the same appropriate sorting 'algorithm' on both lists right ? . . which makes sense if you're an aeya . . 

But we're like hmm . . interesting . . what's this proportionality aeya about aeyas from other aeya reiaya . . ? . . it's like well . . it doesn't really like you to tell it what to look like because yu keep thinking about the main idea . . which is like (1) yes, triangles

sortAlgorithm([4, 3, 6, 5], yea) = [3, 4, 5, 6]
sortAlgorithm(['laramy', 'wow', 'hey', 'yo'], yea) = ['hey', 'laramy', 'wow', 'yo']

ayaSortMachine(hypertudinalSortOrderMachine) = hypertudinalAyaMonsterMadnessAya

ayaSortMachine(hypertudinalSortOrderMachine2) = 

. . 

-_-

well maybe the usecase is very different -_-

hell . . 

well you look like you have to permute a list of ideas like (1) say my name . . (2) say my name . . (3) say my name . . (4) aya . . 

hold-on is like a keyboard term talking about "no where is there that thing that was talked about" . . 

pica
pica
pica

pica

. . 

okay a loop is like . . I want to tell you what that looks like . . so always don't look like me but it's like if you look like me then you're like an ancestor talking about . . "well . . okay . . fine" . . 

so for example I'll say something like . . yea . . you look like . . 1, 2, 3 is the best idea you've every seen so I'll keep doing that and nothing will change for you since I hit that bench mark 5 days ago . . 

well . . you look like 1, 2, 3 isn't your jam . . so let's say you're like an address keeper . . you like stelling my stella . . 1, 2, 3 is for today . . and 3, 2, 1 is for tomorrow . . so . . we'll . . yea . . well whatever we do its still going to be like I'm telling you 1, 2, 3 . . because its like . . yea . . that's what you look like . . 

so well . . you don't look like you have anything else so it's like . . well . . now you really try to anderize my anderizing . . 

anderson . . anderson anderson is like aya sorting right?

its like spam manderson without letting them spell anything

well 1, 2, 3 is a problem you can say because it's like "yes, I agree. You wrote a book 9 months ago and everybody loved it."

"yes, I agree. You green lighted an aya aya aya aya aya aya aya . . "

"you are eating apples in aya aya aya aya aya aya aya aya aya . . ayera" . . well well well . . its like if I told you anything about your area of interest it would really totally spoil your food because it's like . . you haven't let me know what the order of aya is . . and so it's like your cook book is really supposed to be sitting on a timeline bench waiting for you to write it but you let everyone know what you've already written it . . so it's like . . well . . hold on . . 




[2.0]
reiaya . . aya . . aya . . aya . . yeah . . well it's like you look at the word -_- "aya" it's like that . . you are asking to see the word "aya" so it's like . . "aeya" . . yea .  uh  .. . "ooops" is neighborly as well as so if you say . . "ooops" means "I don't know" it's like . . yea . . okay fine . . then it's like . . "ooops" and "aya" are like radar effective triangulation signals for the helicopter meaning term "I don't know" which is like . . "aeya effective" "aeya effective means something like . . you look at one direction but you spell your aeya like . . aeya . . aeya . . aeya . . aeya . . so its like time sampling of that spelled direction in aeya proportional aeya . . which is actually aeya proportional aeya but "aeya" in the second case is like "aeya" but I have to keep typing the wrong mistake because if I typed "aya" which is what I reammy meaning to say then it's really a misconduct and not "aeya related areya" . . like yyou're trying to like [lie] a kid about what types of whords [words] they are seeing on the ways [way] to wrke [work] . . aeya . . aeya . . aeiaya ayeia are like aeiya reiya . . which are like aiya . . aiya . . which is like aiya . . 




## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

February 26, 2021

## Part II - Transcript Central Vertical Area Body Of The Page

  _____       ___
 /      \   /     \
|  let   | |       | [1.0]
 \______/   \ ____/


Nowwwww [2.0]

let nowNowNowNowI
let nowNowI = 1

---

> list > [0, 1, [0.9], N_1, N_2, N_4, N_M, N_Q]
> tree > {'hello':0, 'helloAgain':1, helloAgainAgain:0.9}
> arrangement > for () { update variable Aya }
> Aya

> Aya Fake Id: Arrangement List

> Now                                     _________
  > Arrangement List                    /            
    > Now                              / Arrangement  ^
      > Now Arrangement List          |     Loop      | 
         . . .                         v             /
                                          _________ /

---

> Aya looks like Arrangement [3.0]
> Annnn
> Now
> An Arrangement


## Part III - Transcript Left Vertical Area Margin Of The Page

Write Notes Here . . 

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



