
# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 26 February, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 28 February, 2021
- Time Page Transcription Started: 11:37
- Time Page Transcription Stopped: 11:39

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0] Well . . "Proportionality Ratio" . . this is a term meaning something like . . "Aya" . . 
  - "You can take a loop of eyas" . . "you can fantomize that loop" . . "you can aya-ize that fantom" . . "you can spam that sample" . . "you can aya-ize that la-la-la like what the ffffuuucccccc" -_- xD . . please don't read that . . 
  - okay well if you want to think about proportionality ratios you can think of . . well . . take a look at this whatever . . now I want you to tell me . . whatever . . -_|
  - take a look at this . . whatever . . "a cat" . . tell me what the hell the cat looks like . . in relation to this . . "aya" . . which is like . . "well . . I think it looks like . . hmm . . if I had to put my finger on it . . [finger on lip, looking up at ths sky emoji character here] . . hmm . . if I had to put my finger on it . . the proportionality ratio is . . "aya" . . like . . "what the fucccccccccc"" . . yea . . that's pretty much it . . you got it . mhmm . . you got it . . good job . . wow . . aya . . 
  - take a look at this statement: tell me
  - take a look at this statemenet: well me
  - take a look at this statement: okay me
  - what are the proportionality constants between these like . . "wow" statements . . well . . it's possibly something like . . "wow" . . okay . . so it's like . . "well . . okay . . wow . . hmm . . like . . hmm . . well . . hmm . . okay . . yea . . okay . . okay . . okay . . yea . . wow . . okay . . yea . . " . . well . . it's like . . "well . . well . . well . . well . . hmm . . well . . "
  - yea so if you look at that proportionality constant you can do something like "double take" which is like . . "let's take a look at that again" . . "well . . that was like . . hmm . . let's see . . " . . "well . . double take again and your proportionality constant is like . . "well . . okay . . fine . . okay . . fine . . " . . and so its like . . "well . . okay . . fine . . okay . . fine . . okay . . fine . . uh . . well . . hmm . . hmm . . hmm . . hmm . . well . . okay . . well . . okay . . well . . okay . . well . . hmmm . . okay . . well . . okay . . well . . okay . . well . . okay . . hmm . . " . . "well . . it's like . . hmm . . well . . okay . . hmm . . " . . "well . . well . . well . . well . . well . . well "
  - okay so in time and space you can span a lot of proportionality ayas . . like . . "what the hell?!" . . and the ayas of space and time are like . . "what the hell?!" . . its like . . "you create your own reality" which is like a space time aya that lets you look like . . "what the fucccccc?!" . . 
  - maybe a more practical example
  - 0 is proportional to 1 by a ratio of 1
  - 0 is proportional to 2 by a ratio of 2
  - 0 is proportional to 3 by a ratio of 3
  - but if you want to say something like, what's the proportionality ratio between 2 and 3 ? . . 
  - well . . you can ask (1) what is the proportionality ratio aya between each of these ? . . its like . . aya . . well . . aya . . is like possibly even a variable case statement like . . "what is the proportionality ratio relationship between each of these?" . . well . . "relationship" is like "aya" but its like a weak "aya" because its like a term that possibly supposes "aya aya aya aya aya . . aya aya . . aya . . aya . . aya . . " like . . aya-degrees of aya . . which is possibly a weak form of aya . . which means something like . . "well . . I really have no clue but in aya-variable circumstances . . I'm like . . well . . it's like . . well . . alright fine . . I'll say this is aya . . " . . 
  - its like your proportions can change in space uh if for example those are your aya definitions . . but if they don't change maybe you need aya-variable proportionality aya-edge-case-ayas . . which are like aya . . but if you don't say that then the problem may be like . . "what the hell?!" . . it's like . . aya . . like "what the hell?! do we have enough information here?!" but -_- . . "what the hell?! do we have enough information here?!" -_- is like possibly a weak form of aya . . because its like . . "what the hell?! . . do we even look like we're asking a question?!" . .
  - hmm maybe a more practical example
  - its like . . hmm . . I want to say that my friend is like 10 feet tall . . 
  - My cat is maybe 1 foot tall . . 
  - well . . that's like a proportionality ratio of . . 1 to 10 . . right ? . . well -_- okay its like a fake example because really the proportionality ratio is "aya" . . its like . . well . . it could be 10 to 1 right ? . . or it could be like . . 2 to 20 which is like ? . . well what the hell?! . . and in certain mathematical systems its like . . 1 to 10 and 2 to 20 are not equal . . so its like you have to aya like "what the hell?!" . . so for example its like . . "aya" . . 
  - equal and equivalent are interesting terms, well its like . . 'equal' is possibly -_- . . proportional to 'equivalent' right ? -_- well . . aya . . its like . . maybe it depends on the language you use . . its like . . well . . "i speak math from 1922 . . " . . and . . "i speak math from 1999" . . well . . its like hmm . . "what's the proportionality ratio of agreements between these two sets of goofballs from 1922 goofballs to 1999 goofballs . . what are the areas of interest for spamming an aya that spams the topic of interest . . " . . its possibly . . aya . . which is something like . . "well?! . . hmm . . well . . hmm . . well . ."
  - its like if you want to use number systems to say . . uh . . what proportionalities are . . then thats possibly okay . . uhm . . 
  - well then you have proportionalities like . . "what the hell?!" . . which are like . . "dreams" and uh . . "the taste of food" . . and uhm . . "physics" and uhm . . "building houses" . .
  - 
- [1.1]
  - "Proportionality Ratio Gun"
  - Proportionality Ratio . . uhm . . 
  - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
  - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
  - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
  - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
  . . 
  - that is possibly a proportionality ratio list of 4 proportionality ratio eyas . . eyas . . are like . . arrows of aya . . which are possibly like . . aya . . aeyas . . eyas . . which ever one makes more sense to use . . I'm not really sure . . aeyas looks cool but . . eyas . . uh . . is possibly easier to write so its like [shrug emoji character here] . . also I haven't written about ayes . . aeyas . . eyas . . yet before so its like . . aeya . . I'm not sure where I'm going with this whole topic . . it's like . . 'what the hell?! in one dimension of aya' . . so its like . . 'aya aya' . . but it's like . . 'aya' . . because aya is like . . aya . . so its like . . aya . . uh . . aya is aya . . so it's like . . aya . . aya . . aya . . aya . . aya . . if you can write a book . . and then you can tell yourself you wrote that book . . what types of books would have been typed because of one book being typed . . its like . . aeya books were typed . . aeya . . aeya . . aeya and each of those books is like a physical manuscript book with their own typographical errors and spelling mistakes and aeya recourse correction maya . . maya is like mathematical maneuvering aya . . maya . . maya . . maya . . maya . . 
  - well . . these for aeya . . eya . . aeya . . is like well . . let's see . . we can possibly look like . . 0.1 is the incremental difference between each of these eyas pronounced "eh-yuh" (if you want)
  - 0.1 . . is aeya difference between 0.1 and 0.2 . . so its like . . well . . its like okay fine . . 
  - 0.1 is aeya difference between 0.3 and 0.4 and so its like . . well . . okay . . fine . . 
  - well . . hmm . . 
  - okay fine lets look at another proportionality ratio aeya
  - 0, 0.333333, 0.666666, 1
  - 0, 0.333333, 0.666666, 1
  - 0, 0.333333, 0.666666, 1
  - 0, 0.333333, 0.666666, 1
  - 0, 0.333333, 0.666666, 1
  - well that's another aeya . . aeya . . okay awesome its like we have awesome proportionality measurements which are like . . well in one set we can try to measure the . . uh . . proportionality of uh . . light skinned to dark skinned people in the city . . which is like . . well . . 0.1 can be like . . that's uh . . 10% dark skinned people . . and 0.2 can be . . 20% dark skinned people . . and so that's pretty cool . . so . . if we have lots of these lists its like . . 
  - San Francisco
    - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
    - 0.1 = 10% = 10% dark skinned people
    - 0.9 = 90% = 90% light skinned people
  - New York
    - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
    - 0.4 = 40% = 40% dark skinned people
    - 0.6 = 60% = 60% light skinned people
  - Papua New Guinea
    - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
    - 0.8 = 80% = 80% dark skinned people
    - 0.2 = 20% = 20% light skinned people
  - Paris
    - 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1
    - 0.3 = 30% = 30% dark skinned people
    - 0.7 = 70% = 70% light skinned people
  - well well well . . that's really good . . we got a list of proportionality ratios for the light skinned and dark skinned people in the city . . we can do that for cats and dogs and other animals and creatures as well . . if we keep a so called . . "proportionality graph" . . which is like . . well . . "here is our set of things we're measuring the proportionality of" . . and so . . "set one can contain . . cats, dogs, lions, hippopotamus, and mice" . . and that's like . . 5 creatures so its like . . a nice proportionality ratio list uh . . well its going to look like we sample 5 different creatures like . . uh . . whats the proportionality of this or that creature with respect to the rest of the group ? . . well . . lets say 1 cat, 1 dog, 1 lion, 1 hippopotamus, 1 mouse . . and so . . hmm . . well . . that's a ratio of . . 1 / 5 . . for each of those creatures . . so the total number of aya . . reports to . . 100% . . which is like . . yea . . we've accounted for all the creatures in the set of proportionality group aya . . like wow . wow . 
  - well . . 
  - you can look like you know what the set looks like but that's like . . okay fine . . that's not a bad idea . . 
  - but now you really area not a friend of yourself when you talk about . . well . . what the hell . . 
  - what the hell is like . . well . . that's uh . . hmm . . well . . 
  - yea . . and so for example . . you're like . . hmm . . [hand of chin emoji character here] . . and so you're like . . well . . hmm . . 
  - aya . . 
  - okay well you're like . . "hmm . . it's like . . wait a minute . . there's like . . how many types of water molecule configurations in this lake of water?" . . hmm . . "like what the hell?!" . . hmm . . 
  - okay
  - well . . your proportionality ratio gun takes your spallicking and spans the spizzas of spanneristima and tells you what the hell faster than the hell the hell . . the hell
  - -_-
  - a proportionality ratio gun is like a racing meter where you tell your computer to tell you all the speed high ways that look like they have illegal battles to settle like "what the hell kinds of turns are those?!" . . 
  - 
  - multi-beaked-poem-race-car-machine-head-track-road-aya:
  - by jon ide-aya
  - 
  - well you know that birds have beaks
  - but not all birds have beaks
  - and not all beaks look like beaks
  - not all beaks look like beaks but your beak checker needs to know
  - your beak checker needs to tell you what the hell type of beak is that
  - it looks like your beak checker is broken because cities are not beaks
  - well beaks are supposed to be like food gathering systems
  - so why are cities being tabled as beaks in the record meter
  - well cities are like huge skyscrapper building race roads
  - if that's what your city looks like
  - the planet is like a multi-beaked bird right?
  - well, that's not necessarily what my master wants to hear
  - let's tell them this is a poem instead
  - aya
- [1.2]
  - Aya Proportionality Ratio Gun
  - well . . Aya . . and proportionality are like aya related . . its like . . aya . . aya . . aya . . aya . . aya . . well . . that's it . . right ? . . it's like . . 5 degrees of aya ? . . right ? . . and then you know for sure ? . . well . . yea . . let's 
  - aya -> aya
  - aya -> aya like you don't know
  - aya -> aya like hey, I thought about that
  - aya -> aya like hmm, well that looks interesting
  - aya -> aya like . . well . . okay . . that's kind of . . aya
  - . . 
  - alright, it's like hmm . . well . . let's see . . I don't know if I like this aya relational aya . . it's like . . hmm . . aya . . 
  - alright . . 




Eric Weinstein: Geometric Unity and the Call for New Ideas & Institutions | Lex Fridman Podcast #88
By Lex Fridman
https://www.youtube.com/watch?v=rIAZJNe7YtE
[1.1]: [shout out to Eric Weinstein, I think this topic of "rulers and protractors" relates to their field of work in Gauge Theory]


## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

February 26, 2021

## Part II - Transcript Central Vertical Area Body Of The Page

Aya Proportionality Ratio Gun [1.0] [1.1] [1.2]

Aya Proportionality Aya Nan

// for

---

// 0, 1

---

// list of loops

---

// variable let now = 0

---

- Easy to say: Easy to Read
- let now = 0
- let now
- 

---

- start a loop (Aya frequency) (Aya)
  - now update
  - loop list update // Aya
  - now update

## Part III - Transcript Left Vertical Area Margin Of The Page

Write Notes Here . . 

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels



