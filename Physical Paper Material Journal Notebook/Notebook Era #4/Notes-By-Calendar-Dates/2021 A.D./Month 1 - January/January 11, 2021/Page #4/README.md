


# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 11 January, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 12 January, 2021
- Time Page Transcription Started: 23:09
- Time Page Transcription Stopped: approximately 23:32 plus or minus 10 minutes

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0] At some time during the day I had . . thought about . . the possibility of . . a t-shirt . . that had . . uh . . I guess . . movement . . uh . . -_- . . uh . . but I'm uh . . not really remembering what inspired that thought so much now . . I think . . I was thinking about . . how possibly in the past . . the . . Greeks . . uh . . or . . uh . . Ancient Romans . . uh . . some type of people in the civilizations of old . . the people that were . . uh . . comfortable in their social situations and were spending their leisurely time thinking about all sorts of possibilities . . well anyway . . I imagined myself as one of those people . . if I was a person in the old days . . and uh . . uh . . well . . things like . . uh . . the current . . manufacturing processes for developing . . clothing . . are really unknown to me . . even today . . and so for example . . I'm not really a gifted sewer or uh . . I don't really know what types of gifted activities go into making clothes . . and so . . uh . . if I were living in those old days . . those things would really puzzle me and I don't really know how I would direct my attention to think about those things being a possibility even in those earlier days . . and so . . trying to do . . like . . a reverse engineering process for our cvilization today . . for someone who lives in that earlier civilization is really a . . uh . . complicated task if I am only allowed to use thinking patterns that that earlier person would use . . and not take advantage of thinking patterns that are easily available today . . and so I don't assume things like . . knowing you can use plants . . and plant cotton to make certain types of threads . . because uh . . to be honest . . that's possibly a serendipitous thought . . that's uh . . not necessarily the most apparent things to do . . to use cotton fibers that are spun in troves and . . uh . . concategnoes those things into a cotton fabric t-shirt or something like that . . uh . . I uh . . even today don't reallly know much about that process . . and so . . uh . . well . . to think in terms of someone uh . . who uh . . was living in the old days . . is an interesting exercise to perhaps imagine the sorts of ideas uh . . that are available and abundant to use today . . or to . . "us" . . today . . to the people of earth today . . and so for example . . you can improve the state of civilization in the past . . as someone who had the time to leirsurely exercise their thoughts and endeavours in that type of way . . uh . . well . . uh . . hmm . . uh . . I guess you could do the same thing with computers uh . . I've done that exercise in the past before as well . . and it's interesting to try to imagine what types of leaps of imagination you would have to make to imagine and create those types of tools and instruments and even the precursor to the tools and instruments that lead to . . the computer revolution that we know about today . . those tools and precursor tools were first lead by computerized thoughts in the minds of a human being as you could possibly imagine . . uh . . I'm not really sure myself . . and I'm not really necessarily related to the topic of being an expert in this discipline of scientific improvisation . . which is a type of computer mathematics which takes advantage of free will creativity xD -_- I'm not really familiar with this topic to be honest . . ghosts could be heavily involved or more heavily involved than I think . . because some of the knowledge and insights may not lead themselves to easy and clever maninpulation without considered careful thoughts in those types of jump patterned barrier ideologies -_- but that's a weird description that I just made up and so don't let your imagination dwindle by paying attention to those words . . uh . . 
- But to continue what I was saying . . I was thinking if . . I was a person in . . the greek days . . uh . . what types of things would be interesting about . . what people wear . . well . . one thing that would possibly be interesting to me . . is . . the idea . . of motion . . since it seems that people are attracted to motion in some sense . . it would possibly be in an area of thinking to imagine . . the clothes on people's bodies . . uh . . and also wonder about things like . . what if the body of the clothes was made of of . . water . . which is a uh . . substance that could be around and flowing in lakes and rivers and streams or something like that . . and so for example . . uh . . if I am a greek young person . . uh . . a greek young person uh . . -_- . . a young man I suppose . . uh . . uh . . well . . uh . . -_- . . I would possibly be thinking about . . or I could possibly think to myself . . hmm . . well . . I see this water flowing in this direction from this . . river bed . . or from this small waterfall near our house . . or something like that . . wouldn't that be interesting to see . . water flowing on the bodies of people as well . . -_- . . well . . maybe it is a leap of imagination . . but something like that is possibly possible for someone who is laying around and relating things they see uh . . or something like that . . 
- Well . . uh . . t-shirts that had . . things like water that moves the gels or inks around in the t-shirt to make uh . . smiley faces move and flex or something like that . . would possibly be an impact or an effect of this type of idea . . uh . . 
- The frames of pictures drawn in my physical paper material journal notebook . . are 3 frames . . 1 with the . . sweating face emoji . . 😅 . . and the other 2 frames . . are showing the same emoji . . but . . the . . sweat drop is going to the floor . . or going to the ground . . with the way it slides down the face of the person's face or something like that . . and so . . uh . . hmm . . I suppose that could be an interesting movie to watch on someone's T-Shirt . . as they are walking through the grocery store or something like that . . 

## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

An order of magnitude schema on the page of this device on a sheet of paper with a pencil is a most copricious Device to carry out and manage and so you should start your live stream to show everyone your work in progress

-

- Microfluidic Fibers for
        Water Dancing Effects [1.0]
  - T-Shirt with water that moves and colors change shapes and sizes like horizontal and vertical display sheet clothing


{Notes @ Additional Notes: 3 shirts are drawn here}
{Shirt Number 1} {Shirt Number 2} {Shirt Number 3}
Frame #1            Frame #2          Frame #3

## Part III - Transcript Left Vertical Area Margin Of The Page

January
11, 2021

## Part IV - Transcript Right Vertical Area Margin Of The Page

Write Notes Here . . 

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels






