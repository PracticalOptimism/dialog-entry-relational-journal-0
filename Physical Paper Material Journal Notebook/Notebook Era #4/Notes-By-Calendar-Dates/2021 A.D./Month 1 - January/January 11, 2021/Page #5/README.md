


# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 11 January, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 12 January, 2021
- Time Page Transcription Started: 23:32
- Time Page Transcription Stopped: xx:xx

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0] I was laying on the couch thinking about something . . uh . . I was imaging a scene of some kind . . I'm not really sure what those people were doing in the imagination scene that I created . . they could have been . . dancing activities . . -_- . . -_- . . it was something ordinary and not necessarily related to dancing -_ . . in any case . . I found it interesting to think of this thought . . uh . . and uh . . how possibly it's difficult to share this thought even if it an ordinary idea . . for example . . imagining someone sitting on a couch . . while typing on their keyboard . . is a good imagination device to imagine what I was doing on January 12, 2021 @ 23:35 . . but . . uh . . well . . uh . . reading the types of text on the page . . gives you an idea . . but also . . -_- another type of description relating to uh . . other characters are possibly possible but . . uh . . -_- . . well I think I summarized those types of characters by this statement of . . "a dance memory fever which you're not allowed to observe" . . -_- . . uh . . -_- . .hmm . . one thing that's possibly interesting about this statement of . . "observation"-_- . . -_- . . well I think I was meaning that it is not observed because it was uh -_- at my own typme period in which this was a difficult thing for uh . . people to do or something like -_- for me to -_- keep the memory image of this small dance scene envigonrment in my mind's eye and project ito to my brother . . uh . . or my mother who are usually in the house and doing their own things . . -_- . . -_- . . well . . if you have this imagine . . -_- . . -_- . . uh . . -_- . . hmm . . "dance" was meant to describe the type of . . dancing spirit of the type of thing that is an imagination device . . which possibly seems to me like a type of dancing thing . . like an arrangement in computer terms I suppose -_- . . which is like a variable data structure with infinite loops that traverse the variable data structure and update its attributes in various orders of further arrangements or something like that . . :O . . uh . . "memory fever" . . felt to me to refer to possibly a joke since it is like . . I felt like -_- it's a type of sickness to have an imagionation like a human being which is only possibly one type of imagionation construct of several other types of imagination constructs that create new and original thoughts and appearance -_- but possibly those aren't even my words but possibly they were suggested to me by invisble ghost people that like to suggest things for me to consider -_- and so I'm hardly even a smart person to begin with I'm only a smote of dust typing for the lucky luck of lucktyping scriptualizations________ . -_- . . hmm -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . -_-

- [2.0] "Overlays" are something that I've been working with while building the "Ecoin" website . . uh . . When . . you click on a button . . like . . "Send Payment" . . and . . "Overlay" . . structure is shown . . which is . . like a . . rectangle dialog box . . with the background of the dialog being slightly transparent and so you can see the original website content that resides . . "behind" . . or . . "underneath" . . or . . "on the other side" . . of the . . "overlay dialog component" . . or something like that . . and so for example . . you are able to see . . some of the . . elements of the original page . . to give you a clue that you are able to possibly click on the overlay . . background transparency screen . . to allow you to navigate back to the original page . . which is quite a useful feature . . the overlay background transparency area . . is like . . a back button . . which is very large . . and so the large surface area is really useful to allow you to . . uh . . have more degrees of freedom in your traversal process of going back to the original content that you're looking for . . or something like that . . 
- Well . . uh . . I'm not exactly sure . . I have thought about overlays . . uh . . here and here . . but haven't been really able to relate to them in a type of . . uh . . formal way that relates to allowing them to exist without a type of theoretical system that is also able to relate them to abilities like uh . . compartmentalization . . or uh . . making them uh . . a part of uh . . a type of conversation system that makes it really easy to apply overlays in a types of mathematical algorithmic format or something like that -_- . . possibly other types of ways of tsaying this same thing is to say that . . I have no clue how to use Overlays . . I don't know why they are uh . . -_- . . something . . they seem to be really cool . . because you are still able to see the original page uh . . -_- . . but they are -_- . . (1) if you open multiple overlays at once . . so for example . . 2 or 3 overlays on top of the overlay that you already have open . . that is really possibly not going to look that great . . uh . . -_- . . I haven't tried that is practice . . uh . . well . . -_- . . not in a serious experimental way as to try to verify the claims I'm making . . but in my imagination . . I'm creating the idea that . . if you have so many . . uh . . overlays open . . you have to . . (1.1) press the various different overlays . . to go back to the very original page . . uh . . which is the uh . . base page I suppose you could call it . . the page without overlays or something like that . . and so there are no transparent or semi-transparent backgrounds that you need to click on to get back to the original page surface area . . or something like that . . uh . . well this . . uh . . way of navigating . . by . . clicking . . it's uh . . possibly uh . . equivalent or possibly -_- the same thing as clicking lots of buttons to navigate back . . uh . . or clicking a lot of back buttons . . which isn't necessarily uh . . an efficient way to navigate on a web page . . and so for example . . "scrolling" . . is a really uh . . popular way of navigating a webpage these days as it comes to my attention -_- . . but uh . . well . . uh . . scrolling up and down for example is uh . . a really fast and effective process . . and traversing through the page contents of a page . . and so for example . . uh . . if you can avoid clicking a lot of down arrow or up arrow buttons to go up and down a page . . you are uh . . really making the experience possibly more uh . . smoother or something like that . . uh . . since possibly uh . . button mashing . . -_- . . button pressing . . uh . . to uh . . travel across the page isn't always the type way that people are appearing to navigate in experiences thatare nice and easy to use . . uh . . possibly another . . idea that is related to this topic of scrolling . . is how . . if you hold down the arrow keys in the mmorp (massive multiplayer online role playing) game . . you are possibly knowing that . . you don't need to repeatedly press the arrow key . . and instead . . you can keep your finger pressed on it . . or . . "so-called" "hold it down" . . and so for example . . you can "hold down" . . the . . arrow key . . and that allows your character to . . constantly apply the effect of traversing the landscape . . uh . . and so that type of effect is possibly comfortable to players today who are familiar with games like world of warcraft where you are uh . . typically or possibly able to use things like . . "WASD" . . uh . . or . . "WASD-QE" . . to traverse the landscape . . uh . . -_- well . . uh . . those keyboard keys are able to allow you to perform the same types of abilities like . . scrolling . . uh . . because of possibly how the automation effect of . . getting to where you want with . . only uh . . one intended effect button or something like that . . is uh . . possibly the relation that could be made here . . 
- Well overlays have been a mystery to me . . and then . . uh . . uh . . well . . at least these notes are now making it more sympathetic for me to reason about their possibilities . . and so for example . . one way that . . overlays are -_- . . I don't know . . -_- . . uh . . -_- . . -_- . . -_- . . -_- . . -_- . . -_- . . uh . . -_- . . -_- [Seth]: you have to forgive my frous [friend] . . he is meaning to say that you are able to use overlays to comportmentalize [compartmentalize] your experience in saovisue [several, jargon, directions] at once without having to compromise the experience of the user's ability to make it is [easy] on themselves to travel back to their original intended content
- hmm . . I am still working on the idea of an overlay that also acts like a website traversal mechanism and so for example you can visit the previous overlays that you were on without having a stack or group of them overlayed one on top of the other . . and so for example . . (1) a menu list component [if you are using the vuetify component library for example] can help you accomplish this because a list of indices for the overlays that you had previously acquired is possibly a way of relating back to them in a list that is appendixed to show you which ones you have already visited . . (1.1) such a menu could contain (1.1.1) a name of the overlay item . . (1.1.2) a photographic image of the contents of the overlay (1.1.3) other types of information that are useful for that related topic but that I havne't necessarily thought about yet . . I'm sorry . . 

A quick example could look like . . 

[Menu List Button] (click this button)

------------------------------------------------------
                    _____________
Overlay Name #1     |           |       [Close Button]
                    |  Image #1 |
                    |___________|

-------------------------------------------------------
                    _____________
Overlay Name #2     |           |       [Close Button]
                    |  Image #2 |
                    |___________|

-------------------------------------------------------
                    _____________
Overlay Name #3     |           |       [Close Button]
                    |  Image #3 |
                    |___________|

-------------------------------------------------------
(this list opens when you click the button)

[Close Button] - this button closes the overlay
[Overlay Name] - clicking on the overlay name allows you to show its contents
[Image] - an image is useful queue to preview the contents of intended concern

- [3.0] I was thinking about the topic of . . consciousness . . I think . . at the time . . I could . . have been . . watching the video . . [1.1] . . in which the comment was made at . . [1.1 @ 12:41 - 12:56] "We're all . . slightly different . . We're in different vehicles . . but . . the . . Intelligence . . that's . . inhabiting . . these vehicles . . It's the same one . . Intelligence . . it's the same . . consciousness . . 
" . . I . . (1) envisioned consciousness as a type of creature like a human being . . uh . . and so . . this "one intelligence" . . that is being talked about in the video . . could possible be imagined as . . "one creature" . . as I allowed myself to imagine . . at that time of this day . . uh . . and so for example . . if it is like a fox shaped creature . . then . . it has . . fox-shaped cells and those cells form groups of cells like tissues and organs and other types of structures . . that create the fox creature . . together . . in their way of being or something like that . . and so . . well . . uh . . (2) I imagined that . . if . . consciousness of individuals is as unique like the consciousness of a cell or like the being of a cell . . uh . . -_- . . which I guess is like a type of microorganism . . uh . . and so . . each individual works together in groups like tissues which are possibly related to groups of consciousness or . . consciousness families or soul mates familiar of consciousness or something like that . . soulmates . . or . . soulfamilies . . or something like that . . could possibly be . . tissue-related spiritual consciousness related tissue formats or something like that . . uh . . and then . . our essence as consciousness intelligence beings . . uh . . uh . . I guess . . uh . . if . . (1) and (2) have been thought about in this respect . . I thought that it could be an interesting idea to imagine . . uh . . that this type of . . consciousness creature that . . uh . . my consciousness uh . . as an individual is uh . . a part of . . and so for example . . another person's consciousness uh . . like uh . . Louise Kay . . could also . . be uh . . individualized uh . . like a portion of this ghost creature that uh . . we don't necessarily know that we're serving or something like that -_- which is kind of a rude observation because possibly it's already the order of events that a cell uh . . like the cellular microorganisms that compose of the human body . . uh . . are uh . . possibly aware to some aspect or to some degree that they are helping someone type on the keyboard or cut a grapefruit open with a knife or something like that . . uh . . possibly not necessarily in terms relatable to english knowledge or uh . . english translated -_- -_- I don't know -_- this topic goes to things that are talked about in the . . Seth books . . which are more encouraged to be read if you are interested to learn about this topic of how cellular consciousness relates to human consciousness or something like that . . uh . . but the idea is that these are uh . . knowledges that are uh . . known and so it is uh . . related that the hand of the person is also knowing that is part of the person and helping the person fulfill a greater purpose that the hand itself doesn't necessarily know in terms that the person understands like . . "I'm wiping my bottom with a piece of toilet paper" or uh . . -_- . . "I'm peeing with my weiner and you are helping me do that, you great technological tool case [wink face emoji]" . . but if that is an idea . . then possibly our hand of earth spirit consciousness could possibly be helping the consciousness of a being that we are helping but not uh fully aware of uh . . what types of those things that are taking on in other aspects of its communities . . or something like that . . but uh . . well . . I started to ask myself . . what is the name of this consciousness creature . . that we are a part of in uh . . our understanding of the places of consciousness that surround our being today on earth but also possibly elsewhere in the universe wehreever other consciousness creatures reside ? . . well uh . . I started to channel the . . name . . "Dourning" . . which sounds epic to me . . :D but in any case . . that was a name . . and so possibly the great spirit consciousness creature of which we could possibly ascribe "oneness" too . . could possibly be related to the name "Dourning" . . hehe but that is of course only an idea and opinion of this lonely channeler person who is uh . . typing for fun

- [4.0] Dourning . . as a being . . uh . . composed of consciousness beings . . that creat their own lifetimes and experiences . . uh . . well . . uh . . I was also told the channeling message that . . "A Day Is Like A Being" . . well that is really surprising to me . . for example . . the topic of . . "Being" . . seems to possibly relate to all that is . . and all that could "quote-on-quote" "be" . . but . . how is it . . that . . "being" . . could be like . . a "day" . . a . . "quote-on-quote" . . "day" . . to this creature . . -_- . . of course I'm possibly aware that translations aren't possibly a thing that are comparable in such related terms but still it is interesting to imagine that it is an interesting idea to . . "be a being" . . or that . . "other days could . . uh . . be other types of being . . or possibly unrelated to being in general" . . uh . . and so . . maybe . . "sleep periods??" . . in which "non-being related topics" are its essense ? . . well -_- . . it's such an interesting topic . . and so throughout the so called . . "week" . . of "Dourning" . . O_O

[1.1]
Do Good #43 Louise Kay (spiritual teacher) on awakening through adversity
By Rob Watson
https://www.youtube.com/watch?v=s_GccRiTrnU


## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

Write Notes Here . . 

## Part II - Transcript Central Vertical Area Body Of The Page

- A Dance Memory Fever
  Which you're not allowed
    to observe [1.0]

- Overlays are mostly separable from Reality [2.0]

- Overlay Systems are aspects of Realities which exist to create a map of identical constructs Related to the Reality Ilusion that is Created By you

[My Real Name Here] Not Now Focus on the Live Stream

-

Dourning is a Key . Day is a being. Day has a week [4.0]
A being of eternal creation

## Part III - Transcript Left Vertical Area Margin Of The Page

- Dourning [3.0]
is a name
Names give us power to create our own reality

## Part IV - Transcript Right Vertical Area Margin Of The Page

January
11, 2021

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels






