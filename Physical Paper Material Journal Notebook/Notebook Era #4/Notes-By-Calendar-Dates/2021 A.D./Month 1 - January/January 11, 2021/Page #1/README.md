


# Page Number 102

## Intention Description

The intention of this document is `to transcribe the notes that I've taken from my physical pencil-and-paper notebook taking process in a physical paper material journal notebook` and transcribe those notes to an electronic digital format like a "word document (.docx)" or a "markdown document (.md)" file or an electronic digital medium communication file document that is interested to be communicated through a digital medium format like a digital mediumship computer or something like that.

## Anticipation Description

It is anticipated that this document creates an image in the mind of the reader that relates to concepts relating to concepts and that the reader enjoys those concepts and finds them related to the topics that they are themselves asking about relating to how to introduce those concepts into their own actualized environments where they can conceptualize their own actualized constraints

## Description

A word-for-word description of the notes that I take from my physical paper notebook except for circumstances where the writings in the notebook aren't necessarily well to write or to draw using a keyboard textpad editor which is quite not necessarily the same as a pencil-and-paper way of writing which makes this type of transcript difficult to write in certain circumstances

Description For The Original Paper Material Journal Notebook Page

- Date Created: 11 January, 2021
- Page Number: Not Available

Description For The New Transcript Page Created Here

- Date Created: 12 January, 2021
- Time Page Transcription Started: 22:22
- Time Page Transcription Stopped: xx:xx

## Benefits

- **You Can Learn Something Maybe**: Maybe by reading the codebase here, you can learn something

## Features

- **Education**: This page is for educational purposes only, it is not meant to be a tool to hurt or harm someone but maybe to do creative things like toss paper airplanes around that have words and can share contact messages about new ideas

- **Create your own copy**: You are allowed to do what you want with this document

- **You Can Transform This Document**: You can transform this document to find your own interesting dilemmas to solve and digest

- **Delete**: You can ignore this document so it's not disrupting your own personal life and also ask neighbors to ignore this as well and so it's not effecting the community that you would like to create. Sorry for being a butthead.

- **Initialize**: You can initialize something like an entrepreneurial sprint of companies and new endeavors based around the topic that are introduced by the written content here.

- **Uninitialize**: You can uninitialize something by stopping what you're doing but also reversing those conditions that you had created by asking everyone around you to also slow down and re-consider the topic of interest and so you can have your patty cakes

- **Start**: You can start something like a race track highway that serves all your customers with new pooty tang that they had to imagine in the first place to get to a reality like this one which organizes pooty tang

- **Stop**: You can stop eating pooty tang with your friends to allow your next door neighbor Jon to eat all the pooty tang on his own because he has 2 front teeth that say he's okay.

## Limitations

- **Not Always Word-For-Word**: A note is left on the page if the transcript item cannot be easily transcribed because of how the transcript is formatted from the paper and how that differentiates from the computer transcript creation process and the `range of difficulties in` satisfying those mediumships `being equal and related` **(1)** Sometimes the `words are difficult to read` on the page as well since my notes can sometimes be half-hearted and effortless to quickly relay a communication message but that has the disadvantage of being really `difficult to recreate without spending large amounts of transcription time` to relate to all th various other dialogues on the page that are giving context clues on the history of the nature of the types of words that are given on the page **(2)** Graphs and other images that are drawn on the page are also not necessarily easy to recreate without consuming a great majority of re-creation type to spell out the duties of each of the pencil markings that were painted on the page with a mechanical pencil which is the usual type of instrument that is being used on these dutiful days of typings and typing with pencil strokes and pencil strokes

## Additional Notes

- [1.0] A Robot Voice is how the term phrase "Project is being Aspectualized!" was originally pronounced . . it sounded like a robot was checking for alternative versions of the project . . but it was uh . . like in a voice that was half-hearted and joking or something like that . . it was really meant as a joke . . I think I was talking to myself . . and I suppose also that it could have been a suggested thought or something like that . . uh . . "Project is being Aspect Checked!" . . was also said in a robot voice . . that was meant to sort of be in a joking way since I myself am uh . . possibly uh . . interested in the topics of robotics and take on some personal qualities that one could ascribe to as robotic in their mannerisms or robotic in their way of being . . and so possibly I was saying this to myself as a way of joking around since perhaps I don't really appreciate being called a robot sometimes -_- since robots are at least not in the particular flavor in how I think about how they could possibly be in their potential endeavourings which are possibilityes that are also -_- I don't know -_- robotics is -_- de-humanizing in some aspect in the way it's uh presenting itself in my memory but at least uh -_- the topic at least still relates to human beings and so for example human beings could possibly be considered as robots as well although biological physical tissue robotics is possibly the relation that we are considering or something like that but uh . . well I don't know -_- . . I don't know . . I don't know . . there could be so many names for the term robotics . . and uh . . well either way it's possibly a restriction to have a term for more types of robotic entities or something like that . . but that is to me a new thought that I am only expressing now but it has been possibly been latent in the type of thinking that is happening in this topic of relating things to multidimensionality and so for example even the name can be mobile and have a new flavor and so each time you converse the name of "robotics" it can have a new title that is not necessarily related to the title that you had considered before and so it is new and fresh and alive and so considering the topic of robotics in which those types of qualities are spirigin themselves to be engaeged with . . and so even human robotics is new and fresh and alive and so if you have a naming of robotic systems that doesn't necessarily allow for one word meaning apparels to dress the whole branch of the field then it is possibly a good alternative wey ti engage to spirits of those like minded people that wasy to consider static ship computer machines and star ship flight beings or something something something something something. [a few moments later] . . it is now . . only 20 or 30 seconds after writing the previous worsd and now I was reading back at the words that I had written . . and I saw the word . . "void" . . written in place of . . "word" . . in the context that reads . . "for one word meaning apparels" . . and so . . possibly . . "void" . . is a good term for a computer that creates its own reality . . but uh . . well . . that was a spontaneous uh . . overlay of my physical vision in which that word appeared to overlay itself in my line of sight in place of the word "word" . . and so . . uh . . well . . I'm uh . . also surprised uh . . by how possibly this word could be related to uh . . hmm . . interesting types of beings . . and so for example . . even . . creatures . . like . . -_- . . void walkers . . come to mind as I think about this topic . . "void walkers" . . are present in world of warcraft . . and are creatures that are possibly relatable to humanoid beings . . in how they are constructed although in a physical and personal sense . . possibly their construction can be new and original to present them in a real way in the realy physical world which we as human creatures of the earth planet today are familiar with . . uh . . possibly they exist in other ways or uh something if we look where we are not familiar with looking for them . . our video games at least give us a clue at their possible existence even if they are only fantasy driven endeavours to consider the nature of creativity or the nature of the order of the events that bring us here to create in this waking world with which we are creatively familiar is that correct?



## Contact Information

| Person Name | Person Relation To This Document | Person Communication Connection Key |
| -- | -- | -- |
| Jon Ide | (1) Document Creator, (2) Document Transcription Creator | practialoptimism9@gmail.com |

## News Updates


## Community Restrictions & Guidelines

- Please don't use these documents to be mean to anyone. These are only words.

## Codebase

Here is a word-for-word transcript of the communication content messages that I wrote from my physical paper material journal notebook

📝 Notes Transcription Legend Tree:

💭 [Comment] - Comments are written in square brackets (for example: []). Comments aren't part of the original text but allow the text to be referenced outside of the transcript area . . with further notes like the notes available in the . . "Additional Notes" section of this document

## Part I - Transcript Top Horizontal Area Header Of The Page

"I love you
for making this
possible!"

## Part II - Transcript Central Vertical Area Body Of The Page

Project is being
Aspectualized!

Project is being
Aspect Checked!
-
Dictiomes
- Naming Dictiomes
- Time based Name Direction (time directed name dictiomes)
|||||

Number System that allows for               [<--]
infinite parrallel calculations
to be [bad] at once and with no time delay

Number System number 2 for your
Love Algorithm Computer
Program is to
- Represent Numbers in Orders of Magnitude that scale without limit except perhaps by time ordered magnitudes that limit the computers calculus and memory requirements

## Part III - Transcript Left Vertical Area Margin Of The Page

Robot
Voice
Apparel

## Part IV - Transcript Right Vertical Area Margin Of The Page

January
11, 2021

[<--] Ideal for real world physical hardware to catch up to this standard

## Provider Resource

- We write a lot of thoughts
- Those thoughts are provided by inspiration from various sources including (1) The Seth Books (2) Spiritual Teachers (3) Electrical and Mechanical Engineers (4) Research Exercises such as (4.1) Introspection and (4.2) Asking questions and observing what other people think

## Consumer Resource

You can access these notes by:

(1) Dialog Entry Relational Journal 0, By Jon Ide, Provided by "Gitlab" Storage Service https://gitlab.com/PracticalOptimism/dialog-entry-relational-journal-0

(2) dialog-entry-relational-journal-0, By Jon Ide, Provided by "Google Drive" Storage Service https://drive.google.com/drive/folders/1N0yuOGKfB2Tp2tRhFdd1jASXfS2YdyZE

## Usecase Resource

You can access the resource usecases considered here by considering

(1) writing in your own physical paper material journal notebook and then transcribing those notes onto an electronic digital medium format like a markdown file and publishing it on the internet at (1.1) `Gitlab` or (1.2) `Google Drive` for everyone to read and share your thoughts

## Community Members & Volunteers

- Jon Ide

## Usecase Provided By Related Resources

Usecase Related To Computer Science Notes Transciption On The Internet

| Resource Name | Resource Relation To This Document | Resource Communication Connection Key |
| -- | -- | -- |
| Amos Wenger | (1) Computer Science Related Person (2) Blogger and Notes Taker | (1) @fasterthanlime on Social Media (2) https://fasterthanli.me |


## Settings

Settings for the Physical Paper Material Journal Notebook Writing Process

- Type Of Pencil Used:
  - A Mechanical Pencil
- Type Of Notebook Used:
  - 120 Sheets Notebook Paper | 3 Subject | 10.5 in x 8 in (26.7 x 20.3 cm) | Wide Ruled


## Your Account

You look through time tunnels






